﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using log4net;
using NPoco;

namespace Cms.Services.Tracking
{
    internal class PageTrackingService : IPageTrackingService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(PageTrackingService));

        private readonly IIpToCountryService _ipToCountryService;

        public PageTrackingService(IIpToCountryService ipToCountryService)
        {
            _ipToCountryService = ipToCountryService;
        }

        public async Task TrackPageViewAsync(ulong websiteId, ulong pageId, string ipAddress, string sessionId, string persistedId)
        {
            try
            {
                var country = await _ipToCountryService.GetCountryAsync(ipAddress);

                using (var database = new Database("CmsDatabaseConnection"))
                {
                    database.Insert(new PageTrackingDto
                    {
                        Date = DateTime.UtcNow,
                        SessionId = sessionId,
                        PersistedId = persistedId,
                        PageId = Convert.ToInt64(pageId),
                        WebsiteId = Convert.ToInt64(websiteId),
                        Country = country,
                        Count = 1,
                    });
                }
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
            }
        }

        public IEnumerable<Tuple<ulong, int>> GetTheTenMostViewedPagesForOneMonth(ulong websiteId)
        {
            const string sql = @"
select top 10 PageId, count(*) as [count]
from PageTracking
where Date > dateadd(m, -1, getutcdate())
and WebsiteId = @0
group by PageId
order by count(*) desc";

            try
            {
                var rows = new List<PageCountDto>(10);
                var tuples = new List<Tuple<ulong, int>>(10);

                using (var database = new Database("CmsDatabaseConnection"))
                {
                    rows.AddRange(database.Fetch<PageCountDto>(sql, Convert.ToInt64(websiteId)));
                }

                foreach (var dto in rows)
                {
                    tuples.Add(Tuple.Create(Convert.ToUInt64(dto.PageId), dto.Count));
                }

                return tuples;
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        public int GetTotalPageViewsForOneMonth(ulong websiteId)
        {
            const string sql = @"
select count(*) as [count]
from PageTracking
where Date > dateadd(m, -1, getutcdate())
and WebsiteId = @0";

            try
            {
                using (var database = new Database("CmsDatabaseConnection"))
                {
                    return database.ExecuteScalar<int>(sql, Convert.ToInt64(websiteId));
                }
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        public IEnumerable<Tuple<string, int>> GetPageViewsPerCountryForOneMonth(ulong websiteId)
        {
            const string sql = @"
select Country, count(*) as [count]
from PageTracking
where Date > dateadd(m, -1, getutcdate())
and WebsiteId = @0
group by Country
order by count(*) desc";

            try
            {
                List<CountryCountDto> rows;
                List<Tuple<string, int>> tuples;

                using (var database = new Database("CmsDatabaseConnection"))
                {
                    rows = database.Fetch<CountryCountDto>(sql, Convert.ToInt64(websiteId));
                    tuples = new List<Tuple<string, int>>(rows.Count);
                }

                foreach (var dto in rows)
                {
                    tuples.Add(Tuple.Create(dto.Country, dto.Count));
                }

                return tuples;
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        public int GetTotalSessionsForOneMonth(ulong websiteId)
        {
            const string sql = @"
select count(distinct SessionId) as [count]
from PageTracking
where Date > dateadd(m, -1, getutcdate())
and WebsiteId = @0";

            try
            {
                using (var database = new Database("CmsDatabaseConnection"))
                {
                    return database.ExecuteScalar<int>(sql, Convert.ToInt64(websiteId));
                }
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        public IEnumerable<Tuple<DateTime, int>> GetTotalPageViewsPerMonth(ulong websiteId)
        {
            const string sql = @"
select convert(nvarchar(7), Date, 20) as month, count(*) as [count]
from PageTracking
where Date > dateadd(yy, -1, getutcdate())
and WebsiteId = @0
group by convert(nvarchar(7), Date, 20)
order by convert(nvarchar(7), Date, 20)";

            try
            {
                var rows = new List<MonthCountDto>(13);
                var tuples = new List<Tuple<DateTime, int>>(13);

                using (var database = new Database("CmsDatabaseConnection"))
                {
                    rows.AddRange(database.Fetch<MonthCountDto>(sql, Convert.ToInt64(websiteId)));
                }

                foreach (var dto in rows)
                {
                    var date = DateTime.ParseExact(dto.Month, "yyyy-MM", CultureInfo.InvariantCulture);
                    tuples.Add(Tuple.Create(date, dto.Count));
                }

                return tuples;
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        public IEnumerable<Tuple<DateTime, int>> GetTotalPageViewsPerDay(ulong websiteId)
        {
            const string sql = @"
select convert(nvarchar(10), cast(Date as date), 20) as day, count(*) as [count]
from PageTracking
where Date > dateadd(m, -1, getutcdate())
and WebsiteId = @0
group by cast(Date as date)
order by cast(Date as date)";

            try
            {
                var rows = new List<DayCountDto>(32);
                var tuples = new List<Tuple<DateTime, int>>(32);

                using (var database = new Database("CmsDatabaseConnection"))
                {
                    rows.AddRange(database.Fetch<DayCountDto>(sql, Convert.ToInt64(websiteId)));
                }

                foreach (var dto in rows)
                {
                    var date = DateTime.ParseExact(dto.Day, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    tuples.Add(Tuple.Create(date, dto.Count));
                }

                return tuples;
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        public int GetAveragePageViewsPerVisit(ulong websiteId)
        {
            const string sql = @"
with viewsPerSession as (
    select SessionId, count(*) as views
    from PageTracking
    where Date > dateadd(m, -1, getutcdate())
    and WebsiteId = @0
    group by SessionId
)
select(
  (select max(views) from (select top 50 percent views from viewsPerSession order by views) as bottomHalf) +
  (select min(views) from (select top 50 percent views from viewsPerSession order by views desc) as topHalf)
) / 2 as Median";

            try
            {
                using (var database = new Database("CmsDatabaseConnection"))
                {
                    return database.ExecuteScalar<int>(sql, Convert.ToInt64(websiteId));
                }
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        public int GetReturningVisitorsInPercent(ulong websiteId)
        {
            const string sql = @"
with usersWithMoreThanOneSession as (
  select PersistedId, count(distinct SessionId) as c
  from PageTracking
  where Date > dateadd(m, -1, getutcdate())
  and WebsiteId = @0
  group by PersistedId having count(distinct SessionId) > 1
)
select ((select count(*) from usersWithMoreThanOneSession) * 100 / (case (select count(distinct PersistedId) from PageTracking where Date > dateadd(m, -1, getutcdate()) and WebsiteId = @0) when 0 then 1 else (select count(distinct PersistedId) from PageTracking where Date > dateadd(m, -1, getutcdate()) and WebsiteId = @0) end))";

            try
            {
                using (var database = new Database("CmsDatabaseConnection"))
                {
                    return database.ExecuteScalar<int>(sql, Convert.ToInt64(websiteId));
                }
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        public int GetTotalViewsPerPage(ulong pageId)
        {
            const string sql = @"select count(*) from PageTracking where PageId = @0";

            try
            {
                using (var database = new Database("CmsDatabaseConnection"))
                {
                    return database.ExecuteScalar<int>(sql, Convert.ToInt64(pageId));
                }
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        private class PageCountDto
        {
            public long PageId { get; set; }
            public int Count { get; set; }
        }

        private class CountryCountDto
        {
            public string Country { get; set; }
            public int Count { get; set; }
        }

        private class MonthCountDto
        {
            public string Month { get; set; }
            public int Count { get; set; }
        }

        private class DayCountDto
        {
            public string Day { get; set; }
            public int Count { get; set; }
        }

        [TableName("PageTracking")]
        [PrimaryKey("Date", AutoIncrement = false)]
        private class PageTrackingDto
        {
            public DateTime Date { get; set; }
            public string SessionId { get; set; }
            public string PersistedId { get; set; }
            public long PageId { get; set; }
            public long WebsiteId { get; set; }
            public string Country { get; set; }
            public int Count { get; set; }
        }
    }
}
