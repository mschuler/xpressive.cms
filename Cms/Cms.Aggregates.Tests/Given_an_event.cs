﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.EventSourcing;
using Cms.EventSourcing.Contracts.Events;
using Xunit;

namespace Cms.Aggregates.Tests
{
    public class Given_an_event
    {
        [Fact]
        public void Then_there_is_an_event_handler()
        {
            var events = typeof(EventBase<>).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(IEvent)))
                .ToList();

            var eventHandlers = typeof(EventBase<>).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(IEventHandler)))
                .ToList();

            var unhandledEvents = new List<string>();

            foreach (var eventType in events)
            {
                var isHandled = false;
                foreach (var eventHandlerType in eventHandlers)
                {
                    if (RedirectToWhen.IsMethodAvailable(eventHandlerType, eventType))
                    {
                        isHandled = true;
                        break;
                    }
                }
                if (!isHandled)
                {
                    unhandledEvents.Add(eventType.Name);
                }
            }

            Assert.True(unhandledEvents.Count == 0, string.Format("There are events without an EventHandler\n{0}", string.Join("\n", unhandledEvents)));
        }
    }

    public class Given_the_solution
    {
        [Fact]
        public void Then_plot_all_events()
        {
            var events = typeof(EventBase<>).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(IEvent)))
                .Select(e => e.Name)
                .OrderBy(e => e)
                .ToList();

            foreach (var @event in events)
            {
                Console.WriteLine(@event);
            }
        }
    }
}
