using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Events
{
    internal class DynamicObjectEventHandler : EventHandlerBase
    {
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly IDynamicObjectRepository _repository;

        public DynamicObjectEventHandler(
            IDynamicObjectRepository repository,
            IDynamicObjectDefinitionRepository definitionRepository,
            IUserRepository userRepository,
            IGroupRepository groupRepository)
        {
            _repository = repository;
            _definitionRepository = definitionRepository;
            _userRepository = userRepository;
            _groupRepository = groupRepository;
        }

        public void When(DynamicObjectCreated @event)
        {
            var definition = _definitionRepository.Get(@event.DefinitionId);
            var tenantId = @event.TenantId;

            if (tenantId == 0)
            {
                tenantId = definition.TenantId;
            }

            var o = new DynamicObject
            {
                Id = @event.AggregateId,
                PublicId = @event.PublicId,
                TenantId = tenantId,
                DefinitionId = @event.DefinitionId,
                CreationDate = @event.Date,
            };

            foreach (var definitionField in definition.Fields)
            {
                o.Add(new DynamicObjectField
                {
                    Name = definitionField.Name,
                    ContentType = definitionField.ContentType,
                    Value = string.Empty,
                });
            }

            FillFields(o, @event.Values);

            _repository.Add(o);
        }

        public void When(DynamicObjectChanged @event)
        {
            var o = _repository.Get(@event.AggregateId);

            FillFields(o, @event.Values);
        }

        private void FillFields(DynamicObject obj, IDictionary<string, string> values)
        {
            foreach (var field in obj.Fields)
            {
                string value;
                if (values.TryGetValue(field.Name, out value))
                {
                    field.Value = value;
                }
            }
        }

        public void When(DynamicObjectDeleted @event)
        {
            _repository.Delete(@event.AggregateId);
        }

        public void When(DynamicObjectDefinitionFieldAdded @event)
        {
            var objects = _repository.GetAll().Where(o => o.DefinitionId == @event.AggregateId).ToList();

            foreach (var o in objects)
            {
                var field = new DynamicObjectField
                {
                    Name = @event.Name,
                    ContentType = @event.ContentType,
                };
                o.Add(field);
            }
        }

        public void When(DynamicObjectDefinitionFieldRemoved @event)
        {
            var objects = _repository.GetAll().Where(o => o.DefinitionId == @event.AggregateId).ToList();

            foreach (var o in objects)
            {
                var toRemove = o.Fields
                    .Where(f => f.Name.Equals(@event.Name, StringComparison.OrdinalIgnoreCase))
                    .ToList();

                foreach (var field in toRemove)
                {
                    o.Remove(field);
                }
            }
        }

        public void When(DynamicObjectDefinitionFieldRenamed @event)
        {
            var objects = _repository.GetAll().Where(o => o.DefinitionId == @event.AggregateId).ToList();

            foreach (var o in objects)
            {
                var field = o.Fields.FirstOrDefault(f => f.Name.Equals(@event.OldName, StringComparison.Ordinal));

                if (field != null)
                {
                    field.Name = @event.NewName;
                }
            }
        }

        public void When(DynamicObjectShared @event)
        {
            var o = _repository.Get(@event.AggregateId);
            var user = _userRepository.Get(@event.UserId);
            var allowedTenantIds = user.GroupIds.Select(g => _groupRepository.Get(g).TenantId).ToList();
            var sharedTenantIds = @event.TenantIds.Union(o.SharedForTenants).Distinct().ToList();
            sharedTenantIds.RemoveAll(t => allowedTenantIds.Contains(t) && !@event.TenantIds.Contains(t));
            o.Share(sharedTenantIds);
        }
    }
}
