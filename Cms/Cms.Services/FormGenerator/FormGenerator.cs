﻿using System;
using System.Collections.Generic;
using System.Text;
using Cms.Aggregates.Pages;
using Cms.Services.Contracts;
using Cms.SharedKernel;

namespace Cms.Services.FormGenerator
{
    public class FormGenerator : IFormGenerator
    {
        private readonly IList<IFormFieldRenderer> _entries = new List<IFormFieldRenderer>();
        private readonly ILocalizationService _localizationService;
        private readonly ICurrentRequestService _currentRequestService;
        private readonly ulong _formId;

        public FormGenerator(ILocalizationService localizationService, ICurrentRequestService currentRequestService, ulong formId)
        {
            _localizationService = localizationService;
            _currentRequestService = currentRequestService;
            _formId = formId;
        }

        public void AddField(string name, string type, bool isRequired, IEnumerable<string> options = null, bool isMultiple = false)
        {
            switch (type.ToLowerInvariant())
            {
                case "text":
                    AddField(new FormFieldTextboxRenderer(name, isRequired));
                    break;
                case "number":
                    AddField(new FormFieldNumberRenderer(name, isRequired));
                    break;
                case "textarea":
                    AddField(new FormFieldTextareaRenderer(name, isRequired));
                    break;
                case "checkbox":
                    AddField(new FormFieldCheckboxRenderer(name, isRequired));
                    break;
                case "radio":
                    var radioOptions = options ?? new List<string>(0);
                    AddField(new FormFieldRadioRenderer(name, isRequired, radioOptions));
                    break;
                case "select":
                    var selectOptions = options ?? new List<string>(0);
                    AddField(new FormFieldListRenderer(name, isRequired, selectOptions, isMultiple));
                    break;
                default:
                    throw new NotSupportedException(type);
            }
        }

        public string Render(FormLayoutType type)
        {
            var sb = new StringBuilder("<form");

            if (type == FormLayoutType.Asside)
            {
                sb.Append(" class=\"form-horizontal\"");
            }

            sb.AppendFormat(" method=\"POST\" action=\"/api/form/{0}/entry\" accept-charset=\"UTF-8\">", _formId);
            sb.AppendLine();

            foreach (var entry in _entries)
            {
                var id = "f" + Guid.NewGuid().ToString("n").Substring(0, 10);
                entry.Render(type, sb, id);
            }

            var submitButton = new FormFieldSubmitButtonRenderer(_localizationService, _currentRequestService);
            submitButton.Render(type, sb, null);

            sb.AppendLine("</form>");

            return sb.ToString();
        }

        private void AddField(IFormFieldRenderer entry)
        {
            _entries.Add(entry);
        }
    }
}
