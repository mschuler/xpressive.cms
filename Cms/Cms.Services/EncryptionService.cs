using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Cms.SharedKernel;

namespace Cms.Services
{
    internal class EncryptionService : IEncryptionService
    {
        internal static EncryptionService Create(string encryptionKey)
        {
            return new EncryptionService(encryptionKey);
        }

        private readonly byte[] _encryptionKey;

        public EncryptionService() : this(ConfigurationManager.AppSettings["encryptionKey"]) { }

        private EncryptionService(string encryptionKey)
        {
            if (string.IsNullOrEmpty(encryptionKey))
            {
                throw new ArgumentNullException("encryptionKey", "There is no encryption key defined.");
            }

            while (encryptionKey.Length < 32)
            {
                encryptionKey += encryptionKey;
            }

            _encryptionKey = new byte[32];
            Buffer.BlockCopy(Encoding.UTF8.GetBytes(encryptionKey), 0, _encryptionKey, 0, 32);
        }

        public byte[] EncryptString(string plain)
        {
            var binary = Encoding.UTF8.GetBytes(plain);
            return Encrypt(binary);
        }

        public byte[] Encrypt(byte[] plain)
        {
            var iv = GetIv();
            var encrypted = EncryptInternal(plain, _encryptionKey, iv);
            var result = new byte[iv.Length + encrypted.Length];

            Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
            Buffer.BlockCopy(encrypted, 0, result, iv.Length, encrypted.Length);

            return result;
        }

        public byte[] Decrypt(byte[] encrypted)
        {
            var iv = new byte[16];
            var cipher = new byte[encrypted.Length - 16];

            Buffer.BlockCopy(encrypted, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(encrypted, 16, cipher, 0, cipher.Length);

            return DecryptInternal(cipher, _encryptionKey, iv);
        }

        public string DecryptString(byte[] encrypted)
        {
            var binary = Decrypt(encrypted);
            return Encoding.UTF8.GetString(binary);
        }

        private static byte[] EncryptInternal(byte[] plain, byte[] key, byte[] iv)
        {
            using (var algorithm = GetAlgorithm(key, iv))
            {
                using (var encryptor = algorithm.CreateEncryptor(key, iv))
                {
                    using (var stream = new MemoryStream())
                    {
                        using (var cryptoStream = new CryptoStream(stream, encryptor, CryptoStreamMode.Write))
                        {
                            cryptoStream.Write(plain, 0, plain.Length);
                        }

                        return stream.ToArray();
                    }
                }
            }
        }

        private static byte[] DecryptInternal(byte[] encrypted, byte[] key, byte[] iv)
        {
            using (var algorithm = GetAlgorithm(key, iv))
            {
                using (var decryptor = algorithm.CreateDecryptor(key, iv))
                {
                    using (var stream = new MemoryStream(encrypted))
                    {
                        using (var cryptoStream = new CryptoStream(stream, decryptor, CryptoStreamMode.Read))
                        {
                            using (var ms = new MemoryStream())
                            {
                                cryptoStream.CopyTo(ms);
                                return ms.ToArray();
                            }
                        }
                    }
                }
            }
        }

        private static byte[] GetIv()
        {
            using (var generator = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[16];
                generator.GetBytes(bytes);
                return bytes;
            }
        }

        private static SymmetricAlgorithm GetAlgorithm(byte[] key, byte[] iv)
        {
            return new AesManaged
            {
                Key = key,
                IV = iv,
            };
        }
    }
}