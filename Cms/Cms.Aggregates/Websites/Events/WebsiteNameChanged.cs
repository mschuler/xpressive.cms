﻿namespace Cms.Aggregates.Websites.Events
{
    public class WebsiteNameChanged : EventBase<Website>
    {
        public string Name { get; set; }
    }
}