﻿namespace Cms.Aggregates.Files.Events
{
    public class FileNameChanged : EventBase<File>
    {
        public string Name { get; set; }
    }
}