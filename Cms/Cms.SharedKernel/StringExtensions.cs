﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Cms.SharedKernel
{
    public static class StringExtensions
    {
        private const string ValidNonAlphanumericUrlChars = "-_.";
        private static readonly List<KeyValuePair<char, char>> _replacementUriChars;
        private static readonly List<KeyValuePair<char, string>> _replacementUriStrings;
        private static readonly char[] _controlCharsToRemove =
        {
            '\x00', '\x01', '\x02', '\x03', '\x04', '\x05', '\x06', '\x07', '\x08', '\x0B', '\x0C', '\x0E',
            '\x0F', '\x10', '\x11', '\x12', '\x13', '\x14', '\x15', '\x16', '\x17', '\x18', '\x19', '\x1A',
            '\x1B', '\x1C', '\x1D', '\x1E', '\x1F'
        };

        static StringExtensions()
        {
            _replacementUriChars = new Dictionary<char, char>
            {
                {'á', 'a'}, {'à', 'a'}, {'ă', 'a'}, {'â', 'a'}, {'å', 'a'}, {'ã', 'a'}, {'ą', 'a'}, {'ā', 'a'},
                {'æ', 'a'}, {'ć', 'c'}, {'ĉ', 'c'}, {'č', 'c'}, {'ċ', 'c'}, {'ç', 'c'}, {'đ', 'd'}, {'ď', 'd'},
                {'é', 'e'}, {'è', 'e'}, {'ĕ', 'e'}, {'ê', 'e'}, {'ě', 'e'}, {'ë', 'e'}, {'ė', 'e'}, {'ę', 'e'},
                {'ē', 'e'}, {'ğ', 'g'}, {'ĝ', 'g'}, {'ġ', 'g'}, {'ģ', 'g'}, {'ĥ', 'h'}, {'ħ', 'h'}, {'í', 'i'},
                {'ì', 'i'}, {'ĭ', 'i'}, {'î', 'i'}, {'ï', 'i'}, {'ĩ', 'i'}, {'į', 'i'}, {'ī', 'i'}, {'ı', 'i'},
                {'ĵ', 'j'}, {'ķ', 'k'}, {'ĸ', 'k'}, {'ĺ', 'l'}, {'ľ', 'l'}, {'ļ', 'l'}, {'ł', 'l'}, {'ń', 'n'},
                {'ň', 'n'}, {'ñ', 'n'}, {'ņ', 'n'}, {'ŋ', 'n'}, {'ó', 'o'}, {'ò', 'o'}, {'ŏ', 'o'}, {'ô', 'o'},
                {'ő', 'o'}, {'õ', 'o'}, {'ø', 'o'}, {'ō', 'o'}, {'œ', 'o'}, {'ŕ', 'r'}, {'ř', 'r'}, {'ŗ', 'r'},
                {'ś', 's'}, {'ŝ', 's'}, {'š', 's'}, {'ş', 's'}, {'ť', 't'}, {'ţ', 't'}, {'ŧ', 't'}, {'ú', 'u'},
                {'ù', 'u'}, {'ŭ', 'u'}, {'û', 'u'}, {'ů', 'u'}, {'ű', 'u'}, {'ũ', 'u'}, {'ų', 'u'}, {'ū', 'u'},
                {'ŵ', 'w'}, {'ý', 'y'}, {'ŷ', 'y'}, {'ÿ', 'y'}, {'ź', 'z'}, {'ž', 'z'}, {'ż', 'z'}, {'&', '-'},
                {' ', '-'}
            }.ToList();

            _replacementUriStrings = new Dictionary<char, string>
            {
                {'ß', "ss"}, {'ä', "ae"}, {'ö', "oe"}, {'ü', "ue"},
                {'Ä', "Ae"}, {'Ö', "Oe"}, {'Ü', "Ue"}
            }.ToList();
        }

        public static string ToValidUrlPart(this string urlPart)
        {
            if (string.IsNullOrEmpty(urlPart))
            {
                return string.Empty;
            }

            var parts = urlPart.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            return string.Join("/", parts.Select(ToValidUrlPartInternal));
        }

        public static string ToValidFileName(this string fileName)
        {
            var extension = fileName.GetExtension();
            var name = fileName.Substring(0, fileName.Length - extension.Length - 1);
            name = ToValidUrlPartInternal(name);
            return string.Format("{0}.{1}", name, extension);
        }

        public static string GetExtension(this string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return string.Empty;
            }
            if (fileName.IndexOf('.') < 0)
            {
                return string.Empty;
            }

            var p = fileName.LastIndexOf('.') + 1;
            return fileName.Substring(p);
        }

        public static string RemoveControlChars(this string s)
        {
            var sb = new StringBuilder(s.Length);

            foreach (var c in s)
            {
                if (_controlCharsToRemove.Contains(c))
                {
                    continue;
                }

                sb.Append(c);
            }

            return sb.ToString();
        }

        private static string ToValidUrlPartInternal(string urlPart)
        {
            _replacementUriStrings.ForEach(p => urlPart = urlPart.Replace(p.Key.ToString(CultureInfo.InvariantCulture), p.Value));
            _replacementUriChars.ForEach(p => urlPart = urlPart.Replace(p.Key, p.Value));

            var result = new StringBuilder(urlPart.Length);

            foreach (var c in urlPart)
            {
                if (char.IsLetterOrDigit(c) || ValidNonAlphanumericUrlChars.Contains(c))
                {
                    result.Append(c);
                }
            }

            urlPart = result.ToString();
            urlPart = urlPart.Replace("--", "-").Replace("--", "-");
            urlPart = urlPart.Replace("__", "_").Replace("__", "_");

            return urlPart;
        }
    }
}
