using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.FileStore
{
    internal class FileStoreProvider : IFileStoreProvider
    {
        private readonly IDictionary<Guid, IFileStoreFactory> _factories;

        public FileStoreProvider(IEnumerable<IFileStoreFactory> factories)
        {
            _factories = factories.ToDictionary(f => f.Id);
        }

        public IFileStoreFactory GetFactory(Guid id)
        {
            IFileStoreFactory factory;
            if (_factories.TryGetValue(id, out factory))
            {
                return factory;
            }
            return null;
        }

        public IEnumerable<IFileStoreFactory> GetFactories()
        {
            return _factories.Values.ToList().AsReadOnly();
        }
    }
}