﻿namespace Cms.TemplateEngine.Interfaces
{
    public interface IInterpreter
    {
        int Priority { get; set; }
        string Name { get; }
        void Calculate(TreeItem tree);
    }
}
