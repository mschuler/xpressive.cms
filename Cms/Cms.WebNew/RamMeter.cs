using System;
using System.Management;
using log4net;

namespace Cms.WebNew
{
    internal static class RamMeter
    {
        private static readonly double _totalMemory;

        static RamMeter()
        {
            try
            {
                using (var searcher = new ManagementObjectSearcher("SELECT Capacity FROM Win32_PhysicalMemory"))
                {
                    long bytes = 0;
                    foreach (var o in searcher.Get())
                    {
                        bytes += Convert.ToInt64(o.Properties["Capacity"].Value);
                    }

                    _totalMemory = bytes / 1024.0 / 1024;
                }
            }
            catch (Exception e)
            {
                var logger = LogManager.GetLogger(typeof(RamMeter));
                logger.Error("Unable to get total amount of memory.", e);
            }
        }

        public static double GetAvailableMemory()
        {
            try
            {
                using (var searcher = new ManagementObjectSearcher("SELECT FreePhysicalMemory FROM Win32_OperatingSystem"))
                {
                    long bytes = 0;
                    foreach (var o in searcher.Get())
                    {
                        bytes += Convert.ToInt64(o.Properties["FreePhysicalMemory"].Value);
                    }

                    return bytes / 1024.0;
                }
            }
            catch (Exception e)
            {
                var logger = LogManager.GetLogger(typeof(RamMeter));
                logger.Error("Unable to get available memory.", e);
            }
            return 0;
        }

        public static double GetTotalMemory()
        {
            return _totalMemory;
        }
    }
}