using System.Net;
using System.Web.Http;
using Cms.Aggregates.Forms;
using Cms.Services.Contracts;

namespace Cms.WebNew.Controllers.Admin
{
    public class FormQueryController : ApiControllerAuthorizedByApiKey
    {
        private readonly IFormRepository _formRepository;

        public FormQueryController(IApiKeyService apiKeyService, IFormRepository formRepository)
            : base(apiKeyService)
        {
            _formRepository = formRepository;
        }

        [HttpGet, Route("api/public/form/{formId:ulong}")]
        public string Get(ulong formId)
        {
            if (!IsAuthorized())
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            Form form;
            return _formRepository.TryGet(formId, out form) ? form.Template : string.Empty;
        }
    }
}