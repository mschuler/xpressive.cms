﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Cms.Aggregates;
using Cms.Aggregates.FileStore;
using Cms.SharedKernel;
using Microsoft.WindowsAzure.Storage.Blob;
using File = Cms.Aggregates.Files.File;

namespace Cms.Plugin.AzureStorage
{
    internal class AzureFileStore : IFileStore
    {
        private static readonly IDictionary<ulong, Uri> _fileVersionUrlCache = new Dictionary<ulong, Uri>();
        private static readonly object _fileVersionUrlCacheLock = new object();
        private readonly ulong _id;
        private readonly string _name;
        private readonly IDictionary<string, string> _settings;
        private readonly IContentTypeService _contentTypeService;

        public AzureFileStore(ulong id, string name, IDictionary<string, string> settings, IContentTypeService contentTypeService)
        {
            _id = id;
            _name = name;
            _settings = settings;
            _contentTypeService = contentTypeService;
        }

        public ulong Id { get { return _id; } set { } }
        public Guid FileStoreFactoryId { get { return new Guid("7D6418DD-DFB3-473D-BCDA-BB91A0C0110A"); } }
        public string Name { get { return _name; } }
        public IDictionary<string, string> Settings { get { return new ReadOnlyDictionary<string, string>(_settings); } }
        public bool IsLocalFilesystem { get { return false; } }

        public void Save(string fileName, ulong fileId, ulong version, FileInfo temporaryFile)
        {
            var blobClient = GetBlobClient();
            var container = blobClient.GetContainerReference(version.ToString("D"));
            container.CreateIfNotExists();
            container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
            fileName = fileName.ToValidFileName();

            var blob = container.GetBlockBlobReference(fileName);

            blob.Properties.ContentType = _contentTypeService.GetContentType(fileName.GetExtension());

            using (var stream = temporaryFile.OpenRead())
            {
                blob.UploadFromStream(stream);
            }

            _fileVersionUrlCache[version] = blob.StorageUri.PrimaryUri;
        }

        public void Delete(ulong fileId, ulong version)
        {
            var blobClient = GetBlobClient();
            var container = blobClient.GetContainerReference(version.ToString("D"));

            var blob = container.ListBlobs().SingleOrDefault() as CloudBlockBlob;

            if (blob == null)
            {
                return;
            }

            blob.Delete();
            container.DeleteIfExists();
        }

        public string GetLocalFileLocation(ulong fileId, ulong version)
        {
            throw new NotSupportedException();
        }

        public Uri GetIcon()
        {
            return new Uri("/admin/images/icons/azure_24.png", UriKind.Relative);
        }

        public Uri GetUrl(File file)
        {
            var version = file.Versions.Max(v => v.Version);
            Uri url;

            if (_fileVersionUrlCache.TryGetValue(version, out url))
            {
                return url;
            }

            lock (_fileVersionUrlCacheLock)
            {
                if (_fileVersionUrlCache.TryGetValue(version, out url))
                {
                    return url;
                }

                var blobClient = GetBlobClient();
                var container = blobClient.GetContainerReference(version.ToString("D"));
                var blob = container.ListBlobs().SingleOrDefault();

                if (blob == null)
                {
                    return null;
                }

                url = blob.StorageUri.PrimaryUri;
                _fileVersionUrlCache[version] = url;
                return url;
            }
        }

        public Uri GetUrl(File file, ImageSettingsDto imageSettings)
        {
            return GetUrl(file);
        }

        private CloudBlobClient GetBlobClient()
        {
            var blobClient = AzureAccountHelper.GetClient(Settings);

            var serviceProperties = blobClient.GetServiceProperties();
            serviceProperties.DefaultServiceVersion = "2011-08-18";
            blobClient.SetServiceProperties(serviceProperties);

            return blobClient;
        }
    }
}