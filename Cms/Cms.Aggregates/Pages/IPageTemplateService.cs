using System.Collections.Generic;
using Cms.Aggregates.PageTemplates;

namespace Cms.Aggregates.Pages
{
    public interface IPageTemplateService
    {
        string GetTemplate(PageTemplate pageTemplate);

        IEnumerable<IPageField> GetPageFields(PageTemplate pageTemplate);

        void Reset(PageTemplate pageTemplate);

        string ReplaceField(string template, string fieldName, string replacement);
    }
}