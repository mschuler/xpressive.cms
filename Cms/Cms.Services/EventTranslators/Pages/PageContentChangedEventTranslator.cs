using System;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.Websites;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Pages
{
    internal class PageContentChangedEventTranslator : IEventTranslator
    {
        private readonly IPageRepository _repository;
        private readonly IWebsiteRepository _websiteRepository;

        public PageContentChangedEventTranslator(IPageRepository repository, IWebsiteRepository websiteRepository)
        {
            _repository = repository;
            _websiteRepository = websiteRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageContentChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seiteninhalt ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageContentChanged)@event;
            var page = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Seiteninhalt <strong>{0}</strong> der Seite <strong>{1}</strong> wurde ge�ndert.", e.Name, page.Fullname);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var page = _repository.GetEvenIfDeleted(@event.AggregateId);
            return _websiteRepository.GetEvenIfDeleted(page.WebsiteId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "page";
        }
    }
}