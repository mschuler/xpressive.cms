using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Websites;

namespace Cms.Aggregates.Pages
{
    internal class PageDtoService : IPageDtoService
    {
        private readonly IPageRepository _pageRepository;
        private readonly IPageUrlService _pageUrlService;

        public PageDtoService(IPageRepository pageRepository, IPageUrlService pageUrlService)
        {
            _pageRepository = pageRepository;
            _pageUrlService = pageUrlService;
        }

        public object GetPageDto(Website website, PageBase page, List<object> content = null, bool withChildren = false)
        {
            List<object> children = null;

            if (withChildren)
            {
                children = page.Children.Select(c => GetPageDto(website, c, null, true)).ToList();
            }

            var p = page as Page;

            var title = p != null ? p.Title : string.Empty;
            var nr = p != null ? p.PublicId : 0;
            var enabled = p == null || p.IsEnabled;
            var templateId = p != null ? p.PageTemplateId : 0;
            var permalink = p != null ? _pageUrlService.GetPermalink(p) : string.Empty;

            return new
            {
                Id = page.Id.ToJsonString(),
                Label = page.Name,
                page.Name,
                Title = title,
                Nr = nr,
                page.Fullname,
                IsDefault = page.Id == website.DefaultPageId,
                Disabled = !enabled,
                WebsiteId = page.WebsiteId.ToJsonString(),
                TemplateId = templateId.ToJsonString(),
                Children = children,
                Content = content,
                page.IsLink,
                Permalink = permalink,
                Link = GetLink(page),
                IsCollapsed = true,
                page.SortOrder,
                ParentId = page.ParentPageId.ToJsonString(),
            };
        }

        private object GetLink(PageBase page)
        {
            var l = page as PageLink;
            if (l == null)
            {
                return null;
            }

            if (!string.IsNullOrEmpty(l.Url))
            {
                return new
                {
                    Id = "0",
                    Fullname = "",
                    WebsiteId = "0",
                    l.Url,
                };
            }

            var linkedPage = _pageRepository.Get(l.PageId);
            return new
            {
                Id = linkedPage.Id.ToJsonString(),
                linkedPage.Fullname,
                WebsiteId = linkedPage.WebsiteId.ToJsonString(),
                Url = "",
            };
        }
    }
}