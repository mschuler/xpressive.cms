﻿namespace Cms.Aggregates.Security.Events
{
    public class UserUnlocked : EventBase<User> { }
}