﻿using System;
using Cms.Aggregates.Forms;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class FormInterpreter : IInterpreter
    {
        private readonly IFormRepository _formRepository;

        public FormInterpreter(IFormRepository formRepository)
        {
            _formRepository = formRepository;
        }

        public int Priority { get; set; }

        public string Name
        {
            get { return "Form"; }
        }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function && tree.Value.Equals("form", StringComparison.OrdinalIgnoreCase))
            {
                CalculateForm(tree);
            }
            else if (tree.ItemType == TreeItemType.Attribute && tree.LeftItem != null)
            {
                CalculateAttribute(tree);
            }
            else if (tree.ItemType == TreeItemType.Function && tree.LeftItem != null)
            {
                CalculateFunction(tree);
            }
        }

        private void CalculateForm(TreeItem tree)
        {
            if (tree.Children.Count == 1 && tree.Children[0].ParameterType == ParameterType.Integer)
            {
                var formId = (int)tree.Children[0].Result;
                tree.Result = _formRepository.GetByPublicId(formId);
            }
        }

        private void CalculateFunction(TreeItem tree)
        {
            var form = tree.LeftItem.Result as Form;

            if (form == null)
            {
                return;
            }

            if (tree.Value.Equals("render", StringComparison.OrdinalIgnoreCase))
            {
                tree.Result = form.Template;
            }
        }

        private void CalculateAttribute(TreeItem tree)
        {
            var form = tree.LeftItem.Result as Form;

            if (form == null)
            {
                return;
            }

            switch (tree.Value.ToLowerInvariant())
            {
                case "id":
                    tree.Result = form.PublicId;
                    break;
                case "name":
                    tree.Result = form.Name;
                    break;
            }
        }
    }
}
