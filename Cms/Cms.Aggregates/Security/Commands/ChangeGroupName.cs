﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("group", PermissionLevel.CanChange)]
    public class ChangeGroupName : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }
    }
}