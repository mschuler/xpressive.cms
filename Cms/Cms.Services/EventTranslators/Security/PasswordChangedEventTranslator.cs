using System;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class PasswordChangedEventTranslator : IEventTranslator
    {
        private readonly IUserRepository _repository;

        public PasswordChangedEventTranslator(IUserRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PasswordChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Passwort ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var user = _repository.GetEvenIfDeleted(@event.AggregateId);
            var username = string.Format("{0} {1}", user.FirstName, user.LastName).Trim();
            return string.Format("Das Passwort des Benutzers <strong>{0}</strong> wurde ge�ndert.", username);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}