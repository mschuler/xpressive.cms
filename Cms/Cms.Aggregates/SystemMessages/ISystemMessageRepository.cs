﻿namespace Cms.Aggregates.SystemMessages
{
    public interface ISystemMessageRepository : IRepository<SystemMessage> { }
}