﻿using System.Collections.Generic;

namespace Cms.Aggregates
{
    public interface IRepository<T> where T : IAggregate
    {
        void AssertIdIsNotUsed(ulong aggregateId);

        T Get(ulong aggregateId);

        T GetEvenIfDeleted(ulong aggregateId);

        bool TryGet(ulong aggregateId, out T t);

        bool TryGetEvenIfDeleted(ulong aggregateId, out T t);

        IEnumerable<T> GetAll();

        void Add(T aggregate);

        void Delete(ulong aggregateId);

        void DeleteAllByTenantId(ulong tenantId);
    }
}