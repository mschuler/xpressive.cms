﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Cms.EventSourcing.Contracts.Events;
using Xunit;

namespace Cms.EventSourcing.Tests
{
    public class PerformanceTestWithDifferentEventDispatchings
    {
        [Fact]
        public void RedirectToWhen_ThreadPoolQueueUserWorkItem()
        {
            var countdownEvent = new CountdownEvent(0);
            var eventHandlers = Enumerable.Range(0, 100).Select(_ => new RedirectToWhenEventHandler(countdownEvent)).ToList<IEventHandler>();
            var eventDispatcher = new ThreadPoolEventDispatcher();
            Run(eventDispatcher, eventHandlers, countdownEvent);
            Run(eventDispatcher, eventHandlers, countdownEvent);
        }

        [Fact]
        public void RedirectToWhen_BlockingCollectionEventDispatcher()
        {
            var countdownEvent = new CountdownEvent(0);
            var eventHandlers = Enumerable.Range(0, 100).Select(_ => new RedirectToWhenEventHandler(countdownEvent)).ToList<IEventHandler>();
            var eventDispatcher = new BlockingCollectionEventDispatcher(true);
            Run(eventDispatcher, eventHandlers, countdownEvent);
            Run(eventDispatcher, eventHandlers, countdownEvent);
        }

        [Fact]
        public void RedirectToWhen_BlockingCollectionEventDispatcherAsync()
        {
            var countdownEvent = new CountdownEvent(0);
            var eventHandlers = Enumerable.Range(0, 100).Select(_ => new RedirectToWhenEventHandler(countdownEvent)).ToList<IEventHandler>();
            var eventDispatcher = new BlockingCollectionEventDispatcher(false);
            Run(eventDispatcher, eventHandlers, countdownEvent);
            Run(eventDispatcher, eventHandlers, countdownEvent);
        }

        private void Run(IEventDispatcher eventDispatcher, IList<IEventHandler> eventHandlers, CountdownEvent countdownEvent)
        {
            var events = new List<IEvent>(26001);

            for (int i = 0; i < 1000; i++)
            {
                events.Add(new EventA());
                events.Add(new EventB());
                events.Add(new EventC());
                events.Add(new EventD());
                events.Add(new EventE());
                events.Add(new EventF());
                events.Add(new EventG());
                events.Add(new EventH());
                events.Add(new EventI());
                events.Add(new EventJ());
                events.Add(new EventK());
                events.Add(new EventL());
                events.Add(new EventM());
                events.Add(new EventN());
                events.Add(new EventO());
                events.Add(new EventP());
                events.Add(new EventQ());
                events.Add(new EventR());
                events.Add(new EventS());
                events.Add(new EventT());
                events.Add(new EventU());
                events.Add(new EventV());
                events.Add(new EventW());
                events.Add(new EventX());
                events.Add(new EventY());
                events.Add(new EventZ());
            }

            countdownEvent.Reset(2600000);
            var stopwatch = Stopwatch.StartNew();
            eventDispatcher.Handle(events, eventHandlers);
            countdownEvent.Wait();
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedMilliseconds);
        }
    }

    #region Interfaces

    interface IEventDispatcher
    {
        void Handle(IEnumerable<IEvent> events, IEnumerable<IEventHandler> eventHandlers);
    }

    #endregion

    #region Events

    abstract class EventBase : IEvent
    {
        public ulong UserId { get; set; }
        public ulong Version { get; set; }
        public Type AggregateType { get; private set; }
        public ulong AggregateId { get; set; }
        public DateTime Date { get; set; }
    }

    class EventA : EventBase { }
    class EventB : EventBase { }
    class EventC : EventBase { }
    class EventD : EventBase { }
    class EventE : EventBase { }
    class EventF : EventBase { }
    class EventG : EventBase { }
    class EventH : EventBase { }
    class EventI : EventBase { }
    class EventJ : EventBase { }
    class EventK : EventBase { }
    class EventL : EventBase { }
    class EventM : EventBase { }
    class EventN : EventBase { }
    class EventO : EventBase { }
    class EventP : EventBase { }
    class EventQ : EventBase { }
    class EventR : EventBase { }
    class EventS : EventBase { }
    class EventT : EventBase { }
    class EventU : EventBase { }
    class EventV : EventBase { }
    class EventW : EventBase { }
    class EventX : EventBase { }
    class EventY : EventBase { }
    class EventZ : EventBase { }

    #endregion

    #region RedirectToWhen and ThreadPool.QueueUserWorkItem

    class RedirectToWhenEventHandler : IEventHandler
    {
        private readonly CountdownEvent _countdownEvent;

        public RedirectToWhenEventHandler(CountdownEvent countdownEvent)
        {
            _countdownEvent = countdownEvent;
        }

        public void Handle(IEvent @event)
        {
            RedirectToWhen.InvokeEventHandler(this, @event);
        }

        public void When(EventA e) { _countdownEvent.Signal(); }
        public void When(EventB e) { _countdownEvent.Signal(); }
        public void When(EventC e) { _countdownEvent.Signal(); }
        public void When(EventD e) { _countdownEvent.Signal(); }
        public void When(EventE e) { _countdownEvent.Signal(); }
        public void When(EventF e) { _countdownEvent.Signal(); }
        public void When(EventG e) { _countdownEvent.Signal(); }
        public void When(EventH e) { _countdownEvent.Signal(); }
        public void When(EventI e) { _countdownEvent.Signal(); }
        public void When(EventJ e) { _countdownEvent.Signal(); }
        public void When(EventK e) { _countdownEvent.Signal(); }
        public void When(EventL e) { _countdownEvent.Signal(); }
        public void When(EventM e) { _countdownEvent.Signal(); }
        public void When(EventN e) { _countdownEvent.Signal(); }
        public void When(EventO e) { _countdownEvent.Signal(); }
        public void When(EventP e) { _countdownEvent.Signal(); }
        public void When(EventQ e) { _countdownEvent.Signal(); }
        public void When(EventR e) { _countdownEvent.Signal(); }
        public void When(EventS e) { _countdownEvent.Signal(); }
        public void When(EventT e) { _countdownEvent.Signal(); }
        public void When(EventU e) { _countdownEvent.Signal(); }
        public void When(EventV e) { _countdownEvent.Signal(); }
        public void When(EventW e) { _countdownEvent.Signal(); }
        public void When(EventX e) { _countdownEvent.Signal(); }
        public void When(EventY e) { _countdownEvent.Signal(); }
        public void When(EventZ e) { _countdownEvent.Signal(); }
    }

    class ThreadPoolEventDispatcher : IEventDispatcher
    {
        public void Handle(IEnumerable<IEvent> events, IEnumerable<IEventHandler> eventHandlers)
        {
            var handlers = eventHandlers.ToList();

            foreach (var @event in events)
            {
                foreach (var eventHandler in handlers)
                {
                    var handler = eventHandler;
                    var localEvent = @event;
                    ThreadPool.QueueUserWorkItem(_ => handler.Handle(localEvent));
                }
            }
        }
    }

    class BlockingCollectionEventDispatcher : IEventDispatcher
    {
        private static readonly BlockingCollection<IEvent> _queue = new BlockingCollection<IEvent>();
        private static readonly object _lock = new object();
        private static bool _isRunning;

        private readonly bool _waitUntilQueueIsEmpty;

        public BlockingCollectionEventDispatcher(bool waitUntilQueueIsEmpty)
        {
            _waitUntilQueueIsEmpty = waitUntilQueueIsEmpty;
        }

        public void Handle(IEnumerable<IEvent> events, IEnumerable<IEventHandler> eventHandlers)
        {
            foreach (var @event in events)
            {
                _queue.Add(@event);
            }

            StartThread(eventHandlers, _waitUntilQueueIsEmpty);
        }

        private static void StartThread(IEnumerable<IEventHandler> eventHandlers, bool waitUntilQueueIsEmpty)
        {
            if (!_isRunning)
            {
                lock (_lock)
                {
                    if (!_isRunning)
                    {
                        _isRunning = true;

                        if (waitUntilQueueIsEmpty)
                        {
                            RunThread(eventHandlers);
                        }
                        else
                        {
                            Task.Run(() => RunThread(eventHandlers));
                        }
                    }
                }
            }
        }

        private static void RunThread(IEnumerable<IEventHandler> eventHandlers)
        {
            var handlers = eventHandlers.ToList();

            IEvent @event;
            while (_queue.TryTake(out @event, 200))
            {
                foreach (var eventHandler in handlers.AsParallel())
                {
                    eventHandler.Handle(@event);
                }
            }

            _isRunning = false;
        }
    }

    #endregion
}
