namespace Cms.Aggregates.Pages.Events
{
    public class UserStarredPageEventHandler : EventHandlerBase
    {
        private readonly IUserStarredPageRepository _userStarredPageRepository;

        public UserStarredPageEventHandler(IUserStarredPageRepository userStarredPageRepository)
        {
            _userStarredPageRepository = userStarredPageRepository;
        }

        public void When(PageStarred @event)
        {
            _userStarredPageRepository.Add(@event.UserId, @event.AggregateId);
        }

        public void When(PageUnstarred @event)
        {
            _userStarredPageRepository.Remove(@event.UserId, @event.AggregateId);
        }
    }
}