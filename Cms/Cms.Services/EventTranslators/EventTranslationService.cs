﻿using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Security;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;
using log4net;

namespace Cms.Services.EventTranslators
{
    internal class EventTranslationService : IEventTranslationService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(EventTranslationService));
        private readonly IList<IEventTranslator> _eventTranslators;
        private readonly IGroupRepository _groupRepository;

        public EventTranslationService(IEnumerable<IEventTranslator> eventTranslators, IGroupRepository groupRepository)
        {
            _groupRepository = groupRepository;
            _eventTranslators = eventTranslators.ToList();
        }

        public string GetHeader(IEvent @event)
        {
            var translator = GetTranslator(@event);
            if (translator == null)
            {
                return @event.GetType().Name;
            }
            return translator.GetHeader(@event);
        }

        public string GetContent(IEvent @event)
        {
            var translator = GetTranslator(@event);
            if (translator == null)
            {
                return string.Empty;
            }
            return translator.GetContent(@event);
        }

        public bool CanUserSeeEvent(User user, IEvent @event)
        {
            var translator = GetTranslator(@event);
            if (translator == null)
            {
                return false;
            }

            if (user.Id == @event.UserId || user.Id == @event.AggregateId)
            {
                return true;
            }

            var tenantId = translator.GetAffectedTenantId(@event);
            var moduleId = translator.GetAffectedModuleId();

            return user.HasPermission(moduleId, tenantId, PermissionLevel.CanSee, _groupRepository);
        }

        public bool BelongsToTenant(ulong tenantId, IEvent @event)
        {
            var translator = GetTranslator(@event);
            if (translator == null)
            {
                return false;
            }

            var affectedTenantId = translator.GetAffectedTenantId(@event);
            return affectedTenantId.Equals(0) || affectedTenantId.Equals(tenantId);
        }

        private IEventTranslator GetTranslator(IEvent @event)
        {
            var type = @event.GetType();
            var translator = _eventTranslators.SingleOrDefault(t => t.IsResponsibleFor(type));

            if (translator == null)
            {
                _log.ErrorFormat("No event translator for event type '{0}' found.", @event.GetType().Name);
            }

            return translator;
        }
    }
}
