﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Websites.Queries
{
    internal class WebsiteQueryHandler : QueryHandlerBase
    {
        private readonly IWebsiteRepository _repository;

        public WebsiteQueryHandler(IWebsiteRepository repository)
        {
            _repository = repository;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetAllWebsites),
                typeof (GetWebsiteWithDomains),
            };
        }

        public IEnumerable<object> When(GetAllWebsites query)
        {

            return _repository
                .GetAll(query.TenantId)
                .Select(w => new
                {
                    Id = w.Id.ToJsonString(),
                    Nr = w.PublicId,
                    w.Name,
                });
        }

        public IEnumerable<object> When(GetWebsiteWithDomains query)
        {
            var website = _repository.Get(query.WebsiteId);
            var dto = new
            {
                Id = website.Id.ToJsonString(),
                Nr = website.PublicId,
                website.Name,
                Domains = website.Domains.OrderBy(d => d.Host).Select(d => new
                {
                    d.Host,
                    IsDefault = website.DefaultDomain != null && string.Equals(website.DefaultDomain.Host, d.Host, StringComparison.OrdinalIgnoreCase)
                })
            };
            yield return dto;
        }
    }
}
