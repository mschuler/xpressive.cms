﻿using System;
using Cms.Aggregates.Pages;
using Cms.Services.Contracts;

namespace Cms.Services.PageContentHandlers
{
    internal class ContentCodeHandler : IContentHandler
    {
        public bool CanHandle(string pageContentType)
        {
            return pageContentType.Equals("code", StringComparison.OrdinalIgnoreCase);
        }

        public string ContentType
        {
            get { return "Code"; }
        }

        public string GetImage()
        {
            return "fa fa-code";
        }

        public string Render(PageContentValueBase value)
        {
            var htmlValue = value as PageContentCodeValue;
            if (htmlValue == null)
            {
                return string.Empty;
            }

            return htmlValue.Value;
        }

        public object GetValueForInterpreter(PageContentValueBase value)
        {
            return Render(value);
        }
    }
}