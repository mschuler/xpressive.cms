using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.Files;
using Cms.Aggregates.Files.Commands;
using Cms.Aggregates.Security;
using Cms.EventSourcing.Contracts.Commands;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;
using log4net;
using File = System.IO.File;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class FileUploadController : ApiControllerBase
    {
        private static readonly ConcurrentDictionary<string, Tuple<string, ConcurrentDictionary<int, FileInfo>>> _temporaryFiles = new ConcurrentDictionary<string, Tuple<string, ConcurrentDictionary<int, FileInfo>>>();
        private static readonly ILog _log = LogManager.GetLogger(typeof(FileUploadController));

        private readonly IUniqueIdProvider _uniqueIdProvider;
        private readonly IRepository<FileDirectory> _fileDirectoryRepository;
        private readonly IFileRepository _repository;
        private readonly IContentTypeService _contentTypeService;

        public FileUploadController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IUniqueIdProvider uniqueIdProvider, IRepository<FileDirectory> fileDirectoryRepository, IFileRepository repository, IContentTypeService contentTypeService)
            : base(commandQueue, userRepository, queryHandler)
        {
            _uniqueIdProvider = uniqueIdProvider;
            _fileDirectoryRepository = fileDirectoryRepository;
            _repository = repository;
            _contentTypeService = contentTypeService;
        }

        [HttpPost, Route("api/file/new/{directoryId:ulong}")]
        public async Task<FineUpload> Create(ulong directoryId)
        {
            var tenantId = _fileDirectoryRepository.Get(directoryId).TenantId;
            Func<FileInfo, string, ICommand> createCommand = (file, fileName) => new CreateFile
            {
                AggregateId = _uniqueIdProvider.GetId(),
                FileDirectoryId = directoryId,
                FileVersion = _uniqueIdProvider.GetId(),
                Name = fileName,
                TemporaryFilePath = file.FullName,
                TenantId = tenantId,
            };

            return await ProcessData(createCommand);
        }

        [HttpPost, Route("api/file/replace/{fileId:ulong}")]
        public async Task<FineUpload> Replace(ulong fileId)
        {
            var directoryId = _repository.Get(fileId).FileDirectoryId;
            var tenantId = _fileDirectoryRepository.Get(directoryId).TenantId;
            Func<FileInfo, string, ICommand> createCommand = (file, fileName) => new CreateFileVersion
            {
                AggregateId = fileId,
                FileVersion = _uniqueIdProvider.GetId(),
                TemporaryFilePath = file.FullName,
                TenantId = tenantId,
            };

            return await ProcessData(createCommand);
        }

        [HttpPost, Route("api/file/replace/{fileId:ulong}/content")]
        public async Task<string> Replace(ulong fileId, [FromBody]FileContentDto content)
        {
            if (content == null)
            {
                throw new ExceptionWithReason("invalid");
            }

            var file = _repository.Get(fileId);
            var tenantId = _fileDirectoryRepository.Get(file.FileDirectoryId).TenantId;
            var isTextFile = _contentTypeService.IsTextFile(file.GetExtension());

            if (!isTextFile)
            {
                throw new ExceptionWithReason("This is not a text file.");
            }

            var tempFile = new FileInfo(Path.GetTempFileName());

            using (var stream = tempFile.OpenWrite())
            {
                using (var writer = new StreamWriter(stream, Encoding.UTF8))
                {
                    await writer.WriteAsync(content.Content);
                }
            }

            var command = new CreateFileVersion
            {
                AggregateId = fileId,
                FileVersion = _uniqueIdProvider.GetId(),
                TemporaryFilePath = tempFile.FullName,
                TenantId = tenantId,
            };

            SendCommand(command);

            return command.FileVersion.ToJsonString();
        }

        private async Task<FineUpload> ProcessData(Func<FileInfo, string, ICommand> createCommand)
        {
            var fineUpload = new FineUpload();
            FileInfo tempFile = null;

            try
            {
                var provider = new MultipartFileStreamProvider(Path.GetTempPath());
                await Request.Content.ReadAsMultipartAsync(provider);

                var parts = provider.FileData.ToList();
                var uuid = ReadFile(parts, "\"qquuid\"");
                var filename = ReadFile(parts, "\"qqfilename\"");
                var totalparts = int.Parse(ReadFile(parts, "\"qqtotalparts\"") ?? "1");
                var partindex = int.Parse(ReadFile(parts, "\"qqpartindex\"") ?? "0");
                var partoffset = long.Parse(ReadFile(parts, "\"qqpartbyteoffset\"") ?? "0");
                var data = ReadFileData(parts, "\"qqfile\"");

                var tuple = _temporaryFiles.GetOrAdd(uuid, _ =>
                    Tuple.Create(filename, new ConcurrentDictionary<int, FileInfo>()));

                tempFile = tuple.Item2.GetOrAdd(partindex, _ => new FileInfo(Path.GetTempFileName()));

                using (var stream = tempFile.OpenWrite())
                {
                    await stream.WriteAsync(data, 0, data.Length);
                }

                if (partindex + 1 == totalparts)
                {
                    FinalizeFileUpload(uuid, filename, tuple.Item2, createCommand);
                }

                parts.ForEach(p => File.Delete(p.LocalFileName));

                fineUpload.Success = true;
            }
            catch (Exception e)
            {
                fineUpload.Success = false;
                fineUpload.Error = e.Message;
                _log.Error("Error when uploading file.", e);

                if (tempFile != null && tempFile.Exists)
                {
                    try
                    {
                        tempFile.Delete();
                    }
                    catch { }
                }
            }

            return fineUpload;
        }

        private void FinalizeFileUpload(string uuid, string fileName, ConcurrentDictionary<int, FileInfo> files, Func<FileInfo, string, ICommand> createCommand)
        {
            var tempFile = new FileInfo(Path.GetTempFileName());

            using (var stream = tempFile.OpenWrite())
            {
                var previousIndex = 0;
                foreach (var pair in files.ToList().OrderBy(p => p.Key))
                {
                    Debug.Assert(previousIndex == 0 || pair.Key == previousIndex + 1);

                    var buffer = File.ReadAllBytes(pair.Value.FullName);
                    stream.Write(buffer, 0, buffer.Length);
                    pair.Value.Delete();
                    previousIndex = pair.Key;
                }
            }

            var command = createCommand(tempFile, fileName);
            SendCommand(command);
            Tuple<string, ConcurrentDictionary<int, FileInfo>> i;
            _temporaryFiles.TryRemove(uuid, out i);
        }

        private string ReadFile(IEnumerable<MultipartFileData> parts, string name)
        {
            var part = GetPart(parts, name);

            if (part != null)
            {
                if (File.Exists(part.LocalFileName))
                {
                    return File.ReadAllText(part.LocalFileName, Encoding.UTF8);
                }
            }

            return null;
        }

        private byte[] ReadFileData(IEnumerable<MultipartFileData> parts, string name)
        {
            var part = GetPart(parts, name);

            if (part != null)
            {
                if (File.Exists(part.LocalFileName))
                {
                    return File.ReadAllBytes(part.LocalFileName);
                }
            }

            return null;
        }

        private MultipartFileData GetPart(IEnumerable<MultipartFileData> parts, string name)
        {
            return parts.SingleOrDefault(
                p => p.Headers.ContentDisposition.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
        }

        public class FineUpload
        {
            public bool Success { get; set; }
            public string Error { get; set; }
            public bool PreventRetry { get; set; }
            public string FileName { get; set; }
        }

        public class FileContentDto
        {
            public string Content { get; set; }
        }
    }
}