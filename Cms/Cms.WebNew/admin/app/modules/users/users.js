﻿(function() {
    angular.module("app").controller("UsersController", ["$rootScope", "$scope", "$http", "$log", "$modal", "$location", "dialogService", "toastService", "tenantService", "$translate", "currentUser", function ($rootScope, $scope, $http, $log, $modal, $location, dialogService, toastService, tenantService, $translate, currentUser) {
        function loadUsers() {
            $http.get("/api/users/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
                $scope.users = data;
            });
        };

        $scope.currentUser = currentUser;

        $http.get("/api/permission/user/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        function showUserModalDialog(subject, user, onAccept) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/user/editUserDialog.min.html",
                controller: "EditUserController",
                resolve: {
                    user: function () { return user; },
                    subject: function () { return subject; },
                    currentUser: function() { return currentUser; }
                }
            });

            modalInstance.result.then(onAccept);
        };

        $scope.select = function(user) {
            $scope.selectedUser = user;
        };

        var createUser = "";
        var errorCreateUser = "";
        var editUser = "";
        var errorEditUser = "";
        var deleteUser = "";
        var confirmDeleteUser = "";
        var errorDeleteUser = "";
        var errorLockUser = "";
        var errorUnlockUser = "";
        var errorMakeSysadmin = "";
        var errorMakeNotSysadmin = "";
        var errorRemove2Fa = "";

        $translate(["create_user", "error_create_user", "edit_user", "error_edit_user", "delete_user", "confirm_delete_user", "error_delete_user", "error_lock_user", "error_unlock_user", "error_make_sysadmin", "error_make_not_sysadmin", "error_remove_2fa"]).then(function (t) {
            createUser = t.create_user;
            errorCreateUser = t.error_create_user;
            editUser = t.edit_user;
            errorEditUser = t.error_edit_user;
            deleteUser = t.delete_user;
            confirmDeleteUser = t.confirm_delete_user;
            errorDeleteUser = t.error_delete_user;
            errorLockUser = t.error_lock_user;
            errorUnlockUser = t.error_unlock_user;
            errorMakeSysadmin = t.error_make_sysadmin;
            errorMakeNotSysadmin = t.error_make_not_sysadmin;
            errorRemove2Fa = t.error_remove_2fa;
        });

        $scope.create = function() {
            showUserModalDialog(createUser, {}, function (u) {
                if (u.password && u.password.length > 0) {
                    var parsed = CryptoJS.enc.Utf8.parse(u.password);
                    u.password = CryptoJS.enc.Base64.stringify(parsed);
                }
                $http.post("/api/users/" + tenantService.getTenant().id, u).success(function(result) {
                    $scope.users.push(result);
                }).error(function(error) {
                    toastService.error(errorCreateUser, error);
                });
            });
        };

        $scope.show = function(user) {
            $location.path("/user/user/" + user.id);
        };

        $scope.edit = function (user) {
            var copy = angular.copy(user);
            showUserModalDialog(editUser, copy, function (u) {
                if (u.password && u.password.length > 0) {
                    var parsed = CryptoJS.enc.Utf8.parse(u.password);
                    u.password = CryptoJS.enc.Base64.stringify(parsed);
                }
                var url = String.format("/api/users/{0}/user/{1}", tenantService.getTenant().id, copy.id);
                $http.post(url, u).success(function() {
                    user.firstName = u.firstName;
                    user.lastName = u.lastName;
                    user.email = u.email;
                }).error(function (error) {
                    toastService.error(errorEditUser, error);
                });
            });
        };

        $scope.delete = function (user) {
            dialogService.confirmDialog(deleteUser, String.format(confirmDeleteUser, user.name), function () {
                var url = String.format("/api/users/{0}/user/{1}", tenantService.getTenant().id, user.id);
                $http.delete(url).success(function() {
                    $scope.users = _.reject($scope.users, function(u) { return u.id === user.id; });
                }).error(function(e) {
                    toastService.error(errorDeleteUser, e);
                });
            });
        };

        $scope.makeSysadmin = function (user) {
            var url = String.format("/api/users/{0}/user/{1}/sysadmin", tenantService.getTenant().id, user.id);
            return $http.post(url).success(function () {
                user.isSysadmin = true;
            }).error(function (error) {
                toastService.error(errorMakeSysadmin, error);
            });
        };

        $scope.makeNotSysadmin = function(user) {
            var url = String.format("/api/users/{0}/user/{1}/notsysadmin", tenantService.getTenant().id, user.id);
            return $http.post(url).success(function() {
                user.isSysadmin = false;
            }).error(function(error) {
                toastService.error(errorMakeNotSysadmin, error);
            });
        };

        $scope.lock = function (user) {
            var url = String.format("/api/users/{0}/user/{1}/lock", tenantService.getTenant().id, user.id);
            return $http.post(url).success(function() {
                user.isLocked = true;
            }).error(function(error) {
                toastService.error(errorLockUser, error);
            });
        };

        $scope.unlock = function (user) {
            var url = String.format("/api/users/{0}/user/{1}/unlock", tenantService.getTenant().id, user.id);
            return $http.post(url).success(function () {
                user.isLocked = false;
            }).error(function (error) {
                toastService.error(errorUnlockUser, error);
            });
        };

        $scope.disable2fa = function(user) {
            var url = String.format("/api/2fa/totp/{0}", user.id);
            return $http.delete(url).success(function() {
                user.isTotp = false;
            }).error(function(error) {
                toastService.error(errorRemove2Fa, error);
            });
        };

        $rootScope.$on("tenantChanged", loadUsers);

        $rootScope.$on('logout', function () { $scope.users = []; });

        loadUsers();
    }]);

    angular.module("app").controller("UserController", ["$rootScope", "$scope", "$routeParams", "$http", "tenantService", function ($rootScope, $scope, $routeParams, $http, tenantService) {
        var userId = $routeParams.userId;
        var tenantId = tenantService.getTenant().id;

        $scope.events = [];
        $scope.isBusy = false;
        var isFinish = false;

        var url = String.format("/api/users/{0}/user/{1}", tenantId, userId);
        $http.get(url, { cache: false }).success(function(data) {
            $scope.user = data;
        });

        $scope.nextPage = function () {
            if ($scope.isBusy || isFinish) {
                return;
            }
            $scope.isBusy = true;

            var count = $scope.events.length;
            var url = String.format("/api/events/{1}/user/{2}?$skip={0}&$top=20&$orderby=Id desc", count, tenantId, userId);
            $http.get(url, { cache: false }).success(function (data) {
                if (data.length == 0) {
                    isFinish = true;
                }

                for (var i = 0; i < data.length; i++) {
                    $scope.events.push(data[i]);
                }
                $scope.isBusy = false;
            });
        };
    }]);

    angular.module("app").controller("EditUserController", ["$scope", "$modalInstance", "$log", "$translate", "currentUser", "subject", "user", function ($scope, $modalInstance, $log, $translate, currentUser, subject, user) {
        $scope.user = user;
        $scope.subject = subject;
        $scope.currentUser = currentUser;
        $scope.isEditing = user.id ? true : false;

        var ridiculous = "";
        var veryWeak = "";
        var weak = "";
        var good = "";
        var strong = "";
        var veryStrong = "";

        $translate(["ridiculous", "very_weak", "weak", "good", "strong", "very_strong"]).then(function (t) {
            ridiculous = t.ridiculous;
            veryWeak = t.very_weak;
            weak = t.weak;
            good = t.good;
            strong = t.strong;
            veryStrong = t.very_strong;
        });

        $scope.$watch("user.password", function(newValue) {
            var score = new Score(newValue);
            var entropy = score.calculateEntropyScore() * 1.25;
            entropy = entropy > 100 ? 100 : (entropy < 0 ? 0 : entropy);

            $scope.pwStrength = entropy;
            $scope.pwStrengthColor = "danger";

            if (entropy < 25) {
                $scope.pwStrengthColor = "danger";
            } else if (entropy < 50) {
                $scope.pwStrengthColor = "warning";
            } else {
                $scope.pwStrengthColor = "success";
            }

            if (entropy < 10) {
                $scope.pwStrengthDisplay = ridiculous;
            } else if (entropy < 20) {
                $scope.pwStrengthDisplay = veryWeak;
            } else if (entropy < 30) {
                $scope.pwStrengthDisplay = weak;
            } else if (entropy < 40) {
                $scope.pwStrengthDisplay = good;
            } else if (entropy < 50) {
                $scope.pwStrengthDisplay = strong;
            } else {
                $scope.pwStrengthDisplay = veryStrong;
            }
        });

        $scope.showOrHidePassword = function () {
            if (document.getElementById("password").type == "password") {
                document.getElementById("password").type = "text";
            } else {
                document.getElementById("password").type = "password";
            }
        };

        $scope.generatePassword = function () {
            document.getElementById("password").type = "text";
            $scope.user.password = generatePassword(12, false);
        };

        $scope.ok = function () {
            $modalInstance.close($scope.user);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());