namespace Cms.Aggregates.Security.Events
{
    public class UserDeleted : EventBase<User>
    {
        public string FullName { get; set; }
    }
}