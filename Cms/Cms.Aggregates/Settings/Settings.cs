namespace Cms.Aggregates.Settings
{
    public class Settings : AggregateBase
    {
        private readonly ulong _tenantId;

        public Settings(ulong tenantId)
        {
            _tenantId = tenantId;
            EmailSettings = new EmailSettings();
            UrlShorteningSettings = new UrlShorteningSettings();
        }

        public ulong TenantId { get { return _tenantId; } }

        public EmailSettings EmailSettings { get; internal set; }

        public UrlShorteningSettings UrlShorteningSettings { get; internal set; }
    }
}