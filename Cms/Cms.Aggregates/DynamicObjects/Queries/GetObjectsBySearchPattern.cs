﻿using System;
using System.Linq;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Queries
{
    [RequiredPermission("object", PermissionLevel.CanSee)]
    public class GetObjectsBySearchPattern : QueryBase
    {
        public string SearchPattern { get; set; }

        internal bool IsMatch(DynamicObject obj, DynamicObjectDefinition definition)
        {
            var parts = SearchPattern.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return parts.All(p =>
                obj.Title.ToUpperInvariant().Contains(p.ToUpperInvariant()) ||
                definition.Name.ToUpperInvariant().Contains(p.ToUpperInvariant()) ||
                obj.PublicId.ToString("D").Contains(p));
        }
    }
}