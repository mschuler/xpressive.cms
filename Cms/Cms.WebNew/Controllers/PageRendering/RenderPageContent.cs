using Cms.Services.Contracts;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class RenderPageContent : IFilter<RenderPageMessage>
    {
        private readonly IPageRenderingService _pageRenderingService;

        public RenderPageContent(IPageRenderingService pageRenderingService)
        {
            _pageRenderingService = pageRenderingService;
        }

        public bool Execute(RenderPageMessage msg)
        {
            msg.Content = _pageRenderingService.Render(msg.Page);
            return true;
        }
    }
}