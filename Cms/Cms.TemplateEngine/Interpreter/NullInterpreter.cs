using System;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    public sealed class NullInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "null"; } }

        public void Calculate(TreeItem treeItem)
        {
            if (treeItem.ItemType != TreeItemType.Function ||
                !treeItem.Value.Equals("isnull", StringComparison.OrdinalIgnoreCase) ||
                treeItem.Children == null ||
                treeItem.Children.Count != 1)
            {
                return;
            }

            var child = treeItem.Children[0];
            var value = child.Result;

            if (value == null)
            {
                treeItem.Result = true.ToString();
                return;
            }

            if (child.Result is string)
            {
                treeItem.Result = string.IsNullOrEmpty((string)child.Result).ToString();
                return;
            }

            if (child.Result is int)
            {
                treeItem.Result = (((int)child.Result) == 0).ToString();
                return;
            }

            treeItem.Result = false.ToString();
        }
    }
}