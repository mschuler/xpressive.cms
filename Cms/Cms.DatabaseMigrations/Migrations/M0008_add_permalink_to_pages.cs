using System;
using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0008_add_permalink_to_pages : DatabaseMigrationBase
    {
        public M0008_add_permalink_to_pages() : base(8) { }

        public override void Execute(Database database)
        {
            var events = database.Query<EventDto>().Where(e => e.Event == "PageCreated").ToList();

            foreach (var dto in events)
            {
                var json = dto.Data.Replace(
                    "\"Permalink\":\"00000000-0000-0000-0000-000000000000\"",
                    "\"Permalink\":\"" + Guid.NewGuid().ToString("D") + "\"");

                if (!json.Contains("Permalink"))
                {
                    json = json.Replace("\"UserId", "\"Permalink\":\"" + Guid.NewGuid().ToString("D") + "\",\"UserId");
                }

                dto.Data = json;
                database.Update(dto);
            }
        }
    }
}