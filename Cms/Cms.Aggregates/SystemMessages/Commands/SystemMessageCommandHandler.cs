﻿using System;
using Cms.Aggregates.SystemMessages.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.SharedKernel;

namespace Cms.Aggregates.SystemMessages.Commands
{
    internal class SystemMessageCommandHandler : CommandHandlerBase
    {
        private readonly ISystemMessageRepository _repository;

        public SystemMessageCommandHandler(ISystemMessageRepository repository)
        {
            _repository = repository;
        }

        public void When(CreateSystemMessage command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            eventHandler(new SystemMessageCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Title = command.Title.Trim().RemoveControlChars(),
                Message = command.Message.Trim().RemoveControlChars(),
                Validity = command.Validity,
            });
        }

        public void When(ChangeSystemMessage command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            eventHandler(new SystemMessageChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Title = command.Title.RemoveControlChars(),
                Message = command.Message.RemoveControlChars(),
                Validity = command.Validity,
            });
        }

        public void When(DeleteSystemMessage command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            eventHandler(new SystemMessageDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            });
        }
    }
}