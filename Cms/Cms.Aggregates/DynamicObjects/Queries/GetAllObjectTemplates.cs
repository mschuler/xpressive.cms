using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Queries
{
    [RequiredPermission("objecttemplate", PermissionLevel.CanSee)]
    public class GetAllObjectTemplates : QueryBase
    {
        public ulong ObjectDefinitionId { get; set; }
    }
}