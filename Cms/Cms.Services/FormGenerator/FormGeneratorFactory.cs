﻿using Cms.Aggregates.Pages;
using Cms.Services.Contracts;
using Cms.SharedKernel;

namespace Cms.Services.FormGenerator
{
    public class FormGeneratorFactory : IFormGeneratorFactory
    {
        private readonly ILocalizationService _localizationService;
        private readonly ICurrentRequestService _currentRequestService;

        public FormGeneratorFactory(ILocalizationService localizationService, ICurrentRequestService currentRequestService)
        {
            _localizationService = localizationService;
            _currentRequestService = currentRequestService;
        }

        public IFormGenerator Create( ulong formId)
        {
            return new FormGenerator(_localizationService, _currentRequestService, formId);
        }
    }
}