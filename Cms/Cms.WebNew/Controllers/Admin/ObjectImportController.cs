using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Commands;
using Cms.Aggregates.Security;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using CsvHelper;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class ObjectImportController : CsvImportControllerBase
    {
        private readonly IUniqueIdProvider _idProvider;
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;
        private readonly IDynamicObjectRepository _objectRepository;
        private readonly IObjectFieldContentTypeConverterService _contentTypeConverter;

        public ObjectImportController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IUniqueIdProvider idProvider, IUtf8Checker utf8Checker, IDynamicObjectDefinitionRepository definitionRepository, IDynamicObjectRepository objectRepository, IObjectFieldContentTypeConverterService contentTypeConverter, ITemporaryFileStoreService temporaryFileStoreService)
            : base(commandQueue, userRepository, queryHandler, utf8Checker, temporaryFileStoreService)
        {
            _idProvider = idProvider;
            _definitionRepository = definitionRepository;
            _objectRepository = objectRepository;
            _contentTypeConverter = contentTypeConverter;
        }

        [HttpGet, Route("api/objects/import/start")]
        public override object StartImport()
        {
            return base.StartImport();
        }

        [HttpGet, Route("api/objects/import/encodings")]
        public override IEnumerable<object> GetEncodings()
        {
            return base.GetEncodings();
        }

        [HttpGet, Route("api/objects/import/{id:ulong}/encoding")]
        public override string GetEncoding(ulong id)
        {
            return base.GetEncoding(id);
        }

        [HttpPost, Route("api/objects/import/{id:ulong}/file")]
        public override Task UploadFile(ulong id)
        {
            return base.UploadFile(id);
        }

        [HttpPost, Route("api/objects/import/{id:ulong}/columns")]
        public IEnumerable<object> GetColumns(ulong id, [FromBody]CsvSettingsDto settings)
        {
            return base.GetColumns(id, settings.Delimiter, settings.HasHeaderRecord, settings.GetEncoding());
        }

        [HttpPost, Route("api/objects/{definitionId:ulong}/import/{id:ulong}/preview")]
        public IEnumerable<object> GetPreview(ulong definitionId, ulong id, [FromBody]CsvSettingsDto settings)
        {
            FileInfo file;
            if (!TryGetTemporaryFile(id, out file) || !file.Exists)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound).WithGuid();
            }

            if (settings == null || settings.Mapping == null)
            {
                yield break;
            }

            var definition = _definitionRepository.Get(definitionId);

            using (var textReader = new StreamReader(file.FullName, settings.GetEncoding()))
            {
                using (var csvReader = new CsvReader(textReader))
                {
                    csvReader.Configuration.Delimiter = settings.Delimiter;
                    csvReader.Configuration.HasHeaderRecord = settings.HasHeaderRecord;

                    while (csvReader.Read())
                    {
                        var cells = new List<object>();
                        var isValid = true;
                        var invalidFields = new List<string>();

                        foreach (var definitionField in definition.Fields.OrderBy(f => f.SortOrder))
                        {
                            var mapping = settings.Mapping.SingleOrDefault(m => m.ObjectField.Equals(definitionField.Name, StringComparison.Ordinal));
                            if (mapping == null || string.IsNullOrEmpty(mapping.Column))
                            {
                                continue;
                            }

                            var value = GetField(csvReader, mapping.Column, settings.HasHeaderRecord) ?? string.Empty;
                            var isValueValid = _contentTypeConverter.IsValidValueForImport(definitionField.ContentType, value, mapping.Format, definitionField.ContentTypeOptions);

                            isValid &= string.IsNullOrEmpty(value) || isValueValid;

                            if (isValueValid)
                            {
                                value = _contentTypeConverter.SerializeForImportPreview(definitionField.ContentType, value, mapping.Format, definitionField.ContentTypeOptions);
                            }
                            else if (!string.IsNullOrEmpty(value))
                            {
                                invalidFields.Add(definitionField.Name);
                            }

                            if (definitionField.SortOrder == 0 && string.IsNullOrEmpty(value))
                            {
                                isValid = false;
                            }

                            cells.Add(new
                            {
                                Header = definitionField.Name,
                                Value = value,
                            });
                        }

                        yield return new
                        {
                            Cells = cells,
                            IsValid = isValid,
                            Error = invalidFields.Any() ? "Invalid fields: " + string.Join(", ", invalidFields) : string.Empty
                        };
                    }
                }
            }
        }

        [HttpPost, Route("api/objects/{tenantId:ulong}/{definitionId:ulong}/import/{id:ulong}/import")]
        public void Import(ulong tenantId, ulong definitionId, ulong id, [FromBody]CsvSettingsDto settings)
        {
            FileInfo file;
            if (!TryGetTemporaryFile(id, out file) || !file.Exists)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound).WithGuid();
            }

            if (settings == null || settings.Mapping == null)
            {
                return;
            }

            var definition = _definitionRepository.Get(definitionId);

            using (var textReader = new StreamReader(file.FullName, settings.GetEncoding()))
            {
                using (var csvReader = new CsvReader(textReader))
                {
                    csvReader.Configuration.Delimiter = settings.Delimiter;
                    csvReader.Configuration.HasHeaderRecord = settings.HasHeaderRecord;

                    while (csvReader.Read())
                    {
                        var isValid = true;
                        var values = new Dictionary<string, string>();

                        foreach (var definitionField in definition.Fields.OrderBy(f => f.SortOrder))
                        {
                            var mapping = settings.Mapping.SingleOrDefault(m => m.ObjectField.Equals(definitionField.Name, StringComparison.Ordinal));
                            var value = string.Empty;
                            var isValueValid = false;

                            if (mapping != null && !string.IsNullOrEmpty(mapping.Column))
                            {
                                value = GetField(csvReader, mapping.Column, settings.HasHeaderRecord) ?? string.Empty;
                                isValueValid = _contentTypeConverter.IsValidValueForImport(definitionField.ContentType, value, mapping.Format, definitionField.ContentTypeOptions);
                            }

                            isValid &= string.IsNullOrEmpty(value) || isValueValid;
                            value = _contentTypeConverter.SerializeForImport(definitionField.ContentType, value, mapping.Format, definitionField.ContentTypeOptions);
                            values.Add(definitionField.Name, value);

                            if (definitionField.SortOrder == 0 && string.IsNullOrEmpty(value))
                            {
                                isValid = false;
                            }
                        }

                        if (isValid)
                        {
                            SendCommand(new CreateDynamicObject
                            {
                                AggregateId = _idProvider.GetId(),
                                DefinitionId = definitionId,
                                PublicId = _objectRepository.GetNextPublicId(),
                                TenantId = tenantId,
                                Values = values,
                            });
                        }
                    }
                }
            }
        }

        public class CsvSettingsDto
        {
            private string _delimiter;

            public string Delimiter
            {
                get { return _delimiter; }
                set
                {
                    _delimiter = value;
                    if (value == "\\t")
                    {
                        _delimiter = "\t";
                    }
                }
            }

            public bool HasHeaderRecord { get; set; }

            public string Encoding { get; set; }

            public List<CsvMappingEntryDto> Mapping { get; set; }
        }

        public class CsvMappingEntryDto
        {
            public string Column { get; set; }
            public string ObjectField { get; set; }
            public string Format { get; set; }
        }
    }
}