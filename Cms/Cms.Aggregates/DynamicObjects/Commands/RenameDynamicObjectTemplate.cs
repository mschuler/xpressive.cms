﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("objecttemplate", PermissionLevel.CanChange)]
    public class RenameDynamicObjectTemplate : CommandBase
    {
        [ValidId]
        public ulong TemplateId { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }
    }
}