using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.Forms.Events;
using Cms.Aggregates.Pages;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.Forms.Commands
{
    internal class FormCommandHandler : CommandHandlerBase
    {
        private static readonly HashSet<string> _invalidFieldNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "id", "timestamp", "ipaddress", "isspam", "values"
        };

        private readonly IFormRepository _repository;
        private readonly IPageRepository _pageRepository;
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;
        private readonly IUniqueIdProvider _idProvider;

        public FormCommandHandler(IFormRepository repository, IPageRepository pageRepository, IDynamicObjectDefinitionRepository definitionRepository, IUniqueIdProvider idProvider)
        {
            _repository = repository;
            _pageRepository = pageRepository;
            _definitionRepository = definitionRepository;
            _idProvider = idProvider;
        }

        public void When(CreateNewForm command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            var e = new NewFormCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = command.Name.Trim().RemoveControlChars(),
                TenantId = command.TenantId,
                PublicId = command.PublicId,
            };

            eventHandler(e);
        }

        public void When(DuplicateForm command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);
            var existing = _repository.Get(command.FormIdToDuplicate);
            var fieldIdMapping = new Dictionary<ulong, ulong>();

            eventHandler(new NewFormCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = command.Name.Trim().RemoveControlChars(),
                TenantId = command.TenantId,
                PublicId = command.PublicId
            });

            foreach (var field in existing.Fields)
            {
                var id = _idProvider.GetId();
                fieldIdMapping.Add(field.Id, id);
                eventHandler(new FormFieldAdded
                {
                    AggregateId = command.AggregateId,
                    UserId = command.UserId,
                    Name = field.Name,
                    Id = id
                });
            }

            foreach (var action in existing.Actions)
            {
                var configuration = action.Configuration.Clone();
                var sendEmailToFieldFormAction = configuration as SendEmailToFieldFormAction;

                if (sendEmailToFieldFormAction != null)
                {
                    ulong oldFieldId;
                    ulong newFieldId;
                    if (ulong.TryParse(sendEmailToFieldFormAction.FieldId, out oldFieldId) &&
                        fieldIdMapping.TryGetValue(oldFieldId, out newFieldId))
                    {
                        sendEmailToFieldFormAction.FieldId = newFieldId.ToString("D");
                    }
                }

                eventHandler(new FormActionAdded
                {
                    AggregateId = command.AggregateId,
                    UserId = command.UserId,
                    Id = _idProvider.GetId(),
                    Type = action.Type,
                    Configuration = configuration,
                });
            }

            var generatorFields = new List<GeneratorField>();
            foreach (var field in existing.GeneratorFields)
            {
                ulong newFieldId;
                if (fieldIdMapping.TryGetValue(field.FieldId, out newFieldId))
                {
                    generatorFields.Add(new GeneratorField
                    {
                        FieldId = newFieldId,
                        SortOrder = field.SortOrder,
                        Type = field.Type,
                        IsRequired = field.IsRequired,
                        IsMultiselect = field.IsMultiselect,
                        Options = field.Options.ToList(),
                    });
                }
            }

            eventHandler(new FormGeneratorFieldsChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                LabelPosition = existing.GeneratorLabelPosition,
                Fields = generatorFields,
            });

            eventHandler(new FormTemplateChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Template = existing.Template.Replace(existing.Id.ToString("D"), command.AggregateId.ToString("D")),
            });
        }

        public void When(ClearFormEntries command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            var e = new FormEntriesCleared
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            };

            eventHandler(e);
        }

        public void When(ChangeFormName command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            var e = new FormNameChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = command.Name.Trim().RemoveControlChars(),
            };

            eventHandler(e);
        }

        public void When(ChangeFormTemplate command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            var e = new FormTemplateChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Template = command.Template.Trim().RemoveControlChars(),
            };

            eventHandler(e);
        }

        public void When(AddFormField command, Action<IEvent> eventHandler)
        {
            var form = _repository.Get(command.AggregateId);
            var name = command.Name.Trim().RemoveControlChars();

            if (form.Fields.Any(f => f.Name.Equals(name, StringComparison.OrdinalIgnoreCase)))
            {
                throw new ExceptionWithReason("There is already a field with this name.");
            }

            if (form.Fields.Any(f => f.Id == command.Id))
            {
                throw new ExceptionWithReason("There is already a field with this id.");
            }

            if (_invalidFieldNames.Contains(name))
            {
                throw new ExceptionWithReason("This field name is invalid.");
            }

            var e = new FormFieldAdded
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Id = command.Id,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(RemoveFormField command, Action<IEvent> eventHandler)
        {
            var form = _repository.Get(command.AggregateId);
            var field = form.Fields.SingleOrDefault(f => f.Id == command.Id);

            if (field == null)
            {
                throw new ExceptionWithReason("There is no form field with this id.");
            }

            var e = new FormFieldRemoved
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Id = command.Id,
                Name = field.Name
            };

            eventHandler(e);
        }

        public void When(ChangeFormField command, Action<IEvent> eventHandler)
        {
            var form = _repository.Get(command.AggregateId);
            var name = command.Name.Trim().RemoveControlChars();

            if (form.Fields.Any(f => f.Id != command.Id && f.Name.Equals(name, StringComparison.OrdinalIgnoreCase)))
            {
                throw new ExceptionWithReason("There is already a field with this name.");
            }

            if (form.Fields.All(a => a.Id != command.Id))
            {
                throw new ExceptionWithReason("There is no form field with this id.");
            }

            if (_invalidFieldNames.Contains(name))
            {
                throw new ExceptionWithReason("This field name is invalid.");
            }

            var e = new FormFieldChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Id = command.Id,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(DeleteForm command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            var e = new FormDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            };

            eventHandler(e);
        }

        public void When(CreateFormEntry command, Action<IEvent> eventHandler)
        {
            var form = _repository.Get(command.AggregateId);

            if (command.RunFormActions)
            {
                if (form.Actions.All(a => a.Type != FormActionType.Persist))
                {
                    // if there is no persist action, then don't create event.
                    return;
                }
            }

            var fields = form.Fields.Select(f => f.Name).ToList();

            var values = command.Values
                .Where(p => fields.Contains(p.Key, StringComparer.OrdinalIgnoreCase))
                .ToDictionary(k => k.Key, k => k.Value.RemoveControlChars());

            var e = new FormEntryCreated
            {
                AggregateId = command.AggregateId,
                FormEntryId = command.FormEntryId,
                UserId = command.UserId,
                Timestamp = DateTime.UtcNow,
                IpAddress = command.IpAddress,
                IsSpam = command.IsSpam,
                Values = values,
            };

            eventHandler(e);
        }

        public void When(ChangeFormEntry command, Action<IEvent> eventHandler)
        {
            var form = _repository.Get(command.AggregateId);
            var entry = form.Entries.SingleOrDefault(e => e.Id == command.FormEntryId);
            var values = new Dictionary<string, string>();

            if (entry == null)
            {
                throw new ExceptionWithReason("There is no such form entry.");
            }

            foreach (var key in command.Values.Keys)
            {
                if (entry.Values.ContainsKey(key))
                {
                    continue;
                }
                if (form.Fields.Any(f => f.Name.Equals(key, StringComparison.OrdinalIgnoreCase)))
                {
                    continue;
                }

                throw new ExceptionWithReason("There is no such form field.");
            }

            foreach (var pair in command.Values)
            {
                values[pair.Key] = pair.Value.RemoveControlChars();
            }

            eventHandler(new FormEntryChanged
            {
                AggregateId = command.AggregateId,
                FormEntryId = command.FormEntryId,
                UserId = command.UserId,
                Values = values,
            });
        }

        public void When(DeleteFormEntry command, Action<IEvent> eventHandler)
        {
            var form = _repository.Get(command.AggregateId);

            if (form.Entries.All(a => a.Id != command.FormEntryId))
            {
                throw new ExceptionWithReason("There is no such form entry.");
            }

            var e = new FormEntryDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                FormEntryId = command.FormEntryId,
            };

            eventHandler(e);
        }

        public void When(AddFormAction command, Action<IEvent> eventHandler)
        {
            var form = _repository.Get(command.AggregateId);

            ValidateFormAction(form, command.Type, command.Configuration);

            eventHandler(new FormActionAdded
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Id = command.Id,
                Type = command.Type,
                Configuration = command.Configuration,
            });
        }

        public void When(ChangeFormAction command, Action<IEvent> eventHandler)
        {
            var form = _repository.Get(command.AggregateId);

            if (form.Actions.All(a => a.Id != command.Id))
            {
                throw new ExceptionWithReason("There is no form action with this id.");
            }

            ValidateFormAction(form, command.Type, command.Configuration);

            eventHandler(new FormActionChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Id = command.Id,
                Type = command.Type,
                Configuration = command.Configuration,
            });
        }

        public void When(RemoveFormAction command, Action<IEvent> eventHandler)
        {
            var form = _repository.Get(command.AggregateId);

            if (form.Actions.All(a => a.Id != command.Id))
            {
                throw new ExceptionWithReason("There is no form action with this id.");
            }

            eventHandler(new FormActionRemoved
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Id = command.Id,
            });
        }

        public void When(ChangeFormGeneratorFields command, Action<IEvent> eventHandler)
        {
            var form = _repository.Get(command.AggregateId);

            if (command.Fields.Any(g => form.Fields.All(f => f.Id != g.FieldId)))
            {
                throw new ExceptionWithReason("The form doesn't have a form field with this id.");
            }

            eventHandler(new FormGeneratorFieldsChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                LabelPosition = command.LabelPosition,
                Fields = command.Fields
            });
        }

        private void ValidateFormAction(Form form, FormActionType type, FormActionConfiguration configuration)
        {
            if (type == FormActionType.PageForwarding)
            {
                ValidatePageForwardingFormAction(form, (PageForwardingFormAction)configuration);
            }
            else if (type == FormActionType.CreateObject)
            {
                ValidateCreateObjectFormAction((CreateObjectFormAction)configuration);
            }
            else if (type == FormActionType.SendEmailToAddress)
            {
                ValidateSendEmailToAddressFormAction((SendEmailToAddressFormAction)configuration);
            }
            else if (type == FormActionType.SendEmailToField)
            {
                ValidateSendEmailToFieldFormAction(form, (SendEmailToFieldFormAction)configuration);
            }
        }

        private void ValidatePageForwardingFormAction(Form form, PageForwardingFormAction configuration)
        {
            ulong pageId;
            PageBase page;

            if (!ulong.TryParse(configuration.Id, out pageId) || !_pageRepository.TryGet(pageId, out page))
            {
                throw new ExceptionWithReason("Invalid page for forwarding.");
            }

            if (form.Actions.Any(a => a.Type == FormActionType.PageForwarding))
            {
                throw new ExceptionWithReason("There is already a page forwarding defined.");
            }
        }

        private void ValidateCreateObjectFormAction(CreateObjectFormAction configuration)
        {
            ulong definitionId;
            DynamicObjectDefinition definition;

            if (!ulong.TryParse(configuration.ObjectDefinitionId, out definitionId) ||
                !_definitionRepository.TryGet(definitionId, out definition))
            {
                throw new ExceptionWithReason("Invalid object definition.");
            }

            var titleField = definition.Fields.OrderBy(f => f.SortOrder).FirstOrDefault();
            string titleMapping;
            if (titleField != null && configuration.Mapping.TryGetValue(titleField.Name, out titleMapping))
            {
                return;
            }

            throw new ExceptionWithReason("Invalid title field mapping.");
        }

        private void ValidateSendEmailToAddressFormAction(SendEmailToAddressFormAction configuration)
        {
            if (string.IsNullOrEmpty(configuration.EmailAddress))
            {
                throw new ExceptionWithReason("Invalid email address.");
            }

            ValidateEmailSubjectAndBody(configuration.EmailSubject, configuration.EmailTemplateHtml, configuration.EmailTemplateText);
        }

        private void ValidateSendEmailToFieldFormAction(Form form, SendEmailToFieldFormAction configuration)
        {
            ulong formFieldId;
            if (!ulong.TryParse(configuration.FieldId, out formFieldId) || form.Fields.All(f => f.Id != formFieldId))
            {
                throw new ExceptionWithReason("Invalid field for email address.");
            }

            ValidateEmailSubjectAndBody(configuration.EmailSubject, configuration.EmailTemplateHtml, configuration.EmailTemplateText);
        }

        private void ValidateEmailSubjectAndBody(string subject, string htmlTemplate, string textTemplate)
        {
            if (string.IsNullOrEmpty(subject))
            {
                throw new ExceptionWithReason("Invalid email subject.");
            }

            if (string.IsNullOrEmpty(htmlTemplate) && string.IsNullOrEmpty(textTemplate))
            {
                throw new ExceptionWithReason("Invalid email template.");
            }
        }
    }
}