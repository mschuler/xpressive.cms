﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class PunishHackingActivities : IFilter<RenderPageMessage>
    {
        private static readonly List<string> _headerBlacklist = new List<string>
        {
            "masscan",
            "python-requests",
            "python-urllib",
            "scrapy",
            "morfeus fucking scanner",
            "libwww",
            "nmap.org",
        };

        private static readonly List<string> _urlBlacklist = new List<string>
        {
            "wp-login.php",
            "iesvc/iesvc.jsp",
            "wstats/wstats.jsp",
            "zecmd/zecmd.jsp",
            "idsvc/idsvc.jsp",
            "wincfg/wincfg.jsp",
            "web-console/ServerInfo.jsp",
            "jmx-console/HtmlAdaptor",
            "invoker/JMXInvokerServlet",
            ".cgi",
            "login.stm",
            "?PHPSESSID=",
        };

        public bool Execute(RenderPageMessage msg)
        {
            if (msg.Request.Headers.UserAgent == null)
            {
                Punish(msg);
                return false;
            }

            var agent = msg.Request.Headers.UserAgent.ToString().ToLowerInvariant();

            if (_headerBlacklist.Any(agent.Contains))
            {
                Punish(msg);
                return false;
            }

            if (_urlBlacklist.Any(msg.Request.RequestUri.AbsoluteUri.Contains))
            {
                Punish(msg);
                return false;
            }

            return true;
        }

        private void Punish(RenderPageMessage msg)
        {
            Task.WaitAll(Task.Delay(TimeSpan.FromSeconds(10)));

            msg.Response = new HttpResponseMessage(HttpStatusCode.NotFound);
        }
    }
}