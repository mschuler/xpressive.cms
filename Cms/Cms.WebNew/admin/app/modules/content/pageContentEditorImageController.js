﻿(function() {
    angular.module("app").controller("PageContentEditorImageController", ["$scope", "$http", "$log", "$interval", "dialogService", "tenantService", function ($scope, $http, $log, $interval, dialogService, tenantService) {
        var previousImageSettings = "";
        $scope.previewUrl = null;
        $scope.fileStores = [];

        $scope.$watch("c.value.linkType", function (newVal) {
            if (newVal != 'page' && $scope.c.value) {
                $scope.c.value.pageLink = null;
            }
            if (newVal != 'external' && $scope.c.value) {
                $scope.c.value.externalLink = null;
            }
        });

        $http.get("/api/filestores/" + tenantService.getTenant().id, { cache: false }).success(function(fileStores) {
            $scope.fileStores = fileStores;
            updateIsLocalFile();
        });

        $scope.$watch("c.value.file", function (newVal) {
            if (newVal) {
                $scope.c.value.fileId = newVal.id;
            }
            updateIsLocalFile();
        });

        var updateIsLocalFile = function() {
            $scope.isLocalFile = false;

            if ($scope.c.value && $scope.c.value.file) {
                _.each($scope.fileStores, function (fs) {
                    if ($scope.c.value.file.fileStoreId === fs.id) {
                        $scope.isLocalFile = fs.isLocalFilesystem;
                    }
                });
            }
        };

        $scope.$watch("c.value.pageLink", function(newVal) {
            if (newVal) {
                $scope.c.value.pageId = newVal.id;
            }
        });

        $scope.$watch("c.value.imageSettings", function(newVal) {
            if (newVal) {
                $scope.c.value.alpha = newVal.alpha;
                $scope.c.value.saturation = newVal.saturation;
                $scope.c.value.roundedCorners = newVal.roundedCorners;
                $scope.c.value.brightness = newVal.brightness;
                $scope.c.value.contrast = newVal.contrast;
                $scope.c.value.filter = newVal.filter;
                $scope.c.value.tint = newVal.tint;
            }
        }, true);

        if ($scope.c.value && $scope.c.type && $scope.c.type.toUpperCase() == "IMAGE") {
            if ($scope.c.value.fileId) {
                var fileUrl = String.format("/api/file/{0}/detail", $scope.c.value.fileId);
                $http.get(fileUrl, { cache: false }).success(function(data) {
                    $scope.c.value.file = data;
                });
            }
            if ($scope.c.value.linkType && $scope.c.value.linkType.toUpperCase() == "PAGE" && $scope.c.value.pageId) {
                var pageUrl = String.format("/api/page/{0}", $scope.c.value.pageId);
                $http.get(pageUrl, { cache: false }).success(function(data) {
                    data.content = null;
                    $scope.c.value.pageLink = data;
                });
            }

            $scope.c.value.imageSettings = {
                alpha: $scope.c.value.alpha,
                saturation: $scope.c.value.saturation,
                roundedCorners: $scope.c.value.roundedCorners,
                brightness: $scope.c.value.brightness,
                contrast: $scope.c.value.contrast,
                filter: $scope.c.value.filter,
                tint: $scope.c.value.tint,
            };
        }

        $scope.matrixFilters = [ '', 'BlackWhite', 'Comic', 'Gotham', 'GreyScale', 'HiSatch', 'Invert', 'Lomograph', 'LoSatch', 'Polaroid', 'Sepia' ];

        $http.get("/api/preview/colors").success(function(data) {
            $scope.tintColors = data;
            $scope.tintColors.splice(0, 0, '');
        });

        $scope.getFiles = function (value) {
            var url = String.format("/api/file/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, {cache: false}).then(function (response) {
                return response.data;
            });
        };

        $scope.getPages = function (value) {
            var url = String.format("/api/page/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, { cache: false}).then(function (response) {
                return response.data;
            });
        };

        $scope.browseImage = function () {
            var f = null;
            if ($scope.c.value) {
                f = $scope.c.value.file;
            }
            dialogService.selectFileDialog(f, function (file) {
                if (!$scope.c.value) {
                    $scope.c.value = {};
                }
                if (!file) {
                    return;
                }
                $scope.c.value.file = file;
            });
        };

        $scope.browsePage = function () {
            var p = null;
            if ($scope.c.value) {
                p = $scope.c.value.pageLink;
            }
            dialogService.selectPageDialog(p, function (page) {
                if (!$scope.c.value) {
                    $scope.c.value = {};
                }
                if (!page) {
                    return;
                }
                page.children = [];
                $scope.c.value.pageLink = page;
            });
        };

        var updatePreviewUrl = function () {
            if (!$scope.c.value) {
                $scope.previewUrl = '';
                return;
            }
            var file = $scope.c.value.file;
            if (!file || !file.previewUrl) {
                $scope.previewUrl = '';
                return;
            }

            if (!$scope.c.value.imageSettings) {
                $scope.c.value.imageSettings = {
                    alpha: 100,
                    saturation: 0,
                    roundedCorners: 0,
                    brightness: 0,
                    contrast: 0,
                    filter: "",
                    tint: ""
                };
            }

            var imageSettingsKey = String.format("{0}+{1}", file.previewUrl, angular.toJson($scope.c.value.imageSettings));
            if (previousImageSettings == imageSettingsKey) {
                return;
            }
            previousImageSettings = imageSettingsKey;

            var url = String.format("{0}?size=500", file.previewUrl);
            var imageSettings = $scope.c.value.imageSettings;
            if (imageSettings.alpha != 100) {
                url += "&alpha=" + imageSettings.alpha;
            }
            if (imageSettings.saturation != 0) {
                url += "&saturation=" + imageSettings.saturation;
            }
            if (imageSettings.roundedCorners != 0) {
                url += "&roundedCorners=" + imageSettings.roundedCorners;
            }
            if (imageSettings.brightness != 0) {
                url += "&brightness=" + imageSettings.brightness;
            }
            if (imageSettings.contrast != 0) {
                url += "&contrast=" + imageSettings.contrast;
            }
            if (imageSettings.filter) {
                url += "&filter=" + imageSettings.filter;
            }
            if (imageSettings.tint) {
                url += "&tint=" + imageSettings.tint;
            }
            $scope.previewUrl = url;
        };

        var updateImagePreviewInterval = null;

        if (updateImagePreviewInterval == null) {
            updateImagePreviewInterval = $interval(updatePreviewUrl, 1000);
        }

        $scope.$on('$destroy', function () {
            $interval.cancel(updateImagePreviewInterval);
        });
    }]);
}());