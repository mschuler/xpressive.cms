using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using log4net;
using NPoco;

namespace Cms.Services.Tracking
{
    internal class FileTrackingService : IFileTrackingService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(FileTrackingService));
        private readonly IIpToCountryService _ipToCountryService;

        public FileTrackingService(IIpToCountryService ipToCountryService)
        {
            _ipToCountryService = ipToCountryService;
        }

        public async Task TrackFileViewAsync(ulong websiteId, ulong fileId, string ipAddress, string sessionId)
        {
            try
            {
                var country = await _ipToCountryService.GetCountryAsync(ipAddress);

                using (var database = new Database("CmsDatabaseConnection"))
                {
                    database.Insert(new FileTrackingDto
                    {
                        Date = DateTime.UtcNow,
                        FileId = Convert.ToInt64(fileId),
                        WebsiteId = Convert.ToInt64(websiteId),
                        SessionId = sessionId,
                        Country = country,
                        Count = 1,
                    });
                }
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error("Error while tracking file view.", e);
            }
        }

        public IEnumerable<Tuple<ulong, int>> GetTheTenMostViewedFilesForOneMonth(ulong websiteId)
        {
            const string sql = @"
select top 10 FileId, count(*) as [count]
from FileTracking
where Date > dateadd(m, -1, getutcdate())
and WebsiteId = @0
group by FileId
order by count(*) desc";

            try
            {
                var rows = new List<FileCountDto>(10);
                var tuples = new List<Tuple<ulong, int>>(10);

                using (var database = new Database("CmsDatabaseConnection"))
                {
                    rows.AddRange(database.Fetch<FileCountDto>(sql, Convert.ToInt64(websiteId)));
                }

                foreach (var dto in rows)
                {
                    tuples.Add(Tuple.Create(Convert.ToUInt64(dto.FileId), dto.Count));
                }

                return tuples;
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        public IEnumerable<Tuple<string, int>> GetFileViewsPerCountryForOneMonth(ulong websiteId)
        {
            const string sql = @"
select Country, count(*) as [count]
from FileTracking
where Date > dateadd(m, -1, getutcdate())
and WebsiteId = @0
group by Country
order by count(*) desc";

            try
            {
                List<CountryCountDto> rows;
                List<Tuple<string, int>> tuples;

                using (var database = new Database("CmsDatabaseConnection"))
                {
                    rows = database.Fetch<CountryCountDto>(sql, Convert.ToInt64(websiteId));
                    tuples = new List<Tuple<string, int>>(rows.Count);
                }

                foreach (var dto in rows)
                {
                    tuples.Add(Tuple.Create(dto.Country, dto.Count));
                }

                return tuples;
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        public int GetTotalViewsPerFile(ulong fileId)
        {
            const string sql = @"select count(*) from FileTracking where FileId = @0";

            try
            {
                using (var database = new Database("CmsDatabaseConnection"))
                {
                    return database.ExecuteScalar<int>(sql, Convert.ToInt64(fileId));
                }
            }
            catch (Exception e)
            {
                e.WithGuid();
                _log.Error(e.Message, e);
                throw;
            }
        }

        private class FileCountDto
        {
            public long FileId { get; set; }
            public int Count { get; set; }
        }

        private class CountryCountDto
        {
            public string Country { get; set; }
            public int Count { get; set; }
        }

        [TableName("FileTracking")]
        [PrimaryKey("Date", AutoIncrement = false)]
        private class FileTrackingDto
        {
            public DateTime Date { get; set; }
            public long FileId { get; set; }
            public string SessionId { get; set; }
            public long WebsiteId { get; set; }
            public string Country { get; set; }
            public int Count { get; set; }
        }
    }
}