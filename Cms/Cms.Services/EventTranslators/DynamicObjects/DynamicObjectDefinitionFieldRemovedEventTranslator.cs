using System;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectDefinitionFieldRemovedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectDefinitionFieldRemovedEventTranslator(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectDefinitionFieldRemoved);
        }

        public string GetHeader(IEvent @event)
        {
            return "Objektdefinition ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectDefinitionFieldRemoved)@event;
            var definition = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format(
                "Das Feld <strong>{2}</strong> der Objektdefinition <strong>{0} (Nr. {1})</strong> wurde entfernt.",
                definition.Name,
                definition.PublicId,
                e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objectdefinition";
        }
    }
}