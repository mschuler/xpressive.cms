using System;
using Cms.Aggregates.Files;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class FileTypeInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "File Type"; } }

        public void Calculate(TreeItem treeItem)
        {
            if (treeItem.ItemType != TreeItemType.Function ||
                !treeItem.Value.Equals("isfile", StringComparison.OrdinalIgnoreCase) ||
                treeItem.Children == null ||
                treeItem.Children.Count != 1)
            {
                return;
            }

            var child = treeItem.Children[0];
            var value = child.Result as File;

            treeItem.Result = (value != null).ToString();
        }
    }
}