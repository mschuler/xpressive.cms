﻿namespace Cms.Aggregates.Websites.Events
{
    public class WebsiteCreated : EventBase<Website>
    {
        public ulong TenantId { get; set; }
        public int PublicId { get; set; }
        public string Name { get; set; }
    }
}