namespace Cms.Aggregates.Links
{
    internal interface ILinkResolverStrategy
    {
        bool CanHandle(ILink link);
        string GetHyperlink(ILink link);
    }
}