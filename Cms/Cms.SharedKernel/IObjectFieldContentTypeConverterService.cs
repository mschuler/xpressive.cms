﻿namespace Cms.SharedKernel
{
    public interface IObjectFieldContentTypeConverterService
    {
        string Serialize(string type, string json);

        object Deserialize(string type, string value);

        SortableObjectField DeserializeForSortableField(string type, string value);

        object DeserializeForPublicApi(string type, string value);

        bool TryGetMetadataForPublicApi(string type, string value, out object metadata);

        object DeserializeForInterpreter(string type, string value);

        string SerializeForImport(string type, string json, string format, string options);

        string SerializeForImportPreview(string type, string json, string format, string options);

        bool IsValidValueForImport(string type, string json, string format, string options);
    }

    public class SortableObjectField
    {
        public SortableObjectField() : this(string.Empty, string.Empty) { }

        public SortableObjectField(string value) : this(value, value) { }

        public SortableObjectField(string visibleValue, string sortableValue)
        {
            VisibleValue = visibleValue;
            SortableValue = sortableValue;
        }

        public string SortableValue { get; set; }
        public string VisibleValue { get; set; }
    }
}