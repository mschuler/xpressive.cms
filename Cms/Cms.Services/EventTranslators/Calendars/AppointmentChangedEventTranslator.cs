using System;
using Cms.Aggregates.Calendars;
using Cms.Aggregates.Calendars.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Calendars
{
    internal class AppointmentChangedEventTranslator : IEventTranslator
    {
        private readonly ICalendarRepository _repository;

        public AppointmentChangedEventTranslator(ICalendarRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(AppointmentChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Termin ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (AppointmentChanged)@event;
            return string.Format("Der Termin <strong>{0}</strong> f�r am <strong>{1:f}</strong> wurde ge�ndert.", e.Title, e.StartEnd.Start);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var calendar = _repository.GetEvenIfDeleted(@event.AggregateId);
            return calendar.TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "appointment";
        }
    }
}