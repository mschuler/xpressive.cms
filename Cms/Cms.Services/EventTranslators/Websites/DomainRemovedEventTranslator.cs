﻿using System;
using Cms.Aggregates.Websites;
using Cms.Aggregates.Websites.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Websites
{
    internal class DomainRemovedEventTranslator : IEventTranslator
    {
        private readonly IWebsiteRepository _repository;

        public DomainRemovedEventTranslator(IWebsiteRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DomainRemoved);
        }

        public string GetHeader(IEvent @event)
        {
            return "Domäne entfernt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DomainRemoved)@event;
            return string.Format("Die Domäne <strong>{0}</strong> wurde entfernt.", e.Host);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "website";
        }
    }
}