using Cms.Aggregates.Security;

namespace Cms.Aggregates.Pages.Queries
{
    [RequiredPermission("page", PermissionLevel.CanSee)]
    public class GetPage : QueryBase
    {
        public ulong PageId { get; set; }
    }
}