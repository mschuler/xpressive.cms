using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cms.Aggregates.Security;
using Cms.Aggregates.Settings.Commands;
using Cms.Services.Contracts;
using log4net;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class UrlShorteningController : ApiControllerBase
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(UrlShorteningController));
        private readonly IUrlShorteningServiceProvider _urlShorteningServiceProvider;

        public UrlShorteningController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IUrlShorteningServiceProvider urlShorteningServiceProvider)
            : base(commandQueue, userRepository, queryHandler)
        {
            _urlShorteningServiceProvider = urlShorteningServiceProvider;
        }

        [HttpGet, Route("api/url/settings/{tenantId:ulong}")]
        public UrlShorteningSettingsDto GetUrlShorteningServices(ulong tenantId)
        {
            var services = _urlShorteningServiceProvider.GetServices().ToList();
            var selected = _urlShorteningServiceProvider.GetSelectedService(tenantId);
            var selectedName = selected == null ? string.Empty : selected.Name;

            var serviceDtos = services.Select(s =>
                new UrlShorteningServiceDto
                {
                    Name = s.Name,
                    Configuration = s
                        .GetConfigurationKeys()
                        .Select(k => new UrlShorteningConfigurationDto { Key = k, Value = string.Empty })
                        .ToList()
                })
                .OrderBy(s => s.Name)
                .ToList();

            return new UrlShorteningSettingsDto
            {
                SelectedService = selectedName,
                Services = serviceDtos
            };
        }

        [HttpPost, Route("api/url/settings/{tenantId:ulong}/validate")]
        public IHttpActionResult ValidateUrlShorteningService(ulong tenantId)
        {
            var selected = _urlShorteningServiceProvider.GetSelectedService(tenantId);

            if (selected == null)
            {
                return BadRequest();
            }

            try
            {
                var longUrl = "https://www.google.ch/search?q=" + Guid.NewGuid().ToString("N");
                var shortUrl = selected.ShortenUrl(longUrl, useCache: false);

                if (!string.Equals(longUrl, shortUrl, StringComparison.Ordinal))
                {
                    return Ok();
                }
            }
            catch (Exception e)
            {
                _log.Error(e.Message, e);
            }

            return BadRequest();
        }

        [HttpPost, Route("api/url/settings/{tenantId:ulong}")]
        public void SaveUrlShorteningSettings(ulong tenantId, [FromBody]UrlShorteningSettingsDto dto)
        {
            var configuration = dto.Configuration.Select(c => Tuple.Create(c.Key, c.Value)).ToList();

            SendCommand(new ChangeUrlShorteningSettings
            {
                TenantId = tenantId,
                AggregateId = tenantId,
                ServiceName = dto.SelectedService,
                Configuration = configuration
            });
        }

        public class UrlShorteningSettingsDto
        {
            public string SelectedService { get; set; }
            public IList<UrlShorteningServiceDto> Services { get; set; }
            public IList<UrlShorteningConfigurationDto> Configuration { get; set; }
        }

        public class UrlShorteningServiceDto
        {
            public string Name { get; set; }
            public IList<UrlShorteningConfigurationDto> Configuration { get; set; }
        }

        public class UrlShorteningConfigurationDto
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }
    }
}