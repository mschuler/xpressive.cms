using System.Net;
using System.Net.Http;
using Cms.Aggregates.PageTemplates;
using log4net;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class LoadPageTemplate : IFilter<RenderPageMessage>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(LoadPageTemplate));
        private readonly IPageTemplateRepository _repository;

        public LoadPageTemplate(IPageTemplateRepository repository)
        {
            _repository = repository;
        }

        public bool Execute(RenderPageMessage msg)
        {
            PageTemplate pageTemplate;
            if (_repository.TryGet(msg.Page.PageTemplateId, out pageTemplate) && !string.IsNullOrEmpty(pageTemplate.Template))
            {
                msg.PageTemplate = pageTemplate;
                return true;
            }

            _log.WarnFormat("There is no page template for page {0}.", msg.Page.Fullname);
            msg.Response = new HttpResponseMessage(HttpStatusCode.NoContent);
            return false;
        }
    }
}