﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cms.Services.Contracts
{
    public interface IFileTrackingService
    {
        Task TrackFileViewAsync(ulong websiteId, ulong fileId, string ipAddress, string sessionId);

        IEnumerable<Tuple<ulong, int>> GetTheTenMostViewedFilesForOneMonth(ulong websiteId);

        IEnumerable<Tuple<string, int>> GetFileViewsPerCountryForOneMonth(ulong websiteId);

        int GetTotalViewsPerFile(ulong fileId);
    }
}
