﻿using System.Collections.Generic;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.Forms;
using Cms.Services.ObjectFieldContentTypeConverter;
using Cms.Services.TemplateEngineInterpreters;
using Cms.SharedKernel;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Interpreter;
using Moq;
using Xunit;

namespace Cms.Services.Tests
{
    public class Given_an_object_field_in_concat
    {
        public Given_an_object_field_in_concat()
        {
            var objectRepositoryMock = new Mock<IDynamicObjectRepository>();
            var definitionRepositoryMock = new Mock<IDynamicObjectDefinitionRepository>();
            var formRepositoryMock = new Mock<IFormRepository>();
            var definitions = new List<DynamicObjectDefinition>();
            var converters = new List<IObjectFieldContentTypeConverter>();
            var f = new Form(1, 1, "formname", 1);
            formRepositoryMock.Setup(m => m.GetByPublicId(1)).Returns(f);
            formRepositoryMock.Setup(m => m.TryGet(1, out f)).Returns(true);
            var formRepository = formRepositoryMock.Object;
            definitions.Add(new DynamicObjectDefinition { PublicId = 1, Id = 1234567 });
            converters.Add(new ObjectFieldFormConverter(formRepository));
            var o = new DynamicObject { DefinitionId = 1234567, Id = 1, PublicId = 1 };
            o.Add(new DynamicObjectField { ContentType = "form", Name = "Formular", Value = "1" });
            objectRepositoryMock.Setup(m => m.Get(1)).Returns(() => o);
            definitionRepositoryMock.Setup(s => s.GetAll()).Returns(() => definitions);
            var objectRepository = objectRepositoryMock.Object;
            var contentTypeConverterService = new ObjectFieldContentTypeConverterService(converters);

            InterpreterFactory.Instance.Clear();
            InterpreterFactory.Instance.Register(new IifInterpreter());
            InterpreterFactory.Instance.Register(new NullInterpreter());
            InterpreterFactory.Instance.Register(new StringInterpreter());
            InterpreterFactory.Instance.Register(new ObjectInterpreter(objectRepository, contentTypeConverterService));
            InterpreterFactory.Instance.Register(new FormInterpreter(formRepository));
        }

        [Fact]
        public void Then_it_works()
        {
            var engine = new Engine();
            var o = new DynamicObject { DefinitionId = 1234567, Id = 1, PublicId = 1 };
            o.Add(new DynamicObjectField { ContentType = "form", Name = "Formular", Value = "1" });
            var expression = "{iif(isnull($(\"Formular\")), \"true\",\"\", concat('<a title=\"jetzt anmelden\" class=\"flaticon-pencil8 formular\" href=\"#\" onclick=\"openFormular(', $(\"Formular\").id,')\">Anmeldung</a>'))}";

            using (TemplateStateMap.Set(TemplateStateMap.CurrentObjectKey, o))
            {
                var result = engine.Generate(expression);
                result.ToString();
            }
        }
    }
}
