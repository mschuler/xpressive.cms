﻿using Cms.Aggregates.Pages;

namespace Cms.Services.Contracts
{
    public interface IPageRenderingService
    {
        string Render(Page page);
    }
}
