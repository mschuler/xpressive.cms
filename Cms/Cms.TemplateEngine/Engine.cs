﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cms.TemplateEngine.Interpreter;
using log4net;

namespace Cms.TemplateEngine
{
    public class Engine
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Engine));
        private static readonly char[] _newLineChars = { '\r', '\n' };
        private readonly ExpressionExtractor _extractor = new ExpressionExtractor();

        public string Generate(string template)
        {
            var result = new StringBuilder();
            var lines = template.Split(_newLineChars, StringSplitOptions.RemoveEmptyEntries);

            foreach (var line in lines)
            {
                var expressions = _extractor.GetAllExpressions(line);
                var lineBuilder = new StringBuilder(line);

                foreach (var expression in expressions.OrderByDescending(e => e.Position))
                {
                    object obj;

                    try
                    {
                        obj = CalculateExpression(expression);
                    }
                    catch (Exception ex)
                    {
                        var msg = string.Format("Unable to calculate expression '{0}'.", expression.FullName);
                        _log.Error(msg);
                        throw new InvalidOperationException(msg, ex);
                    }

                    if (obj != null)
                    {
                        lineBuilder.Replace(expression.FullName, obj.ToString(), expression.Position, expression.FullName.Length);
                    }
                }

                result.AppendLine(lineBuilder.ToString());
            }

            return result.ToString().TrimEnd(_newLineChars);
        }

        private static object CalculateExpression(Expression expression)
        {
            var item = Parser.Parse(expression.Name);
            var obj = InterpreterFactory.Instance.Calculate(item);
            return obj;
        }

        public List<Expression> ParseExpression(string expression)
        {
            return _extractor.GetAllExpressions(expression);
        }

        public TreeItem ParseTreeItem(string expression)
        {
            expression = expression.Trim(' ', '{', '}');
            return Parser.Parse(expression);
        }
    }
}
