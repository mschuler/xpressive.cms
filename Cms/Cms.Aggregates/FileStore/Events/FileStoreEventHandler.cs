﻿namespace Cms.Aggregates.FileStore.Events
{
    internal class FileStoreEventHandler : EventHandlerBase
    {
        private readonly IFileStoreRepository _repository;
        private readonly IFileStoreProvider _provider;

        public FileStoreEventHandler(IFileStoreRepository repository, IFileStoreProvider provider)
        {
            _repository = repository;
            _provider = provider;
        }

        public void When(FileStoreCreated @event)
        {
            var provider = _provider.GetFactory(@event.FileStoreFactoryId);

            var fileStore = provider.Create(@event.AggregateId, @event.Name, @event.Settings);

            _repository.Add(fileStore);
        }
    }
}
