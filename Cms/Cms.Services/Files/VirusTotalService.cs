using System;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Cms.Aggregates.FileStore;
using Cms.Services.Contracts;
using Newtonsoft.Json;

namespace Cms.Services.Files
{
    internal sealed class VirusTotalService : IVirusTotalService
    {
        private static DateTime _lastApiCall = DateTime.MinValue;
        private static Regex _apiKeyValidator = new Regex("^[a-fA-F0-9]{64}$", RegexOptions.Compiled);
        private readonly IFileHashCalculator _hashCalculator;
        private readonly string _apiKey;

        public VirusTotalService(IFileHashCalculator hashCalculator)
        {
            _hashCalculator = hashCalculator;
            _apiKey = ConfigurationManager.AppSettings["virusTotalApiKey"];
        }

        public async Task<EnqueueResult> EnqueueAsync(FileInfo file, string fileName)
        {
            if (!IsValidApiKey())
            {
                return new EnqueueResult();
            }

            await SleepIfNecessaryAsync();

            var content = new MultipartFormDataContent();
            content.Add(new StringContent(_apiKey), "apikey");
            content.Add(new ByteArrayContent(File.ReadAllBytes(file.FullName)), "file", fileName);
            var response = await new HttpClient().PostAsync("https://www.virustotal.com/vtapi/v2/file/scan", content);
            _lastApiCall = DateTime.UtcNow;

            response.EnsureSuccessStatusCode();

            var json = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<VirusTotalScanResponse>(json);

            if (result.ResponseCode == 1)
            {
                return new EnqueueResult
                {
                    ScanId = result.ScanId,
                    Success = true,
                };
            }

            return new EnqueueResult();
        }

        public async Task<ReportResult> GetReportAsync(FileInfo file)
        {
            var hash = _hashCalculator.Sha256AsHexString(file);
            return await GetReportAsync(hash);
        }

        public async Task<ReportResult> GetReportAsync(string scanId)
        {
            if (!IsValidApiKey())
            {
                return new ReportResult();
            }

            await SleepIfNecessaryAsync();

            var url = string.Format("https://www.virustotal.com/vtapi/v2/file/report?apikey={0}&resource={1}", _apiKey, scanId);
            var client = new HttpClient();
            var json = await client.GetStringAsync(url);
            var result = JsonConvert.DeserializeObject<VirusTotalReportResponse>(json);
            _lastApiCall = DateTime.UtcNow;

            if (result.ResponseCode == 1)
            {
                return new ReportResult
                {
                    Success = true,
                    IsMalicious = result.Positives > 1
                };
            }

            return new ReportResult();
        }

        public bool IsValidApiKey()
        {
            if (string.IsNullOrEmpty(_apiKey))
            {
                return false;
            }

            return _apiKeyValidator.IsMatch(_apiKey);
        }

        private static async Task SleepIfNecessaryAsync()
        {
            var sleepSeconds = 16 - (DateTime.UtcNow - _lastApiCall).TotalSeconds;
            if (sleepSeconds > 0)
            {
                await Task.Delay(TimeSpan.FromSeconds(sleepSeconds));
            }
        }

        public class VirusTotalScanResponse
        {
            [JsonProperty("response_code")]
            public int ResponseCode { get; set; }

            [JsonProperty("scan_id")]
            public string ScanId { get; set; }
        }

        public class VirusTotalReportResponse
        {
            [JsonProperty("response_code")]
            public int ResponseCode { get; set; }

            [JsonProperty("positives")]
            public int Positives { get; set; }
        }
    }
}