using System;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class Totp2FaEnabledEventTranslator : IEventTranslator
    {
        private readonly IUserRepository _repository;

        public Totp2FaEnabledEventTranslator(IUserRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(Totp2FaEnabled);
        }

        public string GetHeader(IEvent @event)
        {
            return "2FA Aktiviert";
        }

        public string GetContent(IEvent @event)
        {
            var user = _repository.GetEvenIfDeleted(@event.AggregateId);
            var username = string.Format("{0} {1}", user.FirstName, user.LastName).Trim();
            return string.Format("Der Benutzers <strong>{0}</strong> hat die Zwei-Faktor Authentifizierung aktiviert.", username);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}