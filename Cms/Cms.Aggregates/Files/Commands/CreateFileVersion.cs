using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Files.Commands
{
    [RequiredPermission("file", PermissionLevel.CanChange)]
    public class CreateFileVersion : CommandBase
    {
        [Required, ValidId]
        public ulong FileVersion { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string TemporaryFilePath { get; set; }
    }
}