﻿namespace Cms.Aggregates.Websites.Events
{
    public class DomainHostChanged : EventBase<Website>
    {
        public string OldHost { get; set; }
        public string NewHost { get; set; }
    }
}