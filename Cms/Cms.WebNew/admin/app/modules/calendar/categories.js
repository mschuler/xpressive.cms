﻿(function() {
    angular.module("app").controller("CalendarCategoriesController", ["$rootScope", "$scope", "$http", "toastService", "dialogService", "tenantService", "$translate", function ($rootScope, $scope, $http, toastService, dialogService, tenantService, $translate) {
        $scope.categories = [];
        $scope.selectedCategory = {};

        function reloadCategories() {
            $http.get("/api/calendar/categories/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
                $scope.categories = data;
            });
        };

        $http.get("/api/permission/calendarcategory/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.select = function(category) {
            $scope.selectedCategory = category;
        }

        $scope.rename = function (category) {
            $translate(["rename_calendar_category", "name", "error_rename_calendar_category"]).then(function(t) {
                dialogService.singleInputDialog(t.rename_calendar_category, t.name, category.name, function (name) {
                    var dto = {
                        oldName: category.name,
                        newName: name
                    };
                    $http.post("/api/calendar/categories/" + tenantService.getTenant().id + "/rename", dto).success(function () {
                        category.name = name;
                    }).error(function (error) {
                        toastService.error(t.error_rename_calendar_category, error);
                    });
                });
            });
        };

        $scope.delete = function (category) {
            $translate(["delete_calendar_category", "confirm_delete_calendar_category", "error_delete_calendar_category"]).then(function(t) {
                dialogService.confirmDialog(t.delete_calendar_category, String.format(t.confirm_delete_calendar_category, category.name), function () {
                    var url = String.format("/api/calendar/categories/{0}?name={1}", tenantService.getTenant().id, encodeURIComponent(category.name));
                    $http.delete(url).success(function () {
                        $scope.categories = _.reject($scope.categories, function (c) {
                            return c.name === category.name;
                        });
                    }).error(function (error) {
                        toastService.error(t.error_delete_calendar_category, error);
                    });
                });
            });
        };

        $scope.create = function () {
            $translate(["create_calendar_category", "name", "error_create_calendar_category"]).then(function(t) {
                dialogService.singleInputDialog(t.create_calendar_category, t.name, "", function (name) {
                    var dto = {
                        name: name
                    };
                    $http.post("/api/calendar/categories/" + tenantService.getTenant().id + "/new", dto).success(function() {
                        $scope.categories.push({name: name});
                    }).error(function (error) {
                        toastService.error(t.error_create_calendar_category, error);
                    });
                });
            });
            
        };

        $rootScope.$on("tenantChanged", reloadCategories);

        $rootScope.$on('logout', function () { $scope.categories = {}; });

        reloadCategories();
    }]);
}());