﻿using System;
using System.Linq;
using Cms.Aggregates.DynamicObjects;
using Cms.SharedKernel;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal sealed class ObjectInterpreter : IInterpreter
    {
        private readonly IDynamicObjectRepository _repository;
        private readonly IObjectFieldContentTypeConverterService _contentTypeConverterService;

        public ObjectInterpreter(IDynamicObjectRepository repository, IObjectFieldContentTypeConverterService contentTypeConverterService)
        {
            _repository = repository;
            _contentTypeConverterService = contentTypeConverterService;
        }

        public int Priority { get; set; }

        public string Name { get { return "object"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function && tree.Value.Equals("object", StringComparison.OrdinalIgnoreCase))
            {
                CalculateObject(tree);
            }
            else if (tree.ItemType == TreeItemType.Attribute && tree.LeftItem != null)
            {
                CalculateAttribute(tree);
            }
            else if (tree.ItemType == TreeItemType.Function && tree.LeftItem != null && tree.Children.Count == 1 && tree.Value.Equals("field"))
            {
                CalculateField(tree);
            }
            else if (tree.ItemType == TreeItemType.Function && tree.Value.Equals("$", StringComparison.Ordinal) && tree.Children.Count == 1 && tree.Children[0].ParameterType == ParameterType.String)
            {
                var currentObject = TemplateStateMap.Get<DynamicObject>(TemplateStateMap.CurrentObjectKey);
                if (currentObject != null)
                {
                    CalculateCurrentObject(tree, currentObject);
                }
            }
        }

        private void CalculateField(TreeItem tree)
        {
            var o = tree.LeftItem.Result as DynamicObject;
            if (o == null)
            {
                return;
            }

            var name = tree.Children[0].Result as string;

            SetFieldValue(name, tree, o);
        }

        private void CalculateAttribute(TreeItem tree)
        {
            var o = tree.LeftItem.Result as DynamicObject;

            if (o == null)
            {
                return;
            }

            SetAttributeValue(tree.Value, tree, o);
        }

        private void CalculateObject(TreeItem tree)
        {
            if (tree.Children.Count == 1)
            {
                var child = tree.Children[0];

                if (child.Result is int)
                {
                    var o = _repository.GetByPublicId((int)child.Result);
                    tree.Result = o;
                }
            }
        }

        private void CalculateCurrentObject(TreeItem tree, DynamicObject dynamicObject)
        {
            var fieldName = (string)tree.Children[0].Result;

            if (SetAttributeValue(fieldName, tree, dynamicObject))
            {
                return;
            }

            SetFieldValue(fieldName, tree, dynamicObject);
        }

        private bool SetAttributeValue(string name, TreeItem tree, DynamicObject dynamicObject)
        {
            switch (name.ToLowerInvariant())
            {
                case "title":
                    tree.Result = dynamicObject.Title;
                    return true;
                case "id":
                    tree.Result = dynamicObject.PublicId;
                    return true;
                case "date":
                    tree.Result = dynamicObject.CreationDate;
                    return true;
            }

            return false;
        }

        private void SetFieldValue(string name, TreeItem tree, DynamicObject dynamicObject)
        {
            if (string.IsNullOrEmpty(name))
            {
                return;
            }

            var field = dynamicObject.Fields.SingleOrDefault(f => f.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            if (field == null)
            {
                return;
            }

            var result = _contentTypeConverterService.DeserializeForInterpreter(field.ContentType, field.Value);
            tree.Result = result;
        }
    }
}