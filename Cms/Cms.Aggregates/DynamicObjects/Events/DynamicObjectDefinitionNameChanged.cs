namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDefinitionNameChanged : EventBase<DynamicObjectDefinition>
    {
        public string Name { get; set; }
    }
}