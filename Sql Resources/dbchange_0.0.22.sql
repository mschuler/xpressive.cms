
begin transaction

alter table MediaContent add PreviewImageId int;

alter table MediaContent add constraint FK_MediaContent_MediaContentImage foreign key (PreviewImageId) references MediaContentImage (Id);

insert into Version (Major, Minor, Revision) values (0, 0, 22);

commit transaction
