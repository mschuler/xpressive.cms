﻿using System.Collections.Generic;

namespace Cms.EventSourcing.Contracts.Queries
{
    public interface IQueryHandler
    {
        bool CanHandle(IQuery query);

        IEnumerable<object> Handle(IQuery query);
    }
}
