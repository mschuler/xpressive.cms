using System;
using System.Collections.Generic;

namespace Cms.Aggregates.FileStore
{
    public interface IFileStoreFactory
    {
        Guid Id { get; }

        string Name { get; }

        IFileStore Create();

        IFileStore Create(ulong id, string name, IDictionary<string, string> settings);

        bool IsValidConfiguration(IDictionary<string, string> settings);
    }
}