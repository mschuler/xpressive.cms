﻿using System;
using Cms.SharedKernel;

namespace Cms.Aggregates.Pages
{
    public abstract class PageContentValueBase : ValueObject<PageContentValueBase>
    {
        public static string GetType(PageContentValueBase value)
        {
            if (value == null) { return string.Empty; }
            if (value is PageContentImageValue) { return "Image"; }
            if (value is PageContentHtmlValue) { return "Html"; }
            if (value is PageContentMarkdownValue) { return "Markdown"; }
            if (value is PageContentFormValue) { return "Form"; }
            if (value is PageContentObjectValue) { return "Object"; }
            if (value is PageContentTextValue) { return "Text"; }
            if (value is PageContentCodeValue) { return "Code"; }
            if (value is PageContentBooleanValue) { return "Boolean"; }

            throw new NotSupportedException(value.GetType().Name).WithGuid();
        }

        public static Type GetPageContentType(string type)
        {
            type = type.ToUpperInvariant();
            switch (type)
            {
                case "IMAGE":
                    return typeof(PageContentImageValue);
                case "HTML":
                    return typeof(PageContentHtmlValue);
                case "MARKDOWN":
                    return typeof(PageContentMarkdownValue);
                case "FORM":
                    return typeof(PageContentFormValue);
                case "OBJECT":
                    return typeof(PageContentObjectValue);
                case "TEXT":
                    return typeof(PageContentTextValue);
                case "CODE":
                    return typeof(PageContentCodeValue);
                case "BOOLEAN":
                    return typeof(PageContentBooleanValue);
                default:
                    throw new NotSupportedException(type).WithGuid();
            }
        }
    }
}