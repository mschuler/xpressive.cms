﻿namespace Cms.Aggregates.Forms.Events
{
    public class FormNameChanged : EventBase<Form>
    {
        public string Name { get; set; }
    }
}