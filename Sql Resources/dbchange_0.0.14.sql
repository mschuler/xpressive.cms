begin transaction

alter table Article drop column RenderedContent;

insert into Version (Major, Minor, Revision) values (0, 0, 14);

commit transaction
