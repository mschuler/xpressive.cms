﻿using System;
using Cms.Aggregates.Websites;
using Cms.Aggregates.Websites.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Websites
{
    internal class DomainHostChangedEventTranslator : IEventTranslator
    {
        private readonly IWebsiteRepository _repository;

        public DomainHostChangedEventTranslator(IWebsiteRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DomainHostChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Domainname geändert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DomainHostChanged)@event;
            var website = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Die Domain <strong>{0}</strong> der Website <strong>{1}</strong> wurde in <strong>{2}</strong> umbenannt.", e.OldHost, website.Name, e.NewHost);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "website";
        }
    }
}