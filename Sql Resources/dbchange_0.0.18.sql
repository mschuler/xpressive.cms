
begin transaction

alter table Directory add Guid uniqueidentifier not null default '00000000-0000-0000-0000-000000000000';

update Directory set Guid = newid();

alter table Directory add constraint UX_Directory_Guid unique (Guid);

insert into Version (Major, Minor, Revision) values (0, 0, 18);

commit transaction

