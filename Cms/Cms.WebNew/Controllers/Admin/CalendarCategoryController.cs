using System.Collections.Generic;
using System.Web.Http;
using Cms.Aggregates.Calendars.Commands;
using Cms.Aggregates.Calendars.Queries;
using Cms.Aggregates.Security;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class CalendarCategoryController : ApiControllerBase
    {
        public CalendarCategoryController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler)
            : base(commandQueue, userRepository, queryHandler)
        {
        }

        [HttpGet, Route("api/calendar/categories/{tenantId:ulong}")]
        public IEnumerable<object> Get(ulong tenantId)
        {
            return ExecuteQuery(new GetAllCalendarCategories { TenantId = tenantId });
        }

        [HttpGet, Route("api/calendar/categories/{tenantId:ulong}")]
        public IEnumerable<object> Get(ulong tenantId, [FromUri]string search)
        {
            return ExecuteQuery(new GetAllCalendarCategories { TenantId = tenantId, SearchText = search });
        }


        [HttpPost, Route("api/calendar/categories/{tenantId:ulong}/new")]
        public void Create(ulong tenantId, [FromBody]NameDto dto)
        {
            SendCommand(new AddCategory
            {
                AggregateId = tenantId,
                Name = dto.Name,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/calendar/categories/{tenantId:ulong}/rename")]
        public void Rename(ulong tenantId, [FromBody]RenameDto dto)
        {
            SendCommand(new ChangeCategoryName
            {
                AggregateId = tenantId,
                OldName = dto.OldName,
                NewName = dto.NewName,
                TenantId = tenantId,
            });
        }

        [HttpDelete, Route("api/calendar/categories/{tenantId:ulong}")]
        public void Delete(ulong tenantId, [FromUri]string name)
        {
            SendCommand(new RemoveCategory
            {
                AggregateId = tenantId,
                Name = name,
                TenantId = tenantId,
            });
        }

        public class NameDto
        {
            public string Name { get; set; }
        }

        public class RenameDto
        {
            public string NewName { get; set; }
            public string OldName { get; set; }
        }
    }
}