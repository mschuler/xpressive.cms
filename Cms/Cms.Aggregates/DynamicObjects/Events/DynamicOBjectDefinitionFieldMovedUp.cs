﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDefinitionFieldMovedUp : EventBase<DynamicObjectDefinition>
    {
        public string Name { get; set; }
    }
}