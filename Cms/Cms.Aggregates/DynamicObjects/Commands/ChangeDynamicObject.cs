﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("object", PermissionLevel.CanChange)]
    public class ChangeDynamicObject : CommandBase
    {
        [Required]
        public Dictionary<string, string> Values { get; set; }
    }
}