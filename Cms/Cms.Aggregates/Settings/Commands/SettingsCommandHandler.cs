﻿using System;
using System.Linq;
using Cms.Aggregates.Settings.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.Settings.Commands
{
    internal class SettingsCommandHandler : CommandHandlerBase
    {
        private readonly IEncryptionService _encryptionService;

        public SettingsCommandHandler(IEncryptionService encryptionService)
        {
            _encryptionService = encryptionService;
        }

        public void When(ChangeEmailSettings command, Action<IEvent> eventHandler)
        {
            var smtpPassword = command.SmtpPassword.RemoveControlChars();

            if (command.IsSmtpAuthenticationRequired)
            {
                if (string.IsNullOrEmpty(command.SmtpUsername) || string.IsNullOrEmpty(smtpPassword))
                {
                    throw new ExceptionWithReason("If SMTP authentication is required, you have to provide a username and a password.").WithGuid();
                }
            }

            smtpPassword = Encrypt(smtpPassword);

            var e = new EmailSettingsChanged
            {
                AggregateId = command.TenantId,
                UserId = command.UserId,
                EmailAddress = command.EmailAddress.RemoveControlChars(),
                ResponseAddress = command.ResponseAddress.RemoveControlChars(),
                Host = command.Host.RemoveControlChars(),
                Port = command.Port,
                IsSmtpAuthenticationRequired = command.IsSmtpAuthenticationRequired,
                IsSslUsedForAuthentication = command.IsSslUsedForAuthentication,
                SmtpUsername = command.SmtpUsername.RemoveControlChars(),
                SmtpPassword = smtpPassword,
            };

            eventHandler(e);
        }

        public void When(ChangeUrlShorteningSettings command, Action<IEvent> eventHandler)
        {
            var configuration = command.Configuration.Select(GetEncryptedTuple).ToList();

            var e = new UrlShorteningSettingsChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                ServiceName = command.ServiceName.RemoveControlChars(),
                Configuration = configuration
            };

            eventHandler(e);
        }

        private string Encrypt(string plain)
        {
            if (string.IsNullOrEmpty(plain))
            {
                return string.Empty;
            }

            var encrypted = _encryptionService.EncryptString(plain);
            return Convert.ToBase64String(encrypted);
        }

        private Tuple<string, string> GetEncryptedTuple(Tuple<string, string> tuple)
        {
            return Tuple.Create(
                tuple.Item1.RemoveControlChars(),
                Encrypt(tuple.Item2.RemoveControlChars()));
        }
    }
}