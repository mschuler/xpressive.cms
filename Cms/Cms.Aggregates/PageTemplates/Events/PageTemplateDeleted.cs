﻿namespace Cms.Aggregates.PageTemplates.Events
{
    public class PageTemplateDeleted : EventBase<PageTemplate>
    {
        public string Name { get; set; }
    }
}