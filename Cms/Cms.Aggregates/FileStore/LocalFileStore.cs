﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using Cms.Aggregates.Files;
using Cms.SharedKernel;
using File = Cms.Aggregates.Files.File;

namespace Cms.Aggregates.FileStore
{
    internal class LocalFileStore : IFileStore
    {
        private readonly IDictionary<string, string> _settings;

        public LocalFileStore()
        {
            _settings = new ReadOnlyDictionary<string, string>(new Dictionary<string, string>());
        }

        public LocalFileStore(IDictionary<string, string> settings)
        {
            _settings = new ReadOnlyDictionary<string, string>(settings);
        }

        public ulong Id { get; set; }
        public Guid FileStoreFactoryId { get; set; }
        public string Name { get; set; }
        public IDictionary<string, string> Settings { get { return _settings; } }
        public bool IsLocalFilesystem { get { return true; } }

        public void Save(string fileName, ulong fileId, ulong version, FileInfo temporaryFile)
        {
            var location = Files.LocalFileStore.GetFileLocation(fileId, version);
            temporaryFile.CopyTo(location);
        }

        public void Delete(ulong fileId, ulong version)
        {
            var location = Files.LocalFileStore.GetFileLocation(fileId, version);
            var fileInfo = new FileInfo(location);

            if (fileInfo.Exists)
            {
                fileInfo.Delete();
            }
        }

        public string GetLocalFileLocation(ulong fileId, ulong version)
        {
            var location = Files.LocalFileStore.GetFileLocation(fileId, version);
            return location;
        }

        public Uri GetIcon()
        {
            return new Uri("/admin/images/icons/hdd_24.png", UriKind.Relative);
        }

        public Uri GetUrl(File file)
        {
            return GetUrl(file, null);
        }

        public Uri GetUrl(File file, ImageSettingsDto imageSettings)
        {
            var json = string.Empty;

            if (file.IsImage() && imageSettings != null && !imageSettings.Equals(new ImageSettingsDto()))
            {
                json = ImageSettingsDto.Serialize(imageSettings) + "/";
            }

            var url = string.Format(
                CultureInfo.InvariantCulture,
                "/download/local/{0:D}/{1:D}/{2}{3}",
                file.Id,
                file.Versions.Max(v => v.Version),
                json,
                file.Name.ToValidFileName());

            return new Uri(url, UriKind.Relative);
        }
    }
}