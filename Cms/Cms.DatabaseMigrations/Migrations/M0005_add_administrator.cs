using System;
using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0005_add_administrator : DatabaseMigrationBase
    {
        public M0005_add_administrator() : base(5) { }

        public override void Execute(Database database)
        {
            // password = 1234
            var json = string.Format("{{\"EmailAddress\":\"admin@xpressive.cms\",\"FirstName\":\"\",\"LastName\":\"Administrator\",\"IsSysadmin\":true,\"Password\":\"$2a$10$XMy3JQDZMDrBZnp2VfWM2O0p05TORUF19tfzSfXc6Tmnx9djEckfu\",\"Date\":\"{0:O}\"}}", DateTime.UtcNow);
            var dto = new EventDto
            {
                Transaction = 62130515292995584,
                Version = 62130515292987392,
                AggregateType = "User",
                AggregateId = 123456789,
                Event = "UserCreated",
                UserId = 100,
                Data = json,
                IsDeleted = false,
            };

            database.Insert(dto);
        }
    }
}