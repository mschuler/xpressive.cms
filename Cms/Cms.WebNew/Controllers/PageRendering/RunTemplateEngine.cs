using System;
using Cms.TemplateEngine;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class RunTemplateEngine : IFilter<RenderPageMessage>
    {
        public bool Execute(RenderPageMessage msg)
        {
            var engine = new Engine();
            string previous;

            do
            {
                previous = msg.Content;
                msg.Content = engine.Generate(msg.Content);
            } while (!string.Equals(previous, msg.Content, StringComparison.Ordinal));

            return true;
        }
    }
}