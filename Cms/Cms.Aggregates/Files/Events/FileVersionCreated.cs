namespace Cms.Aggregates.Files.Events
{
    public class FileVersionCreated : EventBase<File>
    {
        public ulong FileVersion { get; set; }
        public string Hash { get; set; }
        public ulong Size { get; set; }
    }
}