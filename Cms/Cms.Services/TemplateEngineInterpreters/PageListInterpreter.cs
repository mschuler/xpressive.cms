﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Websites;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal sealed class PageListInterpreter : IInterpreter
    {
        private readonly IPageRepository _pageRepository;
        private readonly IWebsiteRepository _websiteRepository;

        public PageListInterpreter(IPageRepository pageRepository, IWebsiteRepository websiteRepository)
        {
            _pageRepository = pageRepository;
            _websiteRepository = websiteRepository;
        }

        public int Priority { get; set; }

        public string Name { get { return "pages"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function &&
                tree.Value.Equals("pages", StringComparison.OrdinalIgnoreCase) &&
                tree.Children != null &&
                tree.Children.Count >= 1 &&
                tree.Children.Count <= 2 &&
                tree.Children[0].Result is int)
            {
                var publicWebsiteId = (int)tree.Children[0].Result;
                var website = _websiteRepository.GetAll().SingleOrDefault(w => w.PublicId == publicWebsiteId);
                var showLinks = false;

                if (website == null)
                {
                    return;
                }

                if (tree.Children.Count == 2 &&
                    tree.Children[1].Result is string &&
                    ((string)tree.Children[1].Result).Equals("true", StringComparison.OrdinalIgnoreCase))
                {
                    showLinks = true;
                }

                var allPages = _pageRepository.GetAllByWebsiteId(website.Id).ToList();
                var pages = allPages.OfType<Page>().Where(p => p.IsEnabled).ToList<PageBase>();

                if (showLinks)
                {
                    pages.AddRange(allPages.OfType<PageLink>());
                }

                pages = pages
                    .OrderBy(p => p.SortOrder)
                    .ThenBy(p => p.Name, StringComparer.OrdinalIgnoreCase)
                    .ToList();

                tree.Result = pages;
            }

            if (tree.ItemType == TreeItemType.Function &&
                tree.Value.Equals("template", StringComparison.OrdinalIgnoreCase) &&
                tree.Children != null && tree.Children.Count == 1 &&
                tree.LeftItem != null && tree.LeftItem.Result != null)
            {
                var collection = tree.LeftItem.Result as IEnumerable<Page>;
                var collectionWithLinks = tree.LeftItem.Result as IEnumerable<PageBase>;
                var o = tree.LeftItem.Result as PageBase;
                var pages = new List<PageBase>();

                if (collection == null && o == null && collectionWithLinks == null)
                {
                    return;
                }

                if (o != null)
                {
                    pages.Add(o);
                }
                else if (collection != null)
                {
                    pages.AddRange(collection);
                }
                else
                {
                    pages.AddRange(collectionWithLinks);
                }

                var result = new StringBuilder();
                var engine = new Engine();

                if (tree.Children[0].Result is string)
                {
                    var template = (string)tree.Children[0].Result;

                    if (string.IsNullOrEmpty(template))
                    {
                        return;
                    }

                    foreach (var page in pages)
                    {
                        result.Append(RenderTemplate(engine, page, template));
                    }
                }

                tree.Result = result.ToString();
            }
        }

        private string RenderTemplate(Engine engine, PageBase page, string template)
        {
            using (TemplateStateMap.Set(TemplateStateMap.CurrentObjectKey, page))
            {
                string previousResult;
                var result = template;

                do
                {
                    previousResult = result;
                    result = engine.Generate(previousResult);
                } while (!string.Equals(result, previousResult, StringComparison.Ordinal));

                return result;
            }
        }
    }
}