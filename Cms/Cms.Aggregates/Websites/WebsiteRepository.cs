using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Pages;

namespace Cms.Aggregates.Websites
{
    internal class WebsiteRepository : RepositoryBase<Website>, IWebsiteRepository
    {
        private static readonly object _lock = new object();
        private static int _currentMaxPublicId;
        private readonly IPageRepository _pageRepository;

        public WebsiteRepository(IPageRepository pageRepository)
        {
            _pageRepository = pageRepository;
        }

        public int GetNextPublicId()
        {
            lock (_lock)
            {
                if (_currentMaxPublicId == 0)
                {
                    var websites = GetAll().ToList();
                    _currentMaxPublicId = websites.Count == 0 ? 0 : websites.Max(p => p.PublicId);
                }

                _currentMaxPublicId++;
                return _currentMaxPublicId;
            }
        }

        public Website GetByHost(string host)
        {
            var websites = GetAll();
            foreach (var website in websites.ToList())
            {
                var domains = website.Domains.ToList();
                if (domains.Any(d => d.Host.Equals(host, StringComparison.OrdinalIgnoreCase)))
                {
                    return website;
                }
            }
            return null;
        }

        public IEnumerable<Website> GetAll(ulong tenantId)
        {
            return GetAll()
                .Where(w => w.TenantId.Equals(tenantId))
                .ToList();
        }

        public override void DeleteAllByTenantId(ulong tenantId)
        {
            var websites = GetAll(tenantId).ToList();

            foreach (var website in websites)
            {
                var pages = _pageRepository.GetAllByWebsiteId(website.Id).ToList();
                pages.ForEach(p => _pageRepository.Delete(p.Id));

                Delete(website.Id);
            }
        }
    }
}