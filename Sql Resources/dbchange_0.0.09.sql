begin transaction

create table PageContent (
	Id int not null identity(1, 1),
	PageId int not null,
	[Name] nvarchar(128) not null default '',
	[Value] nvarchar(max) not null default '',

	constraint pk_PageContent primary key (Id),
	constraint fk_PageContent_Page foreign key (PageId) references Page (Id) on delete cascade
)

insert into Version (Major, Minor, Revision) values (0, 0, 9);

commit transaction