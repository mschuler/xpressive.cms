using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Commands;
using Cms.Aggregates.Pages.Queries;
using Cms.Aggregates.PageTemplates.Queries;
using Cms.Aggregates.Security;
using Cms.Aggregates.Websites;
using Cms.SharedKernel;
using Cms.WebNew.Authentication;
using Newtonsoft.Json;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class PageController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _uniqueIdProvider;
        private readonly IWebsiteRepository _websiteRepository;
        private readonly IPageRepository _repository;
        private readonly IPageUrlService _pageUrlService;

        public PageController(IUniqueIdProvider uniqueIdProvider, ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IWebsiteRepository websiteRepository, IPageRepository repository, IPageUrlService pageUrlService)
            : base(commandQueue, userRepository, queryHandler)
        {
            _uniqueIdProvider = uniqueIdProvider;
            _websiteRepository = websiteRepository;
            _repository = repository;
            _pageUrlService = pageUrlService;
        }

        [HttpGet, Route("api/pages/{websiteId:ulong}/starred")]
        public IEnumerable<object> Starred(ulong websiteId)
        {
            var tenantId = _websiteRepository.Get(websiteId).TenantId;
            return ExecuteQuery(new GetStarredPages
            {
                WebsiteId = websiteId,
                TenantId = tenantId
            });
        }

        [HttpGet, Route("api/pages/{websiteId:ulong}")]
        public IEnumerable<object> GetPages(ulong websiteId)
        {
            var tenantId = _websiteRepository.Get(websiteId).TenantId;
            return ExecuteQuery(new GetPagesWithChildren
            {
                WebsiteId = websiteId,
                TenantId = tenantId
            });
        }

        [HttpGet, Route("api/pages/templates/{tenantId:ulong}")]
        public IEnumerable<object> GetTemplates(ulong tenantId)
        {
            return ExecuteQuery(new GetAllPageTemplates { TenantId = tenantId });
        }

        [HttpPut, Route("api/pages/{websiteId:ulong}/create")]
        public object Create(ulong websiteId, [FromBody]CreatePageDto dto)
        {
            var id = _uniqueIdProvider.GetId();
            var tenantId = _websiteRepository.Get(websiteId).TenantId;
            var publicId = _repository.GetNextPublicId();
            var permalink = Guid.NewGuid();
            var parentPageId = ulong.Parse(dto.ParentPageId);
            var sortOrder = GetMaxSortOrderForParentPage(websiteId, parentPageId);

            SendCommand(new CreateNewPage
            {
                AggregateId = id,
                WebsiteId = websiteId,
                Permalink = permalink,
                PublicId = publicId,
                Name = dto.Name,
                Title = dto.Name,
                ParentPageId = parentPageId,
                TenantId = tenantId,
            });

            return new
            {
                Id = id.ToJsonString(),
                WebsiteId = websiteId.ToJsonString(),
                Disabled = true,
                Label = dto.Name,
                Fullname = string.Format("{0} (Nr. {1}) [inaktiv]", dto.Name, publicId),
                IsLink = false,
                Permalink = _pageUrlService.GetPermalink(websiteId, id, permalink),
                IsCollapsed = true,
                SortOrder = sortOrder,
                ParentId = dto.ParentPageId,
            };
        }

        [HttpPut, Route("api/pages/{websiteId:ulong}/createlink")]
        public object CreateLink(ulong websiteId, [FromBody]CreatePageLinkDto dto)
        {
            var id = _uniqueIdProvider.GetId();
            var tenantId = _websiteRepository.Get(websiteId).TenantId;
            ulong pageId;
            ulong.TryParse(dto.PageId, out pageId);
            var parentPageId = ulong.Parse(dto.ParentPageId);
            var sortOrder = GetMaxSortOrderForParentPage(websiteId, parentPageId);

            PageBase link;
            _repository.TryGet(pageId, out link);

            SendCommand(new CreatePageLink
            {
                AggregateId = id,
                WebsiteId = websiteId,
                Name = dto.Name,
                PageId = pageId,
                ParentPageId = parentPageId,
                TenantId = tenantId,
                Url = dto.Url,
            });

            return new
            {
                Id = id.ToJsonString(),
                WebsiteId = websiteId.ToJsonString(),
                Name = dto.Name,
                Label = dto.Name,
                IsLink = true,
                Link = new
                {
                    Id = link != null ? link.Id.ToJsonString() : "0",
                    Fullname = link != null ? link.Fullname : "",
                    WebsiteId = link != null ? link.WebsiteId.ToJsonString() : "0",
                    Url = dto.Url,
                },
                IsCollapsed = true,
                SortOrder = sortOrder,
                ParentId = dto.ParentPageId,
            };
        }

        private int GetMaxSortOrderForParentPage(ulong websiteId, ulong parentPageId)
        {
            var siblings = _repository
                .GetAllByWebsiteId(websiteId)
                .Where(p => p.ParentPageId == parentPageId)
                .ToList();

            if (siblings.Any())
            {
                return siblings.Max(p => p.SortOrder);
            }

            return 0;
        }

        [HttpPut, Route("api/page/{pageId:ulong}/link")]
        public object EditLink(ulong pageId, [FromBody]CreatePageLinkDto dto)
        {
            var page = _repository.Get(pageId);
            var tenantId = _websiteRepository.Get(page.WebsiteId).TenantId;
            var linkPageId = ulong.Parse(dto.PageId);

            PageBase link;
            _repository.TryGet(pageId, out link);

            SendCommand(new ChangePageLink
            {
                AggregateId = pageId,
                TenantId = tenantId,
                PageId = linkPageId,
                Name = dto.Name,
                Url = dto.Url,
            });

            return new
            {
                Id = pageId.ToJsonString(),
                WebsiteId = page.WebsiteId.ToJsonString(),
                Name = dto.Name,
                Label = dto.Name,
                IsLink = true,
                Link = new
                {
                    Id = link != null ? link.Id.ToJsonString() : "0",
                    Fullname = link != null ? link.Fullname : "",
                    WebsiteId = link != null ? link.WebsiteId.ToJsonString() : "0",
                    Url = dto.Url,
                },
            };
        }

        [HttpGet, Route("api/page/search/{tenantId:ulong}")]
        public IEnumerable<object> Search(ulong tenantId, [FromUri]string search)
        {
            return ExecuteQuery(new GetPagesBySearchPattern
            {
                SearchPattern = search,
                TenantId = tenantId,
            });
        }

        [HttpPut, Route("api/page/{id:ulong}/star")]
        public IHttpActionResult MarkAsStarred(ulong id)
        {
            var websiteId = _repository.Get(id).WebsiteId;
            var tenantId = _websiteRepository.Get(websiteId).TenantId;
            SendCommand(new StarPage
            {
                AggregateId = id,
                TenantId = tenantId,
            });
            return Ok();
        }

        [HttpPut, Route("api/page/{id:ulong}/unstar")]
        public IHttpActionResult MarkAsUnstarred(ulong id)
        {
            var websiteId = _repository.Get(id).WebsiteId;
            var tenantId = _websiteRepository.Get(websiteId).TenantId;
            SendCommand(new UnstarPage
            {
                AggregateId = id,
                TenantId = tenantId,
            });
            return Ok();
        }

        [HttpPut, Route("api/page/{id:ulong}/enable")]
        public IHttpActionResult Enable(ulong id)
        {
            var websiteId = _repository.Get(id).WebsiteId;
            var tenantId = _websiteRepository.Get(websiteId).TenantId;
            SendCommand(new EnablePage
            {
                AggregateId = id,
                TenantId = tenantId,
            });
            return Ok();
        }

        [HttpPut, Route("api/page/{id:ulong}/disable")]
        public IHttpActionResult Disable(ulong id)
        {
            var websiteId = _repository.Get(id).WebsiteId;
            var tenantId = _websiteRepository.Get(websiteId).TenantId;
            SendCommand(new DisablePage
            {
                AggregateId = id,
                TenantId = tenantId,
            });
            return Ok();
        }

        [HttpPost, Route("api/page/{pageId:ulong}/move/{parentPageId:ulong}/{sortOrder:int}")]
        public IHttpActionResult Move(ulong pageId, ulong parentPageId, int sortOrder)
        {
            var websiteId = _repository.Get(pageId).WebsiteId;
            var tenantId = _websiteRepository.Get(websiteId).TenantId;

            SendCommand(new MovePage
            {
                AggregateId = pageId,
                UserId = User.GetId(),
                ParentPageId = parentPageId,
                TenantId = tenantId,
                SortOrder = sortOrder,
            });

            return Ok();
        }

        [HttpGet, Route("api/page/{id:ulong}")]
        public object GetPage(ulong id)
        {
            var websiteId = _repository.Get(id).WebsiteId;
            var tenantId = _websiteRepository.Get(websiteId).TenantId;
            return ExecuteQuery(new GetPage
            {
                PageId = id,
                TenantId = tenantId
            }).SingleOrDefault();
        }

        [HttpPut, Route("api/page/{id:ulong}/name")]
        public void ChangePageName(ulong id, [FromBody]NameDto dto)
        {
            var websiteId = _repository.Get(id).WebsiteId;
            var tenantId = _websiteRepository.Get(websiteId).TenantId;

            SendCommand(new ChangePageName
            {
                AggregateId = id,
                Name = dto.Name,
                TenantId = tenantId,
            });
        }

        [HttpPut, Route("api/page/{id:ulong}/title")]
        public void ChangePageTitle(ulong id, [FromBody]TitleDto dto)
        {
            var websiteId = _repository.Get(id).WebsiteId;
            var tenantId = _websiteRepository.Get(websiteId).TenantId;

            SendCommand(new ChangePageTitle
            {
                AggregateId = id,
                Title = dto.Title,
                TenantId = tenantId,
            });
        }

        [HttpPut, Route("api/page/{id:ulong}/template/{templateId:ulong}")]
        public IHttpActionResult ChangePageTemplate(ulong id, ulong templateId)
        {
            var websiteId = _repository.Get(id).WebsiteId;
            var tenantId = _websiteRepository.Get(websiteId).TenantId;
            SendCommand(new ChangePageTemplate
            {
                AggregateId = id,
                PageTemplateId = templateId,
                TenantId = tenantId,
            });
            return Ok();
        }

        [HttpPut, Route("api/page/{pageId:ulong}/content")]
        public void ChangePageContent(ulong pageId, [FromBody]PageDetailContentDto content)
        {
            if (content == null)
            {
                return;
            }

            var websiteId = _repository.Get(pageId).WebsiteId;
            var tenantId = _websiteRepository.Get(websiteId).TenantId;

            var json = JsonConvert.SerializeObject(content.Value);
            var type = PageContentValueBase.GetPageContentType(content.Type);
            var value = (PageContentValueBase)JsonConvert.DeserializeObject(json, type);

            SendCommand(new ChangePageContent
            {
                AggregateId = pageId,
                Name = content.Name,
                Value = value,
                TenantId = tenantId,
            });
        }

        [HttpDelete, Route("api/page/{id:ulong}")]
        public void Delete(ulong id)
        {
            var websiteId = _repository.Get(id).WebsiteId;
            var tenantId = _websiteRepository.Get(websiteId).TenantId;
            SendCommand(new DeletePage
            {
                AggregateId = id,
                TenantId = tenantId,
            });
        }

        public class PageDetailContentDto
        {
            public string Name { get; set; }

            public string Type { get; set; }

            public dynamic Value { get; set; }
        }

        public class NameDto
        {
            public string Name { get; set; }
        }

        public class TitleDto
        {
            public string Title { get; set; }
        }

        public class CreatePageDto
        {
            public string ParentPageId { get; set; }
            public string Name { get; set; }
        }

        public class CreatePageLinkDto
        {
            public string PageId { get; set; }
            public string ParentPageId { get; set; }
            public string Name { get; set; }
            public string Url { get; set; }
        }
    }
}