using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Caching;
using System.Text;
using Metrics;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class ServeRequestFromCache : RequestCacheFilterBase
    {
        private readonly Meter _meter = Metric.Meter("ServedFromCache", Unit.Requests);

        public override bool Execute(RenderPageMessage msg)
        {
            var cachedPage = MemoryCache.Default.Get(GetKey(msg)) as CachedPage;

            if (cachedPage != null)
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(cachedPage.Content, Encoding.UTF8);
                response.Content.Headers.ContentType = new MediaTypeHeaderValue(cachedPage.ContentType);
                response.Headers.AddCookies(msg.Cookies);
                msg.Response = response;

                _meter.Mark();

                return false;
            }

            return true;
        }
    }
}