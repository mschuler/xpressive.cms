﻿using System.ComponentModel.DataAnnotations;
using Cms.EventSourcing.Contracts.Commands;

namespace Cms.Aggregates
{
    public abstract class CommandBase : ICommand
    {
        [Required]
        [ValidId]
        public ulong AggregateId { get; set; }

        [Required]
        [ValidId]
        public ulong UserId { get; set; }

        [Required]
        [ValidId]
        public ulong TenantId { get; set; }
    }
}