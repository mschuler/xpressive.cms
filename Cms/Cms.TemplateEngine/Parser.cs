﻿using System.Collections.Generic;
using System.Linq;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Lexer;

namespace Cms.TemplateEngine
{
    public static class Parser
    {
        public static readonly List<char> Quotations = new List<char> { '"', '\'' };

        public static TreeItem Parse(string s)
        {
            var parentToken = ExpressionLexer.GenerateTree(s);

            var treeItem = Convert(parentToken);

            return treeItem;
        }

        private static TreeItem Convert(Token token)
        {
            var treeItem = new TreeItem
            {
                Value = (token.Value ?? string.Empty).Trim(),
                IsBelongingToLeftItem = token.IsBelongingToLeftToken,
            };

            foreach (var child in token.Children)
            {
                var childTreeItem = Convert(child);
                childTreeItem.Parent = treeItem;
                treeItem.Children.Add(childTreeItem);
            }

            if (string.IsNullOrEmpty(treeItem.Value))
            {
                treeItem.ItemType = TreeItemType.Expression;
            }
            else if (treeItem.Children.Count > 0)
            {
                treeItem.ItemType = TreeItemType.Function;

                PlaceMethodChainsInExpressions(treeItem);

                if (treeItem.Children.Count == 1
                    && treeItem.Children[0].Children.Count == 0
                    && string.IsNullOrEmpty(treeItem.Children[0].Value))
                {
                    treeItem.Children.Clear();
                }
            }
            else if (treeItem.Value.IsNumber())
            {
                treeItem.ItemType = TreeItemType.Parameter;
                treeItem.ParameterType = ParameterType.Integer;
                treeItem.Result = int.Parse(treeItem.Value);
            }
            else if (treeItem.Value.IsTextParameter())
            {
                treeItem.ItemType = TreeItemType.Parameter;
                treeItem.ParameterType = ParameterType.String;
                treeItem.Value = treeItem.Value.Trim();
                treeItem.Result = treeItem.Value.Substring(1, treeItem.Value.Length - 2);
            }
            else
            {
                treeItem.ItemType = TreeItemType.Attribute;
            }

            if (treeItem.ItemType == TreeItemType.Expression &&
                treeItem.Children.Count == 1 &&
                treeItem.Children[0].ItemType == TreeItemType.Expression)
            {
                var parent = treeItem.Parent;
                treeItem = treeItem.Children[0];
                treeItem.Parent = parent;
            }

            return treeItem;
        }

        private static void PlaceMethodChainsInExpressions(TreeItem treeItem)
        {
            // page(this()).title should NOT be an expression
            // iif(now().year, 2014, '', '') -> now().year should be an expression

            if (treeItem.Children.Count <= 0 ||
                treeItem.Children.Skip(1).All(c => !c.IsBelongingToLeftItem))
            {
                return;
            }

            for (int i = 0; i < treeItem.Children.Count - 1; i++)
            {
                var child = treeItem.Children[i];

                if (child.RightItem.IsBelongingToLeftItem)
                {
                    var expression = new TreeItem { ItemType = TreeItemType.Expression, Parent = treeItem };

                    treeItem.Children.Insert(i, expression);
                    i++;
                    bool first = true;

                    for (; i < treeItem.Children.Count; )
                    {
                        var chainItem = treeItem.Children[i];
                        if (!chainItem.IsBelongingToLeftItem && !first)
                        {
                            break;
                        }

                        first = false;
                        treeItem.Children.Remove(chainItem);
                        expression.Children.Add(chainItem);
                        chainItem.Parent = expression;
                    }

                    i--;
                }
            }
        }

        private static bool IsTextParameter(this string s)
        {
            return Quotations.Contains(s.First()) && Quotations.Contains(s.Last());
        }
    }
}
