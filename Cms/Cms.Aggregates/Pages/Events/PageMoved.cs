namespace Cms.Aggregates.Pages.Events
{
    public class PageMoved : EventBase<PageBase>
    {
        public ulong ParentPageId { get; set; }

        public int SortOrder { get; set; }
    }
}