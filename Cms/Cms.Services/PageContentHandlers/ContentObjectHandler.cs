﻿using System;
using System.Linq;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.Pages;
using Cms.Services.Contracts;
using Cms.TemplateEngine;

namespace Cms.Services.PageContentHandlers
{
    internal class ContentObjectHandler : IContentHandler
    {
        private readonly IDynamicObjectRepository _repository;
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;

        public ContentObjectHandler(IDynamicObjectRepository repository, IDynamicObjectDefinitionRepository definitionRepository)
        {
            _repository = repository;
            _definitionRepository = definitionRepository;
        }

        public bool CanHandle(string pageContentType)
        {
            return pageContentType.Equals("object", StringComparison.OrdinalIgnoreCase);
        }

        public string ContentType
        {
            get { return "Object"; }
        }

        public string GetImage()
        {
            return "ti-ticket";
        }

        public string Render(PageContentValueBase value)
        {
            var objectValue = value as PageContentObjectValue;

            if (objectValue == null)
            {
                return string.Empty;
            }

            ulong objectId;
            ulong definitionId;
            DynamicObject obj;
            DynamicObjectDefinition definition;

            if (!ulong.TryParse(objectValue.ObjectId, out objectId) ||
                !ulong.TryParse(objectValue.DefinitionId, out definitionId) ||
                !_repository.TryGet(objectId, out obj) ||
                !_definitionRepository.TryGet(definitionId, out definition))
            {
                return string.Empty;
            }

            var template = definition.Templates.FirstOrDefault(t => t.Name.Equals(objectValue.TemplateName, StringComparison.OrdinalIgnoreCase));

            if (template == null)
            {
                return string.Empty;
            }

            var engine = new Engine();
            using (TemplateStateMap.Set(TemplateStateMap.CurrentObjectKey, obj))
            {
                var result = engine.Generate(template.Template);
                return result;
            }
        }

        public object GetValueForInterpreter(PageContentValueBase value)
        {
            var objectValue = value as PageContentObjectValue;

            if (objectValue == null)
            {
                return null;
            }

            ulong objectId;
            DynamicObject obj;

            if (!ulong.TryParse(objectValue.ObjectId, out objectId) ||
                !_repository.TryGet(objectId, out obj))
            {
                return null;
            }

            return obj;
        }
    }
}