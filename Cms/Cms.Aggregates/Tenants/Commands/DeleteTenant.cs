using Cms.Aggregates.Security;

namespace Cms.Aggregates.Tenants.Commands
{
    [RequiredPermission("tenant", PermissionLevel.CanEverything)]
    public class DeleteTenant : CommandBase { }
}