﻿using Cms.Aggregates.Tenants.Events;

namespace Cms.Aggregates.Security.Events
{
    public class GroupEventHandler : EventHandlerBase
    {
        private readonly IGroupRepository _groupRepository;
        private readonly IUserRepository _userRepository;

        public GroupEventHandler(IGroupRepository groupRepository, IUserRepository userRepository)
        {
            _groupRepository = groupRepository;
            _userRepository = userRepository;
        }

        public void When(GroupCreated @event)
        {
            var group = new Group
            {
                Id = @event.AggregateId,
                TenantId = @event.TenantId,
                Name = @event.Name,
            };
            _groupRepository.Add(group);
        }

        public void When(GroupNameChanged @event)
        {
            var group = _groupRepository.Get(@event.AggregateId);
            group.Name = @event.Name;
        }

        public void When(GroupDeleted @event)
        {
            _groupRepository.Delete(@event.AggregateId);

            foreach (var user in _userRepository.GetAll())
            {
                user.RemoveGroupId(@event.AggregateId);
            }
        }

        public void When(PermissionChanged @event)
        {
            var group = _groupRepository.Get(@event.AggregateId);
            group.SetPermission(@event.ModuleId, @event.Permission);
        }

        public void When(GroupMemberAdded @event)
        {
            var group = _groupRepository.Get(@event.AggregateId);
            var user = _userRepository.Get(@event.MemberId);
            group.AddMember(user);
            user.AddGroupId(group.Id);
        }

        public void When(GroupMemberRemoved @event)
        {
            var group = _groupRepository.Get(@event.AggregateId);
            var user = _userRepository.GetEvenIfDeleted(@event.MemberId);
            group.RemoveMember(@event.MemberId);
            user.RemoveGroupId(group.Id);
        }

        public void When(TenantDeleted @event)
        {
            _groupRepository.DeleteAllByTenantId(@event.AggregateId);
        }
    }
}