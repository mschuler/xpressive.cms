﻿using System.Collections.Generic;

namespace Cms.TemplateEngine.Lexer
{
    internal class Token
    {
        public Token()
        {
            Children = new List<Token>();
        }

        public string Value { get; set; }
        public List<Token> Children { get; set; }
        public Token Parent { get; set; }
        public bool IsBelongingToLeftToken { get; set; }
    }
}
