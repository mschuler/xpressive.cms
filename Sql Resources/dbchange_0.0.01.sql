
begin transaction

create table Version (
	Id int identity(1,1) not null,
	Major int not null default 0,
	Minor int not null default 0,
	Revision int not null default 0,

	constraint PK_Version primary key(Id),
	constraint UX_Version unique (Major, Minor, Revision)
)

create table Configuration (
	[Key] nvarchar(128) not null,
	[Value] nvarchar(256) not null,

	constraint PK_Configuration primary key ([Key])
)

create table Directory (
	Id int not null identity (1, 1),
	[Name] nvarchar(128) not null,
	[Type] nvarchar(16) not null,
	ParentId int not null,

	constraint PK_Directory primary key (Id)
)

create table [File] (
	Id int not null identity(1, 1),
	Guid uniqueidentifier not null,
	[Name] nvarchar(128) not null,
	ContentType nvarchar(64) not null,
	DirectoryId int not null,
	Content varbinary(max) not null,
	Length bigint not null,

	constraint PK_File primary key (Id),
	constraint FK_File_Directory foreign key (DirectoryId) references Directory (Id) on delete cascade
)

create table Form (
	Id int not null identity(1, 1),
	[Name] nvarchar(256) not null,

	constraint PK_Form primary key (Id)
)

create table FormEntry (
	Id int not null identity(1, 1),
	[Timestamp] datetime not null,
	IpAddress nvarchar(16) not null,
	FormName nvarchar(256) not null,

	constraint PK_FormEntry primary key (Id)
)

create table FormField (
	Id int not null identity(1, 1),
	FormId int not null,
	[Name] nvarchar(256) not null,
	Options nvarchar(256) not null,
	IsMandatory bit not null,
	[Type] int not null,

	constraint PK_FormField primary key (Id),
	constraint FK_FormField_Form foreign key (FormId) references Form (Id) on delete cascade
)

create table FormFieldEntry (
	Id int not null identity(1, 1),
	[Value] nvarchar(max) not null,
	FormEntryId int not null,
	[Type] int not null,
	[Name] nvarchar(256) not null,

	constraint PK_FormFieldEntry primary key (Id),
	constraint FK_FormFieldEntry_FormEntry foreign key (FormEntryId) references FormEntry (Id) on delete cascade
)

create table Navigation (
	Id int not null identity(1, 1),
	[Name] nvarchar(256) not null,
	Hyperlink nvarchar(1024) not null,
	ParentId int null,
	TargetPageId int not null,
	Sort int not null,

	constraint PK_Navigation primary key (Id),
	constraint FK_Navigation_Navigation foreign key (ParentId) references Navigation (Id) on delete no action
)

create table Page (
	Id int not null identity(1, 1),
	PermalinkId uniqueidentifier not null,
	ParentId int not null,
	MasterPage nvarchar(128) not null,
	Title nvarchar(256) not null,
	IsPublished bit not null,
	Domain nvarchar(256) not null,
	IsDefault bit not null,

	constraint PK_Page primary key (Id),
	constraint FK_Page_Page foreign key (ParentId) references Page (Id) on delete no action
)

create table PageProperty (
	Id int not null identity(1, 1),
	PageId int not null,
	[Key] nvarchar(128) not null,
	[Value] nvarchar(256) not null,

	constraint PK_PageProperty primary key (Id),
	constraint FK_PageProperty_Page foreign key (PageId) references Page (Id) on delete cascade
)

create table Template (
	Id int not null identity(1, 1),
	[Name] nvarchar(256) not null,

	constraint PK_Template primary key (Id),
	constraint UX_Template unique ([Name])
)

create table TemplateVersion (
	Id int not null identity(1, 1),
	Version datetime not null,
	[Content] nvarchar(max) not null,
	TemplateId int not null,

	constraint PK_TemplateVersion primary key (Id),
	constraint FK_TemplateVersion_Template foreign key (TemplateId) references Template (Id)
)

create table Article (
	Id int not null identity(1, 1),
	[Name] nvarchar(256) not null,
	DirectoryId int not null,
	TemplateVersionId int not null,
	RenderedContent nvarchar(max) not null,

	constraint PK_Article primary key (Id),
	constraint FK_Article_TemplateVersion foreign key (TemplateVersionId) references TemplateVersion (Id)
)

create table ArticleTemplateField (
	ID int not null identity(1, 1),
	PlaceholderName nvarchar(256) not null,
	PlaceholderType nvarchar(32) not null,
	[Value] nvarchar(max) not null,
	ArticleId int not null,

	constraint PK_ArticleTemplateField primary key (Id),
	constraint FK_ArticleTemplateField_Article foreign key (ArticleId) references Article (Id) on delete cascade
)

insert into Version (Major, Minor, Revision) values (0, 0, 1);

commit transaction