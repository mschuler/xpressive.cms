using System;
using System.Data;
using System.Linq;
using Cms.Services.Contracts;
using NPoco;

namespace Cms.Services.RecurrentJobs
{
    internal class PageTrackingMonthlyGroupingJob : IRecurrentJob
    {
        public string CronTab { get { return "0 0 3 1 * ?"; } }

        public void Execute()
        {
            var today = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            var maxDate = today.AddMonths(-1);
            var minDate = today.AddMonths(-2);

            using (var database = new Database("CmsDatabaseConnection"))
            {
                database.BeginTransaction(IsolationLevel.ReadCommitted);

                try
                {
                    var entries = database.Query<PageTrackingDto>().Where(d => d.Date < maxDate && d.Date >= minDate).ToList();
                    var groups = entries.GroupBy(e => Tuple.Create(e.SessionId, e.PersistedId, e.PageId, e.WebsiteId, e.Country)).ToList();

                    var date = minDate.AddHours(12);

                    foreach (var entry in entries)
                    {
                        Delete(database, entry);
                    }

                    foreach (var group in groups)
                    {
                        var dtos = group.ToList();
                        var sum = dtos.Sum(d => d.Count);

                        database.Insert(new PageTrackingDto
                        {
                            Date = date,
                            SessionId = group.Key.Item1,
                            PersistedId = group.Key.Item2,
                            PageId = group.Key.Item3,
                            WebsiteId = group.Key.Item4,
                            Country = group.Key.Item5,
                            Count = sum,
                        });
                    }

                    database.CompleteTransaction();
                }
                catch (Exception)
                {
                    database.AbortTransaction();
                    throw;
                }
            }
        }

        private void Delete(Database database, PageTrackingDto dto)
        {
            database.Execute(
                "delete from PageTracking where Date=@0 and SessionId=@1 and PersistedId=@6 and PageId=@2 and WebsiteId=@3 and Country=@4 and Count=@5",
                dto.Date,
                dto.SessionId,
                dto.PageId,
                dto.WebsiteId,
                dto.Country,
                dto.Count,
                dto.PersistedId);
        }

        [TableName("PageTracking")]
        [PrimaryKey("Date", AutoIncrement = false)]
        private class PageTrackingDto
        {
            public DateTime Date { get; set; }
            public string SessionId { get; set; }
            public string PersistedId { get; set; }
            public long PageId { get; set; }
            public long WebsiteId { get; set; }
            public string Country { get; set; }
            public int Count { get; set; }
        }
    }
}