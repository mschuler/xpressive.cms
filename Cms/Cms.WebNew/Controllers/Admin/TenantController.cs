using System.Collections.Generic;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Security;
using Cms.Aggregates.Tenants.Commands;
using Cms.Aggregates.Tenants.Queries;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class TenantController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _uniqueIdProvider;

        public TenantController(IUniqueIdProvider uniqueIdProvider, ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler)
            : base(commandQueue, userRepository, queryHandler)
        {
            _uniqueIdProvider = uniqueIdProvider;
        }

        [HttpGet, Route("api/tenants")]
        public IEnumerable<object> GetAll()
        {
            return ExecuteQuery(new GetTenants());
        }

        [HttpPut, Route("api/tenant")]
        public object Create([FromBody]TenantNameDto dto)
        {
            var id = _uniqueIdProvider.GetId();
            SendCommand(new CreateTenant
            {
                AggregateId = id,
                Name = dto.Name,
                TenantId = id
            });
            return new { Id = id.ToJsonString(), dto.Name };
        }

        [HttpPost, Route("api/tenant/{tenantId:ulong}")]
        public void Rename(ulong tenantId, [FromBody]TenantNameDto dto)
        {
            SendCommand(new ChangeTenantName
            {
                AggregateId = tenantId,
                Name = dto.Name,
                TenantId = tenantId
            });
        }

        [HttpDelete, Route("api/tenant/{tenantId:ulong}")]
        public void Delete(ulong tenantId)
        {
            SendCommand(new DeleteTenant
            {
                AggregateId = tenantId,
                TenantId = tenantId
            });
        }

        public class TenantNameDto
        {
            public string Name { get; set; }
        }
    }
}