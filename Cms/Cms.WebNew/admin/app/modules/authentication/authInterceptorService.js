﻿(function() {
    angular.module('app').factory('authInterceptorService', ["$q", "$injector", "$location", "localStorageService", "$log", function ($q, $injector, $location, localStorageService, $log) {
        var authInterceptorServiceFactory = {};
        var $http;

        var _request = function (config) {

            config.headers = config.headers || {};

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        }

        var _responseError = function (rejection) {
            var deferred = $q.defer();
            if (rejection.status === 401) {
                var authService = $injector.get('authService');
                authService.refreshToken().then(function (response) {
                    _retryHttpRequest(rejection.config, deferred);
                }, function () {
                    authService.logout();
                    var path = $location.path();
                    if (path != "/pages/lock-screen") {
                        $location.path('/login');
                    }
                    deferred.reject(rejection);
                });
            } else if (rejection.status === 500) {
                var path = "/500";
                if (typeof (rejection.data) === "string") {
                    var isGuid = /^[0-9a-f\-]+$/i;
                    if (isGuid.test(rejection.data)) {
                        path += "/" + rejection.data;
                    }
                }
                $location.path(path);
                deferred.reject(rejection);
            } else {
                deferred.reject(rejection);
            }
            return deferred.promise;
        }

        var _retryHttpRequest = function (config, deferred) {
            $http = $http || $injector.get('$http');
            $http(config).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });
        }

        authInterceptorServiceFactory.request = _request;
        authInterceptorServiceFactory.responseError = _responseError;

        return authInterceptorServiceFactory;
    }]);
}());