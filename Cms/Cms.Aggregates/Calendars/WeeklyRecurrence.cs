using System;

namespace Cms.Aggregates.Calendars
{
    public class WeeklyRecurrence : AppointmentRecurrence
    {
        private int _everyXthWeek;

        public DateTime? EndDate { get; set; }
        public bool IsOnMonday { get; set; }
        public bool IsOnTuesday { get; set; }
        public bool IsOnWednesday { get; set; }
        public bool IsOnThursday { get; set; }
        public bool IsOnFriday { get; set; }
        public bool IsOnSaturday { get; set; }
        public bool IsOnSunday { get; set; }

        public int EveryXthWeek
        {
            get { return Math.Max(_everyXthWeek, 1); }
            set { _everyXthWeek = value; }
        }

        public bool HasEndDate
        {
            get { return EndDate.HasValue; }
            set
            {
                if (!value)
                {
                    EndDate = null;
                }
            }
        }

        public override bool IsMatch(Appointment appointment, DateTime dateTime)
        {
            if (dateTime < appointment.StartDate.Date)
            {
                return false;
            }

            if (EndDate.HasValue && dateTime > EndDate.Value)
            {
                return false;
            }

            if (dateTime.DayOfWeek == DayOfWeek.Monday && !IsOnMonday) { return false; }
            if (dateTime.DayOfWeek == DayOfWeek.Tuesday && !IsOnTuesday) { return false; }
            if (dateTime.DayOfWeek == DayOfWeek.Wednesday && !IsOnWednesday) { return false; }
            if (dateTime.DayOfWeek == DayOfWeek.Thursday && !IsOnThursday) { return false; }
            if (dateTime.DayOfWeek == DayOfWeek.Friday && !IsOnFriday) { return false; }
            if (dateTime.DayOfWeek == DayOfWeek.Saturday && !IsOnSaturday) { return false; }
            if (dateTime.DayOfWeek == DayOfWeek.Sunday && !IsOnSunday) { return false; }

            if (EveryXthWeek <= 1) { return true; }

            var totalWeeks = (int)((dateTime.Date - appointment.StartDate.Date).TotalDays / 7);
            return totalWeeks % EveryXthWeek == 0;
        }
    }
}