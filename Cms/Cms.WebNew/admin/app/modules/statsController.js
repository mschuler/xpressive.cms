﻿(function() {
    angular.module("app").controller("StatsController", ["$rootScope", "$scope", "$http", "tenantService", "$log", "currentUser", "$timeout", function ($rootScope, $scope, $http, tenantService, $log, currentUser, $timeout) {
        var timer;
        var gaugeCpu;
        var gaugeDisc;
        var gaugeRam;
        var gaugeCache;

        $scope.monthOptions = {
            responsive: true,
            maintainAspectRatio: true
        };

        function getMetrics(t) {
            timer = $timeout(function() {
                $http.get("/metrics/json", { cache: false }).success(function (data) {
                    if (data.Timers.PageRenderingRequests) {
                        var h = data.Timers.PageRenderingRequests.Histogram;
                        $scope.metricData[0].push(h.Percentile95);
                        $scope.metricData[1].push(h.Percentile75);
                        $scope.metricData[2].push(h.Median);
                        $scope.metricLabels.push(new Date(data.Timestamp).toLocaleTimeString());
                    }
                    if (data.Units.Timers.PageRenderingRequests) {
                        var d = data.Units.Timers.PageRenderingRequests.Duration;
                        $scope.metricSeries = [
                            "95% [" + d + "]",
                            "75% [" + d + "]",
                            "Median [" + d + "]"
                        ];
                    }
                    if (data.Meters && data.Meters.ServedFromCache) {
                        if (!gaugeCache) {
                            gaugeCache = new JustGage({
                                id: "gaugeCache",
                                value: 0,
                                min: 0,
                                max: 100,
                                title: "Cache",
                                label: "%",
                                levelColorsGradient: false
                            });
                        }

                        if (data.Timers && data.Timers.PageRenderingRequests) {
                            var max = data.Timers.PageRenderingRequests.Rate.OneMinuteRate * 100;

                            if (max <= 0) {
                                gaugeCache.refresh(0);
                            } else {
                                var current = data.Meters.ServedFromCache.OneMinuteRate * 100;
                                var percent = ((current / max) * 100).toFixed(0);
                                gaugeCache.refresh(percent);
                            }
                        }
                    }
                    if (data.Gauges) {
                        var cpuUsed = data.Gauges["[System] CPU Usage"];
                        var cpuAlternative = data.Gauges["[Alternative] CPU Usage"];

                        var diskFree = data.Gauges["[System] Physical Disk Free Space"];
                        var diskTotal = data.Gauges["[System] Physical Disk Total Space"];
                        var diskUsed = diskTotal - diskFree;

                        var ramFree = data.Gauges["[System] Available RAM"];
                        var ramTotal = data.Gauges["[System] Total RAM"];
                        var ramUsed = ramTotal - ramFree;

                        if (!gaugeDisc) {
                            gaugeDisc = new JustGage({
                                id: "gaugeDisc",
                                value: 0,
                                min: 0,
                                max: (diskTotal / 1024).toFixed(0),
                                title: "Disc",
                                label: "GB",
                                levelColorsGradient: false
                            });
                        }

                        if (!gaugeCpu) {
                            gaugeCpu = new JustGage({
                                id: "gaugeCpu",
                                value: 0,
                                min: 0,
                                max: 100,
                                title: "CPU",
                                label: "%",
                                levelColorsGradient: false
                            });
                        }

                        if (!gaugeRam) {
                            gaugeRam = new JustGage({
                                id: "gaugeRam",
                                value: 0,
                                min: 0,
                                max: (ramTotal / 1024).toFixed(0),
                                title: "RAM",
                                label: "GB",
                                levelColorsGradient: false
                            });
                        }

                        if ((!cpuUsed || cpuUsed <= 0) && cpuAlternative) {
                            cpuUsed = cpuAlternative;
                        }

                        gaugeDisc.refresh((diskUsed / 1024).toFixed(1));
                        gaugeRam.refresh((ramUsed / 1024).toFixed(1));
                        gaugeCpu.refresh(cpuUsed.toFixed(1));
                    }

                    while ($scope.metricData[0].length > 30) {
                        $scope.metricData[0] = $scope.metricData[0].slice(1);
                        $scope.metricData[1] = $scope.metricData[1].slice(1);
                        $scope.metricData[2] = $scope.metricData[2].slice(1);
                    }
                    while ($scope.metricLabels.length > 30) {
                        $scope.metricLabels = $scope.metricLabels.slice(1);
                    }

                    getMetrics(5000);
                });
            }, t);
        };

        function updateMetrics() {
            if (!currentUser) {
                $scope.showMetrics = false;
                return;
            }

            $scope.showMetrics = currentUser.isSysadmin ? true : false;

            if (currentUser.isSysadmin) {
                $scope.metricData = [[], [], []];
                $scope.metricLabels = [];
                $scope.metricSeries = [
                    "95%",
                    "75%",
                    "median"
                ];

                $scope.options = {
                    animation: false,
                };

                getMetrics(0);
            }
        };

        function getStats() {
            var tenant = tenantService.getTenant();

            $scope.monthData = [];
            $scope.monthLabels = [];
            $scope.monthSeries = [];
            $scope.dayData = [];
            $scope.dayLabels = [];
            $scope.daySeries = [];
            $scope.returningUsers = 0;
            $scope.totalPageViews = 0;
            $scope.totalSessions = 0;
            $scope.viewsPerVisit = 0;

            if (!tenant || !tenant.id) {
                return;
            }

            var url = String.format("/api/stats/{0}", tenant.id);
            $http.get(url, { cache: false }).success(function (data) {
                $scope.data = data;

                var isFirst = true;
                _.each(data, function (pw) {
                    $scope.monthSeries.push(pw.websiteName);
                    $scope.daySeries.push(pw.websiteName);

                    $scope.returningUsers += pw.returningUsers;
                    $scope.totalPageViews += pw.totalPageViews;
                    $scope.totalSessions += pw.totalSessions;
                    $scope.viewsPerVisit += pw.viewsPerVisit;

                    if (isFirst) {
                        isFirst = false;

                        _.each(pw.month, function(m) {
                            $scope.monthLabels.push(m.label);
                        });
                        _.each(pw.day, function(d) {
                            $scope.dayLabels.push(d.label);
                        });
                    }

                    var mv = [];
                    _.each(pw.month, function(m) {
                        mv.push(m.value);
                    });
                    $scope.monthData.push(mv);

                    var dv = [];
                    _.each(pw.day, function(d) {
                        dv.push(d.value);
                    });
                    $scope.dayData.push(dv);

                    var pwPageCountries = {};
                    _.each(pw.pageCountries, function(c) {
                        pwPageCountries[c.label] = c.value;
                    });

                    var pwFileCountries = {};
                    _.each(pw.fileCountries, function(c) {
                        pwFileCountries[c.label] = c.value;
                    });

                    pw.pageMap = {
                        map: "world_mill_en",
                        zoomOnScroll: false,
                        backgroundColor: null,
                        regionStyle: {
                            initial: { fill: "#EEEFF3" }
                        },
                        series: {
                            regions: [{
                                values: pwPageCountries,
                                attribute: 'fill',
                                scale: ['#C8EEFF', '#0071A4'],
                                normalizeFunction: 'polynomial'
                            }]
                        },
                        onRegionTipShow: function (e, el, code) {
                            var count = 0;
                            if (pwPageCountries[code]) {
                                count = pwPageCountries[code];
                            }
                            el.html(el.html() + ': ' + count);
                        }
                    };

                    pw.fileMap = {
                        map: "world_mill_en",
                        zoomOnScroll: false,
                        backgroundColor: null,
                        regionStyle: {
                            initial: { fill: "#EEEFF3" }
                        },
                        series: {
                            regions: [{
                                values: pwFileCountries,
                                scale: ['#C8EEFF', '#0071A4'],
                                normalizeFunction: 'polynomial'
                            }]
                        },
                        onRegionTipShow: function (e, el, code) {
                            var count = 0;
                            if (pwFileCountries[code]) {
                                count = pwFileCountries[code];
                            }
                            el.html(el.html() + ': ' + count);
                        }
                    };
                });
            }).error(function(error) {
                $log.error(error);
            });
        };

        updateMetrics();

        $rootScope.$on("tenantChanged", getStats);

        $rootScope.$on("currentUserChanged", updateMetrics);

        $scope.$on("$destroy", function () {
            if (timer) {
                $timeout.cancel(timer);
            }
        });

        getStats();
    }]);
}());