using System;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectDefinitionDeletedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectDefinitionDeletedEventTranslator(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectDefinitionDeleted);
        }

        public string GetHeader(IEvent @event)
        {
            return "Objektdefinition gel�scht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectDefinitionDeleted)@event;
            return string.Format("Die Objektdefinition <strong>{0} (Nr. {1})</strong> wurde gel�scht.", e.Name, e.PublicId);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objectdefinition";
        }
    }
}