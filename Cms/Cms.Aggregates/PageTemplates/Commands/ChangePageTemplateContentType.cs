using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.PageTemplates.Commands
{
    [RequiredPermission("pagetemplate", PermissionLevel.CanChange)]
    public class ChangePageTemplateContentType : CommandBase
    {
        [Required(AllowEmptyStrings = true)]
        public string ContentType { get; set; }
    }
}