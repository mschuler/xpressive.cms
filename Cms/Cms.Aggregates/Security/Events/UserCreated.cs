﻿namespace Cms.Aggregates.Security.Events
{
    public class UserCreated : EventBase<User>
    {
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsSysadmin { get; set; }
        public string Password { get; set; }
    }
}