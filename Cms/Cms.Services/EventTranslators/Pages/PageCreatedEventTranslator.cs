using System;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.Websites;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Pages
{
    internal class PageCreatedEventTranslator : IEventTranslator
    {
        private readonly IWebsiteRepository _websiteRepository;

        public PageCreatedEventTranslator(IWebsiteRepository websiteRepository)
        {
            _websiteRepository = websiteRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Neue Seite erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageCreated)@event;
            return string.Format("Die Seite <strong>{0} (Nr. {1})</strong> wurde neu erstellt.", e.Name, e.PublicId);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var e = (PageCreated)@event;
            return _websiteRepository.GetEvenIfDeleted(e.WebsiteId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "page";
        }
    }
}