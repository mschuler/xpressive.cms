namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("sysadmin", PermissionLevel.CanEverything)]
    public class ChangeUserSysadmin : CommandBase
    {
        public bool IsSysadmin { get; set; }
    }
}