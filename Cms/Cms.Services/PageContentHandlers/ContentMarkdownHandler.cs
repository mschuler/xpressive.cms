﻿using System;
using Cms.Aggregates.Pages;
using Cms.Services.Contracts;
using Cms.TemplateEngine;

namespace Cms.Services.PageContentHandlers
{
    internal class ContentMarkdownHandler : IContentHandler
    {
        public bool CanHandle(string pageContentType)
        {
            return pageContentType.Equals("markdown", StringComparison.OrdinalIgnoreCase);
        }

        public string ContentType
        {
            get { return "Markdown"; }
        }

        public string GetImage()
        {
            return "fa fa-medium";
        }

        public string Render(PageContentValueBase value)
        {
            var markdown = value as PageContentMarkdownValue;

            if (markdown == null || string.IsNullOrEmpty(markdown.Value))
            {
                return string.Empty;
            }

            var engine = new Engine();
            var result = engine.Generate(markdown.Value);

            var md = new MarkdownDeep.Markdown();
            //md.SafeMode = true;
            //md.ExtraMode = true;

            result = md.Transform(result);
            return result;
        }

        public object GetValueForInterpreter(PageContentValueBase value)
        {
            var markdown = value as PageContentMarkdownValue;

            if (markdown == null || string.IsNullOrEmpty(markdown.Value))
            {
                return string.Empty;
            }

            return markdown.Value;
        }
    }
}