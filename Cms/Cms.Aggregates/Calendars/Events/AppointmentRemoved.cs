namespace Cms.Aggregates.Calendars.Events
{
    public class AppointmentRemoved : EventBase<Calendar>
    {
        public ulong AppointmentId { get; set; }
    }
}