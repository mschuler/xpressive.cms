using System;
using System.Collections.Generic;
using System.Linq;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    public sealed class ListCountInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "count"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Attribute &&
                tree.Value.Equals("count", StringComparison.OrdinalIgnoreCase) &&
                tree.LeftItem != null &&
                tree.LeftItem.Result is IEnumerable<object> &&
                tree.Children.Count == 0)
            {
                tree.Result = ((IEnumerable<object>)tree.LeftItem.Result).Count();
            }
        }
    }
}