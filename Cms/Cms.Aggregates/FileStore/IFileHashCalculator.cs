using System.IO;

namespace Cms.Aggregates.FileStore
{
    public interface IFileHashCalculator
    {
        byte[] Sha1(FileInfo file);

        byte[] Sha256(FileInfo file);

        string Sha1AsBase64String(FileInfo file);

        string Sha256AsHexString(FileInfo file);

        string Sha256AsBase64String(FileInfo file);
    }
}