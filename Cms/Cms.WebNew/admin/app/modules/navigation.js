﻿(function() {
    angular.module("app").controller("NavigationController", ["$rootScope", "$scope", "$log", "$http", "tenantService", function ($rootScope, $scope, $log, $http, tenantService) {

        $scope.visibility = {};

        function getNavigation() {
            var tenant = tenantService.getTenant();
            if (!tenant || !tenant.id) {
                return;
            }

            $http.get("/api/navigation/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
                var visibility = angular.fromJson(data);
                $scope.visibility = visibility;
            });
        };

        $rootScope.$on("logout", function () { $scope.navigations = []; });

        $rootScope.$on("tenantChanged", getNavigation);

        getNavigation();
    }]);
}());