﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Cms.Services.Contracts;
using log4net;

namespace Cms.Services.UrlShortening
{
    internal abstract class UrlShorteningServiceBase : IUrlShorteningService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(UrlShorteningServiceBase));

        private static readonly Dictionary<string, object> _locks = new Dictionary<string, object>();
        private static readonly object _locksLock = new object();
        private static readonly ConcurrentDictionary<string, string> _shortenedUrls = new ConcurrentDictionary<string, string>(StringComparer.Ordinal);

        private readonly Dictionary<string, string> _configuration = new Dictionary<string, string>(StringComparer.Ordinal);
        private readonly IUrlShorteningRepository _urlShorteningRepository;

        protected UrlShorteningServiceBase(IEnumerable<string> keys, IUrlShorteningRepository urlShorteningRepository)
        {
            foreach (var key in keys)
            {
                _configuration.Add(key, string.Empty);
            }

            _urlShorteningRepository = urlShorteningRepository;

            if (_shortenedUrls.IsEmpty)
            {
                var persistedUrls = urlShorteningRepository.GetAll();
                foreach (var persistedUrl in persistedUrls)
                {
                    _shortenedUrls.TryAdd(persistedUrl.Item1, persistedUrl.Item2);
                }
            }
        }

        public abstract string Name { get; }

        public IEnumerable<string> GetConfigurationKeys()
        {
            return _configuration.Keys.ToList();
        }

        public void SetConfigurationValue(string key, string value)
        {
            if (!_configuration.ContainsKey(key))
            {
                throw new ArgumentException("This configuration key does not exist.", "key");
            }

            _configuration[key] = value;
        }

        public bool VerifyConfiguration()
        {
            var result = ShortenInternal("https://www.google.com/");
            return !string.IsNullOrEmpty(result);
        }

        public string ShortenUrl(string longUrl, bool useCache)
        {
            try
            {
                if (!useCache)
                {
                    return ShortenInternal(longUrl);
                }

                string value;
                if (_shortenedUrls.TryGetValue(longUrl, out value))
                {
                    return value;
                }

                var @lock = GetLock(longUrl);

                lock (@lock)
                {
                    value = _shortenedUrls.GetOrAdd(longUrl, ShortenInternal);

                    if (!string.Equals(longUrl, value, StringComparison.Ordinal))
                    {
                        _urlShorteningRepository.Add(longUrl, value);
                    }

                    _locks.Remove(longUrl);
                    return value;
                }
            }
            catch (Exception e)
            {
                _log.Error("Unable to shorten url: " + e.Message, e);
            }

            return longUrl;
        }

        protected bool TryGetConfigurationValue(string key, out string value)
        {
            return _configuration.TryGetValue(key, out value);
        }

        protected abstract string ShortenInternal(string longUrl);

        private object GetLock(string longUrl)
        {
            lock (_locksLock)
            {
                object @lock;
                if (!_locks.TryGetValue(longUrl, out @lock))
                {
                    @lock = new object();
                    _locks.Add(longUrl, @lock);
                }
                return @lock;
            }
        }
    }
}