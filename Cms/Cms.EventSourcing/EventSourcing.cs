﻿using System;
using System.Collections.Generic;
using Cms.EventSourcing.Contracts.Commands;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;
using log4net;
using UniqueIdGenerator.Net;

namespace Cms.EventSourcing
{
    public sealed class EventSourcing
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(EventSourcing));
        private static readonly Generator _versionGenerator = new Generator(1, new DateTime(2014, 10, 1));

        private static Func<ICollection<ICommandHandler>> _resolveCommandHandlers;
        private static Func<ICollection<IEventHandler>> _resolveEventHandlers;

        public static void SetCommandHandlerResolver(Func<ICollection<ICommandHandler>> resolver)
        {
            _resolveCommandHandlers = resolver;
        }

        public static void SetEventHandlerResolver(Func<ICollection<IEventHandler>> resolver)
        {
            _resolveEventHandlers = resolver;
        }

        private readonly IEventStore _eventStore;

        public EventSourcing(IEventStore eventStore)
        {
            _eventStore = eventStore;
        }

        public void Invoke(ICommand command)
        {
            CommandValidator.AssertIsValid(command);

            var commandHandlers = _resolveCommandHandlers();
            var eventHandlers = _resolveEventHandlers();
            var events = new List<IEvent>();

            foreach (var commandHandler in commandHandlers)
            {
                commandHandler.Handle(command, events.Add);
            }

            foreach (var @event in events)
            {
                @event.Version = _versionGenerator.NextLong();
            }

            _eventStore.Persist(events);

            EventDispatcher.Handle(events, eventHandlers);
        }

        public void RestoreModelFromEventStore()
        {
            var numberOfEvents = 0;
            Func<string> message = () => string.Format("Restore {0} events from event store", numberOfEvents);

            using (PerformanceMeasurer.Get(message, o => _log.Info(o)))
            {
                var events = _eventStore.Fetch();
                var eventHandlers = _resolveEventHandlers();
                EventDispatcher.Handle(events, eventHandlers, waitUntilQueueIsEmpty: true);
                numberOfEvents = events.Count;
            }
        }

        public static void RestoreModelFromEvents(IEnumerable<IEvent> events)
        {
            var eventHandlers = _resolveEventHandlers();

            foreach (var @event in events)
            {
                foreach (var eventHandler in eventHandlers)
                {
                    eventHandler.Handle(@event);
                }
            }
        }
    }
}
