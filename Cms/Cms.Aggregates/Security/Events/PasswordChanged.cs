﻿namespace Cms.Aggregates.Security.Events
{
    public class PasswordChanged : EventBase<User>
    {
        public string Password { get; set; }
    }
}