using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0009_update_page_aggregate_types : DatabaseMigrationBase
    {
        public M0009_update_page_aggregate_types() : base(9) { }

        public override void Execute(Database database)
        {
            database.Execute("update [Event] set AggregateType = 'PageBase' where AggregateType = 'Page'");
        }
    }
}