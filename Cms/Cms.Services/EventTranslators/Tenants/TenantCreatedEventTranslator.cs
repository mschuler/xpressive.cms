using System;
using Cms.Aggregates.Tenants.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Tenants
{
    internal class TenantCreatedEventTranslator : IEventTranslator
    {
        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(TenantCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Neuen Mandant erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (TenantCreated)@event;
            return string.Format("Der Mandant <strong>{0}</strong> wurde neu erstellt.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return @event.AggregateId;
        }

        public string GetAffectedModuleId()
        {
            return "tenant";
        }
    }
}