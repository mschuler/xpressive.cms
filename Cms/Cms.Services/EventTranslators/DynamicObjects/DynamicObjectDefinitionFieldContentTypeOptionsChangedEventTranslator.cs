using System;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectDefinitionFieldContentTypeOptionsChangedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectDefinitionFieldContentTypeOptionsChangedEventTranslator(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectDefinitionFieldContentTypeOptionsChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Objektdefinitionsfeld ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectDefinitionFieldContentTypeOptionsChanged)@event;
            var definition = _repository.GetEvenIfDeleted(@event.AggregateId);
            return string.Format("Das Feld <strong>{0}</strong> der Objektdefinition <strong>{1} (Nr. {2})</strong> wurde ge�ndert.", e.Name, definition.Name, definition.PublicId);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objectdefinition";
        }
    }
}