﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using Microsoft.Owin;

namespace Cms.WebNew
{
    internal static class HttpRequestMessageExtensions
    {
        public static string GetClientIpAddress(this HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return IPAddress.Parse(((HttpContextBase)request.Properties["MS_HttpContext"]).Request.UserHostAddress).ToString();
            }
            if (request.Properties.ContainsKey("MS_OwinContext"))
            {
                return IPAddress.Parse(((OwinContext)request.Properties["MS_OwinContext"]).Request.RemoteIpAddress).ToString();
            }
            return null;
        }

        public static string GetReferrer(this HttpRequestMessage request)
        {
            if (request.Headers.Referrer == null)
            {
                return null;
            }
            return request.Headers.Referrer.Authority;
        }

        public static bool IsLocal(this HttpRequestMessage request)
        {
            var ipAddress = request.GetClientIpAddress();
            if (string.IsNullOrEmpty(ipAddress))
            {
                return false;
            }

            return ipAddress.Equals("127.0.0.1", StringComparison.Ordinal) ||
                   ipAddress.Equals("::1", StringComparison.Ordinal);
        }
    }
}