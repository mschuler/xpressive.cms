using System.Net;
using System.Net.Http;
using Cms.Aggregates.Pages;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class ValidateIsEnabled : IFilter<RenderPageMessage>
    {
        private readonly IDisabledPageTokenService _disabledPageTokenService;

        public ValidateIsEnabled(IDisabledPageTokenService disabledPageTokenService)
        {
            _disabledPageTokenService = disabledPageTokenService;
        }

        public bool Execute(RenderPageMessage msg)
        {
            if (msg.Page.IsEnabled)
            {
                return true;
            }

            var query = msg.Request.RequestUri.ParseQueryString();
            var accessKey = query.Get("accessToken");

            if (_disabledPageTokenService.IsValidToken(msg.Page.Id, accessKey))
            {
                return true;
            }

            msg.Response = new HttpResponseMessage(HttpStatusCode.NotFound);
            return false;
        }
    }
}