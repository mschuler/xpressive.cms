﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.Websites.Queries
{
    [RequiredPermission("website", PermissionLevel.CanSee)]
    [RequiredPermission("page", PermissionLevel.CanSee)]
    public class GetAllWebsites : QueryBase { }
}