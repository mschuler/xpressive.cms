﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.Pages.Commands
{
    [RequiredPermission("page", PermissionLevel.CanChange)]
    public class ChangePageTemplate : CommandBase
    {
        public ulong PageTemplateId { get; set; }
    }
}