﻿namespace Cms.SharedKernel
{
    public interface IEncryptionService
    {
        byte[] EncryptString(string plain);

        byte[] Encrypt(byte[] plain);

        string DecryptString(byte[] encrypted);

        byte[] Decrypt(byte[] encrypted);
    }
}