using System.Collections.Generic;
using System.Linq;
using Cms.SharedKernel;

namespace Cms.Aggregates.Calendars
{
    internal class CalendarRepository : RepositoryBase<Calendar>, ICalendarRepository
    {
        public IEnumerable<AppointmentOccurrence> GetAppointments(ulong tenantId, DateTimeRange fromTo)
        {
            return GetAll()
                .Where(c => c.TenantId == tenantId)
                .SelectMany(c => c.Appointments)
                .SelectMany(a => a.Occurrences(fromTo))
                .Distinct()
                .OrderBy(a => a.StartEnd.Start)
                .ToList();
        }

        public IEnumerable<Category> GetCategories(ulong tenantId)
        {
            return GetAll()
                .Where(c => c.TenantId == tenantId)
                .SelectMany(c => c.Categories)
                .ToList();
        }

        public Appointment GetAppointment(ulong tenantId, ulong appointmentId)
        {
            var appointment = GetAll()
                .Where(c => c.TenantId == tenantId)
                .SelectMany(c => c.Appointments)
                .SingleOrDefault(a => a.Id == appointmentId);

            if (appointment == null)
            {
                throw new AggregateNotFoundException(appointmentId).WithGuid();
            }

            return appointment;
        }

        public override void DeleteAllByTenantId(ulong tenantId)
        {
            var calendars = GetAll().Where(c => c.TenantId == tenantId).ToList();
            calendars.ForEach(c => Delete(c.Id));
        }
    }
}