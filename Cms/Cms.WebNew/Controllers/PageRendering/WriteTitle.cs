using System;
using System.Net;
using System.Text.RegularExpressions;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class WriteTitle : IFilter<RenderPageMessage>
    {
        private static readonly Regex _titleRegex = new Regex("<title>([^<]*)</title>", RegexOptions.Compiled | RegexOptions.IgnoreCase, TimeSpan.FromSeconds(1));

        public bool Execute(RenderPageMessage msg)
        {
            if (_titleRegex.IsMatch(msg.Content))
            {
                // there is already a title tag
                return true;
            }

            if (!msg.IsHtml)
            {
                return true;
            }

            var title = WebUtility.HtmlEncode(msg.Page.Title);
            msg.Content = msg.Content.Replace("<head>", "<head>\n<title>" + title + "</title>");
            return true;
        }
    }
}