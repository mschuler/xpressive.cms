﻿using System;

namespace Cms.Aggregates.Calendars
{
    public class DailyRecurrence : AppointmentRecurrence
    {
        private int _everyXthDay;

        public bool IsEveryXthDay { get; set; }

        public int EveryXthDay
        {
            get { return Math.Max(1, _everyXthDay); }
            set { _everyXthDay = value; }
        }

        public DateTime? EndDate { get; set; }

        public bool HasEndDate
        {
            get { return EndDate.HasValue; }
            set
            {
                if (!value)
                {
                    EndDate = null;
                }
            }
        }

        public override bool IsMatch(Appointment appointment, DateTime dateTime)
        {
            if (dateTime < appointment.StartDate.Date)
            {
                return false;
            }

            if (EndDate.HasValue && dateTime > EndDate.Value)
            {
                return false;
            }

            if (!IsEveryXthDay)
            {
                return dateTime.DayOfWeek == DayOfWeek.Monday ||
                       dateTime.DayOfWeek == DayOfWeek.Tuesday ||
                       dateTime.DayOfWeek == DayOfWeek.Wednesday ||
                       dateTime.DayOfWeek == DayOfWeek.Thursday ||
                       dateTime.DayOfWeek == DayOfWeek.Friday;
            }

            if (EveryXthDay <= 1)
            {
                return true;
            }

            var totalDays = (int)(dateTime.Date - appointment.StartDate.Date).TotalDays;
            return totalDays % EveryXthDay == 0;
        }
    }
}