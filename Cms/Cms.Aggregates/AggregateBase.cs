namespace Cms.Aggregates
{
    public abstract class AggregateBase : IAggregate
    {
        public ulong Id { get; set; }

        protected bool Equals(AggregateBase other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }
            if (obj.GetType() != GetType()) { return false; }
            return Equals((AggregateBase)obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }

    public interface IAggregate
    {
        ulong Id { get; set; }
    }
}