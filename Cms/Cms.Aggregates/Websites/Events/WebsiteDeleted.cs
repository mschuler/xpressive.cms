﻿namespace Cms.Aggregates.Websites.Events
{
    public class WebsiteDeleted : EventBase<Website>
    {
        public string Name { get; set; }
    }
}