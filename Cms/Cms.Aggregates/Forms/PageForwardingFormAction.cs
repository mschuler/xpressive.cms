﻿namespace Cms.Aggregates.Forms
{
    public class PageForwardingFormAction : FormActionConfiguration
    {
        public string Id { get; set; }
        public string WebsiteId { get; set; }
        public string Fullname { get; set; }

        public override FormActionConfiguration Clone()
        {
            return new PageForwardingFormAction
            {
                Id = Id,
                WebsiteId = WebsiteId,
                Fullname = Fullname
            };
        }
    }
}