﻿using System;

namespace Cms.Aggregates.Pages
{
    public interface IPageUrlService
    {
        string GetUrl(ulong pageId);

        string GetUrl(PageBase page);

        string GetPermalink(Page page);

        string GetPermalink(ulong websiteId, ulong pageId, Guid permalink);
    }
}