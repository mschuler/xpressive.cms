﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Queries
{
    [RequiredPermission("objectdefinition", PermissionLevel.CanSee)]
    [RequiredPermission("object", PermissionLevel.CanSee)]
    public class GetObjectDefinition : QueryBase
    {
        public ulong Id { get; set; }
    }
}