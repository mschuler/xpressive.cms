using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0012_add_local_file_system_store : DatabaseMigrationBase
    {
        public M0012_add_local_file_system_store() : base(12) { }

        public override void Execute(Database database)
        {
            var fileStoreId = AddFileStoreCreatedEvent(database);

            var events = database
                .Query<EventDto>()
                .Where(e => e.Event == "FileDirectoryCreated")
                .ToList();

            foreach (var dto in events)
            {
                var json = dto.Data;
                json = json.Replace(",\"Name\":", ",\"FileStoreId\":" + fileStoreId + ",\"Name\":");
                dto.Data = json;

                database.Update(dto, d => d.Data);
            }
        }

        private long AddFileStoreCreatedEvent(Database database)
        {
            var firstEvent = database
                .Query<EventDto>()
                .OrderBy(d => d.Version)
                .First();

            var min = Math.Min(firstEvent.Transaction, firstEvent.Version);

            var dto = new FileStoreCreatedDto
            {
                FileStoreFactoryId = new Guid("427B9C16-34CA-4617-84A5-923599EC633A"),
                Name = "Dateisystem auf Server",
                Date = new DateTime(2015, 1, 1),
                Settings = new Dictionary<string, string>(0)
            };

            var json = JsonConvert.SerializeObject(dto);

            var fs = new EventDto
            {
                Transaction = min - 100,
                Version = min - 150,
                AggregateType = "IFileStore",
                AggregateId = min - 200,
                Event = "FileStoreCreated",
                UserId = 100,
                Data = json,
            };

            database.Insert(fs);

            return fs.AggregateId;
        }

        [TableName("Event")]
        [PrimaryKey("Version", AutoIncrement = false)]
        private class EventDto
        {
            public long Transaction { get; set; }
            public long Version { get; set; }
            public string AggregateType { get; set; }
            public long AggregateId { get; set; }
            public string Event { get; set; }
            public long UserId { get; set; }
            public string Data { get; set; }
            public bool IsDeleted { get; set; }
        }

        private class FileStoreCreatedDto
        {
            public DateTime Date { get; set; }
            public Guid FileStoreFactoryId { get; set; }
            public string Name { get; set; }
            public IDictionary<string, string> Settings { get; set; }
        }
    }
}