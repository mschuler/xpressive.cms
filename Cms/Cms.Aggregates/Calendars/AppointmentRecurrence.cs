﻿using System;

namespace Cms.Aggregates.Calendars
{
    public abstract class AppointmentRecurrence
    {
        public abstract bool IsMatch(Appointment appointment, DateTime dateTime);
    }
}