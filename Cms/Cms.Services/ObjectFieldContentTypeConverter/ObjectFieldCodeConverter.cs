using System;
using Cms.SharedKernel;

namespace Cms.Services.ObjectFieldContentTypeConverter
{
    internal class ObjectFieldCodeConverter : IObjectFieldContentTypeConverter
    {
        public bool CanHandle(string type)
        {
            return type.Equals("code", StringComparison.OrdinalIgnoreCase);
        }

        public string Serialize(string json)
        {
            return json ?? string.Empty;
        }

        public object Deserialize(string value)
        {
            return value ?? string.Empty;
        }

        public SortableObjectField DeserializeForSortableField(string value)
        {
            return new SortableObjectField(value ?? string.Empty);
        }

        public object DeserializeForPublicApi(string value)
        {
            return Deserialize(value);
        }

        public bool TryGetMetadataForPublicApi(string value, out object metadata)
        {
            metadata = null;
            return false;
        }

        public object DeserializeForInterpreter(string value)
        {
            return Deserialize(value);
        }

        public string SerializeForImport(string json, string format, string options)
        {
            return json ?? string.Empty;
        }

        public string SerializeForImportPreview(string json, string format, string options)
        {
            return json ?? string.Empty;
        }

        public bool IsValidValueForImport(string json, string format, string options)
        {
            return true;
        }
    }
}