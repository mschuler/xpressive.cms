# xpressive.cms #

Open source Content Management System (CMS). Still in development.

License: [MIT](http://opensource.org/licenses/MIT)

### Contributors ###

* [Michael Schuler](https://www.michaelschuler.ch)

### Sponsors ###

* [JetBrains](https://www.jetbrains.com/)
* [BrowserStack](https://www.browserstack.com/)

### Technologies ###

* ASP.NET WebAPI
* AngularJS
* [CQRS](http://martinfowler.com/bliki/CQRS.html) and [EventSourcing](http://www.martinfowler.com/eaaDev/EventSourcing.html)