using System;
using System.Collections;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    public sealed class WhereListInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "where"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.Value.Equals("where", StringComparison.OrdinalIgnoreCase) &&
                tree.LeftItem != null && tree.LeftItem.Result is IEnumerable &&
                tree.Children != null && tree.Children.Count == 3 &&
                tree.Children[0].ParameterType == ParameterType.String &&
                tree.Children[1].ParameterType == ParameterType.String &&
                tree.Children[2].Result != null)
            {
                var engine = new Engine();
                var result = ((IEnumerable)tree.LeftItem.Result).GetListOfSameType();
                var whereExpression = (string)tree.Children[0].Result;
                var @operator = ((string)tree.Children[1].Result).ToLowerInvariant();

                foreach (var item in (IEnumerable)tree.LeftItem.Result)
                {
                    var treeItem = CreateTreeItem(engine, whereExpression, item);

                    var whereResult = InterpreterFactory.Instance.Calculate(treeItem);

                    if (OperatorHelper.IsMatch(whereResult, @operator, tree.Children[2].Result))
                    {
                        result.Add(item);
                    }
                }

                tree.Result = result;
            }
        }

        private static TreeItem CreateTreeItem(Engine engine, string whereExpression, object item)
        {
            var treeItem = engine.ParseTreeItem(whereExpression);

            if (treeItem.ItemType == TreeItemType.Attribute || treeItem.ItemType == TreeItemType.Function)
            {
                var parent = new TreeItem
                {
                    ItemType = TreeItemType.Expression,
                    Value = string.Empty,
                };
                parent.Children.Add(treeItem);
                treeItem.Parent = parent;
                treeItem = parent;
            }

            treeItem.Children.Insert(0, new TreeItem
            {
                ItemType = TreeItemType.Function,
                Parent = treeItem,
                Result = item,
            });

            return treeItem;
        }
    }
}