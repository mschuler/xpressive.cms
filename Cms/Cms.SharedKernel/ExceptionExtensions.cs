﻿using System;

namespace Cms.SharedKernel
{
    public static class ExceptionExtensions
    {
        public static T WithGuid<T>(this T exception) where T : Exception
        {
            if (exception.Data.Contains("guid"))
            {
                return exception;
            }

            var guid = Guid.NewGuid().ToString("d");

            if (exception.InnerException != null && exception.InnerException.Data.Contains("guid"))
            {
                guid = (string)exception.InnerException.Data["guid"];
            }

            exception.Data.Add("guid", guid);
            return exception;
        }
    }
}
