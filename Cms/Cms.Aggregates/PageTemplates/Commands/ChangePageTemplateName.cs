using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.PageTemplates.Commands
{
    [RequiredPermission("pagetemplate", PermissionLevel.CanChange)]
    public class ChangePageTemplateName : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }
    }
}