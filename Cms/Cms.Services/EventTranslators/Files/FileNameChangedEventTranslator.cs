﻿using System;
using Cms.Aggregates.Files;
using Cms.Aggregates.Files.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Files
{
    internal class FileNameChangedEventTranslator : IEventTranslator
    {
        private readonly IFileRepository _repository;

        public FileNameChangedEventTranslator(IFileRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FileNameChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Datei umbenannt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FileNameChanged)@event;
            var file = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Die Datei <strong>(Nr. {0})</strong> wurde in <strong>{1}</strong> umbenannt.", file.PublicId, e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "file";
        }
    }
}