﻿namespace Cms.Aggregates.Forms.Events
{
    public class FormEntryDeleted : EventBase<Form>
    {
        public ulong FormEntryId { get; set; }
    }
}