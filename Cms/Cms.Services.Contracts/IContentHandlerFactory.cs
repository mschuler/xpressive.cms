﻿using System.Collections.Generic;

namespace Cms.Services.Contracts
{
    public interface IContentHandlerFactory
    {
        IContentHandler Get(string pageContent);

        IEnumerable<string> ContentTypes { get; }
    }
}
