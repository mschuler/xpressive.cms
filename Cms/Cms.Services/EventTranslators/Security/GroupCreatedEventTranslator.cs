﻿using System;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class GroupCreatedEventTranslator : IEventTranslator
    {
        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(GroupCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Benutzergruppe erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (GroupCreated)@event;
            return string.Format("Die Benutzergruppe <strong>{0}</strong> wurde neu erstellt.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var e = (GroupCreated)@event;
            return e.TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "group";
        }
    }
}