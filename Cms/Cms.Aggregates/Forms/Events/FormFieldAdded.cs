﻿namespace Cms.Aggregates.Forms.Events
{
    public class FormFieldAdded : EventBase<Form>
    {
        public ulong Id { get; set; }

        public string Name { get; set; }
    }
}