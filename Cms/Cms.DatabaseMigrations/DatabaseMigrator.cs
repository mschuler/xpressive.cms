﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Cms.DatabaseMigrations.Migrations;
using log4net;
using NPoco;

namespace Cms.DatabaseMigrations
{
    internal class DatabaseMigrator : IDatabaseMigrator
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(DatabaseMigrator));
        private readonly Database _database;

        public DatabaseMigrator()
        {
            _database = new Database("CmsDatabaseConnection");
        }

        public void Migrate()
        {
            var currentVersion = GetCurrentVersion();
            var migrations = GetMigrations().Where(m => m.Id > currentVersion).OrderBy(m => m.Id).ToList();

            _log.InfoFormat("The current database version is {0}.", currentVersion);
            _database.BeginTransaction();

            foreach (var migration in migrations)
            {
                ExecuteMigration(migration);
            }

            _database.CompleteTransaction();
            currentVersion = GetCurrentVersion();
            _log.InfoFormat("The database is up to date ({0}).", currentVersion);
        }

        private void ExecuteMigration(DatabaseMigrationBase migration)
        {
            try
            {
                migration.Execute(_database);

                _database.Execute("insert into [Version] values (@0)", migration.Id);
            }
            catch (Exception e)
            {
                _log.ErrorFormat("Error while executing sql update {0}. ErrorMessage: {1}.", migration.Id, e.Message);
                _database.AbortTransaction();
                throw;
            }
        }

        private int GetCurrentVersion()
        {
            try
            {
                return _database.ExecuteScalar<int>("select max([Version]) from [Version]");
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private IEnumerable<DatabaseMigrationBase> GetMigrations()
        {
            var baseClass = typeof(DatabaseMigrationBase);
            var migrations = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(t => t.IsSubclassOf(baseClass))
                .Where(t => !t.IsAbstract)
                .ToList();

            foreach (var migration in migrations)
            {
                yield return (DatabaseMigrationBase)Activator.CreateInstance(migration);
            }
        }
    }
}