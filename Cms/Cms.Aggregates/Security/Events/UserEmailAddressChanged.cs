﻿namespace Cms.Aggregates.Security.Events
{
    public class UserEmailAddressChanged : EventBase<User>
    {
        public string EmailAddress { get; set; }
    }
}