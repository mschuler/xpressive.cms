using System;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class UserSysadminChangedEventTranslator : IEventTranslator
    {
        private readonly IUserRepository _repository;

        public UserSysadminChangedEventTranslator(IUserRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(UserSysadminChanged);
        }

        public string GetHeader(IEvent @event)
        {
            var e = (UserSysadminChanged)@event;
            return e.IsSysadmin ? "Benutzer wurde Systemadministrator Status vergeben" : "Benutzer wurde Systemadministrator Status entfernt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (UserSysadminChanged)@event;
            var user = _repository.GetEvenIfDeleted(@event.AggregateId);

            if (e.IsSysadmin)
            {
                return string.Format("Dem Benutzer <strong>{0}</strong> wurde der Systemadministrator Status vergeben.", user.FullName);
            }
            return string.Format("Dem Benutzer <strong>{0}</strong> wurde der Systemadministrator Status entfernt.", user.FullName);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}