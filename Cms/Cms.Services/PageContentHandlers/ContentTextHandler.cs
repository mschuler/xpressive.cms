﻿using System;
using Cms.Aggregates.Pages;
using Cms.Services.Contracts;

namespace Cms.Services.PageContentHandlers
{
    internal class ContentTextHandler : IContentHandler
    {
        public bool CanHandle(string pageContentType)
        {
            return pageContentType.Equals("text", StringComparison.OrdinalIgnoreCase);
        }

        public string ContentType
        {
            get { return "Text"; }
        }

        public string GetImage()
        {
            return "fa fa-font";
        }

        public string Render(PageContentValueBase value)
        {
            var text = value as PageContentTextValue;

            if (text == null || string.IsNullOrEmpty(text.Value))
            {
                return string.Empty;
            }

            return text.Value;
        }

        public object GetValueForInterpreter(PageContentValueBase value)
        {
            return Render(value);
        }
    }
}