using System;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.OData;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.Tenants;
using Cms.Services.Contracts;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers.Admin
{
    public class ObjectQueryController : ApiControllerAuthorizedByApiKey
    {
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;
        private readonly IDynamicObjectRepository _objectRepository;
        private readonly IDynamicObjectDtoService _dtoService;
        private readonly IObjectFieldContentTypeConverterService _contentTypeConverter;
        private readonly ITenantRepository _tenantRepository;

        public ObjectQueryController(
            IApiKeyService apiKeyService,
            IDynamicObjectDefinitionRepository definitionRepository,
            IDynamicObjectRepository objectRepository,
            IDynamicObjectDtoService dtoService,
            IObjectFieldContentTypeConverterService contentTypeConverter,
            ITenantRepository tenantRepository)
            : base(apiKeyService)
        {
            _definitionRepository = definitionRepository;
            _objectRepository = objectRepository;
            _dtoService = dtoService;
            _contentTypeConverter = contentTypeConverter;
            _tenantRepository = tenantRepository;
        }

        [HttpGet, Route("api/public/object/{definitionId:int}"), EnableQuery]
        public IQueryable<IDynamicObjectDtoForPublicApi> Get(int definitionId, [FromUri]string t = null)
        {
            if (!IsAuthorized())
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            var definition = _definitionRepository.GetAll().SingleOrDefault(d => d.PublicId == definitionId);
            var tenant = _tenantRepository.GetAll().SingleOrDefault(tn => tn.Name.Equals(t, StringComparison.OrdinalIgnoreCase));

            if (definition == null)
            {
                return new IDynamicObjectDtoForPublicApi[0].AsQueryable();
            }

            var tenantId = tenant != null ? tenant.Id : definition.TenantId;

            return _objectRepository
                .GetAll(tenantId, definition.Id)
                .OrderByDescending(o => o.PublicId)
                .Select(o => _dtoService.GetDtoForPublicApi(o, definition, _contentTypeConverter))
                .AsQueryable();
        }
    }
}