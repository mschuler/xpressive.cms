using Cms.Aggregates.Security;

namespace Cms.Aggregates.Pages.Commands
{
    [RequiredPermission("page", PermissionLevel.CanChange)]
    public class MovePage : CommandBase
    {
        public ulong ParentPageId { get; set; }

        public int SortOrder { get; set; }
    }
}