namespace Cms.Aggregates.DynamicObjects
{
    public class DynamicObjectField
    {
        public string Name { get; set; }
        public string ContentType { get; set; }
        public string Value { get; set; }
    }
}