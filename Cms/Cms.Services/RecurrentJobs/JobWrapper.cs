using System;
using Cms.Services.Contracts;
using log4net;
using Quartz;

namespace Cms.Services.RecurrentJobs
{
    internal class JobWrapper : IJob
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(JobWrapper));
        private readonly IRecurrentJob _job;

        public JobWrapper(IRecurrentJob job)
        {
            _job = job;
        }

        public void Execute(IJobExecutionContext context)
        {
            if (_job == null)
            {
                return;
            }

            _log.DebugFormat("Execute job {0}.", _job.GetType().Name);

            try
            {
                _job.Execute();
            }
            catch (Exception e)
            {
                var msg = string.Format("Error while executing job {0}.", _job.GetType().Name);
                _log.Error(msg, e);
            }
        }
    }
}