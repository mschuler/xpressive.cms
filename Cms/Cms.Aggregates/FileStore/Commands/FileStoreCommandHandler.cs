﻿using System;
using System.Linq;
using Cms.Aggregates.FileStore.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.FileStore.Commands
{
    internal class FileStoreCommandHandler : CommandHandlerBase
    {
        private readonly IFileStoreProvider _fileStoreProvider;
        private readonly IFileStoreRepository _repository;

        public FileStoreCommandHandler(IFileStoreProvider fileStoreProvider, IFileStoreRepository repository)
        {
            _fileStoreProvider = fileStoreProvider;
            _repository = repository;
        }

        public void When(CreateFileStore command, Action<IEvent> eventHandler)
        {
            var fileStoreFactory = _fileStoreProvider.GetFactory(command.FileStoreFactoryId);
            var name = command.Name.Trim().RemoveControlChars();

            if (!fileStoreFactory.IsValidConfiguration(command.Settings))
            {
                throw new ExceptionWithReason("Invalid file store configuration.");
            }

            if (_repository.GetAll().Any(fs => fs.Name.Equals(name, StringComparison.OrdinalIgnoreCase)))
            {
                throw new ExceptionWithReason("There is already a file store with this name.");
            }

            var fileStore = fileStoreFactory.Create();
            if (fileStore.IsLocalFilesystem)
            {
                throw new ExceptionWithReason("You can't create multiple instances of the local file system.");
            }

            var e = new FileStoreCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                FileStoreFactoryId = command.FileStoreFactoryId,
                Name = name,
                Settings = command.Settings,
            };

            eventHandler(e);
        }
    }
}
