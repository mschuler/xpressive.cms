﻿using System;
using NUnit.Framework;

namespace Entities.Fixtures
{
	[TestFixture]
	public class PageServiceFixture
	{
		[Test]
		public void Test()
		{
			PageService ps = new PageService();

			ps.Find(new Uri("http://sub.domain.com/page.aspx?query=true"));
			Console.WriteLine("-------------------------------------------------");
			ps.Find(new Uri("http://www.domain.com/page.aspx"));
			Console.WriteLine("-------------------------------------------------");
			ps.Find(new Uri("http://domain.com/dir1/dir2/page.aspx"));
		}
	}
}
