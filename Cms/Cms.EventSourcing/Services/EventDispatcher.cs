using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cms.EventSourcing.Contracts.Events;
using Cms.SharedKernel;
using log4net;

namespace Cms.EventSourcing.Services
{
    internal static class EventDispatcher
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(EventDispatcher));

        private static readonly BlockingCollection<IEvent> _queue = new BlockingCollection<IEvent>();
        private static readonly object _lock = new object();
        private static bool _isRunning;

        public static void Handle(IEnumerable<IEvent> events, IEnumerable<IEventHandler> eventHandlers, bool waitUntilQueueIsEmpty = false)
        {
            foreach (var @event in events.OrderBy(e => e.Version))
            {
                _queue.Add(@event);
            }

            StartThread(eventHandlers, waitUntilQueueIsEmpty);
        }

        private static void StartThread(IEnumerable<IEventHandler> eventHandlers, bool waitUntilQueueIsEmpty)
        {
            if (_isRunning)
            {
                return;
            }

            lock (_lock)
            {
                if (_isRunning)
                {
                    return;
                }
                _isRunning = true;
            }

            if (waitUntilQueueIsEmpty)
            {
                RunThread(eventHandlers);
            }
            else
            {
                Task.Run(() => RunThread(eventHandlers));
            }
        }

        private static void RunThread(IEnumerable<IEventHandler> eventHandlers)
        {
            var handlers = eventHandlers.ToList();

            using (PerformanceMeasurer.Get("Handle events in EventDispatcher", o => _log.Debug(o)))
            {
                IEvent @event;
                while (_queue.TryTake(out @event, 200))
                {
                    foreach (var eventHandler in handlers)
                    {
                        try
                        {
                            eventHandler.Handle(@event);
                        }
                        catch (Exception e)
                        {
                            var msg = string.Format(
                                "Error in event handler {0} while handling event {2} with version {1:D}.",
                                eventHandler.GetType().Name,
                                @event.Version,
                                @event.GetType().Name);
                            _log.Error(msg, e);
                        }
                    }
                }
            }

            _isRunning = false;
        }
    }
}