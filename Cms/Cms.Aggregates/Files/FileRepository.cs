using System;
using System.Collections.Generic;
using System.Linq;
using Cms.SharedKernel;

namespace Cms.Aggregates.Files
{
    internal class FileRepository : RepositoryBase<File>, IFileRepository
    {
        private static readonly object _lock = new object();
        private static int _currentMaxPublicId;

        public int GetNextPublicId()
        {
            lock (_lock)
            {
                if (_currentMaxPublicId == 0)
                {
                    var allFiles = GetAll().ToList();
                    _currentMaxPublicId = allFiles.Count == 0 ? 0 : allFiles.Max(f => f.PublicId);
                }

                _currentMaxPublicId++;
                return _currentMaxPublicId;
            }
        }

        public File GetByPublicId(int publicId)
        {
            return GetAll().SingleOrDefault(f => f.PublicId.Equals(publicId));
        }

        public IEnumerable<File> GetByDirectory(ulong fileDirectoryId)
        {
            return GetAll()
                .Where(f => f.FileDirectoryId.Equals(fileDirectoryId))
                .ToList();
        }

        public override void DeleteAllByTenantId(ulong tenantId)
        {
            throw new NotSupportedException("FileDirectoryRepository is deleting these files.").WithGuid();
        }
    }
}