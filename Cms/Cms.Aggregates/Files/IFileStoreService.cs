﻿using System.IO;

namespace Cms.Aggregates.Files
{
    public interface ILocalFileStore
    {
        FileInfo Get(ulong fileId, ulong version);
    }
}