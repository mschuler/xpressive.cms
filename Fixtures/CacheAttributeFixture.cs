﻿using Entities.Aop;
using NUnit.Framework;

namespace Entities.Fixtures
{
	public class CacheAttributeFixture
	{
		public interface ITestService
		{
			string Foo(string bar);
			bool IsCalled { get; set; }
		}

		public class TestService : ITestService
		{
			public TestService()
			{
				IsCalled = false;
			}

			public bool IsCalled { get; set; }

			[Cache("TestService.Foo", Type = CacheType.TimeInMinutes, TimeInMinutes = 5)]
			public string Foo(string bar)
			{
				IsCalled = true;
				return bar;
			}
		}

		private CacheService myCacheService;
		private ITestService myTestService;
		
		[SetUp]
		public void SetUp()
		{
			myCacheService = new CacheService();
			myTestService = new CachingProxy<ITestService>(myCacheService, new TestService()).GetTransparentProxy();
		}

		[Test]
		public void Test()
		{
			Assert.AreEqual(0, myCacheService.CacheSize);
			Assert.IsFalse(myCacheService.Contains("TestService.Foo"));
			Assert.IsFalse(myTestService.IsCalled);

			myTestService.Foo("Test");

			Assert.AreNotEqual(0, myCacheService.CacheSize);
			Assert.IsTrue(myCacheService.Contains("TestService.Foo1-Test"));
			Assert.IsTrue(myTestService.IsCalled);

			myTestService.IsCalled = false;
			myTestService.Foo("Test");

			Assert.IsFalse(myTestService.IsCalled);
		}
	}
}
