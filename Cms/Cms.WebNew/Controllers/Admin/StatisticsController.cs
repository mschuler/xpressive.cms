using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Files;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Security;
using Cms.Aggregates.Websites;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using Newtonsoft.Json;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class StatisticsController : ApiControllerBase
    {
        private readonly IPageTrackingService _pageTrackingService;
        private readonly IFileTrackingService _fileTrackingService;
        private readonly IGroupRepository _groupRepository;
        private readonly IWebsiteRepository _websiteRepository;
        private readonly IPageRepository _pageRepository;
        private readonly IFileRepository _fileRepository;

        public StatisticsController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IPageTrackingService pageTrackingService, IGroupRepository groupRepository, IWebsiteRepository websiteRepository, IFileTrackingService fileTrackingService, IPageRepository pageRepository, IFileRepository fileRepository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _pageTrackingService = pageTrackingService;
            _groupRepository = groupRepository;
            _websiteRepository = websiteRepository;
            _fileTrackingService = fileTrackingService;
            _pageRepository = pageRepository;
            _fileRepository = fileRepository;
        }

        [HttpGet, Route("api/stats/{tenantId:ulong}")]
        public IEnumerable<PageStatsDto> GetStatsDemoValues(ulong tenantId)
        {
            if (!CmsUser.HasPermission("page", tenantId, PermissionLevel.CanSee, _groupRepository))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized).WithGuid();
            }

            var stats = new List<PageStatsDto>();

            foreach (var website in _websiteRepository.GetAll(tenantId))
            {
                var stat = new PageStatsDto
                {
                    WebsiteId = website.Id.ToJsonString(),
                    WebsiteName = website.Name,
                    ReturningUsers = _pageTrackingService.GetReturningVisitorsInPercent(website.Id),
                    TotalPageViews = _pageTrackingService.GetTotalPageViewsForOneMonth(website.Id),
                    TotalSessions = _pageTrackingService.GetTotalSessionsForOneMonth(website.Id),
                    ViewsPerVisit = _pageTrackingService.GetAveragePageViewsPerVisit(website.Id),
                    Pages = GetTop10Pages(website).ToList(),
                    Files = GetTop10Files(website).ToList(),
                    FileCountries = GetFilesByCountry(website).ToList(),
                    PageCountries = GetPagesByCountry(website).ToList(),
                    Day = GetPagesByDay(website).ToList(),
                    Month = GetPagesByMonth(website).ToList(),
                };

                stats.Add(stat);
            }

            return stats;
        }

        private IEnumerable<LineChartTuple> GetTop10Pages(Website website)
        {
            if (!CmsUser.HasPermission("page", website.TenantId, PermissionLevel.CanSee, _groupRepository))
            {
                yield break;
            }

            var tuples = _pageTrackingService
                .GetTheTenMostViewedPagesForOneMonth(website.Id)
                .OrderByDescending(t => t.Item2);

            foreach (var tuple in tuples)
            {
                yield return new LineChartTuple
                {
                    Label = _pageRepository.GetEvenIfDeleted(tuple.Item1).Fullname,
                    Value = tuple.Item2,
                };
            }
        }

        private IEnumerable<LineChartTuple> GetTop10Files(Website website)
        {
            if (!CmsUser.HasPermission("file", website.TenantId, PermissionLevel.CanSee, _groupRepository))
            {
                yield break;
            }

            var tuples = _fileTrackingService
                .GetTheTenMostViewedFilesForOneMonth(website.Id)
                .OrderByDescending(t => t.Item2);

            foreach (var tuple in tuples)
            {
                yield return new LineChartTuple
                {
                    Label = _fileRepository.GetEvenIfDeleted(tuple.Item1).Fullname,
                    Value = tuple.Item2,
                };
            }
        }

        private IEnumerable<LineChartTuple> GetPagesByCountry(Website website)
        {
            if (!CmsUser.HasPermission("page", website.TenantId, PermissionLevel.CanSee, _groupRepository))
            {
                yield break;
            }

            var tuples = _pageTrackingService.GetPageViewsPerCountryForOneMonth(website.Id);

            foreach (var tuple in tuples)
            {
                yield return new LineChartTuple
                {
                    Label = tuple.Item1,
                    Value = tuple.Item2,
                };
            }
        }

        private IEnumerable<LineChartTuple> GetFilesByCountry(Website website)
        {
            if (!CmsUser.HasPermission("file", website.TenantId, PermissionLevel.CanSee, _groupRepository))
            {
                yield break;
            }

            var tuples = _fileTrackingService.GetFileViewsPerCountryForOneMonth(website.Id);

            foreach (var tuple in tuples)
            {
                yield return new LineChartTuple
                {
                    Label = tuple.Item1,
                    Value = tuple.Item2,
                };
            }
        }

        private IEnumerable<LineChartTuple> GetPagesByDay(Website website)
        {
            if (!CmsUser.HasPermission("page", website.TenantId, PermissionLevel.CanSee, _groupRepository))
            {
                yield break;
            }

            var dict = _pageTrackingService
                .GetTotalPageViewsPerDay(website.Id)
                .ToDictionary(t => t.Item1, t => t.Item2);

            var day = DateTime.Today.AddMonths(-1);

            while (day <= DateTime.Today)
            {
                int value;
                if (!dict.TryGetValue(day, out value))
                {
                    value = 0;
                }

                yield return new LineChartTuple
                {
                    Date = day,
                    Label = day.ToString("dd. MMM"),
                    Value = value,
                };

                day = day.AddDays(1);
            }
        }

        private IEnumerable<LineChartTuple> GetPagesByMonth(Website website)
        {
            if (!CmsUser.HasPermission("page", website.TenantId, PermissionLevel.CanSee, _groupRepository))
            {
                yield break;
            }

            var dict = _pageTrackingService
                .GetTotalPageViewsPerMonth(website.Id)
                .ToDictionary(t => t.Item1, t => t.Item2);

            var month = DateTime.Today.AddDays(1 - DateTime.Today.Day).AddYears(-1);

            while (month <= DateTime.Today)
            {
                int value;
                if (!dict.TryGetValue(month, out value))
                {
                    value = 0;
                }

                yield return new LineChartTuple
                {
                    Date = month,
                    Label = month.ToString("MMM yy"),
                    Value = value,
                };

                month = month.AddMonths(1);
            }
        }

        public class PageStatsDto
        {
            public PageStatsDto()
            {
                Month = new List<LineChartTuple>();
                Day = new List<LineChartTuple>();
                Pages = new List<LineChartTuple>();
                Files = new List<LineChartTuple>();
                PageCountries = new List<LineChartTuple>();
                FileCountries = new List<LineChartTuple>();
            }

            public string WebsiteId { get; set; }
            public string WebsiteName { get; set; }

            public int TotalPageViews { get; set; }
            public int TotalSessions { get; set; }
            public int ReturningUsers { get; set; }
            public int ViewsPerVisit { get; set; }

            public List<LineChartTuple> Month { get; set; }
            public List<LineChartTuple> Day { get; set; }
            public List<LineChartTuple> Pages { get; set; }
            public List<LineChartTuple> Files { get; set; }
            public List<LineChartTuple> PageCountries { get; set; }
            public List<LineChartTuple> FileCountries { get; set; }
        }

        public class LineChartTuple
        {
            [JsonIgnore]
            public DateTime Date { get; set; }
            public string Label { get; set; }
            public int Value { get; set; }
        }
    }
}