﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.FileStore.Queries
{
    internal class FileStoreQueryHandler : QueryHandlerBase
    {
        private readonly IFileStoreRepository _repository;
        private readonly IFileStoreProvider _provider;

        public FileStoreQueryHandler(IFileStoreRepository repository, IFileStoreProvider provider)
        {
            _repository = repository;
            _provider = provider;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetFileStores),
                typeof (GetFileStoreDetail),
                typeof (GetFileStoreFactories),
                typeof (GetFileStoreFactorySettings),
            };
        }

        public IEnumerable<object> When(GetFileStores query)
        {
            var fileStores = _repository.GetAll().OrderBy(fs => fs.Name).ToList();

            foreach (var fileStore in fileStores)
            {
                yield return new
                {
                    Id = fileStore.Id.ToJsonString(),
                    fileStore.Name,
                    fileStore.IsLocalFilesystem,
                    Icon = fileStore.GetIcon(),
                };
            }
        }

        public IEnumerable<object> When(GetFileStoreDetail query)
        {
            var fileStore = _repository.Get(query.FileStoreId);

            yield return new
            {
                Id = fileStore.Id.ToJsonString(),
                fileStore.Name,
                fileStore.IsLocalFilesystem,
                Icon = fileStore.GetIcon(),
                fileStore.Settings,
            };
        }

        public IEnumerable<object> When(GetFileStoreFactories query)
        {
            var factories = _provider.GetFactories();

            foreach (var factory in factories)
            {
                yield return new
                {
                    Id = factory.Id.ToString("n"),
                    factory.Name,
                };
            }
        }

        public IEnumerable<object> When(GetFileStoreFactorySettings query)
        {
            var factory = _provider.GetFactory(query.Id);
            var fileStore = factory.Create();
            return fileStore.Settings.Keys;
        }
    }
}
