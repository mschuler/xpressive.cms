﻿using System;
using System.Collections.Generic;
using System.IO;
using File = Cms.Aggregates.Files.File;

namespace Cms.Aggregates.FileStore
{
    public interface IFileStore : IAggregate
    {
        Guid FileStoreFactoryId { get; }

        string Name { get; }

        IDictionary<string, string> Settings { get; }

        bool IsLocalFilesystem { get; }

        void Save(string fileName, ulong fileId, ulong version, FileInfo temporaryFile);

        void Delete(ulong fileId, ulong version);

        string GetLocalFileLocation(ulong fileId, ulong version);

        Uri GetIcon();

        Uri GetUrl(File file);

        Uri GetUrl(File file, ImageSettingsDto imageSettings);
    }
}