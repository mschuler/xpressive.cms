using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Security;
using Cms.Aggregates.Tenants.Events;

namespace Cms.Aggregates.DynamicObjects.Events
{
    internal class DynamicObjectDefinitionEventHandler : EventHandlerBase
    {
        private readonly IDynamicObjectDefinitionRepository _repository;
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;

        public DynamicObjectDefinitionEventHandler(
            IDynamicObjectDefinitionRepository repository,
            IUserRepository userRepository,
            IGroupRepository groupRepository)
        {
            _repository = repository;
            _userRepository = userRepository;
            _groupRepository = groupRepository;
        }

        public void When(DynamicObjectDefinitionCreated @event)
        {
            var definition = new DynamicObjectDefinition
            {
                TenantId = @event.TenantId,
                Id = @event.AggregateId,
                PublicId = @event.PublicId,
                Name = @event.Name
            };

            definition.Add(new DynamicObjectDefinitionField
            {
                Name = "Title",
                ContentType = "Text",
            });

            _repository.Add(definition);
        }

        public void When(DynamicObjectDefinitionDeleted @event)
        {
            _repository.Delete(@event.AggregateId);
        }

        public void When(DynamicObjectDefinitionFieldAdded @event)
        {
            var definition = _repository.Get(@event.AggregateId);
            var fields = definition.Fields.ToList();
            var sortOrder = fields.Any() ? fields.Max(f => f.SortOrder) + 1 : 0;

            definition.Add(new DynamicObjectDefinitionField
            {
                Name = @event.Name,
                SortOrder = sortOrder,
                ContentType = @event.ContentType,
                ContentTypeOptions = @event.ContentTypeOptions
            });

            SetSortOrder(definition.Fields);
        }

        public void When(DynamicObjectDefinitionFieldContentTypeOptionsChanged @event)
        {
            var definition = _repository.Get(@event.AggregateId);
            var field = definition.Fields.SingleOrDefault(f => f.Name.Equals(@event.Name, StringComparison.Ordinal));

            if (field != null)
            {
                field.ContentTypeOptions = @event.ContentTypeOptions;
            }
        }

        public void When(DynamicObjectDefinitionFieldRemoved @event)
        {
            var definition = _repository.Get(@event.AggregateId);
            var fields = definition.Fields.Where(f => f.Name.Equals(@event.Name, StringComparison.Ordinal)).ToList();

            foreach (var field in fields)
            {
                if (field.SortOrder == 0)
                {
                    // this is the title field
                    continue;
                }

                definition.Remove(field);
            }

            SetSortOrder(definition.Fields);
        }

        public void When(DynamicObjectDefinitionFieldRenamed @event)
        {
            var definition = _repository.Get(@event.AggregateId);
            var field = definition.Fields.FirstOrDefault(f => f.Name.Equals(@event.OldName, StringComparison.Ordinal));

            if (field != null)
            {
                field.Name = @event.NewName;
            }
        }

        public void When(DynamicObjectDefinitionFieldMovedUp @event)
        {
            var definition = _repository.Get(@event.AggregateId);
            var field = GetField(definition, @event.Name);

            if (field == null)
            {
                return;
            }

            var fields = definition.Fields.ToArray();
            var index = 0;

            for (var i = 0; i < fields.Length; i++)
            {
                if (ReferenceEquals(fields[i], field))
                {
                    index = i;
                    break;
                }
            }

            if (index <= 1)
            {
                // the title field is the first one
                return;
            }

            var temp = fields[index - 1];
            fields[index - 1] = field;
            fields[index] = temp;

            SetSortOrder(fields);
        }

        public void When(DynamicObjectDefinitionFieldMovedDown @event)
        {
            var definition = _repository.Get(@event.AggregateId);
            var field = GetField(definition, @event.Name);

            if (field == null)
            {
                return;
            }

            var fields = definition.Fields.ToArray();
            var index = 0;

            for (var i = 0; i < fields.Length; i++)
            {
                if (ReferenceEquals(fields[i], field))
                {
                    index = i;
                    break;
                }
            }

            if (index == 0)
            {
                // this is the title field
                return;
            }

            if (index == fields.Length - 1)
            {
                return;
            }

            var temp = fields[index + 1];
            fields[index + 1] = field;
            fields[index] = temp;

            SetSortOrder(fields);
        }

        public void When(DynamicObjectDefinitionFieldMarkedSortable @event)
        {
            var definition = _repository.Get(@event.AggregateId);
            var field = GetField(definition, @event.Name);

            if (field == null)
            {
                return;
            }

            field.IsSortableField = @event.IsSortable;
        }

        public void When(DynamicObjectDefinitionNameChanged @event)
        {
            var definition = _repository.Get(@event.AggregateId);

            definition.Name = @event.Name;
        }

        public void When(DynamicObjectDefinitionShared @event)
        {
            var definition = _repository.Get(@event.AggregateId);
            var user = _userRepository.Get(@event.UserId);
            var allowedTenantIds = user.GroupIds.Select(g => _groupRepository.Get(g).TenantId).ToList();
            var sharedTenantIds = @event.TenantIds.Union(definition.SharedForTenants).Distinct().ToList();
            sharedTenantIds.RemoveAll(t => allowedTenantIds.Contains(t) && !@event.TenantIds.Contains(t));
            definition.Share(sharedTenantIds);
        }

        public void When(TenantDeleted @event)
        {
            _repository.DeleteAllByTenantId(@event.AggregateId);
        }

        private DynamicObjectDefinitionField GetField(DynamicObjectDefinition definition, string name)
        {
            return definition.Fields.SingleOrDefault(f => f.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
        }

        private void SetSortOrder(IEnumerable<DynamicObjectDefinitionField> fields)
        {
            var i = 0;
            foreach (var field in fields)
            {
                field.SortOrder = i;
                i++;
            }
        }
    }
}
