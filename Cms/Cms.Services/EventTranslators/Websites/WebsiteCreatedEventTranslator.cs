﻿using System;
using Cms.Aggregates.Websites;
using Cms.Aggregates.Websites.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Websites
{
    internal class WebsiteCreatedEventTranslator : IEventTranslator
    {
        private readonly IWebsiteRepository _repository;

        public WebsiteCreatedEventTranslator(IWebsiteRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(WebsiteCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Neue Website erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (WebsiteCreated)@event;
            return string.Format("Die Website <strong>{0}</strong> wurde neu erstellt.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "website";
        }
    }
}