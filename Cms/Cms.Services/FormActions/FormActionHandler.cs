﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Commands;
using Cms.Aggregates.Settings;
using Cms.EventSourcing.Contracts.Events;
using Cms.SharedKernel;
using Cms.TemplateEngine;
using log4net;
using MailKit.Net.Smtp;
using MimeKit;

namespace Cms.Services.FormActions
{
    internal class FormActionHandler : CommandHandlerBase
    {
        private readonly IFormRepository _repository;
        private readonly ISettingsRepository _settingsRepository;
        private readonly IDynamicObjectRepository _objectRepository;
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;
        private readonly IUniqueIdProvider _idProvider;

        public FormActionHandler(IFormRepository repository, ISettingsRepository settingsRepository, IDynamicObjectDefinitionRepository definitionRepository, IUniqueIdProvider idProvider, IDynamicObjectRepository objectRepository)
        {
            _repository = repository;
            _settingsRepository = settingsRepository;
            _definitionRepository = definitionRepository;
            _idProvider = idProvider;
            _objectRepository = objectRepository;
        }

        public void When(CreateFormEntry command, Action<IEvent> eventHandler)
        {
            var form = _repository.Get(command.AggregateId);
            var fields = form.Fields.Select(f => f.Name).ToList();

            var values = command.Values
                .Where(p => fields.Contains(p.Key, StringComparer.OrdinalIgnoreCase))
                .ToDictionary(k => k.Key, k => k.Value.RemoveControlChars(), StringComparer.OrdinalIgnoreCase);

            values["IpAddress"] = command.IpAddress;

            if (!command.RunFormActions)
            {
                return;
            }

            foreach (var formAction in form.Actions.ToList())
            {
                try
                {
                    HandleFormAction(formAction, form, values, command.UserId, eventHandler);
                }
                catch (Exception e)
                {
                    var msg = string.Format(
                        "Error while processing form action {1} ({0}) for form {2} ({3}) and entry {4}.",
                        formAction.Id,
                        formAction.Type,
                        form.Name,
                        form.Id,
                        command.FormEntryId);
                    LogManager.GetLogger(GetType()).Error(msg, e);
                }
            }
        }

        private void HandleFormAction(FormAction action, Form form, Dictionary<string, string> values, ulong userId, Action<IEvent> eventHandler)
        {
            switch (action.Type)
            {
                case FormActionType.SendEmailToAddress:
                    HandleSendEmailToAddress((SendEmailToAddressFormAction)action.Configuration, form, values);
                    break;
                case FormActionType.PageForwarding:
                    break;
                case FormActionType.Persist:
                    break;
                case FormActionType.CreateObject:
                    HandleCreateObject((CreateObjectFormAction)action.Configuration, form, values, userId, eventHandler);
                    break;
                case FormActionType.SendEmailToField:
                    HandleSendEmailToField((SendEmailToFieldFormAction)action.Configuration, form, values);
                    break;
                default:
                    throw new NotSupportedException(action.Type.ToString()).WithGuid();
            }
        }

        private void HandleCreateObject(CreateObjectFormAction configuration, Form form, IDictionary<string, string> values, ulong userId, Action<IEvent> eventHandler)
        {
            ulong definitionId;
            if (!ulong.TryParse(configuration.ObjectDefinitionId, out definitionId))
            {
                throw new InvalidOperationException("Unable to parse object definition id.");
            }

            DynamicObjectDefinition definition;
            if (!_definitionRepository.TryGet(definitionId, out definition))
            {
                throw new InvalidOperationException("There is no object definition with this id. Maybe it have been deleted.");
            }

            Func<string, string> getValue = definitionFieldName =>
            {
                string fieldJsonId;
                ulong fieldId;
                if (configuration.Mapping.TryGetValue(definitionFieldName, out fieldJsonId) &&
                    ulong.TryParse(fieldJsonId, out fieldId))
                {
                    var field = form.Fields.SingleOrDefault(f => f.Id == fieldId);

                    if (field != null)
                    {
                        string value;
                        if (values.TryGetValue(field.Name, out value))
                        {
                            return value;
                        }
                    }
                }

                return string.Empty;
            };

            var objectValues = definition.Fields.Select(f => f.Name).ToDictionary(n => n, getValue);

            var @event = new DynamicObjectCreated
            {
                AggregateId = _idProvider.GetId(),
                UserId = userId,
                DefinitionId = definitionId,
                PublicId = _objectRepository.GetNextPublicId(),
                Values = objectValues,
            };

            eventHandler(@event);
        }

        private void HandleSendEmailToField(SendEmailToFieldFormAction configuration, Form form, IDictionary<string, string> values)
        {
            var field = form.Fields.SingleOrDefault(f => string.Equals(f.Id.ToJsonString(), configuration.FieldId, StringComparison.Ordinal));
            if (field == null)
            {
                throw new InvalidOperationException("Form field to send email to found.").WithGuid();
            }

            var emailContent = new EmailContent
            {
                Subject = configuration.EmailSubject,
                Recipient = values[field.Name],
                BodyText = configuration.EmailTemplateText,
                BodyHtml = configuration.EmailTemplateHtml,
            };

            var emailSettings = _settingsRepository.GetSettings(form.TenantId).EmailSettings;
            SendEmails(emailContent, emailSettings, values);
        }

        private void HandleSendEmailToAddress(SendEmailToAddressFormAction configuration, Form form, IDictionary<string, string> values)
        {
            var emailContent = new EmailContent
            {
                Subject = configuration.EmailSubject,
                Recipient = configuration.EmailAddress,
                BodyText = configuration.EmailTemplateText,
                BodyHtml = configuration.EmailTemplateHtml,
            };

            var emailSettings = _settingsRepository.GetSettings(form.TenantId).EmailSettings;
            SendEmails(emailContent, emailSettings, values);
        }

        private static void SendEmails(EmailContent emailContent, EmailSettings emailSettings, IDictionary<string, string> values)
        {
            var recipients = emailContent.Recipient.Split(new[] {' ', ',', ';'}, StringSplitOptions.RemoveEmptyEntries);

            foreach (var recipient in recipients)
            {
                var content = new EmailContent
                {
                    Recipient = recipient,
                    Subject = emailContent.Subject,
                    BodyHtml = emailContent.BodyHtml,
                    BodyText = emailContent.BodyText,
                };

                SendEmail(content, emailSettings, values);
            }
        }

        private static void SendEmail(EmailContent emailContent, EmailSettings emailSettings, IDictionary<string, string> values)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(emailSettings.EmailAddress, emailSettings.EmailAddress));
            message.To.Add(new MailboxAddress(emailContent.Recipient, emailContent.Recipient));
            message.Subject = FillTemplate(emailContent.Subject, values);

            if (!string.IsNullOrEmpty(emailSettings.ResponseAddress))
            {
                message.Headers.Add(new Header(HeaderId.ReplyTo, emailSettings.ResponseAddress));
            }

            message.Body = GetEmailBody(emailContent, values);

            using (var client = new SmtpClient())
            {
                client.Connect(emailSettings.Host, emailSettings.Port, emailSettings.IsSslUsedForAuthentication);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                if (emailSettings.IsSmtpAuthenticationRequired)
                {
                    client.Authenticate(emailSettings.SmtpUsername, emailSettings.SmtpPassword);
                }

                client.Send(message);
                client.Disconnect(true);
            }
        }

        private static MimeEntity GetEmailBody(EmailContent emailContent, IDictionary<string, string> values)
        {
            var templateText = FillTemplate(emailContent.BodyText, values);
            var templateHtml = FillTemplate(emailContent.BodyHtml, values);

            if (!string.IsNullOrEmpty(templateText) && !string.IsNullOrEmpty(templateHtml))
            {
                var bodyBuilder = new BodyBuilder
                {
                    TextBody = templateText,
                    HtmlBody = templateHtml
                };
                return bodyBuilder.ToMessageBody();
            }

            if (!string.IsNullOrEmpty(templateText))
            {
                return new TextPart("plain") { Text = templateText };
            }

            if (!string.IsNullOrEmpty(templateHtml))
            {
                return new TextPart("html") { Text = templateHtml };
            }

            throw new InvalidOperationException("There is no body specified.").WithGuid();
        }

        private static string FillTemplate(string template, IEnumerable<KeyValuePair<string, string>> values)
        {
            if (string.IsNullOrEmpty(template))
            {
                return string.Empty;
            }

            foreach (var pair in values)
            {
                var name = string.Format("$(\"{0}\")", pair.Key);
                template = Replace(template, name, pair.Value);
            }

            var engine = new Engine();
            template = engine.Generate(template);

            return template;
        }

        private static string Replace(string template, string oldValue, string newValue)
        {
            template = template.Replace(oldValue, newValue);

            oldValue = oldValue.Replace("&", "&amp;");
            oldValue = oldValue.Replace("ü", "&uuml;");
            oldValue = oldValue.Replace("ä", "&auml;");
            oldValue = oldValue.Replace("ö", "&ouml;");
            oldValue = oldValue.Replace("Ü", "&Uuml;");
            oldValue = oldValue.Replace("Ä", "&Auml;");
            oldValue = oldValue.Replace("Ö", "&Ouml;");
            oldValue = oldValue.Replace("<", "&lt;");
            oldValue = oldValue.Replace(">", "&gt;");
            oldValue = oldValue.Replace("¢", "&cent;");
            oldValue = oldValue.Replace("£", "&pound;");
            oldValue = oldValue.Replace("€", "&euro;");
            oldValue = oldValue.Replace("¥", "&yen;");
            oldValue = oldValue.Replace("©", "&copy;");
            oldValue = oldValue.Replace("®", "&reg;");
            oldValue = oldValue.Replace("'", "&apos;");

            template = template.Replace(oldValue, newValue);

            return template;
        }

        private class EmailContent
        {
            public string Recipient { get; set; }
            public string Subject { get; set; }
            public string BodyText { get; set; }
            public string BodyHtml { get; set; }
        }
    }
}
