﻿using Cms.Aggregates.Links;

namespace Cms.Aggregates
{
    public class PageReferenceLink : ILink
    {
        public ulong PageId { get; set; }
    }
}