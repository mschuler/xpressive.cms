namespace Cms.Aggregates.Forms
{
    public class SendEmailToFieldFormAction : FormActionConfiguration
    {
        public string FieldId { get; set; }
        public string FieldName { get; set; }
        public string EmailSubject { get; set; }
        public string EmailTemplateText { get; set; }
        public string EmailTemplateHtml { get; set; }

        public override FormActionConfiguration Clone()
        {
            return new SendEmailToFieldFormAction
            {
                FieldId = FieldId,
                FieldName = FieldName,
                EmailSubject = EmailSubject,
                EmailTemplateText = EmailTemplateText,
                EmailTemplateHtml = EmailTemplateHtml,
            };
        }
    }
}