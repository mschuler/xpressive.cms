using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("user", PermissionLevel.CanChange)]
    public class ChangeUserPassword : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Password { get; set; }
    }
}