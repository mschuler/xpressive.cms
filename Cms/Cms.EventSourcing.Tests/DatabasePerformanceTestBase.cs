using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using UniqueIdGenerator.Net;
using Xunit;

namespace Cms.EventSourcing.Tests
{
    public abstract class DatabasePerformanceTestBase
    {
        protected void StartTest()
        {
            var generator = new Generator(1, new DateTime(1990, 1, 1));
            var queue = new BlockingCollection<ulong>();
            const int rowCount = 10000;

            ClearTable();

            for (var i = 0; i < rowCount; i++)
            {
                queue.Add(generator.NextLong());
            }

            var tasks = new Task[10];
            for (var i = 0; i < 10; i++)
            {
                tasks[i] = Task.Run(() => InsertIntoDatabase(queue));
            }

            Task.WaitAll(tasks);

            var count = GetNumberOfRowsInTestTable();
            var threads = GetNumberOfDistinctThreads();
            Assert.Equal(rowCount, count);
            Assert.True(threads >= 2);
        }

        protected abstract void ClearTable();

        protected abstract int GetNumberOfRowsInTestTable();

        protected abstract int GetNumberOfDistinctThreads();

        protected abstract Task InsertIntoDatabase(BlockingCollection<ulong> queue);
    }
}