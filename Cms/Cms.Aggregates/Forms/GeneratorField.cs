using System.Collections.Generic;

namespace Cms.Aggregates.Forms
{
    public class GeneratorField
    {
        public ulong FieldId { get; set; }

        public int SortOrder { get; set; }

        public string Type { get; set; }

        public IEnumerable<string> Options { get; set; }

        public bool IsMultiselect { get; set; }

        public bool IsRequired { get; set; }

        protected bool Equals(GeneratorField other)
        {
            return FieldId == other.FieldId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }
            if (obj.GetType() != GetType()) { return false; }
            return Equals((GeneratorField)obj);
        }

        public override int GetHashCode()
        {
            return FieldId.GetHashCode();
        }
    }
}