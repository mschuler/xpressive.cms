using System.Collections.Generic;

namespace Cms.Aggregates.Pages
{
    public interface IHtmlTag
    {
        string Tag { get; }
        IEnumerable<IHtmlAttribute> Attributes { get; }
    }
}