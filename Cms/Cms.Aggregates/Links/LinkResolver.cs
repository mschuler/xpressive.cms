using System;
using System.Collections.Generic;
using System.Linq;
using Cms.SharedKernel;

namespace Cms.Aggregates.Links
{
    internal class LinkResolver : ILinkResolver
    {
        private readonly IList<ILinkResolverStrategy> _strategies;

        public LinkResolver(IList<ILinkResolverStrategy> strategies)
        {
            _strategies = strategies;
        }

        public string GetHyperlink(ILink link)
        {
            if (link == null)
            {
                throw new ArgumentNullException("link").WithGuid();
            }

            var strategy = _strategies.FirstOrDefault(s => s.CanHandle(link));

            if (strategy == null)
            {
                throw new InvalidOperationException("There is no link resolver for link type " + link.GetType().Name).WithGuid();
            }

            return strategy.GetHyperlink(link);
        }
    }
}