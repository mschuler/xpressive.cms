using Cms.Aggregates.Security;

namespace Cms.Aggregates.Websites.Queries
{
    [RequiredPermission("website", PermissionLevel.CanSee)]
    public class GetWebsiteWithDomains : QueryBase
    {
        public ulong WebsiteId { get; set; }
    }
}