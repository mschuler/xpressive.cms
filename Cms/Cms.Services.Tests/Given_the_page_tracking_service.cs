﻿using System.Globalization;
using Cms.Services.Tracking;
using Xunit;
using Xunit.Abstractions;

namespace Cms.Services.Tests
{
    public class Given_the_page_tracking_service
    {
        private readonly PageTrackingService _service = new PageTrackingService(null);
        private readonly ITestOutputHelper _outputHelper;

        public Given_the_page_tracking_service(ITestOutputHelper outputHelper)
        {
            _outputHelper = outputHelper;
        }

        [Fact]
        public void GetAveragePageViewsPerVisit()
        {
            _outputHelper.WriteLine(_service.GetAveragePageViewsPerVisit(1).ToString(CultureInfo.InvariantCulture));
        }

        [Fact]
        public void GetReturningVisitorsInPercent()
        {
            _outputHelper.WriteLine(_service.GetReturningVisitorsInPercent(1).ToString(CultureInfo.InvariantCulture));
        }

        [Fact]
        public void GetTheTenMostViewedPagesForOneMonth()
        {
            foreach (var tuple in _service.GetTheTenMostViewedPagesForOneMonth(1))
            {
                _outputHelper.WriteLine("{0}: {1}", tuple.Item1, tuple.Item2);
            }
        }

        [Fact]
        public void GetTotalPageViewsForOneMonth()
        {
            _outputHelper.WriteLine(_service.GetTotalPageViewsForOneMonth(1).ToString(CultureInfo.InvariantCulture));
        }

        [Fact]
        public void GetTotalSessionsForOneMonth()
        {
            _outputHelper.WriteLine(_service.GetTotalSessionsForOneMonth(1).ToString(CultureInfo.InvariantCulture));
        }

        [Fact]
        public void GetTotalPageViewsPerDay()
        {
            foreach (var tuple in _service.GetTotalPageViewsPerDay(1))
            {
                _outputHelper.WriteLine("{0}: {1}", tuple.Item1.ToString("yyyy-MM-dd"), tuple.Item2);
            }
        }

        [Fact]
        public void GetTotalPageViewsPerMonth()
        {
            foreach (var tuple in _service.GetTotalPageViewsPerMonth(1))
            {
                _outputHelper.WriteLine("{0}: {1}", tuple.Item1.ToString("yyyy-MM"), tuple.Item2);
            }
        }
    }
}
