﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Cms.Services")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("81c904dd-559d-4cc6-af58-f453c14eeecb")]

[assembly: InternalsVisibleTo("Cms.Services.Tests")]
[assembly: InternalsVisibleTo("Cms.WebNew.Tests")]
