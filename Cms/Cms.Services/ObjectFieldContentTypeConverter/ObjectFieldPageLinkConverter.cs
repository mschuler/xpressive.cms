using System;
using Cms.Aggregates;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Websites;
using Cms.SharedKernel;
using log4net;
using Newtonsoft.Json;

namespace Cms.Services.ObjectFieldContentTypeConverter
{
    internal class ObjectFieldPageLinkConverter : IObjectFieldContentTypeConverter
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ObjectFieldPageLinkConverter));
        private readonly IPageRepository _repository;
        private readonly IPageUrlService _pageUrlService;
        private readonly IPageDtoService _pageDtoService;
        private readonly IWebsiteRepository _websiteRepository;

        public ObjectFieldPageLinkConverter(IPageRepository repository, IPageUrlService pageUrlService, IPageDtoService pageDtoService, IWebsiteRepository websiteRepository)
        {
            _repository = repository;
            _pageUrlService = pageUrlService;
            _pageDtoService = pageDtoService;
            _websiteRepository = websiteRepository;
        }

        public bool CanHandle(string type)
        {
            return type.Equals("pagelink", StringComparison.OrdinalIgnoreCase);
        }

        public string Serialize(string json)
        {
            Dto dto;

            try
            {
                dto = JsonConvert.DeserializeObject<Dto>(json);
            }
            catch (Exception)
            {
                _log.ErrorFormat("Unable to deserialize to Dto for json={0}", json);
                return string.Empty;
            }

            if (dto == null)
            {
                return string.Empty;
            }

            if ("page".Equals(dto.LinkType, StringComparison.OrdinalIgnoreCase))
            {
                ulong id;
                if (dto.PageLink != null && ulong.TryParse(dto.PageLink.Id, out id))
                {
                    return id.ToJsonString();
                }
            }
            else if ("external".Equals(dto.LinkType, StringComparison.OrdinalIgnoreCase))
            {
                return dto.ExternalLink;
            }

            return string.Empty;
        }

        public object Deserialize(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new Dto
                {
                    ExternalLink = string.Empty,
                    LinkType = "none",
                    PageLink = null,
                };
            }

            ulong pageId;
            PageBase page;
            if (ulong.TryParse(value, out pageId) && _repository.TryGet(pageId, out page))
            {
                var website = _websiteRepository.Get(page.WebsiteId);

                return new
                {
                    ExternalLink = string.Empty,
                    LinkType = "page",
                    PageLink = _pageDtoService.GetPageDto(website, page)
                };
            }

            return new Dto
            {
                ExternalLink = value,
                LinkType = "external",
                PageLink = null,
            };
        }

        public SortableObjectField DeserializeForSortableField(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new SortableObjectField();
            }

            ulong pageId;
            PageBase page;
            if (ulong.TryParse(value, out pageId) && _repository.TryGet(pageId, out page))
            {
                return new SortableObjectField(page.Fullname);
            }

            return new SortableObjectField(value);
        }

        public object DeserializeForPublicApi(string value)
        {
            return DeserializeToLink(value);
        }

        public bool TryGetMetadataForPublicApi(string value, out object metadata)
        {
            metadata = null;
            return false;
        }

        public object DeserializeForInterpreter(string value)
        {
            return DeserializeToLink(value);
        }

        public string SerializeForImport(string json, string format, string options)
        {
            int publicId;
            if (int.TryParse(json, out publicId))
            {
                var page = _repository.GetByPublicId(publicId);
                if (page != null)
                {
                    return page.Id.ToJsonString();
                }
            }

            if (string.IsNullOrEmpty(json) || json.StartsWith("http", StringComparison.OrdinalIgnoreCase))
            {
                return json;
            }

            return string.Empty;
        }

        public string SerializeForImportPreview(string json, string format, string options)
        {
            int publicId;
            if (int.TryParse(json, out publicId))
            {
                var page = _repository.GetByPublicId(publicId);
                if (page != null)
                {
                    return page.Name + " (Nr. " + page.PublicId + ")";
                }
            }

            if (string.IsNullOrEmpty(json) || json.StartsWith("http", StringComparison.OrdinalIgnoreCase))
            {
                return json;
            }

            return string.Empty;
        }

        public bool IsValidValueForImport(string json, string format, string options)
        {
            if (string.IsNullOrEmpty(json) || json.StartsWith("http", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            int publicId;
            if (int.TryParse(json, out publicId))
            {
                var page = _repository.GetByPublicId(publicId);
                return page != null;
            }

            return false;
        }

        private string DeserializeToLink(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            ulong pageId;
            PageBase page;
            if (ulong.TryParse(value, out pageId) && _repository.TryGet(pageId, out page))
            {
                return _pageUrlService.GetUrl(page);
            }

            return value;
        }

        public class Dto
        {
            public string LinkType { get; set; }
            public PageLinkDto PageLink { get; set; }
            public string ExternalLink { get; set; }
        }

        public class PageLinkDto
        {
            public string Id { get; set; }
        }
    }
}