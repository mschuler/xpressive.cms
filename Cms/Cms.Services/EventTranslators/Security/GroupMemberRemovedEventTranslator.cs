﻿using System;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class GroupMemberRemovedEventTranslator : IEventTranslator
    {
        private readonly IGroupRepository _groupRepository;
        private readonly IUserRepository _userRepository;

        public GroupMemberRemovedEventTranslator(IGroupRepository groupRepository, IUserRepository userRepository)
        {
            _groupRepository = groupRepository;
            _userRepository = userRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(GroupMemberRemoved);
        }

        public string GetHeader(IEvent @event)
        {
            return "Mitglied entfernt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (GroupMemberRemoved)@event;
            var group = _groupRepository.GetEvenIfDeleted(e.AggregateId);
            var user = _userRepository.GetEvenIfDeleted(e.MemberId);
            return string.Format(
                "Der Benutzer <strong>{0}</strong> wurde von der Gruppe <strong>{1}</strong> entfernt.",
                user.FullName,
                group.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _groupRepository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "group";
        }
    }
}