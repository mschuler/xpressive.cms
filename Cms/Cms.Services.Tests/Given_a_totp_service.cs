﻿using System;
using Cms.Services._2FA;
using Xunit;
using Xunit.Abstractions;

namespace Cms.Services.Tests
{
    public class Given_a_totp_service
    {
        private readonly ITestOutputHelper _outputWriter;

        public Given_a_totp_service(ITestOutputHelper outputWriter)
        {
            _outputWriter = outputWriter;
        }

        [Fact]
        public void Generate()
        {
            var service = new TotpService();

            var totp = service.Generate("username");

            _outputWriter.WriteLine("Secret (base32): {0}", totp.SecretBase32);
            _outputWriter.WriteLine("Secret (base64): {0}", Convert.ToBase64String(totp.Secret));
            _outputWriter.WriteLine("Url: {0}", totp.Url);
        }
    }
}
