﻿using System.Diagnostics;
using System.Linq;
using Irony.Ast;
using Irony.Parsing;
using Xunit;
using Xunit.Abstractions;

namespace Cms.TemplateEngine.Tests
{
    public class Given_a_grammar
    {
        private static readonly string[] _expressions =
        {
            "now()",
            "now().toRfc1123()",
            "now().format(\"dd.MM.yyyy HH:mm:ss\")",
            "now().format('dd.MM.yyyy HH:mm:ss')",
            "now().date",
            "page(1234)",
            "page(1234).title",
            "page(1234).parent.title",
            "page(1234).render()",
            "page(1234).parent.render()",
            "website(this())",
            "website(this()).defaultpage",
            "website(this()).defaultpage.render()",
            "$('feldname')",
            "$(\"feldname\").parent.id",
            "youtube(\"asdf\", 1920, 1080)",
            "vimeo(111593305, 1920, 1080, \"black\")",
            "pages(1).union(pages(2))",
            "objects(1).where('field(\"feldname\").bytesize', 'between', '100|10000')",
            "page(1).template(\"{ $('id') }\")",
        };

        private readonly ITestOutputHelper _outputWriter;

        public Given_a_grammar(ITestOutputHelper outputWriter)
        {
            _outputWriter = outputWriter;
        }

        [Fact]
        public void Then_all_expressions_are_valid()
        {
            foreach (var expression in _expressions)
            {
                Assert.True(IsValid(expression), expression);
            }
        }

        [Fact]
        public void Then_parsing_is_fast()
        {
            var language = new LanguageData(new ExpressionGrammar());
            var parser = new Irony.Parsing.Parser(language);

            var stopwatch = Stopwatch.StartNew();

            foreach (var expression in _expressions)
            {
                parser.Parse(expression);
            }

            stopwatch.Stop();

            _outputWriter.WriteLine("Took {0}ms to parse {1} expressions.", stopwatch.ElapsedMilliseconds, _expressions.Length);
        }

        [Fact]
        public void Then_there_is_a_tree()
        {
            foreach (var expression in _expressions)
            {
                _outputWriter.WriteLine("===================================================");
                _outputWriter.WriteLine(expression);
                _outputWriter.WriteLine("");

                var tree = GetTree(expression);
                AddParseNodeRec(0, tree.Root);
                //AddAstNodeRec(0, tree.Root.AstNode);

                _outputWriter.WriteLine("");
            }
        }

        [Fact]
        public void Then_expression_extractor_works_with_inline_brackets()
        {
            var template = "<div> { pages(1).template(\"<span>{ $('title') }</span>\") } </div>";
            var extractor = new ExpressionExtractor();

            var expressions = extractor.GetAllExpressions(template);

            Assert.Equal(1, expressions.Count);
            Assert.Equal("{ pages(1).template(\"<span>{ $('title') }</span>\") }", expressions.Single().FullName);
        }

        private void AddParseNodeRec(int level, ParseTreeNode node)
        {
            if (node == null)
            {
                return;
            }

            _outputWriter.WriteLine(new string(' ', level * 2) + node);

            foreach (var child in node.ChildNodes)
            {
                AddParseNodeRec(level + 1, child);
            }
        }

        private void AddAstNodeRec(int level, object astNode)
        {
            if (astNode == null)
            {
                return;
            }

            _outputWriter.WriteLine(new string(' ', level * 2) + astNode);

            var iBrowsable = astNode as IBrowsableAstNode;
            if (iBrowsable == null)
            {
                return;
            }

            var childList = iBrowsable.GetChildNodes();
            foreach (var child in childList)
            {
                AddAstNodeRec(level + 1, child);
            }
        }

        private ParseTree GetTree(string expression)
        {
            var language = new LanguageData(new ExpressionGrammar());
            var parser = new Irony.Parsing.Parser(language);
            var parseTree = parser.Parse(expression);

            return parser.Context.CurrentParseTree;

            return parseTree;
        }

        private bool IsValid(string expression)
        {
            var language = new LanguageData(new ExpressionGrammar());
            var parser = new Irony.Parsing.Parser(language);
            var parseTree = parser.Parse(expression);
            var root = parseTree.Root;
            return root != null;
        }
    }
}
