namespace Cms.Aggregates.Pages
{
    public interface IPageField
    {
        string Name { get; set; }

        string Type { get; set; }

        int SortOrder { get; set; }

        bool CanChangeType { get; set; }
    }
}