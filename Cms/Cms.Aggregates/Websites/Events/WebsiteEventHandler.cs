using System;
using System.Linq;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.Tenants.Events;

namespace Cms.Aggregates.Websites.Events
{
    public class WebsiteEventHandler : EventHandlerBase
    {
        private readonly IWebsiteRepository _repository;

        public WebsiteEventHandler(IWebsiteRepository repository)
        {
            _repository = repository;
        }

        public void When(WebsiteCreated @event)
        {
            var website = new Website
            {
                Id = @event.AggregateId,
                TenantId = @event.TenantId,
                Name = @event.Name,
                PublicId = @event.PublicId,
            };

            _repository.Add(website);
        }

        public void When(WebsiteNameChanged @event)
        {
            var website = _repository.Get(@event.AggregateId);
            website.Name = @event.Name;
        }

        public void When(WebsiteDeleted @event)
        {
            _repository.Delete(@event.AggregateId);
        }

        public void When(DomainAdded @event)
        {
            var website = _repository.Get(@event.AggregateId);
            var domain = new Domain { Host = @event.Host };
            website.AddDomain(domain);
        }

        public void When(DomainHostChanged @event)
        {
            var website = _repository.Get(@event.AggregateId);

            foreach (var domain in website.Domains)
            {
                if (domain.Host.Equals(@event.OldHost, StringComparison.OrdinalIgnoreCase))
                {
                    domain.Host = @event.NewHost;
                }
            }
        }

        public void When(DomainRemoved @event)
        {
            var website = _repository.Get(@event.AggregateId);
            var domain = website.Domains.Single(d => d.Host.Equals(@event.Host, StringComparison.OrdinalIgnoreCase));
            website.RemoveDomain(domain);
        }

        public void When(DefaultDomainChanged @event)
        {
            var website = _repository.Get(@event.AggregateId);
            website.DefaultDomain = website.Domains.Single(d => d.Host.Equals(@event.Host, StringComparison.OrdinalIgnoreCase));
        }

        public void When(DefaultPageSet @event)
        {
            var website = _repository.Get(@event.AggregateId);
            website.DefaultPageId = @event.PageId;
        }

        public void When(PageDeleted @event)
        {
            var websites = _repository.GetAll();
            foreach (var website in websites)
            {
                if (website.DefaultPageId == @event.AggregateId)
                {
                    website.DefaultPageId = 0;
                }
            }
        }

        public void When(TenantDeleted @event)
        {
            _repository.DeleteAllByTenantId(@event.AggregateId);
        }
    }
}