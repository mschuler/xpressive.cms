﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectTemplateRenamed : EventBase<DynamicObjectDefinition>
    {
        public ulong TemplateId { get; set; }

        public string Name { get; set; }

        public string OldName { get; set; }
    }
}