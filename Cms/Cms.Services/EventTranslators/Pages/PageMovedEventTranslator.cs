using System;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.Websites;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Pages
{
    internal class PageMovedEventTranslator : IEventTranslator
    {
        private readonly IPageRepository _repository;
        private readonly IWebsiteRepository _websiteRepository;

        public PageMovedEventTranslator(IPageRepository repository, IWebsiteRepository websiteRepository)
        {
            _repository = repository;
            _websiteRepository = websiteRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageMoved);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seite verschoben";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageMoved)@event;
            var page = _repository.GetEvenIfDeleted(e.AggregateId);
            var parent = e.ParentPageId > 0 ? _repository.GetEvenIfDeleted(e.ParentPageId) : null;
            var parentName = parent != null ? parent.Fullname : "ROOT";
            return string.Format("Die Seite <strong>{0}</strong> wurde nach <strong>{1}</strong> verschoben.", page.Fullname, parentName);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var page = _repository.GetEvenIfDeleted(@event.AggregateId);
            return _websiteRepository.GetEvenIfDeleted(page.WebsiteId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "page";
        }
    }
}