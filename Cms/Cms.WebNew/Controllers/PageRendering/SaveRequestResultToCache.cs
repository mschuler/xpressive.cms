using System;
using System.Runtime.Caching;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class SaveRequestResultToCache : RequestCacheFilterBase
    {
        public override bool Execute(RenderPageMessage msg)
        {
            var cachedPage = new CachedPage
            {
                Content = msg.Content,
                ContentType = msg.PageTemplate.ContentType
            };

            MemoryCache.Default.Add(GetKey(msg), cachedPage, DateTimeOffset.Now.AddSeconds(2));

            return true;
        }
    }
}