﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.FileDirectories.Commands
{
    [RequiredPermission("file", PermissionLevel.CanChange)]
    public class ChangeFileDirectoryName : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }
    }
}