﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Calendars.Queries
{
    internal class AppointmentQueryHandler : QueryHandlerBase
    {
        private readonly ICalendarRepository _repository;
        private readonly IPageRepository _pageRepository;

        public AppointmentQueryHandler(ICalendarRepository repository, IPageRepository pageRepository)
        {
            _repository = repository;
            _pageRepository = pageRepository;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetAppointmentOccurrences),
                typeof (GetAppointment)
            };
        }

        public IEnumerable<object> When(GetAppointmentOccurrences query)
        {
            var range = new DateTimeRange(query.StartDate.Date, query.EndDate.Date);
            var occurrences = _repository.GetAppointments(query.TenantId, range);

            foreach (var occurrence in occurrences)
            {
                yield return new
                {
                    AppointmentId = occurrence.AppointmentId.ToJsonString(),
                    occurrence.Title,
                    occurrence.IsHighlight,
                    occurrence.StartEnd.Start,
                    occurrence.StartEnd.End,
                    occurrence.IsSingleOccurrence,
                    occurrence.Location,
                };
            }
        }

        public IEnumerable<object> When(GetAppointment query)
        {
            var appointment = _repository.GetAppointment(query.TenantId, query.AppointmentId);
            var recurrence = GetRecurrenceType(appointment);
            var link = GetLinkDto(appointment);
            var categories = appointment.Categories.Select(c => new { c.Name }).ToList();

            yield return new
            {
                Id = appointment.Id.ToJsonString(),
                appointment.Location,
                appointment.Title,
                appointment.IsHighlight,
                appointment.StartDate,
                StartTime = appointment.StartEnd.Start.ToString("HH:mm"),
                appointment.EndDate,
                EndTime = appointment.StartEnd.End.ToString("HH:mm"),
                Recurrence = recurrence,
                RecurrenceOptions = appointment.Recurrence,
                Link = link,
                Categories = categories,
            };
        }

        private string GetRecurrenceType(Appointment appointment)
        {
            var recurrenceType = appointment.Recurrence.GetType();

            if (recurrenceType == typeof(DailyRecurrence)) { return "daily"; }
            if (recurrenceType == typeof(WeeklyRecurrence)) { return "weekly"; }
            if (recurrenceType == typeof(MonthlyRecurrence)) { return "monthly"; }
            if (recurrenceType == typeof(YearlyRecurrence)) { return "yearly"; }

            return "single";
        }

        private object GetLinkDto(Appointment appointment)
        {
            var hyperlink = appointment.Link as Hyperlink;
            var pageLink = appointment.Link as PageReferenceLink;
            PageBase page;

            if (hyperlink != null)
            {
                return new
                {
                    LinkType = "external",
                    ExternalLink = hyperlink.Url
                };
            }

            if (pageLink != null && _pageRepository.TryGet(pageLink.PageId, out page))
            {
                var p = page as Page;

                var fullname = page.Fullname;
                var enabled = true;
                var nr = 0;

                if (p != null)
                {
                    enabled = p.IsEnabled;
                    nr = p.PublicId;
                    if (!p.IsEnabled)
                    {
                        fullname += " [inaktiv]";
                    }
                }

                return new
                {
                    LinkType = "page",
                    PageLink = new
                    {
                        Id = pageLink.PageId.ToJsonString(),
                        Label = page.Name,
                        Name = page.Name,
                        Nr = nr,
                        Disabled = !enabled,
                        Fullname = fullname,
                        page.IsLink,
                    }
                };
            }

            return new
            {
                LinkType = "none"
            };
        }
    }

    [RequiredPermission("appointment", PermissionLevel.CanSee)]
    public class GetAppointmentOccurrences : QueryBase
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    [RequiredPermission("appointment", PermissionLevel.CanSee)]
    public class GetAppointment : QueryBase
    {
        public ulong AppointmentId { get; set; }
    }
}
