namespace Cms.Aggregates.Files.Events
{
    public class FileDirectoryChanged : EventBase<File>
    {
        public ulong FileDirectoryId { get; set; }
    }
}