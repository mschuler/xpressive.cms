using System;
using System.Net.Http.Headers;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class WriteTrackingCookie : IFilter<RenderPageMessage>
    {
        public bool Execute(RenderPageMessage msg)
        {
            foreach (var cookie in msg.Cookies)
            {
                foreach (var cookieState in cookie.Cookies)
                {
                    if (cookieState.Name.Equals("tsid", StringComparison.Ordinal))
                    {
                        msg.TrackingSessionId = cookieState.Value;
                        cookie.HttpOnly = true;
                    }

                    if (cookieState.Name.Equals("tpid", StringComparison.Ordinal))
                    {
                        msg.TrackingPersistedId = cookieState.Value;
                        cookie.HttpOnly = true;
                        cookie.Expires = new DateTimeOffset(DateTime.Today.AddMonths(6));
                    }
                }
            }

            if (string.IsNullOrEmpty(msg.TrackingSessionId))
            {
                msg.TrackingSessionId = Guid.NewGuid().ToString("N");

                msg.Cookies.Add(new CookieHeaderValue("tsid", msg.TrackingSessionId)
                {
                    HttpOnly = true
                });
            }

            if (string.IsNullOrEmpty(msg.TrackingPersistedId))
            {
                msg.TrackingPersistedId = Guid.NewGuid().ToString("N");

                msg.Cookies.Add(new CookieHeaderValue("tpid", msg.TrackingPersistedId)
                {
                    HttpOnly = true,
                    Expires = new DateTimeOffset(DateTime.Today.AddMonths(6))
                });
            }

            return true;
        }
    }
}