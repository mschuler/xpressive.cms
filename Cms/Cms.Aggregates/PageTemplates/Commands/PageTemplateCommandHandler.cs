using System;
using System.Linq;
using Cms.Aggregates.Files;
using Cms.Aggregates.PageTemplates.Events;
using Cms.Aggregates.Tenants;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.PageTemplates.Commands
{
    public class PageTemplateCommandHandler : CommandHandlerBase
    {
        private readonly IPageTemplateRepository _repository;
        private readonly IFileRepository _fileRepository;
        private readonly ITenantRepository _tenantRepository;

        public PageTemplateCommandHandler(IPageTemplateRepository repository, IFileRepository fileRepository, ITenantRepository tenantRepository)
        {
            _repository = repository;
            _fileRepository = fileRepository;
            _tenantRepository = tenantRepository;
        }

        public void When(CreatePageTemplate command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            var name = command.Name.Trim().RemoveControlChars();

            if (_repository.GetAll(command.TenantId).Any(t => t.Name.Equals(name, StringComparison.OrdinalIgnoreCase)))
            {
                throw new ExceptionWithReason("There is already a template with this name.");
            }

            var e = new PageTemplateCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TenantId = command.TenantId,
                Name = name,
                PublicId = command.PublicId,
                ContentType = command.ContentType,
            };

            eventHandler(e);
        }

        public void When(ChangePageTemplateName command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var template = _repository.Get(command.AggregateId);

            if (_repository.GetAll(template.TenantId).Where(t => t.Id != template.Id).Any(t => t.Name.Equals(name, StringComparison.OrdinalIgnoreCase)))
            {
                throw new ExceptionWithReason("There is already a template with this name.");
            }

            var e = new PageTemplateNameChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(ChangePageTemplateContentType command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);
            var contentType = command.ContentType.Trim().RemoveControlChars();

            if (string.IsNullOrEmpty(contentType))
            {
                throw new ExceptionWithReason("This is not a valid content type.");
            }

            var e = new PageTemplateContentTypeChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                ContentType = contentType,
            };

            eventHandler(e);
        }

        public void When(ChangePageTemplateIcon command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            File file;
            if (command.IconFileId > 0 && !_fileRepository.TryGet(command.IconFileId, out file))
            {
                throw new ExceptionWithReason("There is no file with this id.");
            }

            var e = new PageTemplateIconChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                IconFileId = command.IconFileId,
            };

            eventHandler(e);
        }

        public void When(ChangePageTemplateTemplate command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);
            var template = command.Template.Trim().RemoveControlChars();

            var e = new PageTemplateTemplateChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Template = template,
            };

            eventHandler(e);
        }

        public void When(SharePageTemplate command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            foreach (var tenantId in command.TenantIds)
            {
                _tenantRepository.Get(tenantId);
            }

            var e = new PageTemplateShared
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TenantIds = command.TenantIds.ToList(),
            };

            eventHandler(e);
        }

        public void When(DeletePageTemplate command, Action<IEvent> eventHandler)
        {
            var template = _repository.Get(command.AggregateId);

            var e = new PageTemplateDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = template.Name,
            };

            eventHandler(e);
        }
    }
}