﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Pages.Commands
{
    [RequiredPermission("page", PermissionLevel.CanChange)]
    public class ChangePageContent : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }

        [Required]
        public PageContentValueBase Value { get; set; }
    }
}