﻿using System;
using System.Linq;
using Cms.Aggregates.Tenants.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.Tenants.Commands
{
    public class TenantCommandHandler : CommandHandlerBase
    {
        private readonly ITenantRepository _repository;

        public TenantCommandHandler(ITenantRepository repository)
        {
            _repository = repository;
        }

        public void When(CreateTenant command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            var name = command.Name.Trim().RemoveControlChars();

            var isOtherWithSameName = _repository
                .GetAll()
                .Any(t => t.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

            if (isOtherWithSameName)
            {
                throw new ExceptionWithReason("There is already a tenant with this name.");
            }

            var e = new TenantCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(ChangeTenantName command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);
            var name = command.Name.Trim().RemoveControlChars();

            var isOtherWithSameName = _repository
                .GetAll()
                .Where(t => t.Id != command.AggregateId)
                .Any(t => t.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

            if (isOtherWithSameName)
            {
                throw new ExceptionWithReason("There is already a tenant with this name.");
            }

            var e = new TenantNameChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(DeleteTenant command, Action<IEvent> eventHandler)
        {
            var tenant = _repository.Get(command.AggregateId);
            var e = new TenantDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = tenant.Name,
            };

            eventHandler(e);
        }
    }
}