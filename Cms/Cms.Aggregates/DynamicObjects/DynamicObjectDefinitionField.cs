﻿namespace Cms.Aggregates.DynamicObjects
{
    public class DynamicObjectDefinitionField
    {
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string ContentType { get; set; }
        public string ContentTypeOptions { get; set; }
        public bool IsSortableField { get; set; }
    }
}