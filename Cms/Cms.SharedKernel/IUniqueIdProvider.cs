﻿namespace Cms.SharedKernel
{
    public interface IUniqueIdProvider
    {
        ulong GetId();
    }
}