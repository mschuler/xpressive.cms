﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.Tenants.Events;
using Cms.SharedKernel;
using log4net;

namespace Cms.Aggregates.Forms.Events
{
    public class FormEventHandler : EventHandlerBase
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(FormEventHandler));

        private readonly IFormRepository _repository;
        private readonly IPageRepository _pageRepository;
        private readonly IUniqueIdProvider _idProvider;

        public FormEventHandler(IFormRepository repository, IPageRepository pageRepository, IUniqueIdProvider idProvider)
        {
            _repository = repository;
            _pageRepository = pageRepository;
            _idProvider = idProvider;
        }

        public void When(NewFormCreated @event)
        {
            var form = new Form(@event.TenantId, @event.AggregateId, @event.Name, @event.PublicId);
            var template = string.Format("<form method=\"POST\" action=\"/api/form/{0}/entry\">", @event.AggregateId);
            template += Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine;
            template += "</form>";
            form.Template = template;
            _repository.Add(form);
        }

        public void When(FormDuplicated @event)
        {
            var existing = _repository.Get(@event.FormIdToDuplicate);
            var form = new Form(existing.TenantId, @event.AggregateId, @event.Name, @event.PublicId);
            form.Template = existing.Template.Replace(existing.Id.ToString("D"), form.Id.ToString("D"));
            form.GeneratorLabelPosition = existing.GeneratorLabelPosition;
            form.IsDuplicate = true;

            var fieldIdMapping = new Dictionary<ulong, ulong>();

            foreach (var formField in existing.Fields)
            {
                var id = _idProvider.GetId();
                fieldIdMapping.Add(formField.Id, id);

                form.Add(new FormField
                {
                    Id = id,
                    Name = formField.Name,
                });
            }

            foreach (var generatorField in existing.GeneratorFields)
            {
                ulong fieldId;
                if (!fieldIdMapping.TryGetValue(generatorField.FieldId, out fieldId))
                {
                    _log.WarnFormat("Unable to copy generator field with id {2} for form {0} ({1}).", form.Name, form.Id, generatorField.FieldId);
                    continue;
                }

                form.Add(new GeneratorField
                {
                    FieldId = fieldIdMapping[generatorField.FieldId],
                    SortOrder = generatorField.SortOrder,
                    Type = generatorField.Type,
                    IsMultiselect = generatorField.IsMultiselect,
                    Options = generatorField.Options.ToList(),
                });
            }

            foreach (var formAction in existing.Actions)
            {
                var action = new FormAction
                {
                    Id = _idProvider.GetId(),
                    Type = formAction.Type,
                    Configuration = formAction.Configuration.Clone(),
                };

                var sendEmailToFieldFormAction = action.Configuration as SendEmailToFieldFormAction;
                if (sendEmailToFieldFormAction != null)
                {
                    ulong fieldId;
                    if (!fieldIdMapping.TryGetValue(ulong.Parse(sendEmailToFieldFormAction.FieldId), out fieldId))
                    {
                        _log.WarnFormat("Unable to copy SendEmailToFieldFormAction for form {1} ({2}) because field with id {0} not found.", sendEmailToFieldFormAction.FieldId, form.Name, form.Id);
                        continue;
                    }
                    sendEmailToFieldFormAction.FieldId = fieldId.ToString("D");
                }

                form.Add(action);
            }

            _repository.Add(form);
        }

        public void When(FormEntriesCleared @event)
        {
            var form = _repository.Get(@event.AggregateId);
            form.ClearEntries();
        }

        public void When(FormNameChanged @event)
        {
            var form = _repository.Get(@event.AggregateId);
            form.Name = @event.Name;
        }

        public void When(FormTemplateChanged @event)
        {
            var form = _repository.Get(@event.AggregateId);
            form.Template = @event.Template;
        }

        public void When(FormDeleted @event)
        {
            _repository.Delete(@event.AggregateId);
        }

        public void When(FormFieldAdded @event)
        {
            var form = _repository.Get(@event.AggregateId);
            var formField = new FormField
            {
                Id = @event.Id,
                Name = @event.Name,
            };

            form.Add(formField);
        }

        public void When(FormFieldChanged @event)
        {
            var form = _repository.Get(@event.AggregateId);
            var field = form.Fields.SingleOrDefault(f => f.Id == @event.Id);

            if (field == null)
            {
                return;
            }

            field.Name = @event.Name;
        }

        public void When(FormFieldRemoved @event)
        {
            var form = _repository.Get(@event.AggregateId);
            var field = form.Fields.SingleOrDefault(f => f.Id == @event.Id);

            if (field == null)
            {
                return;
            }

            form.Remove(field);
        }

        public void When(FormEntryCreated @event)
        {
            var entry = new FormEntry
            {
                Id = @event.FormEntryId,
                Timestamp = @event.Timestamp,
                IpAddress = @event.IpAddress,
                IsSpam = @event.IsSpam,
                Values = new Dictionary<string, string>(@event.Values, StringComparer.OrdinalIgnoreCase),
            };
            var form = _repository.Get(@event.AggregateId);
            form.Add(entry);
        }

        public void When(FormEntryChanged @event)
        {
            var form = _repository.Get(@event.AggregateId);
            var entry = form.Entries.SingleOrDefault(e => e.Id == @event.FormEntryId);
            if (entry == null)
            {
                return;
            }

            foreach (var pair in @event.Values)
            {
                if (entry.Values.ContainsKey(pair.Key))
                {
                    entry.Values[pair.Key] = pair.Value;
                }
                else
                {
                    entry.Values.Add(pair.Key, pair.Value);
                }
            }
        }

        public void When(FormEntryDeleted @event)
        {
            var form = _repository.Get(@event.AggregateId);
            var entry = form.Entries.SingleOrDefault(e => e.Id == @event.FormEntryId);

            if (entry == null)
            {
                return;
            }

            form.Remove(entry);
        }

        public void When(FormActionAdded @event)
        {
            var form = _repository.Get(@event.AggregateId);
            var action = new FormAction
            {
                Id = @event.Id,
                Type = @event.Type,
                Configuration = @event.Configuration,
            };
            form.Add(action);
        }

        public void When(FormActionChanged @event)
        {
            var form = _repository.Get(@event.AggregateId);
            var action = form.Actions.SingleOrDefault(a => a.Id == @event.Id);

            if (action != null)
            {
                action.Type = @event.Type;
                action.Configuration = @event.Configuration;
            }
        }

        public void When(FormActionRemoved @event)
        {
            var form = _repository.Get(@event.AggregateId);
            var action = form.Actions.SingleOrDefault(a => a.Id == @event.Id);

            if (action != null)
            {
                form.Remove(action);
            }
        }

        public void When(FormGeneratorFieldsChanged @event)
        {
            var form = _repository.Get(@event.AggregateId);
            form.GeneratorLabelPosition = @event.LabelPosition;
            form.ReplaceAll(@event.Fields);
        }

        public void When(PageNameChanged @event)
        {
            var pageIdString = @event.AggregateId.ToJsonString();
            var actions = _repository
                .GetAll()
                .SelectMany(f => f.Actions)
                .Where(a => a.Type == FormActionType.PageForwarding)
                .Select(a => a.Configuration)
                .OfType<PageForwardingFormAction>()
                .Where(a => a.Id.Equals(pageIdString, StringComparison.Ordinal))
                .ToList();

            if (actions.Any())
            {
                var page = _pageRepository.Get(@event.AggregateId);
                var fullname = @event.Name;

                var p = page as Page;
                if (p != null)
                {
                    fullname += " (Nr. " + p.PublicId + ")";
                }

                foreach (var action in actions)
                {
                    action.Fullname = fullname;
                }
            }
        }

        public void When(DynamicObjectDefinitionNameChanged @event)
        {
            var definitionIdString = @event.AggregateId.ToJsonString();
            var actions = _repository
                .GetAll()
                .SelectMany(f => f.Actions)
                .Where(a => a.Type == FormActionType.CreateObject)
                .Select(a => a.Configuration)
                .OfType<CreateObjectFormAction>()
                .Where(a => a.ObjectDefinitionId.Equals(definitionIdString, StringComparison.Ordinal))
                .ToList();

            if (actions.Any())
            {
                foreach (var action in actions)
                {
                    action.ObjectDefinitionName = @event.Name;
                }
            }
        }

        public void When(TenantDeleted @event)
        {
            _repository.DeleteAllByTenantId(@event.AggregateId);
        }
    }
}