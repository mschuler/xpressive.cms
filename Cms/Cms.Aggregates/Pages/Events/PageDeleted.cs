namespace Cms.Aggregates.Pages.Events
{
    public class PageDeleted : EventBase<PageBase>
    {
        public string Fullname { get; set; }
    }
}