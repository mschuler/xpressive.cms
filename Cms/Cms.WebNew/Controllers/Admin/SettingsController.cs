﻿using System.Web.Http;
using Cms.Aggregates.Security;
using Cms.Aggregates.Settings;
using Cms.Aggregates.Settings.Commands;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class SettingsController : ApiControllerBase
    {
        private static readonly string _defaultPassword = "zR8XYhQq";
        private readonly ISettingsRepository _repository;

        public SettingsController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, ISettingsRepository repository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _repository = repository;
        }

        [HttpGet, Route("api/settings/{tenantId:ulong}/email")]
        public EmailSettingsDto GetEmailSettings(ulong tenantId)
        {
            var settings = _repository.GetSettings(tenantId);
            var dto = new EmailSettingsDto(settings.EmailSettings);
            dto.SmtpPassword = _defaultPassword;

            return dto;
        }

        [HttpPost, Route("api/settings/{tenantId:ulong}/email")]
        public void SetEmailSettings(ulong tenantId, [FromBody] EmailSettingsDto settings)
        {
            var smtpPassword = settings.SmtpPassword;

            if (_defaultPassword.Equals(smtpPassword))
            {
                var s = _repository.GetSettings(tenantId);
                smtpPassword = s.EmailSettings.SmtpPassword;
            }

            SendCommand(new ChangeEmailSettings
            {
                AggregateId = tenantId,
                TenantId = tenantId,
                EmailAddress = settings.EmailAddress,
                ResponseAddress = settings.ResponseAddress,
                Host = settings.Host,
                Port = settings.Port,
                IsSmtpAuthenticationRequired = settings.IsSmtpAuthenticationRequired,
                IsSslUsedForAuthentication = settings.IsSslUsedForAuthentication,
                SmtpUsername = settings.SmtpUsername,
                SmtpPassword = settings.SmtpPassword,
            });
        }

        public class EmailSettingsDto
        {
            public EmailSettingsDto() { }

            public EmailSettingsDto(EmailSettings settings)
            {
                Host = settings.Host;
                Port = settings.Port;
                EmailAddress = settings.EmailAddress;
                ResponseAddress = settings.ResponseAddress;
                IsSmtpAuthenticationRequired = settings.IsSmtpAuthenticationRequired;
                IsSslUsedForAuthentication = settings.IsSslUsedForAuthentication;
                SmtpUsername = settings.SmtpUsername;
                SmtpPassword = settings.SmtpPassword;
            }

            public string Host { get; set; }
            public int Port { get; set; }
            public string EmailAddress { get; set; }
            public string ResponseAddress { get; set; }
            public bool IsSmtpAuthenticationRequired { get; set; }
            public bool IsSslUsedForAuthentication { get; set; }
            public string SmtpUsername { get; set; }
            public string SmtpPassword { get; set; }
        }
    }
}