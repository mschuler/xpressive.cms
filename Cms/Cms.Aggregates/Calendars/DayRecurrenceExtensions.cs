﻿using System;

namespace Cms.Aggregates.Calendars
{
    internal static class DayRecurrenceExtensions
    {
        public static bool IsMatch(this DayRecurrence dayRecurrence, DayOfWeek dayOfWeek)
        {
            var dayOfWeekRecurrence = dayOfWeek.ToRecurrence();
            return dayRecurrence.HasFlag(dayOfWeekRecurrence);
        }

        public static DayRecurrence ToRecurrence(this DayOfWeek dayOfWeek)
        {
            return
                dayOfWeek == DayOfWeek.Monday ? DayRecurrence.Mondays :
                dayOfWeek == DayOfWeek.Tuesday ? DayRecurrence.Tuesdays :
                dayOfWeek == DayOfWeek.Wednesday ? DayRecurrence.Wednesdays :
                dayOfWeek == DayOfWeek.Thursday ? DayRecurrence.Thursdays :
                dayOfWeek == DayOfWeek.Friday ? DayRecurrence.Fridays :
                dayOfWeek == DayOfWeek.Saturday ? DayRecurrence.Saturdays : DayRecurrence.Sundays;
        }
    }
}