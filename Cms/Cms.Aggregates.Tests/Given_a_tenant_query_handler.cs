﻿using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Security;
using Cms.Aggregates.Tenants;
using Cms.Aggregates.Tenants.Queries;
using Moq;
using Xunit;

namespace Cms.Aggregates.Tests
{
    public class Given_a_tenant_query_handler
    {
        private const ulong UserId = 1234;
        private readonly IList<Tenant> _tenants = new List<Tenant>();
        private readonly Mock<IGroupRepository> _groupRepositoryMock = new Mock<IGroupRepository>();
        private readonly Mock<IUserRepository> _userRepositoryMock = new Mock<IUserRepository>();
        private readonly Mock<ITenantRepository> _tenantRepositoryMock = new Mock<ITenantRepository>();

        public Given_a_tenant_query_handler()
        {
            _tenantRepositoryMock.Setup(m => m.GetAll()).Returns(() => _tenants);
        }

        [Fact]
        public void When_a_user_without_group_is_calling_then_no_tenants_are_returned()
        {
            SetupUser(UserId);
            var handler = new TenantQueryHandler(_tenantRepositoryMock.Object, _userRepositoryMock.Object, _groupRepositoryMock.Object);
            var query = new GetTenants { UserId = UserId };

            var result = handler.When(query);

            Assert.Empty(result);
        }

        [Fact]
        public void When_a_sysadmin_is_calling_then_all_tenants_are_returned()
        {
            SetupUser(UserId).IsSysadmin = true;
            SetupTenant(1234, "t1");
            SetupTenant(2345, "t2");
            SetupTenant(3456, "t3");
            var handler = new TenantQueryHandler(_tenantRepositoryMock.Object, _userRepositoryMock.Object, _groupRepositoryMock.Object);
            var query = new GetTenants { UserId = UserId };

            var result = handler.When(query);

            Assert.Equal(3, result.Count());
        }

        [Fact]
        public void When_a_user_with_group_is_calling_then_only_the_tenants_of_his_groups_are_returned()
        {
            const ulong groupId = 9876543210;
            const ulong tenantId = 8765432109;

            SetupTenant(tenantId, "t");
            SetupGroup(groupId, "g", tenantId);
            var user = SetupUser(UserId);
            user.AddGroupId(groupId);

            var handler = new TenantQueryHandler(_tenantRepositoryMock.Object, _userRepositoryMock.Object, _groupRepositoryMock.Object);
            var query = new GetTenants { UserId = UserId };

            var result = handler.When(query);
            Assert.Equal(1, result.Count());
        }

        private void SetupGroup(ulong groupId, string name, ulong tenantId)
        {
            var group = new Group
            {
                Id = groupId,
                Name = name,
                TenantId = tenantId
            };

            _groupRepositoryMock.Setup(m => m.Get(groupId)).Returns(group);
        }

        private void SetupTenant(ulong tenantId, string name)
        {
            var tenant = new Tenant
            {
                Id = tenantId,
                Name = name
            };

            _tenants.Add(tenant);

            _tenantRepositoryMock.Setup(m => m.Get(tenantId)).Returns(tenant);
        }

        private User SetupUser(ulong userId)
        {
            var user = new User { Id = userId };
            _userRepositoryMock.Setup(m => m.Get(userId)).Returns(user);
            return user;
        }
    }
}
