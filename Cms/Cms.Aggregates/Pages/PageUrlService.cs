﻿using System;
using System.Collections.Generic;
using Cms.Aggregates.Websites;
using Cms.SharedKernel;

namespace Cms.Aggregates.Pages
{
    internal class PageUrlService : IPageUrlService
    {
        private readonly IPageRepository _pageRepository;
        private readonly IWebsiteRepository _websiteRepository;
        private readonly ICurrentRequestService _currentRequestService;
        private readonly IDisabledPageTokenService _disabledPageTokenService;

        public PageUrlService(IPageRepository pageRepository, IWebsiteRepository websiteRepository, ICurrentRequestService currentRequestService, IDisabledPageTokenService disabledPageTokenService)
        {
            _pageRepository = pageRepository;
            _websiteRepository = websiteRepository;
            _currentRequestService = currentRequestService;
            _disabledPageTokenService = disabledPageTokenService;
        }

        public string GetUrl(ulong pageId)
        {
            var page = _pageRepository.Get(pageId);
            return GetUrl(page);
        }

        public string GetUrl(PageBase page)
        {
            var parts = new Stack<string>();

            parts.Push(page.Name.ToValidUrlPart());

            while (_pageRepository.TryGet(page.ParentPageId, out page))
            {
                parts.Push(page.Name.ToValidUrlPart());
            }

            return "/" + string.Join("/", parts);
        }

        public string GetPermalink(Page page)
        {
            var useAccessToken = !page.IsEnabled;
            return GetPermalink(page.WebsiteId, page.Id, page.Permalink, useAccessToken);
        }

        public string GetPermalink(ulong websiteId, ulong pageId, Guid permalink)
        {
            return GetPermalink(websiteId, pageId, permalink, true);
        }

        public string GetPermalink(ulong websiteId, ulong pageId, Guid permalink, bool useAccessToken)
        {
            var website = _websiteRepository.Get(websiteId);
            var port = _currentRequestService.Port;
            var isDefaultPort = _currentRequestService.IsDefaultPort;

            if (website.DefaultDomain == null)
            {
                return string.Empty;
            }

            var url = "http://";
            url += website.DefaultDomain.Host;

            if (!isDefaultPort)
            {
                url += ":" + port;
            }

            url += "/" + permalink.ToString("N");

            if (useAccessToken)
            {
                url += "?accessToken=" + _disabledPageTokenService.GetToken(pageId);
            }

            return url;
        }
    }
}
