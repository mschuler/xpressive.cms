using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Pages.Commands
{
    [RequiredPermission("page", PermissionLevel.CanChange)]
    public class ChangePageLink : CommandBase
    {
        public ulong PageId { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }

        public string Url { get; set; }
    }
}