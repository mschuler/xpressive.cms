﻿using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Files;
using Cms.Aggregates.Security;
using Cms.Aggregates.Tenants.Events;

namespace Cms.Aggregates.FileDirectories.Events
{
    public class FileDirectoryEventHandler : EventHandlerBase
    {
        private readonly IRepository<FileDirectory> _repository;
        private readonly IFileRepository _fileRepository;
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;

        public FileDirectoryEventHandler(
            IRepository<FileDirectory> repository,
            IFileRepository fileRepository,
            IUserRepository userRepository,
            IGroupRepository groupRepository)
        {
            _repository = repository;
            _fileRepository = fileRepository;
            _userRepository = userRepository;
            _groupRepository = groupRepository;
        }

        public void When(FileDirectoryCreated @event)
        {
            var parent = @event.ParentId > 0 ? _repository.Get(@event.ParentId) : null;

            var directory = new FileDirectory
            {
                Id = @event.AggregateId,
                TenantId = @event.TenantId,
                FileStoreId = @event.FileStoreId,
                Name = @event.Name,
                Parent = parent,
            };

            _repository.Add(directory);

            if (parent != null)
            {
                parent.Add(directory);
            }
        }

        public void When(FileDirectoryNameChanged @event)
        {
            var directory = _repository.Get(@event.AggregateId);
            directory.Name = @event.Name;
        }

        public void When(FileDirectoryParentChanged @event)
        {
            var directory = _repository.Get(@event.AggregateId);
            var oldParent = directory.Parent;
            var newParent = @event.ParentId > 0 ? _repository.Get(@event.ParentId) : null;

            if (oldParent != null)
            {
                oldParent.Remove(directory);
            }

            directory.Parent = newParent;

            if (newParent != null)
            {
                newParent.Add(directory);
            }
        }

        public void When(FileDirectoryDeleted @event)
        {
            var directory = _repository.Get(@event.AggregateId);
            _repository.Delete(@event.AggregateId);

            if (directory.Parent != null)
            {
                directory.Parent.Remove(directory);
            }

            var directories = new List<FileDirectory>();
            var queue = new Queue<FileDirectory>();
            queue.Enqueue(directory);

            while (queue.Count > 0)
            {
                var dir = queue.Dequeue();
                directories.Add(dir);
                dir.Children.ToList().ForEach(queue.Enqueue);
            }

            foreach (var dir in directories)
            {
                foreach (var file in _fileRepository.GetByDirectory(dir.Id))
                {
                    file.FileDirectoryId = 0;
                }
            }
        }

        public void When(FileDirectoryShared @event)
        {
            var directory = _repository.Get(@event.AggregateId);
            var user = _userRepository.Get(@event.UserId);
            var allowedTenantIds = user.GroupIds.Select(g => _groupRepository.Get(g).TenantId).ToList();
            var sharedTenantIds = @event.TenantIds.Union(directory.SharedForTenants).Distinct().ToList();
            sharedTenantIds.RemoveAll(t => allowedTenantIds.Contains(t) && !@event.TenantIds.Contains(t));
            directory.Share(sharedTenantIds);
        }

        public void When(TenantDeleted @event)
        {
            _repository.DeleteAllByTenantId(@event.AggregateId);

            foreach (var fileDirectory in _repository.GetAll())
            {
                if (fileDirectory.SharedForTenants.Contains(@event.AggregateId))
                {
                    var share = fileDirectory.SharedForTenants.Where(t => t != @event.AggregateId).ToList();
                    fileDirectory.Share(share);
                }
            }
        }
    }
}
