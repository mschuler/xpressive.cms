using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace Cms.Aggregates.Pages
{
    internal class DisabledPageTokenService : IDisabledPageTokenService
    {
        private static readonly object _lock = new object();
        private static readonly Dictionary<string, ulong> _tokens = new Dictionary<string, ulong>(StringComparer.Ordinal);

        public string GetToken(ulong pageId)
        {
            lock (_lock)
            {
                var pair = _tokens.SingleOrDefault(t => t.Value == pageId);

                if (!pair.Equals(default(KeyValuePair<string, ulong>)) && pair.Value == pageId)
                {
                    return pair.Key;
                }

                using (var provider = new RNGCryptoServiceProvider())
                {
                    var data = new byte[32];
                    provider.GetBytes(data);
                    var token = Convert.ToBase64String(data)
                        .Replace("+", "")
                        .Replace("/", "")
                        .Replace("=", "");

                    _tokens.Add(token, pageId);
                    return token;
                }
            }
        }

        public bool IsValidToken(ulong pageId, string token)
        {
            lock (_lock)
            {
                if (string.IsNullOrEmpty(token))
                {
                    return false;
                }

                ulong id;
                return _tokens.TryGetValue(token, out id) && id == pageId;
            }
        }
    }
}