using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission(false)]
    public class RemoveU2FDevice : CommandBase
    {
        [Required]
        public byte[] PublicKey { get; set; }
    }
}