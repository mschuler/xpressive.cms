(function() {
    "use strict";

    String.format = function () {
        // The string containing the format items (e.g. "{0}")
        // will and always has to be the first argument.
        var theString = arguments[0];

        // start with the second argument (i = 1)
        for (var i = 1; i < arguments.length; i++) {
            // "gm" = RegEx options for Global search (more than one instance)
            // and for Multiline search
            var regEx = new RegExp("\\{" + (i - 1) + "\\}", "gm");
            theString = theString.replace(regEx, arguments[i]);
        }

        return theString;
    }

    angular.module('app', [
        "ngRoute", "ngAnimate", 'toastr', 'ui.select', 'ui.utils', 'wiz.markdown', 'ngDragDrop', "fineUploaderDirective",
        'ui.tree', 'ui.bootstrap', "angular-loading-bar", "ngSanitize",'angularMoment','infinite-scroll',
        "angular-md5", 'LocalStorageModule', "app.controllers", "app.directives", "chart.js",
        "app.nav", "app.ui.map", "app.ui.directives", "ui.sortable",'ui.codemirror',
        "ui.slider", "ui.tinymce", "ngTagsInput", "pascalprecht.translate"]);

    angular.module("app").factory("toastService", ["toastr", function (toastr) {
        var factory = {};
        var error = function (message, e) {
            if (e && e.message && e.message.length > 0) {
                message += "<br/>Grund: " + e.message;
            }
            toastr.error(message, 'Fehler', { allowHtml: true, closeButton: true, timeOut: 0 });
        };

        factory.error = error;
        return factory;
    }]);

    angular.module("app").config(function (toastrConfig) {
        angular.extend(toastrConfig, {
            preventDuplicates: true,
        });
    });

    angular.module('app').config(function ($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: '/admin/i18n/',
            suffix: '.json'
        });

        $translateProvider.useMissingTranslationHandlerLog();
        $translateProvider.fallbackLanguage("en");

        $translateProvider.registerAvailableLanguageKeys(['en', 'de'], {
            'en_US': 'en',
            'en_UK': 'en',
            'de_DE': 'de',
            'de_CH': 'de'
        });

        $translateProvider.determinePreferredLanguage();
    });

    angular.module('app').constant('angularMomentConfig', {
        timezone: 'Europe/Zurich'
    });

    angular.module("app").run(function ($log, $location, authService, amMoment) {
        try {
            amMoment.changeLocale('de');
            authService.fillAuthData();
            if (!authService.authentication || !authService.authentication.isAuth) {
                var currentPath = $location.path();
                $log.debug("currentPath: " + currentPath);
                if (currentPath != "/login" && currentPath != "/lock") {
                    $location.path("/login");
                }
            }
        } catch (e) {
            $log.error(e);
        }
    });

    angular.module('app').run(function ($rootScope, $location, $log, authService) {
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            if (!authService.authentication || !authService.authentication.isAuth) {
                if (next.templateUrl == "views/pages/signin.min.html") {
                    // already going to /login, no redirect needed
                } else if (next.templateUrl == "views/pages/lock-screen.min.html") {
                    // already going to /lock, no redirect needed
                } else {
                    $location.path("/login");
                }
            }
        });
    });

    //angular.module('app').config(['$compileProvider', function ($compileProvider) {
    //    $compileProvider.debugInfoEnabled(false);
    //}]);

    angular.module('app').config(['localStorageServiceProvider', function (localStorageServiceProvider) {
        localStorageServiceProvider.setPrefix('xpressivecms');
    }]);

    angular.module('app').filter('unsafe', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    });

    angular.module('app').directive('repeatDone', function () {
        return function(scope, element, attrs) {
            if (scope.$last) { // all are rendered
                try {
                    scope.collapseAll();
                } catch (e) { }
            }
        }
    });

    angular.module("app").directive('clickAndWait', function () {
        return {
            scope: {
                clickAndWait: '&'
            },
            restrict: 'A',
            link: function (scope, iElement, iAttrs) {
                iElement.bind('click', function () {
                    var tagName = iElement[0].tagName.toUpperCase();
                    if (tagName == "A") {
                        iElement.attr("disabled", "disabled");
                    } else {
                        iElement.prop('disabled', true);
                    }
                    var promise = scope.clickAndWait();
                    if (promise) {
                        promise.finally(function () {
                            if (tagName == "A") {
                                iElement.removeAttr("disabled");
                            } else {
                                iElement.prop('disabled', false);
                            }
                        });
                    } else {
                        if (tagName == "A") {
                            iElement.removeAttr("disabled");
                        } else {
                            iElement.prop('disabled', false);
                        }
                    }
                });
            }
        };
    });

    angular.module('app').config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
    }]);

    angular.module('app').config([
        "$routeProvider", function ($routeProvider) {
            var routes, setRoutes;
            return routes = [
                    "ui/typography", "ui/buttons", "ui/icons", "ui/grids", "ui/widgets", "ui/components", "ui/boxes", "ui/timeline", "ui/nested-lists",
                    "ui/pricing-tables", "ui/maps", "tables/static", "tables/dynamic", "tables/responsive", "forms/elements", "forms/layouts", "forms/validation",
                    "forms/wizard", "charts/charts", "charts/flot", "pages/blank", "pages/forgot-password", "pages/invoice",
                    "pages/invoice", "mail/compose", "mail/inbox", "mail/single", "tasks/tasks"
                ],
                setRoutes = function(route) {
                    var config, url;
                    return url = "/" + route,
                        config = { templateUrl: "views/" + route + ".html" },
                        $routeProvider.when(url, config),
                        $routeProvider;
                },
                routes.forEach(function(route) {
                    return setRoutes(route);
                }),
                $routeProvider
                    .when("/", { redirectTo: "/dashboard" })
                    .when("/404", { templateUrl: "views/pages/404.min.html" })
                    .when("/500", {
                        templateUrl: "views/pages/500.min.html",
                        controller: "ErrorController",
                    })
                    .when("/500/:id", {
                        templateUrl: "views/pages/500.min.html",
                        controller: "ErrorController",
                    })
                    .when("/dashboard", { templateUrl: "views/dashboard.min.html" })
                    .when("/lock", { templateUrl: "views/pages/lock-screen.min.html" })
                    .when("/login", {
                        templateUrl: "views/pages/signin.min.html",
                        controller: "AuthenticationController"
                    })
                    .when("/profile", {
                        templateUrl: "views/pages/profile.min.html",
                        controller: "ProfileController"
                    })
                    .when("/user/users", {
                        templateUrl: "views/user/users.min.html",
                        controller: "UsersController"
                    })
                    .when("/user/user/:userId", {
                        templateUrl: "views/user/user.min.html",
                        controller: "UserController"
                    })
                    .when("/user/groups", {
                        templateUrl: "views/user/groups.min.html",
                        controller: "GroupsController"
                    })
                    .when("/user/group/:groupId/permissions", {
                        templateUrl: "views/user/permissions.min.html",
                        controller: "PermissionController"
                    })
                    .when("/user/group/:groupId/members", {
                        templateUrl: "views/user/members.min.html",
                        controller: "MemberController"
                    })
                    .when("/content/pages", {
                        templateUrl: "views/content/pages.min.html",
                        controller: "PageController"
                    })
                    .when("/content/page/:pageId", {
                        templateUrl: "views/content/page.min.html",
                        controller: "PageDetailController"
                    })
                    .when("/content/files", {
                        templateUrl: "views/content/files.min.html",
                        controller: "FileController"
                    })
                    .when("/content/forms", {
                        templateUrl: "views/forms/forms.min.html",
                        controller: "FormsController"
                    })
                    .when("/content/form/:formId", {
                        templateUrl: "views/forms/form.min.html",
                        controller: "FormController"
                    })
                    .when("/master/tenants", {
                        templateUrl: "views/masterdata/tenants.min.html",
                        controller: "TenantController"
                    })
                    .when("/master/websites", {
                        templateUrl: "views/masterdata/websites.min.html",
                        controller: "WebsitesController"
                    })
                    .when("/master/website/:websiteId", {
                        templateUrl: "views/masterdata/website.min.html",
                        controller: "WebsiteController"
                    })
                    .when("/master/pagetemplates", {
                        templateUrl: "views/masterdata/pagetemplates.min.html",
                        controller: "PageTemplatesController"
                    })
                    .when("/master/pagetemplate/:templateId", {
                        templateUrl: "views/masterdata/pagetemplate.min.html",
                        controller: "PageTemplateController"
                    })
                    .when("/objects/objects", {
                        templateUrl: "views/objects/objects.min.html",
                        controller: "ObjectsDefinitionsController"
                    })
                    .when("/objects/objects/:definitionId", {
                        templateUrl: "views/objects/objectlist.min.html",
                        controller: "ObjectsController"
                    })
                    .when("/objects/object/:objectId", {
                        templateUrl: "views/objects/object.min.html",
                        controller: "ObjectController"
                    })
                    .when("/objects/templates", {
                        templateUrl: "views/objects/objects.min.html",
                        controller: "ObjectDefinitionsForTemplatesController"
                    })
                    .when("/objects/templates/:definitionId", {
                        templateUrl: "views/objects/templates.min.html",
                        controller: "ObjectTemplatesController"
                    })
                    .when("/objects/template/:definitionId/:templateId", {
                        templateUrl: "views/objects/template.min.html",
                        controller: "ObjectTemplateController"
                    })
                    .when("/objects/definitions", {
                        templateUrl: "views/objects/definitions.min.html",
                        controller: "ObjectDefinitionsController"
                    })
                    .when("/objects/definition/:definitionId", {
                        templateUrl: "views/objects/definition.min.html",
                        controller: "ObjectDefinitionController"
                    })
                    .when("/calendar/appointments", {
                        templateUrl: "views/calendar/calendar.min.html",
                        controller: "AppointmentController"
                    })
                    .when("/calendar/categories", {
                        templateUrl: "views/calendar/categories.min.html",
                        controller: "CalendarCategoriesController"
                    })
                    .when("/calendar/templates", {
                        templateUrl: "views/calendar/templates.min.html",
                        controller: "CalendarTemplatesController"
                    })
                    .when("/calendar/templates/:templateName", {
                        templateUrl: "views/calendar/template.min.html",
                        controller: "CalendarTemplateController"
                    })
                    .when("/system/eventlog", {
                        templateUrl: "views/system/eventlog.min.html",
                        controller: "EventlogController"
                    })
                    .when("/system/eventlog/:aggregateType/:aggregateId", {
                        templateUrl: "views/system/eventlog.min.html",
                        controller: "AggregateEventlogController"
                    })
                    .when("/system/serverlog", {
                        templateUrl: "views/system/serverlog.min.html",
                        controller: "ServerLogController"
                    })
                    .when("/system/settings", {
                        templateUrl: "views/system/settings.min.html",
                        controller: "SettingsController"
                    })
                    .when("/system/messages", {
                        templateUrl: "views/system/messages.min.html",
                        controller: "SystemMessagesController"
                    })
                    .when("/system/filestore", {
                        templateUrl: "views/system/filestore.min.html",
                        controller: "FileStoreController"
                    })
                    .otherwise({ redirectTo: "/404" });
        }
    ]);

    angular.module("app.ui.map", []).directive("uiJvectormap", [
        function() {
            return{
                restrict: "A",
                scope: { options: "=" },
                link: function(scope, ele) {
                    var options;
                    return options = scope.options, ele.vectorMap(options);
                }
            }
        }
    ]);

    angular.module("app.ui.directives", [])
        .directive("uiTime", [function() {
            return{
                restrict: "A",
                link: function(scope, ele) {
                    var checkTime, startTime;
                    return startTime = function() {
                        var h, m, s, t, time, today;
                        return today = new Date, h = today.getHours(), m = today.getMinutes(), s = today.getSeconds(), m = checkTime(m), s = checkTime(s), time = h + ":" + m + ":" + s, ele.html(time), t = setTimeout(startTime, 500);
                    }, checkTime = function(i) { return 10 > i && (i = "0" + i), i; }, startTime();
                }
            }
        }])
        .directive("slimScroll", [function() {
            return {
                restrict: "A",
                link: function (scope, ele, attrs) {
                    try {
                        return ele.slimScroll({ height: attrs.scrollHeight || "100%" });
                    } catch (e) {
                        return null;
                    } 
                }
            };
        }]);

    angular.module("app.nav", []).directive("toggleNavCollapsedMin", [
        "$rootScope", function($rootScope) {
            return{
                restrict: "A",
                link: function(scope, ele) {
                    var app;
                    return app = $("#app"), ele.on("click", function(e) {
                        return app.hasClass("nav-collapsed-min") ? app.removeClass("nav-collapsed-min") : (app.addClass("nav-collapsed-min"), $rootScope.$broadcast("nav:reset")), e.preventDefault()
                    });
                }
            }
        }
    ]).directive("collapseNav", [
        function() {
            return{
                restrict: "A",
                link: function(scope, ele) {
                    var $a, $aRest, $app, $lists, $listsRest, $nav, $window, Timer, prevWidth, updateClass;
                    return $window = $(window),
                        $lists = ele.find("ul").parent("li"),
                        $lists.append('<i class="ti-angle-down icon-has-ul-h"></i><i class="ti-angle-double-right icon-has-ul"></i>'),
                        $a = $lists.children("a"),
                        $listsRest = ele.children("li").not($lists),
                        $aRest = $listsRest.children("a"),
                        $app = $("#app"),
                        $nav = $("#nav-container"),
                        $a.on("click", function(event) {
                            var $parent, $this;
                            return $app.hasClass("nav-collapsed-min") || $nav.hasClass("nav-horizontal") && $window.width() >= 768
                                ? !1
                                : (
                                    $this = $(this),
                                    $parent = $this.parent("li"),
                                    $lists.not($parent).removeClass("open").find("ul").slideUp(),
                                    $parent.toggleClass("open").find("ul").stop().slideToggle(),
                                    event.preventDefault()
                                );
                        }),
                        $aRest.on("click", function() {
                            return $lists.removeClass("open").find("ul").slideUp();
                        }),
                        scope.$on("nav:reset", function() {
                            return $lists.removeClass("open").find("ul").slideUp();
                        }),
                        Timer = void 0,
                        prevWidth = $window.width(),
                        updateClass = function() {
                            var currentWidth;
                            return currentWidth = $window.width(),
                                768 > currentWidth && $app.removeClass("nav-collapsed-min"),
                                768 > prevWidth && currentWidth >= 768 && $nav.hasClass("nav-horizontal") && $lists.removeClass("open").find("ul").slideUp(),
                                prevWidth = currentWidth;
                        },
                        $window.resize(function() {
                            var t;
                            return clearTimeout(t), t = setTimeout(updateClass, 300);
                        });
                }
            }
        }
    ]).directive("highlightActive", [
        function() {
            return{
                restrict: "A",
                controller: [
                    "$scope", "$element", "$attrs", "$location", function($scope, $element, $attrs, $location) {
                        var highlightActive, links, path;
                        return links = $element.find("a"), path = function() { return $location.path() }, highlightActive = function(links, path) {
                            return path = "#" + path, angular.forEach(links, function(link) {
                                var $li, $link, href;
                                return $link = angular.element(link), $li = $link.parent("li"), href = $link.attr("href"), $li.hasClass("active") && $li.removeClass("active"), 0 === path.indexOf(href) ? $li.addClass("active") : void 0
                            })
                        }, highlightActive(links, $location.path()), $scope.$watch(path, function(newVal, oldVal) { return newVal !== oldVal ? highlightActive(links, $location.path()) : void 0 })
                    }
                ]
            }
        }
    ]).directive("toggleOffCanvas", [function() { return{ restrict: "A", link: function(scope, ele) { return ele.on("click", function() { return $("#app").toggleClass("on-canvas"); }); } } }]);

    angular.module("app.directives", [])
        .directive("customPage", function () {
            return{
                restrict: "A",
                controller: [
                    "$scope", "$element", "$location", function($scope, $element, $location) {
                        var addBg, path;
                        return path = function() { return $location.path(); }, addBg = function(path) {
                            switch ($element.removeClass("body-wide body-err body-lock body-auth"), path) {
                            case"/404":
                            case"/500":
                                return $element.addClass("body-wide body-err");
                            case"/login":
                                return $element.addClass("body-wide body-auth");
                            case"/lock":
                                return $element.addClass("body-wide body-lock");
                            }
                            if (path.indexOf("/500/") === 0) {
                                return $element.addClass("body-wide body-err");
                            }
                        }, addBg($location.path()), $scope.$watch(path, function(newVal, oldVal) { return newVal !== oldVal ? addBg($location.path()) : void 0; });
                    }
                ]
            }
        })
        .directive("goBack", [function() {
            return {
                restrict: "A",
                controller: ["$scope", "$element", "$window", function($scope, $element, $window) {
                    return $element.on("click", function () { return $window.history.back(); });
                }]
            }
        }]);

    angular.module("app.controllers", ["ui.tree"])
        .controller("AppCtrl", ["$scope", "$rootScope", function($scope, $rootScope) {
            var $window;
            return $window = $(window),
                $scope.main = {
                    brand: "xpressive.cms",
                    name: "Lisa Doe"
                },
                $scope.admin = {
                    layout: "wide",
                    menu: "vertical",
                    fixedHeader: !0,
                    fixedSidebar: !0,
                    skin: "32"
                },
                $scope.$watch("admin", function(newVal, oldVal) {
                    return "horizontal" === newVal.menu && "vertical" === oldVal.menu ?
                        void $rootScope.$broadcast("nav:reset") :
                        newVal.fixedHeader === !1 && newVal.fixedSidebar === !0 ?
                            (oldVal.fixedHeader === !1 && oldVal.fixedSidebar === !1 && ($scope.admin.fixedHeader = !0, $scope.admin.fixedSidebar = !0), void (oldVal.fixedHeader === !0 && oldVal.fixedSidebar === !0 && ($scope.admin.fixedHeader = !1, $scope.admin.fixedSidebar = !1))) :
                            (newVal.fixedSidebar === !0 && ($scope.admin.fixedHeader = !0), void (newVal.fixedHeader === !1 && ($scope.admin.fixedSidebar = !1)))
                }, !0),
                $scope.color = {
                    primary: "#5B90BF",
                    success: "#A3BE8C",
                    info: "#7FABD2",
                    infoAlt: "#B48EAD",
                    warning: "#EBCB8B",
                    danger: "#BF616A",
                    gray: "#DCDCDC"
                }
            }
        ]);
}());
