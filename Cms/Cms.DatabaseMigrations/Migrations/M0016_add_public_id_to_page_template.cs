using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0016_add_public_id_to_page_template : DatabaseMigrationBase
    {
        public M0016_add_public_id_to_page_template() : base(16) { }

        public override void Execute(Database database)
        {
            var events = database.Query<EventDto>().Where(e => e.Event == "PageTemplateCreated").ToList();
            var publicId = 1;

            foreach (var dto in events)
            {
                var json = dto.Data.Replace("\"Name\"", "\"PublicId\":" + publicId + ",\"Name\"");
                dto.Data = json;
                database.Update(dto);

                publicId++;
            }
        }
    }
}