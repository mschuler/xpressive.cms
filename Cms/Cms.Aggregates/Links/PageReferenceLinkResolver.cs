using Cms.Aggregates.Pages;

namespace Cms.Aggregates.Links
{
    internal class PageReferenceLinkResolver : ILinkResolverStrategy
    {
        private readonly IPageUrlService _pageUrlService;

        public PageReferenceLinkResolver(IPageUrlService pageUrlService)
        {
            _pageUrlService = pageUrlService;
        }

        public bool CanHandle(ILink link)
        {
            return link.GetType() == typeof(PageReferenceLink);
        }

        public string GetHyperlink(ILink link)
        {
            var pageLink = (PageReferenceLink)link;
            return _pageUrlService.GetUrl(pageLink.PageId);
        }
    }
}