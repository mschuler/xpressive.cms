﻿using System;
using System.Web;
using Cms.Aggregates.Pages;

namespace Cms.WebNew
{
    public class CurrentRequestService : ICurrentRequestService
    {
        private readonly Lazy<HttpRequest> _request = new Lazy<HttpRequest>(() => HttpContext.Current.Request);

        public int Port { get { return _request.Value.Url.Port; } }
        public bool IsDefaultPort { get { return _request.Value.Url.IsDefaultPort; } }
        public string Scheme { get { return _request.Value.Url.Scheme; } }

        public string Language
        {
            get
            {
                var languages = _request.Value.UserLanguages;
                if (languages == null || languages.Length <= 0)
                {
                    return "en";
                }
                return languages[0];
            }
        }
    }
}