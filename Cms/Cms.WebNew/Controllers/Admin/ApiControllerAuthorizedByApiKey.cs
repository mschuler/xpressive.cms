using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Cms.Services.Contracts;

namespace Cms.WebNew.Controllers.Admin
{
    public abstract class ApiControllerAuthorizedByApiKey : ApiController
    {
        private readonly IApiKeyService _apiKeyService;

        protected ApiControllerAuthorizedByApiKey(IApiKeyService apiKeyService)
        {
            _apiKeyService = apiKeyService;
        }

        protected bool IsAuthorized()
        {
            var apiKey = GetApiKey();
            var cookie = GetCookieValue();
            var ipAddress = Request.GetClientIpAddress();

            if (string.IsNullOrEmpty(apiKey) || string.IsNullOrEmpty(cookie))
            {
                return false;
            }

            return _apiKeyService.IsValidApiKey(apiKey, cookie, ipAddress);
        }

        private string GetApiKey()
        {
            var authenticationHeader = Request.Headers.Authorization;

            if (authenticationHeader != null)
            {
                if ("apikey".Equals(authenticationHeader.Scheme, StringComparison.OrdinalIgnoreCase))
                {
                    return authenticationHeader.Parameter;
                }
            }

            var query = Request.GetQueryNameValuePairs();
            var pairs = query.Where(p => p.Key.Equals("apikey", StringComparison.OrdinalIgnoreCase)).ToList();

            if (pairs.Any())
            {
                return pairs.First().Value;
            }

            return null;
        }

        private string GetCookieValue()
        {
            var cookies = Request.Headers.GetCookies().ToList();

            foreach (var cookie in cookies)
            {
                foreach (var cookieState in cookie.Cookies)
                {
                    if (cookieState.Name.Equals("ak", StringComparison.Ordinal))
                    {
                        return cookieState.Value;
                    }
                }
            }

            return null;
        }
    }
}