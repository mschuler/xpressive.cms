﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Cms.Aggregates.Security;
using Cms.EventSourcing.Contracts.Commands;
using Cms.EventSourcing.Contracts.Queries;
using Cms.WebNew.Authentication;

namespace Cms.WebNew.Controllers
{
    public abstract class ApiControllerBase : ApiController
    {
        private readonly ICommandQueue _commandQueue;
        private readonly IUserRepository _userRepository;
        private readonly IQueryHandler _queryHandler;

        protected ApiControllerBase(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler)
        {
            _commandQueue = commandQueue;
            _userRepository = userRepository;
            _queryHandler = queryHandler;
        }

        protected ulong CmsUserId => User.GetId();

        protected User CmsUser => _userRepository.Get(CmsUserId);

        protected void SendCommand(ICommand command)
        {
            var user = CmsUser;
            user.RecentActivity = DateTime.UtcNow;
            command.UserId = user.Id;
            _commandQueue.Put(command, Request);
        }

        protected IEnumerable<object> ExecuteQuery(IQuery query)
        {
            var user = CmsUser;
            user.RecentActivity = DateTime.UtcNow;
            query.UserId = user.Id;
            return _queryHandler.Handle(query, Request);
        }
    }
}