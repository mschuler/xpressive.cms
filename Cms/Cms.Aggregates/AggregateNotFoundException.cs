﻿using System;

namespace Cms.Aggregates
{
    public sealed class AggregateNotFoundException : InvalidOperationException
    {
        private readonly ulong _id;

        public AggregateNotFoundException(ulong id)
        {
            _id = id;
        }

        public override string Message
        {
            get { return string.Format("Aggregate with ID='{0}' not found.", _id); }
        }
    }
}