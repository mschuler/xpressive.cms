﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Files;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.Websites;

namespace Cms.Aggregates.Pages.Queries
{
    internal class PageQueryHandler : QueryHandlerBase
    {
        private readonly IWebsiteRepository _websiteRepository;
        private readonly IUserStarredPageRepository _userStarredPageRepository;
        private readonly IPageRepository _pageRepository;
        private readonly IPageTemplateRepository _pageTemplateRepository;
        private readonly IPageTemplateService _pageTemplateService;
        private readonly IPageDtoService _dtoService;
        private readonly IFileRepository _fileRepository;

        public PageQueryHandler(IWebsiteRepository websiteRepository, IUserStarredPageRepository userStarredPageRepository, IPageRepository pageRepository, IPageTemplateRepository pageTemplateRepository, IPageDtoService dtoService, IFileRepository fileRepository, IPageTemplateService pageTemplateService)
        {
            _websiteRepository = websiteRepository;
            _userStarredPageRepository = userStarredPageRepository;
            _pageRepository = pageRepository;
            _pageTemplateRepository = pageTemplateRepository;
            _dtoService = dtoService;
            _fileRepository = fileRepository;
            _pageTemplateService = pageTemplateService;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetStarredPages),
                typeof (GetPagesWithChildren),
                typeof (GetPagesBySearchPattern),
                typeof (GetPage)
            };
        }

        public IEnumerable<object> When(GetStarredPages query)
        {
            var website = _websiteRepository.Get(query.WebsiteId);
            return _userStarredPageRepository
                .Get(query.UserId)
                .Where(p => p.WebsiteId.Equals(query.WebsiteId))
                .OrderBy(p => p.Name)
                .Select(p => _dtoService.GetPageDto(website, p));
        }

        public IEnumerable<object> When(GetPagesWithChildren query)
        {
            var website = _websiteRepository.Get(query.WebsiteId);
            return _pageRepository
                .GetAllByWebsiteId(query.WebsiteId)
                .OrderBy(p => p.Name)
                .Select(p => _dtoService.GetPageDto(website, p));
        }

        public IEnumerable<object> When(GetPagesBySearchPattern query)
        {
            var websites = _websiteRepository
                .GetAll(query.TenantId)
                .ToDictionary(w => w.Id);

            return _pageRepository
                .GetAll()
                .Where(query.IsMatch)
                .Where(p => websites.ContainsKey(p.WebsiteId))
                .OrderBy(p => p.Name, StringComparer.OrdinalIgnoreCase)
                .Select(p => _dtoService.GetPageDto(websites[p.WebsiteId], p));
        }

        public IEnumerable<object> When(GetPage query)
        {
            var page = _pageRepository.Get(query.PageId);
            var website = _websiteRepository.Get(page.WebsiteId);
            var p = page as Page;

            PageTemplate pageTemplate;
            if (p == null ||
                !_pageTemplateRepository.TryGet(p.PageTemplateId, out pageTemplate) ||
                string.IsNullOrEmpty(pageTemplate.Template))
            {
                yield return _dtoService.GetPageDto(website, page);
                yield break;
            }

            var pageFields = _pageTemplateService.GetPageFields(pageTemplate);
            var pageContents = p.GetPageContent(pageTemplate, _pageTemplateService).ToList();
            var content = new List<object>();

            foreach (var pageField in pageFields)
            {
                var pageContent = pageContents.SingleOrDefault(pc => pc.Name.Equals(pageField.Name, StringComparison.OrdinalIgnoreCase));
                if (pageContent != null)
                {
                    content.Add(new
                    {
                        pageContent.Name,
                        Type = PageContentValueBase.GetType(pageContent.Value),
                        Value = AddMissingData(pageContent.Value),
                        pageField.CanChangeType
                    });
                }
                else
                {
                    content.Add(new
                    {
                        pageField.Name,
                        pageField.Type,
                        Value = (object)string.Empty,
                        pageField.CanChangeType
                    });
                }
            }

            yield return _dtoService.GetPageDto(website, page, content);
        }

        private object AddMissingData(PageContentValueBase pageContent)
        {
            var imageValue = pageContent as PageContentImageValue;

            if (imageValue != null)
            {
                var newValue = new PageContentImageValueWithAdditionalInfo(imageValue);
                File file;
                ulong fileId;
                PageBase page;
                ulong pageId;

                if (ulong.TryParse(newValue.FileId, out fileId) && _fileRepository.TryGet(fileId, out file))
                {
                    newValue.File = file.ToDto();
                }

                if (ulong.TryParse(newValue.PageId, out pageId) && _pageRepository.TryGet(pageId, out page))
                {
                    var website = _websiteRepository.Get(page.WebsiteId);
                    newValue.PageLink = _dtoService.GetPageDto(website, page);
                }

                return newValue;
            }

            return pageContent;
        }

        internal sealed class PageContentImageValueWithAdditionalInfo : PageContentImageValue
        {
            public PageContentImageValueWithAdditionalInfo(PageContentImageValue value)
            {
                Alpha = value.Alpha;
                Brightness = value.Brightness;
                Contrast = value.Contrast;
                ExternalLink = value.ExternalLink;
                FileId = value.FileId;
                Filter = value.Filter;
                LinkType = value.LinkType;
                PageId = value.PageId;
                RoundedCorners = value.RoundedCorners;
                Saturation = value.Saturation;
                Tint = value.Tint;
            }

            public object File { get; set; }
            public object PageLink { get; set; }
        }
    }
}
