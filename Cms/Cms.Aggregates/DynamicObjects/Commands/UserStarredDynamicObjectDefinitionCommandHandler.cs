using System;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    internal class UserStarredDynamicObjectDefinitionCommandHandler : CommandHandlerBase
    {
        private readonly IDynamicObjectDefinitionRepository _repository;
        private readonly IUserStarredDynamicObjectDefinitionRepository _starredRepository;

        public UserStarredDynamicObjectDefinitionCommandHandler(IDynamicObjectDefinitionRepository repository, IUserStarredDynamicObjectDefinitionRepository starredRepository)
        {
            _repository = repository;
            _starredRepository = starredRepository;
        }

        public void When(StarDynamicObjectDefinition command, Action<IEvent> eventHandler)
        {

            _repository.Get(command.AggregateId);

            if (_starredRepository.IsStarred(command.UserId, command.AggregateId))
            {
                return;
            }

            var e = new DynamicObjectDefinitionStarred()
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            };

            eventHandler(e);
        }

        public void When(UnstarDynamicObjectDefinition command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            if (!_starredRepository.IsStarred(command.UserId, command.AggregateId))
            {
                return;
            }

            var e = new DynamicObjectDefinitionUnstarred
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            };

            eventHandler(e);
        }
    }
}