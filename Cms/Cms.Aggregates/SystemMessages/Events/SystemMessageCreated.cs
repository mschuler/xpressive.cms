﻿namespace Cms.Aggregates.SystemMessages.Events
{
    public class SystemMessageCreated : EventBase<SystemMessage>
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTimeRange Validity { get; set; }
    }
}