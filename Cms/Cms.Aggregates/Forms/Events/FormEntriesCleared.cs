﻿namespace Cms.Aggregates.Forms.Events
{
    public class FormEntriesCleared : EventBase<Form> { }
}