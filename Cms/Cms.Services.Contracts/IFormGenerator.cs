﻿using System.Collections.Generic;

namespace Cms.Services.Contracts
{
    public interface IFormGenerator
    {
        void AddField(string name, string type, bool isRequired, IEnumerable<string> options = null, bool isMultiple = false);

        string Render(FormLayoutType type);
    }

    public interface IFormGeneratorFactory
    {
        IFormGenerator Create(ulong formId);
    }

    public enum FormLayoutType
    {
        Above,
        Asside,
        NoLabel,
    }
}
