using System;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class UserLockedEventTranslator : IEventTranslator
    {
        private readonly IUserRepository _repository;

        public UserLockedEventTranslator(IUserRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(UserLocked);
        }

        public string GetHeader(IEvent @event)
        {
            return "Benutzer gesperrt";
        }

        public string GetContent(IEvent @event)
        {
            var user = _repository.GetEvenIfDeleted(@event.AggregateId);
            return string.Format("Der Benutzer <strong>{0}</strong> wurde gesperrt.", user.FullName);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}