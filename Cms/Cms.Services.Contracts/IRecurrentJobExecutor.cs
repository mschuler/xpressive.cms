﻿namespace Cms.Services.Contracts
{
    public interface IRecurrentJobExecutor
    {
        void Start();
    }
}