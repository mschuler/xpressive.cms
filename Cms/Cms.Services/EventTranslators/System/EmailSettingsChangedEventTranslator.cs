﻿using System;
using Cms.Aggregates.Settings.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.System
{
    internal class EmailSettingsChangedEventTranslator : IEventTranslator
    {
        public bool IsResponsibleFor(Type eventType)
        {
            return typeof(EmailSettingsChanged) == eventType;
        }

        public string GetHeader(IEvent @event)
        {
            return "E-Mail Einstellungen geändert";
        }

        public string GetContent(IEvent @event)
        {
            return "Die E-Mail Einstellungen wurden geändert.";
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return @event.AggregateId;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}
