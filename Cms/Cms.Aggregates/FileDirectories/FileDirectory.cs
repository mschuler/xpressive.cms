﻿using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.FileDirectories
{
    public class FileDirectory : AggregateBase
    {
        private readonly object _childrenLock = new object();
        private readonly IList<FileDirectory> _children;
        private readonly object _sharedForTenantsLock = new object();
        private readonly List<ulong> _sharedForTenants;

        public FileDirectory()
        {
            _children = new List<FileDirectory>();
            _sharedForTenants = new List<ulong>();
        }

        public ulong TenantId { get; set; }
        public ulong FileStoreId { get; set; }
        public string Name { get; set; }
        public FileDirectory Parent { get; set; }

        public IEnumerable<FileDirectory> Children
        {
            get
            {
                lock (_childrenLock)
                {
                    return _children.ToList();
                }
            }
        }

        public IEnumerable<ulong> SharedForTenants
        {
            get
            {
                lock (_sharedForTenantsLock)
                {
                    return _sharedForTenants.ToList();
                }
            }
        }

        public void Add(FileDirectory child)
        {
            lock (_childrenLock)
            {
                _children.Add(child);
            }
        }

        public void Remove(FileDirectory child)
        {
            lock (_childrenLock)
            {
                _children.Remove(child);
            }
        }

        public void Share(IEnumerable<ulong> tenantIds)
        {
            lock (_sharedForTenantsLock)
            {
                _sharedForTenants.Clear();
                _sharedForTenants.AddRange(tenantIds);
            }
        }
    }
}
