using Cms.Aggregates.Security;

namespace Cms.Aggregates.Pages.Queries
{
    [RequiredPermission("page", PermissionLevel.CanSee)]
    public class GetPagesWithChildren : QueryBase
    {
        public ulong WebsiteId { get; set; }
    }
}