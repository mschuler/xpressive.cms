using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Files.Commands
{
    [RequiredPermission("file", PermissionLevel.CanChange)]
    public class CreateFile : CommandBase
    {
        [Required, ValidId]
        public ulong FileDirectoryId { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(3)]
        public string Name { get; set; }

        [Required, ValidId]
        public ulong FileVersion { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string TemporaryFilePath { get; set; }
    }
}