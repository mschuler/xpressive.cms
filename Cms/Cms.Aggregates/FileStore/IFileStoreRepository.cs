﻿
namespace Cms.Aggregates.FileStore
{
    public interface IFileStoreRepository : IRepository<IFileStore>
    {
    }
}
