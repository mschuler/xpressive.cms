using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.PageTemplates.Commands
{
    [RequiredPermission("pagetemplate", PermissionLevel.CanChange)]
    public class CreatePageTemplate : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }

        [Range(1, int.MaxValue)]
        public int PublicId { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string ContentType { get; set; }
    }
}