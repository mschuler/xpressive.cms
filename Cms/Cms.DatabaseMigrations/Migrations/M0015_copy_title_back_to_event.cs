﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0015_copy_title_back_to_event : DatabaseMigrationBase
    {
        public M0015_copy_title_back_to_event() :base(15) { }

        public override void Execute(Database database)
        {
            var events = database.Query<EventDto>().Where(e => e.Event == "DynamicObjectCreated").ToList();

            foreach (var dto in events)
            {
                var @event = JsonConvert.DeserializeObject<DynamicObjectCreated>(dto.Data);

                @event.Title = @event.Values.First().Value;

                var data = JsonConvert.SerializeObject(@event);

                dto.Data = data;

                database.Update(dto);
            }
        }

        [TableName("Event")]
        [PrimaryKey("Version", AutoIncrement = false)]
        private class EventDto
        {
            public long Transaction { get; set; }
            public long Version { get; set; }
            public string AggregateType { get; set; }
            public long AggregateId { get; set; }
            public string Event { get; set; }
            public long UserId { get; set; }
            public string Data { get; set; }
            public bool IsDeleted { get; set; }
        }

        private class DynamicObjectCreated
        {
            public DynamicObjectCreated()
            {
                Values = new Dictionary<string, string>();
            }

            public int PublicId { get; set; }
            public ulong DefinitionId { get; set; }
            public string Title { get; set; }
            public DateTime Date { get; set; }
            public Dictionary<string, string> Values { get; set; }
        }
    }
}