using Cms.Aggregates.PageTemplates;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class PageTemplateInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "PageTemplate"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Attribute && tree.LeftItem != null)
            {
                CalculateAttribute(tree);
            }
        }

        private void CalculateAttribute(TreeItem tree)
        {
            var pageTemplate = tree.LeftItem.Result as PageTemplate;

            if (pageTemplate == null)
            {
                return;
            }

            switch (tree.Value.ToLowerInvariant())
            {
                case "id":
                    tree.Result = pageTemplate.PublicId;
                    break;
                case "name":
                    tree.Result = pageTemplate.Name;
                    break;
            }
        }
    }
}