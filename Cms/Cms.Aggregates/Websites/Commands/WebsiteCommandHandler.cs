using System;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Commands;
using Cms.Aggregates.Websites.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.Websites.Commands
{
    public class WebsiteCommandHandler : CommandHandlerBase
    {
        public const string DomainValidationRegex = @"^((([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,10})|localhost|(\d+\.){3}\d+)$";

        private readonly IWebsiteRepository _repository;
        private readonly IPageRepository _pageRepository;

        public WebsiteCommandHandler(IWebsiteRepository repository, IPageRepository pageRepository)
        {
            _repository = repository;
            _pageRepository = pageRepository;
        }

        public void When(CreateWebsite command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            var name = command.Name.Trim().RemoveControlChars();

            var e = new WebsiteCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TenantId = command.TenantId,
                PublicId = command.PublicId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(ChangeWebsiteName command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);
            var name = command.Name.Trim().RemoveControlChars();

            var e = new WebsiteNameChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(DeleteWebsite command, Action<IEvent> eventHandler)
        {
            var website = _repository.Get(command.AggregateId);
            var e = new WebsiteDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = website.Name,
            };

            eventHandler(e);
        }

        public void When(AddDomain command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);
            var host = command.Host.Trim().RemoveControlChars();

            if (_repository.GetByHost(host) != null)
            {
                throw new ExceptionWithReason("There is already a website with this domain host.");
            }

            var e = new DomainAdded
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Host = host,
            };

            eventHandler(e);
        }

        public void When(ChangeDomainHost command, Action<IEvent> eventHandler)
        {
            var oldHost = command.OldHost.Trim().RemoveControlChars();
            var newHost = command.NewHost.Trim().RemoveControlChars();

            if (oldHost.Equals(newHost, StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            if (!Equals(_repository.Get(command.AggregateId), _repository.GetByHost(oldHost)))
            {
                throw new ExceptionWithReason("Host not found for this website.");
            }

            if (_repository.GetByHost(newHost) != null)
            {
                throw new ExceptionWithReason("There is already a website with this domain host.");
            }

            var e = new DomainHostChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                OldHost = command.OldHost,
                NewHost = command.NewHost,
            };

            eventHandler(e);
        }

        public void When(RemoveDomain command, Action<IEvent> eventHandler)
        {
            var host = command.Host.Trim().RemoveControlChars();

            if (!Equals(_repository.Get(command.AggregateId), _repository.GetByHost(host)))
            {
                throw new ExceptionWithReason("Host not found for this website.");
            }

            var e = new DomainRemoved
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Host = host,
            };

            eventHandler(e);
        }

        public void When(ChangeDefaultDomain command, Action<IEvent> eventHandler)
        {
            var host = command.Host.Trim().RemoveControlChars();

            if (!Equals(_repository.Get(command.AggregateId), _repository.GetByHost(host)))
            {
                throw new ExceptionWithReason("Host not found for this website.");
            }

            var e = new DefaultDomainChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Host = host,
            };

            eventHandler(e);
        }

        public void When(SetDefaultPage command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);
            var page = _pageRepository.Get(command.PageId);

            if (page.WebsiteId != command.AggregateId)
            {
                throw new ExceptionWithReason("A page can't be the default of another website.");
            }

            var e = new DefaultPageSet
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                PageId = command.PageId,
            };

            eventHandler(e);
        }

        public void When(DeletePage command, Action<IEvent> eventHandler)
        {
            var page = _pageRepository.Get(command.AggregateId);
            var website = _repository.Get(page.WebsiteId);

            if (website.DefaultPageId == page.Id)
            {
                throw new ExceptionWithReason("You can't delete the default page of this website.");
            }
        }
    }
}