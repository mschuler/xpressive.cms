using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0007_add_public_id_to_websites : DatabaseMigrationBase
    {
        public M0007_add_public_id_to_websites() : base(7) { }

        public override void Execute(Database database)
        {
            var events = database.Query<EventDto>().Where(e => e.Event == "WebsiteCreated").ToList();
            var publicId = 1;

            foreach (var dto in events)
            {
                var json = dto.Data.Replace("\"Name\"", "\"PublicId\":" + publicId + ",\"Name\"");
                dto.Data = json;
                database.Update(dto);

                publicId++;
            }
        }
    }
}