﻿using Cms.EventSourcing.Contracts.Queries;

namespace Cms.Aggregates
{
    public abstract class QueryBase : IQuery
    {
        public ulong UserId { get; set; }

        public ulong TenantId { get; set; }
    }
}