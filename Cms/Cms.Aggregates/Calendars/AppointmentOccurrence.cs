﻿using Cms.Aggregates.Links;

namespace Cms.Aggregates.Calendars
{
    public class AppointmentOccurrence
    {
        public ulong AppointmentId { get; set; }
        public string Title { get; set; }
        public bool IsHighlight { get; set; }
        public DateTimeRange StartEnd { get; set; }
        public string Location { get; set; }
        public ILink Link { get; set; }
        public bool IsSingleOccurrence { get; set; }

        protected bool Equals(AppointmentOccurrence other)
        {
            return AppointmentId == other.AppointmentId && Equals(StartEnd, other.StartEnd);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }
            if (obj.GetType() != GetType()) { return false; }
            return Equals((AppointmentOccurrence)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (AppointmentId.GetHashCode() * 397) ^ (StartEnd != null ? StartEnd.GetHashCode() : 0);
            }
        }
    }
}