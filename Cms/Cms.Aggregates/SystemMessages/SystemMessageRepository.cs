﻿using System;
using Cms.SharedKernel;

namespace Cms.Aggregates.SystemMessages
{
    internal class SystemMessageRepository : RepositoryBase<SystemMessage>, ISystemMessageRepository
    {
        public override void DeleteAllByTenantId(ulong tenantId)
        {
            throw new NotSupportedException("System messages don't belong to tenants.").WithGuid();
        }
    }
}