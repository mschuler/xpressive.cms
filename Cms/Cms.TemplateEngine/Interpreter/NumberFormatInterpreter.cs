﻿using System;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    public sealed class NumberFormatInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "NumberFormatInterpreter"; } }

        public void Calculate(TreeItem treeItem)
        {
            if (treeItem.ItemType == TreeItemType.Function)
            {
                if (treeItem.LeftItem != null && treeItem.LeftItem.Result is IFormattable)
                {
                    var number = (IFormattable)treeItem.LeftItem.Result;

                    if (!string.IsNullOrEmpty(treeItem.Value) &&
                        treeItem.Value.Equals("format", StringComparison.OrdinalIgnoreCase) &&
                        treeItem.Children.Count == 1 &&
                        treeItem.Children[0].Result is string)
                    {
                        try
                        {
                            var format = (string)treeItem.Children[0].Result;
                            treeItem.Result = number.ToString(format, TemplateStateMap.CurrentCulture);
                        }
                        catch (FormatException) { }
                    }
                }
            }
        }
    }
}