using System;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.Files;
using Cms.Aggregates.Files.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Files
{
    internal class FileDeletedEventTranslator : IEventTranslator
    {
        private readonly IFileRepository _repository;
        private readonly IRepository<FileDirectory> _fileDirectoryRepository;

        public FileDeletedEventTranslator(IFileRepository repository, IRepository<FileDirectory> fileDirectoryRepository)
        {
            _repository = repository;
            _fileDirectoryRepository = fileDirectoryRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FileDeleted);
        }

        public string GetHeader(IEvent @event)
        {
            return "Datei gel�scht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FileDeleted)@event;
            return string.Format("Die Datei <strong>{0} (Nr. {1})</strong> wurde gel�scht.", e.Name, e.PublicId);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var file = _repository.GetEvenIfDeleted(@event.AggregateId);
            return _fileDirectoryRepository.GetEvenIfDeleted(file.FileDirectoryId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "file";
        }
    }
}