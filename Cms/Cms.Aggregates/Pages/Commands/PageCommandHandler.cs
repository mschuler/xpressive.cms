using System;
using System.Linq;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.PageTemplates;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.Pages.Commands
{
    public class PageCommandHandler : CommandHandlerBase
    {
        private readonly IPageRepository _repository;
        private readonly IPageTemplateRepository _pageTemplateRepository;
        private readonly IPageTemplateService _pageTemplateService;

        public PageCommandHandler(IPageRepository repository, IPageTemplateRepository pageTemplateRepository, IPageTemplateService pageTemplateService)
        {
            _repository = repository;
            _pageTemplateRepository = pageTemplateRepository;
            _pageTemplateService = pageTemplateService;
        }

        public void When(CreateNewPage command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            var name = command.Name.Trim().RemoveControlChars();
            var parent = command.ParentPageId > 0 ? _repository.Get(command.ParentPageId) : null;
            AssertThereIsNoOtherPageUnderTheSpecificParentWithTheSameName(null, parent, command.WebsiteId, name);
            AssertValidName(parent, name);

            if (parent != null && parent.WebsiteId != command.WebsiteId)
            {
                throw new ExceptionWithReason("This page its website id is not the same than its parent website id.");
            }

            var e = new PageCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                WebsiteId = command.WebsiteId,
                PublicId = command.PublicId,
                Name = name,
                Title = command.Title,
                ParentPageId = command.ParentPageId,
                Permalink = command.Permalink,
            };

            eventHandler(e);
        }

        public void When(RestorePageVersion command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            var e = new PageVersionRestored
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                VersionToRestore = command.VersionToRestore,
            };

            eventHandler(e);
        }

        public void When(ChangePageName command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var aggregate = _repository.Get(command.AggregateId);
            var parent = aggregate.ParentPageId > 0 ? _repository.Get(aggregate.ParentPageId) : null;
            AssertThereIsNoOtherPageUnderTheSpecificParentWithTheSameName(aggregate, parent, aggregate.WebsiteId, name);
            AssertValidName(parent, name);

            var e = new PageNameChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(ChangePageTitle command, Action<IEvent> eventHandler)
        {
            var title = command.Title.Trim().RemoveControlChars();
            _repository.Get(command.AggregateId);

            var e = new PageTitleChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Title = title,
            };

            eventHandler(e);
        }

        public void When(ChangePageContent command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var page = (Page)_repository.Get(command.AggregateId);

            var template = _pageTemplateRepository.GetEvenIfDeleted(page.PageTemplateId);
            var contents = page.GetPageContent(template, _pageTemplateService);
            var content = contents.SingleOrDefault(c => c.Name.Equals(name, StringComparison.Ordinal));

            if (content != null && content.Value != null)
            {
                if (content.Value.Equals(command.Value))
                {
                    return;
                }
            }

            var e = new PageContentChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
                Value = command.Value,
            };

            eventHandler(e);
        }

        public void When(MovePage command, Action<IEvent> eventHandler)
        {
            var aggregate = _repository.Get(command.AggregateId);
            var parent = command.ParentPageId > 0 ? _repository.Get(command.ParentPageId) : null;

            if (parent != null && parent.WebsiteId != aggregate.WebsiteId)
            {
                throw new ExceptionWithReason("This page its website id is not the same than its parent website id.");
            }

            AssertValidName(parent, aggregate.Name);
            AssertThereIsNoOtherPageUnderTheSpecificParentWithTheSameName(aggregate, parent, aggregate.WebsiteId, aggregate.Name);

            var e = new PageMoved
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                ParentPageId = command.ParentPageId,
                SortOrder = command.SortOrder,
            };

            eventHandler(e);
        }

        public void When(EnablePage command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            var e = new PageEnabled
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            };

            eventHandler(e);
        }

        public void When(DisablePage command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            var e = new PageDisabled
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            };

            eventHandler(e);
        }

        public void When(ChangePageTemplate command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            var e = new PageTemplateChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                PageTemplateId = command.PageTemplateId,
            };

            eventHandler(e);
        }

        public void When(DeletePage command, Action<IEvent> eventHandler)
        {
            var page = _repository.Get(command.AggregateId);

            DeletePage(page, command.UserId, eventHandler);
        }

        public void When(CreatePageLink command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            var name = command.Name.Trim().RemoveControlChars();
            var parent = command.ParentPageId > 0 ? _repository.Get(command.ParentPageId) : null;
            var url = command.Url.Trim().RemoveControlChars();
            AssertThereIsNoOtherPageUnderTheSpecificParentWithTheSameName(null, parent, command.WebsiteId, name);
            AssertValidName(parent, name);

            if (parent != null && parent.WebsiteId != command.WebsiteId)
            {
                throw new ExceptionWithReason("This page its website id is not the same than its parent website id.");
            }

            if (string.IsNullOrEmpty(url) && command.PageId == 0)
            {
                throw new ExceptionWithReason("You have to set either the linked page or the linked url.");
            }

            if (!string.IsNullOrEmpty(url) && command.PageId > 0)
            {
                throw new ExceptionWithReason("You have to set either the linked page or the linked url.");
            }

            Uri uri;
            if (!string.IsNullOrEmpty(url) && !Uri.TryCreate(url, UriKind.Absolute, out uri))
            {
                throw new ExceptionWithReason("This is not a valid url.");
            }

            eventHandler(new PageLinkCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                ParentPageId = command.ParentPageId,
                PageId = command.PageId,
                Name = name,
                WebsiteId = command.WebsiteId,
                Url = url,
            });
        }

        public void When(ChangePageLink command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var page = _repository.Get(command.AggregateId);
            var parent = page.ParentPageId > 0 ? _repository.Get(page.ParentPageId) : null;
            var url = command.Url.Trim().RemoveControlChars();
            AssertThereIsNoOtherPageUnderTheSpecificParentWithTheSameName(page, parent, page.WebsiteId, name);
            AssertValidName(parent, name);

            if (string.IsNullOrEmpty(url) && command.PageId == 0)
            {
                throw new ExceptionWithReason("You have to set either the linked page or the linked url.");
            }

            if (!string.IsNullOrEmpty(url) && command.PageId > 0)
            {
                throw new ExceptionWithReason("You have to set either the linked page or the linked url.");
            }

            Uri uri;
            if (!string.IsNullOrEmpty(url) && !Uri.TryCreate(url, UriKind.Absolute, out uri))
            {
                throw new ExceptionWithReason("This is not a valid url.");
            }

            eventHandler(new PageLinkChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                PageId = command.PageId,
                Name = name,
                Url = url,
            });
        }

        private void DeletePage(PageBase pageBase, ulong userId, Action<IEvent> eventHandler)
        {
            foreach (var child in pageBase.Children)
            {
                DeletePage(child, userId, eventHandler);
            }

            eventHandler(new PageDeleted
            {
                AggregateId = pageBase.Id,
                UserId = userId,
                Fullname = pageBase.Fullname,
            });
        }

        private void AssertValidName(PageBase parent, string name)
        {
            if (name.Contains("/"))
            {
                throw new ExceptionWithReason("A page name may not contain a slash, since this is the url separator.");
            }

            if (parent != null)
            {
                return;
            }

            if (PageBase.InvalidRootPageNames.Contains(name))
            {
                throw new ExceptionWithReason("A root page (page without parent) may not be named " + name);
            }
        }

        private void AssertThereIsNoOtherPageUnderTheSpecificParentWithTheSameName(PageBase page, PageBase parentPage, ulong websiteId, string newName)
        {
            var any = parentPage != null && parentPage.Children.Except(new[] { page }).Any(c => EqualUrlPart(c.Name, newName));

            if (parentPage == null)
            {
                any = _repository
                    .GetAll()
                    .Where(p => p.WebsiteId == websiteId)
                    .Where(p => p.ParentPageId == 0)
                    .Where(p => page == null || p.Id != page.Id)
                    .Any(p => EqualUrlPart(p.Name, newName));
            }

            if (any)
            {
                var parentName = parentPage != null ? parentPage.Name : "(root)";
                throw new ExceptionWithReason(string.Format("Parent page {0} already has a child page called {1}.", parentName, newName));
            }
        }

        private static bool EqualUrlPart(string s1, string s2)
        {
            return s1.ToValidUrlPart().Equals(s2.ToValidUrlPart(), StringComparison.OrdinalIgnoreCase);
        }
    }
}