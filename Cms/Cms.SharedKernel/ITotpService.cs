﻿namespace Cms.SharedKernel
{
    public interface ITotpService
    {
        ITotp Generate(string username);

        bool Verify(byte[] secret, string code);
    }

    public interface ITotp
    {
        byte[] Secret { get; }
        string SecretBase32 { get; }
        string Url { get; }
    }
}
