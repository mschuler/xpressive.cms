using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Security;
using Cms.Aggregates.Websites;
using Cms.Aggregates.Websites.Commands;
using Cms.Aggregates.Websites.Queries;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class WebsiteController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _uniqueIdProvider;
        private readonly IWebsiteRepository _repository;

        public WebsiteController(IUniqueIdProvider uniqueIdProvider, ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IWebsiteRepository repository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _uniqueIdProvider = uniqueIdProvider;
            _repository = repository;
        }

        [HttpGet, Route("api/websites/{tenantId:ulong}")]
        public IEnumerable<object> GetAll(ulong tenantId)
        {
            return ExecuteQuery(new GetAllWebsites { TenantId = tenantId });
        }

        [HttpGet, Route("api/website/{websiteId:ulong}")]
        public object Get(ulong websiteId)
        {
            var tenantId = _repository.Get(websiteId).TenantId;
            var result = ExecuteQuery(new GetWebsiteWithDomains
            {
                WebsiteId = websiteId,
                TenantId = tenantId
            });
            return result.FirstOrDefault();
        }

        [HttpPut, Route("api/website/{tenantId:ulong}")]
        public object Create(ulong tenantId, [FromBody]WebsiteNameDto dto)
        {
            var id = _uniqueIdProvider.GetId();
            var publicId = _repository.GetNextPublicId();

            SendCommand(new CreateWebsite
            {
                AggregateId = id,
                TenantId = tenantId,
                Name = dto.Name,
                PublicId = publicId,
            });

            return new
            {
                Id = id.ToJsonString(),
                Nr = publicId,
                dto.Name,
            };
        }

        [HttpPost, Route("api/website/{websiteId:ulong}")]
        public void Rename(ulong websiteId, [FromBody]WebsiteNameDto dto)
        {
            var tenantId = _repository.Get(websiteId).TenantId;
            SendCommand(new ChangeWebsiteName
            {
                AggregateId = websiteId,
                Name = dto.Name,
                TenantId = tenantId
            });
        }

        [HttpDelete, Route("api/website/{websiteId:ulong}")]
        public void Delete(ulong websiteId)
        {
            var tenantId = _repository.Get(websiteId).TenantId;
            SendCommand(new DeleteWebsite
            {
                AggregateId = websiteId,
                TenantId = tenantId
            });
        }

        [HttpPut, Route("api/website/{websiteId:ulong}/domain")]
        public void AddDomain(ulong websiteId, [FromBody]DomainNameDto dto)
        {
            var tenantId = _repository.Get(websiteId).TenantId;
            SendCommand(new AddDomain
            {
                AggregateId = websiteId,
                Host = dto.Domain,
                TenantId = tenantId
            });
        }

        [HttpDelete, Route("api/website/{websiteId:ulong}/domain")]
        public void RemoveDomain(ulong websiteId, [FromUri]string name)
        {
            var tenantId = _repository.Get(websiteId).TenantId;
            SendCommand(new RemoveDomain
            {
                AggregateId = websiteId,
                Host = name,
                TenantId = tenantId
            });
        }

        [HttpPost, Route("api/website/{websiteId:ulong}/domain")]
        public void RenameDomain(ulong websiteId, [FromBody]RenameDomainDto dto)
        {
            var tenantId = _repository.Get(websiteId).TenantId;
            SendCommand(new ChangeDomainHost
            {
                AggregateId = websiteId,
                OldHost = dto.OldDomain,
                NewHost = dto.NewDomain,
                TenantId = tenantId
            });
        }

        [HttpPost, Route("api/website/{websiteId:ulong}/default")]
        public void SetDefaultDomain(ulong websiteId, [FromBody]DomainNameDto dto)
        {
            var tenantId = _repository.Get(websiteId).TenantId;
            SendCommand(new ChangeDefaultDomain
            {
                AggregateId = websiteId,
                Host = dto.Domain,
                TenantId = tenantId
            });
        }

        [HttpPost, Route("api/website/{websiteId:ulong}/defaultpage/{pageId:ulong}")]
        public void SetDefaultPage(ulong websiteId, ulong pageId)
        {
            var tenantId = _repository.Get(websiteId).TenantId;
            SendCommand(new SetDefaultPage
            {
                AggregateId = websiteId,
                PageId = pageId,
                TenantId = tenantId
            });
        }

        public class DomainNameDto
        {
            public string Domain { get; set; }
        }

        public class WebsiteNameDto
        {
            public string Name { get; set; }
        }

        public class RenameDomainDto
        {
            public string OldDomain { get; set; }
            public string NewDomain { get; set; }
        }
    }
}