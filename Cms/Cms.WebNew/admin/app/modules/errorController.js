﻿(function() {
    angular.module("app").controller("ErrorController", ["$scope", "$routeParams", function ($scope, $routeParams) {
        var id = $routeParams.id;

        $scope.showMessage = false;

        if (id) {
            $scope.id = id;
            $scope.showMessage = true;
        }
    }]);
}());