﻿using System;
using System.Linq;
using Cms.EventSourcing.Contracts.Events;
using Cms.SharedKernel;

namespace Cms.Aggregates.Security.Events
{
    public class UserEventHandler : EventHandlerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IEncryptionService _encryptionService;

        public UserEventHandler(IUserRepository userRepository, IEncryptionService encryptionService)
        {
            _userRepository = userRepository;
            _encryptionService = encryptionService;
        }

        public override void Handle(IEvent @event)
        {
            User user;
            if (_userRepository.TryGet(@event.UserId, out user))
            {
                if (@event.Date > user.RecentActivity)
                {
                    user.RecentActivity = @event.Date;
                }
            }

            base.Handle(@event);
        }

        public void When(UserCreated @event)
        {
            var user = new User
            {
                Id = @event.AggregateId,
                EmailAddress = @event.EmailAddress,
                FirstName = @event.FirstName,
                LastName = @event.LastName,
                IsSysadmin = @event.IsSysadmin,
                Password = @event.Password,
            };
            _userRepository.Add(user);
        }

        public void When(UserLocked @event)
        {
            var user = _userRepository.Get(@event.AggregateId);
            user.IsLocked = true;
        }

        public void When(UserUnlocked @event)
        {
            var user = _userRepository.Get(@event.AggregateId);
            user.IsLocked = false;
        }

        public void When(UserNameChanged @event)
        {
            var user = _userRepository.Get(@event.AggregateId);
            user.FirstName = @event.FirstName;
            user.LastName = @event.LastName;
        }

        public void When(UserEmailAddressChanged @event)
        {
            var user = _userRepository.Get(@event.AggregateId);
            user.EmailAddress = @event.EmailAddress;
        }

        public void When(PasswordChanged @event)
        {
            var user = _userRepository.Get(@event.AggregateId);
            user.Password = @event.Password;
        }

        public void When(U2FDeviceAdded @event)
        {
            var user = _userRepository.Get(@event.AggregateId);

            var publicKey = _encryptionService.Decrypt(Convert.FromBase64String(@event.PublicKey));
            var attestationCert = _encryptionService.Decrypt(Convert.FromBase64String(@event.AttestationCert));
            var keyHandle = _encryptionService.Decrypt(Convert.FromBase64String(@event.KeyHandle));

            user.AddU2FDevice(publicKey, attestationCert, keyHandle);
        }

        public void When(U2FDeviceRemoved @event)
        {
            var user = _userRepository.Get(@event.AggregateId);

            var publicKey = _encryptionService.Decrypt(Convert.FromBase64String(@event.PublicKey));

            var device = user.U2FDevices.SingleOrDefault(d => d.PublicKey.SequenceEqual(publicKey));

            if (device != null)
            {
                user.RemoveU2FDevice(device);
            }
        }

        public void When(Totp2FaEnabled @event)
        {
            var user = _userRepository.Get(@event.AggregateId);
            var secretBase64 = @event.TotpSecret;
            var secretEncrypted = Convert.FromBase64String(secretBase64);
            var secret = _encryptionService.Decrypt(secretEncrypted);
            user.EnableTotp(secret);
        }

        public void When(Totp2FaDisabled @event)
        {
            var user = _userRepository.Get(@event.AggregateId);
            user.DisableTotp();
        }

        public void When(UserSysadminChanged @event)
        {
            var user = _userRepository.Get(@event.AggregateId);
            user.IsSysadmin = @event.IsSysadmin;
        }

        public void When(UserDeleted @event)
        {
            _userRepository.Delete(@event.AggregateId);
        }
    }
}