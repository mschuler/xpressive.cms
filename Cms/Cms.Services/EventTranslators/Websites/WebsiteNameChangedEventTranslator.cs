﻿using System;
using Cms.Aggregates.Websites;
using Cms.Aggregates.Websites.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Websites
{
    internal class WebsiteNameChangedEventTranslator : IEventTranslator
    {
        private readonly IWebsiteRepository _repository;

        public WebsiteNameChangedEventTranslator(IWebsiteRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(WebsiteNameChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Websitename geändert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (WebsiteNameChanged)@event;
            return string.Format("Eine Website wurde in <strong>{0}</strong> umbenannt", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "website";
        }
    }
}