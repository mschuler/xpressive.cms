﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.Security.Commands
{
    public class UserCommandHandler : CommandHandlerBase
    {
        private readonly IUserRepository _repository;
        private readonly IGroupRepository _groupRepository;
        private readonly IEncryptionService _encryptionService;
        private readonly ITotpService _totpService;

        public UserCommandHandler(IUserRepository repository, IGroupRepository groupRepository, IEncryptionService encryptionService, ITotpService totpService)
        {
            _repository = repository;
            _groupRepository = groupRepository;
            _encryptionService = encryptionService;
            _totpService = totpService;
        }

        public void When(CreateUser command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            var emailAddress = command.EmailAddress.RemoveControlChars();

            if (_repository.GetByEmailAddress(emailAddress) != null)
            {
                throw new ExceptionWithReason("There is already a user with this email address.");
            }

            if (emailAddress.Contains(":"))
            {
                throw new ExceptionWithReason("An email address must not contain colons (:).");
            }

            var creator = _repository.Get(command.UserId);
            if (command.IsSysadmin && !creator.IsSysadmin)
            {
                throw new ExceptionWithReason("You are not allowed to create a new user as sysadmin when you are not a sysadmin.");
            }

            var password = BCrypt.Net.BCrypt.HashPassword(command.Password.RemoveControlChars());

            var e = new UserCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                EmailAddress = emailAddress,
                FirstName = command.FirstName.RemoveControlChars(),
                LastName = command.LastName.RemoveControlChars(),
                IsSysadmin = command.IsSysadmin,
                Password = password,
            };

            eventHandler(e);
        }

        public void When(ChangeUserName command, Action<IEvent> eventHandler)
        {
            AssertUserCanChangeOtherUser(command.UserId, command.AggregateId, PermissionLevel.CanChange);

            var e = ChangeUserName(command.AggregateId, command.UserId, command.FirstName, command.LastName);
            if (e != null)
            {
                eventHandler(e);
            }
        }

        public void When(ChangeOwnName command, Action<IEvent> eventHandler)
        {
            if (command.AggregateId != command.UserId)
            {
                throw new ExceptionWithReason("This command is only allowed for your own user.");
            }

            var e = ChangeUserName(command.AggregateId, command.UserId, command.FirstName, command.LastName);
            if (e != null)
            {
                eventHandler(e);
            }
        }

        private IEvent ChangeUserName(ulong aggregateId, ulong userId, string firstName, string lastName)
        {
            var user = _repository.Get(aggregateId);
            firstName = firstName.RemoveControlChars();
            lastName = lastName.RemoveControlChars();

            if (string.Equals(user.FirstName, firstName, StringComparison.Ordinal) &&
                string.Equals(user.LastName, lastName, StringComparison.Ordinal))
            {
                return null;
            }

            return new UserNameChanged
            {
                AggregateId = aggregateId,
                UserId = userId,
                FirstName = firstName,
                LastName = lastName,
            };
        }

        public void When(ChangeUserEmailAddress command, Action<IEvent> eventHandler)
        {
            AssertUserCanChangeOtherUser(command.UserId, command.AggregateId, PermissionLevel.CanChange);

            var user = _repository.Get(command.AggregateId);
            var emailAddress = command.EmailAddress.RemoveControlChars();

            if (string.Equals(user.EmailAddress, emailAddress, StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            var otherUser = _repository.GetByEmailAddress(emailAddress);
            if (otherUser != null && otherUser.Id != command.AggregateId)
            {
                throw new ExceptionWithReason("There is already another user with this email address.");
            }

            var e = ChangeEmailAddress(command.AggregateId, command.UserId, emailAddress);
            eventHandler(e);
        }

        public void When(ChangeOwnEmailAddress command, Action<IEvent> eventHandler)
        {
            if (command.AggregateId != command.UserId)
            {
                throw new ExceptionWithReason("This command is only allowed for your own user.");
            }

            var user = _repository.Get(command.AggregateId);
            var emailAddress = command.EmailAddress.RemoveControlChars();

            if (string.Equals(user.EmailAddress, emailAddress, StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            var otherUser = _repository.GetByEmailAddress(emailAddress);
            if (otherUser != null && otherUser.Id != command.AggregateId)
            {
                throw new ExceptionWithReason("There is already another user with this email address.");
            }

            var e = ChangeEmailAddress(command.AggregateId, command.UserId, emailAddress);
            eventHandler(e);
        }

        public void When(ChangeUserPassword command, Action<IEvent> eventHandler)
        {
            AssertUserCanChangeOtherUser(command.UserId, command.AggregateId, PermissionLevel.CanChange);

            var e = ChangePassword(command.AggregateId, command.UserId, command.Password);
            eventHandler(e);
        }

        public void When(ChangeOwnPassword command, Action<IEvent> eventHandler)
        {
            if (command.AggregateId != command.UserId)
            {
                throw new ExceptionWithReason("This command is only allowed for your own user.");
            }

            var user = _repository.Get(command.AggregateId);
            if (!BCrypt.Net.BCrypt.Verify(command.OldPassword, user.Password))
            {
                throw new ExceptionWithReason("Your entered password is invalid.");
            }

            var e = ChangePassword(command.AggregateId, command.UserId, command.NewPassword);
            eventHandler(e);
        }

        public void When(AddU2FDevice command, Action<IEvent> eventHandler)
        {
            if (command.AggregateId != command.UserId)
            {
                throw new ExceptionWithReason("This command is only allowed for your own user.");
            }

            var user = _repository.Get(command.AggregateId);
            if (!BCrypt.Net.BCrypt.Verify(command.Password, user.Password))
            {
                throw new ExceptionWithReason("Your entered password is invalid.");
            }
            if (!user.IsTotpEnabled)
            {
                throw new ExceptionWithReason("You must enable 2FA first.");
            }

            var publicKey = Convert.ToBase64String(_encryptionService.Encrypt(command.PublicKey));
            var attestationCert = Convert.ToBase64String(_encryptionService.Encrypt(command.AttestationCert));
            var keyHandle = Convert.ToBase64String(_encryptionService.Encrypt(command.KeyHandle));

            var e = new U2FDeviceAdded
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                PublicKey = publicKey,
                AttestationCert = attestationCert,
                KeyHandle = keyHandle,
            };

            eventHandler(e);
        }

        public void When(RemoveU2FDevice command, Action<IEvent> eventHandler)
        {
            if (command.AggregateId != command.UserId)
            {
                throw new ExceptionWithReason("This command is only allowed for your own user.");
            }

            var publicKey = Convert.ToBase64String(_encryptionService.Encrypt(command.PublicKey));

            var e = new U2FDeviceRemoved
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                PublicKey = publicKey,
            };

            eventHandler(e);
        }

        public void When(EnableTotp2Fa command, Action<IEvent> eventHandler)
        {
            if (command.AggregateId != command.UserId)
            {
                throw new ExceptionWithReason("This command is only allowed for your own user.");
            }

            var user = _repository.Get(command.AggregateId);
            if (!BCrypt.Net.BCrypt.Verify(command.Password, user.Password))
            {
                throw new ExceptionWithReason("Your entered password is invalid.");
            }

            if (!_totpService.Verify(command.TotpSecret, command.TotpCode))
            {
                throw new ExceptionWithReason("Your entered code is invalid.");
            }

            var secretEncrypted = _encryptionService.Encrypt(command.TotpSecret);
            var secretBase64 = Convert.ToBase64String(secretEncrypted);

            var e = new Totp2FaEnabled
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TotpSecret = secretBase64,
            };

            eventHandler(e);
        }

        public void When(DisableTotp2Fa command, Action<IEvent> eventHandler)
        {
            if (command.AggregateId != command.UserId)
            {
                throw new ExceptionWithReason("This command is only allowed for your own user.");
            }

            var e = new Totp2FaDisabled
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            };

            eventHandler(e);
        }

        public void When(DisableTotp2FaForOtherUser command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);
            var creator = _repository.Get(command.UserId);

            if (!creator.IsSysadmin)
            {
                throw new ExceptionWithReason("You are not allowed to remove 2FA when you are not a sysadmin.");
            }

            var e = new Totp2FaDisabled
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            };

            eventHandler(e);
        }

        public void When(ChangeUserSysadmin command, Action<IEvent> eventHandler)
        {
            var user = _repository.Get(command.AggregateId);
            var creator = _repository.Get(command.UserId);

            if (user.IsSysadmin == command.IsSysadmin)
            {
                return;
            }

            if (!creator.IsSysadmin)
            {
                throw new ExceptionWithReason("You are not allowed to delete a sysadmin when you are not a sysadmin.");
            }

            if (user.Id == creator.Id)
            {
                throw new ExceptionWithReason("You cannot change your own sysadmin rights.");
            }

            var e = new UserSysadminChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                IsSysadmin = command.IsSysadmin,
            };

            eventHandler(e);
        }

        public void When(DeleteUser command, Action<IEvent> eventHandler)
        {
            var user = _repository.Get(command.AggregateId);
            var creator = _repository.Get(command.UserId);

            AssertUserCanChangeOtherUser(creator, user, PermissionLevel.CanEverything);

            if (user.IsSysadmin && !creator.IsSysadmin)
            {
                throw new ExceptionWithReason("You are not allowed to delete a sysadmin when you are not a sysadmin.");
            }

            if (user.Id == creator.Id)
            {
                throw new ExceptionWithReason("You cannot remove yourself.");
            }

            var e = new UserDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                FullName = user.FullName,
            };

            eventHandler(e);
        }

        public void When(LockUser command, Action<IEvent> eventHandler)
        {
            var user = _repository.Get(command.AggregateId);
            var creator = _repository.Get(command.UserId);

            AssertUserCanChangeOtherUser(creator, user, PermissionLevel.CanEverything);

            if (user.IsSysadmin && !creator.IsSysadmin)
            {
                throw new ExceptionWithReason("You are not allowed to lock a sysadmin when you are not a sysadmin.");
            }

            if (user.Id == creator.Id)
            {
                throw new ExceptionWithReason("You cannot lock yourself.");
            }

            eventHandler(new UserLocked
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            });
        }

        public void When(UnlockUser command, Action<IEvent> eventHandler)
        {
            var user = _repository.Get(command.AggregateId);
            var creator = _repository.Get(command.UserId);

            AssertUserCanChangeOtherUser(creator, user, PermissionLevel.CanEverything);

            if (user.Id == creator.Id)
            {
                throw new ExceptionWithReason("You cannot unlock yourself.");
            }

            eventHandler(new UserUnlocked
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            });
        }

        private IEvent ChangeEmailAddress(ulong aggregateId, ulong userId, string emailAddress)
        {
            var user = _repository.Get(aggregateId);
            var creator = _repository.Get(userId);

            if (user.IsSysadmin && !creator.IsSysadmin)
            {
                throw new ExceptionWithReason("You are not allowed to change the email address of a sysadmin when you are not a sysadmin.");
            }

            return new UserEmailAddressChanged
            {
                AggregateId = aggregateId,
                UserId = userId,
                EmailAddress = emailAddress,
            };
        }

        private IEvent ChangePassword(ulong aggregateId, ulong userId, string password)
        {
            var user = _repository.Get(aggregateId);
            var creator = _repository.Get(userId);

            if (user.IsSysadmin && !creator.IsSysadmin)
            {
                throw new ExceptionWithReason("You are not allowed to change the password of a sysadmin when you are not a sysadmin.");
            }

            var passwordHash = BCrypt.Net.BCrypt.HashPassword(password.RemoveControlChars());

            return new PasswordChanged
            {
                AggregateId = aggregateId,
                UserId = userId,
                Password = passwordHash,
            };
        }

        private void AssertUserCanChangeOtherUser(ulong currentUserId, ulong userToChangeId, PermissionLevel permissionLevel)
        {
            var currentUser = _repository.Get(currentUserId);
            var userToChange = _repository.Get(userToChangeId);
            AssertUserCanChangeOtherUser(currentUser, userToChange, permissionLevel);
        }

        private void AssertUserCanChangeOtherUser(User currentUser, User userToChange, PermissionLevel permissionLevel)
        {
            if (CanChangeUser(currentUser, userToChange, permissionLevel))
            {
                return;
            }

            throw new ExceptionWithReason("You can't make changes on this user because you have insufficient rights in some tenants this user is member of.");
        }

        private bool CanChangeUser(User currentUser, User userToChange, PermissionLevel permissionLevel)
        {
            if (currentUser.IsSysadmin)
            {
                return true;
            }

            if (userToChange.IsSysadmin)
            {
                return false;
            }

            var groups = GetGroupsByUser(userToChange);
            var tenantIds = groups.Select(g => g.TenantId).Distinct().ToList();

            foreach (var tenantId in tenantIds)
            {
                if (!currentUser.HasPermission("user", tenantId, permissionLevel, _groupRepository))
                {
                    return false;
                }
            }

            return false;
        }

        private IEnumerable<Group> GetGroupsByUser(User user)
        {
            foreach (var groupId in user.GroupIds)
            {
                Group group;
                if (_groupRepository.TryGet(groupId, out group))
                {
                    yield return group;
                }
            }
        }
    }
}