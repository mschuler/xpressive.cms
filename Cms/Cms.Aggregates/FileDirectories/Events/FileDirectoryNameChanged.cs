﻿namespace Cms.Aggregates.FileDirectories.Events
{
    public class FileDirectoryNameChanged : EventBase<FileDirectory>
    {
        public string Name { get; set; }
    }
}