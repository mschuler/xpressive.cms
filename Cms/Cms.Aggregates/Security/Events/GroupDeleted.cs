﻿namespace Cms.Aggregates.Security.Events
{
    public class GroupDeleted : EventBase<Group>
    {
        public string Name { get; set; }
    }
}