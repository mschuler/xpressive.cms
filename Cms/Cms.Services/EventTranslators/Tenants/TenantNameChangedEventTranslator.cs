﻿using System;
using Cms.Aggregates.Tenants.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Tenants
{
    internal class TenantNameChangedEventTranslator : IEventTranslator
    {
        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(TenantNameChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Mandant umbenannt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (TenantNameChanged)@event;
            return string.Format("Ein Mandant wurde in <strong>{0}</strong> umbenannt.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return @event.AggregateId;
        }

        public string GetAffectedModuleId()
        {
            return "tenant";
        }
    }
}