using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cms.Services.Contracts;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class ContentHandlerController : ApiController
    {
        private readonly IContentHandlerFactory _factory;

        public ContentHandlerController(IContentHandlerFactory factory)
        {
            _factory = factory;
        }

        [HttpGet, AllowAnonymous, Route("api/contenthandler")]
        public IEnumerable<ContentTypeDto> Get()
        {
            var dtos = from ct in _factory.ContentTypes
                       let factory = _factory.Get(ct)
                       select new ContentTypeDto
                       {
                           Name = ct,
                           Icon = factory.GetImage()
                       };
            return dtos.OrderBy(d => d.Name);
        }

        public class ContentTypeDto
        {
            public string Name { get; set; }

            public string Icon { get; set; }
        }
    }
}