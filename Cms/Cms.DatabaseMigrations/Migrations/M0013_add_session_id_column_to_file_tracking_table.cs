namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0013_add_session_id_column_to_file_tracking_table : DatabaseSqlMigration
    {
        public M0013_add_session_id_column_to_file_tracking_table() : base(13) { }

        protected override string GetSql()
        {
            return "alter table FileTracking add SessionId nvarchar(50) default '' not null";
        }
    }
}