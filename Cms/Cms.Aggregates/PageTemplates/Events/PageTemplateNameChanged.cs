﻿namespace Cms.Aggregates.PageTemplates.Events
{
    public class PageTemplateNameChanged : EventBase<PageTemplate>
    {
        public string Name { get; set; }
    }
}