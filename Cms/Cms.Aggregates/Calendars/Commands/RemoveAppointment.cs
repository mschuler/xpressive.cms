using Cms.Aggregates.Security;

namespace Cms.Aggregates.Calendars.Commands
{
    [RequiredPermission("appointment", PermissionLevel.CanEverything)]
    public class RemoveAppointment : CommandBase
    {
        [ValidId]
        public ulong AppointmentId { get; set; }
    }
}