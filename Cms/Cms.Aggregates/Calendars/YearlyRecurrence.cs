using System;

namespace Cms.Aggregates.Calendars
{
    public class YearlyRecurrence : AppointmentRecurrence
    {
        private int _everyXthYear;
        private int _everyXthDay;
        private int _month;

        public DateTime? EndDate { get; set; }
        public bool IsSpecificDay { get; set; }
        public DayRecurrence DayRecurrence { get; set; }

        public int EveryXthYear
        {
            get { return Math.Max(1, _everyXthYear); }
            set { _everyXthYear = value; }
        }

        public int EveryXthDay // entweder [1;31] oder [first, second, third, fourth, last]
        {
            get { return Math.Max(1, Math.Min(31, _everyXthDay)); }
            set { _everyXthDay = value; }
        }

        public int Month // [1;12]
        {
            get { return Math.Max(1, Math.Min(12, _month)); }
            set { _month = value; }
        }

        public bool HasEndDate
        {
            get { return EndDate.HasValue; }
            set
            {
                if (!value)
                {
                    EndDate = null;
                }
            }
        }

        public override bool IsMatch(Appointment appointment, DateTime dateTime)
        {
            if (dateTime < appointment.StartDate.Date)
            {
                return false;
            }

            if (EndDate.HasValue && dateTime > EndDate.Value)
            {
                return false;
            }

            if (dateTime.Month != Month)
            {
                return false;
            }

            if (IsSpecificDay)
            {
                if (dateTime.Day != EveryXthDay)
                {
                    return false;
                }
            }
            else
            {
                var dateTimeRecurrence = dateTime.DayOfWeek.ToRecurrence();
                if (!DayRecurrence.HasFlag(dateTimeRecurrence))
                {
                    return false;
                }

                var totalDayRecurrences = 0;
                var dayRecurrence = 0;

                for (var d = 1; d <= DateTime.DaysInMonth(dateTime.Year, Month); d++)
                {
                    if (DayRecurrence.HasFlag(new DateTime(dateTime.Year, Month, d).DayOfWeek.ToRecurrence()))
                    {
                        totalDayRecurrences++;
                        if (d <= dateTime.Day)
                        {
                            dayRecurrence++;
                        }
                    }
                }

                if (EveryXthDay <= 4 && dayRecurrence % EveryXthDay != 0)
                {
                    return false;
                }
                if (EveryXthDay == 5 && totalDayRecurrences != dayRecurrence)
                {
                    return false;
                }
            }

            if (EveryXthYear <= 0)
            {
                return true;
            }

            var totalYears = dateTime.Year - appointment.StartDate.Year + 1;
            return totalYears % EveryXthYear == 0;
        }
    }
}