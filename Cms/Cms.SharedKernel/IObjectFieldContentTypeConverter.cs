﻿namespace Cms.SharedKernel
{
    public interface IObjectFieldContentTypeConverter
    {
        bool CanHandle(string type);

        string Serialize(string json);

        object Deserialize(string value);

        SortableObjectField DeserializeForSortableField(string value);

        object DeserializeForPublicApi(string value);

        bool TryGetMetadataForPublicApi(string value, out object metadata);

        object DeserializeForInterpreter(string value);

        string SerializeForImport(string json, string format, string options);

        string SerializeForImportPreview(string json, string format, string options);

        bool IsValidValueForImport(string json, string format, string options);
    }
}
