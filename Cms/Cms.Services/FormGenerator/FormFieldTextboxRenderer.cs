using System;
using System.Text;
using Cms.Services.Contracts;

namespace Cms.Services.FormGenerator
{
    internal class FormFieldTextboxRenderer : IFormFieldRenderer
    {
        private readonly string _name;
        private readonly bool _isRequired;

        public FormFieldTextboxRenderer(string name, bool isRequired)
        {
            _name = name;
            _isRequired = isRequired;
        }

        public void Render(FormLayoutType type, StringBuilder builder, string id)
        {
            var required = _isRequired ? "required " : string.Empty;

            if (type == FormLayoutType.Above)
            {
                builder.AppendLine("  <div class=\"form-group\">");
                builder.AppendFormat("    <label for=\"{0}\">{1}</label>", id, _name).AppendLine();
                builder.AppendFormat("    <input type=\"text\" class=\"form-control\" id=\"{0}\" name=\"{1}\" {2}/>", id, _name, required);
                builder.AppendLine();
                builder.AppendLine("  </div>");
            }
            else if (type == FormLayoutType.Asside)
            {
                builder.AppendLine("  <div class=\"form-group\">");
                builder.AppendFormat("    <label for=\"{0}\" class=\"col-sm-2 control-label\">{1}</label>", id, _name);
                builder.AppendLine();
                builder.AppendLine("    <div class=\"col-sm-10\">");
                builder.AppendFormat("      <input type=\"text\" class=\"form-control\" id=\"{0}\" name=\"{1}\" {2}/>", id, _name, required);
                builder.AppendLine();
                builder.AppendLine("    </div>");
                builder.AppendLine("  </div>");
            }
            else if (type == FormLayoutType.NoLabel)
            {
                builder.AppendFormat("  <input type=\"text\" class=\"form-control\" id=\"{0}\" name=\"{1}\" placeholder=\"{1}\" {2}/>", id, _name, required);
                builder.AppendLine();
            }
            else
            {
                throw new NotSupportedException(type.ToString());
            }
        }
    }
}