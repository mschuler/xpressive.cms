﻿using System;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class UserEmailAddressChangedEventTranslator : IEventTranslator
    {
        private readonly IUserRepository _repository;

        public UserEmailAddressChangedEventTranslator(IUserRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(UserEmailAddressChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "E-Mail Adresse geändert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (UserEmailAddressChanged)@event;
            var user = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Die E-Mail Adresse des Benutzers <strong>{0}</strong> wurde geändert.", user.FullName);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}