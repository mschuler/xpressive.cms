using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using System.Web.OData.Query;
using Cms.Aggregates;
using Cms.Aggregates.Security;
using Cms.Aggregates.Tenants;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;
using Cms.WebNew.Authentication;
using log4net;
using Newtonsoft.Json;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class EventLogController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(EventLogController));
        private readonly IUserRepository _userRepository;
        private readonly IEventTranslationService _eventTranslationService;
        private readonly ITenantRepository _tenantRepository;

        public EventLogController(IUserRepository userRepository, IEventTranslationService eventTranslationService, ITenantRepository tenantRepository)
        {
            _userRepository = userRepository;
            _eventTranslationService = eventTranslationService;
            _tenantRepository = tenantRepository;
        }

        [HttpGet, Route("api/events/{tenantId:ulong}")]
        public IEnumerable<EventDto> Get(ulong tenantId, ODataQueryOptions odataOptions)
        {
            var top = Math.Min(odataOptions.Top.Value, 100);
            var skip = Math.Max(odataOptions.Skip.Value, 0);

            if (top <= 0)
            {
                return new List<EventDto>(0);
            }

            var user = _userRepository.Get(User.GetId());
            var events = GetEventsInternal(tenantId, user, skip, top);
            return events;
        }

        [HttpGet, Route("api/events/{tenantId:ulong}/user/{userId:ulong}")]
        public IEnumerable<EventDto> GetByUser(ulong tenantId, ulong userId, ODataQueryOptions odataOptions)
        {
            var top = Math.Min(odataOptions.Top.Value, 100);
            var skip = Math.Max(odataOptions.Skip.Value, 0);

            if (top <= 0)
            {
                return new List<EventDto>(0);
            }

            var user = _userRepository.Get(User.GetId());
            var events = GetEventsInternal(tenantId, user, skip, top);
            return events.Where(e => e.UserId == userId);
        }

        [HttpGet, Route("api/events/{tenantId:ulong}/object/{aggregateType}/{aggregateId:ulong}")]
        public IEnumerable<EventDto> GetByAggregate(ulong tenantId, string aggregateType, ulong aggregateId, ODataQueryOptions odataOptions)
        {
            var top = Math.Min(odataOptions.Top.Value, 100);
            var skip = Math.Max(odataOptions.Skip.Value, 0);

            if (top <= 0)
            {
                return new List<EventDto>(0);
            }

            var user = _userRepository.Get(User.GetId());
            var events = GetEventsInternal(tenantId, user, skip, top);
            return events
                .Where(e => e.AggregateIdLong == aggregateId)
                .Where(e => e.AggregateType.Equals(aggregateType, StringComparison.OrdinalIgnoreCase));
        }

        [HttpDelete, Route("api/events/{eventVersion:ulong}")]
        public void Rollback(ulong eventVersion)
        {
            // TODO: alle events herausfinden, die betroffen sind.
            // TODO: alle der gleichen transaktion
            // TODO: eventuell alle betroffenen aggregate in liste sammeln und vorzu alle neuen events, die die gleichen aggregate betreffen, auch mit einbeziehen
            // TODO: Dem Benutzer zuerst eine Liste zeigen, welche Events alle betroffen sind
            // TODO: Benutzer muss das dann bestštigen (NUR SYSADMINS)
        }

        private IEnumerable<EventDto> GetEventsInternal(ulong tenantId, User user, int skip, int take)
        {
            var result = new List<EventDto>();
            var tenant = _tenantRepository.Get(tenantId);
            int loaded;

            if (!user.IsSysadmin)
            {
                return new List<EventDto>(0);
            }

            do
            {
                var events = LocalEventStore.GetEvents(skip, take).ToList();
                loaded = events.Count;
                skip += take;

                result.AddRange(events
                    .Where(e => e.Date >= tenant.Created)
                    .Where(e => _eventTranslationService.CanUserSeeEvent(user, e))
                    .Where(e => _eventTranslationService.BelongsToTenant(tenantId, e))
                    .OrderByDescending(e => e.Version)
                    .Select(e => new EventDto(e, _userRepository, _eventTranslationService)));
            } while (loaded > 0 && result.Count < take);

            return result;
        }

        public class EventDto
        {
            public EventDto() { }

            public EventDto(IEvent @event, IUserRepository userRepository, IEventTranslationService eventTranslationService)
            {
                Id = @event.Version.ToJsonString();
                User = new UserDto(userRepository.GetEvenIfDeleted(@event.UserId));
                UserId = @event.UserId;
                Type = eventTranslationService.GetHeader(@event);
                Date = @event.Date.ToString("s") + "Z";
                AggregateId = @event.AggregateId.ToJsonString();
                AggregateType = @event.AggregateType.Name;
                AggregateIdLong = @event.AggregateId;

                try
                {
                    Description = eventTranslationService.GetContent(@event);
                }
                catch (Exception e)
                {
                    Description = "(Error occured)";
                    var msg = string.Format("Error while generating event description for event {0}.", @event.Version);
                    _log.Error(msg, e);
                }
            }

            public string Id { get; set; }
            public UserDto User { get; set; }
            public string Type { get; set; }
            public string Description { get; set; }
            public string Date { get; set; }
            public string AggregateId { get; set; }
            public string AggregateType { get; set; }

            [JsonIgnore]
            public ulong UserId { get; set; }

            [JsonIgnore]
            public ulong AggregateIdLong { get; set; }

            [JsonIgnore]
            public ulong TenantId { get; set; }
        }

        public class UserDto
        {
            private static readonly MD5 _md5;

            static UserDto()
            {
                _md5 = MD5.Create();
            }

            public UserDto() { }

            public UserDto(User user)
            {
                Name = user.ToString();

                if (string.IsNullOrEmpty(user.EmailAddress))
                {
                    EmailHash = string.Empty;
                }
                else
                {
                    var bytes = Encoding.ASCII.GetBytes(user.EmailAddress);
                    var hash = _md5.ComputeHash(bytes);
                    EmailHash = string.Join(string.Empty, hash.Select(h => h.ToString("x2"))).ToLowerInvariant();
                }
            }

            public string Name { get; set; }
            public string EmailHash { get; set; }
        }
    }
}