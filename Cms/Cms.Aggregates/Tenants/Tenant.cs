﻿using System;

namespace Cms.Aggregates.Tenants
{
    public class Tenant : AggregateBase
    {
        public string Name { get; set; }

        public DateTime Created { get; set; }
    }
}
