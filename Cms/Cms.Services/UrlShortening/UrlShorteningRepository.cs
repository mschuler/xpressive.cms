﻿using System;
using System.Collections.Generic;
using System.Linq;
using NPoco;

namespace Cms.Services.UrlShortening
{
    internal class UrlShorteningRepository : IUrlShorteningRepository
    {
        public IEnumerable<Tuple<string, string>> GetAll()
        {
            IList<ShortenedUrlDto> result;

            using (var database = new Database("CmsDatabaseConnection"))
            {
                result = database.Fetch<ShortenedUrlDto>("select LongUrl, ShortUrl from Url");
            }

            return result.Select(i => Tuple.Create(i.LongUrl, i.ShortUrl));
        }

        public void Add(string longUrl, string shortUrl)
        {
            using (var database = new Database("CmsDatabaseConnection"))
            {
                database.Insert(new ShortenedUrlDto
                {
                    LongUrl = longUrl,
                    ShortUrl = shortUrl
                });
            }
        }

        [TableName("Url")]
        public class ShortenedUrlDto
        {
            public string LongUrl { get; set; }
            public string ShortUrl { get; set; }
        }
    }
}