﻿using System;
using System.Linq;
using Cms.Aggregates.Pages;
using Cms.Aggregates.PageTemplates;
using Cms.Services.Contracts;

namespace Cms.Services
{
    internal class PageRenderingService : IPageRenderingService
    {
        private readonly IPageTemplateRepository _pageTemplateRepository;
        private readonly IContentHandlerFactory _contentHandlerFactory;
        private readonly IPageTemplateService _pageTemplateService;

        public PageRenderingService(IPageTemplateRepository pageTemplateRepository, IContentHandlerFactory contentHandlerFactory, IPageTemplateService pageTemplateService)
        {
            _pageTemplateRepository = pageTemplateRepository;
            _contentHandlerFactory = contentHandlerFactory;
            _pageTemplateService = pageTemplateService;
        }

        public string Render(Page page)
        {
            PageTemplate pageTemplate;

            if (!_pageTemplateRepository.TryGet(page.PageTemplateId, out pageTemplate))
            {
                return string.Empty;
            }

            var pageFields = _pageTemplateService.GetPageFields(pageTemplate);
            var content = _pageTemplateService.GetTemplate(pageTemplate);
            var pageContents = page.GetPageContent(pageTemplate, _pageTemplateService).ToList();

            foreach (var pageField in pageFields)
            {
                var pageContent = pageContents.SingleOrDefault(c => c.Name.Equals(pageField.Name, StringComparison.OrdinalIgnoreCase));
                var rendered = RenderContent(pageContent);
                content = _pageTemplateService.ReplaceField(content, pageField.Name, rendered);
            }

            return content;
        }

        private string RenderContent(PageContent pageContent)
        {
            if (pageContent != null)
            {
                var contentType = PageContentValueBase.GetType(pageContent.Value);
                var handler = _contentHandlerFactory.Get(contentType);
                return handler.Render(pageContent.Value);
            }

            return string.Empty;
        }
    }
}
