namespace Cms.Aggregates.Files.Events
{
    public class FileTrackingDisabled : EventBase<File> { }
}