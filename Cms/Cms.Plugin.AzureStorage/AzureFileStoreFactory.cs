﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.FileStore;
using Cms.SharedKernel;

namespace Cms.Plugin.AzureStorage
{
    internal class AzureFileStoreFactory : IFileStoreFactory
    {
        private static readonly IList<string> _settingKeys = new[]
        {
            "Account name",
            "Primary key"
        };

        private readonly IContentTypeService _contentTypeService;

        public AzureFileStoreFactory(IContentTypeService contentTypeService)
        {
            _contentTypeService = contentTypeService;
        }

        public Guid Id { get { return new Guid("7D6418DD-DFB3-473D-BCDA-BB91A0C0110A"); } }
        public string Name { get { return "Microsoft Azure Cloud Storage"; } }

        public IFileStore Create()
        {
            var settings = _settingKeys.ToDictionary(k => k, k => string.Empty);
            return new AzureFileStore(0, null, settings, null);
        }

        public IFileStore Create(ulong id, string name, IDictionary<string, string> settings)
        {
            var settingsNew = new Dictionary<string, string>();

            foreach (var setting in settings)
            {
                settingsNew.Add(setting.Key, setting.Value);
            }

            return new AzureFileStore(id, name, settingsNew, _contentTypeService);
        }

        public bool IsValidConfiguration(IDictionary<string, string> settings)
        {
            try
            {
                AzureAccountHelper.GetClient(settings);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}