using Cms.Aggregates.Security;

namespace Cms.Aggregates.Pages.Commands
{
    [RequiredPermission("page", PermissionLevel.CanChange)]
    public class DisablePage : CommandBase { }
}