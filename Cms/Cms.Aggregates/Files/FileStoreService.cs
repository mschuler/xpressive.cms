﻿using System;
using System.IO;

namespace Cms.Aggregates.Files
{
    internal class LocalFileStore : ILocalFileStore
    {
        public FileInfo Get(ulong fileId, ulong version)
        {
            var location = GetFileLocation(fileId, version);
            return new FileInfo(location);
        }

        internal static string GetFileLocation(ulong fileId, ulong fileVersion)
        {
            var rootDirectory = GetDirectoryLocation();
            var path = string.Format("files\\{0:D20}-{1:D20}.bin", fileId, fileVersion);
            var filePath = Path.Combine(rootDirectory, path);
            return filePath;
        }

        private static string GetDirectoryLocation()
        {
            var rootDirectory = AppDomain.CurrentDomain.BaseDirectory;
            return rootDirectory;
        }
    }
}
