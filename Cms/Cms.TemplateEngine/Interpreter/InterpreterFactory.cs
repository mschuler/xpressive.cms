using System;
using System.Collections.Generic;
using System.Linq;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;
using log4net;

namespace Cms.TemplateEngine.Interpreter
{
    public class InterpreterFactory
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(InterpreterFactory));
        private static readonly InterpreterFactory _instance = new InterpreterFactory();
        private readonly List<IInterpreter> _interpreters = new List<IInterpreter>();

        public InterpreterFactory()
        {
            _interpreters.Add(new DateTimeInterpreter());
            _interpreters.Add(new IifInterpreter());
            _interpreters.Add(new NumberFormatInterpreter());
            _interpreters.Add(new CollectionInterpreter());
            _interpreters.Add(new WhereListInterpreter());
            _interpreters.Add(new SortListInterpreter());
            _interpreters.Add(new UnionListInterpreter());
            _interpreters.Add(new DistinctListInterpreter());
            _interpreters.Add(new StringInterpreter());
            _interpreters.Add(new NullInterpreter());
            _interpreters.Add(new ListCountInterpreter());
            _interpreters.Add(new VariableInterpreter());
        }

        public static InterpreterFactory Instance
        {
            get { return _instance; }
        }

        public void Register(IInterpreter interpreter)
        {
            if (!_interpreters.Contains(interpreter))
            {
                _interpreters.Add(interpreter);
            }
        }

        public void Clear()
        {
            _interpreters.Clear();
        }

        public object Calculate(TreeItem tree)
        {
            try
            {
                var children = new List<TreeItem>();
                children.AddRange(tree.Children);

                foreach (var child in children)
                {
                    Calculate(child);
                }

                if (children.Count != tree.Children.Count)
                {
                    Calculate(tree);
                }
            }
            catch (Exception e)
            {
                _log.Error(string.Format("Error in engine: {0}.", e.Message), e);
                throw;
            }

            CalculateInternal(tree);

            var childResult = tree.Result;
            while (childResult is Expression)
            {
                var rootItem = tree.GetRootItem();

                _log.ErrorFormat("Why result is expression?? ('{0}')", rootItem.Value);

                var treeItem = Parser.Parse(((Expression)tree.Result).Name);

                var parent = tree.Parent;
                var index = parent.Children.IndexOf(tree);

                parent.Children.Remove(tree);
                parent.Children.Insert(index, treeItem);
                treeItem.Parent = parent;

                childResult = Calculate(parent);
                tree.Result = childResult;
            }

            object result;

            if (tree.ItemType == TreeItemType.Expression)
            {
                result = tree.Children.Count > 0 ? tree.Children[tree.Children.Count - 1].Result : null;
                tree.Result = result;
            }
            else
            {
                result = tree.Result;
            }

            return result;
        }

        private void CalculateInternal(TreeItem item)
        {
            if (item.Result != null)
            {
                return;
            }

            if (string.IsNullOrEmpty(item.Value))
            {
                return;
            }

            foreach (var interpreter in _interpreters.OrderByDescending(i => i.Priority))
            {
                interpreter.Calculate(item);

                if (item.Result != null)
                {
                    interpreter.Priority++;
                    break;
                }
            }
        }
    }
}
