using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Cms.Aggregates.Pages;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.Websites;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class RenderPageMessage
    {
        private readonly HttpRequestMessage _request;
        private readonly string _ipAddress;

        public RenderPageMessage(HttpRequestMessage request)
        {
            _request = request;
            _ipAddress = request.GetClientIpAddress();
            Cookies = new List<CookieHeaderValue>(request.Headers.GetCookies());
            StatusCode = HttpStatusCode.OK;
        }

        public Website Website { get; set; }
        public Domain Domain { get; set; }
        public Page Page { get; set; }
        public PageTemplate PageTemplate { get; set; }
        public string Content { get; set; }
        public string TrackingSessionId { get; set; }
        public string TrackingPersistedId { get; set; }
        public string IpAddress { get { return _ipAddress; } }
        public HttpResponseMessage Response { get; set; }
        public HttpRequestMessage Request { get { return _request; } }
        public HttpStatusCode StatusCode { get; set; }
        public string[] RouteParts { get; set; }
        public List<CookieHeaderValue> Cookies { get; private set; }
        public bool IsKnownBot { get; set; }

        public bool IsHtml
        {
            get
            {
                return PageTemplate != null && string.Equals(PageTemplate.ContentType, "text/html", StringComparison.OrdinalIgnoreCase);
            }
        }
    }
}