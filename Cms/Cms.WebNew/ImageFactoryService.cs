using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Cms.Aggregates;
using ImageProcessor;
using ImageProcessor.Imaging.Filters.Photo;
using ImageProcessor.Imaging.Formats;

namespace Cms.WebNew
{
    public static class ImageFactoryService
    {
        public static Stream GetImageStream(FileInfo fileInfo, ImageSettingsDto settings, Action<ImageFactory> changeSettings)
        {
            return GetStreamInternal(f => f.Load(fileInfo.FullName), settings, changeSettings);
        }

        private static Stream GetStreamInternal(Action<ImageFactory> loadFile, ImageSettingsDto settings, Action<ImageFactory> changeSettings)
        {
            var stream = new MemoryStream();

            using (var factory = new ImageFactory(preserveExifData: false))
            {
                loadFile(factory);

                changeSettings(factory);

                ApplySize(settings, factory);
                ApplyQuality(settings, factory);
                ApplyAlpha(settings, factory);
                ApplySaturation(settings, factory);
                ApplyRoundedCorners(settings, factory);
                ApplyBrightness(settings, factory);
                ApplyContrast(settings, factory);
                ApplyTint(settings, factory);
                ApplyFilter(settings, factory);

                factory.Save(stream);
            }

            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        private static void ApplyQuality(ImageSettingsDto settings, ImageFactory factory)
        {
            if (settings.Quality >= 100 || settings.Quality <= 0)
            {
                return;
            }

            factory.Quality(settings.Quality);
        }

        private static void ApplySize(ImageSettingsDto settings, ImageFactory factory)
        {
            if (settings.Size > 0)
            {
                double height = factory.Image.Height;
                double width = factory.Image.Width;
                var factor = width / height;
                var resize = false;

                if (height > settings.Size)
                {
                    height = settings.Size;
                    width = height * factor;
                    resize = true;
                }

                if (width > settings.Size)
                {
                    width = settings.Size;
                    height = width / factor;
                    resize = true;
                }

                if (resize)
                {
                    var w = (int)Math.Round(width);
                    var h = (int)Math.Round(height);
                    factory.Resize(new Size(w, h));
                }
            }
        }

        private static void ApplyAlpha(ImageSettingsDto settings, ImageFactory factory)
        {
            if (settings.Alpha >= 0 && settings.Alpha < 100)
            {
                factory.Alpha(settings.Alpha);
            }
        }

        private static void ApplySaturation(ImageSettingsDto settings, ImageFactory factory)
        {
            if (settings.Saturation >= -100 && settings.Saturation <= 100 && settings.Saturation != 0)
            {
                factory.Saturation(settings.Saturation);
            }
        }

        private static void ApplyRoundedCorners(ImageSettingsDto settings, ImageFactory factory)
        {
            if (settings.RoundedCorners > 0)
            {
                factory.Format(new PngFormat());
                factory.BackgroundColor(Color.Transparent);
                factory.RoundedCorners(settings.RoundedCorners);
            }
        }

        private static void ApplyBrightness(ImageSettingsDto settings, ImageFactory factory)
        {
            if (settings.Brightness >= -100 && settings.Brightness <= 100 && settings.Brightness != 0)
            {
                factory.Brightness(settings.Brightness);
            }
        }

        private static void ApplyContrast(ImageSettingsDto settings, ImageFactory factory)
        {
            if (settings.Contrast >= -100 && settings.Contrast <= 100 && settings.Contrast != 0)
            {
                factory.Contrast(settings.Contrast);
            }
        }

        private static void ApplyTint(ImageSettingsDto settings, ImageFactory factory)
        {
            if (string.IsNullOrEmpty(settings.Tint))
            {
                return;
            }

            settings.Tint = settings.Tint.Replace(" ", string.Empty);
            KnownColor knownColor;
            if (Enum.TryParse(settings.Tint, out knownColor))
            {
                var color = Color.FromKnownColor(knownColor);
                factory.Tint(color);
            }
        }

        private static void ApplyFilter(ImageSettingsDto settings, ImageFactory factory)
        {
            if (string.IsNullOrEmpty(settings.Filter))
            {
                return;
            }

            var filters = new Dictionary<string, Func<IMatrixFilter>>(StringComparer.OrdinalIgnoreCase)
            {
                { "BlackWhite", () => MatrixFilters.BlackWhite },
                { "Comic", () => MatrixFilters.Comic },
                { "Gotham", () => MatrixFilters.Gotham },
                { "GreyScale", () => MatrixFilters.GreyScale },
                { "HiSatch", () => MatrixFilters.HiSatch },
                { "Invert", () => MatrixFilters.Invert },
                { "LoSatch", () => MatrixFilters.LoSatch },
                { "Lomograph", () => MatrixFilters.Lomograph },
                { "Polaroid", () => MatrixFilters.Polaroid },
                { "Sepia", () => MatrixFilters.Sepia },
            };
            Func<IMatrixFilter> filter;
            if (filters.TryGetValue(settings.Filter, out filter))
            {
                factory.Filter(filter());
            }
        }
    }
}