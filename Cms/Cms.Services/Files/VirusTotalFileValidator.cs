﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.Files;
using Cms.Aggregates.Files.Commands;
using Cms.Aggregates.Files.Events;
using Cms.Aggregates.FileStore;
using Cms.Aggregates.Security;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using log4net;

namespace Cms.Services.Files
{
    internal class VirusTotalFileValidator : CommandHandlerBase
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(VirusTotalFileValidator));
        private static readonly ConcurrentQueue<FileToCheck> _filesToScan = new ConcurrentQueue<FileToCheck>();
        private static readonly SingleTaskRunner _taskRunner = new SingleTaskRunner();

        private readonly IFileStoreRepository _fileStoreRepository;
        private readonly IRepository<FileDirectory> _fileDirectoryRepository;
        private readonly IFileRepository _fileRepository;
        private readonly IVirusTotalService _virusTotalService;

        public VirusTotalFileValidator(IFileStoreRepository fileStoreRepository, IRepository<FileDirectory> fileDirectoryRepository, IFileRepository fileRepository, IVirusTotalService virusTotalService)
        {
            _fileStoreRepository = fileStoreRepository;
            _fileDirectoryRepository = fileDirectoryRepository;
            _fileRepository = fileRepository;
            _virusTotalService = virusTotalService;
        }

        public void When(CreateFile command, Action<IEvent> eventHandler)
        {
            Enqueue(command.FileDirectoryId, command.AggregateId, command.FileVersion, command.Name, eventHandler);
        }

        public void When(CreateFileVersion command, Action<IEvent> eventHandler)
        {
            var file = _fileRepository.Get(command.AggregateId);
            var directoryId = file.FileDirectoryId;
            Enqueue(directoryId, command.AggregateId, command.FileVersion, file.Name, eventHandler);
        }

        private void Enqueue(ulong directoryId, ulong fileId, ulong fileVersion, string fileName, Action<IEvent> eventHandler)
        {
            if (!_virusTotalService.IsValidApiKey())
            {
                _log.Warn("Unable to check file with VirusTotal because no valid ApiKey provided.");
                return;
            }

            var directory = _fileDirectoryRepository.Get(directoryId);
            var store = _fileStoreRepository.Get(directory.FileStoreId);

            if (!store.IsLocalFilesystem)
            {
                return;
            }

            var filePath = store.GetLocalFileLocation(fileId, fileVersion);
            _filesToScan.Enqueue(new FileToCheck
            {
                FileId = fileId,
                FileName = fileName,
                FileInfo = new FileInfo(filePath),
                FlagAsMalicious = () =>
                {
                    eventHandler(new FileMarkedAsMalicious
                    {
                        AggregateId = fileId,
                        FileVersion = fileVersion,
                        UserId = User.System.Id
                    });
                }
            });

            _taskRunner.StartIfNotAlreadyRunning(Validate);
        }

        private async Task Validate()
        {
            FileToCheck file;
            while (_filesToScan.TryDequeue(out file))
            {
                try
                {
                    var isFinish = await ValidateFile(file);
                    if (!isFinish)
                    {
                        _filesToScan.Enqueue(file);
                    }
                }
                catch (Exception e)
                {
                    var msg = $"Unable to get file report for {file.FileName} ({file.FileId}).";
                    _log.Warn(msg, e);
                    _filesToScan.Enqueue(file);
                }
            }
        }

        private async Task<bool> ValidateFile(FileToCheck file)
        {
            if (!System.IO.File.Exists(file.FileInfo.FullName))
            {
                await Task.Delay(50);
                return false;
            }

            if (file.FileInfo.Length >= 23000000)
            {
                _log.Warn($"Unable to get file report for {file.FileName} ({file.FileId}) because its larger than 23MB");
                return true;
            }

            var fileReport = await (string.IsNullOrEmpty(file.ScanId)
                ? _virusTotalService.GetReportAsync(file.FileInfo)
                : _virusTotalService.GetReportAsync(file.ScanId));

            if (fileReport.Success)
            {
                if (fileReport.IsMalicious)
                {
                    file.FlagAsMalicious();
                }

                return true;
            }

            var enqueueResult = await _virusTotalService.EnqueueAsync(file.FileInfo, file.FileName);

            if (enqueueResult.Success)
            {
                file.ScanId = enqueueResult.ScanId;
            }

            return false;
        }

        private class FileToCheck
        {
            public ulong FileId { get; set; }
            public string FileName { get; set; }
            public FileInfo FileInfo { get; set; }
            public string ScanId { get; set; }
            public Action FlagAsMalicious { get; set; }
        }
    }
}
