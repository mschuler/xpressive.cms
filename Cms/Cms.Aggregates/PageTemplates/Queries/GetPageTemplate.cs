﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.PageTemplates.Queries
{
    [RequiredPermission("pagetemplate", PermissionLevel.CanSee)]
    public class GetPageTemplate : QueryBase
    {
        public ulong Id { get; set; }
    }
}