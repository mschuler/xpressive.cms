﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using Cms.Services.Contracts;

namespace Cms.Services.RecurrentJobs
{
    internal class MoveLogfilesToZipfile : IRecurrentJob
    {
        public string CronTab { get { return "0 0 4 20 * ?"; } }

        public void Execute()
        {
            var directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "logs");
            var affectedMonth = DateTime.Today.AddMonths(-1);
            var pattern = string.Format("logfile.{0}-{1:D2}", affectedMonth.Year, affectedMonth.Month);
            var zipfile = Path.Combine(directory, pattern + ".zip");
            var files = Directory
                .GetFiles(directory, "*.txt", SearchOption.TopDirectoryOnly)
                .Select(f => Tuple.Create(f, Path.GetFileName(f)))
                .Where(t => t.Item2 != null)
                .Where(t => t.Item2.StartsWith(pattern))
                .ToList();

            if (!files.Any())
            {
                return;
            }

            using (var zipStream = new FileStream(zipfile, FileMode.CreateNew))
            {
                using (var archive = new ZipArchive(zipStream, ZipArchiveMode.Create, false, Encoding.UTF8))
                {

                    foreach (var file in files)
                    {
                        var data = File.ReadAllBytes(file.Item1);
                        var zipEntry = archive.CreateEntry(file.Item2, CompressionLevel.Optimal);
                        using (var entryStream = zipEntry.Open())
                        {
                            entryStream.Write(data, 0, data.Length);
                        }
                    }
                }
            }

            foreach (var file in files)
            {
                File.Delete(file.Item1);
            }
        }
    }
}
