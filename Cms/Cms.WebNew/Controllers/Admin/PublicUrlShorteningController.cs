using System.Net;
using System.Web.Http;
using Cms.Aggregates.Websites;
using Cms.Services.Contracts;

namespace Cms.WebNew.Controllers.Admin
{
    public class PublicUrlShorteningController : ApiControllerAuthorizedByApiKey
    {
        private readonly IWebsiteRepository _websiteRepository;
        private readonly IUrlShorteningServiceProvider _urlShorteningServiceProvider;

        public PublicUrlShorteningController(IApiKeyService apiKeyService, IUrlShorteningServiceProvider urlShorteningServiceProvider, IWebsiteRepository websiteRepository)
            : base(apiKeyService)
        {
            _urlShorteningServiceProvider = urlShorteningServiceProvider;
            _websiteRepository = websiteRepository;
        }

        [HttpGet, Route("api/public/url/shorten")]
        public UrlShorteningResultDto Shorten([FromUri]string longUrl)
        {
            if (!IsAuthorized())
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            var host = Request.RequestUri.Host;
            var website = _websiteRepository.GetByHost(host);

            if (website == null)
            {
                return new UrlShorteningResultDto { ShortUrl = longUrl };
            }

            var service = _urlShorteningServiceProvider.GetSelectedService(website.TenantId);

            if (service == null)
            {
                return new UrlShorteningResultDto { ShortUrl = longUrl };
            }

            var shortUrl = service.ShortenUrl(longUrl);
            return new UrlShorteningResultDto { ShortUrl = shortUrl };
        }

        public class UrlShorteningResultDto
        {
            public string ShortUrl { get; set; }
        }
    }
}