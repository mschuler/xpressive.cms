﻿using System;
using Cms.Aggregates.Calendars;
using Cms.Aggregates.Calendars.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Calendars
{
    internal class AppointmentRemovedEventTranslator : IEventTranslator
    {
        private readonly ICalendarRepository _repository;

        public AppointmentRemovedEventTranslator(ICalendarRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(AppointmentRemoved);
        }

        public string GetHeader(IEvent @event)
        {
            return "Termin gelöscht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (AppointmentRemoved)@event;
            return string.Format("Ein Termin wurde gelöscht.");
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var calendar = _repository.GetEvenIfDeleted(@event.AggregateId);
            return calendar.TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "appointment";
        }
    }
}