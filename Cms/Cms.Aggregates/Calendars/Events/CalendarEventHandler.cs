using System;
using System.Linq;
using Cms.Aggregates.Tenants.Events;

namespace Cms.Aggregates.Calendars.Events
{
    public class CalendarEventHandler : EventHandlerBase
    {
        private readonly ICalendarRepository _repository;

        public CalendarEventHandler(ICalendarRepository repository)
        {
            _repository = repository;
        }

        public void When(TenantCreated @event)
        {
            var calendar = new Calendar
            {
                Id = @event.AggregateId,
                TenantId = @event.AggregateId,
            };
            _repository.Add(calendar);
        }

        public void When(TenantDeleted @event)
        {
            _repository.DeleteAllByTenantId(@event.AggregateId);
        }

        public void When(CategoryAdded @event)
        {
            var calendar = _repository.Get(@event.AggregateId);
            var category = new Category { Name = @event.Name };
            calendar.Add(category);
        }

        public void When(CategoryNameChanged @event)
        {
            var calendar = _repository.Get(@event.AggregateId);
            var category = calendar.Categories.Single(c => c.Name.Equals(@event.OldName, StringComparison.OrdinalIgnoreCase));
            category.Name = @event.NewName;
        }

        public void When(CategoryRemoved @event)
        {
            var calendar = _repository.Get(@event.AggregateId);
            var category = calendar.Categories.Single(c => c.Name.Equals(@event.Name, StringComparison.OrdinalIgnoreCase));
            calendar.Remove(category);

            foreach (var appointment in calendar.Appointments)
            {
                appointment.Remove(category);
            }
        }

        public void When(AppointmentAdded @event)
        {
            var calendar = _repository.Get(@event.AggregateId);
            var appointment = new Appointment
            {
                Id = @event.AppointmentId,
                StartEnd = @event.StartEnd,
                Title = @event.Title,
                IsHighlight = @event.IsHighlight,
                Location = @event.Location,
                Link = @event.Link,
                Recurrence = @event.Recurrence,
            };

            if (@event.Categories != null)
            {
                foreach (var categoryName in @event.Categories)
                {
                    var category = calendar.Categories.SingleOrDefault(c => c.Name.Equals(categoryName, StringComparison.Ordinal));
                    if (category != null)
                    {
                        appointment.Add(category);
                    }
                }
            }

            calendar.Add(appointment);
        }

        public void When(AppointmentChanged @event)
        {
            var calendar = _repository.Get(@event.AggregateId);
            var appointment = calendar.Appointments.Single(a => a.Id.Equals(@event.AppointmentId));
            appointment.StartEnd = @event.StartEnd;
            appointment.Title = @event.Title;
            appointment.IsHighlight = @event.IsHighlight;
            appointment.Location = @event.Location;
            appointment.Link = @event.Link;

            if (@event.Recurrence != null)
            {
                appointment.Recurrence = @event.Recurrence;
            }

            foreach (var category in appointment.Categories.ToList())
            {
                appointment.Remove(category);
            }

            if (@event.Categories != null)
            {
                foreach (var categoryName in @event.Categories)
                {
                    var category = calendar.Categories.SingleOrDefault(c => c.Name.Equals(categoryName, StringComparison.Ordinal));
                    if (category != null)
                    {
                        appointment.Add(category);
                    }
                }
            }
        }

        public void When(AppointmentRemoved @event)
        {
            var calendar = _repository.Get(@event.AggregateId);
            var appointment = calendar.Appointments.Single(a => a.Id.Equals(@event.AppointmentId));
            calendar.Remove(appointment);
        }
    }
}