﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.FileDirectories;
using Cms.SharedKernel;

namespace Cms.Aggregates.Files.Queries
{
    internal class FileQueryHandler : QueryHandlerBase
    {
        private readonly IFileRepository _repository;
        private readonly IRepository<FileDirectory> _fileDirectoryRepository;
        private readonly IContentTypeService _contentTypeService;

        public FileQueryHandler(IFileRepository repository, IRepository<FileDirectory> fileDirectoryRepository, IContentTypeService contentTypeService)
        {
            _repository = repository;
            _fileDirectoryRepository = fileDirectoryRepository;
            _contentTypeService = contentTypeService;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetAllFiles),
                typeof (GetFilesWithoutDirectory),
                typeof (GetFile),
                typeof (GetFilesBySearchPattern)
            };
        }

        public IEnumerable<object> When(GetAllFiles query)
        {
            return _repository
                .GetByDirectory(query.DirectoryId)
                .OrderBy(f => f.Name, StringComparer.OrdinalIgnoreCase)
                .Select(GetDto);
        }

        public IEnumerable<object> When(GetFilesWithoutDirectory query)
        {
            return When(new GetAllFiles
            {
                DirectoryId = 0,
                UserId = query.UserId,
                TenantId = query.TenantId
            });
        }

        public IEnumerable<object> When(GetFile query)
        {
            yield return GetDto(_repository.Get(query.FileId));
        }

        public IEnumerable<object> When(GetFilesBySearchPattern query)
        {
            return _repository
                .GetAll()
                .Where(f => IsVisibleForTenant(f, query.TenantId))
                .Where(query.IsMatch)
                .OrderBy(f => f.Name, StringComparer.OrdinalIgnoreCase)
                .Select(GetDto);
        }

        private object GetDto(File file)
        {
            var isTextFile = _contentTypeService.IsTextFile(file.GetExtension());
            return file.ToDto(isTextFile);
        }

        private bool IsVisibleForTenant(File file, ulong tenantId)
        {
            FileDirectory directory;
            if (!_fileDirectoryRepository.TryGet(file.FileDirectoryId, out directory))
            {
                return false;
            }

            return IsVisibleForTenant(directory, tenantId);
        }

        private bool IsVisibleForTenant(FileDirectory directory, ulong tenantId)
        {
            if (directory == null)
            {
                return false;
            }

            if (directory.TenantId == tenantId)
            {
                return true;
            }

            if (directory.SharedForTenants.Contains(tenantId))
            {
                return true;
            }

            return IsVisibleForTenant(directory.Parent, tenantId);
        }
    }
}
