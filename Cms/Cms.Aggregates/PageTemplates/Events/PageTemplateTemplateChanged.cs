﻿namespace Cms.Aggregates.PageTemplates.Events
{
    public class PageTemplateTemplateChanged : EventBase<PageTemplate>
    {
        public string Template { get; set; }
    }
}