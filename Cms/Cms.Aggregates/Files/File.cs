﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Cms.Aggregates.Files
{
    public class File : AggregateBase
    {
        private static readonly List<string> _byteUnits = new List<string>
        {
            "byte", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"
        };

        private readonly object _versionsLock = new object();
        private readonly IList<FileVersion> _versions;

        public File()
        {
            _versions = new List<FileVersion>();
        }

        public ulong FileDirectoryId { get; set; }
        public ulong TenantId { get; set; }
        public ulong FileStoreId { get; set; }
        public int PublicId { get; set; }
        public string Name { get; set; }
        public bool IsTracked { get; set; }
        public bool IsMalicious { get; set; }

        public string Fullname
        {
            get { return string.Format("{0} (Nr. {1})", Name, PublicId); }
        }

        public IEnumerable<FileVersion> Versions
        {
            get
            {
                lock (_versionsLock)
                {
                    return _versions.ToList();
                }
            }
        }

        public void Add(FileVersion version)
        {
            lock (_versionsLock)
            {
                _versions.Add(version);
            }
        }

        public string GetDateString(FileVersion version = null)
        {
            if (version == null)
            {
                version = _versions.Last();
            }

            var d = version.Date.ToLocalTime();
            return string.Format("{0} {1}", d.ToShortDateString(), d.ToShortTimeString());
        }

        public string GetFileSizeLabel(FileVersion version = null)
        {
            if (version == null)
            {
                version = _versions.Last();
            }

            decimal dSize = version.Size;
            var unit = _byteUnits[0];

            foreach (var t in _byteUnits)
            {
                if (dSize > 1000)
                {
                    dSize = dSize / 1000;
                }
                else
                {
                    unit = t;
                    break;
                }
            }

            var sizeLabel = string.Format(CultureInfo.InvariantCulture, "{0:f2}", dSize).Trim('.', '0', ',');
            var result = string.Format(CultureInfo.InvariantCulture, "{0} {1}", sizeLabel, unit);

            return result;
        }

        public object ToDto(bool isTextFile = false)
        {
            return new
            {
                Id = Id.ToJsonString(),
                PublicId,
                DirectoryId = FileDirectoryId.ToJsonString(),
                FileStoreId = FileStoreId.ToJsonString(),
                Name,
                IsTracked,
                IsMalicious,
                Fullname,
                Date = GetDateString(),
                Size = GetFileSizeLabel(),
                IsTextFile = isTextFile,
                DownloadUrl = this.ToDownloadUrl(withTracking: false),
                PreviewUrl = this.ToPreviewUrl(),
            };
        }
    }
}
