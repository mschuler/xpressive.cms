using System;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.PageTemplates.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.PageTemplates
{
    internal class PageTemplateDeletedEventTranslator : IEventTranslator
    {
        private readonly IPageTemplateRepository _repository;

        public PageTemplateDeletedEventTranslator(IPageTemplateRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageTemplateDeleted);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seitenvorlage gel�scht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageTemplateDeleted)@event;
            return string.Format("Die Seitenvorlage <strong>{0}</strong> wurde gel�scht.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "pagetemplate";
        }
    }
}