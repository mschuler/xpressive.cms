﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Cms.SharedKernel;

namespace Cms.Aggregates
{
    internal abstract class RepositoryBase<T> : IRepository<T> where T : IAggregate
    {
        private static readonly ConcurrentDictionary<ulong, T> _cache = new ConcurrentDictionary<ulong, T>();
        private static readonly ConcurrentDictionary<ulong, T> _deleted = new ConcurrentDictionary<ulong, T>();

        public void AssertIdIsNotUsed(ulong aggregateId)
        {
            if (_cache.ContainsKey(aggregateId) || _deleted.ContainsKey(aggregateId))
            {
                throw new InvalidOperationException("There is already an aggregate with this id.").WithGuid();
            }
        }

        public virtual T Get(ulong aggregateId)
        {
            T aggregate;
            if (!TryGet(aggregateId, out aggregate))
            {
                throw new AggregateNotFoundException(aggregateId).WithGuid();
            }
            return aggregate;
        }

        public virtual T GetEvenIfDeleted(ulong aggregateId)
        {
            T aggregate;
            if (_cache.TryGetValue(aggregateId, out aggregate) || _deleted.TryGetValue(aggregateId, out aggregate))
            {
                return aggregate;
            }
            throw new AggregateNotFoundException(aggregateId).WithGuid();
        }

        public virtual bool TryGet(ulong aggregateId, out T t)
        {
            return _cache.TryGetValue(aggregateId, out t);
        }

        public virtual bool TryGetEvenIfDeleted(ulong aggregateId, out T t)
        {
            return _cache.TryGetValue(aggregateId, out t) || _deleted.TryGetValue(aggregateId, out t);
        }

        public IEnumerable<T> GetAll()
        {
            var copy = new List<T>(_cache.Count);
            copy.AddRange(_cache.Values);
            return copy;
        }

        public virtual void Add(T aggregate)
        {
            _cache.TryAdd(aggregate.Id, aggregate);
        }

        public virtual void Delete(ulong aggregateId)
        {
            T aggregate;
            if (_cache.TryRemove(aggregateId, out aggregate))
            {
                _deleted.TryAdd(aggregateId, aggregate);
            }
        }

        public abstract void DeleteAllByTenantId(ulong tenantId);

        internal void Clear()
        {
            _cache.Clear();
            _deleted.Clear();
        }
    }
}