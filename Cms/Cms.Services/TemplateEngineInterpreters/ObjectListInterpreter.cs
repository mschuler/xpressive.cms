using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.Websites;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal sealed class ObjectListInterpreter : IInterpreter
    {
        private readonly IDynamicObjectRepository _repository;
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;

        public ObjectListInterpreter(IDynamicObjectRepository repository, IDynamicObjectDefinitionRepository definitionRepository)
        {
            _repository = repository;
            _definitionRepository = definitionRepository;
        }

        public int Priority { get; set; }

        public string Name { get { return "objects"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function &&
                tree.Value.Equals("objects", StringComparison.OrdinalIgnoreCase) &&
                tree.Children != null && tree.Children.Count == 1 &&
                tree.Children[0].ParameterType == ParameterType.Integer)
            {
                var publicDefinitionId = (int)tree.Children[0].Result;
                var definition = _definitionRepository.GetAll().SingleOrDefault(d => d.PublicId == publicDefinitionId);

                if (definition == null)
                {
                    return;
                }

                var website = TemplateStateMap.Get<Website>("CurrentWebsite");
                var tenantId = website != null ? website.TenantId : definition.TenantId;
                var objects = _repository.GetAll(tenantId, definition.Id).ToList();
                tree.Result = objects;
            }

            if (tree.ItemType == TreeItemType.Function &&
                tree.Value.Equals("template", StringComparison.OrdinalIgnoreCase) &&
                tree.Children != null && tree.Children.Count == 1 &&
                tree.LeftItem != null && tree.LeftItem.Result != null)
            {
                var collection = tree.LeftItem.Result as IEnumerable<DynamicObject>;
                var o = tree.LeftItem.Result as DynamicObject;
                var objects = new List<DynamicObject>();

                if (collection == null && o == null)
                {
                    return;
                }

                if (collection == null)
                {
                    objects.Add(o);
                }
                else
                {
                    objects.AddRange(collection);
                }

                var result = new StringBuilder();
                var engine = new Engine();

                if (tree.Children[0].Result is int)
                {
                    var templateId = (int)tree.Children[0].Result;

                    foreach (var dynamicObject in objects)
                    {
                        var definition = _definitionRepository.Get(dynamicObject.DefinitionId);
                        var template = definition.Templates.SingleOrDefault(t => t.PublicId == templateId);

                        if (template == null)
                        {
                            continue;
                        }

                        result.Append(RenderTemplate(engine, dynamicObject, template.Template));
                    }
                }

                if (tree.Children[0].Result is string)
                {
                    var template = (string)tree.Children[0].Result;

                    if (string.IsNullOrEmpty(template))
                    {
                        return;
                    }

                    foreach (var dynamicObject in objects)
                    {
                        result.Append(RenderTemplate(engine, dynamicObject, template));
                    }
                }

                tree.Result = result.ToString();
            }
        }

        private string RenderTemplate(Engine engine, DynamicObject dynamicObject, string template)
        {
            using (TemplateStateMap.Set(TemplateStateMap.CurrentObjectKey, dynamicObject))
            {
                string previousResult;
                var result = template;

                do
                {
                    previousResult = result;
                    result = engine.Generate(previousResult);
                } while (!string.Equals(result, previousResult, StringComparison.Ordinal));

                return result;
            }
        }
    }
}