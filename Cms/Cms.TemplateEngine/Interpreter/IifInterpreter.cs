﻿using System;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    public sealed class IifInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "iif"; } }

        public void Calculate(TreeItem treeItem)
        {
            if (treeItem.ItemType == TreeItemType.Function &&
                treeItem.Value.Equals("iif", StringComparison.OrdinalIgnoreCase) && treeItem.Children != null)
            {
                if (treeItem.Children.Count == 4)
                {
                    var expected = treeItem.Children[0].Result;
                    var value = treeItem.Children[1].Result;
                    var ifTrue = treeItem.Children[2].Result;
                    var ifFalse = treeItem.Children[3].Result;

                    if (expected == null && value == null)
                    {
                        treeItem.Result = ifTrue;
                        return;
                    }
                    if (expected == null || value == null)
                    {
                        treeItem.Result = ifFalse;
                        return;
                    }
                    if (expected.GetType() == value.GetType())
                    {
                        var expectedStringType = expected as string;
                        var valueStringType = value as string;

                        if (expectedStringType != null && valueStringType != null)
                        {
                            treeItem.Result = string.Equals(
                                expectedStringType,
                                valueStringType,
                                StringComparison.OrdinalIgnoreCase)
                                ? ifTrue
                                : ifFalse;
                            return;
                        }
                    }

                    var expectedString = expected.ToString();
                    var valueString = value.ToString();
                    treeItem.Result = string.Equals(expectedString, valueString, StringComparison.OrdinalIgnoreCase)
                        ? ifTrue
                        : ifFalse;
                }
                else if (treeItem.Children.Count == 5 && treeItem.Children[1].ParameterType == ParameterType.String)
                {
                    var expected = treeItem.Children[0].Result;
                    var op = (string)treeItem.Children[1].Result;
                    var value = treeItem.Children[2].Result;
                    var ifTrue = treeItem.Children[3].Result;
                    var ifFalse = treeItem.Children[4].Result;

                    treeItem.Result = OperatorHelper.IsMatch(expected, op, value) ? ifTrue : ifFalse;
                }
            }
        }
    }
}