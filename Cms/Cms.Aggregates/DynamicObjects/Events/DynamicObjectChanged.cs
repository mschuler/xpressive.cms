using System;
using System.Collections.Generic;

namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectChanged : EventBase<DynamicObject>
    {
        private readonly Dictionary<string, string> _values;

        public DynamicObjectChanged()
        {
            _values = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        }

        public Dictionary<string, string> Values
        {
            get { return _values; }
            set
            {
                _values.Clear();
                foreach (var pair in value)
                {
                    _values.Add(pair.Key, pair.Value);
                }
            }
        }
    }
}