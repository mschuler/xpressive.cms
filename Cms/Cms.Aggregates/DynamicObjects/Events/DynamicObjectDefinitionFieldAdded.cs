﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDefinitionFieldAdded : EventBase<DynamicObjectDefinition>
    {
        public string Name { get; set; }
        public string ContentType { get; set; }
        public string ContentTypeOptions { get; set; }
    }
}