﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.FileDirectories.Queries
{
    [RequiredPermission("file", PermissionLevel.CanSee)]
    public class GetSharedFileDirectories : QueryBase { }
}