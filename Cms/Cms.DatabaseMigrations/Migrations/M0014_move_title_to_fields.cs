﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0014_move_title_to_fields : DatabaseMigrationBase
    {
        public M0014_move_title_to_fields() : base(14) { }

        public override void Execute(Database database)
        {
            var events = database.Query<EventDto>().Where(e => e.Event == "DynamicObjectCreated").ToList();

            foreach (var dto in events)
            {
                var eventOld = JsonConvert.DeserializeObject<DynamicObjectCreatedOld>(dto.Data);

                var eventNew = new DynamicObjectCreatedNew
                {
                    DefinitionId = eventOld.DefinitionId,
                    PublicId = eventOld.PublicId,
                    Date = eventOld.Date,
                };

                eventNew.Values.Add("Title", eventOld.Title);

                foreach (var value in eventOld.Values)
                {
                    eventNew.Values.Add(value.Key, value.Value);
                }

                var data = JsonConvert.SerializeObject(eventNew);

                dto.Data = data;

                database.Update(dto);
            }
        }

        [TableName("Event")]
        [PrimaryKey("Version", AutoIncrement = false)]
        private class EventDto
        {
            public long Transaction { get; set; }
            public long Version { get; set; }
            public string AggregateType { get; set; }
            public long AggregateId { get; set; }
            public string Event { get; set; }
            public long UserId { get; set; }
            public string Data { get; set; }
            public bool IsDeleted { get; set; }
        }

        private class DynamicObjectCreatedOld
        {
            public DynamicObjectCreatedOld()
            {
                Values = new Dictionary<string, string>();
            }

            public int PublicId { get; set; }
            public ulong DefinitionId { get; set; }
            public string Title { get; set; }
            public DateTime Date { get; set; }
            public Dictionary<string, string> Values { get; set; }
        }

        private class DynamicObjectCreatedNew
        {
            public DynamicObjectCreatedNew()
            {
                Values = new Dictionary<string, string>();
            }

            public int PublicId { get; set; }
            public ulong DefinitionId { get; set; }
            public DateTime Date { get; set; }
            public Dictionary<string, string> Values { get; set; }
        }
    }
}
