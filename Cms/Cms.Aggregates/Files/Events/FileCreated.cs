namespace Cms.Aggregates.Files.Events
{
    public class FileCreated : EventBase<File>
    {
        public ulong FileDirectoryId { get; set; }
        public int PublicId { get; set; }
        public string Name { get; set; }
        public ulong FileVersion { get; set; }
        public string Hash { get; set; }
        public ulong Size { get; set; }
    }
}