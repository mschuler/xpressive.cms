﻿namespace Cms.Aggregates.Pages
{
    public class PageContentFormValue : PageContentValueBase
    {
        public string FormId { get; set; }
        public string TemplateId { get; set; }
    }
}