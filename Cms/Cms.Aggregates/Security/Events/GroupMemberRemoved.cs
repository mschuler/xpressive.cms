namespace Cms.Aggregates.Security.Events
{
    public class GroupMemberRemoved : EventBase<Group>
    {
        public ulong MemberId { get; set; }
    }
}