using System;
using System.Collections.Generic;
using Cms.Services.Contracts;
using log4net;
using Quartz;
using Quartz.Spi;

namespace Cms.Services.RecurrentJobs
{
    internal class JobFactory : IJobFactory
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(JobFactory));
        private readonly IDictionary<Guid, IRecurrentJob> _recurrentJobs;

        public JobFactory(IDictionary<Guid, IRecurrentJob> jobs)
        {
            _recurrentJobs = jobs;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            var job = FindJob(bundle.JobDetail.Key.Name);
            return new JobWrapper(job);
        }

        public void ReturnJob(IJob job) { }

        private IRecurrentJob FindJob(string jobName)
        {
            Guid id;
            IRecurrentJob job;

            if (!Guid.TryParse(jobName, out id) || !_recurrentJobs.TryGetValue(id, out job))
            {
                _log.ErrorFormat("Unable to find recurrent job with id={0}.", jobName);
                return null;
            }

            return job;
        }
    }
}