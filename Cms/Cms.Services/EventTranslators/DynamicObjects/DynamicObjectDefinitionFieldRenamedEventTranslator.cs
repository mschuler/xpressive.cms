using System;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectDefinitionFieldRenamedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectDefinitionFieldRenamedEventTranslator(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof (DynamicObjectDefinitionFieldRenamed);
        }

        public string GetHeader(IEvent @event)
        {
            return "Objektdefinitionsfeld umbenannt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectDefinitionFieldRenamed)@event;
            var definition = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format(
                "Das Feld <strong>{2}</strong> der Objektdefinition <strong>{0} (Nr. {1})</strong> wurde in <strong>{3}</strong> umbenannt.",
                definition.Name,
                definition.PublicId,
                e.OldName,
                e.NewName);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objectdefinition";
        }
    }
}