﻿using System.Net.Http;
using Cms.EventSourcing.Contracts.Commands;

namespace Cms.WebNew.Controllers
{
    public interface ICommandQueue
    {
        void Put(ICommand command, HttpRequestMessage request = null);
    }
}