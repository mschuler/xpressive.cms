using System;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Forms
{
    internal class FormEntryCreatedEventTranslator : IEventTranslator
    {
        private readonly IFormRepository _repository;

        public FormEntryCreatedEventTranslator(IFormRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FormEntryCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Formulareintrag erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FormEntryCreated)@event;
            var form = _repository.GetEvenIfDeleted(e.AggregateId);
            var name = string.Format("{0} (Nr. {1})", form.Name, form.PublicId);
            return string.Format("F�r das Formular <strong>{0}</strong> wurde ein neuer Eintrag erstellt.", name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "form";
        }
    }
}