﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.FileStore.Commands
{
    [RequiredPermission("sysadmin", PermissionLevel.CanEverything)]
    public class CreateFileStore : CommandBase
    {
        [Required]
        public Guid FileStoreFactoryId { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }

        [Required]
        public IDictionary<string, string> Settings { get; set; }
    }
}