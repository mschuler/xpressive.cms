using System.IO;

namespace Cms.Aggregates.Files.Commands
{
    // http://techmikael.blogspot.ch/2009/07/removing-exif-data-continued.html
    internal static class JpegPatcher
    {
        public static void PatchAwayExif(Stream inStream, Stream outStream)
        {
            var jpegHeader = new byte[2];
            jpegHeader[0] = (byte)inStream.ReadByte();
            jpegHeader[1] = (byte)inStream.ReadByte();

            if (jpegHeader[0] == 0xff && jpegHeader[1] == 0xd8) //check if it's a jpeg file
            {
                SkipAppHeaderSection(inStream);
            }

            outStream.WriteByte(0xff);
            outStream.WriteByte(0xd8);

            int readCount;
            var readBuffer = new byte[4096];

            while ((readCount = inStream.Read(readBuffer, 0, readBuffer.Length)) > 0)
            {
                outStream.Write(readBuffer, 0, readCount);
            }
        }

        private static void SkipAppHeaderSection(Stream stream)
        {
            var header = new byte[2];
            header[0] = (byte)stream.ReadByte();
            header[1] = (byte)stream.ReadByte();

            while (header[0] == 0xff && (header[1] >= 0xe0 && header[1] <= 0xef))
            {
                int exifLength = stream.ReadByte();
                exifLength = exifLength << 8;
                exifLength |= stream.ReadByte();

                for (int i = 0; i < exifLength - 2; i++)
                {
                    stream.ReadByte();
                }
                header[0] = (byte)stream.ReadByte();
                header[1] = (byte)stream.ReadByte();
            }
            stream.Position -= 2; //skip back two bytes
        }
    }
}