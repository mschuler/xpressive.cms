﻿namespace Cms.Aggregates.Forms.Events
{
    public class FormTemplateChanged : EventBase<Form>
    {
        public string Template { get; set; }
    }
}