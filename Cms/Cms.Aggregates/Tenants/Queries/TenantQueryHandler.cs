﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Tenants.Queries
{
    internal class TenantQueryHandler : QueryHandlerBase
    {
        private readonly ITenantRepository _repository;
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;

        public TenantQueryHandler(ITenantRepository repository, IUserRepository userRepository, IGroupRepository groupRepository)
        {
            _repository = repository;
            _userRepository = userRepository;
            _groupRepository = groupRepository;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetTenants),
            };
        }

        public IEnumerable<object> When(GetTenants query)
        {
            var user = _userRepository.Get(query.UserId);
            var groups = user.GroupIds.Select(g => _groupRepository.Get(g)).ToList();
            var tenantIds = groups.Select(g => g.TenantId).Distinct().ToList();
            var tenants = tenantIds.Select(t => _repository.Get(t)).ToList();

            if (user.IsSysadmin)
            {
                tenants = _repository.GetAll().ToList();
            }

            return tenants
                .OrderBy(t => t.Name)
                .Select(t => new
                {
                    Id = t.Id.ToJsonString(),
                    t.Name,
                });
        }
    }
}
