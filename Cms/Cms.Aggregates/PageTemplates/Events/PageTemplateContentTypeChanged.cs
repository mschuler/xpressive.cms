﻿namespace Cms.Aggregates.PageTemplates.Events
{
    public class PageTemplateContentTypeChanged : EventBase<PageTemplate>
    {
        public string ContentType { get; set; }
    }
}