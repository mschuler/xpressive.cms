﻿using Cms.Aggregates.Pages;

namespace Cms.Services.Contracts
{
    public interface IContentHandler
    {
        bool CanHandle(string pageContentType);

        string ContentType { get; }

        string GetImage();

        string Render(PageContentValueBase value);

        object GetValueForInterpreter(PageContentValueBase value);
    }
}