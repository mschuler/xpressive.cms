using System;
using RestSharp;
using RestSharp.Deserializers;

namespace Cms.Services.UrlShortening
{
    internal class BitlyShorteningService : UrlShorteningServiceBase
    {
        private const string AccessTokenKey = "Access Token";

        public BitlyShorteningService(IUrlShorteningRepository urlShorteningRepository)
            : base(new[] { AccessTokenKey }, urlShorteningRepository) { }

        public override string Name { get { return "bit.ly"; } }

        protected override string ShortenInternal(string longUrl)
        {
            string accessToken;
            if (!TryGetConfigurationValue(AccessTokenKey, out accessToken) || string.IsNullOrEmpty(accessToken))
            {
                throw new InvalidOperationException("Invalid configuration.");
            }

            var client = new RestClient("https://api-ssl.bitly.com/v3/");

            var request = new RestRequest("shorten", Method.GET);
            request.AddQueryParameter("longUrl", longUrl);
            request.AddQueryParameter("access_token", accessToken);

            var response = client.Execute<BitlyResult>(request);

            if (response == null)
            {
                throw new InvalidOperationException("Unable to access service.");
            }

            if (response.Data == null || response.Data.Data == null)
            {
                throw new InvalidOperationException("Unable to parse JSON result.");
            }

            return response.Data.Data.Url;
        }

        public class BitlyResult
        {
            [DeserializeAs(Name = "data")]
            public BitlyResultData Data { get; set; }

            [DeserializeAs(Name = "status_code")]
            public string StatusCode { get; set; }

            [DeserializeAs(Name = "status_txt")]
            public string StatusTxt { get; set; }
        }

        public class BitlyResultData
        {
            [DeserializeAs(Name = "global_hash")]
            public string GlobalHash { get; set; }

            [DeserializeAs(Name = "hash")]
            public string Hash { get; set; }

            [DeserializeAs(Name = "long_url")]
            public string LongUrl { get; set; }

            [DeserializeAs(Name = "new_hash")]
            public int NewHash { get; set; }

            [DeserializeAs(Name = "url")]
            public string Url { get; set; }
        }
    }
}