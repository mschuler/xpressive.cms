﻿using System;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class GroupDeletedEventTranslator : IEventTranslator
    {
        private readonly IGroupRepository _repository;

        public GroupDeletedEventTranslator(IGroupRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(GroupDeleted);
        }

        public string GetHeader(IEvent @event)
        {
            return "Benutzergruppe gelöscht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (GroupDeleted)@event;
            return string.Format("Die Benutzergruppe <strong>{0}</strong> wurde gelöscht.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "group";
        }
    }
}