using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Cms.SharedKernel;

namespace Cms.Aggregates.Pages
{
    internal class HtmlTagSearchService : IHtmlTagSearchService
    {
        private const string BaseRegexPattern = "<{0}((\\s+[a-z\\-0-9]+\\s*(?:=\\s*\"[^\"]*\")?)|(\\s+[a-z\\-0-9]+\\s*(?:=\\s*'[^']*')?))+\\s*/?>";
        private static readonly IDictionary<string, Regex> _regexes = new Dictionary<string, Regex>(StringComparer.OrdinalIgnoreCase);
        private static readonly object _regexesLock = new object();

        public IEnumerable<IHtmlTag> Find(string tagName, string html)
        {
            if (string.IsNullOrEmpty(tagName))
            {
                throw new ArgumentNullException("tagName").WithGuid();
            }
            if (string.IsNullOrEmpty(html))
            {
                yield break;
            }

            var regex = GetRegex(tagName);

            var matches = regex.Matches(html);

            foreach (Match match in matches)
            {
                var tag = new HtmlTag(match.Value);

                var attributes = new List<string>();
                foreach (var group in match.Groups.OfType<Group>().Skip(1).Take(2))
                {
                    foreach (Capture capture in group.Captures)
                    {
                        attributes.Add(capture.Value.Trim());
                    }
                }

                attributes = attributes.Distinct(StringComparer.Ordinal).ToList();

                foreach (var attribute in attributes)
                {
                    tag.AddAttribute(HtmlAttribute.Create(attribute));
                }

                yield return tag;
            }
        }

        private static Regex GetRegex(string tagName)
        {
            tagName = tagName.ToLowerInvariant();

            Regex regex;
            if (!_regexes.TryGetValue(tagName, out regex))
            {
                lock (_regexesLock)
                {
                    if (!_regexes.TryGetValue(tagName, out regex))
                    {
                        var pattern = string.Format(CultureInfo.InvariantCulture, BaseRegexPattern, tagName);
                        const RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Compiled;
                        regex = new Regex(pattern, options, TimeSpan.FromSeconds(5));
                        _regexes[tagName] = regex;
                    }
                }
            }

            return regex;
        }

        private class HtmlTag : IHtmlTag
        {
            private readonly List<IHtmlAttribute> _attributes;
            public HtmlTag(string tag)
            {
                Tag = tag;
                _attributes = new List<IHtmlAttribute>();
            }

            public string Tag { get; private set; }
            public IEnumerable<IHtmlAttribute> Attributes { get { return _attributes.AsReadOnly(); } }

            public void AddAttribute(IHtmlAttribute attribute)
            {
                _attributes.Add(attribute);
            }
        }

        private class HtmlAttribute : IHtmlAttribute
        {
            public static HtmlAttribute Create(string attribute)
            {
                attribute = attribute.Trim();
                var index = attribute.IndexOf('=');

                if (index < 0)
                {
                    return new HtmlAttribute(attribute, string.Empty, attribute);
                }

                var name = attribute.Substring(0, index);
                var value = attribute.Substring(index + 2, attribute.Length - index - 3);
                return new HtmlAttribute(name, value, attribute);
            }

            private HtmlAttribute(string name, string value, string attribute)
            {
                Name = name;
                Value = value;
                FullAttribute = attribute;
            }

            public string Name { get; private set; }
            public string Value { get; private set; }
            public string FullAttribute { get; private set; }
        }
    }
}