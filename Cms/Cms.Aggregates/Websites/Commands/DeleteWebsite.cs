using Cms.Aggregates.Security;

namespace Cms.Aggregates.Websites.Commands
{
    [RequiredPermission("website", PermissionLevel.CanEverything)]
    public class DeleteWebsite : CommandBase { }
}