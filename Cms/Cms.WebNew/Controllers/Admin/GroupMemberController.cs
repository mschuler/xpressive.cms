using System.Collections.Generic;
using System.Web.Http;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Commands;
using Cms.Aggregates.Security.Queries;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class GroupMemberController : ApiControllerBase
    {
        private readonly IGroupRepository _repository;

        public GroupMemberController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IGroupRepository repository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _repository = repository;
        }

        [HttpGet, Route("api/members/{groupId:ulong}")]
        public IEnumerable<object> Get(ulong groupId)
        {
            var tenantId = _repository.Get(groupId).TenantId;
            return ExecuteQuery(new GetGroupMembers
            {
                GroupId = groupId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/members/{groupId:ulong}/add/{userId:ulong}")]
        public void Add(ulong groupId, ulong userId)
        {
            var tenantId = _repository.Get(groupId).TenantId;
            SendCommand(new AddGroupMember
            {
                AggregateId = groupId,
                MemberId = userId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/members/{groupId:ulong}/remove/{userId:ulong}")]
        public void Remove(ulong groupId, ulong userId)
        {
            var tenantId = _repository.Get(groupId).TenantId;
            SendCommand(new RemoveGroupMember
            {
                AggregateId = groupId,
                MemberId = userId,
                TenantId = tenantId,
            });
        }
    }
}