﻿using System;
using System.Collections.Generic;

namespace Cms.Aggregates.Settings.Events
{
    public class UrlShorteningSettingsChanged : EventBase<Settings>
    {
        public string ServiceName { get; set; }
        public IList<Tuple<string, string>> Configuration { get; set; }
    }
}