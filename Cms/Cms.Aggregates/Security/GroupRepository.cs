using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Security
{
    internal class GroupRepository : RepositoryBase<Group>, IGroupRepository
    {
        public IEnumerable<Group> GetAll(ulong tenantId)
        {
            return GetAll().Where(g => g.TenantId == tenantId);
        }

        public override void DeleteAllByTenantId(ulong tenantId)
        {
            var groups = GetAll(tenantId).ToList();
            groups.ForEach(g => Delete(g.Id));
        }
    }
}