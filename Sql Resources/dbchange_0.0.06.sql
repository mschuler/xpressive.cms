begin transaction

create table FormAction (
	Id int not null identity(1, 1),
	FormId int not null,
	[Type] nvarchar(64) not null,
	[Value] nvarchar(256) not null,

	constraint PK_FormAction primary key (Id),
	constraint FK_FormAction_Form foreign key (FormId) references Form (Id)
)

insert into Version (Major, Minor, Revision) values (0, 0, 6);

commit transaction