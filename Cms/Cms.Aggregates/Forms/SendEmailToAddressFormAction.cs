namespace Cms.Aggregates.Forms
{
    public class SendEmailToAddressFormAction : FormActionConfiguration
    {
        private string _emailAddress;
        private string _emailSubject;
        private string _emailTemplateText;
        private string _emailTemplateHtml;

        public string EmailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = (value ?? string.Empty).Trim(); }
        }

        public string EmailSubject
        {
            get { return _emailSubject; }
            set { _emailSubject = (value ?? string.Empty).Trim(); }
        }

        public string EmailTemplateText
        {
            get { return _emailTemplateText; }
            set { _emailTemplateText = (value ?? string.Empty).Trim(); }
        }

        public string EmailTemplateHtml
        {
            get { return _emailTemplateHtml; }
            set { _emailTemplateHtml = (value ?? string.Empty).Trim(); }
        }

        public override FormActionConfiguration Clone()
        {
            return new SendEmailToAddressFormAction
            {
                EmailAddress = EmailAddress,
                EmailSubject = EmailSubject,
                EmailTemplateText = EmailTemplateText,
                EmailTemplateHtml = EmailTemplateHtml
            };
        }
    }
}