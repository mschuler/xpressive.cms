using Cms.Aggregates.Security;

namespace Cms.Aggregates.Calendars.Commands
{
    [RequiredPermission("calendarcategory", PermissionLevel.CanChange)]
    public class ChangeCategoryName : CommandBase
    {
        public string OldName { get; set; }
        public string NewName { get; set; }
    }
}