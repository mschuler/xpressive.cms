using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("user", PermissionLevel.CanChange)]
    public class CreateUser : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(5)]
        public string EmailAddress { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string LastName { get; set; }

        public bool IsSysadmin { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Password { get; set; }
    }
}