using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Commands;
using Cms.Aggregates.DynamicObjects.Queries;
using Cms.Aggregates.Security;
using Cms.SharedKernel;
using Cms.TemplateEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class ObjectController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _uniqueIdProvider;
        private readonly IDynamicObjectRepository _repository;
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;
        private readonly IObjectFieldContentTypeConverterService _contentTypeConverter;

        public ObjectController(IUniqueIdProvider uniqueIdProvider, ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IDynamicObjectDefinitionRepository definitionRepository, IDynamicObjectRepository repository, IObjectFieldContentTypeConverterService contentTypeConverter)
            : base(commandQueue, userRepository, queryHandler)
        {
            _uniqueIdProvider = uniqueIdProvider;
            _definitionRepository = definitionRepository;
            _repository = repository;
            _contentTypeConverter = contentTypeConverter;
        }

        [HttpGet, Route("api/objects/{tenantId:ulong}/{definitionId:ulong}")]
        public IEnumerable<object> GetAll(ulong tenantId, ulong definitionId, string filter = null)
        {
            var filters = new List<GetObjectFilter>();

            if (!string.IsNullOrEmpty(filter))
            {
                var settings = new JsonSerializerSettings();
                settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                filter = filter.Replace("_", "/");
                filter = Encoding.UTF8.GetString(Convert.FromBase64String(filter));
                filters.AddRange(JsonConvert.DeserializeObject<GetObjectFilter[]>(filter, settings));
            }

            return ExecuteQuery(new GetAllObjects
            {
                ObjectDefinitionId = definitionId,
                TenantId = tenantId,
                Filters = filters
            });
        }

        [HttpGet, Route("api/object/{objectId:ulong}")]
        public object Get(ulong objectId)
        {
            var tenantId = _repository.Get(objectId).TenantId;
            return ExecuteQuery(new GetObject
            {
                Id = objectId,
                TenantId = tenantId
            }).SingleOrDefault();
        }

        [HttpGet, Route("api/object/search/{tenantId:ulong}/{searchPattern}")]
        public IEnumerable<object> Search(ulong tenantId, string searchPattern)
        {
            return ExecuteQuery(new GetObjectsBySearchPattern
            {
                TenantId = tenantId,
                SearchPattern = searchPattern,
            });
        }

        [HttpPost, Route("api/objects/{tenantId:ulong}/{definitionId:ulong}")]
        public object Create(ulong tenantId, ulong definitionId, [FromBody]ObjectEntryDto dto)
        {
            var id = _uniqueIdProvider.GetId();
            var publicId = _repository.GetNextPublicId();
            var values = GetValuesFromDto(dto);

            var definition = _definitionRepository.Get(definitionId);
            var titleFieldName = definition.Fields.Single(f => f.SortOrder == 0).Name;
            var title = values[titleFieldName];

            if (definition.SharedForTenants == null || !definition.SharedForTenants.Contains(tenantId))
            {
                tenantId = definition.TenantId;
            }

            SendCommand(new CreateDynamicObject
            {
                AggregateId = id,
                DefinitionId = definitionId,
                TenantId = tenantId,
                PublicId = publicId,
                Values = values,
            });

            return new
            {
                Id = id.ToJsonString(),
                Nr = publicId,
                Title = title,
                dto.Fields,
                Fullname = string.Format("{0} (Nr. {1})", title, publicId),
            };
        }

        [HttpPost, Route("api/object/{tenantId:ulong}/{objectId:ulong}")]
        public void Change(ulong tenantId, ulong objectId, [FromBody] ObjectEntryDto dto)
        {
            var values = GetValuesFromDto(dto);
            tenantId = _repository.Get(objectId).TenantId;

            SendCommand(new ChangeDynamicObject
            {
                AggregateId = objectId,
                TenantId = tenantId,
                Values = values,
            });
        }

        [HttpPut, Route("api/object/{objectId:ulong}/share")]
        public void Share(ulong objectId, [FromBody] TenantListDto dto)
        {
            var tenantId = _repository.Get(objectId).TenantId;

            SendCommand(new ShareDynamicObject
            {
                AggregateId = objectId,
                TenantId = tenantId,
                TenantIds = dto.TenantIds.ToArray()
            });
        }

        [HttpDelete, Route("api/object/{tenantId:ulong}/{objectId:ulong}")]
        public void Delete(ulong tenantId, ulong objectId)
        {
            tenantId = _repository.Get(objectId).TenantId;

            SendCommand(new DeleteDynamicObject
            {
                AggregateId = objectId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/object/template/{templateId:ulong}")]
        public string GetWithTemplate(ulong templateId, [FromBody]ObjectEntryDto dto)
        {
            var definition = _definitionRepository.GetAll().SingleOrDefault(d => d.Templates.Any(t => t.Id == templateId));

            if (definition == null)
            {
                return string.Empty;
            }

            var template = definition.Templates.Single(t => t.Id == templateId);
            var o = new DynamicObject
            {
                CreationDate = DateTime.Now,
                TenantId = definition.TenantId,
                DefinitionId = definition.Id,
                Id = 0,
                PublicId = 0,
            };

            foreach (var pair in GetValuesFromDto(dto))
            {
                var field = dto.Fields.Single(f => f.Name == pair.Key);
                o.Add(new DynamicObjectField
                {
                    Name = field.Name,
                    ContentType = field.ContentType,
                    Value = pair.Value,
                });
            }

            TemplateStateMap.Set("Visitor.IpAddress", Request.GetClientIpAddress());
            TemplateStateMap.Set("QueryString", new NameValueCollection());

            using (TemplateStateMap.Set(TemplateStateMap.CurrentObjectKey, o))
            {
                var engine = new Engine();
                return engine.Generate(template.Template);
            }
        }

        private Dictionary<string, string> GetValuesFromDto(ObjectEntryDto dto)
        {
            var values = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            foreach (var pair in dto.Fields)
            {
                var value = pair.Value as string;
                if (value == null)
                {
                    value = JsonConvert.SerializeObject(pair.Value);
                }
                value = _contentTypeConverter.Serialize(pair.ContentType, value);
                values.Add(pair.Name, value);
            }

            return values;
        }

        public class ObjectEntryDto
        {
            public List<ObjectFieldEntryDto> Fields { get; set; }
        }

        public class ObjectFieldEntryDto
        {
            public string Name { get; set; }
            public string ContentType { get; set; }
            public dynamic Value { get; set; }
        }

        public class TenantListDto
        {
            public List<ulong> TenantIds { get; set; }
        }
    }
}