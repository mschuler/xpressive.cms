namespace Cms.WebNew.Controllers.PageRendering
{
    public abstract class RequestCacheFilterBase : IFilter<RenderPageMessage>
    {
        public abstract bool Execute(RenderPageMessage msg);

        protected string GetKey(RenderPageMessage msg)
        {
            return string.Format("PageRenderingPipeline/{0}/{1}", msg.Domain.Host, string.Join("/", msg.RouteParts));
        }

        protected class CachedPage
        {
            public string Content { get; set; }
            public string ContentType { get; set; }
        }
    }
}