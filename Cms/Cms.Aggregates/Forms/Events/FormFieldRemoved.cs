﻿namespace Cms.Aggregates.Forms.Events
{
    public class FormFieldRemoved : EventBase<Form>
    {
        public ulong Id { get; set; }
        public string Name { get; set; }
    }
}