﻿(function() {
    angular.module("app").controller("WebsitesController", [
        "$rootScope", "$scope", "$http", "$log", "tenantService", "$location", "dialogService", "toastService", "$translate",
        function ($rootScope, $scope, $http, $log, tenantService, $location, dialogService, toastService, $translate) {

        $scope.websites = [];
        $scope.selectedWebsite = {};

        var getWebsites = function () {
            $scope.websites = [];
            $http.get("/api/websites/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
                $scope.websites = data;
            });
        };

        $http.get("/api/permission/website/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.selectWebsite = function(website) {
            $scope.selectedWebsite = website;
        };

        var renameWebsite = "";
        var name = "";
        var errorRenameWebsite = "";
        var createWebsite = "";
        var errorCreateWebsite = "";
        var deleteWebsite = "";
        var confirmDeleteWebsite = "";
        var errorDeleteWebsite = "";

        $translate(["name", "rename_website", "error_rename_website", "create_website", "error_create_website", "delete_website", "confirm_delete_website", "error_delete_website"]).then(function(t) {
            renameWebsite = t.rename_website;
            name = t.name;
            errorRenameWebsite = t.error_rename_website;
            createWebsite = t.create_website;
            errorCreateWebsite = t.error_create_website;
            deleteWebsite = t.delete_website;
            confirmDeleteWebsite = t.confirm_delete_website;
            errorDeleteWebsite = t.error_delete_website;
        });

        $scope.renameWebsite = function (website) {
            dialogService.singleInputDialog(renameWebsite, name, website.name, function (newName) {
                var dto = {
                    name: newName
                };
                $http.post("/api/website/" + website.id, dto).success(function() {
                    website.name = newName;
                });
            });
        };

        $scope.createWebsite = function () {
            dialogService.singleInputDialog(createWebsite, name, "", function(n) {
                var dto = {
                    name: n
                };
                $http.put("/api/website/" + tenantService.getTenant().id, dto).success(function (data) {
                    $scope.websites.push(data);
                }).error(function(error) {
                    toastService.error(errorCreateWebsite, error);
                });
            });
        };

        $scope.deleteWebsite = function (website) {
            var n = String.format("{0} (Nr. {1})", website.name, website.nr);
            dialogService.confirmDialog(deleteWebsite, String.format(confirmDeleteWebsite, n), function () {
                $http.delete("/api/website/" + website.id).success(function() {
                    $scope.websites = _.reject($scope.websites, function(w) { return w.id === website.id; });
                }).error(function(error) {
                    toastService.error(errorDeleteWebsite, error);
                });
            });
        };

        $scope.editWebsite = function(website) {
            $location.path("/master/website/" + website.id);
        };

        $rootScope.$on("tenantChanged", getWebsites);

        $rootScope.$on("logout", function () {
            $scope.websites = [];
        });

        getWebsites();
    }]);

    angular.module("app").controller("WebsiteController", ["$scope", "$http", "$log", "$routeParams", "dialogService", "$translate", "toastService", function ($scope, $http, $log, $routeParams, dialogService, $translate, toastService) {
        var websiteId = $routeParams.websiteId;
        $scope.website = {};
        $scope.selectedHost = [];

        var tAddDomain = "";
        var tErrorAddDomain = "";
        var tRenameDomain = "";
        var tErrorRenameDomain = "";
        var tDeleteDomain = "";
        var tConfirmDeleteDomain = "";
        var tErrorDeleteDomain = "";

        $translate(["add_domain", "error_add_domain", "rename_domain", "error_rename_domain", "delete_domain", "confirm_delete_domain", "error_delete_domain"]).then(function (t) {
            tAddDomain = t.add_domain;
            tErrorAddDomain = t.error_add_domain;
            tRenameDomain = t.rename_domain;
            tErrorRenameDomain = t.error_rename_domain;
            tDeleteDomain = t.delete_domain;
            tConfirmDeleteDomain = t.confirm_delete_domain;
            tErrorDeleteDomain = t.error_delete_domain;
        });

        $http.get("/api/website/" + websiteId, { cache: false }).success(function (data) {
            $scope.website = data;
        });

        $scope.selectHost = function(domain) {
            $scope.selectedHost = domain.host;
        };

        $scope.setDefault = function (domain) {
            var dto = {
                domain: domain.host
            };
            return $http.post("/api/website/" + websiteId + "/default/", dto).success(function() {
                angular.forEach($scope.website.domains, function(d) {
                    d.isDefault = false;
                });
                domain.isDefault = true;
            });
        };

        $scope.addDomain = function() {
            dialogService.singleInputDialog(tAddDomain, "Host", "", function (name) {
                var dto = {
                    domain: name
                };
                $http.put("/api/website/" + websiteId + "/domain/", dto).success(function () {
                    $scope.website.domains.push({ host: name, isDefault: false });
                }).error(function (error) {
                    toastService.error(tErrorAddDomain, error);
                });
            });
        };

        $scope.deleteDomain = function (domain) {
            dialogService.confirmDialog(tDeleteDomain, String.format(tConfirmDeleteDomain, domain.host), function () {
                var url = String.format("/api/website/{0}/domain?name={1}", websiteId, domain.host);
                $http.delete(url).success(function () {
                    $scope.website.domains = _.reject($scope.website.domains, function (d) {
                        return d.host === domain.host;
                    });
                }).error(function (error) {
                    toastService.error(tErrorDeleteDomain, error);
                });
            });
        };

        $scope.renameDomain = function(domain) {
            dialogService.singleInputDialog(tRenameDomain, "Host", domain.host, function (newName) {
                var dto = {
                    oldDomain: domain.host,
                    newDomain: newName
                };
                $http.post("/api/website/" + websiteId + "/domain/", dto).success(function() {
                    domain.host = newName;
                }).error(function (error) {
                    toastService.error(tErrorRenameDomain, error);
                });
            });
        };
    }]);
}());
