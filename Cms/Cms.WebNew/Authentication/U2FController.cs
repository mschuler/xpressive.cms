﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Formatting;
using System.Web.Http;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Commands;
using Cms.Aggregates.Websites;
using Cms.Services.Contracts;
using Cms.WebNew.Controllers;
using Newtonsoft.Json.Serialization;

namespace Cms.WebNew.Authentication
{
    [Authorize]
    public class U2FController : ApiControllerBase
    {
        private readonly IWebsiteRepository _websiteRepository;
        private readonly IU2FService _u2FService;

        public U2FController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IWebsiteRepository websiteRepository, IU2FService u2FService)
            : base(commandQueue, userRepository, queryHandler)
        {
            _websiteRepository = websiteRepository;
            _u2FService = u2FService;
        }

        public static string GetAppId(string scheme, string host, int port)
        {
            var url = $"{scheme}://{host}:{port}";
            var uri = new Uri(url, UriKind.Absolute);

            if (uri.IsDefaultPort)
            {
                url = $"{scheme}://{host}";
            }

            return url + "/api/u2f/appids.json";
        }

        [AllowAnonymous]
        [HttpGet, HttpHead, Route("api/u2f/appids.json"),]
        public IHttpActionResult GetValidAppIds()
        {
            var appid = GetAppId();
            var port = appid.IsDefaultPort ? string.Empty : ":" + appid.Port;

            var ids = _websiteRepository
                .GetAll()
                .SelectMany(w => w.Domains.Select(d => d.Host))
                .Union(new[] { "localhost" })
                .Distinct(StringComparer.OrdinalIgnoreCase)
                .Select(d => $"https://{d}{port}")
                .ToArray();

            var dto = new AppIdsDto
            {
                TrustedFacets = new[]
                {
                    new TrustedFacetDto
                    {
                        Version = new TrustedFacetVersion(),
                        Ids = ids
                    }
                }
            };

            var formatter = new JsonMediaTypeFormatter
            {
                SerializerSettings = { ContractResolver = new CamelCasePropertyNamesContractResolver() }
            };

            return Content(HttpStatusCode.OK, dto, formatter, "application/fido.trusted-apps+json");
        }

        [HttpGet, Route("api/u2f/register")]
        public IDeviceRegistration StartRegistration()
        {
            var appid = GetAppId();
            var registration = _u2FService.StartRegistration(CmsUserId, appid.OriginalString);
            return registration;
        }

        [HttpPost, Route("api/u2f/register")]
        public IHttpActionResult FinishRegistration([FromBody]U2FDeviceRegistrationDto dto)
        {
            IDevice device;
            if (!_u2FService.TryFinishRegistration(CmsUserId, dto.JsonData, out device))
            {
                return NotFound();
            }

            SendCommand(new AddU2FDevice
            {
                AggregateId = CmsUserId,
                Password = dto.Password,
                PublicKey = device.PublicKey,
                AttestationCert = device.AttestationCert,
                KeyHandle = device.KeyHandle
            });

            return Ok();
        }

        [HttpGet, Route("api/u2f/devices")]
        public IEnumerable<string> GetDevices()
        {
            return CmsUser.U2FDevices.Select(d => BitConverter.ToString(d.PublicKey).Replace("-", string.Empty));
        }

        public IHttpActionResult RemoveDevice()
        {
            return NotFound();
        }

        private Uri GetAppId()
        {
            var uri = Request.RequestUri;
            var url = GetAppId(uri.Scheme, uri.Host, uri.Port);
            return new Uri(url, UriKind.Absolute);
        }

        public class U2FDeviceRegistrationDto
        {
            public string Password { get; set; }
            public string JsonData { get; set; }
        }

        public class AppIdsDto
        {
            public TrustedFacetDto[] TrustedFacets { get; set; }
        }

        public class TrustedFacetDto
        {
            public TrustedFacetVersion Version { get; set; }
            public string[] Ids { get; set; }
        }

        public class TrustedFacetVersion
        {
            public TrustedFacetVersion()
            {
                Major = 1;
                Minor = 0;
            }

            public int Major { get; set; }
            public int Minor { get; set; }
        }
    }
}