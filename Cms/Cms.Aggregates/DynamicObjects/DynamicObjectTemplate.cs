namespace Cms.Aggregates.DynamicObjects
{
    public class DynamicObjectTemplate
    {
        public ulong Id { get; set; }
        public int PublicId { get; set; }
        public string Name { get; set; }
        public string Template { get; set; }
    }
}