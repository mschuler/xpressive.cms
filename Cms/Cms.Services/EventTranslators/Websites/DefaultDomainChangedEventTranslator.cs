﻿using System;
using Cms.Aggregates.Websites;
using Cms.Aggregates.Websites.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Websites
{
    internal class DefaultDomainChangedEventTranslator : IEventTranslator
    {
        private readonly IWebsiteRepository _repository;

        public DefaultDomainChangedEventTranslator(IWebsiteRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DefaultDomainChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Standarddomain geändert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DefaultDomainChanged)@event;
            var website = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Die Standarddomain der Website <strong>{1}</strong> wurde in <strong>{0}</strong> geändert.", e.Host, website.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var website = _repository.GetEvenIfDeleted(@event.AggregateId);
            return website.TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "website";
        }
    }
}