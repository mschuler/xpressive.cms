﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("object", PermissionLevel.CanEverything)]
    public class DeleteDynamicObject : CommandBase { }
}