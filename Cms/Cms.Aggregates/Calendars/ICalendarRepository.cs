using System.Collections.Generic;

namespace Cms.Aggregates.Calendars
{
    public interface ICalendarRepository : IRepository<Calendar>
    {
        IEnumerable<AppointmentOccurrence> GetAppointments(ulong tenantId, DateTimeRange fromTo);

        IEnumerable<Category> GetCategories(ulong tenantId);

        Appointment GetAppointment(ulong tenantId, ulong appointmentId);
    }
}