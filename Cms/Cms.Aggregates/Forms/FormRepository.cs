﻿using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Forms
{
    internal class FormRepository : RepositoryBase<Form>, IFormRepository
    {
        private static readonly object _lock = new object();
        private static int _currentMaxPublicId;

        public int GetNextPublicId()
        {
            lock (_lock)
            {
                if (_currentMaxPublicId == 0)
                {
                    var allForms = GetAll().ToList();
                    _currentMaxPublicId = allForms.Count == 0 ? 0 : allForms.Max(p => p.PublicId);
                }

                _currentMaxPublicId++;
                return _currentMaxPublicId;
            }
        }

        public Form GetByPublicId(int publicId)
        {
            return GetAll().SingleOrDefault(f => f.PublicId == publicId);
        }

        public IEnumerable<Form> GetAll(ulong tenantId)
        {
            return GetAll().Where(f => f.TenantId == tenantId);
        }

        public override void DeleteAllByTenantId(ulong tenantId)
        {
            var forms = GetAll(tenantId).ToList();
            forms.ForEach(f => Delete(f.Id));
        }
    }
}
