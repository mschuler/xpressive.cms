﻿namespace Cms.Aggregates.Security.Queries
{
    [RequiredPermission("group", PermissionLevel.CanSee)]
    public class GetAllGroups : QueryBase { }
}