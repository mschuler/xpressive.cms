﻿(function() {
    angular.module("app").controller("PageTemplatesController", [
        "$rootScope", "$scope", "$http", "$log", "$location", "tenantService", "dialogService", "$modal", "$translate", "toastService",
        function ($rootScope, $scope, $http, $log, $location, tenantService, dialogService, $modal, $translate, toastService) {
            $scope.templates = [];
            $scope.selectedTemplate = {};

            function reloadTemplates() {
                $scope.templates = [];
                $scope.selectedTemplate = {};

                var tenantId = tenantService.getTenant().id;

                $http.get("/api/pagetemplates/" + tenantId, { cache: false }).success(function (data) {
                    _.each(data, function (d) {
                        d.isShared = false;
                        $scope.templates.push(d);
                    });
                });

                $http.get("/api/pagetemplates/" + tenantId + "/shared", { cache: false }).success(function (data) {
                    _.each(data, function (d) {
                        d.isShared = true;
                        $scope.templates.push(d);
                    });
                });
            };

            $http.get("/api/permission/pagetemplate/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
                $scope.canEdit = permission.canEdit;
                $scope.canAdmin = permission.canAdmin;
            });

            $scope.select = function (template) {
                $scope.selectedTemplate = template;
            };

            $scope.shareTemplate = function (template) {
                var existingTenantIds = [];

                if (template.shared) {
                    existingTenantIds = _.map(template.shared, function (t) { return t.id; });
                }

                dialogService.selectTenantsDialog(existingTenantIds, function (tenants) {
                    var ids = _.map(tenants, function (t) {
                        return t.id;
                    });
                    var dto = {
                        tenantIds: ids
                    };
                    var url = String.format("/api/pagetemplate/{0}/share", template.id);
                    $http.put(url, dto).success(function () {
                        template.shared = tenants;
                    });
                });
            };

            $scope.donotshareTemplate = function (template) {
                var url = String.format("/api/pagetemplate/{0}/share", template.id);
                var dto = {
                    tenantIds: []
                };
                return $http.put(url, dto).success(function () {
                    template.shared = [];
                });
            };

            $scope.getNames = function (tenants) {
                var names = "";
                _.each(tenants, function (t) {
                    names += t.name + ", ";
                });
                names = names.trim();
                return names.substring(0, names.length - 1);
            };

            var errorCreatePageTemplate = "";
            var renamePageTemplate = "";
            var name = "";
            var errorRenamePageTemplate = "";
            var deletePageTemplate = "";
            var confirmDeletePageTemplate = "";
            var errorDeletePageTemplate = "";

            $translate(["error_create_page_template", "rename_page_template", "name", "error_rename_page_template", "delete_page_template", "confirm_delete_page_template", "error_delete_page_template"]).then(function (t) {
                errorCreatePageTemplate = t.error_create_page_template;
                renamePageTemplate = t.rename_page_template;
                name = t.name;
                errorRenamePageTemplate = t.error_rename_page_template;
                deletePageTemplate = t.delete_page_template;
                confirmDeletePageTemplate = t.confirm_delete_page_template;
                errorDeletePageTemplate = t.error_delete_page_template;
            });

            $scope.create = function () {
                var modalInstance = $modal.open({
                    backdrop: 'static',
                    templateUrl: "/admin/views/masterdata/createPageTemplateTemplate.min.html",
                    controller: "CreatePageTemplateController"
                });

                modalInstance.result.then(function (template) {
                    if (template && template.name && template.contentType) {
                        var dto = {
                            name: template.name,
                            contentType: template.contentType.contentType,
                        };
                        $http.put("/api/pagetemplates/" + tenantService.getTenant().id, dto).success(function (data) {
                            $scope.templates.push(data);
                            $scope.selectedTemplate = data;
                        }).error(function (error) {
                            toastService.error(errorCreatePageTemplate, error);
                        });
                    }
                });
            };

            $scope.edit = function (template) {
                $location.path("/master/pagetemplate/" + template.id);
            };

            $scope.rename = function (template) {
                dialogService.singleInputDialog(renamePageTemplate, name, template.name, function (n) {
                    var dto = {
                        name: n
                    };
                    $http.post("/api/pagetemplate/" + template.id + "/name", dto).success(function() {
                        template.name = n;
                    }).error(function(error) {
                        toastService.error(errorRenamePageTemplate, error);
                    });
                });
            };

            $scope.delete = function (template) {
                dialogService.confirmDialog(deletePageTemplate, String.format(confirmDeletePageTemplate, template.name), function () {
                    $http.delete("/api/pagetemplate/" + template.id).success(function () {
                        var others = _.reject($scope.templates, function(t) {
                            return t.id === template.id;
                        });
                        $scope.templates = others;
                    }).error(function (error) {
                        toastService.error(errorDeletePageTemplate, error);
                    });
                });
            };

            $rootScope.$on("tenantChanged", reloadTemplates);

            $rootScope.$on('logout', function () {
                $scope.templates = [];
            });

            reloadTemplates();
        }
    ]);

    angular.module('app').controller("CreatePageTemplateController", ["$scope", "$modalInstance", "$http", function ($scope, $modalInstance, $http) {
        $scope.scope = {
            name: "",
            contentTyp: {}
        };

        $scope.ok = function () {
            $modalInstance.close($scope.scope);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $http.get("/api/pagetemplates/contenttypes", { cache: false }).success(function (data) {
            $scope.contentTypes = data;
        });
    }]);
}());