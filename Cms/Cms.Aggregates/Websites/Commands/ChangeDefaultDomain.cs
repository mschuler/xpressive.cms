using Cms.Aggregates.Security;

namespace Cms.Aggregates.Websites.Commands
{
    [RequiredPermission("website", PermissionLevel.CanChange)]
    public class ChangeDefaultDomain : CommandBase
    {
        public string Host { get; set; }
    }
}