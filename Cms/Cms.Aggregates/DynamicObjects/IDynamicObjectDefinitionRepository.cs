﻿using System.Collections.Generic;

namespace Cms.Aggregates.DynamicObjects
{
    public interface IDynamicObjectDefinitionRepository : IRepository<DynamicObjectDefinition>
    {
        int GetNextPublicId();

        int GetNextPublicIdForTemplate();

        IEnumerable<DynamicObjectDefinition> GetAll(ulong tenantId);
    }
}