﻿using System.Collections.Generic;
using System.Linq;
using Cms.Services.Contracts;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class IsKnownBot : IFilter<RenderPageMessage>
    {
        private static readonly IList<string> _botUserAgents = new List<string>
        {
            "bot", "google", "coccoc", "genieo", "nutch", "princexml.com",
            "bingpreview", "zune", "ltx71", "itms", "skype", "netcraft.com",
            "yahoo", "crawler", "spider", "podcast", "quicktime", "player",
            "browsershots", "pinterest", "lipperhey", "siteimprove",
            "feed", "securityheaders", "furred.net", "followthatpage", "scanner",
            "masscan", "asafaweb", "reeder", "scrapy", "streamium", "stagefright",
            "python-requests", "archiver", "metauri", "funwebproducts",
            "validator.w3.org", "itunes", "rss", "poddirectory", "podkicker",
            "loader.io", "ssl server survey", "ssllabs", "crazy browser",
            "coremedia", "ips-agent", "thefreedictionary", "webdav",
            "premo-retriever", "sitemapgenerator", "xenu link", "webcapture",
            "facebookexternalhit", "apple-pubsub", "wget", "activexperts",
            "overcast", "facebookexternalhit", "okhttp", "naver.com",
            "updown.io", "cfnetwork", "searchch", "windowsmail", "microsoft outlook",
            "jakarta commons-httpclient",
        };

        public bool Execute(RenderPageMessage msg)
        {
            var userAgent = msg.Request.Headers?.UserAgent?.ToString();

            if (string.IsNullOrEmpty(userAgent))
            {
                msg.IsKnownBot = true;
                return true;
            }

            userAgent = userAgent.ToLowerInvariant();
            msg.IsKnownBot = _botUserAgents.Any(userAgent.Contains);
            return true;
        }
    }
}