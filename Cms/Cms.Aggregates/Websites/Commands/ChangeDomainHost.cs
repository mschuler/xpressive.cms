using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Websites.Commands
{
    [RequiredPermission("website", PermissionLevel.CanChange)]
    public class ChangeDomainHost : CommandBase
    {
        [Required(AllowEmptyStrings = false), RegularExpression(WebsiteCommandHandler.DomainValidationRegex)]
        public string OldHost { get; set; }

        [Required(AllowEmptyStrings = false), RegularExpression(WebsiteCommandHandler.DomainValidationRegex)]
        public string NewHost { get; set; }
    }
}