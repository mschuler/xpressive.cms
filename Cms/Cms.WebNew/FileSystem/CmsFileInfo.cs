using System;
using System.IO;
using Cms.Aggregates;
using Microsoft.Owin.FileSystems;

namespace Cms.WebNew.FileSystem
{
    public class CmsFileInfo : IFileInfo
    {
        private readonly FileInfo _fileInfo;
        private readonly ImageSettingsDto _imageSettings;
        private readonly Action<Stream> _addToCache;
        private readonly string _physicalPath;
        private readonly Lazy<Stream> _imageFactoryStream;

        public CmsFileInfo(FileInfo fileInfo, ImageSettingsDto imageSettings, Action<Stream> addToCache)
        {
            _fileInfo = fileInfo;
            _imageSettings = imageSettings;
            _addToCache = addToCache;
            _physicalPath = fileInfo.FullName;
            Name = fileInfo.Name;
            LastModified = fileInfo.LastWriteTimeUtc;

            if (_imageSettings != null && _imageSettings.RoundedCorners > 0)
            {
                Name = Name.Substring(0, Name.Length - 4).TrimEnd('.') + ".png";
            }

            _imageFactoryStream = new Lazy<Stream>(() => CreateStream(_physicalPath, _imageSettings));
        }

        public long Length
        {
            get
            {
                if (IsImageFactoryStream)
                {
                    return _imageFactoryStream.Value.Length;
                }
                return _fileInfo.Length;
            }
        }

        public string PhysicalPath => null;
        public string Name { get; private set; }
        public DateTime LastModified { get; private set; }
        public bool IsDirectory => false;

        private bool IsImageFactoryStream => _imageSettings != null && !_imageSettings.Equals(new ImageSettingsDto());

        public Stream CreateReadStream()
        {
            if (IsImageFactoryStream)
            {
                var result = _imageFactoryStream.Value;
                _addToCache(result);
                return result;
            }

            return new FileStream(
                _physicalPath,
                FileMode.Open,
                FileAccess.Read,
                FileShare.Read,
                1024 * 64,
                FileOptions.SequentialScan);
        }

        private static Stream CreateStream(string physicalPath, ImageSettingsDto imageSettings)
        {
            return ImageFactoryService.GetImageStream(new FileInfo(physicalPath), imageSettings, _ => { });
        }
    }
}