﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Routing;
using Autofac.Integration.WebApi;
using Cms.Aggregates.Files;
using Cms.Aggregates.Security;
using Cms.DatabaseMigrations;
using Cms.EventSourcing.Contracts.Commands;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using Cms.TemplateEngine.Interfaces;
using Cms.WebNew.Authentication;
using Cms.WebNew.Controllers;
using Cms.WebNew.Controllers.RoutingAndAttributes;
using Cms.WebNew.FileSystem;
using log4net;
using Metrics;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.StaticFiles;
using Newtonsoft.Json.Serialization;
using Owin;
using Owin.Metrics;

[assembly: OwinStartup(typeof(Cms.WebNew.Startup))]

namespace Cms.WebNew
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            SetupLogging();

            ServiceLocator.RegisterApiControllers();
            ServiceLocator.Build();

            SetupDatabase();

            SetupTemplateEngine();

            SetupEventSourcing();

            GlobalConfiguration.Configure(config =>
            {
                var constraintResolver = new DefaultInlineConstraintResolver();
                constraintResolver.ConstraintMap.Add("ulong", typeof(UlongConstraint));

                config.MapHttpAttributeRoutes(constraintResolver);
                config.Services.Add(typeof(IExceptionLogger), new Log4NetExceptionLogger());
                config.Filters.Add(new NoCacheHeaderFilter());
                config.Filters.Add(new ExceptionWithReasonFilter());
                config.Filters.Add(new RequireHttpsAttribute());

                app.MapSignalR(new HubConfiguration());

                config.Routes.MapHttpRoute(
                    "PageRenderingRoute",
                    "{*pages}",
                    new { controller = "PageRendering" }
                );

                var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
                jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                config.DependencyResolver = new AutofacWebApiDependencyResolver(ServiceLocator.ServiceContainer);

                ConfigureOAuth(app);

                SetupMetrics(app);

                SetupFileServer(app);

                app.UseWebApi(config);
            });

            RestoreModelFromEventStore();

            SetupRecurrentJobs();
        }

        private void SetupMetrics(IAppBuilder app)
        {
            Metric.Config
                .WithOwin(middleware => app.Use(middleware), conf => conf
                    .WithMetricsEndpoint(ec => ec
                        .MetricsTextEndpoint("metrics/text")
                        .MetricsJsonEndpoint("metrics/json")
                        .MetricsEndpoint(enabled: false)
                        .MetricsHealthEndpoint(enabled: false)
                        .MetricsPingEndpoint(enabled: false)
                    )
                );

            var driveName = AppDomain.CurrentDomain.BaseDirectory.Substring(0, 2);

            Metric.Gauge("[System] CPU Usage", CpuMeter.GetCpuUtilization, Unit.Percent);
            Metric.Gauge("[System] Available RAM", RamMeter.GetAvailableMemory, Unit.MegaBytes);
            Metric.Gauge("[System] Physical Disk Free Space", () => new DriveInfo(driveName).AvailableFreeSpace / 1024.0 / 1024.0, Unit.MegaBytes);
            Metric.Gauge("[System] Physical Disk Total Space", () => new DriveInfo(driveName).TotalSize / 1024.0 / 1024.0, Unit.MegaBytes);
            Metric.Gauge("[System] Total RAM", RamMeter.GetTotalMemory, Unit.MegaBytes);
        }

        private void SetupRecurrentJobs()
        {
            var recurrentJobExecutor = ServiceLocator.Resolve<IRecurrentJobExecutor>();
            recurrentJobExecutor.Start();
        }

        private void SetupDatabase()
        {
            var migrator = ServiceLocator.Resolve<IDatabaseMigrator>();
            migrator.Migrate();
        }

        private void SetupFileServer(IAppBuilder app)
        {
            var localFileStore = ServiceLocator.Resolve<ILocalFileStore>();
            var fileRepository = ServiceLocator.Resolve<IFileRepository>();
            var contentTypeService = ServiceLocator.Resolve<IContentTypeService>();
            var temporaryFileStoreService = ServiceLocator.Resolve<ITemporaryFileStoreService>();
            var fileSystem = new CmsFileSystem(localFileStore, fileRepository, temporaryFileStoreService);

            var fileServerOptions = new FileServerOptions
            {
                FileSystem = fileSystem,
                RequestPath = new PathString("/download/local"),
                EnableDefaultFiles = false,
                EnableDirectoryBrowsing = false,
            };

            fileServerOptions.StaticFileOptions.ContentTypeProvider = new CmsContentTypeProvider(contentTypeService);
            fileServerOptions.StaticFileOptions.OnPrepareResponse = fileSystem.PrepareResponse;

            app.UseFileServer(fileServerOptions);
        }

        private void SetupTemplateEngine()
        {
            var templateInterpreters = ServiceLocator.ResolveAll<IInterpreter>();
            foreach (var interpreter in templateInterpreters)
            {
                TemplateEngine.Interpreter.InterpreterFactory.Instance.Register(interpreter);
            }
        }

        private void ConfigureOAuth(IAppBuilder app)
        {
            var userRepository = ServiceLocator.Resolve<IUserRepository>();
            var totpService = ServiceLocator.Resolve<ITotpService>();
            var u2FService = ServiceLocator.Resolve<IU2FService>();
            var authorizationServerProvider = new SimpleAuthorizationServerProvider(userRepository, totpService, u2FService);

            var oAuthServerOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = false,
                TokenEndpointPath = new PathString("/api/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(120),
                Provider = authorizationServerProvider,
            };

#if DEBUG
            oAuthServerOptions.AllowInsecureHttp = true;
#endif

            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        private void RestoreModelFromEventStore()
        {
            LocalEventStore.RestoreModelFromEventStore();
        }

        private void SetupEventSourcing()
        {
            EventSourcing.EventSourcing.SetCommandHandlerResolver(ServiceLocator.ResolveAll<ICommandHandler>);
            EventSourcing.EventSourcing.SetEventHandlerResolver(ServiceLocator.ResolveAll<IEventHandler>);
        }

        private void SetupLogging()
        {
            log4net.Config.XmlConfigurator.Configure();
            var logger = LogManager.GetLogger(GetType());

            logger.Info("-----------------------------------------------------------------------------");
            logger.Info("Application is starting up");
            logger.InfoFormat("Version        : {0}", Assembly.GetExecutingAssembly().GetName().Version);
            logger.InfoFormat("Base directory : {0}", AppDomain.CurrentDomain.BaseDirectory);
            logger.Info("-----------------------------------------------------------------------------");
        }
    }
}
