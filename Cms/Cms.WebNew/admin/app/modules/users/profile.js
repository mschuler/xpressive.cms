﻿(function() {
    angular.module("app").controller("ProfileController", ["$rootScope", "$scope", "$http", "$timeout", "$log", "toastService", "userService", "dialogService", "tenantService", "$translate", "$modal", function ($rootScope, $scope, $http, $timeout, $log, toastService, userService, dialogService, tenantService, $translate, $modal) {
        var user = userService.currentUser();

        $scope.events = [];
        $scope.isBusy = false;
        $scope.isChrome = false;
        var isFinish = false;

        if (window.chrome) {
            $scope.isChrome = true;
        }

        var changeName = "";
        var firstName = "";
        var lastName = "";
        var errorSaveName = "";
        var errorSavePassword = "";
        var changePassword = "";
        var enterNewPassword = "";

        $translate(["change_name", "first_name", "last_name", "error_save_name", "error_save_password", "change_password", "enter_new_password"]).then(function (t) {
            changeName = t.change_name;
            firstName = t.first_name;
            lastName = t.last_name;
            errorSaveName = t.error_save_name;
            errorSavePassword = t.error_save_password;
            changePassword = t.change_password;
            enterNewPassword = t.enter_new_password;
        });

        $scope.nextPage = function () {
            if ($scope.isBusy || isFinish) {
                return;
            }
            $scope.isBusy = true;

            var count = $scope.events.length;
            var url = String.format("/api/events/{1}/user/{2}?$skip={0}&$top=20&$orderby=Id desc", count, tenantService.getTenant().id, user.id);
            $http.get(url, { cache: false }).success(function (data) {
                if (data.length < 20) {
                    isFinish = true;
                }

                for (var i = 0; i < data.length; i++) {
                    $scope.events.push(data[i]);
                }
                $scope.isBusy = false;
            });
        };

        $scope.changeName = function () {
            dialogService.twoInputsDialog(changeName, firstName, user.firstName, lastName, user.lastName, function (firstName, lastName) {
                var url = String.format("/api/users/{0}/user/name", tenantService.getTenant().id);
                $http.post(url, { firstName: firstName, lastName: lastName }).success(function() {
                    user.firstName = firstName;
                    user.lastName = lastName;
                    user.name = (firstName + " " + lastName).trim();
                    $timeout($scope.nextPage, 5000);
                }).error(function (error) {
                    toastService.error(errorSaveName, error);
                });
            });
        };

        $scope.changePassword = function () {
            dialogService.singlePasswordDialog(changePassword, enterNewPassword, function (passwords) {
                passwords.oldPassword = CryptoJS.enc.Utf8.parse(passwords.oldPassword);
                passwords.oldPassword = CryptoJS.enc.Base64.stringify(passwords.oldPassword);
                passwords.newPassword = CryptoJS.enc.Utf8.parse(passwords.newPassword);
                passwords.newPassword = CryptoJS.enc.Base64.stringify(passwords.newPassword);
                var url = String.format("/api/users/{0}/user/password", tenantService.getTenant().id);
                $http.post(url, passwords).success(function () {
                    $timeout($scope.nextPage, 5000);
                }).error(function (error) {
                    toastService.error(errorSavePassword, error);
                });
            });
        };

        $scope.enable2ndFactor = function() {
            var modalInstance = $modal.open({
                backdrop: "static",
                templateUrl: "/admin/views/pages/enable2ndFactorDialog.min.html",
                controller: "Enable2ndFactorController",
                size: "lg",
                resolve: { }
            });

            modalInstance.result.then(function (a) {
                user.isTotp = true;
            });
        };

        $scope.disable2ndFactor = function () {
            // todo: nachfragen
            $http.delete("/api/2fa/totp").success(function () {
                user.isTotp = false;
            });
        };

        $http.get("/api/u2f/devices", { cache: false }).success(function(devices) {
            $scope.devices = devices.length;
        });

        $scope.addU2FDevice = function () {
            $http.get("/api/u2f/register", { cache: false }).success(function(req) {
                var modalInstance = $modal.open({
                    backdrop: "static",
                    templateUrl: "/admin/views/pages/addU2FDevice.min.html",
                    controller: "AddU2FDeviceController",
                    resolve: {
                        u2frequest: function () { return req; }
                    }
                });

                modalInstance.result.then(function (a) {
                    $scope.devices += 1;
                });
            });
        };

        $scope.removeU2FDevices = function() {
            // todo: open modal dialog with list of all keys
        };

        $rootScope.$on("logout", function () { $scope.events = []; });

        $rootScope.$on("tenantChanged", function () {
            $scope.events = [];
            isFinish = false;
            $scope.isBusy = false;
            $scope.nextPage();
        });
    }]);

    angular.module("app").controller("AddU2FDeviceController", ["$modalInstance", "$scope", "$http", "$log", "u2frequest", function($modalInstance, $scope, $http, $log, u2frequest) {
        $scope.currentStep = 0;
        $scope.isError = false;
        $scope.isValid = false;

        $scope.goToStep = function (step) {
            if ($scope.currentStep < step) {
                return;
            }
            $scope.currentStep = step;
        };

        $scope.save = function () {
            $modalInstance.close();
        };

        $scope.back = function () {
            if ($scope.currentStep > 0) {
                $scope.currentStep--;
            }
        };

        $scope.next = function () {
            if ($scope.currentStep < 1) {
                $scope.currentStep = 1;

                u2f.register([u2frequest], [], function (data) {
                    var dto = {
                        password: $scope.password,
                        jsonData: angular.toJson(data)
                    };

                    if (data.errorCode) {
                        // https://developers.yubico.com/U2F/Libraries/Client_error_codes.html
                        $log.debug("Unable to register device: error code " + data.errorCode);

                        $scope.isError = true;

                        if (data.errorCode === 5) {
                            // timeout
                            // show error message
                        }

                        $scope.isValid = false;
                        return;
                    }

                    $http.post("/api/u2f/register", dto).success(function () {
                        $scope.isValid = true;
                    }).error(function() {
                        $scope.isError = true;
                        // show error message
                    });
                }, 120);
            }
        }

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("Enable2ndFactorController", ["$scope", "$http", "$modalInstance", "$log", "$translate", "currentUser", function ($scope, $http, $modalInstance, $log, $translate, currentUser) {
        $scope.currentStep = 0;
        $scope.isError = false;
        var dto = {};

        $http.get("/api/2fa/totp", { cache: false }).success(function (data) {
            dto = data;
            var qrdata = encodeURIComponent(data.url);
            $scope.qr = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=" + qrdata;
            $scope.secret = data.secret;
        });

        $scope.goToStep = function (step) {
            if ($scope.currentStep < step) {
                return;
            }
            $scope.currentStep = step;
        };

        $scope.save = function () {
            $scope.isError = false;
            dto.code = $scope.code;
            dto.password = $scope.password;

            $http.put("/api/2fa/totp", dto).success(function (data) {
                if (data.isSuccessful) {
                    $modalInstance.close();
                } else {
                    $scope.isError = true;
                }
            });
        };

        $scope.back = function () {
            if ($scope.currentStep > 0) {
                $scope.currentStep--;
            }
        };

        $scope.next = function() {
            if ($scope.currentStep < 1) {
                $scope.currentStep = 1;
            }
        }

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);
}());