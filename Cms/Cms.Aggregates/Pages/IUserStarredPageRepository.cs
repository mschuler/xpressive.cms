namespace Cms.Aggregates.Pages
{
    public interface IUserStarredPageRepository : IUserStarredRepository<PageBase>
    {
    }
}