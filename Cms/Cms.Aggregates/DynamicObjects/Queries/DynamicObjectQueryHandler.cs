﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.SharedKernel;

namespace Cms.Aggregates.DynamicObjects.Queries
{
    internal class DynamicObjectQueryHandler : QueryHandlerBase
    {
        private readonly IDynamicObjectRepository _repository;
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;
        private readonly IDynamicObjectDtoService _dtoService;
        private readonly IObjectFieldContentTypeConverterService _contentTypeConverter;

        public DynamicObjectQueryHandler(IDynamicObjectRepository repository, IDynamicObjectDefinitionRepository definitionRepository, IDynamicObjectDtoService dtoService, IObjectFieldContentTypeConverterService contentTypeConverter)
        {
            _repository = repository;
            _definitionRepository = definitionRepository;
            _dtoService = dtoService;
            _contentTypeConverter = contentTypeConverter;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetAllObjects),
                typeof (GetObject),
                typeof (GetObjectsBySearchPattern)
            };
        }

        public IEnumerable<object> When(GetAllObjects query)
        {
            var definition = _definitionRepository.Get(query.ObjectDefinitionId);

            var objects = _repository
                .GetAll(query.TenantId, query.ObjectDefinitionId)
                .Where(o => IsMatchingFilter(query, o))
                .Select(o => _dtoService.GetDtoWithOnlySortableFields(o, definition, query.TenantId, _contentTypeConverter))
                .ToList();

            return objects;
        }

        public IEnumerable<object> When(GetObject query)
        {
            var obj = _repository.Get(query.Id);
            var definition = _definitionRepository.Get(obj.DefinitionId);
            yield return _dtoService.GetDtoWithFields(obj, definition, _contentTypeConverter);
        }

        public IEnumerable<object> When(GetObjectsBySearchPattern query)
        {
            var definitions = _definitionRepository
                .GetAll()
                .Where(d => IsVisibleForTenant(d, query.TenantId))
                .ToDictionary(d => d.Id);

            foreach (var o in _repository.GetAll())
            {
                DynamicObjectDefinition definition;
                if (definitions.TryGetValue(o.DefinitionId, out definition) && query.IsMatch(o, definition))
                {
                    yield return _dtoService.GetDtoWithoutFields(o, definition);
                }
            }
        }

        private bool IsVisibleForTenant(DynamicObjectDefinition definition, ulong tenantId)
        {
            if (definition.TenantId == tenantId)
            {
                return true;
            }

            return definition.SharedForTenants.Contains(tenantId);
        }

        private bool IsMatchingFilter(GetAllObjects query, DynamicObject obj)
        {
            if (query.Filters == null || query.Filters.Count == 0)
            {
                return true;
            }

            foreach (var filter in query.Filters)
            {
                if (!IsMatchingFilter(filter, obj))
                {
                    return false;
                }
            }

            return true;
        }

        private bool IsMatchingFilter(GetObjectFilter filter, DynamicObject obj)
        {
            var field = obj.Fields.SingleOrDefault(f => f.Name.Equals(filter.FieldName, StringComparison.Ordinal));

            if (field == null)
            {
                return false;
            }

            return IsMatchingFilter(filter, field);
        }

        private bool IsMatchingFilter(GetObjectFilter filter, DynamicObjectField field)
        {
            switch (field.ContentType.ToLowerInvariant())
            {
                case "text":
                case "html":
                case "markdown":
                case "code":
                    return IsMatchingText(filter.Operator, filter.Value, field.Value);
                case "date":
                    return IsMatchingDate(filter.Operator, filter.Value, field.Value);
                case "list":
                    return IsMatchingList(filter.Operator, filter.Value, field.Value);
                case "boolean":
                    return IsMatchingBool(filter.Operator, filter.Value, field.Value);
                case "number":
                    return IsMatchingNumber(filter.Operator, filter.Value, field.Value);
                case "object":
                case "file":
                case "form":
                default:
                    break;
            }

            return false;
        }

        private bool IsMatchingNumber(string @operator, string filterValue, string value)
        {
            decimal fv;
            decimal v;

            if (!decimal.TryParse(filterValue, out fv) || !decimal.TryParse(value, out v))
            {
                return false;
            }

            switch (@operator)
            {
                case "<":
                    return v < fv;
                case "<=":
                    return v <= fv;
                case ">":
                    return v > fv;
                case ">=":
                    return v >= fv;
                case "=":
                    return v == fv;
            }

            return false;
        }

        private bool IsMatchingBool(string @operator, string filterValue, string value)
        {
            if (!string.Equals(@operator, "=", StringComparison.Ordinal))
            {
                return false;
            }

            bool fv;
            bool v;

            if (!bool.TryParse(filterValue, out fv) || !bool.TryParse(value, out v))
            {
                return false;
            }

            return fv == v;
        }

        private bool IsMatchingText(string @operator, string filterValue, string value)
        {
            switch (@operator.ToLowerInvariant())
            {
                case "=":
                    return string.Equals(filterValue, value, StringComparison.OrdinalIgnoreCase);
                case "contains":
                    return value.ToLowerInvariant().Contains(filterValue.ToLowerInvariant());
            }

            return false;
        }

        private bool IsMatchingList(string @operator, string filterValue, string value)
        {
            if (@operator == "=")
            {
                return string.Equals(filterValue, value, StringComparison.Ordinal);
            }

            return false;
        }

        private bool IsMatchingDate(string @operator, string filterValue, string value)
        {
            DateTime filterDate;
            DateTime date;

            if (!DateTime.TryParse(filterValue, out filterDate) || !DateTime.TryParse(value, out date))
            {
                return false;
            }

            switch (@operator)
            {
                case "<":
                    return date < filterDate;
                case "<=":
                    return date <= filterDate;
                case ">":
                    return date > filterDate;
                case ">=":
                    return date >= filterDate;
                case "=":
                    return Math.Abs((filterDate - date).TotalMinutes) < 1;
            }

            return false;
        }
    }
}
