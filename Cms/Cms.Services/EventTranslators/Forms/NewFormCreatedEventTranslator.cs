using System;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Forms
{
    internal class NewFormCreatedEventTranslator : IEventTranslator
    {
        private readonly IFormRepository _repository;

        public NewFormCreatedEventTranslator(IFormRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(NewFormCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Neues Formular erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (NewFormCreated)@event;
            var name = string.Format("{0} (Nr. {1})", e.Name, e.PublicId);
            return string.Format("Das Formular <strong>{0}</strong> wurde neu erstellt.", name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "form";
        }
    }
}