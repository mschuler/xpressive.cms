﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("object", PermissionLevel.CanSee)]
    public class ShareDynamicObject : CommandBase
    {
        [Required]
        public ulong[] TenantIds { get; set; }
    }
}