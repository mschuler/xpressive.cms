﻿using Cms.Aggregates.Links;

namespace Cms.Aggregates
{
    public class Hyperlink : ILink
    {
        public string Url { get; set; }
    }
}