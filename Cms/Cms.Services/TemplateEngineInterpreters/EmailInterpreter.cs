﻿using System;
using System.Text;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class EmailInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "Email"; } }

        public void Calculate(TreeItem tree)
        {
            if (!tree.Value.Equals("email", StringComparison.OrdinalIgnoreCase) ||
                tree.Children == null ||
                tree.Children.Count < 1 ||
                tree.Children.Count > 2 ||
                !(tree.Children[0].Result is string))
            {
                return;
            }

            var email = (string)tree.Children[0].Result;
            var text = email;

            if (tree.Children.Count == 2 && tree.Children[1].Result is string)
            {
                text = (string)tree.Children[1].Result;
            }

            tree.Result = BuildLink(email, text);
        }

        internal string BuildLink(string email, string text)
        {
            var builder = new StringBuilder();
            var encoder = new UTF8Encoding();

            builder.Append("<a href=\"");

            email = email.Replace("@", "%40");
            email = email.Replace(".", "%2e");
            email = "mailto:" + email.ToLowerInvariant();

            foreach (var c in email)
            {
                Encode(Rot13(c), encoder, builder);
            }

            builder.Append("\" onclick=\"");
            builder.Append("if(this.href.slice(0,1)=='z')");
            builder.Append("this.href=this.href.replace(/[a-zA-Z]/g,function(c){");
            builder.Append("return String.fromCharCode((c<='Z'?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);");
            builder.Append("})\">");

            foreach (var c in text)
            {
                Encode(c, encoder, builder);
            }

            builder.Append("</a>");

            return builder.ToString();
        }

        internal void Encode(char c, UTF8Encoding encoder, StringBuilder builder)
        {
            builder.Append("&#x");

            foreach (var b in encoder.GetBytes(new[] { c }))
            {
                builder.AppendFormat("{0:x2}", b);
            }

            builder.Append(";");
        }

        internal char Rot13(char c)
        {
            const string alphabet = "abcdefghijklmnopqrstuvwxyz";
            var i = alphabet.IndexOf(c);

            if (i < 0)
            {
                return c;
            }

            i = (i + 13) % 26;
            return alphabet[i];
        }
    }
}
