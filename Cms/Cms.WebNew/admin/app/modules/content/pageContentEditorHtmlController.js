﻿(function() {
    angular.module("app").controller("PageContentEditorHtmlController", ["$scope", function ($scope) {
        if (!$scope.c || !$scope.c.value || !$scope.c.value.value || !_.isString($scope.c.value.value)) {
            $scope.c.value = { value: "" };
        }

        $scope.tinymceOptions = {
            theme: "modern",
            //relative_urls: true,
            remove_linebreaks: false,
            convert_urls: false,
            forced_root_blocks: false,
            allow_script_urls: true,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code hr wordcount textcolor colorpicker",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media"
        };
    }]);

    angular.module("app").controller("PageContentEditorCodeController", ["$scope", function($scope) {
        if (!$scope.c || !$scope.c.value || !$scope.c.value.value || !_.isString($scope.c.value.value)) {
            $scope.c.value = { value: "" };
        }

        $scope.codemirrorOptions = {
            lineNumbers: true,
            theme: 'neo',
            lineWrapping: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            matchTags: { bothTags: true },
            autoCloseTags: true,
            styleActiveLine: true,
            viewportMargin: Infinity,
            mode: "htmlmixed"
        };
    }]);
}());