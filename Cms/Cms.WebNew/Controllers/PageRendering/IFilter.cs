namespace Cms.WebNew.Controllers.PageRendering
{
    public interface IFilter<in T>
    {
        bool Execute(T msg);
    }
}