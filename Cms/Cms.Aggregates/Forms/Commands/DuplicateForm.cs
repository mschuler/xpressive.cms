﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Commands
{
    [RequiredPermission("form", PermissionLevel.CanChange)]
    public class DuplicateForm : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }

        [ValidId, Required]
        public ulong FormIdToDuplicate { get; set; }

        [Range(1, int.MaxValue)]
        public int PublicId { get; set; }
    }
}