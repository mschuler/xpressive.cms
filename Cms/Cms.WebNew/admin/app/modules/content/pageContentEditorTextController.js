﻿(function () {
    angular.module("app").controller("PageContentEditorTextController", ["$scope", "$log", function ($scope, $log) {
        $scope.preview = { html: "" };
        $scope.previous = null;

        if ($scope.c.value && $scope.c.value.value && _.isString($scope.c.value.value)) {
            $scope.preview.html = $scope.c.value;
        } else {
            $scope.c.value = { value: "" };
        }
    }]);
}());