﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("objectdefinition", PermissionLevel.CanSee)]

    public class UnstarDynamicObjectDefinition : CommandBase { }
}