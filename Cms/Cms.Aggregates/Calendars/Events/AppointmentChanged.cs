﻿using System.Collections.Generic;
using Cms.Aggregates.Links;

namespace Cms.Aggregates.Calendars.Events
{
    public class AppointmentChanged : EventBase<Calendar>
    {
        public ulong AppointmentId { get; set; }
        public string Title { get; set; }
        public bool IsHighlight { get; set; }
        public DateTimeRange StartEnd { get; set; }
        public string Location { get; set; }
        public ILink Link { get; set; }
        public AppointmentRecurrence Recurrence { get; set; }
        public IList<string> Categories { get; set; }
    }
}