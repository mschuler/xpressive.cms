﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("objectdefinition", PermissionLevel.CanChange)]
    public class CreateDynamicObjectDefinition : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }
    }
}
