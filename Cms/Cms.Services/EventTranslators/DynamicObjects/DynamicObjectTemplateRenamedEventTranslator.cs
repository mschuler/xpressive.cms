﻿using System;
using System.Globalization;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectTemplateRenamedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectTemplateRenamedEventTranslator(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectTemplateRenamed);
        }

        public string GetHeader(IEvent @event)
        {
            return "Objektvorlage umbenannt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectTemplateRenamed)@event;
            var d = _repository.GetEvenIfDeleted(@event.AggregateId);
            return string.Format(
                CultureInfo.InvariantCulture,
                "Die Objektvorlage <strong>{2}</strong> der Objektdefinition <strong>{0} (Nr. {1})</strong> wurde in <strong>{3}</strong> umbenannt.",
                d.Name,
                d.PublicId,
                e.OldName,
                e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objecttemplate";
        }
    }
}