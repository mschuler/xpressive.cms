using System.Collections.Generic;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Commands;
using Cms.Aggregates.Security.Queries;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class GroupController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _idProvider;
        private readonly IGroupRepository _repository;

        public GroupController(ICommandQueue commandQueue, IUserRepository userRepository, IUniqueIdProvider idProvider, IQueryHandler queryHandler, IGroupRepository repository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _idProvider = idProvider;
            _repository = repository;
        }

        [HttpGet, Route("api/groups/{tenantId:ulong}")]
        public IEnumerable<object> GetAll(ulong tenantId)
        {
            return ExecuteQuery(new GetAllGroups { TenantId = tenantId });
        }

        [HttpPost, Route("api/groups/{tenantId:ulong}")]
        public object Create(ulong tenantId, [FromBody]NameDto dto)
        {
            var id = _idProvider.GetId();

            SendCommand(new CreateGroup
            {
                AggregateId = id,
                TenantId = tenantId,
                Name = dto.Name,
            });

            return new
            {
                Id = id.ToJsonString(),
                Name = dto.Name,
            };
        }

        [HttpPost, Route("api/group/{groupId:ulong}/name")]
        public void Rename(ulong groupId, [FromBody]NameDto dto)
        {
            var tenantId = _repository.Get(groupId).TenantId;
            SendCommand(new ChangeGroupName
            {
                AggregateId = groupId,
                Name = dto.Name,
                TenantId = tenantId,
            });
        }

        [HttpDelete, Route("api/group/{groupId:ulong}")]
        public void Delete(ulong groupId)
        {
            var tenantId = _repository.Get(groupId).TenantId;
            SendCommand(new DeleteGroup
            {
                AggregateId = groupId,
                TenantId = tenantId,
            });
        }

        public class NameDto
        {
            public string Name { get; set; }
        }
    }
}