using System;
using Cms.Aggregates;
using Cms.Aggregates.Forms;
using Cms.SharedKernel;
using log4net;
using Newtonsoft.Json;

namespace Cms.Services.ObjectFieldContentTypeConverter
{
    internal class ObjectFieldFormConverter : IObjectFieldContentTypeConverter
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ObjectFieldFormConverter));
        private readonly IFormRepository _repository;

        public ObjectFieldFormConverter(IFormRepository repository)
        {
            _repository = repository;
        }

        public bool CanHandle(string type)
        {
            return type.Equals("form", StringComparison.OrdinalIgnoreCase);
        }

        public string Serialize(string json)
        {
            DtoWithId dto;

            try
            {
                dto = JsonConvert.DeserializeObject<DtoWithId>(json);
            }
            catch (Exception)
            {
                _log.ErrorFormat("Unable to deserialize to DtoWithId for json={0}", json);
                return "0";
            }

            ulong id;
            if (dto != null && ulong.TryParse(dto.Id, out id))
            {
                return dto.Id;
            }
            return "0";
        }

        public object Deserialize(string value)
        {
            var form = DeserializeToForm(value);
            if (form != null)
            {
                return form.ToDto();
            }
            return null;
        }

        public SortableObjectField DeserializeForSortableField(string value)
        {
            var form = DeserializeToForm(value);
            if (form != null)
            {
                var fullname = form.Name + " (Nr. " + form.PublicId + ")";
                return new SortableObjectField(fullname);
            }
            return new SortableObjectField();
        }

        public object DeserializeForPublicApi(string value)
        {
            var form = DeserializeToForm(value);
            if (form != null)
            {
                return form.ToDto();
            }
            return null;
        }

        public bool TryGetMetadataForPublicApi(string value, out object metadata)
        {
            metadata = null;
            return false;
        }

        public object DeserializeForInterpreter(string value)
        {
            return DeserializeToForm(value);
        }

        public string SerializeForImport(string json, string format, string options)
        {
            int publicId;
            if (int.TryParse(json, out publicId))
            {
                var form = _repository.GetByPublicId(publicId);
                if (form != null)
                {
                    return form.Id.ToJsonString();
                }
            }
            return "0";
        }

        public string SerializeForImportPreview(string json, string format, string options)
        {
            int publicId;
            if (int.TryParse(json, out publicId))
            {
                var form = _repository.GetByPublicId(publicId);
                if (form != null)
                {
                    return form.Name + " (Nr. " + form.PublicId + ")";
                }
            }
            return json;
        }

        public bool IsValidValueForImport(string json, string format, string options)
        {
            int publicId;
            return int.TryParse(json, out publicId) && _repository.GetByPublicId(publicId) != null;
        }

        private Form DeserializeToForm(string value)
        {
            ulong id;
            Form form;
            if (ulong.TryParse(value, out id) && _repository.TryGet(id, out form))
            {
                return form;
            }
            return null;
        }

        public class DtoWithId
        {
            public string Id { get; set; }
        }
    }
}