﻿using Cms.Aggregates.Pages;
using Cms.SharedKernel;
using Moq;
using Xunit;

namespace Cms.Aggregates.Tests
{
    public class Given_a_page_url_service
    {
        [Fact]
        public void When_the_page_does_not_exist_then_it_throws_an_exception()
        {
            var repositoryMock = new Mock<IPageRepository>();
            repositoryMock.Setup(s => s.Get(It.IsAny<ulong>())).Throws(new AggregateNotFoundException(123));
            var repository = repositoryMock.Object;
            var service = new PageUrlService(repository, null, null, null);

            Assert.Throws<AggregateNotFoundException>(() => service.GetUrl(123));
        }

        [Fact]
        public void When_the_page_has_no_parent_then_the_result_is_the_page_name()
        {
            PageBase page = new Page { Name = "single" };
            PageBase parent;
            var repositoryMock = new Mock<IPageRepository>();
            repositoryMock.Setup(s => s.Get(123)).Returns(page);
            repositoryMock.Setup(s => s.TryGet(0, out parent)).Returns(false);
            var repository = repositoryMock.Object;
            var service = new PageUrlService(repository, null, null, null);

            var result = service.GetUrl(123);

            Assert.Equal("/single", result);
        }

        [Fact]
        public void When_the_page_has_parents_then_the_result_has_the_correct_order()
        {
            PageBase first = new Page { Name = "first", ParentPageId = 0 };
            PageBase second = new Page { Name = "second", ParentPageId = 1 };
            PageBase third = new Page { Name = "third", ParentPageId = 2 };
            var repositoryMock = new Mock<IPageRepository>();
            repositoryMock.Setup(s => s.Get(123)).Returns(third);
            repositoryMock.Setup(s => s.TryGet(2, out second)).Returns(true);
            repositoryMock.Setup(s => s.TryGet(1, out first)).Returns(true);
            repositoryMock.Setup(s => s.TryGet(0, out first)).Returns(false);
            var repository = repositoryMock.Object;
            var service = new PageUrlService(repository, null, null, null);

            var result = service.GetUrl(123);

            Assert.Equal("/first/second/third", result);
        }

        [Fact]
        public void When_using_string_extensions_then_they_behave_correctly()
        {
            var given = "über uns";
            var expected = "ueber-uns";

            Assert.Equal(expected, given.ToValidUrlPart());
        }

        [Fact]
        public void When_using_string_extensions_for_file_names_then_they_behave_correctly()
        {
            var given = "2015-06-21 Kirche leben - In der Ergänzung.mp4";
            var expected = "2015-06-21-Kirche-leben-In-der-Ergaenzung.mp4";

            Assert.Equal(expected, given.ToValidFileName());
        }
    }
}
