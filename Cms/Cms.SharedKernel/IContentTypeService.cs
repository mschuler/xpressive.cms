﻿using System.Collections.Generic;

namespace Cms.SharedKernel
{
    public interface IContentTypeService
    {
        string GetContentType(string extension);

        bool IsTextFile(string extension);

        IEnumerable<string> GetContentTypesForPages();
    }
}
