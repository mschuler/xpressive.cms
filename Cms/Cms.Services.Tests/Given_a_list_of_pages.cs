﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Websites;
using Cms.Services.Contracts;
using Cms.Services.TemplateEngineInterpreters;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Interpreter;
using Moq;
using Xunit;

namespace Cms.Services.Tests
{
    public class Given_a_list_of_pages
    {
        private readonly Page _parentPage;

        public Given_a_list_of_pages()
        {
            var pageRepository = new Mock<IPageRepository>();
            var websiteRepository = new Mock<IWebsiteRepository>();
            var pageRenderingService = new Mock<IPageRenderingService>();
            var pages = new List<Page>();
            var websites = new List<Website>();
            pages.Add(new Page { PublicId = 1, Title = "Abc", IsEnabled = true, ParentPageId = 100 });
            pages.Add(new Page { PublicId = 2, Title = "A", IsEnabled = true, ParentPageId = 100 });
            pages.Add(new Page { PublicId = 3, Title = "a", IsEnabled = true, ParentPageId = 200 });
            pages.Add(new Page { PublicId = 4, Title = "bcd", IsEnabled = true, ParentPageId = 100 });
            pages.Add(new Page { PublicId = 5, Title = "Cde", IsEnabled = true, ParentPageId = 100 });
            pages.Add(new Page { PublicId = 100, Title = "Parent", IsEnabled = true, Id = 100 });
            websites.Add(new Website { Id = 1234567, PublicId = 1 });
            pages.ForEach(p => p.Name = p.Title);

            _parentPage = pages.Last();
            PageBase parent = _parentPage;

            pageRepository.Setup(s => s.GetAllByWebsiteId(1234567)).Returns(() => pages);
            pageRepository.Setup(m => m.TryGet(100, out parent)).Returns(true);
            websiteRepository.Setup(s => s.GetAll()).Returns(() => websites);
            pageRenderingService.Setup(m => m.Render(It.IsAny<Page>())).Returns((Page p) => p.Title);

            var pageRepositoryMock = pageRepository.Object;
            var websiteRepositoryMock = websiteRepository.Object;
            var pageRenderingServiceMock = pageRenderingService.Object;

            InterpreterFactory.Instance.Clear();
            InterpreterFactory.Instance.Register(new WhereListInterpreter());
            InterpreterFactory.Instance.Register(new SortListInterpreter());
            InterpreterFactory.Instance.Register(new PageListInterpreter(pageRepositoryMock, websiteRepositoryMock));
            InterpreterFactory.Instance.Register(new PageInterpreter(pageRepositoryMock, null, null, pageRenderingServiceMock, null, null, null, null, null));
        }

        [Fact]
        public void When_filtering_by_title_then_there_are_less_results()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).where('title', 'startswith', 'a')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(3, result.OfType<Page>().Count());
            Assert.True(result.Cast<Page>().All(p => p.Title.StartsWith("a", StringComparison.OrdinalIgnoreCase)));
        }

        [Fact]
        public void When_filtering_by_public_id_with_in_operator_then_there_are_less_results()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).where('id', 'in', '1,3,5')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(3, result.Count());
        }

        [Fact]
        public void When_filtering_by_public_id_with_between_operator_then_there_are_less_results()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).where('id', 'between', '2|4')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(3, result.Count());
        }

        [Fact]
        public void When_filtering_by_parent_id()
        {
            TemplateStateMap.Set(TemplateStateMap.CurrentObjectKey, _parentPage);
            var treeItem = new Engine().ParseTreeItem("{ pages(1).where(\"parent.id\", \"=\", page(this()).id).render() }");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as string;

            Assert.NotNull(result);
            Assert.Equal("AAbcbcdCde", result);
        }

        [Fact]
        public void When_filtering_by_public_id_with_greater_operator()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).where('id', '>', '4')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(2, result.Count());

            treeItem = new Engine().ParseTreeItem("pages(1).where('id', '>', '2')");
            result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(4, result.Count());
        }

        [Fact]
        public void When_filtering_by_public_id_with_greater_or_equal_operator()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).where('id', '>=', '4')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(3, result.Count());

            treeItem = new Engine().ParseTreeItem("pages(1).where('id', '>=', '2')");
            result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(5, result.Count());
        }

        [Fact]
        public void When_filtering_by_public_id_with_less_operator()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).where('id', '<', '4')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(3, result.Count());

            treeItem = new Engine().ParseTreeItem("pages(1).where('id', '<', '2')");
            result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(1, result.Count());
        }

        [Fact]
        public void When_filtering_by_public_id_with_less_or_equal_operator()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).where('id', '<=', '4')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(4, result.Count());

            treeItem = new Engine().ParseTreeItem("pages(1).where('id', '<=', '2')");
            result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(2, result.Count());
        }

        [Fact]
        public void When_sorting_and_filtering()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).sortDesc('id').where('id', 'between', '2|4')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(3, result.OfType<Page>().Count());
            Assert.Equal(4, ((Page)result[0]).PublicId);
            Assert.Equal(3, ((Page)result[1]).PublicId);
            Assert.Equal(2, ((Page)result[2]).PublicId);
        }

        [Fact]
        public void When_filtering_and_sorting()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).where('id', 'between', '2|4').sortDesc('title')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(3, result.OfType<Page>().Count());
            Assert.Equal("bcd", ((Page)result[0]).Title);
            Assert.Equal("A", ((Page)result[1]).Title);
            Assert.Equal("a", ((Page)result[2]).Title);
        }

        [Fact]
        public void When_sorted_by_title_then_the_sort_order_is_changed()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).sortAsc('title')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(6, result.Count());
            Assert.Equal("a", ((Page)result[0]).Title);
            Assert.Equal("Abc", ((Page)result[2]).Title);
            Assert.Equal("Cde", ((Page)result[4]).Title);
        }

        [Fact]
        public void When_sorted_by_title_descending_then_the_sort_order_is_changed()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).sortDesc('title')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(6, result.Count());
            Assert.Equal("Parent", ((Page)result[0]).Title);
            Assert.Equal("Cde", ((Page)result[1]).Title);
            Assert.Equal("Abc", ((Page)result[3]).Title);
            Assert.Equal("a", ((Page)result[5]).Title);
        }

        [Fact]
        public void When_filtered_with_regex_then_it_works()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).where('title', 'regex', '[b]+')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(2, result.Count());
        }

        [Fact]
        public void When_filtered_with_equal_then_it_works()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).where('id', '=', '1')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(1, result.Count());
        }

        [Fact]
        public void When_filtered_with_not_equal_then_it_works()
        {
            var treeItem = new Engine().ParseTreeItem("pages(1).where('id', '!=', '1')");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as IList<PageBase>;

            Assert.NotNull(result);
            Assert.Equal(5, result.Count());
        }
    }
}
