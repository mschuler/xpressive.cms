﻿namespace Cms.Aggregates.Security
{
    public enum PermissionLevel
    {
        Forbidden = 0,
        CanSee = 1,
        CanChange = 2,
        CanEverything = 3,
    }
}