﻿using Cms.TemplateEngine.Enums;
using Xunit;

namespace Cms.TemplateEngine.Tests
{
    public class EngineTests
    {
        private readonly Engine _engine = new Engine();

        [Fact]
        public void MediaCenterPopupWithImageLinkFileTag()
        {
            const string expression = "mediacenter(1).popup(file(517).imagetag)";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.NotNull(treeItem);
            Assert.Equal(TreeItemType.Expression, treeItem.ItemType);
            Assert.Equal(2, treeItem.Children.Count);
            Assert.Equal("mediacenter", treeItem.Children[0].Value);
            Assert.Equal("popup", treeItem.Children[1].Value);
            Assert.Equal(TreeItemType.Function, treeItem.Children[0].ItemType);
            Assert.Equal(TreeItemType.Function, treeItem.Children[1].ItemType);
            Assert.Equal(1, treeItem.Children[1].Children.Count);
            Assert.Equal(TreeItemType.Expression, treeItem.Children[1].Children[0].ItemType);
        }

        [Fact]
        public void IifWithConcatAndIsNull()
        {
            const string expression = "{iif(isnull($(\"Formular\")), \"true\",\"\", concat('<a title=\"jetzt anmelden\" class=\"flaticon-pencil8 formular\" href=\"#\" onclick=\"openFormular(', $(\"Formular\").id,')\">Anmeldung</a>'))}";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.NotNull(treeItem);
        }

        [Fact]
        public void IsNullTest()
        {
            Assert.Equal("True", _engine.Generate("{isnull('')}"));
            Assert.Equal("True", _engine.Generate("{isnull(0)}"));
            Assert.Equal("False", _engine.Generate("{isnull('a')}"));
            Assert.Equal("False", _engine.Generate("{isnull(5)}"));
        }

        [Fact]
        public void Vimeo()
        {
            const string expression = "vimeo(17495824, 900, 385, 'B40F23')";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.NotNull(treeItem);
            Assert.Equal(TreeItemType.Function, treeItem.ItemType);
            Assert.Equal(4, treeItem.Children.Count);
            Assert.Equal(TreeItemType.Parameter, treeItem.Children[0].ItemType);
            Assert.Equal(TreeItemType.Parameter, treeItem.Children[1].ItemType);
            Assert.Equal(TreeItemType.Parameter, treeItem.Children[2].ItemType);
            Assert.Equal(TreeItemType.Parameter, treeItem.Children[3].ItemType);
            Assert.Equal(ParameterType.Integer, treeItem.Children[0].ParameterType);
            Assert.Equal(ParameterType.Integer, treeItem.Children[1].ParameterType);
            Assert.Equal(ParameterType.Integer, treeItem.Children[2].ParameterType);
            Assert.Equal(ParameterType.String, treeItem.Children[3].ParameterType);
            Assert.Equal(17495824, treeItem.Children[0].Result);
            Assert.Equal(900, treeItem.Children[1].Result);
            Assert.Equal(385, treeItem.Children[2].Result);
            Assert.Equal("B40F23", treeItem.Children[3].Result);
        }

        [Fact]
        public void ThisPageTitle()
        {
            const string expression = "page(this()).title";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.Equal(TreeItemType.Expression, treeItem.ItemType);
            Assert.Equal(2, treeItem.Children.Count);
            Assert.Equal(TreeItemType.Function, treeItem.Children[0].ItemType);
            Assert.Equal(TreeItemType.Attribute, treeItem.Children[1].ItemType);
            Assert.Equal(1, treeItem.Children[0].Children.Count);
            Assert.Equal(TreeItemType.Function, treeItem.Children[0].Children[0].ItemType);
        }

        [Fact]
        public void CalendarListWithCategories()
        {
            const string expression = "calendar().list(47)";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.NotNull(treeItem);
            Assert.Equal(TreeItemType.Expression, treeItem.ItemType);
            Assert.Equal(2, treeItem.Children.Count);
            Assert.Equal(TreeItemType.Function, treeItem.Children[0].ItemType);
            Assert.Equal(TreeItemType.Function, treeItem.Children[1].ItemType);
            Assert.Equal(0, treeItem.Children[0].Children.Count);
            Assert.Equal(1, treeItem.Children[1].Children.Count);
            Assert.Equal(47, treeItem.Children[1].Children[0].Result);
        }

        [Fact]
        public void ArticleById()
        {
            const string expression = "article(13)";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.NotNull(treeItem);
            Assert.Equal(TreeItemType.Function, treeItem.ItemType);
            Assert.Equal(1, treeItem.Children.Count);
            Assert.Equal(13, treeItem.Children[0].Result);
        }

        [Fact]
        public void Text2ImageTest()
        {
            const string expression = "text2image('cms.home', '<El&Font! Brush>', 8, 'Black')";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.NotNull(treeItem);
            Assert.Equal(TreeItemType.Function, treeItem.ItemType);
            Assert.Equal(4, treeItem.Children.Count);

            Assert.Equal(TreeItemType.Parameter, treeItem.Children[0].ItemType);
            Assert.Equal(ParameterType.String, treeItem.Children[0].ParameterType);
            Assert.Equal("cms.home", treeItem.Children[0].Result);

            Assert.Equal(TreeItemType.Parameter, treeItem.Children[1].ItemType);
            Assert.Equal(ParameterType.String, treeItem.Children[1].ParameterType);
            Assert.Equal("<El&Font! Brush>", treeItem.Children[1].Result);

            Assert.Equal(TreeItemType.Parameter, treeItem.Children[2].ItemType);
            Assert.Equal(ParameterType.Integer, treeItem.Children[2].ParameterType);
            Assert.Equal(8, treeItem.Children[2].Result);

            Assert.Equal(TreeItemType.Parameter, treeItem.Children[3].ItemType);
            Assert.Equal(ParameterType.String, treeItem.Children[3].ParameterType);
            Assert.Equal("Black", treeItem.Children[3].Result);
        }

        [Fact]
        public void Text2ImageTest2()
        {
            const string expression = "{text2image('cms_referenzen', 'Segoe WP Semibold', 9, 'Black')}";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.NotNull(treeItem);
            Assert.Equal(TreeItemType.Function, treeItem.ItemType);
            Assert.Equal(4, treeItem.Children.Count);

            Assert.Equal(TreeItemType.Parameter, treeItem.Children[0].ItemType);
            Assert.Equal(ParameterType.String, treeItem.Children[0].ParameterType);
            Assert.Equal("cms_referenzen", treeItem.Children[0].Result);

            Assert.Equal(TreeItemType.Parameter, treeItem.Children[1].ItemType);
            Assert.Equal(ParameterType.String, treeItem.Children[1].ParameterType);
            Assert.Equal("Segoe WP Semibold", treeItem.Children[1].Result);

            Assert.Equal(TreeItemType.Parameter, treeItem.Children[2].ItemType);
            Assert.Equal(ParameterType.Integer, treeItem.Children[2].ParameterType);
            Assert.Equal(9, treeItem.Children[2].Result);

            Assert.Equal(TreeItemType.Parameter, treeItem.Children[3].ItemType);
            Assert.Equal(ParameterType.String, treeItem.Children[3].ParameterType);
            Assert.Equal("Black", treeItem.Children[3].Result);
        }

        [Fact]
        public void Text2ImageTest3()
        {
            const string expression = "{text2image(page(this()).title, 'Segoe WP Semibold', 9, 'Black')}";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.NotNull(treeItem);
            Assert.Equal(TreeItemType.Function, treeItem.ItemType);
            Assert.Equal(4, treeItem.Children.Count);

            Assert.Equal(TreeItemType.Expression, treeItem.Children[0].ItemType);
            //Assert.Equal(ParameterType.String, treeItem.Children[0].ParameterType);
            Assert.Equal(null, treeItem.Children[0].Result);

            Assert.Equal(TreeItemType.Parameter, treeItem.Children[1].ItemType);
            Assert.Equal(ParameterType.String, treeItem.Children[1].ParameterType);
            Assert.Equal("Segoe WP Semibold", treeItem.Children[1].Result);

            Assert.Equal(TreeItemType.Parameter, treeItem.Children[2].ItemType);
            Assert.Equal(ParameterType.Integer, treeItem.Children[2].ParameterType);
            Assert.Equal(9, treeItem.Children[2].Result);

            Assert.Equal(TreeItemType.Parameter, treeItem.Children[3].ItemType);
            Assert.Equal(ParameterType.String, treeItem.Children[3].ParameterType);
            Assert.Equal("Black", treeItem.Children[3].Result);
        }

        [Fact]
        public void IifWithEscapingCharacterTest()
        {
            const string expression = "iif(object(this()).field('IsSold'), 'true', 'style=\"color:gray;\"', '')";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.NotNull(treeItem);
            Assert.Equal(4, treeItem.Children.Count);
            Assert.Equal("style=\"color:gray;\"", treeItem.Children[2].Result);
        }

        [Fact]
        public void IifWithExpressionsAsChildrenTest()
        {
            const string expression = "iif(object(param('id')).field('OpenAmount'), 0, '', object(param('id')).withTemplate(2)";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.NotNull(treeItem);
            Assert.Equal(TreeItemType.Function, treeItem.ItemType);
            Assert.Equal(4, treeItem.Children.Count);
            Assert.Equal("iif", treeItem.Value);
            Assert.Equal(TreeItemType.Expression, treeItem.Children[0].ItemType);
            Assert.Equal(TreeItemType.Parameter, treeItem.Children[1].ItemType);
            Assert.Equal(TreeItemType.Parameter, treeItem.Children[2].ItemType);
            Assert.Equal(TreeItemType.Expression, treeItem.Children[3].ItemType);
            Assert.Equal(null, treeItem.Children[0].Value);
            Assert.Equal("0", treeItem.Children[1].Value);
            Assert.Equal(0, treeItem.Children[1].Result);
            Assert.Equal("''", treeItem.Children[2].Value);
            Assert.Equal(string.Empty, treeItem.Children[2].Result);
            Assert.Equal(null, treeItem.Children[3].Value);
            Assert.Equal(2, treeItem.Children[0].Children.Count);
            Assert.Equal(2, treeItem.Children[3].Children.Count);
            Assert.Equal("object", treeItem.Children[0].Children[0].Value);
            Assert.Equal("field", treeItem.Children[0].Children[1].Value);
            Assert.Equal("object", treeItem.Children[3].Children[0].Value);
            Assert.Equal("withTemplate", treeItem.Children[3].Children[1].Value);
        }

        [Fact]
        public void ObjectTest()
        {
            const string expression1 = "object(param('id')).field('OpenAmount')";
            const string expression2 = "object(param('id')).withTemplate(2)";

            var ti1 = _engine.ParseTreeItem(expression1);
            var ti2 = _engine.ParseTreeItem(expression2);

            Assert.Equal(2, ti1.Children.Count);
            Assert.Equal(TreeItemType.Expression, ti1.ItemType);
            Assert.Equal("object", ti1.Children[0].Value);
            Assert.Equal("field", ti1.Children[1].Value);
            Assert.Equal(2, ti2.Children.Count);
            Assert.Equal(TreeItemType.Expression, ti2.ItemType);
            Assert.Equal("object", ti2.Children[0].Value);
            Assert.Equal("withTemplate", ti2.Children[1].Value);
        }

        [Fact]
        public void ExcapedCharacterTest()
        {
            const string expression = @"article('NameW\'Char')";
            var treeItem = _engine.ParseTreeItem(expression);

            Assert.NotNull(treeItem);
            Assert.Equal(TreeItemType.Function, treeItem.ItemType);
            Assert.Equal(1, treeItem.Children.Count);
            Assert.Equal("NameW'Char", treeItem.Children[0].Result);
        }

        [Fact]
        public void ExpressionExtractorWithMultiLine()
        {
            var text = @"
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' >
  <head>
    <title>{website(this()).name} - {page(this()).title}</title>
    <link rel='shortcut icon' href='/favicon.ico' type='image/x-icon'>
    <link href='{file(5495).url}' type='text/css' rel='stylesheet' />
    <link href='{file(5496).url}' type='text/css' rel='stylesheet' />
    <link href='{file(5497).url}' type='text/css' rel='stylesheet' />
    <link href='{
    file(5498).url
    }' type='text/css' rel='stylesheet' />
    
    <script type='text/javascript'>
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-19082102-1']);
        _gaq.push(['_trackPageview']);
        function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        };
    </script>";

            var extractor = new ExpressionExtractor();
            var expressions = extractor.GetAllExpressions(text);

            Assert.Equal(5, expressions.Count);
            Assert.Equal("{website(this()).name}", expressions[0].FullName);
            Assert.Equal("{page(this()).title}", expressions[1].FullName);
            Assert.Equal("{file(5495).url}", expressions[2].FullName);
            Assert.Equal("{file(5496).url}", expressions[3].FullName);
            Assert.Equal("{file(5497).url}", expressions[4].FullName);
        }

        [Fact]
        public void GetContentFromWebsiteDefaultPageViaId()
        {
            var treeItem = _engine.ParseTreeItem("page(website(this()).DefaultPage.id).content('areaName')");

            Assert.NotNull(treeItem);
            Assert.Equal(TreeItemType.Expression, treeItem.ItemType);
            Assert.Equal(2, treeItem.Children.Count);
            Assert.Equal("page", treeItem.Children[0].Value);
            Assert.Equal("content", treeItem.Children[1].Value);
            Assert.Equal(TreeItemType.Function, treeItem.Children[0].ItemType);
            Assert.Equal(TreeItemType.Function, treeItem.Children[1].ItemType);
            Assert.Equal(1, treeItem.Children[0].Children.Count);
            Assert.Equal(TreeItemType.Expression, treeItem.Children[0].Children[0].ItemType);
            Assert.Equal(3, treeItem.Children[0].Children[0].Children.Count);
            Assert.Equal(TreeItemType.Function, treeItem.Children[0].Children[0].Children[0].ItemType);
            Assert.Equal(TreeItemType.Attribute, treeItem.Children[0].Children[0].Children[1].ItemType);
            Assert.Equal(TreeItemType.Attribute, treeItem.Children[0].Children[0].Children[2].ItemType);
            Assert.Equal("website", treeItem.Children[0].Children[0].Children[0].Value);
            Assert.Equal("DefaultPage", treeItem.Children[0].Children[0].Children[1].Value);
            Assert.Equal("id", treeItem.Children[0].Children[0].Children[2].Value);
            Assert.Equal(1, treeItem.Children[0].Children[0].Children[0].Children.Count);
            Assert.Equal(TreeItemType.Function, treeItem.Children[0].Children[0].Children[0].Children[0].ItemType);
            Assert.Equal("this", treeItem.Children[0].Children[0].Children[0].Children[0].Value);
            Assert.Equal(1, treeItem.Children[1].Children.Count);
            Assert.Equal("areaName", treeItem.Children[1].Children[0].Result);
        }

        [Fact]
        public void WebsiteDefaultAsParameter()
        {
            const string expression = "iif(isnull(website(this()).defaultpage.field('Logo ersetzen')), 'True', file(147).url, website(this()).defaultpage.field('Logo ersetzen').url)";
            var iif = Parser.Parse(expression);

            Assert.NotNull(iif);
            Assert.Equal(4, iif.Children.Count);

            var isnull = iif.Children[0];
            var file = iif.Children[2];
            var exp = iif.Children[3];

            Assert.Equal("isnull", isnull.Value);
            Assert.Equal(1, isnull.Children.Count);
            Assert.Equal(3, isnull.Children[0].Children.Count);
            Assert.Equal("website", isnull.Children[0].Children[0].Value);
            Assert.Equal("defaultpage", isnull.Children[0].Children[1].Value);
            Assert.Equal("field", isnull.Children[0].Children[2].Value);

            Assert.Equal(2, file.Children.Count);
            Assert.Equal("file", file.Children[0].Value);
            Assert.Equal("url", file.Children[1].Value);

            Assert.Equal(4, exp.Children.Count);
            Assert.Equal("website", exp.Children[0].Value);
            Assert.Equal("defaultpage", exp.Children[1].Value);
            Assert.Equal("field", exp.Children[2].Value);
            Assert.Equal("url", exp.Children[3].Value);
        }

        [Fact]
        public void HugeChainWithWheres()
        {
            const string expression = "objects(3).where('field(\"Eventstart\").format(\"yyyyMMdd\")','<',now().format(\"yyyyMMdd\")).where(\"field('Vimeo-ID')\", \"isnull\", \"false\").where(\"field('Vimeo-ID')\", \" != \", \"\").union(objects(3).where('field(\"Eventstart\").format(\"yyyyMMdd\")','<',now().format(\"yyyyMMdd\")).where(\"field('Videodatei')\", \"isnull\", \"false\")).distinct().template(4)";
            var chain = Parser.Parse(expression);

            Assert.NotNull(chain);
            Assert.Equal(7, chain.Children.Count);

            var list = chain.Children[0];
            var where1 = chain.Children[1];
            var where2 = chain.Children[2];
            var where3 = chain.Children[3];
            var union = chain.Children[4];
            var distinct = chain.Children[5];
            var template = chain.Children[6];

            Assert.Equal("objects", list.Value);
            Assert.Equal(1, list.Children.Count);

            Assert.Equal("where", where1.Value);
            Assert.Equal(3, where1.Children.Count);

            Assert.Equal("where", where2.Value);
            Assert.Equal(3, where2.Children.Count);

            Assert.Equal("where", where3.Value);
            Assert.Equal(3, where3.Children.Count);

            Assert.Equal("union", union.Value);
            Assert.Equal(1, union.Children.Count);
            Assert.Equal(3, union.Children[0].Children.Count);

            Assert.Equal("distinct", distinct.Value);
            Assert.Equal(0, distinct.Children.Count);

            Assert.Equal("template", template.Value);
            Assert.Equal(1, template.Children.Count);
        }
    }
}
