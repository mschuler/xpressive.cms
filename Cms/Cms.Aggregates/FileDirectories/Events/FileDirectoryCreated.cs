﻿namespace Cms.Aggregates.FileDirectories.Events
{
    public class FileDirectoryCreated : EventBase<FileDirectory>
    {
        public ulong TenantId { get; set; }
        public ulong FileStoreId { get; set; }
        public string Name { get; set; }
        public ulong ParentId { get; set; }
    }
}