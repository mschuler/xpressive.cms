using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cms.Services.Contracts;

namespace Cms.Services.FormGenerator
{
    internal class FormFieldListRenderer : IFormFieldRenderer
    {
        private readonly string _name;
        private readonly bool _isRequired;
        private readonly IList<string> _options;
        private readonly bool _isMultiple;

        public FormFieldListRenderer(string name, bool isRequired, IEnumerable<string> options, bool isMultiple)
        {
            _name = name;
            _isRequired = isRequired;
            _options = options.ToList();
            _isMultiple = isMultiple;
        }

        public void Render(FormLayoutType type, StringBuilder builder, string id)
        {
            var multiple = _isMultiple ? " multiple" : string.Empty;
            var required = _isRequired ? "required " : string.Empty;
            var size = _isMultiple ? string.Format(" size=\"{0}\"", Math.Min(_options.Count, 4)) : string.Empty;

            if (type == FormLayoutType.Above)
            {
                builder.AppendLine("  <div class=\"form-group\">");
                builder.AppendFormat("    <label for=\"{0}\">{1}</label>", id, _name);
                builder.AppendLine();
                builder.AppendFormat("    <select id=\"{0}\"{2} class=\"form-control\" name=\"{1}\" {3}{4}>", id, _name, multiple, required, size);
                builder.AppendLine();

                foreach (var option in _options)
                {
                    builder.AppendFormat("      <option value=\"{0}\">{0}</option>", option);
                    builder.AppendLine();
                }

                builder.AppendLine("    </select>");
                builder.AppendLine("  </div>");
            }
            else if (type == FormLayoutType.Asside)
            {
                builder.AppendLine("  <div class=\"form-group\">");
                builder.AppendFormat("    <label for=\"{0}\" class=\"col-sm-2 control-label\">{1}</label>", id, _name);
                builder.AppendLine();
                builder.AppendLine("    <div class=\"col-sm-10\">");
                builder.AppendFormat("      <select id=\"{0}\"{2} class=\"form-control\" name=\"{1}\" {3}{4}>", id, _name, multiple, required, size);
                builder.AppendLine();

                foreach (var option in _options)
                {
                    builder.AppendFormat("        <option value=\"{0}\">{0}</option>", option);
                    builder.AppendLine();
                }

                builder.AppendLine("      </select>");
                builder.AppendLine("    </div>");
                builder.AppendLine("  </div>");
            }
            else if (type == FormLayoutType.NoLabel)
            {
                builder.AppendFormat("  <select id=\"{0}\"{2} class=\"form-control\" name=\"{1}\" {3}{4}>", id, _name, multiple, required, size);
                builder.AppendLine();

                foreach (var option in _options)
                {
                    builder.AppendFormat("    <option value=\"{0}\">{0}</option>", option);
                    builder.AppendLine();
                }

                builder.AppendLine("  </select>");
            }
            else
            {
                throw new NotSupportedException(type.ToString());
            }
        }
    }
}