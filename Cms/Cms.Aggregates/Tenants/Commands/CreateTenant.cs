using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Tenants.Commands
{
    [RequiredPermission("sysadmin", PermissionLevel.CanEverything)]
    public class CreateTenant : CommandBase
    {
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
    }
}