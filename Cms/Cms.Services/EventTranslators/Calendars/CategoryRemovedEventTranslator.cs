﻿using System;
using Cms.Aggregates.Calendars;
using Cms.Aggregates.Calendars.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Calendars
{
    internal class CategoryRemovedEventTranslator : IEventTranslator
    {
        private readonly ICalendarRepository _repository;

        public CategoryRemovedEventTranslator(ICalendarRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(CategoryRemoved);
        }

        public string GetHeader(IEvent @event)
        {
            return "Kalenderkategorie gelöscht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (CategoryRemoved)@event;
            return string.Format("Die Kalenderkategorie <strong>{0}</strong> wurde gelöscht.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var calendar = _repository.GetEvenIfDeleted(@event.AggregateId);
            return calendar.TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "calendarcategory";
        }
    }
}