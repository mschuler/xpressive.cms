﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Cms.Aggregates.Security
{
    public class User : AggregateBase
    {
        private static readonly User _systemUser;
        private static readonly User _guestUser;

        static User()
        {
            _systemUser = new User
            {
                Id = 100,
                EmailAddress = "system.cms@xpressive.ch",
                FirstName = string.Empty,
                LastName = "System",
                IsSysadmin = true,
                Password = "$2a$10$klVRh9mvSHjubDxxEODy7uTbTbdO.hNvJYI5wTjlUGRAWK.Pmibny",
            };
            _guestUser = new User
            {
                Id = 101,
                EmailAddress = "guest.cms@xpressive.ch",
                FirstName = string.Empty,
                LastName = "Guest",
                IsSysadmin = false,
                Password = "$2a$10$8O1tS/lDimNH8LvpOFetg.Yrnsk5Ky6i4fyknibqvdHE1pKCqiXxa",
            };
        }

        public static User System { get { return _systemUser; } }
        public static User Guest { get { return _guestUser; } }

        private readonly object _groupIdsLock = new object();
        private readonly IList<ulong> _groupIds;
        private readonly object _u2fDevicesLock = new object();
        private readonly IList<U2FDevice> _u2fDevices;
        private byte[] _totpSecret = new byte[0];

        public User()
        {
            _groupIds = new List<ulong>();
            _u2fDevices = new List<U2FDevice>();
        }

        public string EmailAddress { get; internal set; }
        public string FirstName { get; internal set; }
        public string LastName { get; internal set; }
        public bool IsSysadmin { get; internal set; }
        public string Password { get; internal set; }
        public bool IsLocked { get; internal set; }
        public DateTime RecentActivity { get; set; }

        public IEnumerable<ulong> GroupIds
        {
            get
            {
                lock (_groupIdsLock)
                {
                    return _groupIds.ToList().AsReadOnly();
                }
            }
        }

        public IEnumerable<U2FDevice> U2FDevices
        {
            get
            {
                lock (_u2fDevicesLock)
                {
                    return _u2fDevices.ToList().AsReadOnly();
                }
            }
        }

        public string FullName
        {
            get
            {
                return string.Format(CultureInfo.InvariantCulture, "{0} {1}", FirstName, LastName).Trim();
            }
        }

        public bool IsTotpEnabled
        {
            get { return _totpSecret != null && _totpSecret.Length > 0; }
        }

        public bool IsU2FEnabled
        {
            get { return _u2fDevices.Any(); }
        }

        public byte[] GetTotpSecret()
        {
            return _totpSecret;
        }

        public void EnableTotp(byte[] totpSecret)
        {
            _totpSecret = totpSecret;
        }

        public void DisableTotp()
        {
            _totpSecret = new byte[0];
        }

        public U2FDevice AddU2FDevice(byte[] publicKey, byte[] attestationCert, byte[] keyHandle)
        {
            lock (_u2fDevicesLock)
            {
                var device = new U2FDevice
                {
                    PublicKey = publicKey,
                    AttestationCert = attestationCert,
                    KeyHandle = keyHandle,
                    Counter = 0,
                    IsCompromised = false
                };
                _u2fDevices.Add(device);
                return device;
            }
        }

        public void RemoveU2FDevice(U2FDevice device)
        {
            lock (_u2fDevicesLock)
            {
                _u2fDevices.Remove(device);
            }
        }

        public bool HasPermission(string moduleId, ulong tenantId, PermissionLevel permissionLevel, IGroupRepository groupRepository)
        {
            if (IsSysadmin)
            {
                return true;
            }

            var groups = GroupIds
                .Select(groupRepository.Get)
                .Where(g => g.TenantId == tenantId)
                .ToList();

            foreach (var group in groups)
            {
                if (group.HasPermission(moduleId, permissionLevel))
                {
                    return true;
                }
            }

            return false;
        }

        public IEnumerable<Permission> GetPermissions(ulong tenantId, IGroupRepository groupRepository)
        {
            var groups = GroupIds
                .Select(groupRepository.Get)
                .Where(g => g.TenantId == tenantId)
                .ToList();

            var permissions = new Dictionary<string, Permission>(StringComparer.OrdinalIgnoreCase);

            foreach (var group in groups)
            {
                foreach (var permission in group.Permissions)
                {
                    Permission currentPermission;
                    if (permissions.TryGetValue(permission.ModuleId, out currentPermission) &&
                        currentPermission.PermissionLevel >= permission.PermissionLevel)
                    {
                        continue;
                    }

                    permissions[permission.ModuleId] = permission;
                }
            }

            return permissions.Values;
        }

        public override string ToString()
        {
            return FullName;
        }

        public object ToDto(IList<string> groups = null)
        {
            if (IsSysadmin)
            {
                return new
                {
                    Id = Id.ToJsonString(),
                    FirstName,
                    LastName,
                    Name = string.Format("{0} {1}", FirstName, LastName).Trim(),
                    Email = EmailAddress,
                    UiLanguage = "de",
                    IsLocked,
                    IsTotp = IsTotpEnabled,
                    IsU2FEnabled,
                    RecentActivity,
                    IsSysadmin,
                    Groups = groups,
                };
            }

            return new
            {
                Id = Id.ToJsonString(),
                FirstName,
                LastName,
                Name = string.Format("{0} {1}", FirstName, LastName).Trim(),
                Email = EmailAddress,
                UiLanguage = "de",
                IsLocked,
                IsTotp = IsTotpEnabled,
                RecentActivity,
                Groups = groups,
            };
        }

        internal void AddGroupId(ulong groupId)
        {
            lock (_groupIdsLock)
            {
                _groupIds.Add(groupId);
            }
        }

        internal void RemoveGroupId(ulong groupId)
        {
            lock (_groupIdsLock)
            {
                _groupIds.Remove(groupId);
            }
        }
    }

    public class U2FDevice
    {
        public byte[] KeyHandle { get; internal set; }

        public byte[] PublicKey { get; internal set; }

        public byte[] AttestationCert { get; internal set; }

        public int Counter { get; set; }

        public bool IsCompromised { get; set; }

        protected bool Equals(U2FDevice other)
        {
            return PublicKey.SequenceEqual(other.PublicKey);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }
            if (obj.GetType() != GetType()) { return false; }
            return Equals((U2FDevice)obj);
        }

        public override int GetHashCode()
        {
            return (PublicKey != null ? PublicKey.GetHashCode() : 0);
        }
    }
}