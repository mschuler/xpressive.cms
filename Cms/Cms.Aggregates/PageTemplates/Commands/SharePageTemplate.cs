using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.PageTemplates.Commands
{
    [RequiredPermission("pagetemplate", PermissionLevel.CanEverything)]
    public class SharePageTemplate : CommandBase
    {
        [Required]
        public ulong[] TenantIds { get; set; }
    }
}