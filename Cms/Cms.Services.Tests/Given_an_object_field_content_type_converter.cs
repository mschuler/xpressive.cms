﻿using System;
using System.Linq;
using System.Reflection;
using Cms.SharedKernel;
using Xunit;

namespace Cms.Services.Tests
{
    public class Given_an_object_field_content_type_converter
    {
        [Fact]
        public void When_there_is_an_invalid_value_then_there_is_no_exception()
        {
            var converterTypes = Assembly.Load("Cms.Services")
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(IObjectFieldContentTypeConverter)))
                .ToList();

            foreach (var converterType in converterTypes)
            {
                var converter = (IObjectFieldContentTypeConverter)Activator.CreateInstance(converterType, new object[converterType.GetConstructors().First().GetParameters().Length]);

                converter.Serialize(null);
                converter.Serialize("");
                converter.Serialize("123");
                converter.Serialize("asdf");
                converter.Serialize("{id=\"asdf\"}");
                converter.Serialize("{asdf}");
            }
        }

        [Fact]
        public void Then_there_are_converters()
        {
            var converterTypes = Assembly.Load("Cms.Services")
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(IObjectFieldContentTypeConverter)))
                .ToList();

            Assert.NotEmpty(converterTypes);
        }
    }
}
