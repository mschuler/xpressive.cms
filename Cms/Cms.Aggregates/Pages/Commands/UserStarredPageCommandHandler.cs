using System;
using Cms.Aggregates.Pages.Events;
using Cms.EventSourcing.Contracts.Events;

namespace Cms.Aggregates.Pages.Commands
{
    public class UserStarredPageCommandHandler : CommandHandlerBase
    {
        private readonly IPageRepository _repository;
        private readonly IUserStarredPageRepository _starredRepository;

        public UserStarredPageCommandHandler(IPageRepository repository, IUserStarredPageRepository starredRepository)
        {
            _repository = repository;
            _starredRepository = starredRepository;
        }

        public void When(StarPage command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            if (_starredRepository.IsStarred(command.UserId, command.AggregateId))
            {
                return;
            }

            var e = new PageStarred
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            };

            eventHandler(e);
        }

        public void When(UnstarPage command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            if (!_starredRepository.IsStarred(command.UserId, command.AggregateId))
            {
                return;
            }

            var e = new PageUnstarred
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            };

            eventHandler(e);
        }
    }
}