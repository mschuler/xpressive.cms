using System;
using Cms.Aggregates.Calendars;
using Cms.Aggregates.Calendars.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Calendars
{
    internal class AppointmentAddedEventTranslator : IEventTranslator
    {
        private readonly ICalendarRepository _repository;

        public AppointmentAddedEventTranslator(ICalendarRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(AppointmentAdded);
        }

        public string GetHeader(IEvent @event)
        {
            return "Neuer Termin erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (AppointmentAdded)@event;
            return string.Format("Der Termin <strong>{0}</strong> f�r am <strong>{1:f}</strong> wurde erstellt.", e.Title, e.StartEnd.Start);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var calendar = _repository.GetEvenIfDeleted(@event.AggregateId);
            return calendar.TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "appointment";
        }
    }
}