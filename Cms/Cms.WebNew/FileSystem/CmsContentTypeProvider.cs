﻿using Cms.SharedKernel;
using Microsoft.Owin.StaticFiles.ContentTypes;

namespace Cms.WebNew.FileSystem
{
    public class CmsContentTypeProvider : IContentTypeProvider
    {
        private readonly FileExtensionContentTypeProvider _internal = new FileExtensionContentTypeProvider();
        private readonly IContentTypeService _contentTypeService;

        public CmsContentTypeProvider(IContentTypeService contentTypeService)
        {
            _contentTypeService = contentTypeService;
        }

        public bool TryGetContentType(string subpath, out string contentType)
        {
            if (_internal.TryGetContentType(subpath, out contentType))
            {
                return true;
            }

            contentType = _contentTypeService.GetContentType(subpath.GetExtension());
            return true;
        }
    }
}