using Cms.Aggregates.Security;

namespace Cms.Aggregates.PageTemplates.Commands
{
    [RequiredPermission("pagetemplate", PermissionLevel.CanChange)]
    public class ChangePageTemplateIcon : CommandBase
    {
        public ulong IconFileId { get; set; }
    }
}