namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("group", PermissionLevel.CanChange)]
    public class AddGroupMember : CommandBase
    {
        [ValidId]
        public ulong MemberId { get; set; }
    }
}