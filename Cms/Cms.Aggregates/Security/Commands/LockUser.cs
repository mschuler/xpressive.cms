namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("user", PermissionLevel.CanEverything)]
    public class LockUser : CommandBase { }
}