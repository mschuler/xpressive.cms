﻿using Cms.Aggregates.Security;

namespace Cms.Services.Contracts
{
    public interface IU2FService
    {
        IDeviceRegistration StartRegistration(ulong userId, string appId);
        bool TryFinishRegistration(ulong userId, string registerResponse, out IDevice device);
        IDeviceVerification StartVerification(User user, string appId);
        bool TryFinishVerification(User user, string authenticateResponse);
    }

    public interface IDeviceVerification
    {
        string AppId { get; }
        string Challenge { get; }
        string KeyHandle { get; }
        string Version { get; }
    }

    public interface IDeviceRegistration
    {
        string AppId { get; }
        string Challenge { get; }
        string Version { get; }
    }

    public interface IDevice
    {
        byte[] AttestationCert { get; }
        int Counter { get; }
        byte[] KeyHandle { get; }
        byte[] PublicKey { get; }
    }
}