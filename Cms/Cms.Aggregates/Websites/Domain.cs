﻿using System;

namespace Cms.Aggregates.Websites
{
    public class Domain
    {
        public string Host { get; set; }

        protected bool Equals(Domain other)
        {
            return string.Equals(Host, other.Host, StringComparison.OrdinalIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }
            if (obj.GetType() != GetType()) { return false; }
            return Equals((Domain)obj);
        }

        public override int GetHashCode()
        {
            return (Host != null ? Host.GetHashCode() : 0);
        }
    }
}