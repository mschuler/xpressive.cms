﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.Files.Commands
{
    [RequiredPermission("file", PermissionLevel.CanChange)]
    public class EnableFileTracking : CommandBase { }
}