
begin transaction

create table MediaContent (
	Id int not null identity(1, 1),
	FileId int not null,
	Title nvarchar(256) not null,
	DurationInSeconds int not null, -- in seconds
	Author nvarchar(256) not null,
	MediaType nvarchar(32) not null,
	Description nvarchar(4000) not null,

	constraint PK_MediaContent primary key (Id),
	constraint FK_MediaContent_File foreign key (FileId) references [File] (Id)
)

create table MediaCategory (
	Id int not null identity(1, 1),
	Guid uniqueidentifier not null,
	[Name] nvarchar(128) not null,
	ParentId int null,

	constraint PK_MediaCategory primary key (Id),
	constraint UX_MediaCategory_Guid unique (Guid)
)

create table MediaContentCategory (
	MediaCategoryId int not null,
	MediaContentId int not null,

	constraint PK_MediaContentCategory primary key (MediaCategoryId, MediaContentId),
	constraint FK_MediaContentCategory_MediaCategory foreign key (MediaCategoryId) references MediaCategory (Id),
	constraint FK_MediaContentCategory_MediaContent foreign key (MediaContentId) references MediaContent (Id)
)

create table MediaAdditionalResource (
	ParentContentId int not null,
	ChildContentId int not null,

	constraint PK_MediaAdditionalResource primary key (ParentContentId, ChildContentId),
	constraint FK_MediaAdditionalResource_Parent foreign key (ParentContentId) references MediaContent (Id),
	constraint FK_MediaAdditionalResource_Child foreign key (ChildContentId) references MediaContent (Id)
)

alter table [File] drop column [Content];
alter table [File] drop column Description;

insert into Version (Major, Minor, Revision) values (0, 0, 19);

commit transaction
