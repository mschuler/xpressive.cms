﻿namespace Cms.Services.Contracts
{
    public interface ITemporaryFileStoreService
    {
        ITemporaryFile Create();

        void Delete(ITemporaryFile file);

        bool TryGet(ulong id, out ITemporaryFile file);
    }
}