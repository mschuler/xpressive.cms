﻿(function() {
    angular.module("app").controller("SettingsController", ["$rootScope", "$scope", "$http", "$timeout", "tenantService", "toastService", "$translate", "$log", "toastr", function($rootScope, $scope, $http, $timeout, tenantService, toastService, $translate, $log, toastr) {
        var emailUrl = String.format("/api/settings/{0}/email", tenantService.getTenant().id);
        var urlUrl = String.format("/api/url/settings/{0}", tenantService.getTenant().id);
        var urlWatchSet = false;

        function getSettings() {
            $scope.email = {};
            $scope.urlShortener = {};

            $http.get(emailUrl, { cache: false }).success(function (data) {
                $scope.email = data;
            });

            $http.get(urlUrl, { cache: false }).success(function (data) {
                $scope.urlShortener = data;

                if (data && data.selectedService) {
                    $scope.urlShortener.selection = _.find(data.services, function(s) { return s.name === data.selectedService; });
                }

                if (urlWatchSet) {
                    return;
                }

                urlWatchSet = true;

                $scope.$watch("urlShortener.selection", function (newValue) {
                    if (newValue) {
                        $scope.urlShortener.selectedService = newValue.name;
                        $scope.urlShortener.configuration = newValue.configuration;
                    } else {
                        $scope.urlShortener.selectedService = null;
                        $scope.urlShortener.configuration = [];
                    }
                });
            });
        };

        $scope.saveEmail = function() {
            return $http.post(emailUrl, $scope.email).success(function() {
                $translate("success_save_email_settings").then(function (t) {
                    toastr.success('', t);
                });
            }).error(function (error) {
                $translate("error_save_email_settings").then(function(t) {
                    toastService.error(t, error);
                });
            });
        };

        $scope.saveUrl = function() {
            return $http.post(urlUrl, $scope.urlShortener).success(function() {
                _.each($scope.urlShortener.configuration, function(c) {
                    c.value = "";
                });
                $translate("success_save_url_shortening_configuration").then(function(t) {
                    toastr.success('', t);
                });
            }).error(function (error) {
                $translate("error_save_url_shortening_configuration").then(function (t) {
                    toastService.error(t, error);
                });
            });
        };

        $scope.verifyUrl = function() {
            return $http.post(urlUrl + "/validate").success(function () {
                $translate("success_verify_url_shortening_configuration").then(function (t) {
                    toastr.success('', t);
                });
            }).error(function () {
                $translate("error_verify_url_shortening_configuration").then(function (t) {
                    toastr.error('', t);
                });
            });
        };

        $timeout(function() {
            $scope.$watch("email.emailAddress", function(newValue) {
                if (newValue && !$scope.email.responseAddress) {
                    $scope.email.responseAddress = $scope.email.emailAddress;
                }
            });
        }, 3000);

        getSettings();

        $rootScope.$on("logout", function() {
            $scope.email = {};
            $scope.urlShortener = {};
        });

        $rootScope.$on("tenantChanged", function() {
            emailUrl = String.format("/api/settings/{0}/email", tenantService.getTenant().id);
            urlUrl = String.format("/api/url/settings/{0}", tenantService.getTenant().id);
            getSettings();
        });
    }]);
}());