using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission(false)]
    public class ChangeOwnPassword : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string OldPassword { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string NewPassword { get; set; }
    }
}