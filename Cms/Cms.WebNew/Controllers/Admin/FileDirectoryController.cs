using System.Collections.Generic;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.FileDirectories.Commands;
using Cms.Aggregates.FileDirectories.Queries;
using Cms.Aggregates.Security;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class FileDirectoryController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _uniqueIdProvider;
        private readonly IRepository<FileDirectory> _repository;

        public FileDirectoryController(IUniqueIdProvider uniqueIdProvider, ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IRepository<FileDirectory> repository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _uniqueIdProvider = uniqueIdProvider;
            _repository = repository;
        }

        [HttpGet, Route("api/filedirectory/{tenantId:ulong}/{fileStoreId:ulong}")]
        public IEnumerable<object> Get(ulong tenantId, ulong fileStoreId)
        {
            return ExecuteQuery(new GetAllFileDirectoriesWithChildren
            {
                TenantId = tenantId,
                FileStoreId = fileStoreId
            });
        }

        [HttpPost, Route("api/filedirectory/{tenantId:ulong}/{parentId:ulong}")]
        public object Create(ulong tenantId, ulong parentId, [FromBody]NameAndFileStoreDto dto)
        {
            var id = _uniqueIdProvider.GetId();
            var fileStoreId = ulong.Parse(dto.FileStoreId);

            SendCommand(new CreateFileDirectory
            {
                AggregateId = id,
                TenantId = tenantId,
                ParentId = parentId,
                Name = dto.Name,
                FileStoreId = fileStoreId,
            });

            return new
            {
                Id = id.ToJsonString(),
                FileStoreId = fileStoreId.ToJsonString(),
                Name = dto.Name
            };
        }

        [HttpPut, Route("api/filedirectory/{directoryId:ulong}")]
        public void Rename(ulong directoryId, [FromBody]NameDto dto)
        {
            var tenantId = _repository.Get(directoryId).TenantId;
            SendCommand(new ChangeFileDirectoryName
            {
                AggregateId = directoryId,
                Name = dto.Name,
                TenantId = tenantId,
            });
        }

        [HttpDelete, Route("api/filedirectory/{directoryId:ulong}")]
        public void Delete(ulong directoryId)
        {
            var tenantId = _repository.Get(directoryId).TenantId;
            SendCommand(new DeleteFileDirectory
            {
                AggregateId = directoryId,
                TenantId = tenantId,
            });
        }

        [HttpPut, Route("api/filedirectory/{directoryId:ulong}/move/{parentId:ulong}")]
        public void Move(ulong directoryId, ulong parentId)
        {
            var tenantId = _repository.Get(directoryId).TenantId;
            SendCommand(new ChangeFileDirectoryParent
            {
                AggregateId = directoryId,
                ParentId = parentId,
                TenantId = tenantId,
            });
        }

        [HttpGet, Route("api/filedirectory/{tenantId:ulong}/shared")]
        public IEnumerable<object> GetSharedDirectories(ulong tenantId)
        {
            return ExecuteQuery(new GetSharedFileDirectories
            {
                TenantId = tenantId
            });
        }

        [HttpPut, Route("api/filedirectory/{directoryId:ulong}/share")]
        public void Share(ulong directoryId, [FromBody] TenantListDto dto)
        {
            var tenantId = _repository.Get(directoryId).TenantId;

            SendCommand(new ShareFileDirectory
            {
                AggregateId = directoryId,
                TenantId = tenantId,
                TenantIds = dto.TenantIds.ToArray()
            });
        }

        public class NameDto
        {
            public string Name { get; set; }
        }

        public class NameAndFileStoreDto
        {
            public string FileStoreId { get; set; }
            public string Name { get; set; }
        }

        public class TenantListDto
        {
            public List<ulong> TenantIds { get; set; }
        }
    }
}