
begin transaction

create table [User] (
	Id int not null identity(1, 1),
	Guid uniqueidentifier not null,
	Firstname nvarchar(128) not null,
	Lastname nvarchar(128) not null,
	Email nvarchar(256) not null,
	Password varbinary(128) null,
	Salt varbinary(128) not null,
	IsSysadmin bit not null default 0,

	constraint PK_User primary key (Id),
	constraint UX_User_Guid unique (Guid),
	constraint UX_User_Email unique (Email)
)

create table UserAttribute (
	Id int not null identity(1, 1),
	UserId int not null,
	[Key] nvarchar(128) not null,
	[Value] nvarchar(512) not null default '',

	constraint PK_UserAttribute primary key (Id),
	constraint FK_UserAttribute_User foreign key (UserId) references [User] (Id)
)

create table [Group] (
	Id int not null identity(1, 1),
	Guid uniqueidentifier not null,
	[Name] nvarchar(128) not null,
	ParentId int null,

	constraint PK_Group primary key (Id),
	constraint FK_Group foreign key (ParentId) references [Group] (Id),
	constraint UX_Group unique (Guid),
	constraint UX_Group_Name unique ([Name])
)

create table UserGroup (
	UserId int not null,
	GroupId int not null,

	constraint PK_UserGroup primary key (UserId, GroupId),
	constraint FK_UserGroup_User foreign key (UserId) references [User] (Id),
	constraint FK_UserGroup_Group foreign key (GroupId) references [Group] (Id)
)

create table Acl (
	Id int not null identity(1, 1),
	Guid uniqueidentifier not null,
	[Name] nvarchar(128),
	ParentId int null,

	constraint PK_Acl primary key (Id),
	constraint FK_Acl_Acl foreign key (ParentId) references Acl (Id),
	constraint UX_Acl unique (Guid)
)

create table GroupAcl (
	GroupId int not null,
	AclId int not null

	constraint PK_GroupAcl primary key (GroupId, AclId)
	constraint FK_GroupAcl_Group foreign key (GroupId) references [Group] (Id),
	constraint FK_GroupAcl_Acl foreign key (AclId) references Acl (Id)
)

insert into Version (Major, Minor, Revision) values (0, 0, 15);

commit transaction

