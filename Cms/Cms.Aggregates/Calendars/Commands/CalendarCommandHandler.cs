using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Calendars.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.Calendars.Commands
{
    public class CalendarCommandHandler : CommandHandlerBase
    {
        private readonly ICalendarRepository _repository;

        public CalendarCommandHandler(ICalendarRepository repository)
        {
            _repository = repository;
        }

        public void When(AddCategory command, Action<IEvent> eventHandler)
        {
            var calendar = _repository.Get(command.AggregateId);
            var name = command.Name.Trim().RemoveControlChars();

            if (calendar.Categories.Any(c => c.Name.Equals(name, StringComparison.OrdinalIgnoreCase)))
            {
                throw new ExceptionWithReason("There is already a category with this name.");
            }

            var e = new CategoryAdded
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(ChangeCategoryName command, Action<IEvent> eventHandler)
        {
            var calendar = _repository.Get(command.AggregateId);
            var oldName = command.OldName.Trim().RemoveControlChars();
            var newName = command.NewName.Trim().RemoveControlChars();

            if (!calendar.Categories.Any(c => c.Name.Equals(oldName)))
            {
                throw new ExceptionWithReason("There is no category with this name.");
            }

            if (calendar.Categories.Where(c => !c.Name.Equals(oldName)).Any(c => c.Name.Equals(newName)))
            {
                throw new ExceptionWithReason("There is already a category with this name.");
            }

            var e = new CategoryNameChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                OldName = oldName,
                NewName = newName,
            };

            eventHandler(e);
        }

        public void When(RemoveCategory command, Action<IEvent> eventHandler)
        {
            var calendar = _repository.Get(command.AggregateId);
            var name = command.Name.Trim().RemoveControlChars();

            if (!calendar.Categories.Any(c => c.Name.Equals(name)))
            {
                throw new ExceptionWithReason("There is no category with this name.");
            }

            var e = new CategoryRemoved
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(AddAppointment command, Action<IEvent> eventHandler)
        {
            var calendar = _repository.Get(command.AggregateId);
            var title = command.Title.Trim().RemoveControlChars();

            AssertRecurrenceNotNull(command.Recurrence);

            if (string.IsNullOrEmpty(title))
            {
                throw new ExceptionWithReason("There is no title.");
            }

            var categories = new List<string>();

            foreach (var category in command.Categories)
            {
                if (calendar.Categories.Any(c => c.Name.Equals(category, StringComparison.Ordinal)))
                {
                    categories.Add(category);
                }
            }

            var e = new AppointmentAdded
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                AppointmentId = command.AppointmentId,
                StartEnd = command.StartEnd,
                Title = title,
                IsHighlight = command.IsHighlight,
                Location = command.Location.Trim().RemoveControlChars(),
                Link = command.Link,
                Recurrence = command.Recurrence,
                Categories = categories,
            };

            eventHandler(e);
        }

        public void When(RemoveAppointment command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            var e = new AppointmentRemoved
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                AppointmentId = command.AppointmentId,
            };

            eventHandler(e);
        }

        public void When(ChangeAppointment command, Action<IEvent> eventHandler)
        {
            var calendar = _repository.Get(command.AggregateId);
            var title = command.Title.Trim().RemoveControlChars();

            AssertRecurrenceNotNull(command.Recurrence);

            if (string.IsNullOrEmpty(title))
            {
                throw new ExceptionWithReason("There is no title.");
            }

            var categories = new List<string>();

            foreach (var category in command.Categories)
            {
                if (calendar.Categories.Any(c => c.Name.Equals(category, StringComparison.Ordinal)))
                {
                    categories.Add(category);
                }
            }

            var e = new AppointmentChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                AppointmentId = command.AppointmentId,
                StartEnd = command.StartEnd,
                Title = title,
                IsHighlight = command.IsHighlight,
                Location = command.Location.Trim().RemoveControlChars(),
                Link = command.Link,
                Recurrence = command.Recurrence,
                Categories = categories,
            };

            eventHandler(e);
        }

        private void AssertRecurrenceNotNull(AppointmentRecurrence recurrence)
        {
            if (recurrence == null)
            {
                throw new ExceptionWithReason("Recurrence was not set.");
            }
        }
    }
}