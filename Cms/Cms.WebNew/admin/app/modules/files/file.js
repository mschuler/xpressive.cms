﻿(function() {
    angular.module("app").controller("FileController", ["$rootScope", "$scope", "$log", "$http", "tenantService", "dialogService", "toastService", "$translate", function ($rootScope, $scope, $log, $http, tenantService, dialogService, toastService, $translate) {
        $scope.directories = [];
        $scope.trashDirectory = { id: 123456, name: "Trash" };
        $scope.trashDirectories = [$scope.trashDirectory];
        $scope.sharedDirectoryRoot = { id: 12345, name: "Shared directories" };
        $scope.shared = [$scope.sharedDirectoryRoot];
        $scope.files = [];
        $scope.selectedDirectory = null;
        $scope.fileSearchText = "";

        $scope.treeOptions = {
            dropped: function (event) {
                var thisNr = event.source.nodeScope.node.id;
                var oldParent = event.source.nodesScope == null || event.source.nodesScope.node == null ? null : event.source.nodesScope.node.id;
                var newParent = event.dest.nodesScope == null || event.dest.nodesScope.node == null ? null : event.dest.nodesScope.node.id;

                if ((oldParent === newParent || (!oldParent && !newParent))) {
                    return false;
                }

                newParent = newParent || 0;

                $http.put('/api/filedirectory/' + thisNr + '/move/' + newParent).success(function () {
                    $log.debug('dropped ' + thisNr + " to " + newParent);
                });

                return true;
            }
        };

        $http.get("/api/permission/file/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.selectDirectory = function (directory) {
            $scope.selectedDirectory = directory;

            if (directory.isFileStore) {
                $scope.files = [];
                return;
            }

            if (directory.isShared) {
                getSharedFiles($scope.selectedDirectory.id);
            } else {
                getFiles($scope.selectedDirectory.id);
            }
        };

        $scope.selectTrashDirectory = function() {
            $scope.selectedDirectory = $scope.trashDirectory;
            getTrashFiles();
        };

        var getTrashFiles = function () {
            $scope.files = [];
            var url = String.format("/api/file/{0}/trash", tenantService.getTenant().id);
            $http.get(url, { cache: false }).success(function (data) {
                $scope.files = data;
            });
        };

        var getSharedFiles = function(directoryId) {
            $scope.files = [];
            var url = String.format("/api/files/{0}/{1}/shared", tenantService.getTenant().id, directoryId);
            $http.get(url, { cache: false }).success(function (data) {
                $scope.files = data;
            });
        }

        var moveFile = "";
        var confirmMoveFile = "";
        var errorMoveFile = "";
        var renameFile = "";
        var errorRenameFile = "";
        var name = "";
        var uploadFiles = "";
        var deleteFile = "";
        var confirmDeleteFile = "";
        var errorDeleteFile = "";
        var replaceFileSubject = "";
        var createDirectory = "";
        var errorCreateDirectory = "";
        var deleteDirectory = "";
        var confirmDeleteDirectory = "";
        var errorDeleteDirectory = "";
        var renameDirectory = "";
        var errorRenameDirectory = "";
        var errorEnableFileTracking = "";
        var errorDisableFileTracking = "";

        $translate(["move_file", "confirm_move_file", "error_move_file", "rename_file", "error_rename_file", "name", "upload_files", "delete_file", "confirm_delete_file", "error_delete_file", "replace_file_subject", "create_directory", "error_create_directory", "delete_directory", "confirm_delete_directory", "error_delete_directory", "rename_directory", "error_rename_directory", "error_enable_file_tracking", "error_disable_file_tracking", "shared_directories"]).then(function (t) {
            moveFile = t.move_file;
            confirmMoveFile = t.confirm_move_file;
            errorMoveFile = t.error_move_file;
            renameFile = t.rename_file;
            errorRenameFile = t.error_rename_file;
            name = t.name;
            uploadFiles = t.upload_files;
            deleteFile = t.delete_file;
            confirmDeleteFile = t.confirm_delete_file;
            errorDeleteFile = t.error_delete_file;
            replaceFileSubject = t.replace_file_subject;
            createDirectory = t.create_directory;
            errorCreateDirectory = t.error_create_directory;
            deleteDirectory = t.delete_directory;
            confirmDeleteDirectory = t.confirm_delete_directory;
            errorDeleteDirectory = t.error_delete_directory;
            renameDirectory = t.rename_directory;
            errorRenameDirectory = t.error_rename_directory;
            errorEnableFileTracking = t.error_enable_file_tracking;
            errorDisableFileTracking = t.error_disable_file_tracking;

            $scope.sharedDirectoryRoot.name = t.shared_directories;
        });

        $scope.onDrop = function (target, source) {
            dialogService.confirmDialog(moveFile, String.format(confirmMoveFile, source.fullname, target.name), function () {
                $http.put("/api/file/" + source.id + "/move/" + target.id).success(function () {
                    $scope.files = _.reject($scope.files, function (o) { return o.id === source.id; });
                }).error(function(error) {
                    toastService.error(errorMoveFile, error);
                });
            });
        };

        var getFiles = function (directoryId) {
            $scope.files = [];
            $http.get("/api/file/" + directoryId, { cache: false }).success(function (data) {
                $scope.files = data;
            });
        };

        var markDirectoriesWhenParentsAreShared = function (directory, isShared) {
            if (directory.shared && directory.shared.length > 0) {
                isShared = true;
            }

            _.each(directory.children, function (d) {
                d.parentIsShared = isShared;
                markDirectoriesWhenParentsAreShared(d, isShared);
            });
        };

        var markDirectoryIsShared = function(directory) {
            directory.isShared = true;
            _.each(directory.children, markDirectoryIsShared);
        };

        var getDirectories = function () {
            var tenantId = tenantService.getTenant().id;
            $http.get("/api/filestores/" + tenantId, { cache: false }).success(function (fileStores) {
                $scope.directories = fileStores;
                _.each(fileStores, function (fileStore) {
                    fileStore.isFileStore = true;
                    $http.get("/api/filedirectory/" + tenantId + "/" + fileStore.id, { cache: false }).success(function (data) {
                        fileStore.children = data;

                        _.each(data, function(d) {
                            d.parentIsShared = false;
                            markDirectoriesWhenParentsAreShared(d, false);
                        });
                    });
                });
            });

            $http.get("/api/filedirectory/" + tenantId + "/shared", { cache: false }).success(function (directories) {
                $scope.sharedDirectoryRoot.children = directories;

                _.each(directories, function (d) {
                    markDirectoryIsShared(d);
                });

                var treeRootScope = angular.element(document.getElementById("tree-root-shared")).scope();
                treeRootScope.collapseAll();
            });
        };

        $scope.renameFile = function(file) {
            dialogService.singleInputDialog(renameFile, name, file.name, function (newName) {
                var dto = {
                    name: newName
                };
                $http.put("/api/file/" + file.id + "/rename", dto).success(function() {
                    file.name = newName;
                }).error(function(error) {
                    toastService.error(errorRenameFile, error);
                });
            });
        };

        $scope.editFile = function (file) {
            $http.get(file.downloadUrl, { cache: false }).success(function (content) {
                dialogService.fileEditorDialog(file.fullname, content, function (newContent) {
                    var url = String.format("/api/file/replace/{0}/content", file.id);
                    var dto = {
                        content: newContent
                    };
                    $http.post(url, dto).success(function (newVersion) {
                        var downloadUrl = String.format("/download/{0}/{1}/{2}?isTracked=false", file.id, newVersion, file.name);
                        file.downloadUrl = downloadUrl;
                    }).error(function(error) {
                        $log.error(error);
                    });
                });
            }).error(function(error) {
                $log.error(error);
            });
        };

        $scope.uploadFiles = function() {
            dialogService.fileUploadDialog(uploadFiles, $scope.selectedDirectory.id, function() {
                getFiles($scope.selectedDirectory.id);
            });
        };

        $scope.deleteFile = function (file) {
            dialogService.confirmDialog(deleteFile, String.format(confirmDeleteFile, file.fullname), function() {
                $http.delete("/api/file/" + file.id).success(function () {
                    $scope.files = _.reject($scope.files, function (o) { return o.id === file.id; });
                }).error(function(error) {
                    toastService.error(errorDeleteFile, error);
                });
            });
        };

        $scope.replaceFile = function (file) {
            dialogService.fileReplaceDialog(String.format(replaceFileSubject, file.fullname), file.id, function () {
                getFiles($scope.selectedDirectory.id);
            });
        };

        $scope.shareDirectory = function (directory) {
            var existingTenantIds = [];

            if (directory.shared) {
                existingTenantIds = _.map(directory.shared, function(t) { return t.id; });
            }

            dialogService.selectTenantsDialog(existingTenantIds, function (tenants) {
                var ids = _.map(tenants, function (t) {
                    return t.id;
                });
                var dto = {
                    tenantIds: ids
                };
                var url = String.format("/api/filedirectory/{0}/share", directory.id);
                $http.put(url, dto).success(function () {
                    directory.shared = tenants;
                });
            });
        };

        $scope.donotshareDirectory = function(directory) {
            var url = String.format("/api/filedirectory/{0}/share", directory.id);
            var dto = {
                tenantIds: []
            };
            return $http.put(url, dto).success(function () {
                directory.shared = [];
            });
        };

        $scope.getNames = function(tenants) {
            var names = "";
            _.each(tenants, function(t) {
                names += t.name + ", ";
            });
            names = names.trim();
            return names.substring(0, names.length - 1);
        };

        $scope.createDirectory = function() {
            dialogService.singleInputDialog(createDirectory, name, "", function(newName) {
                var t = tenantService.getTenant().id;
                var p = "0";
                var fs = "0";

                if (!$scope.selectedDirectory.isFileStore) {
                    p = $scope.selectedDirectory.id || "0";
                    fs = $scope.selectedDirectory.fileStoreId;
                } else {
                    fs = $scope.selectedDirectory.id;
                }

                var dto = {
                    name: newName,
                    fileStoreId: fs,
                };

                $http.post("/api/filedirectory/" + t + "/" + p, dto).success(function (newDir) {
                    newDir.children = [];
                    if ($scope.selectedDirectory) {
                        $scope.selectedDirectory.children.push(newDir);
                    } else {
                        $scope.directories.push(newDir);
                    }

                    // TODO: potentieller parent aufklappen
                    // TODO: neuer Knoten selektieren
                }).error(function(error) {
                    toastService.error(errorCreateDirectory, error);
                });
            });
        };

        $scope.searchFiles = function() {
            if ($scope.fileSearchText) {
                $scope.selectedDirectory = null;
                getFiles("search/" + tenantService.getTenant().id + "?search=" + $scope.fileSearchText);
            }
        };

        $scope.deleteDirectory = function(directory) {
            dialogService.confirmDialog(deleteDirectory, String.format(confirmDeleteDirectory, directory.node.name), function () {
                $http.delete("/api/filedirectory/" + directory.node.id).success(function () {
                    directory.remove();
                }).error(function(error) {
                    toastService.error(errorDeleteDirectory, error);
                });
            });
        };

        $scope.renameDirectory = function(directory) {
            dialogService.singleInputDialog(renameDirectory, name, directory.name, function (newName) {
                var dto = {
                    name: newName
                };
                $http.put("/api/filedirectory/" + directory.id, dto).success(function () {
                    directory.name = newName;
                }).error(function(error) {
                    toastService.error(errorRenameDirectory, error);
                });
            });
        };

        $scope.trackFile = function (file) {
            var url = String.format("/api/file/{0}/track", file.id);
            return $http.post(url).success(function() {
                file.isTracked = true;
            }).error(function () {
                toastService.error(errorEnableFileTracking, error);
            });
        };

        $scope.untrackFile = function (file) {
            var url = String.format("/api/file/{0}/untrack", file.id);
            return $http.post(url).success(function () {
                file.isTracked = false;
            }).error(function () {
                toastService.error(errorDisableFileTracking, error);
            });
        };

        $rootScope.$on("tenantChanged", function () {
            $scope.files = [];
            getDirectories();
        });

        $rootScope.$on("logout", function() { $scope.directories = []; });

        getDirectories();
    }]);
}());