﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cms.Services.Contracts;
using Cms.Services.UrlShortening;
using Moq;
using Xunit;
using Xunit.Abstractions;

namespace Cms.Services.Tests
{
    public class Given_an_url_shortening_service
    {
        private readonly Mock<IUrlShorteningRepository> _urlShorteningRepositoryMock = new Mock<IUrlShorteningRepository>();
        private readonly ITestOutputHelper _outputWriter;

        public Given_an_url_shortening_service(ITestOutputHelper outputWriter)
        {
            _outputWriter = outputWriter;

            _urlShorteningRepositoryMock.Setup(m => m.Add(It.IsAny<string>(), It.IsAny<string>()));
            _urlShorteningRepositoryMock.Setup(m => m.GetAll()).Returns(new List<Tuple<string, string>>());
        }

        [Fact]
        public void Bitly()
        {
            var service = new BitlyShorteningService(_urlShorteningRepositoryMock.Object);
            service.SetConfigurationValue("Access Token", "");
            Test(service, false);
        }

        [Fact]
        public void Owly()
        {
            var service = new OwlyShorteningService(_urlShorteningRepositoryMock.Object);
            service.SetConfigurationValue("API Key", "");
            Test(service, false);
        }

        [Fact]
        public void TinyCc()
        {
            var service = new TinyCcShorteningService(_urlShorteningRepositoryMock.Object);
            service.SetConfigurationValue("Login", "");
            service.SetConfigurationValue("API Key", "");
            Test(service, false);
        }

        [Fact]
        public void GooGl()
        {
            var service = new GooGlShorteningService(_urlShorteningRepositoryMock.Object);
            service.SetConfigurationValue("API Key", "");
            Test(service, false);
        }

        [Fact]
        public void WithCache()
        {
            var service = new TinyCcShorteningService(_urlShorteningRepositoryMock.Object);
            service.SetConfigurationValue("Login", "");
            service.SetConfigurationValue("API Key", "");

            var tasks = new[]
            {
                Task.Run(()=>Test(service)),
                Task.Run(()=>Test(service)),
                Task.Run(()=>Test(service)),
                Task.Run(()=>Test(service)),
                Task.Run(()=>Test(service))
            };

            Task.WaitAll(tasks, TimeSpan.FromSeconds(10));

            Assert.True(tasks.Select(t => t.Result).Distinct(StringComparer.Ordinal).Count() == 1);
        }

        private string Test(IUrlShorteningService service, bool useCache = true)
        {
            var result = service.ShortenUrl("http://www.google.ch", useCache);

            Assert.False(string.IsNullOrEmpty(result));
            _outputWriter.WriteLine("Result ({1}): {0}", result, service.GetType().Name);

            return result;
        }
    }
}
