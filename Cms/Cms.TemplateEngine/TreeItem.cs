﻿using System.Collections.Generic;
using Cms.TemplateEngine.Enums;

namespace Cms.TemplateEngine
{
    public class TreeItem
    {
        public TreeItem()
        {
            Children = new List<TreeItem>();
            ParameterType = ParameterType.None;
        }

        public string Value { get; set; }

        public TreeItemType ItemType { get; set; }

        public ParameterType ParameterType { get; set; }

        public TreeItem Parent { get; set; }

        public List<TreeItem> Children { get; private set; }

        public object Result { get; set; }

        public TreeItem LeftItem
        {
            get
            {
                if (Parent != null)
                {
                    int index = Parent.Children.IndexOf(this);
                    if (index > 0)
                    {
                        return Parent.Children[index - 1];
                    }
                }

                return null;
            }
        }

        public TreeItem RightItem
        {
            get
            {
                if (Parent != null)
                {
                    int index = Parent.Children.IndexOf(this);
                    if (index + 1 < Parent.Children.Count)
                    {
                        return Parent.Children[index + 1];
                    }
                }

                return null;
            }
        }

        internal bool IsBelongingToLeftItem { get; set; }

        public bool IsParent()
        {
            return GetRootItem().Equals(this);
        }

        public TreeItem GetRootItem()
        {
            var currentParent = this;

            while (true)
            {
                if (currentParent.Parent == null)
                {
                    return currentParent;
                }
                currentParent = currentParent.Parent;
            }
        }
    }
}
