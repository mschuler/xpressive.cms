using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("user", PermissionLevel.CanChange)]
    public class ChangeUserEmailAddress : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(5)]
        public string EmailAddress { get; set; }
    }
}