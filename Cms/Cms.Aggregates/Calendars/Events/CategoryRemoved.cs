namespace Cms.Aggregates.Calendars.Events
{
    public class CategoryRemoved : EventBase<Calendar>
    {
        public string Name { get; set; }
    }
}