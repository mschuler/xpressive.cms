begin transaction

alter table Article alter column DirectoryId int null;

insert into Version (Major, Minor, Revision) values (0, 0, 12);

commit transaction
