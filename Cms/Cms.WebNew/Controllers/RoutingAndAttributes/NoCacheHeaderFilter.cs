using System;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace Cms.WebNew.Controllers.RoutingAndAttributes
{
    public class NoCacheHeaderFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Response == null)
            {
                return;
            }

            actionExecutedContext.Response.Headers.Pragma.Add(new NameValueHeaderValue("no-cache"));
            actionExecutedContext.Response.Headers.CacheControl = new CacheControlHeaderValue
            {
                NoCache = true,
                NoStore = true,
                MustRevalidate = true
            };

            if (actionExecutedContext.Response.Content != null)
            {
                actionExecutedContext.Response.Content.Headers.Expires = DateTimeOffset.UtcNow;
            }

            base.OnActionExecuted(actionExecutedContext);
        }
    }
}