namespace Cms.Aggregates.DynamicObjects.Events
{
    internal class UserStarredDynamicObjectDefinitionEventHandler : EventHandlerBase
    {
        private readonly IUserStarredDynamicObjectDefinitionRepository _repository;

        public UserStarredDynamicObjectDefinitionEventHandler(IUserStarredDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public void When(DynamicObjectDefinitionStarred @event)
        {
            _repository.Add(@event.UserId, @event.AggregateId);
        }

        public void When(DynamicObjectDefinitionUnstarred @event)
        {
            _repository.Remove(@event.UserId, @event.AggregateId);
        }
    }
}