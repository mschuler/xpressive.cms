using System.Collections.Generic;
using Cms.Aggregates.Websites;

namespace Cms.Aggregates.Pages
{
    public interface IPageDtoService
    {
        object GetPageDto(Website website, PageBase page, List<object> content = null, bool withChildren = false);
    }
}