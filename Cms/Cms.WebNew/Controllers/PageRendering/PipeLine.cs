using System.Collections.Generic;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class PipeLine<T> : IFilter<T>
    {
        private readonly List<IFilter<T>> _filters = new List<IFilter<T>>();

        public PipeLine<T> Register(IFilter<T> filter)
        {
            _filters.Add(filter);
            return this;
        }

        public bool Execute(T input)
        {
            foreach (var filter in _filters)
            {
                if (!Execute(filter, input))
                {
                    return false;
                }
            }

            return true;
        }

        private bool Execute(IFilter<T> filter, T message)
        {
            return filter.Execute(message);
        }
    }
}