namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDeleted : EventBase<DynamicObject> { }
}