using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.PageTemplates
{
    internal class PageTemplateRepository : RepositoryBase<PageTemplate>, IPageTemplateRepository
    {
        private static readonly object _lock = new object();
        private static int _currentMaxPublicId;

        public int GetNextPublicId()
        {
            lock (_lock)
            {
                if (_currentMaxPublicId == 0)
                {
                    var allPages = GetAll().ToList();
                    _currentMaxPublicId = allPages.Count == 0 ? 0 : allPages.Max(p => p.PublicId);
                }

                _currentMaxPublicId++;
                return _currentMaxPublicId;
            }
        }

        public PageTemplate GetByPublicId(int publicId)
        {
            return GetAll().SingleOrDefault(p => p.PublicId.Equals(publicId));
        }

        public IEnumerable<PageTemplate> GetAll(ulong tenantId)
        {
            return GetAll().Where(t => t.TenantId == tenantId);
        }

        public override void DeleteAllByTenantId(ulong tenantId)
        {
            var pageTemplates = GetAll(tenantId).ToList();
            pageTemplates.ForEach(p => Delete(p.Id));
        }
    }
}