﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Cms.SharedKernel
{
    public sealed class PerformanceMeasurer : IDisposable
    {
        [ThreadStatic]
        private static PerformanceMeasurer _currentMeasurer;

        private readonly PerformanceMeasurer _parent;
        private List<PerformanceMeasurer> _children;
        private readonly Stopwatch _stopwatch;
        private readonly Func<string> _message;
        private readonly Action<string> _output;
        private double _duration;

        private PerformanceMeasurer(PerformanceMeasurer parent, Func<string> message, Action<string> output)
        {
            _children = new List<PerformanceMeasurer>();
            _parent = parent;
            _message = message;
            _output = output;

            if (_parent != null)
            {
                _parent._children.Add(this);
            }

            _stopwatch = Stopwatch.StartNew();
        }

        public static IDisposable Get(string message, Action<string> output)
        {
            return Get(() => message, output);
        }

        public static IDisposable Get(Func<string> message, Action<string> output)
        {
            var parent = _currentMeasurer;
            var measurer = new PerformanceMeasurer(parent, message, output);

            _currentMeasurer = measurer;

            return measurer;
        }

        public void Dispose()
        {
            _stopwatch.Stop();
            _duration = _stopwatch.Elapsed.TotalMilliseconds;

            if (ReferenceEquals(_currentMeasurer, this))
            {
                _currentMeasurer = _parent;
            }

            var children = _children.GroupBy(c => c._message())
                .Select(g => new PerformanceMeasurer(this, () => g.Key, _output)
                {
                    _duration = g.Sum(p => p._duration),
                    _children = g.SelectMany(p => p._children).ToList()
                })
                .OrderByDescending(p => p._duration)
                .ToList();

            _children = children;

            if (_currentMeasurer != null)
            {
                return;
            }

            var output = new StringBuilder();

            if (_children.Count > 0)
            {
                output.AppendLine();
            }

            AppendOutput(output, this, 0);

            _output(output.ToString());
            Debug.WriteLine(output.ToString());
        }

        private void AppendOutput(StringBuilder output, PerformanceMeasurer measurer, int level)
        {
            var prefix = new string(' ', level * 4);
            output.Append(prefix);
            output.Append(measurer._message());
            output.Append(": ");
            output.Append(measurer._duration.ToString("F0"));
            output.Append(" ms");

            foreach (var counter in measurer._children)
            {
                output.AppendLine();
                AppendOutput(output, counter, level + 1);
            }
        }
    }
}
