﻿namespace Cms.Aggregates.Forms.Events
{
    public class FormActionAdded : EventBase<Form>
    {
        public ulong Id { get; set; }
        public FormActionType Type { get; set; }
        public FormActionConfiguration Configuration { get; set; }
    }
}