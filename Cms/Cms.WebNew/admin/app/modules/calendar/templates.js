﻿(function () {
    angular.module("app").controller("CalendarTemplatesController", ["$rootScope", "$scope", "$http", "dialogService", "tenantService", function ($rootScope, $scope, $http, dialogService, tenantService) {
        $scope.templates = [];

        function reloadTemplates() {
            $http.get("/api/calendar/templates/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
                $scope.templates = data;
            });
        };

        $rootScope.$on("tenantChanged", reloadTemplates);

        $rootScope.$on('logout', function () { $scope.templates = {}; });

        reloadTemplates();
    }]);
}());