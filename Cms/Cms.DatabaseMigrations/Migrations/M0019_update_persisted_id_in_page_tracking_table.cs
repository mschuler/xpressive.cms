namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0019_update_persisted_id_in_page_tracking_table : DatabaseSqlMigration
    {
        public M0019_update_persisted_id_in_page_tracking_table() : base(19) { }

        protected override string GetSql()
        {
            return "update PageTracking set PersistedId = SessionId";
        }
    }
}