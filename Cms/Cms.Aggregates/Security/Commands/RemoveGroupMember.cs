namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("group", PermissionLevel.CanChange)]
    public class RemoveGroupMember : CommandBase
    {
        [ValidId]
        public ulong MemberId { get; set; }
    }
}