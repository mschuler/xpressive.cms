using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using NPoco;
using Xunit;

namespace Cms.EventSourcing.Tests
{
    public class NPocoTransactionTests : DatabasePerformanceTestBase
    {
        [Fact]
        public void Given_ten_threads_and_thousands_of_rows_then_all_rows_are_inserted()
        {
            StartTest();
        }

        protected override void ClearTable()
        {
            using (var database = new Database("TestDatabaseConnection"))
            {
                database.Execute("delete from TestTable");
            }
        }

        protected override int GetNumberOfRowsInTestTable()
        {
            using (var database = new Database("TestDatabaseConnection"))
            {
                return database.ExecuteScalar<int>("select count(*) from TestTable");
            }
        }

        protected override int GetNumberOfDistinctThreads()
        {
            using (var database = new Database("TestDatabaseConnection"))
            {
                return database.ExecuteScalar<int>("select count(distinct Thread) from TestTable");
            }
        }

        protected override Task InsertIntoDatabase(BlockingCollection<ulong> queue)
        {
            return Task.Run(() =>
            {
                ulong id;
                while (queue.TryTake(out id, TimeSpan.FromMilliseconds(100)))
                {
                    using (var database = new Database("TestDatabaseConnection"))
                    {
                        database.BeginTransaction();

                        database.Execute("insert into TestTable (Id, Thread) values (@0, @1)", Convert.ToInt64(id), Thread.CurrentThread.ManagedThreadId);

                        database.CompleteTransaction();
                    }
                }
            });
        }
    }
}