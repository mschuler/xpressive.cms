using Cms.Services.TemplateEngineInterpreters;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Interpreter;
using Xunit;

namespace Cms.Services.Tests
{
    public class Given_an_email_interpreter
    {
        [Fact]
        public void Then_it_can_encode_emails()
        {
            var interpreter = new EmailInterpreter();

            InterpreterFactory.Instance.Clear();
            InterpreterFactory.Instance.Register(interpreter);

            var treeItem = new Engine().ParseTreeItem("{email(\"me@example.com\")}");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as string;

            Assert.Equal("<a href=\"&#x7a;&#x6e;&#x76;&#x79;&#x67;&#x62;&#x3a;&#x7a;&#x72;&#x25;&#x34;&#x30;&#x72;&#x6b;&#x6e;&#x7a;&#x63;&#x79;&#x72;&#x25;&#x32;&#x72;&#x70;&#x62;&#x7a;\" onclick=\"if(this.href.slice(0,1)=='z')this.href=this.href.replace(/[a-zA-Z]/g,function(c){return String.fromCharCode((c<='Z'?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);})\">&#x6d;&#x65;&#x40;&#x65;&#x78;&#x61;&#x6d;&#x70;&#x6c;&#x65;&#x2e;&#x63;&#x6f;&#x6d;</a>", result);
        }

        [Fact]
        public void Then_it_can_encode_emails_with_alternate_text()
        {
            var interpreter = new EmailInterpreter();

            InterpreterFactory.Instance.Clear();
            InterpreterFactory.Instance.Register(interpreter);

            var treeItem = new Engine().ParseTreeItem("{email(\"me@example.com\", \"email me\")}");
            var result = InterpreterFactory.Instance.Calculate(treeItem) as string;

            Assert.Equal("<a href=\"&#x7a;&#x6e;&#x76;&#x79;&#x67;&#x62;&#x3a;&#x7a;&#x72;&#x25;&#x34;&#x30;&#x72;&#x6b;&#x6e;&#x7a;&#x63;&#x79;&#x72;&#x25;&#x32;&#x72;&#x70;&#x62;&#x7a;\" onclick=\"if(this.href.slice(0,1)=='z')this.href=this.href.replace(/[a-zA-Z]/g,function(c){return String.fromCharCode((c<='Z'?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);})\">&#x65;&#x6d;&#x61;&#x69;&#x6c;&#x20;&#x6d;&#x65;</a>", result);
        }
    }
}