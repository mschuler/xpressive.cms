﻿using System;
using System.Collections.Generic;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Settings.Commands
{
    [RequiredPermission("sysadmin", PermissionLevel.CanEverything)]
    public class ChangeUrlShorteningSettings : CommandBase
    {
        public string ServiceName { get; set; }
        public IList<Tuple<string, string>> Configuration { get; set; }
    }
}