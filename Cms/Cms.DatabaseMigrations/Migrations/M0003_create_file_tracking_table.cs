namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0003_create_file_tracking_table : DatabaseSqlMigration
    {
        public M0003_create_file_tracking_table() : base(3) { }

        protected override string GetSql()
        {
            return @"
create table FileTracking (
    Date smalldatetime not null,
    IpAddress nvarchar(50) not null,
    FileId bigint not null,
    WebsiteId bigint not null,
    Country nvarchar(2) not null
)
create index ix_ft_dt on FileTracking (Date)
";
        }
    }
}