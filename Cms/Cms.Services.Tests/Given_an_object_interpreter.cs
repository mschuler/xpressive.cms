﻿using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.DynamicObjects;
using Cms.Services.ObjectFieldContentTypeConverter;
using Cms.Services.TemplateEngineInterpreters;
using Cms.SharedKernel;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Interpreter;
using Moq;
using Xunit;

namespace Cms.Services.Tests
{
    public class Given_an_object_interpreter
    {
        [Fact]
        public void Then_it_can_render_templates()
        {
            var repositoryMock = new Mock<IDynamicObjectRepository>();
            var definitionRepositoryMock = new Mock<IDynamicObjectDefinitionRepository>();
            var converters = new List<IObjectFieldContentTypeConverter>();

            var obj = new DynamicObject();
            obj.DefinitionId = 123;
            obj.Add(new DynamicObjectField { Name = "Inhalt", ContentType = "Html", Value = "<!DOCTYPE html>\n<html lang=\"de\">\n<head>\n<meta charset=\"utf-8\"/>\n<title>{page(this()).name}&nbsp;&ndash;&nbsp;{tenant(this()).name}</title>\n<link rel=\"stylesheet\" href=\"{file(?).link}\" type=\"text/css\">\n</head>\n<body>" });

            var definition = new DynamicObjectDefinition();
            definition.Add(new DynamicObjectTemplate { PublicId = 1, Template = "{$(\"Inhalt\")}" });

            repositoryMock.Setup(m => m.GetByPublicId(12)).Returns(obj);
            definitionRepositoryMock.Setup(m => m.Get(123)).Returns(definition);

            converters.Add(new ObjectFieldHtmlConverter());

            var repository = repositoryMock.Object;
            var definitionRepository = definitionRepositoryMock.Object;
            var contentTypeConverterService = new ObjectFieldContentTypeConverterService(converters);
            var interpreter = new ObjectInterpreter(repository, contentTypeConverterService);
            var listInterpreter = new ObjectListInterpreter(repository, definitionRepository);

            InterpreterFactory.Instance.Clear();
            InterpreterFactory.Instance.Register(interpreter);
            InterpreterFactory.Instance.Register(listInterpreter);

            var treeItem = new Engine().ParseTreeItem("{object(12).template(1)}");
            var result = InterpreterFactory.Instance.Calculate(treeItem);

            result = ((string)result).Replace("\n", "").Replace("\r", "");
            var exp = obj.Fields.Single().Value.Replace("\n", "").Replace("\r", "");

            Assert.Equal(exp, result);
        }

        [Fact]
        public void Then_iif_with_concat_and_dollarfunctions_works()
        {
            var expression = "{ iif($('alternative Zeitangabe'), '', concat($('Eventstart').format('HH:mm'), ' Uhr'), $('alternative Zeitangabe')) }";
            var obj = new DynamicObject();
            obj.Add(new DynamicObjectField { Name = "alternative Zeitangabe", Value = "9:15 / 11:15Uhr", ContentType = "text" });

            var converters = new List<IObjectFieldContentTypeConverter>();
            converters.Add(new ObjectFieldTextConverter());

            InterpreterFactory.Instance.Clear();
            InterpreterFactory.Instance.Register(new ObjectInterpreter(null, new ObjectFieldContentTypeConverterService(converters)));
            InterpreterFactory.Instance.Register(new StringInterpreter());
            InterpreterFactory.Instance.Register(new IifInterpreter());

            using (TemplateStateMap.Set(TemplateStateMap.CurrentObjectKey, obj))
            {
                var treeItem = new Engine().ParseTreeItem(expression);
                var result = InterpreterFactory.Instance.Calculate(treeItem);

                Assert.Equal(obj.Fields.Single().Value, result);
            }
        }
    }
}
