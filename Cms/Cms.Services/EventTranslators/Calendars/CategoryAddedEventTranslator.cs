using System;
using Cms.Aggregates.Calendars;
using Cms.Aggregates.Calendars.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Calendars
{
    internal class CategoryAddedEventTranslator : IEventTranslator
    {
        private readonly ICalendarRepository _repository;

        public CategoryAddedEventTranslator(ICalendarRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(CategoryAdded);
        }

        public string GetHeader(IEvent @event)
        {
            return "Kalenderkategorie erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (CategoryAdded)@event;
            return string.Format("Die Kalenderkategorie <strong>{0}</strong> wurde neu erstellt.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var calendar = _repository.GetEvenIfDeleted(@event.AggregateId);
            return calendar.TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "calendarcategory";
        }
    }
}