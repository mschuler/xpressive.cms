﻿(function() {
    angular.module("app").controller("ObjectTemplatesController", ["$rootScope", "$scope", "$routeParams", "$http", "$location", "$modal", "dialogService", "toastService", "tenantService", "$translate", function ($rootScope, $scope, $routeParams, $http, $location, $modal, dialogService, toastService, tenantService, $translate) {
        var definitionId = $routeParams.definitionId;

        $scope.templates = [];
        $scope.selectedTemplate = {};
        $scope.isShared = false;

        var tCreateTemplate = "";
        var tErrorCreateTemplate = "";
        var tName = "";
        var tRenameTemplate = "";
        var tDeleteTemplate = "";
        var tErrorRenameTemplate = "";
        var tConfirmDeleteTemplate = "";
        var tErrorDeleteTemplate = "";

        $http.get("/api/permission/objecttemplate/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $translate(["create_template", "error_create_template", "error_delete_template", "confirm_delete_template", "name", "rename_template", "delete_template", "error_rename_template"]).then(function (t) {
            tCreateTemplate = t.create_template;
            tName = t.name;
            tRenameTemplate = t.rename_template;
            tDeleteTemplate = t.delete_template;
            tErrorCreateTemplate = t.error_create_template;
            tErrorRenameTemplate = t.error_rename_template;
            tConfirmDeleteTemplate = t.confirm_delete_template;
            tErrorDeleteTemplate = t.error_delete_template;
        });

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id + "/shared", { cache: false }).success(function (data) {
            _.each(data, function (d) {
                if (d.id === definitionId) {
                    $scope.isShared = true;
                }
            });
        });

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id + "/" + definitionId + "/detail", { cache: false }).success(function (data) {
            $scope.definition = data;
            $scope.definitionName = data.fullname;
        });

        $scope.select = function(template) {
            $scope.selectedTemplate = template;
        };

        $scope.create = function() {
            dialogService.singleInputDialog(tCreateTemplate, tName, "", function (name) {
                $http.put("/api/objecttemplate/" + definitionId, { name: name }).success(function(dto) {
                    $scope.templates.push(dto);
                }).error(function (error) {
                    toastService.error(tErrorCreateTemplate, error);
                });
            });
        };

        $scope.edit = function (template) {
            $location.path("/objects/template/" + definitionId + "/" + template.id);
        };

        $scope.rename = function(template) {
            dialogService.singleInputDialog(tRenameTemplate, tName, template.name, function (newName) {
                $http.post("/api/objecttemplate/" + definitionId + "/" + template.id + "/rename", { name: newName }).success(function () {
                    template.name = newName;
                }).error(function (error) {
                    toastService.error(tErrorRenameTemplate, error);
                });
            });
        };

        $scope.delete = function (template) {
            var msg = String.format(tConfirmDeleteTemplate, template.name + " (Nr. " + template.publicId + ")");
            dialogService.confirmDialog(tDeleteTemplate, msg, function () {
                $http.delete("/api/objecttemplate/" + definitionId + "/" + template.id).success(function() {
                    $scope.templates = _.reject($scope.templates, function (t) { return t.id === template.id; });
                }).error(function(error) {
                    toastService.error(tErrorDeleteTemplate, error);
                });
            });
        };

        var getTemplates = function () {
            $http.get("/api/objecttemplates/" + tenantService.getTenant().id + "/" + definitionId, { cache: false }).success(function (data) {
                $scope.templates = data;
            });
        };

        $rootScope.$on('logout', function () { $scope.templates = []; });

        getTemplates();
    }]);

    angular.module("app").controller("ObjectTemplateController", ["$rootScope", "$scope", "$routeParams", "$http", "$log", "$translate", "dialogService", "toastService", "toastr", function ($rootScope, $scope, $routeParams, $http, $log, $translate, dialogService, toastService, toastr) {
        var definitionId = $routeParams.definitionId;
        var templateId = $routeParams.templateId;

        var tRenameTemplate = "";
        var tErrorRenameTemplate = "";
        var tName = "";
        var tErrorSaveTemplate = "";
        var tSuccessSaveTemplate = "";

        $scope.codemirrorOptions = {
            lineNumbers: true,
            theme: 'neo',
            lineWrapping: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            matchTags: { bothTags: true },
            autoCloseTags: true,
            styleActiveLine: true,
            viewportMargin: Infinity,
            mode: "htmlmixed",
        };

        $translate(["rename_template", "name", "error_rename_template", "error_save_template", "success_save_template"]).then(function (t) {
            tRenameTemplate = t.rename_template;
            tName = t.name;
            tErrorRenameTemplate = t.error_rename_template;
            tErrorSaveTemplate = t.error_save_template;
            tSuccessSaveTemplate = t.success_save_template;
        });

        $http.get("/api/objecttemplate/" + definitionId + "/" + templateId, { cache: false }).success(function(template) {
            $scope.template = template;
        });

        $scope.saveTemplate = function() {
            $http.post("/api/objecttemplate/" + definitionId + "/" + templateId + "/template", { template: $scope.template.template }).success(function() {
                toastr.success('', tSuccessSaveTemplate, { allowHtml: false, timeOut: 1000 });
            }).error(function(error) {
                toastService.error(tErrorSaveTemplate, error);
            });
        };

        $scope.editName = function() {
            dialogService.singleInputDialog(tRenameTemplate, tName, $scope.template.name, function (newName) {
                $http.post("/api/objecttemplate/" + definitionId + "/" + templateId + "/rename", { name: newName }).success(function () {
                    $scope.template.name = newName;
                }).error(function (error) {
                    toastService.error(tErrorRenameTemplate, error);
                });
            });
        };

        //$rootScope.$on('logout', function () { $scope.template = {}; });
    }]);

    angular.module("app").controller("ObjectDefinitionsForTemplatesController", ["$scope", "$rootScope", "$http", "$location", "tenantService", "$translate", function ($scope, $rootScope, $http, $location, tenantService, $translate) {
        $scope.definitions = [];

        $scope.title = "";

        $translate(["Templates"]).then(function (t) {
            $scope.title = t.Templates;
        });

        $rootScope.$on('logout', function () {
            $scope.definitions = [];
        });

        $scope.select = function (node) {
            $location.path("/objects/templates/" + node.id);
        };

        var getDefinitions = function () {
            var tenantId = tenantService.getTenant().id;
            $scope.definitions = [];

            $http.get("/api/objectdefinition/" + tenantId, { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = false;
                    $scope.definitions.push(d);
                });
            });

            $http.get("/api/objectdefinition/" + tenantId + "/shared", { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = true;
                    $scope.definitions.push(d);
                });
            });
        };

        $rootScope.$on("tenantChanged", getDefinitions);

        getDefinitions();
    }]);
}());