﻿namespace Cms.Aggregates.PageTemplates.Events
{
    public class PageTemplateCreated : EventBase<PageTemplate>
    {
        public ulong TenantId { get; set; }
        public string Name { get; set; }
        public int PublicId { get; set; }
        public string ContentType { get; set; }
    }
}