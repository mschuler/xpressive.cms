﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ValidIdAttribute : ValidationAttribute
    {
        public ValidIdAttribute()
            : base("Invalid aggregate id") { }

        public override bool IsValid(object value)
        {
            if (value is ulong)
            {
                var id = (ulong)value;
                return id > 0;
            }

            return false;
        }
    }
}