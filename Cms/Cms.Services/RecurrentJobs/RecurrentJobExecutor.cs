﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Services.Contracts;
using log4net;
using Quartz;
using Quartz.Impl;

namespace Cms.Services.RecurrentJobs
{
    internal class RecurrentJobExecutor : IRecurrentJobExecutor
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(RecurrentJobExecutor));
        private static readonly object _schedulerLock = new object();
        private static volatile IScheduler _scheduler;

        private readonly IDictionary<Guid, IRecurrentJob> _jobs = new Dictionary<Guid, IRecurrentJob>();

        public RecurrentJobExecutor(IEnumerable<IRecurrentJob> jobs)
        {
            _jobs = jobs.ToDictionary(j => Guid.NewGuid(), j => j);
        }

        public void Start()
        {
            if (!InitializeScheduler())
            {
                return;
            }

            foreach (var jobPair in _jobs)
            {
                var job = JobBuilder.Create<JobWrapper>()
                    .WithIdentity(jobPair.Key.ToString("n"))
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity(jobPair.Key.ToString("n"))
                    .WithCronSchedule(jobPair.Value.CronTab)
                    .Build();

                _scheduler.ScheduleJob(job, trigger);

                _log.InfoFormat("Registered job '{0}' with id '{1}'.", jobPair.Value.GetType().Name, jobPair.Key.ToString("n"));
            }
        }

        private bool InitializeScheduler()
        {
            if (_scheduler != null)
            {
                return false;
            }

            lock (_schedulerLock)
            {
                if (_scheduler != null)
                {
                    return false;
                }

                var factory = new StdSchedulerFactory();
                _scheduler = factory.GetScheduler();
                _scheduler.JobFactory = new JobFactory(_jobs);
                _scheduler.Start();
            }

            return true;
        }
    }
}
