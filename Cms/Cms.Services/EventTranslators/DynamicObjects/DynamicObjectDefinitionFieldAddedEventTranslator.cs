using System;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectDefinitionFieldAddedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectDefinitionFieldAddedEventTranslator(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectDefinitionFieldAdded);
        }

        public string GetHeader(IEvent @event)
        {
            return "Objektdefinition ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectDefinitionFieldAdded)@event;
            var definition = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format(
                "Der Objektdefinition <strong>{0} (Nr. {1})</strong> wurde das Feld <strong>{2}</strong> hinzugef�gt.",
                definition.Name,
                definition.PublicId,
                e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objectdefinition";
        }
    }
}