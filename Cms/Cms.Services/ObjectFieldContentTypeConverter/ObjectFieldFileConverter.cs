using System;
using Cms.Aggregates;
using Cms.Aggregates.Files;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using log4net;
using Newtonsoft.Json;

namespace Cms.Services.ObjectFieldContentTypeConverter
{
    internal class ObjectFieldFileConverter : IObjectFieldContentTypeConverter
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ObjectFieldFileConverter));
        private readonly IFileRepository _repository;
        private readonly IFileTrackingService _fileTrackingService;

        public ObjectFieldFileConverter(IFileRepository repository, IFileTrackingService fileTrackingService)
        {
            _repository = repository;
            _fileTrackingService = fileTrackingService;
        }

        public bool CanHandle(string type)
        {
            return type.Equals("file", StringComparison.OrdinalIgnoreCase);
        }

        public string Serialize(string json)
        {
            DtoWithId dto;

            try
            {
                dto = JsonConvert.DeserializeObject<DtoWithId>(json);
            }
            catch (Exception)
            {
                _log.ErrorFormat("Unable to deserialize to DtoWithId for json={0}", json);
                return "0";
            }

            ulong id;
            if (dto != null && ulong.TryParse(dto.Id, out id))
            {
                return dto.Id;
            }
            return "0";
        }

        public object Deserialize(string value)
        {
            var file = DeserializeToFile(value);
            if (file != null)
            {
                return file.ToDto();
            }
            return null;
        }

        public SortableObjectField DeserializeForSortableField(string value)
        {
            var file = DeserializeToFile(value);
            if (file != null)
            {
                return new SortableObjectField(file.Fullname);
            }
            return new SortableObjectField();
        }

        public object DeserializeForPublicApi(string value)
        {
            var file = DeserializeToFile(value);
            if (file != null)
            {
                return file.ToDownloadUrl(withTracking: true);
            }
            return null;
        }

        public bool TryGetMetadataForPublicApi(string value, out object metadata)
        {
            var file = DeserializeToFile(value);
            if (file == null)
            {
                metadata = null;
                return false;
            }

            metadata = new
            {
                viewCount = _fileTrackingService.GetTotalViewsPerFile(file.Id)
            };
            return true;
        }

        public object DeserializeForInterpreter(string value)
        {
            return DeserializeToFile(value);
        }

        public string SerializeForImport(string json, string format, string options)
        {
            int publicId;
            if (int.TryParse(json, out publicId))
            {
                var file = _repository.GetByPublicId(publicId);
                if (file != null)
                {
                    return file.Id.ToJsonString();
                }
            }
            return "0";
        }

        public string SerializeForImportPreview(string json, string format, string options)
        {
            int publicId;
            if (int.TryParse(json, out publicId))
            {
                var file = _repository.GetByPublicId(publicId);
                if (file != null)
                {
                    return file.Name + " (Nr. " + file.PublicId + ")";
                }
            }
            return json;
        }

        public bool IsValidValueForImport(string json, string format, string options)
        {
            int publicId;
            return int.TryParse(json, out publicId) && _repository.GetByPublicId(publicId) != null;
        }

        private File DeserializeToFile(string value)
        {
            ulong id;
            File file;
            if (ulong.TryParse(value, out id) && _repository.TryGet(id, out file))
            {
                return file;
            }
            return null;
        }

        public class DtoWithId
        {
            public string Id { get; set; }
        }
    }
}