using System;
using RestSharp;
using RestSharp.Deserializers;

namespace Cms.Services.UrlShortening
{
    internal class GooGlShorteningService : UrlShorteningServiceBase
    {
        private const string ApiKeyKey = "API Key";

        public GooGlShorteningService(IUrlShorteningRepository urlShorteningRepository)
            : base(new[] { ApiKeyKey }, urlShorteningRepository) { }

        public override string Name { get { return "goo.gl"; } }

        protected override string ShortenInternal(string longUrl)
        {
            string apiKey;
            if (!TryGetConfigurationValue(ApiKeyKey, out apiKey) || string.IsNullOrEmpty(apiKey))
            {
                throw new InvalidOperationException("Invalid configuration.");
            }

            var client = new RestClient("https://www.googleapis.com/urlshortener/v1/");

            var request = new RestRequest("url", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new { longUrl });
            request.AddQueryParameter("key", apiKey);

            var response = client.Execute<GooGlResult>(request);

            if (response == null)
            {
                throw new InvalidOperationException("Unable to access service.");
            }

            if (response.Data == null)
            {
                throw new InvalidOperationException("Unable to parse JSON result.");
            }

            return response.Data.Id;
        }

        public class GooGlResult
        {
            [DeserializeAs(Name = "kind")]
            public string Kind { get; set; }

            [DeserializeAs(Name = "id")]
            public string Id { get; set; }

            [DeserializeAs(Name = "longUrl")]
            public string LongUrl { get; set; }
        }
    }
}