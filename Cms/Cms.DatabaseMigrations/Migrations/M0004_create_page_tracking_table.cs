namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0004_create_page_tracking_table : DatabaseSqlMigration
    {
        public M0004_create_page_tracking_table() : base(4) { }

        protected override string GetSql()
        {
            return @"
create table PageTracking (
    Date smalldatetime not null,
    IpAddress nvarchar(50) not null,
    SessionId nvarchar(50) not null,
    PageId bigint not null,
    WebsiteId bigint not null,
    Country nvarchar(2) not null
)
create index ix_pt_dt on PageTracking (Date)
";
        }
    }
}