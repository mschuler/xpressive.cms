using System;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.Websites;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Pages
{
    internal class PageDeletedEventTranslator : IEventTranslator
    {
        private readonly IPageRepository _repository;
        private readonly IWebsiteRepository _websiteRepository;

        public PageDeletedEventTranslator(IPageRepository repository, IWebsiteRepository websiteRepository)
        {
            _repository = repository;
            _websiteRepository = websiteRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageDeleted);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seite gel�scht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageDeleted)@event;
            return string.Format("Die Seite <strong>{0}</strong> wurde gel�scht.", e.Fullname);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var page = _repository.GetEvenIfDeleted(@event.AggregateId);
            return _websiteRepository.GetEvenIfDeleted(page.WebsiteId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "page";
        }
    }
}