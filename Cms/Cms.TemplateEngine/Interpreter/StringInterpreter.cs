﻿using System;
using System.Linq;
using Cms.SharedKernel;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    public sealed class StringInterpreter : IInterpreter
    {
        public int Priority { get; set; }
        public string Name { get { return "String"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType != TreeItemType.Function && tree.Children.Count == 0)
            {
                return;
            }

            if (tree.Value.Equals("concat", StringComparison.OrdinalIgnoreCase))
            {
                tree.Result = string.Join(string.Empty, tree.Children.Select(c => c.Result)).RemoveControlChars();
            }

            if (tree.Children.Count == 1 && tree.Value.Equals("htmlencode", StringComparison.OrdinalIgnoreCase) && tree.Children[0].Result is string)
            {
                var child = ((string)tree.Children[0].Result).RemoveControlChars();
                tree.Result = System.Net.WebUtility.HtmlEncode(child);
            }

            if (tree.Children.Count == 3 &&
                tree.Value.Equals("replace", StringComparison.OrdinalIgnoreCase) &&
                tree.Children[0].Result is string &&
                tree.Children[1].Result is string &&
                tree.Children[2].Result is string)
            {
                var original = ((string)tree.Children[0].Result).RemoveControlChars();
                var search = ((string)tree.Children[1].Result).RemoveControlChars();
                var replace = ((string)tree.Children[2].Result).RemoveControlChars();

                tree.Result = original.Replace(search, replace);
            }
        }
    }
}