﻿namespace Cms.Aggregates.Security.Events
{
    public class GroupNameChanged : EventBase<Group>
    {
        public string Name { get; set; }
    }
}