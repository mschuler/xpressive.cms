﻿(function() {
    angular.module("app").controller("LanguageController", ["$rootScope", "$scope", "$translate", "$log", "userService", function($rootScope, $scope, $translate, $log, userService) {

        function setLanguageByUser() {
            var user = userService.currentUser();
            if (user && user.uiLanguage) {
                $scope.setLang(user.uiLanguage);
            }
        };

        $rootScope.$on("currentUserChanged", setLanguageByUser);

        $scope.setLang = function (lang) {
            $translate.use(lang);
            $scope.lang = lang.substring(0, 2).toLowerCase();
        };

        $scope.getFlag = function() {
            switch ($scope.lang) {
                case "en": return "flags-american";
                case "de": return "flags-germany";
                case "fr": return "flags-france";
                case "it": return "flags-italy";
            }
            return "flags-american";
        };

        setLanguageByUser();
    }]);
}());