using System;
using System.Globalization;
using System.Text;

namespace Cms.WebNew.Controllers.Admin
{
    internal static class CsvSettingsDtoExtensions
    {
        public static Encoding GetEncoding(this AppointmentImportController.CsvSettingsDto dto)
        {
            if (dto == null || string.IsNullOrEmpty(dto.Encoding))
            {
                return Encoding.UTF8;
            }

            return Encoding.GetEncoding(dto.Encoding);
        }
        public static Encoding GetEncoding(this ObjectImportController.CsvSettingsDto dto)
        {
            if (dto == null || string.IsNullOrEmpty(dto.Encoding))
            {
                return Encoding.UTF8;
            }

            return Encoding.GetEncoding(dto.Encoding);
        }

        public static DateTime? GetStart(this AppointmentImportController.CsvSettingsDto dto, string value)
        {
            if (dto == null || dto.Mapping == null || string.IsNullOrEmpty(dto.Mapping.StartFormat) || string.IsNullOrEmpty(value))
            {
                return null;
            }
            return GetDate(value, dto.Mapping.StartFormat);
        }

        public static DateTime? GetEnd(this AppointmentImportController.CsvSettingsDto dto, string value)
        {
            if (dto == null || dto.Mapping == null || string.IsNullOrEmpty(dto.Mapping.EndFormat) || string.IsNullOrEmpty(value))
            {
                return null;
            }
            return GetDate(value, dto.Mapping.EndFormat);
        }

        private static DateTime? GetDate(string value, string format)
        {
            DateTime dateTime;
            if (string.IsNullOrEmpty(format))
            {
                return DateTime.TryParse(value, out dateTime) ? (DateTime?)dateTime : null;
            }

            var formatInfo = new DateTimeFormatInfo();
            var formatParts = format.Split(' ');
            formatInfo.ShortDatePattern = formatParts[0];

            if (formatParts.Length > 1)
            {
                formatInfo.ShortTimePattern = formatParts[1];
            }

            if (DateTime.TryParse(value, formatInfo, DateTimeStyles.AdjustToUniversal, out dateTime))
            {
                return dateTime;
            }

            return null;
        }
    }
}