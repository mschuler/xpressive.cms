﻿namespace Cms.EventSourcing.Contracts.Commands
{
    public interface ICommand
    {
        ulong AggregateId { get; set; }

        ulong UserId { get; set; }

        ulong TenantId { get; set; }
    }
}