﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.FileDirectories.Commands
{
    [RequiredPermission("file", PermissionLevel.CanEverything)]
    public class ShareFileDirectory : CommandBase
    {
        [Required]
        public ulong[] TenantIds { get; set; }
    }
}