﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Queries
{
    [RequiredPermission("objectdefinition", PermissionLevel.CanSee)]
    [RequiredPermission("object", PermissionLevel.CanSee)]
    public class GetAllObjectDefinitions : QueryBase { }
}