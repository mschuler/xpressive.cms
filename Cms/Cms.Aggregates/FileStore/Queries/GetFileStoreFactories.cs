﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.FileStore.Queries
{
    [RequiredPermission("sysadmin", PermissionLevel.CanEverything)]
    public class GetFileStoreFactories : QueryBase { }
}