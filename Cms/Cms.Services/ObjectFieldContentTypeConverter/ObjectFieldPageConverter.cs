﻿using System;
using Cms.Aggregates;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Websites;
using Cms.SharedKernel;
using log4net;
using Newtonsoft.Json;

namespace Cms.Services.ObjectFieldContentTypeConverter
{
    internal class ObjectFieldPageConverter : IObjectFieldContentTypeConverter
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ObjectFieldPageConverter));
        private readonly IPageRepository _repository;
        private readonly IWebsiteRepository _websiteRepository;
        private readonly IPageDtoService _dtoService;

        public ObjectFieldPageConverter(IPageRepository repository, IPageDtoService dtoService, IWebsiteRepository websiteRepository)
        {
            _repository = repository;
            _dtoService = dtoService;
            _websiteRepository = websiteRepository;
        }

        public bool CanHandle(string type)
        {
            return type.Equals("page", StringComparison.OrdinalIgnoreCase);
        }

        public string Serialize(string json)
        {
            DtoWithId dto;

            try
            {
                dto = JsonConvert.DeserializeObject<DtoWithId>(json);
            }
            catch (Exception)
            {
                _log.ErrorFormat("Unable to deserialize to DtoWithId for json={0}", json);
                return "0";
            }

            ulong id;
            if (dto != null && ulong.TryParse(dto.Id, out id))
            {
                return dto.Id;
            }
            return "0";
        }

        public object Deserialize(string value)
        {
            var page = DeserializeToPage(value);
            if (page != null)
            {
                var website = _websiteRepository.Get(page.WebsiteId);
                return _dtoService.GetPageDto(website, page);
            }
            return null;
        }

        public SortableObjectField DeserializeForSortableField(string value)
        {
            var page = DeserializeToPage(value);
            if (page != null)
            {
                return new SortableObjectField(page.Fullname);
            }
            return new SortableObjectField();
        }

        public object DeserializeForPublicApi(string value)
        {
            return value;
        }

        public bool TryGetMetadataForPublicApi(string value, out object metadata)
        {
            metadata = null;
            return false;
        }

        public object DeserializeForInterpreter(string value)
        {
            return DeserializeToPage(value);
        }

        public string SerializeForImport(string json, string format, string options)
        {
            int publicId;
            if (int.TryParse(json, out publicId))
            {
                var page = _repository.GetByPublicId(publicId);
                if (page != null)
                {
                    return page.Id.ToJsonString();
                }
            }
            return "0";
        }

        public string SerializeForImportPreview(string json, string format, string options)
        {
            int publicId;
            if (int.TryParse(json, out publicId))
            {
                var page = _repository.GetByPublicId(publicId);
                if (page != null)
                {
                    return page.Title + " (Nr. " + page.PublicId + ")";
                }
            }
            return json;
        }

        public bool IsValidValueForImport(string json, string format, string options)
        {
            int publicId;
            return int.TryParse(json, out publicId) && _repository.GetByPublicId(publicId) != null;
        }

        private PageBase DeserializeToPage(string value)
        {
            ulong id;
            PageBase page;
            if (ulong.TryParse(value, out id) && _repository.TryGet(id, out page))
            {
                return page;
            }
            return null;
        }

        public class DtoWithId
        {
            public string Id { get; set; }
        }
    }
}