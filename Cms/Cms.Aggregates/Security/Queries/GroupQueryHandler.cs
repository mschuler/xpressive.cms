﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Security.Queries
{
    internal class GroupQueryHandler : QueryHandlerBase
    {
        private readonly IGroupRepository _repository;

        public GroupQueryHandler(IGroupRepository repository)
        {
            _repository = repository;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetGroupMembers),
                typeof (GetAllGroups)
            };
        }

        public IEnumerable<object> When(GetGroupMembers query)
        {
            var group = _repository.Get(query.GroupId);
            return group.Members.Select(m => new
            {
                Id = m.Id.ToJsonString(),
                m.FirstName,
                m.LastName,
                Email = m.EmailAddress,
                m.IsLocked,
            })
            .ToList();
        }

        public IEnumerable<object> When(GetAllGroups query)
        {
            return _repository
                .GetAll(query.TenantId)
                .OrderBy(g => g.Name)
                .Select(g => new
                {
                    Id = g.Id.ToJsonString(),
                    g.Name
                });
        }
    }
}
