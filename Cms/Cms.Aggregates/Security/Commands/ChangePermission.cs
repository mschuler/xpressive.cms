using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("group", PermissionLevel.CanEverything)]
    public class ChangePermission : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string ModuleId { get; set; }

        public PermissionLevel Permission { get; set; }
    }
}