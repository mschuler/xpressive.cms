using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Security;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using CsvHelper;

namespace Cms.WebNew.Controllers.Admin
{
    public abstract class CsvImportControllerBase : ApiControllerBase
    {
        private readonly ITemporaryFileStoreService _temporaryFileStoreService;
        private readonly IUtf8Checker _utf8Checker;

        protected CsvImportControllerBase(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IUtf8Checker utf8Checker, ITemporaryFileStoreService temporaryFileStoreService)
            : base(commandQueue, userRepository, queryHandler)
        {
            _utf8Checker = utf8Checker;
            _temporaryFileStoreService = temporaryFileStoreService;
        }

        public virtual object StartImport()
        {
            return new
            {
                Id = _temporaryFileStoreService.Create().Id.ToJsonString(),
            };
        }

        public virtual IEnumerable<object> GetEncodings()
        {
            var encodings = Encoding.GetEncodings().ToList();
            encodings = encodings
                .Where(e => e.DisplayName.IndexOf("Arabic", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Baltic", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Chinese", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Croatian", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Cyrillic", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Estonian", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Greek", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Hebrew", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("EBCDIC", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("ISCII", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Japanese", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Korean", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Taiwan", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Turkish", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Thai", StringComparison.OrdinalIgnoreCase) < 0)
                .Where(e => e.DisplayName.IndexOf("Vietnamese", StringComparison.OrdinalIgnoreCase) < 0)
                .ToList();

            return encodings
                .OrderBy(e => e.DisplayName, StringComparer.OrdinalIgnoreCase)
                .Select(e => new
                {
                    e.Name,
                    e.DisplayName
                });
        }

        public virtual string GetEncoding(ulong id)
        {
            FileInfo file;
            if (!TryGetTemporaryFile(id, out file))
            {
                return "utf-8";
            }

            if (_utf8Checker.Check(file.FullName))
            {
                return "utf-8";
            }

            using (var reader = new StreamReader(file.FullName, Encoding.Default, true))
            {
                reader.ReadToEnd();
                if (!Equals(reader.CurrentEncoding, Encoding.Default))
                {
                    var encodingInfo = Encoding.GetEncodings().FirstOrDefault(e => e.CodePage == reader.CurrentEncoding.CodePage);
                    if (encodingInfo != null)
                    {
                        return encodingInfo.Name;
                    }
                }
            }

            return Encoding.GetEncodings().Single(e => e.CodePage == Encoding.Default.CodePage).Name;
        }

        public virtual async Task UploadFile(ulong id)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType).WithGuid();
            }

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);

            foreach (var file in provider.Contents)
            {
                ITemporaryFile temporaryFile;
                if (_temporaryFileStoreService.TryGet(id, out temporaryFile))
                {
                    var buffer = await file.ReadAsByteArrayAsync();
                    File.WriteAllBytes(temporaryFile.File.FullName, buffer);
                }
            }
        }

        public virtual IEnumerable<object> GetColumns(ulong id, string delimiter, bool hasHeaderRecord, Encoding encoding)
        {
            FileInfo file;
            if (!TryGetTemporaryFile(id, out file) || !file.Exists)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            Func<IEnumerable<KeyValuePair<string, string>>, IEnumerable<object>> getColumns = p =>
                p.Select(h => new { Header = h.Value, Value = h.Key });

            using (var textReader = new StreamReader(file.FullName, encoding))
            {
                using (var csvReader = new CsvReader(textReader))
                {
                    csvReader.Configuration.Delimiter = delimiter;
                    csvReader.Configuration.HasHeaderRecord = hasHeaderRecord;

                    csvReader.Read();

                    if (csvReader.FieldHeaders != null)
                    {
                        var h = csvReader.FieldHeaders.Select(fh => new KeyValuePair<string, string>(fh, fh));
                        var columns = getColumns(h).ToList();
                        columns.Insert(0, new { Header = string.Empty, Value = string.Empty });
                        return columns;
                    }

                    if (csvReader.CurrentRecord == null)
                    {
                        return new object[0];
                    }

                    var headers = new Dictionary<string, string>();
                    for (var i = 0; i < csvReader.CurrentRecord.Length; i++)
                    {
                        headers.Add(i.ToString(CultureInfo.InvariantCulture), string.Format("Column {0} ({1})", i + 1, csvReader.CurrentRecord[i]));
                    }

                    var c = getColumns(headers).ToList();
                    c.Insert(0, new { Header = string.Empty, Value = string.Empty });
                    return c;
                }
            }
        }

        protected string GetField(CsvReader reader, string header, bool hasHeaderRecord)
        {
            if (string.IsNullOrEmpty(header))
            {
                return string.Empty;
            }

            int index;
            string value;

            if (hasHeaderRecord && reader.TryGetField(header, out value))
            {
                return value;
            }
            if (!hasHeaderRecord && int.TryParse(header, out index) && reader.TryGetField(index, out value))
            {
                return value;
            }

            return string.Empty;
        }

        protected bool TryGetTemporaryFile(ulong id, out FileInfo file)
        {
            ITemporaryFile temporaryFile;

            if (_temporaryFileStoreService.TryGet(id, out temporaryFile))
            {
                file = temporaryFile.File;
                return true;
            }

            file = null;
            return false;
        }
    }
}