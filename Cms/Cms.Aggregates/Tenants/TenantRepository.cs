﻿namespace Cms.Aggregates.Tenants
{
    internal class TenantRepository : RepositoryBase<Tenant>, ITenantRepository
    {
        public override void DeleteAllByTenantId(ulong tenantId)
        {
            // don't do anything
        }
    }
}