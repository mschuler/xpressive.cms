﻿namespace Cms.Aggregates.SystemMessages
{
    public class SystemMessage : AggregateBase
    {
        public ulong UserId { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTimeRange Validity { get; set; }
    }
}
