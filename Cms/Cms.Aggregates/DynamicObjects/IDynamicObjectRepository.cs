﻿using System.Collections.Generic;

namespace Cms.Aggregates.DynamicObjects
{
    public interface IDynamicObjectRepository : IRepository<DynamicObject>
    {
        int GetNextPublicId();

        DynamicObject GetByPublicId(int publicId);

        IEnumerable<DynamicObject> GetAll(ulong tenantId, ulong definitionId);
    }
}