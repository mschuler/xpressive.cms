using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Security;
using Cms.Services.Contracts;
using u2flib;
using u2flib.Data;
using u2flib.Data.Messages;
using u2flib.Util;

namespace Cms.Services._2FA
{
    internal class U2FService : IU2FService
    {
        private static readonly ConcurrentDictionary<ulong, List<StartedRegistration>> _userDeviceRegistrations = new ConcurrentDictionary<ulong, List<StartedRegistration>>();
        private static readonly ConcurrentDictionary<ulong, List<StartedAuthentication>> _userStartedAuthentications = new ConcurrentDictionary<ulong, List<StartedAuthentication>>();

        public IDeviceRegistration StartRegistration(ulong userId, string appId)
        {
            var deviceRegistrations = _userDeviceRegistrations.GetOrAdd(userId, new List<StartedRegistration>());
            var registration = U2F.StartRegistration(appId);
            deviceRegistrations.Add(registration);

            return new U2FDeviceRegistration(registration.AppId, registration.Challenge, registration.Version);
        }

        public bool TryFinishRegistration(ulong userId, string registerResponse, out IDevice device)
        {
            device = null;
            List<StartedRegistration> registrations;
            if (!_userDeviceRegistrations.TryGetValue(userId, out registrations))
            {
                return false;
            }

            try
            {
                var response = DataObject.FromJson<RegisterResponse>(registerResponse);
                var challenge = response.GetClientData().Challenge;

                var registration = registrations.SingleOrDefault(r => r.Challenge.Equals(challenge, StringComparison.Ordinal));
                if (registration == null)
                {
                    return false;
                }

                var deviceRegistration = U2F.FinishRegistration(registration, response);
                var counter = Convert.ToInt32(deviceRegistration.Counter);
                device = new U2FDevice(deviceRegistration.AttestationCert, counter, deviceRegistration.KeyHandle, deviceRegistration.PublicKey);

                _userDeviceRegistrations.TryRemove(userId, out registrations);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IDeviceVerification StartVerification(User user, string appId)
        {
            var device = user.U2FDevices.FirstOrDefault();

            if (device == null)
            {
                return null;
            }

            var counter = Convert.ToUInt32(device.Counter);

            var deviceRegistration = new DeviceRegistration(device.KeyHandle, device.PublicKey, device.AttestationCert, counter);
            var startedAuthentication = U2F.StartAuthentication(appId, deviceRegistration);

            return new U2FDeviceVerification(startedAuthentication.AppId, startedAuthentication.Challenge, startedAuthentication.KeyHandle, startedAuthentication.Version);
        }

        public bool TryFinishVerification(User user, string authenticateResponse)
        {
            try
            {
                List<StartedAuthentication> authentications;
                if (!_userStartedAuthentications.TryGetValue(user.Id, out authentications))
                {
                    return false;
                }

                var response = DataObject.FromJson<AuthenticateResponse>(authenticateResponse);
                var challenge = response.GetClientData().Challenge;
                var startedAuthentication = authentications.SingleOrDefault(a => a.Challenge == challenge);

                if (startedAuthentication == null)
                {
                    return false;
                }

                var keyHandle = Utils.Base64StringToByteArray(startedAuthentication.KeyHandle);
                var device = user.U2FDevices.SingleOrDefault(d => d.KeyHandle.SequenceEqual(keyHandle));

                if (device == null)
                {
                    return false;
                }

                var counter = Convert.ToUInt32(device.Counter);

                var deviceRegistration = new DeviceRegistration(device.KeyHandle, device.PublicKey, device.AttestationCert, counter);

                U2F.FinishAuthentication(startedAuthentication, response, deviceRegistration);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private class U2FDeviceRegistration : IDeviceRegistration
        {
            public U2FDeviceRegistration(string appId, string challenge, string version)
            {
                Version = version;
                Challenge = challenge;
                AppId = appId;
            }

            public string AppId { get; private set; }
            public string Challenge { get; private set; }
            public string Version { get; private set; }
        }

        private class U2FDevice : IDevice
        {
            public U2FDevice(byte[] attestationCert, int counter, byte[] keyHandle, byte[] publicKey)
            {
                AttestationCert = attestationCert;
                Counter = counter;
                KeyHandle = keyHandle;
                PublicKey = publicKey;
            }

            public byte[] AttestationCert { get; private set; }
            public int Counter { get; private set; }
            public byte[] KeyHandle { get; private set; }
            public byte[] PublicKey { get; private set; }
        }

        private class U2FDeviceVerification : IDeviceVerification
        {
            public U2FDeviceVerification(string appId, string challenge, string keyHandle, string version)
            {
                AppId = appId;
                Challenge = challenge;
                KeyHandle = keyHandle;
                Version = version;
            }

            public string AppId { get; private set; }
            public string Challenge { get; private set; }
            public string KeyHandle { get; private set; }
            public string Version { get; private set; }
        }
    }
}