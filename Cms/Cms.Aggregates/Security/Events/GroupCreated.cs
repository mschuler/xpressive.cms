namespace Cms.Aggregates.Security.Events
{
    public class GroupCreated : EventBase<Group>
    {
        public ulong TenantId { get; set; }
        public string Name { get; set; }
    }
}