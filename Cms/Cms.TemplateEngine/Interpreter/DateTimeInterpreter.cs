﻿using System;
using System.Globalization;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    public sealed class DateTimeInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name
        {
            get { return "DateTime"; }
        }

        public void Calculate(TreeItem tree)
        {
            Interprete(tree, TemplateStateMap.CurrentCulture);
        }

        private static void Interprete(TreeItem treeItem, CultureInfo culture)
        {
            if (treeItem.ItemType == TreeItemType.Function)
            {
                if (treeItem.Value.Equals("now"))
                {
                    treeItem.Result = DateTime.Now;
                }
                else if (treeItem.LeftItem != null && treeItem.LeftItem.Result is DateTime)
                {
                    var dateTime = (DateTime)treeItem.LeftItem.Result;

                    switch (treeItem.Value.ToLowerInvariant())
                    {
                        case "torfc822":
                        case "torfc1123":
                            treeItem.Result = string.Format(culture, "{0:r}", dateTime);
                            break;
                    }

                    if (treeItem.Children != null && treeItem.Children.Count == 1 && treeItem.Children[0].Result is int)
                    {
                        var parameter = (int)treeItem.Children[0].Result;

                        switch (treeItem.Value.ToLowerInvariant())
                        {
                            case "addyear":
                            case "addyears":
                                treeItem.Result = dateTime.AddYears(parameter);
                                break;
                            case "addday":
                            case "adddays":
                                treeItem.Result = dateTime.AddDays(parameter);
                                break;
                            case "addhour":
                            case "addhours":
                                treeItem.Result = dateTime.AddHours(parameter);
                                break;
                            case "addminute":
                            case "addminutes":
                                treeItem.Result = dateTime.AddMinutes(parameter);
                                break;
                            case "addsecond":
                            case "addseconds":
                                treeItem.Result = dateTime.AddSeconds(parameter);
                                break;
                            case "addmonth":
                            case "addmonths":
                                treeItem.Result = dateTime.AddMonths(parameter);
                                break;
                        }
                    }
                }
            }
            else if (treeItem.ItemType == TreeItemType.Attribute)
            {
                if (treeItem.LeftItem != null && treeItem.LeftItem.Result is DateTime)
                {
                    var dateTime = (DateTime)treeItem.LeftItem.Result;

                    switch (treeItem.Value.ToLower())
                    {
                        case "date":
                            treeItem.Result = dateTime.ToShortDateString();
                            break;
                        case "time":
                            treeItem.Result = dateTime.ToShortTimeString();
                            break;
                        case "year":
                            treeItem.Result = dateTime.Year;
                            break;
                        case "month":
                            treeItem.Result = dateTime.Month;
                            break;
                        case "day":
                            treeItem.Result = dateTime.Day;
                            break;
                    }
                }
            }
        }
    }
}
