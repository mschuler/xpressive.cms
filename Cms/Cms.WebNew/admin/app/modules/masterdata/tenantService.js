﻿(function() {
    angular.module("app").provider("tenantService", function() {
        var current = null;
        var allTenants = [];

        this.$get = [
            "$rootScope", "$http", "$log", "$q", "localStorageService", "currentTenant", "tenants", "systemMessageService",
            function ($rootScope, $http, $log, $q, localStorageService, currentTenant, tenants, systemMessageService) {

                $rootScope.$on("logout", function() {
                    allTenants = [];
                    localStorageService.set("tenants", allTenants);
                });

                allTenants = tenants;
                current = currentTenant;

                return {
                    reloadTenants: function () {
                        var deferred = $q.defer();
                        allTenants = [];
                        localStorageService.set("tenants", allTenants);
                        $http.get("/api/tenants", { cache: false }).success(function (data) {
                            allTenants = data;
                            localStorageService.set("tenants", allTenants);
                            var storedCurrentTenant = localStorageService.get('currentTenant');
                            if (storedCurrentTenant) {
                                current = _.find(allTenants, function (t) { return t.id == storedCurrentTenant.id; });
                            }
                            if (current == null) {
                                current = allTenants[0];
                            }
                            $rootScope.$broadcast('tenantChanged');
                            deferred.resolve(current);
                        }).error(function (error) {
                            deferred.reject(error);
                        });
                        return deferred.promise;
                    },
                    setTenant: function (tenant) {
                        current = tenant;
                        angular.module("app").value("currentTenant", tenant);
                        localStorageService.set('currentTenant', tenant);
                        $rootScope.$broadcast('tenantChanged', tenant);
                    },
                    getTenant: function () {
                        if (current) {
                            return current;
                        }
                        if (!allTenants || allTenants.length == 0) {
                            allTenants = localStorageService.get("tenants");
                        }
                        if (allTenants && allTenants.length > 0) {
                            var storedCurrentTenant = localStorageService.get('currentTenant');
                            if (storedCurrentTenant) {
                                current = _.find(allTenants, function (t) { return t.id == storedCurrentTenant.id; });
                                return current;
                            }
                        }
                        return null;
                    },
                    getTenants: function () {
                        if (allTenants && allTenants.length > 0) {
                            return allTenants.slice(0);
                        }
                        allTenants = localStorageService.get("tenants");
                        return allTenants && allTenants.length > 0 ? allTenants.slice(0) : [];
                    },
                    addTenant: function(tenant) {
                        allTenants.push(tenant);
                        localStorageService.set("tenants", allTenants);
                        $rootScope.$broadcast('tenantsChanged');
                    },
                };
            }
        ];
    });
}());