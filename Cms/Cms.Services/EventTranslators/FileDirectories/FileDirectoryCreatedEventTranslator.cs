using System;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.FileDirectories.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.FileDirectories
{
    internal class FileDirectoryCreatedEventTranslator : IEventTranslator
    {
        private readonly IRepository<FileDirectory> _repository;

        public FileDirectoryCreatedEventTranslator(IRepository<FileDirectory> repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FileDirectoryCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Neues Dateiverzeichnis erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FileDirectoryCreated)@event;
            return string.Format("Das Dateiverzeichnis <strong>{0}</strong> wurde neu erstellt.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "file";
        }
    }
}