using System;

namespace Cms.EventSourcing.Services
{
    public class ExceptionWithReason : Exception
    {
        public ExceptionWithReason(string message) : base(message) { }
    }
}