using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission(false)]
    public class AddU2FDevice : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Password { get; set; }

        [Required]
        public byte[] PublicKey { get; set; }

        [Required]
        public byte[] AttestationCert { get; set; }

        [Required]
        public byte[] KeyHandle { get; set; }
    }
}