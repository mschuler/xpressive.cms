using System;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.Websites;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Pages
{
    internal class PageLinkCreatedEventTranslator : IEventTranslator
    {
        private readonly IPageRepository _repository;
        private readonly IWebsiteRepository _websiteRepository;

        public PageLinkCreatedEventTranslator(IPageRepository repository, IWebsiteRepository websiteRepository)
        {
            _repository = repository;
            _websiteRepository = websiteRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageLinkCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Verlinkte Seite erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageLinkCreated)@event;
            return string.Format("Die verlinkte Seite <strong>{0}</strong> wurde erstellt.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var page = _repository.GetEvenIfDeleted(@event.AggregateId);
            return _websiteRepository.GetEvenIfDeleted(page.WebsiteId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "page";
        }
    }
}