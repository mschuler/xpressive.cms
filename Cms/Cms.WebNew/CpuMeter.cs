using System;
using System.Diagnostics;
using log4net;

namespace Cms.WebNew
{
    internal static class CpuMeter
    {
        private static readonly PerformanceCounter _cnt;
        private static CounterSample _previousSample;

        static CpuMeter()
        {
            try
            {
                var instancename = GetCurrentProcessInstanceName();
                _cnt = new PerformanceCounter("Process", "% Processor Time", instancename, true);
                _previousSample = _cnt.NextSample();
            }
            catch (Exception e)
            {
                LogManager.GetLogger(typeof(CpuMeter)).Error("Unable to create CPUMeter.", e);
            }
        }

        public static double GetCpuUtilization()
        {
            if (_cnt == null)
            {
                return 0;
            }

            var sample = _cnt.NextSample();

            double diffValue = sample.RawValue - _previousSample.RawValue;
            double diffTimestamp = sample.TimeStamp100nSec - _previousSample.TimeStamp100nSec;
            double usage = (diffValue / diffTimestamp) * 100;

            _previousSample = sample;

            return usage;
        }

        private static string GetCurrentProcessInstanceName()
        {
            var proc = Process.GetCurrentProcess();
            return GetProcessInstanceName(proc.Id);
        }

        private static string GetProcessInstanceName(int pid)
        {
            var cat = new PerformanceCounterCategory("Process");
            var instances = cat.GetInstanceNames();

            foreach (var instance in instances)
            {
                using (var cnt = new PerformanceCounter("Process", "ID Process", instance, true))
                {
                    var val = (int)cnt.RawValue;
                    if (val == pid)
                    {
                        return instance;
                    }
                }
            }

            throw new Exception("Could not find performance counter instance name for current process. This is truly strange ...");
        }
    }
}