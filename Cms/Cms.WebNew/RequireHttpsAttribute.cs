using System;
using System.Collections.Generic;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Cms.WebNew.Controllers.Admin;
using Cms.WebNew.Controllers.PageRendering;
using WebApiContrib.ResponseMessages;

namespace Cms.WebNew
{
    public class RequireHttpsAttribute : AuthorizationFilterAttribute
    {
        private static readonly HashSet<Type> _excludedControllers = new HashSet<Type>();

        static RequireHttpsAttribute()
        {
            _excludedControllers.Add(typeof(FileDownloadController));
            _excludedControllers.Add(typeof(FormEntryController));
            _excludedControllers.Add(typeof(FormQueryController));
            _excludedControllers.Add(typeof(ObjectQueryController));
            _excludedControllers.Add(typeof(PublicUrlShorteningController));
            _excludedControllers.Add(typeof(PageRenderingController));
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var isExcluded = _excludedControllers.Contains(actionContext.ControllerContext.Controller.GetType());

            if (isExcluded)
            {
                base.OnAuthorization(actionContext);
                return;
            }

            if (actionContext.Request.IsLocal())
            {
                base.OnAuthorization(actionContext);
                return;
            }

            if (Uri.UriSchemeHttps.Equals(actionContext.Request.RequestUri.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                base.OnAuthorization(actionContext);
                return;
            }

            var uri = actionContext.Request.RequestUri.OriginalString;
            uri = uri.Substring(4);
            uri = "https" + uri;
            actionContext.Response = new RedirectResponse(new Uri(uri));
        }
    }
}