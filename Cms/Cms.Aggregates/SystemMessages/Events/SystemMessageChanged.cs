﻿namespace Cms.Aggregates.SystemMessages.Events
{
    public class SystemMessageChanged : EventBase<SystemMessage>
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public DateTimeRange Validity { get; set; }
    }
}