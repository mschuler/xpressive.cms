﻿(function() {
    angular.module("app").service("systemMessageService", ["$http", "$log", "$interval", "$timeout", "toastr", function($http, $log, $interval, $timeout, toastr) {
        function getMessages() {
            $http.get("/api/systemmessages/current", { cache: false }).success(function(data) {
                _.each(data, function (msg) {
                    var body = msg.message + "<br/> - " + msg.user;
                    toastr.warning(body, msg.title, {
                        allowHtml: true,
                        closeButton: true,
                        timeOut: 0,
                        extendedTimeOut: 0
                    });
                });
            });
        };

        $timeout(getMessages, 10000); // show messages the first time after 10 seconds
        $interval(getMessages, 600000); // show messages all 10 minutes
    }]);

    angular.module("app").controller("SystemMessagesController", ["$rootScope", "$scope", "$http", "$modal", "tenantService", "toastService", "dialogService", "$translate", function ($rootScope, $scope, $http, $modal, tenantService, toastService, dialogService, $translate) {
        $scope.selectedMessage = {};

        $http.get("/api/systemmessages/", { cache: false }).success(function (data) {
            $scope.messages = data;
        });

        $scope.select = function(message) {
            $scope.selectedMessage = message;
        };

        var createMessage = "";
        var errorCreateMessage = "";
        var editMessage = "";
        var errorEditMessage = "";
        var deleteMessage = "";
        var confirmDeleteMessage = "";
        var errorDeleteMessage = "";

        $translate(["create_system_message", "error_create_system_message", "edit_system_message", "error_edit_system_message", "delete_system_message", "confirm_delete_system_message", "error_delete_system_message"]).then(function(t) {
            createMessage = t.create_system_message;
            errorCreateMessage = t.error_create_system_message;
            editMessage = t.edit_system_message;
            errorEditMessage = t.error_edit_system_message;
            deleteMessage = t.delete_system_message;
            confirmDeleteMessage = t.confirm_delete_system_message;
            errorDeleteMessage = t.error_delete_system_message;
        });

        $scope.create = function() {
            openMessageDialog(createMessage, {}, function(m) {
                $http.post("/api/systemmessages/" + tenantService.getTenant().id, m).success(function(message) {
                    $scope.messages.push(message);
                }).error(function(error) {
                    toastService.error(errorCreateMessage, error);
                });
            });
        };

        $scope.edit = function(message) {
            openMessageDialog(editMessage, message, function(m) {
                $http.post("/api/systemmessages/" + tenantService.getTenant().id + "/" + message.id, m).success(function (data) {
                    message.title = data.title;
                    message.message = data.message;
                    message.validity = data.validity;
                    message.startDate = data.startDate;
                    message.endDate = data.endDate;
                    message.startTime = data.startTime;
                    message.endTime = data.endTime;
                }).error(function (error) {
                    toastService.error(errorEditMessage , error);
                });
            });
        };

        $scope.delete = function (message) {
            dialogService.confirmDialog(deleteMessage, String.format(confirmDeleteMessage, message.title), function() {
                $http.delete("/api/systemmessages/" + tenantService.getTenant().id + "/" + message.id).success(function () {
                    $scope.messages = _.reject($scope.messages, function (m) { return m.id === message.id; });
                }).error(function (error) {
                    toastService.error(errorDeleteMessage, error);
                });
            });
        };

        function openMessageDialog(subject, message, func) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/views/system/messageDialog.min.html',
                controller: 'SystemMessageDialogController',
                resolve: {
                    subject: function () {
                        return subject;
                    },
                    message: function () {
                        return angular.copy(message);
                    },
                }
            });

            modalInstance.result.then(function (a) {
                func(a);
            });
        };

        $rootScope.$on("logout", function() { $scope.messages = []; });
    }]);

    angular.module("app").controller("SystemMessageDialogController", ["$scope", "$modalInstance", "$filter", "$translate", "subject", "message", function ($scope, $modalInstance, $filter, $translate, subject, message) {
        $scope.subject = subject;
        $scope.message = message;
        $scope.times = [];

        for (var i = 0; i < 24; i++) {
            var hour = i + ":";
            if (hour.length < 3) {
                hour = "0" + hour;
            }

            for (var m = 0; m < 60; m += 5) {
                var minute = m;
                if (m < 10) {
                    minute = "0" + minute;
                }

                $scope.times.push(hour + minute);
            }
        }

        if ($scope.message.startDate) {
            $scope.message.startDatePicker = $scope.message.startDate;
        }
        if ($scope.message.endDate) {
            $scope.message.endDatePicker = $scope.message.endDate;
        }

        $scope.$watch("message.startTime", updateDuration);
        $scope.$watch("message.endTime", updateDuration);
        $scope.$watch("message.startDatePicker", updateDuration);
        $scope.$watch("message.endDatePicker", updateDuration);

        var t_minutes = "";
        var t_hours = "";
        var t_days = "";

        $translate(["minutes", "hours", "days"]).then(function(t) {
            t_minutes = t.minutes;
            t_hours = t.hours;
            t_days = t.days;
        });

        function updateDuration() {
            if ($scope.message.startDatePicker && $scope.message.endDatePicker && $scope.message.startTime && $scope.message.endTime) {
                var s = $scope.message.startTime.split(':');
                var e = $scope.message.endTime.split(':');

                $scope.message.startDate = $filter('date')($scope.message.startDatePicker, 'yyyy-MM-dd') + "T00:00:00.000Z";
                $scope.message.endDate = $filter('date')($scope.message.endDatePicker, 'yyyy-MM-dd') + "T00:00:00.000Z";

                $scope.start = moment($scope.message.startDate);
                $scope.start.add(parseInt(s[0]), 'hours');
                $scope.start.add(parseInt(s[1]), 'minutes');
                $scope.end = moment($scope.message.endDate);
                $scope.end.add(parseInt(e[0]), 'hours');
                $scope.end.add(parseInt(e[1]), 'minutes');

                var minutes = $scope.end.diff($scope.start, 'minutes');
                if (minutes < 60) {
                    $scope.duration = toReadableNumber(minutes) + " " + t_minutes;
                    return;
                }
                var hours = $scope.end.diff($scope.start, 'hours', true);
                if (hours < 24) {
                    $scope.duration = toReadableNumber(hours) + " " + t_hours;
                    return;
                }
                var days = $scope.end.diff($scope.start, 'days', true);
                $scope.duration = toReadableNumber(days) + " " + t_days;
            }
        };

        function toReadableNumber(number) {
            var n = number.toFixed(2);
            if (n.charAt(n.length - 1) == "0") {
                n = n.substring(0, n.length - 1);
                if (n.charAt(n.length - 1) == "0") {
                    n = n.substring(0, n.length - 2);
                }
            }
            return n;
        };

        $scope.openStartDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.startDateOpened = true;
        };

        $scope.openEndDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.endDateOpened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.ok = function () {
            $modalInstance.close($scope.message);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());