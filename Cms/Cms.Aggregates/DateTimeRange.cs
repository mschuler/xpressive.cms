﻿using System;
using Newtonsoft.Json;

namespace Cms.Aggregates
{
    public class DateTimeRange
    {
        private readonly DateTime _start;
        private readonly DateTime _end;

        [JsonConstructor]
        public DateTimeRange(DateTime start, DateTime end)
        {
            _start = start;
            _end = end;

            if (_end < _start)
            {
                _start = end;
                _end = start;
            }
        }

        public DateTimeRange(DateTime start, TimeSpan duration)
        {
            _start = start;
            _end = start.Add(duration);
        }

        public DateTime Start
        {
            get { return _start; }
        }

        public DateTime End
        {
            get { return _end; }
        }

        [JsonIgnore]
        public TimeSpan Duration
        {
            get { return _end - Start; }
        }

        public bool Contains(DateTime dateTime)
        {
            return dateTime >= _start && dateTime <= _end;
        }

        public bool IsOverlapping(DateTimeRange other)
        {
            return other.Start < End && other.End > Start;
        }

        protected bool Equals(DateTimeRange other)
        {
            return _start.Equals(other._start) && _end.Equals(other._end);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }
            if (obj.GetType() != GetType()) { return false; }
            return Equals((DateTimeRange)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_start.GetHashCode() * 397) ^ _end.GetHashCode();
            }
        }
    }
}