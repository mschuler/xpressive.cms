begin transaction

alter table PageContent add [Type] nvarchar(64) not null default ''

insert into Version (Major, Minor, Revision) values (0, 0, 10);

commit transaction