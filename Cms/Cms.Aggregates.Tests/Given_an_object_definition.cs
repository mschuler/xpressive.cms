using System;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.Aggregates.Security;
using Moq;
using Xunit;

namespace Cms.Aggregates.Tests
{
    public class Given_an_object_definition
    {
        [Fact]
        public void When_shared_with_tenants_then_users_without_priviledge_for_all_tenants_dont_remove_these_shares()
        {
            var definitionRepositoryMock = new Mock<IDynamicObjectDefinitionRepository>();
            var userRepositoryMock = new Mock<IUserRepository>();
            var groupRepositoryMock = new Mock<IGroupRepository>();
            var user = new User {  Id = 1 };
            user.AddGroupId(2);
            user.AddGroupId(3);
            user.AddGroupId(4);
            userRepositoryMock.Setup(m => m.Get(1)).Returns(user);
            groupRepositoryMock.Setup(m => m.Get(1)).Returns(new Group { TenantId = 1 });
            groupRepositoryMock.Setup(m => m.Get(2)).Returns(new Group { TenantId = 2 });
            groupRepositoryMock.Setup(m => m.Get(3)).Returns(new Group { TenantId = 3 });
            groupRepositoryMock.Setup(m => m.Get(4)).Returns(new Group { TenantId = 4 });
            var definition = new DynamicObjectDefinition();
            definition.Share(new ulong[] { 1, 2, 4 });
            definitionRepositoryMock.Setup(m => m.Get(1)).Returns(definition);
            var definitionRepository = definitionRepositoryMock.Object;
            var userRepository = userRepositoryMock.Object;
            var groupRepository = groupRepositoryMock.Object;

            var handler = new DynamicObjectDefinitionEventHandler(definitionRepository, userRepository, groupRepository);
            var shareEvent = new DynamicObjectDefinitionShared
            {
                AggregateId = 1,
                Date = DateTime.UtcNow,
                UserId = 1,
                TenantIds = new ulong[] {2,3}
            };
            handler.When(shareEvent);

            Assert.Contains((ulong)1, definition.SharedForTenants);
            Assert.Contains((ulong)2, definition.SharedForTenants);
            Assert.Contains((ulong)3, definition.SharedForTenants);
            Assert.DoesNotContain((ulong)4, definition.SharedForTenants);
        }
    }
}
