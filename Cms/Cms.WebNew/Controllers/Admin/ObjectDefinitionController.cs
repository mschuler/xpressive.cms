using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Commands;
using Cms.Aggregates.DynamicObjects.Queries;
using Cms.Aggregates.Security;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class ObjectDefinitionController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _uniqueIdProvider;
        private readonly IDynamicObjectDefinitionRepository _repository;

        public ObjectDefinitionController(ICommandQueue commandQueue, IUserRepository userRepository, IUniqueIdProvider uniqueIdProvider, IQueryHandler queryHandler, IDynamicObjectDefinitionRepository repository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _uniqueIdProvider = uniqueIdProvider;
            _repository = repository;
        }

        [HttpGet, Route("api/objectdefinition/{tenantId:ulong}")]
        public IEnumerable<object> GetAll(ulong tenantId)
        {
            return ExecuteQuery(new GetAllObjectDefinitions { TenantId = tenantId });
        }

        [HttpGet, Route("api/objectdefinition/{tenantId:ulong}/shared")]
        public IEnumerable<object> GetShared(ulong tenantId)
        {
            return ExecuteQuery(new GetSharedDynamicObjectDefinitions { TenantId = tenantId });
        }

        [HttpGet, Route("api/objectdefinition/{tenantId:ulong}/{definitionId:ulong}/detail")]
        public object Get(ulong tenantId, ulong definitionId)
        {
            return ExecuteQuery(new GetObjectDefinition
            {
                Id = definitionId,
                TenantId = tenantId,
            }).SingleOrDefault();
        }

        [HttpPost, Route("api/objectdefinition/{tenantId:ulong}")]
        public object Create(ulong tenantId, [FromBody]NameDto dto)
        {
            var id = _uniqueIdProvider.GetId();
            SendCommand(new CreateDynamicObjectDefinition
            {
                AggregateId = id,
                TenantId = tenantId,
                Name = dto.Name,
            });

            return new { Id = id.ToJsonString(), Name = dto.Name };
        }

        [HttpDelete, Route("api/objectdefinition/{definitionId:ulong}")]
        public void Delete(ulong definitionId)
        {
            var tenantId = _repository.Get(definitionId).TenantId;

            SendCommand(new DeleteDynamicObjectDefinition
            {
                AggregateId = definitionId,
                TenantId = tenantId,
            });
        }

        [HttpPut, Route("api/objectdefinition/{definitionId:ulong}/name")]
        public void Rename(ulong definitionId, [FromBody] NameDto dto)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            var c = new RenameDynamicObjectDefinition
            {
                AggregateId = definitionId,
                TenantId = tenantId,
                Name = dto.Name,
            };

            SendCommand(c);
        }

        [HttpPut, Route("api/objectdefinition/{definitionId:ulong}/star")]
        public void Star(ulong definitionId)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            var c = new StarDynamicObjectDefinition
            {
                AggregateId = definitionId,
                TenantId = tenantId,
            };

            SendCommand(c);
        }

        [HttpPut, Route("api/objectdefinition/{definitionId:ulong}/unstar")]
        public void Unstar(ulong definitionId)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            var c = new UnstarDynamicObjectDefinition
            {
                AggregateId = definitionId,
                TenantId = tenantId,
            };

            SendCommand(c);
        }

        [HttpPut, Route("api/objectdefinition/{definitionId:ulong}/share")]
        public void Share(ulong definitionId, [FromBody] TenantListDto dto)
        {
            var tenantId = _repository.Get(definitionId).TenantId;

            SendCommand(new ShareDynamicObjectDefinition
            {
                AggregateId = definitionId,
                TenantId = tenantId,
                TenantIds = dto.TenantIds.ToArray()
            });
        }

        [HttpPut, Route("api/objectdefinition/{definitionId:ulong}/up")]
        public void MoveFieldUp(ulong definitionId, [FromBody]NameDto dto)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            SendCommand(new MoveDynamicObjectDefinitionFieldUp
            {
                AggregateId = definitionId,
                Name = dto.Name,
                TenantId = tenantId,
            });
        }

        [HttpPut, Route("api/objectdefinition/{definitionId:ulong}/down")]
        public void MoveFieldDown(ulong definitionId, [FromBody]NameDto dto)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            SendCommand(new MoveDynamicObjectDefinitionFieldDown
            {
                AggregateId = definitionId,
                Name = dto.Name,
                TenantId = tenantId,
            });
        }

        [HttpPut, Route("api/objectdefinition/{definitionId:ulong}/sortable")]
        public void MarkFieldSortable(ulong definitionId, [FromBody] NameDto dto)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            SendCommand(new MarkDynamicObjectDefinitionFieldSortable
            {
                AggregateId = definitionId,
                Name = dto.Name,
                TenantId = tenantId,
                IsSortable = true,
            });
        }

        [HttpPut, Route("api/objectdefinition/{definitionId:ulong}/notsortable")]
        public void MarkFieldNotSortable(ulong definitionId, [FromBody] NameDto dto)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            SendCommand(new MarkDynamicObjectDefinitionFieldSortable
            {
                AggregateId = definitionId,
                Name = dto.Name,
                TenantId = tenantId,
                IsSortable = false,
            });
        }

        [HttpPut, Route("api/objectdefinition/{definitionId:ulong}/add")]
        public void AddField(ulong definitionId, [FromBody]DynamicObjectDefinitionFieldDto field)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            SendCommand(new AddDynamicObjectDefinitionField
            {
                AggregateId = definitionId,
                Name = field.Name,
                ContentType = field.ContentType,
                ContentTypeOptions = field.ContentTypeOptions ?? string.Empty,
                TenantId = tenantId,
            });
        }

        [HttpPut, Route("api/objectdefinition/{definitionId:ulong}/remove")]
        public void RemoveField(ulong definitionId, [FromBody]NameDto dto)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            SendCommand(new RemoveDynamicObjectDefinitionField
            {
                AggregateId = definitionId,
                Name = dto.Name,
                TenantId = tenantId,
            });
        }

        [HttpPut, Route("api/objectdefinition/{definitionId:ulong}/field")]
        public void ChangeField(ulong definitionId, [FromBody] ChangeFieldDto dto)
        {
            var definition = _repository.Get(definitionId);
            var tenantId = definition.TenantId;

            var field = definition.Fields.Single(f => f.Name.Equals(dto.OldName, StringComparison.Ordinal));

            if (!string.Equals(dto.ContentTypeOptions, field.ContentTypeOptions, StringComparison.Ordinal))
            {
                SendCommand(new ChangeDynamicObjectDefinitionFieldContentTypeOptions
                {
                    AggregateId = definitionId,
                    TenantId = tenantId,
                    Name = dto.OldName,
                    ContentTypeOptions = dto.ContentTypeOptions,
                });
            }

            if (!string.Equals(dto.OldName, dto.NewName, StringComparison.Ordinal))
            {
                SendCommand(new RenameDynamicObjectDefinitionField
                {
                    AggregateId = definitionId,
                    OldName = dto.OldName,
                    NewName = dto.NewName,
                    TenantId = tenantId,
                });
            }
        }

        public class DynamicObjectDefinitionFieldDto
        {
            public string Name { get; set; }
            public string ContentType { get; set; }
            public string ContentTypeOptions { get; set; }
        }

        public class NameDto
        {
            public string Name { get; set; }
        }

        public class ChangeFieldDto
        {
            public string OldName { get; set; }
            public string NewName { get; set; }
            public string ContentTypeOptions { get; set; }
        }

        public class TenantListDto
        {
            public List<ulong> TenantIds { get; set; }
        }
    }
}