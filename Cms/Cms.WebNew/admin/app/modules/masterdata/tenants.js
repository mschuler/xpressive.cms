﻿(function() {
    angular.module("app").controller("TenantController", [
        "$rootScope", "$scope", "$http", "$log", "dialogService", "tenantService", "$translate", "toastService",
        function ($rootScope, $scope, $http, $log, dialogService, tenantService, $translate, toastService) {

        $scope.selectedTenant = [];

        function getScopeDataFromTenantService() {
            $scope.tenants = tenantService.getTenants();
            $scope.currentTenant = tenantService.getTenant();

            if ($scope.currentTenant) {
                $http.get("/api/permission/tenant/" + $scope.currentTenant.id, { cache: false }).success(function (permission) {
                    $scope.canEdit = permission.canEdit;
                    $scope.canAdmin = permission.canAdmin;
                });
            }
        };

        getScopeDataFromTenantService();

        $scope.setTenant = function (tenant) {
            $scope.currentTenant = tenant;
            tenantService.setTenant($scope.currentTenant);
        };

        $scope.selectTenant = function(tenant) {
            $scope.selectedTenant = tenant;
        }

        var createTenant = "";
        var name = "";
        var errorCreateTenant = "";
        var renameTenant = "";
        var errorRenameTenant = "";
        var deleteTenant = "";
        var confirmDeleteTenant = "";
        var errorDeleteTenant = "";

        $translate(["create_tenant", "name", "error_create_tenant", "rename_tenant", "error_rename_tenant", "delete_tenant", "confirm_delete_tenant", "error_delete_tenant"]).then(function(t) {
            createTenant = t.create_tenant;
            name = t.name;
            errorCreateTenant = t.error_create_tenant;
            renameTenant = t.rename_tenant;
            errorRenameTenant = t.error_rename_tenant;
            deleteTenant = t.delete_tenant;
            confirmDeleteTenant = t.confirm_delete_tenant;
            errorDeleteTenant = t.error_delete_tenant;
        });

        $scope.createTenant = function() {
            dialogService.singleInputDialog(createTenant, name, "", function (n) {
                var dto = {
                    name: n
                };
                $http.put("/api/tenant", dto).success(function(tenant) {
                    $scope.tenants.push(tenant);
                    tenantService.addTenant(tenant);
                }).error(function(error) {
                    toastService.error(errorCreateTenant, error);
                });
            });
        };

        $scope.renameTenant = function (tenant) {
            dialogService.singleInputDialog(renameTenant, name, tenant.name, function (newName) {
                var dto = {
                    name: newName
                };
                $http.post("/api/tenant/" + tenant.id, dto).success(function() {
                    tenant.name = newName;
                }).error(function(error) {
                    toastService.error(errorRenameTenant, error);
                });
            });
        };

        $scope.deleteTenant = function(tenant) {
            dialogService.confirmDialog(deleteTenant, String.format(confirmDeleteTenant, tenant.name), function() {
                $http.delete("/api/tenant/" + tenant.id).success(function() {
                    $scope.tenants = _.reject($scope.tenants, function(t) { return t.id === tenant.id; });
                }).error(function(error) {
                    toastService.error(errorDeleteTenant, error);
                });
            });
        };

        $rootScope.$on('tenantChanged', getScopeDataFromTenantService);
        $rootScope.$on('tenantsChanged', getScopeDataFromTenantService);
        }]);
}());