using System;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.PageTemplates.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.PageTemplates
{
    internal class PageTemplateTemplateChangedEventTranslator : IEventTranslator
    {
        private readonly IPageTemplateRepository _repository;

        public PageTemplateTemplateChangedEventTranslator(IPageTemplateRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageTemplateTemplateChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seitenvorlage ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageTemplateTemplateChanged)@event;
            var template = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Die Seitenvorlage <strong>{0}</strong> wurde ge�ndert.", template.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "pagetemplate";
        }
    }
}