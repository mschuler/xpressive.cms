using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.DynamicObjects
{
    public class DynamicObject : AggregateBase
    {
        private readonly object _fieldsLock = new object();
        private readonly List<DynamicObjectField> _fields;
        private readonly object _sharedForTenantsLock = new object();
        private readonly List<ulong> _sharedForTenants;

        public DynamicObject()
        {
            _fields = new List<DynamicObjectField>();
            _sharedForTenants = new List<ulong>();
        }

        public ulong TenantId { get; set; }
        public ulong DefinitionId { get; set; }
        public int PublicId { get; set; }
        public DateTime CreationDate { get; set; }

        public string Title
        {
            get
            {
                lock (_fieldsLock)
                {
                    return _fields[0].Value;
                }
            }
        }

        public IEnumerable<DynamicObjectField> Fields
        {
            get
            {
                lock (_fieldsLock)
                {
                    return _fields.ToList();
                }
            }
        }

        public IEnumerable<ulong> SharedForTenants
        {
            get
            {
                lock (_sharedForTenantsLock)
                {
                    return _sharedForTenants.ToList();
                }
            }
        }

        public void Add(DynamicObjectField field)
        {
            lock (_fieldsLock)
            {
                _fields.Add(field);
            }
        }

        public void Remove(DynamicObjectField field)
        {
            lock (_fieldsLock)
            {
                _fields.Remove(field);
            }
        }

        public void Share(IEnumerable<ulong> tenantIds)
        {
            lock (_sharedForTenantsLock)
            {
                _sharedForTenants.Clear();
                _sharedForTenants.AddRange(tenantIds);
            }
        }
    }
}