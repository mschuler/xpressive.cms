﻿(function() {
    angular.module("app").controller("GroupsController", ["$rootScope", "$scope", "$http", "$location", "tenantService", "dialogService", "toastService", "$translate", function ($rootScope, $scope, $http, $location, tenantService, dialogService, toastService, $translate) {
        var createGroup = "";
        var name = "";
        var errorCreateGroup = "";
        var renameGroup = "";
        var errorRenameGroup = "";
        var deleteGroup = "";
        var confirmDeleteGroup = "";
        var errorDeleteGroup = "";

        $translate(["create_group", "name", "error_create_group", "rename_group", "error_rename_group", "delete_group", "confirm_delete_group", "error_delete_group"]).then(function(t) {
            createGroup = t.create_group;
            name = t.name;
            errorCreateGroup = t.error_create_group;
            renameGroup = t.rename_group;
            errorRenameGroup = t.error_rename_group;
            deleteGroup = t.delete_group;
            confirmDeleteGroup = t.confirm_delete_group;
            errorDeleteGroup = t.error_delete_group;
        });

        $http.get("/api/permission/group/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.create = function() {
            dialogService.singleInputDialog(createGroup, name, "", function (n) {
                var dto = {
                    name: n
                };
                $http.post("/api/groups/" + tenantService.getTenant().id, dto).success(function(group) {
                    $scope.groups.push(group);
                }).error(function (e) {
                    toastService.error(errorCreateGroup, e);
                });
            });
        };

        $scope.select = function(group) {
            $scope.selectedGroup = group;
        };

        $scope.rename = function(group) {
            dialogService.singleInputDialog(renameGroup, name, group.name, function (n) {
                var dto = {
                    name: n
                };
                $http.post("/api/group/" + group.id + "/name", dto).success(function() {
                    group.name = n;
                }).error(function(e) {
                    toastService.error(errorRenameGroup, e);
                });
            });
        };

        $scope.delete = function(group) {
            dialogService.confirmDialog(deleteGroup, String.format(confirmDeleteGroup, group.name), function() {
                $http.delete("/api/group/" + group.id).success(function() {
                    $scope.groups = _.reject($scope.groups, function(g) { return g.id === group.id; });
                }).error(function(e) {
                    toastService.error(errorDeleteGroup, e);
                });
            });
        };

        $scope.editMembers = function(group) {
            $location.path("/user/group/" + group.id + "/members");
        };

        $scope.editPermissions = function(group) {
            $location.path("/user/group/" + group.id + "/permissions");
        };

        function loadGroups() {
            $http.get("/api/groups/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
                $scope.groups = data;
            });
        };

        $rootScope.$on("tenantChanged", loadGroups);

        $rootScope.$on('logout', function () {
            $scope.groups = [];
        });

        loadGroups();
    }]);

    angular.module("app").controller("MemberController", ["$rootScope", "$scope", "$routeParams", "$http", "$log", "toastService", "dialogService", "$translate", function ($rootScope, $scope, $routeParams, $http, $log, toastService, dialogService, $translate) {
        var groupId = $routeParams.groupId;
        var errorAddMember = "";
        var errorRemoveMember = "";

        $scope.selectedUser = {};

        $translate(["error_add_member", "error_remove_member"]).then(function (t) {
            errorAddMember = t.error_add_member;
            errorRemoveMember = t.error_remove_member;
        });

        function loadMembers() {
            $http.get("/api/members/" + groupId, { cache: false }).success(function(data) {
                $scope.members = data;
            });
        };

        $scope.select = function(user) {
            $scope.selectedUser = user;
        };

        $scope.add = function() {
            dialogService.selectUserDialog(function(user) {
                $http.post("/api/members/" + groupId + "/add/" + user.id).success(function () {
                    $scope.members.push(user);
                }).error(function (e) {
                    toastService.error(errorAddMember, e);
                });
            });
        };

        $scope.remove = function(user) {
            return $http.post("/api/members/" + groupId + "/remove/" + user.id).success(function() {
                $scope.members = _.reject($scope.members, function(m) { return m.id === user.id; });
            }).error(function(e) {
                toastService.error(errorRemoveMember, e);
            });
        };

        $rootScope.$on('logout', function () {
            $scope.members = [];
        });

        loadMembers();
    }]);

    angular.module("app").controller("PermissionController", ["$rootScope", "$scope", "$routeParams", "$http", "$log", "toastService", "$translate", function ($rootScope, $scope, $routeParams, $http, $log, toastService, $translate) {
        var groupId = $routeParams.groupId;

        $scope.p0 = 0;
        $scope.p1 = 1;
        $scope.p2 = 2;
        $scope.p3 = 3;

        function loadPermissions() {
            $http.get("/api/permissions/" + groupId, { cache: false }).success(function(data) {
                $scope.permissions = data;

                for (var i = 0; i < data.length; i++) {
                    $scope.$watch("permissions[" + i + "]", function (newValue, oldValue) {
                        if (!newValue || !oldValue || newValue.permission === oldValue.permission) {
                            return;
                        }

                        $log.debug(angular.toJson(newValue));

                        $http.post("/api/permissions/" + groupId, newValue).error(function (e) {
                            newValue.permission = oldValue.permission;

                            $translate("error_save_permission").then(function(t) {
                                toastService.error(t, e);
                            });
                        });
                    }, true);
                }
            });
        };

        $rootScope.$on('logout', function () {
            $scope.permissions = [];
        });

        loadPermissions();
    }]);
}());