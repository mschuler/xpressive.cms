﻿using System;
using Cms.Aggregates.SystemMessages.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.SystemMessages
{
    internal class SystemMessageCreatedEventTranslator : IEventTranslator
    {
        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(SystemMessageCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Systemnachricht erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (SystemMessageCreated)@event;
            return string.Format("Die Systemnachricht <strong>{0}</strong> wurde erstellt.", e.Title);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}