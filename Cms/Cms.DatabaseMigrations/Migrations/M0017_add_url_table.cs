namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0017_add_url_table : DatabaseSqlMigration
    {
        public M0017_add_url_table() : base(17) { }

        protected override string GetSql()
        {
            return @"
create table Url (
    LongUrl nvarchar(2100) not null,
    ShortUrl nvarchar(255) not null
)
create index ix_url on Url (LongUrl)
";
        }
    }
}