﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("objectdefinition", PermissionLevel.CanEverything)]
    public class DeleteDynamicObjectDefinition : CommandBase { }
}