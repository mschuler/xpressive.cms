
begin transaction

create table MediaCenter (
	Id int not null identity(1, 1),
	Guid uniqueidentifier not null,
	[Name] nvarchar(256) not null,

	constraint PK_MediaCenter primary key (Id)
)

alter table MediaContent add MediaCenterId int not null;
alter table MediaContent add constraint FK_MediaContent_MediaCenter foreign key (MediaCenterId) references MediaCenter (Id);

alter table MediaCategory add MediaCenterId int not null;
alter table MediaCategory add constraint FK_MediaCategory_MediaCenter foreign key (MediaCenterId) references MediaCenter (Id);

insert into Version (Major, Minor, Revision) values (0, 0, 20);

commit transaction
