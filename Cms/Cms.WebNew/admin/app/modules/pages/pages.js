﻿(function() {
    angular.module('app').controller('PageController', ["$rootScope", "$scope", "$log", "$http", "$modal", "$location", "toastService", "tenantService", "dialogService", "$translate", function ($rootScope, $scope, $log, $http, $modal, $location, toastService, tenantService, dialogService, $translate) {
        $scope.selectedPage = {};

        var errorMovePage = "";
        var changePageLink = "";
        var errorChangePageLink = "";
        var createPageLink = "";
        var errorCreatePageLink = "";
        var createPage = "";
        var tName = "";
        var noPageTemplate = "";
        var noPageTemplateDescription = "";
        var deletePage = "";
        var confirmDeletePage = "";
        var errorDeletePage = "";
        var errorChangePageTemplate = "";
        var errorPinPage = "";
        var errorUnpinPage = "";
        var errorEnablePage = "";
        var errorDisablePage = "";
        var errorMarkAsDefaultPage = "";

        $translate(["error_move_page", "change_page_link", "error_change_page_link", "create_page_link", "error_create_page_link", "create_page", "name", "no_page_template", "no_page_template_description", "delete_page", "confirm_delete_page", "error_delete_page", "error_change_page_template", "error_pin_page", "error_unpin_page", "error_enable_page", "error_disable_page", "error_mark_as_default_page"]).then(function (t) {
            errorMovePage = t.error_move_page;
            changePageLink = t.change_page_link;
            errorChangePageLink = t.error_change_page_link;
            createPageLink = t.create_page_link;
            errorCreatePageLink = t.error_create_page_link;
            createPage = t.create_page;
            tName = t.name;
            noPageTemplate = t.no_page_template;
            noPageTemplateDescription = t.no_page_template_description;
            deletePage = t.delete_page;
            confirmDeletePage = t.confirm_delete_page;
            errorDeletePage = t.error_delete_page;
            errorChangePageTemplate = t.error_change_page_template;
            errorPinPage = t.error_pin_page;
            errorUnpinPage = t.error_unpin_page;
            errorEnablePage = t.error_enable_page;
            errorDisablePage = t.error_disable_page;
            errorMarkAsDefaultPage = t.error_mark_as_default_page;
        });

        $http.get("/api/permission/page/" + tenantService.getTenant().id, { cache: false }).success(function(permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.treeOptions = {
            dropped: function (event) {
                var thisNode = event.source.nodeScope.node;
                var thisNr = thisNode.id;
                var oldParent = event.source.nodesScope && event.source.nodesScope.node ? event.source.nodesScope.node.id : null;
                var newParent = event.dest.nodesScope && event.dest.nodesScope.node ? event.dest.nodesScope.node.id : null;
                var oldData = {};
                oldData[thisNr] = { parentId: thisNode.parentId, sortOrder: thisNode.sortOrder };

                if ((oldParent === newParent || (!oldParent && !newParent))) {
                    newParent = thisNode.parentId;
                } else {
                    newParent = newParent || 0;
                }

                var website = _.find($scope.websites, function (w) { return w.id === thisNode.websiteId; });
                var siblings = _.filter(website.allPages, function (p) { return p.parentId === newParent; });
                siblings = _.sortBy(siblings, "name");
                siblings = _.sortBy(siblings, "sortOrder");
                siblings = _.reject(siblings, function (p) { return p.id === thisNr; });
                siblings.splice(event.dest.index, 0, thisNode);
                thisNode.parentId = newParent;

                var i = 0;
                _.each(siblings, function (p) {
                    if (p.id != thisNr) {
                        oldData[p.id] = { parentId: p.parentId, sortOrder: p.sortOrder };
                    }

                    p.sortOrder = i;
                    i++;
                });

                resetChildren(website);

                var url = String.format("/api/page/{0}/move/{1}/{2}", thisNr, newParent, event.dest.index);
                $http.post(url).success(function () {
                }).error(function (e) {
                    toastService.error(errorMovePage, e);

                    _.each(oldData, function(v, k) {
                        var s = _.find(siblings, function (p) { return p.id === k; });
                        if (s) {
                            s.sortOrder = v.sortOrder;
                            s.parentId = v.parentId;
                        }
                    });

                    resetChildren(website);
                });

                return true;
            }
        };

        function resetChildren(website) {
            website.rootPages = getPagesByParentId(website, "0");

            _.each(website.pages, function (p) {
                p.children = getPagesByParentId(website, p.id);
            });
        };

        function getPagesByParentId(website, parentId) {
            var children = _.filter(website.pages, function (p) { return p.parentId == parentId; });
            children = _.sortBy(children, "name");
            children = _.sortBy(children, "sortOrder");
            return children;
        }

        $scope.searchPages = function (website) {
            var searchText = website.searchText.toUpperCase();

            _.each(website.pages, function(p) { p.isCollapsed = true; });

            var pages = _.filter(website.pages, function(p) {
                 return pageLabelComparer(p, searchText);
            });

            _.each(pages, function (p) { expandParent(website, p); });
        };

        function expandParent(website, page) {
            var parent = _.find(website.pages, function (p) { return p.id === page.parentId; });
            if (parent) {
                parent.isCollapsed = false;
                expandParent(website, parent);
            }
        }

        function pageLabelComparer(page, searchText) {
            return page.fullname.toUpperCase().indexOf(searchText) >= 0;
        }

        function openLinkDialog(subject, name, page, url, onSave) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/content/pageLinkDialog.min.html",
                controller: "PageLinkDialogController",
                resolve: {
                    page: function () { return page; },
                    subject: function () { return subject; },
                    name: function () { return name; },
                    url: function() { return url; },
                }
            });

            modalInstance.result.then(onSave);
        };

        $scope.editLink = function(website, node) {
            openLinkDialog(changePageLink, node.node.name, node.node.link, node.node.link.url, function (result) {
                var name = result.name;
                var url = result.url;
                var pageId = 0;

                if (result.page) {
                    pageId = result.page.id;
                }

                var dto = {
                    pageId: pageId,
                    name: name,
                    url: url,
                };

                $http.put("/api/page/" + node.node.id + "/link", dto).success(function (data) {
                    _.each(website.allPages, function(p) {
                        if (p.id === pageId) {
                            p.name = name;
                            p.link = data.link;
                        }
                    });
                }).error(function (error) {
                    toastService.error(errorChangePageLink, error);
                });
            });
        };

        $scope.createPageLink = function(website) {
            var parent = null;
            var parentId = 0;
            if ($scope.selectedPage && $scope.selectedPage.id) {
                parent = $scope.selectedPage;
                parentId = $scope.selectedPage.id;
            }

            openLinkDialog(createPageLink, "", null, "", function (result) {
                var name = result.name;
                var url = result.url;
                var pageId = 0;

                if (result.page) {
                    pageId = result.page.id;
                }

                var dto = {
                    name: name,
                    pageId: pageId,
                    parentPageId: parentId,
                    url: url,
                };
                $http.put("/api/pages/" + website.id + "/createlink", dto).success(function (data) {
                    data.children = [];
                    website.allPages.push(data);
                    website.pages.push(data);
                    website.isOpen = true;

                    if (parent) {
                        parent.isCollapsed = false;
                    }

                    resetChildren(website);
                    $scope.selectedPage = data;
                }).error(function (error) {
                    toastService.error(errorCreatePageLink, error);
                });
            });
        };

        $scope.createPage = function (website) {
            var parent = null;
            var parentId = 0;
            if ($scope.selectedPage && $scope.selectedPage.id) {
                parent = $scope.selectedPage;
                parentId = $scope.selectedPage.id;
            }

            dialogService.singleInputDialog(createPage, tName, "", function (name) {
                var url = "/api/pages/" + website.id + "/create";
                var dto = {
                    parentPageId : parentId,
                    name: name
                };
                $http.put(url, dto).success(function(data) {
                    data.nr = "n/a";
                    data.children = [];
                    website.allPages.push(data);
                    website.pages.push(data);
                    website.isOpen = true;

                    if (parent) {
                        parent.isCollapsed = false;
                    }

                    resetChildren(website);
                    $scope.selectedPage = data;
                });
            });
        };

        $scope.selectPage = function(node) {
            $scope.selectedPage = node.node;
        };

        $scope.edit = function (node) {
            if (node.node && node.node.templateId && node.node.templateId != "0") {
                $location.path("/content/page/" + node.node.id);
            } else {
                dialogService.messageDialog(noPageTemplate, noPageTemplateDescription);
            }
        };

        $scope.delete = function (node) {
            var website = findWebsite(node.node.websiteId);
            dialogService.confirmDialog(deletePage, String.format(confirmDeletePage, node.node.fullname), function () {
                $http.delete("/api/page/" + node.node.id).success(function () {
                    website.pages = _.reject(website.pages, function(p) { return p.id === node.node.id; });
                    website.allPages = _.reject(website.allPages, function (p) { return p.id === node.node.id; });
                    resetChildren(website);
                }).error(function(error) {
                    toastService.error(errorDeletePage, error);
                });
            });
        };

        $scope.changeLayout = function (node) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/content/editPageLayoutTemplate.min.html",
                controller: "PageEditLayoutController",
                resolve: {
                    page: function () {
                        return node.node;
                    }
                }
            });

            modalInstance.result.then(function (selectedTemplate) {
                if (selectedTemplate && selectedTemplate.id) {
                    $http.put("/api/page/" + node.node.id + "/template/" + selectedTemplate.id).success(function () {
                        node.node.templateId = selectedTemplate.id;
                    }).error(function (error) {
                        toastService.error(errorChangePageTemplate, error);
                    });
                }
            });
        };

        function findWebsite(websiteId) {
            var websites = $scope.websites.filter(function (item) {
                return item.id === websiteId;
            });
            return websites[0];
        };

        // TODO: bei enable/disable etc. auch bei pinned seiten anwenden!
        function findPages(pageId) {
            var pages = [];
            angular.forEach($scope.websites, function(website) {
                angular.forEach(website.allPages, function(page) {
                    if (page.id === pageId) {
                        pages.push(page);
                    }
                });
            });
            return pages;
        };

        $scope.unpin = function (node) {
            return $http.put("/api/page/" + node.node.id + "/unstar").success(function () {
                var website = findWebsite(node.node.websiteId);

                if (!website) {
                    return;
                }

                website.starred = website.starred.filter(function (item) {
                    return item.id !== node.node.id;
                });
            }).error(function(error) {
                toastService.error(errorUnpinPage, error);
            });
        };

        $scope.pin = function (node) {
            return $http.put("/api/page/" + node.node.id + "/star").success(function () {
                var website = findWebsite(node.node.websiteId);

                if (!website) {
                    return;
                }

                var alreadyStarred = false;
                angular.forEach(website.starred, function (value, key) {
                    if (value.id === node.node.id) {
                        alreadyStarred = true;
                    }
                });

                if (alreadyStarred) {
                    return;
                }

                var copy = angular.copy(node.node);
                copy.children = [];
                website.allPages.push(copy);
                website.starred.push(copy);
            }).error(function(error) {
                toastService.error(errorPinPage, error);
            });
        };

        $scope.setDefault = function (page) {
            var websiteId = page.websiteId;
            var pageId = page.id;
            return $http.post("/api/website/" + websiteId + "/defaultpage/" + pageId).success(function () {
                var website = findWebsite(websiteId);
                if (!website) {
                    return;
                }
                _.each(website.allPages, function(p) {
                    p.isDefault = p.id === page.id;
                });
            }).error(function(error) {
                toastService.error(errorMarkAsDefaultPage, error);
            });
        };

        $scope.enable = function (node) {
            return $http.put("/api/page/" + node.node.id + "/enable").success(function () {
                var pages = findPages(node.node.id);
                angular.forEach(pages, function (page) { page.disabled = false; });
            }).error(function(error) {
                toastService.error(errorEnablePage, error);
            });
        };

        $scope.disable = function (node) {
            return $http.put("/api/page/" + node.node.id + "/disable").success(function () {
                var pages = findPages(node.node.id);
                angular.forEach(pages, function (page) { page.disabled = true; });
            }).error(function (error) {
                toastService.error(errorDisablePage, error);
            });
        };

        $scope.toggle = function (node) {
            node.toggle();
        };

        $scope.collapseAll = function () {
            for (var i = 0; i < $scope.websites.length; i++) {
                var website = $scope.websites[i];
                if (website.isOpen) {
                    collapse(website);
                }
            }
        };

        $scope.expandAll = function () {
            for (var i = 0; i < $scope.websites.length; i++) {
                var website = $scope.websites[i];
                if (website.isOpen) {
                    _.each(website.pages, function (p) { p.isCollapsed = false; });
                }
            }
        };

        var collapse = function (website) {
            _.each(website.pages, function (p) { p.isCollapsed = true; });
        };

        var addPagesToCollection = function (website, pages) {
            angular.forEach(pages, function (value, key) {
                website.allPages.push(value);
            });
        };

        var reloadWebsites = function () {
            $http.get("/api/websites/" + tenantService.getTenant().id, { cache: false })
                .success(function (data) {
                    $scope.websites = data;

                    angular.forEach($scope.websites, function (value, key) {
                        angular.extend(value, {
                            isOpen: false,
                            isLoaded: false,
                            pages: [],
                            starred: [],
                            allPages: [],
                            searchText: '',
                        });

                        $scope.$watch("websites[" + key + "].isOpen", function () {
                            if (value.isOpen && !value.isLoaded) {
                                openWebsite(value);
                            }
                        });

                        $scope.$watch("websites[" + key + "].searchText", function() {
                            $scope.searchPages(value);
                            if (!value.searchText) {
                                collapse(value);
                            }
                        });
                    });

                }).error(function (err) {
                    $scope.websites = {};
                });
        };

        var openWebsite = function (website) {
            if (!website.isLoaded) {
                website.isLoaded = true;
                $http.get('/api/pages/' + website.id, { cache: false }).success(function (pageData) {
                    website.pages = pageData;
                    addPagesToCollection(website, website.pages);
                    resetChildren(website);
                });
                $http.get('/api/pages/' + website.id + '/starred', { cache: false }).success(function (starredData) {
                    website.starred = starredData;
                    addPagesToCollection(website, website.starred);
                });
            }
        };

        $rootScope.$on("tenantChanged", reloadWebsites);

        $rootScope.$on('logout', function () {
            $scope.websites = {};
        });

        reloadWebsites();
    }]);

    angular.module('app').controller("PageEditLayoutController", ["$scope", "$modalInstance", "$http", "tenantService", "page", function ($scope, $modalInstance, $http, tenantService, page) {
        $scope.scope = { selectedTemplate: {} };
        $scope.templates = [];

        $scope.ok = function () {
            $modalInstance.close($scope.scope.selectedTemplate);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.selectTemplate = function (e) {
            $scope.selectedTemplate = $(e.target).data('id');
        };

        var tenantId = tenantService.getTenant().id;

        $http.get('/api/pages/templates/' + tenantId, { cache: false }).success(function (data) {
            _.each(data, function (d) {
                d.isShared = false;
                $scope.templates.push(d);

                if (d.id === page.templateId) {
                    $scope.scope.selectedTemplate = d;
                }
            });
        });

        $http.get("/api/pagetemplates/" + tenantId + "/shared", { cache: false }).success(function (data) {
            _.each(data, function (d) {
                d.isShared = true;
                $scope.templates.push(d);

                if (d.id === page.templateId) {
                    $scope.scope.selectedTemplate = d;
                }
            });
        });
    }]);

    angular.module("app").controller("PageLinkDialogController", ["$scope", "$modalInstance", "$http", "tenantService", "dialogService", "page", "subject", "name", "url", function ($scope, $modalInstance, $http, tenantService, dialogService, page, subject, name, url) {
        $scope.page = page;
        $scope.name = name;
        $scope.subject = subject;
        $scope.url = url;

        $scope.getPages = function (value) {
            var q = String.format("/api/page/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(q, { cache: false }).then(function (response) {
                return response.data;
            });
        };

        $scope.browsePage = function () {
            var p = null;
            if ($scope.page) {
                p = $scope.page;
            }
            dialogService.selectPageDialog(p, function (r) {
                $scope.page = {};
                if (!r) {
                    return;
                }
                r.children = [];
                $scope.page = r;
            });
        };

        $scope.ok = function () {
            $modalInstance.close({
                name: $scope.name,
                page: $scope.page,
                url: $scope.url,
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());