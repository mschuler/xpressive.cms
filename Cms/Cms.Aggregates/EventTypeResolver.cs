﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.EventSourcing.Contracts.Events;
using Cms.SharedKernel;

namespace Cms.Aggregates
{
    internal class EventTypeResolver : IEventTypeResolver
    {
        private static readonly Lazy<IDictionary<string, Type>> _types = new Lazy<IDictionary<string, Type>>(() =>
        {
            var types = typeof(EventTypeResolver).Assembly
                .GetTypes()
                .Where(t => !t.IsAbstract && t.IsClass && t.GetInterfaces().Contains(typeof(IEvent)))
                .ToList();
            return types.ToDictionary(t => t.Name, StringComparer.OrdinalIgnoreCase);
        });

        public Type GetEventType(string typeName)
        {
            Type type;
            if (_types.Value.TryGetValue(typeName, out type))
            {
                return type;
            }
            throw new NotSupportedException("EventTypeResolver cannot resolve type " + typeName).WithGuid();
        }
    }
}