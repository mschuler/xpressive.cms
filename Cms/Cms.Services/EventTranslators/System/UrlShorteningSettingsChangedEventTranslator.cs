﻿using System;
using Cms.Aggregates.Settings.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.System
{
    internal class UrlShorteningSettingsChangedEventTranslator : IEventTranslator
    {
        public bool IsResponsibleFor(Type eventType)
        {
            return typeof(UrlShorteningSettingsChanged) == eventType;
        }

        public string GetHeader(IEvent @event)
        {
            return "URL Shortening Einstellungen geändert";
        }

        public string GetContent(IEvent @event)
        {
            return "Die URL Shortening Einstellungen wurden geändert.";
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return @event.AggregateId;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}