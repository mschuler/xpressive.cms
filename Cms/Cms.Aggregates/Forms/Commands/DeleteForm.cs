﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Commands
{
    [RequiredPermission("form", PermissionLevel.CanEverything)]
    public class DeleteForm : CommandBase { }
}