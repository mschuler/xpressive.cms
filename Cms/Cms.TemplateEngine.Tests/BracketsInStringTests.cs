﻿using Cms.TemplateEngine.Enums;
using Xunit;

namespace Cms.TemplateEngine.Tests
{
    public class BracketsInStringTests
    {
        [Fact]
        public void Given_brackets_in_string()
        {
            var engine = new Engine();
            var result = engine.ParseTreeItem("objects(1).where('field(\\\"page\\\").title', '=', 'asdf')");

            Assert.Equal(TreeItemType.Expression, result.ItemType);
            Assert.Equal(2, result.Children.Count);

            Assert.Equal(TreeItemType.Function, result.Children[0].ItemType);
            Assert.Equal("objects", result.Children[0].Value);

            Assert.Equal(TreeItemType.Function, result.Children[1].ItemType);
            Assert.Equal("where", result.Children[1].Value);
            Assert.Equal(3, result.Children[1].Children.Count);
            Assert.Equal(TreeItemType.Parameter, result.Children[1].Children[0].ItemType);
            Assert.Equal(TreeItemType.Parameter, result.Children[1].Children[1].ItemType);
            Assert.Equal(TreeItemType.Parameter, result.Children[1].Children[2].ItemType);
            Assert.Equal(ParameterType.String, result.Children[1].Children[0].ParameterType);
            Assert.Equal(ParameterType.String, result.Children[1].Children[1].ParameterType);
            Assert.Equal(ParameterType.String, result.Children[1].Children[2].ParameterType);
        }

        [Fact]
        public void Given_single_quotes_and_double_quotes()
        {
            var engine = new Engine();
            var result = engine.ParseTreeItem("objects(1).where('field(\"page\").title', '=', 'asdf')");

            Assert.Equal(TreeItemType.Expression, result.ItemType);
            Assert.Equal(2, result.Children.Count);

            Assert.Equal(TreeItemType.Function, result.Children[0].ItemType);
            Assert.Equal("objects", result.Children[0].Value);

            Assert.Equal(TreeItemType.Function, result.Children[1].ItemType);
            Assert.Equal("where", result.Children[1].Value);
            Assert.Equal(3, result.Children[1].Children.Count);
            Assert.Equal(TreeItemType.Parameter, result.Children[1].Children[0].ItemType);
            Assert.Equal(TreeItemType.Parameter, result.Children[1].Children[1].ItemType);
            Assert.Equal(TreeItemType.Parameter, result.Children[1].Children[2].ItemType);
            Assert.Equal(ParameterType.String, result.Children[1].Children[0].ParameterType);
            Assert.Equal(ParameterType.String, result.Children[1].Children[1].ParameterType);
            Assert.Equal(ParameterType.String, result.Children[1].Children[2].ParameterType);
        }
    }
}