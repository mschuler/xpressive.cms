﻿using System;
using System.Collections.Concurrent;
using System.Net;
using System.Threading.Tasks;
using Cms.Services.Contracts;
using RestSharp;

namespace Cms.Services
{
    internal class StopForumSpamService : ISpamService
    {
        private static readonly ConcurrentDictionary<string, Task<bool>> _cache = new ConcurrentDictionary<string, Task<bool>>(StringComparer.Ordinal);

        public async Task<bool> IsSpam(string ipAddress, string userAgent)
        {
            return await _cache.GetOrAdd(ipAddress, async _ => await IsSpam(ipAddress));
        }

        private static async Task<bool> IsSpam(string ipAddress)
        {
            var client = new RestClient("http://api.stopforumspam.org");
            var request = new RestRequest("api");
            request.AddQueryParameter("ip", ipAddress);
            request.AddQueryParameter("f", "json");

            var response = await client.ExecuteGetTaskAsync<StopForumSpamDto>(request);

            if (response.StatusCode == HttpStatusCode.OK && response.Data?.Ip != null)
            {
                return response.Data.Ip.Confidence > 80d;
            }

            return false;
        }
    }

    public class StopForumSpamDto
    {
        public int Success { get; set; }

        public StopForumSpamIpDto Ip { get; set; }
    }

    public class StopForumSpamIpDto
    {
        public int Frequency { get; set; }
        public int Appears { get; set; }
        public double Confidence { get; set; }
    }
}
