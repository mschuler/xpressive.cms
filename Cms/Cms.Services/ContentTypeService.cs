﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Cms.SharedKernel;
using log4net;

namespace Cms.Services
{
    internal class ContentTypeService : IContentTypeService
    {
        private const string DefaultContentType = "unknown/unknown";

        private static readonly ILog _log = LogManager.GetLogger(typeof(ContentTypeService));
        private static readonly Dictionary<string, ContentType> _cache = new Dictionary<string, ContentType>(StringComparer.OrdinalIgnoreCase);

        static ContentTypeService()
        {
            AddCache("master", DefaultContentType);

            // text
            AddCache("txt", "text/plain", true);
            AddCache("text", "text/plain", true);
            AddCache("conf", "text/plain", true);
            AddCache("log", "text/plain", true);
            AddCache("csv", "text/csv", true);
            AddCache("tsv", "text/tab-separated-values", true);
            AddCache("css", "text/css", true);
            AddCache("less", "text/css", true);
            AddCache("sass", "text/css", true);
            AddCache("scss", "text/css", true);
            AddCache("sql", "application/x-sql", true);
            AddCache("html", "text/html", true);
            AddCache("htm", "text/html", true);
            AddCache("xml", "application/xml", true);
            AddCache("xhtml", "application/xhtml+xml", true);
            AddCache("xsl", "application/xml", true);
            AddCache("dtd", "application/xml-dtd", true);
            AddCache("xslt", "application/xslt+xml", true);
            AddCache("js", "application/javascript", true);
            AddCache("md", "text/x-markdown", true);
            AddCache("markdown", "text/x-markdown", true);
            AddCache("json", "application/json", true);
            AddCache("atom", "application/atom+xml", true);
            AddCache("rss", "application/rss+xml", true);
            AddCache("ecma", "application/ecmascript", true);
            AddCache("ics", "text/calendar", true);
            AddCache("ifb", "text/calendar", true);
            AddCache("vcs", "text/x-vcalendar", true);
            AddCache("vcf", "text/x-vcard", true);
            AddCache("vcard", "text/vcard", true);
            AddCache("opml", "text/x-opml", true);
            AddCache("tex", "application/x-tex", true);
            AddCache("latex", "application/x-latex", true);

            // picture
            AddCache("png", "image/png");
            AddCache("jpg", "image/jpeg");
            AddCache("jpeg", "image/jpeg");
            AddCache("bmp", "image/bmp");
            AddCache("gif", "image/gif");
            AddCache("ico", "image/x-icon");
            AddCache("svg", "image/svg+xml");
            AddCache("svgz", "image/svg+xml");
            AddCache("tiff", "image/tiff");
            AddCache("tif", "image/tiff");
            AddCache("psd", "image/vnd.adobe.photoshop");
            AddCache("dwg", "image/vnd.dwg");
            AddCache("dxf", "image/vnd.dxf");

            // music
            AddCache("mp2", "audio/mpeg");
            AddCache("mp3", "audio/mpeg");
            AddCache("wma", "audio/x-ms-wma");
            AddCache("aif", "audio/x-aiff");
            AddCache("aiff", "audio/x-aiff");
            AddCache("aifc", "audio/x-aiff");
            AddCache("au", "audio/basic");
            AddCache("wav", "audio/x-wav");
            AddCache("fla", "audio/x-flac");
            AddCache("flac", "audio/x-flac");
            AddCache("ogg", "audio/ogg");
            AddCache("ra", "audio/x-pn-realaudio");
            AddCache("aac", "audio/x-aac");
            AddCache("m2a", "audio/mpeg");
            AddCache("m3a", "audio/mpeg");
            AddCache("oga", "audio/ogg");
            AddCache("m3u", "audio/x-mpegurl");
            AddCache("mp4a", "audio/mp4");

            // video
            AddCache("mp4", "video/mp4");
            AddCache("mp4v", "video/mp4");
            AddCache("mpg4", "video/mp4");
            AddCache("wmv", "video/x-ms-wmv");
            AddCache("mpg", "video/mpeg");
            AddCache("mpeg", "video/mpeg");
            AddCache("qt", "video/quicktime");
            AddCache("mov", "video/quicktime");
            AddCache("m4u", "video/vnd.mpegurl");
            AddCache("avi", "video/x-msvideo");
            AddCache("f4v", "video/x-f4v");
            AddCache("fli", "video/x-fli");
            AddCache("flv", "video/x-flv");
            AddCache("m4v", "video/x-m4v");
            AddCache("asf", "video/x-ms-asf");
            AddCache("asx", "video/x-ms-asf");
            AddCache("vob", "video/x-ms-vob");
            AddCache("ogv", "video/ogg");
            AddCache("h261", "video/h261");
            AddCache("h263", "video/h263");
            AddCache("h264", "video/h264");
            AddCache("jpgv", "video/jpeg");
            AddCache("jpm", "video/jpm");
            AddCache("jpgm", "video/jpm");
            AddCache("webm", "video/webm");
            AddCache("movie", "video/x-sgi-movie");

            // document
            AddCache("doc", "application/msword");
            AddCache("dot", "application/msword");
            AddCache("docm", "application/vnd.ms-word.document.macroenabled.12");
            AddCache("dotm", "application/vnd.ms-word.template.macroenabled.12");
            AddCache("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            AddCache("dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template");
            AddCache("ppt", "application/vnd.ms-powerpoint");
            AddCache("pps", "application/vnd.ms-powerpoint");
            AddCache("pot", "application/vnd.ms-powerpoint");
            AddCache("ppam", "application/vnd.ms-powerpoint.addin.macroenabled.12");
            AddCache("pptm", "application/vnd.ms-powerpoint.presentation.macroenabled.12");
            AddCache("sldm", "application/vnd.ms-powerpoint.slide.macroenabled.12");
            AddCache("ppsm", "application/vnd.ms-powerpoint.slideshow.macroenabled.12");
            AddCache("potm", "application/vnd.ms-powerpoint.template.macroenabled.12");
            AddCache("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
            AddCache("sldx", "application/vnd.openxmlformats-officedocument.presentationml.slide");
            AddCache("ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow");
            AddCache("potx", "application/vnd.openxmlformats-officedocument.presentationml.template");
            AddCache("xla", "application/vnd.ms-excel");
            AddCache("xls", "application/vnd.ms-excel");
            AddCache("xlm", "application/vnd.ms-excel");
            AddCache("xlc", "application/vnd.ms-excel");
            AddCache("xlt", "application/vnd.ms-excel");
            AddCache("xlw", "application/vnd.ms-excel");
            AddCache("xlam", "application/vnd.ms-excel.addin.macroenabled.12");
            AddCache("xlsb", "application/vnd.ms-excel.sheet.binary.macroenabled.12");
            AddCache("xlsm", "application/vnd.ms-excel.sheet.macroenabled.12");
            AddCache("xltm", "application/vnd.ms-excel.template.macroenabled.12");
            AddCache("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            AddCache("xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template");
            AddCache("vsd", "application/vnd.visio");
            AddCache("vst", "application/vnd.visio");
            AddCache("vss", "application/vnd.visio");
            AddCache("vsw", "application/vnd.visio");
            AddCache("mpp", "application/vnd.ms-project");
            AddCache("mpt", "application/vnd.ms-project");
            AddCache("wps", "application/vnd.ms-works");
            AddCache("wks", "application/vnd.ms-works");
            AddCache("wcm", "application/vnd.ms-works");
            AddCache("wdb", "application/vnd.ms-works");
            AddCache("pub", "application/x-mspublisher");
            AddCache("onetoc", "application/onenote");
            AddCache("onetoc2", "application/onenote");
            AddCache("onetmp", "application/onenote");
            AddCache("onepkg", "application/onenote");
            AddCache("mdb", "application/x-msaccess");
            AddCache("pdf", "application/pdf");
            AddCache("rtf", "application/rtf");
            AddCache("xps", "application/vnd.ms-xpsdocument");
            AddCache("epub", "application/epub+zip");
            AddCache("prc", "application/x-mobipocket-ebook");
            AddCache("mobi", "application/x-mobipocket-ebook");
            AddCache("azw", "application/vnd.amazon.ebook");
            AddCache("odc", "application/vnd.oasis.opendocument.chart");
            AddCache("otc", "application/vnd.oasis.opendocument.chart-template");
            AddCache("odb", "application/vnd.oasis.opendocument.database");
            AddCache("odf", "application/vnd.oasis.opendocument.formula");
            AddCache("odft", "application/vnd.oasis.opendocument.formula-template");
            AddCache("odg", "application/vnd.oasis.opendocument.graphics");
            AddCache("otg", "application/vnd.oasis.opendocument.graphics-template");
            AddCache("odi", "application/vnd.oasis.opendocument.image");
            AddCache("oti", "application/vnd.oasis.opendocument.image-template");
            AddCache("odp", "application/vnd.oasis.opendocument.presentation");
            AddCache("otp", "application/vnd.oasis.opendocument.presentation-template");
            AddCache("ods", "application/vnd.oasis.opendocument.spreadsheet");
            AddCache("ots", "application/vnd.oasis.opendocument.spreadsheet-template");
            AddCache("odt", "application/vnd.oasis.opendocument.text");
            AddCache("odm", "application/vnd.oasis.opendocument.text-master");
            AddCache("ott", "application/vnd.oasis.opendocument.text-template");
            AddCache("oth", "application/vnd.oasis.opendocument.text-web");
            AddCache("oxt", "application/vnd.openofficeorg.extension");

            // misc
            AddCache("zip", "application/zip");
            AddCache("gzip", "application/gzip");
            AddCache("7z", "application/x-7z-compressed");
            AddCache("rar", "application/x-rar-compressed");
            AddCache("tar", "application/x-tar");
            AddCache("gtar", "application/x-gtar");
            AddCache("ace", "application/x-ace-compressed");
            AddCache("cab", "application/vnd.ms-cab-compressed");
            AddCache("dmg", "application/x-apple-diskimage");
            AddCache("iso", "application/x-iso9660-image");
            AddCache("air", "application/vnd.adobe.air-application-installer-package+zip");
            AddCache("apk", "application/vnd.android.package-archive");
            AddCache("exe", "application/x-msdownload");
            AddCache("com", "application/x-msdownload");
            AddCache("bat", "application/x-msdownload");
            AddCache("msi", "application/x-msdownload");
            AddCache("dll", "application/x-msdownload");
            AddCache("bin", "application/octet-stream");
            AddCache("ttf", "application/x-font-ttf");
            AddCache("woff", "application/font-woff");
            AddCache("otf", "application/x-font-otf");
            AddCache("eot", "application/vnd.ms-fontobject");
            AddCache("pkg", "application/octet-stream");
            AddCache("pgp", "application/pgp-encrypted");
            AddCache("asc", "application/pgp-signature");
            AddCache("sig", "application/pgp-signature");
            AddCache("p10", "application/pkcs10");
            AddCache("p7m", "application/pkcs7-mime");
            AddCache("p7c", "application/pkcs7-mime");
            AddCache("p7s", "application/pkcs7-signature");
            AddCache("p8", "application/pkcs8");
            AddCache("p12", "application/x-pkcs12");
            AddCache("pfx", "application/x-pkcs12");
            AddCache("p7b", "application/x-pkcs7-certificates");
            AddCache("spc", "application/x-pkcs7-certificates");
            AddCache("p7r", "application/x-pkcs7-certreqresp");
            AddCache("cer", "application/pkix-cert");
            AddCache("crl", "application/pkix-crl");
            AddCache("der", "application/x-x509-ca-cert");
            AddCache("crt", "application/x-x509-ca-cert");
            AddCache("jar", "application/java-archive");
            AddCache("ser", "application/java-serialized-object");
            AddCache("class", "application/java-vm");
            AddCache("swf", "application/x-shockwave-flash");
            AddCache("xap", "application/x-silverlight-app");
        }

        public string GetContentType(string extension)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(extension))
                {
                    return DefaultContentType;
                }

                extension = extension.Trim(' ', '.');

                ContentType contentType;
                if (_cache.TryGetValue(extension, out contentType))
                {
                    return contentType.Name;
                }

                var result = GetContentTypeFromWebservice(extension);
                AddCache(extension, result);
                return result;
            }
            catch
            {
                return DefaultContentType;
            }
        }

        public bool IsTextFile(string extension)
        {
            if (string.IsNullOrEmpty(extension))
            {
                return false;
            }

            ContentType contentType;
            if (_cache.TryGetValue(extension, out contentType))
            {
                return contentType.IsText;
            }

            return false;
        }

        public IEnumerable<string> GetContentTypesForPages()
        {
            yield return _cache["html"].Name;
            yield return _cache["txt"].Name;
            yield return _cache["ics"].Name;
            yield return _cache["xml"].Name;
            yield return _cache["xhtml"].Name;
            yield return _cache["atom"].Name;
            yield return _cache["rss"].Name;
            yield return _cache["json"].Name;
            yield return _cache["csv"].Name;
            yield return _cache["js"].Name;
            yield return _cache["css"].Name;
        }

        private string GetContentTypeFromWebservice(string extension)
        {
            var webRequest = WebRequest.Create(string.Format("http://www.stdicon.com/ext/{0}", extension));
            var response = webRequest.GetResponse();

            if (response.ContentLength > 0)
            {
                using (var stream = response.GetResponseStream())
                {
                    if (stream != null)
                    {
                        using (var sr = new StreamReader(stream))
                        {
                            var contentType = sr.ReadLine();
                            _log.WarnFormat("Add ContentType '{0}' for extension '{1}' to cache.", contentType, extension);
                            return contentType;
                        }
                    }

                    _log.WarnFormat("Add ContentType for extension '{0}' to cache.", extension);
                    return DefaultContentType;
                }
            }

            _log.WarnFormat("Add ContentType for extension '{0}' to cache.", extension);
            return DefaultContentType;
        }

        private static void AddCache(string extension, string contentType, bool isText = false)
        {
            var type = new ContentType(contentType, isText);
            _cache.Add(extension, type);
        }

        private class ContentType
        {
            public ContentType(string name, bool isText = false)
            {
                Name = name;
                IsText = isText;
            }

            public string Name { get; private set; }

            public bool IsText { get; private set; }
        }
    }
}
