namespace Cms.Aggregates.Pages.Events
{
    public class PageLinkCreated : EventBase<PageBase>
    {
        public ulong WebsiteId { get; set; }
        public ulong PageId { get; set; }
        public ulong ParentPageId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}