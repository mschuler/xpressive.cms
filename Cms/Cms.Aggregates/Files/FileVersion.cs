using System;

namespace Cms.Aggregates.Files
{
    public class FileVersion
    {
        public ulong Version { get; set; }
        public string Hash { get; set; }
        public ulong Size { get; set; }
        public DateTime Date { get; set; }
    }
}