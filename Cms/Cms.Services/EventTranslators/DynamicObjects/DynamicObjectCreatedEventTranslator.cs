using System;
using System.Linq;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectCreatedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;

        public DynamicObjectCreatedEventTranslator(IDynamicObjectDefinitionRepository definitionRepository)
        {
            _definitionRepository = definitionRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Neues Objekt erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectCreated)@event;
            var def = _definitionRepository.GetEvenIfDeleted(e.DefinitionId);
            var title = e.Title;

            if (string.IsNullOrEmpty(title) && !e.Values.TryGetValue("Title", out title))
            {
                var titleField = def.Fields.SingleOrDefault(f => f.SortOrder == 0);
                if (titleField == null || !e.Values.TryGetValue(titleField.Name, out title))
                {
                    title = "n/a";
                }
            }

            return string.Format(
                "Neues Objekt <strong>{2} (Nr. {3})</strong> vom Typ <strong>{0} (Nr. {1})</strong> wurde erstellt.",
                def.Name,
                def.PublicId,
                title,
                e.PublicId);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var e = (DynamicObjectCreated)@event;
            return _definitionRepository.GetEvenIfDeleted(e.DefinitionId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "object";
        }
    }
}