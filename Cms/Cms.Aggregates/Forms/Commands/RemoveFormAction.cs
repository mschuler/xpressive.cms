﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Commands
{
    [RequiredPermission("form", PermissionLevel.CanChange)]
    public class RemoveFormAction : CommandBase
    {
        [ValidId]
        public ulong Id { get; set; }
    }
}