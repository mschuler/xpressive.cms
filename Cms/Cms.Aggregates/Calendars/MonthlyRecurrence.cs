using System;

namespace Cms.Aggregates.Calendars
{
    public class MonthlyRecurrence : AppointmentRecurrence
    {
        private int _xthDay;
        private int _everyXthMonth;

        public DateTime? EndDate { get; set; }
        public bool IsEveryXthDay { get; set; }

        public int XthDay // entweder [1;31] oder [first, second, third, fourth, last]
        {
            get { return Math.Max(_xthDay, 1); }
            set { _xthDay = value; }
        }

        public int EveryXthMonth
        {
            get { return Math.Max(_everyXthMonth, 1); }
            set { _everyXthMonth = value; }
        }

        public DayRecurrence DayRecurrence { get; set; }

        public bool HasEndDate
        {
            get { return EndDate.HasValue; }
            set
            {
                if (!value)
                {
                    EndDate = null;
                }
            }
        }

        public override bool IsMatch(Appointment appointment, DateTime dateTime)
        {
            if (dateTime < appointment.StartDate.Date)
            {
                return false;
            }

            if (EndDate.HasValue && dateTime > EndDate.Value)
            {
                return false;
            }

            if (IsEveryXthDay)
            {
                if (dateTime.Day != XthDay) { return false; }
            }
            else
            {
                var dateTimeRecurrence = dateTime.DayOfWeek.ToRecurrence();
                if (!DayRecurrence.HasFlag(dateTimeRecurrence))
                {
                    return false;
                }

                var totalDayRecurrences = 0;
                var dayRecurrence = 0;

                for (var d = 1; d <= DateTime.DaysInMonth(dateTime.Year, dateTime.Month); d++)
                {
                    if (DayRecurrence.HasFlag(new DateTime(dateTime.Year, dateTime.Month, d).DayOfWeek.ToRecurrence()))
                    {
                        totalDayRecurrences++;
                        if (d <= dateTime.Day)
                        {
                            dayRecurrence++;
                        }
                    }
                }

                if (XthDay <= 4 && dayRecurrence != XthDay)
                {
                    return false;
                }
                if (XthDay == 5 && totalDayRecurrences != dayRecurrence)
                {
                    return false;
                }
            }

            var everyXthMonth = EveryXthMonth < 1 ? 1 : EveryXthMonth;
            if (everyXthMonth <= 1) { return true; }

            var month = new DateTime(appointment.StartDate.Year, appointment.StartDate.Month, 1);

            for (int i = everyXthMonth - 1; i < 1200; i += everyXthMonth)
            {
                var date = month.AddMonths(i);
                if (EndDate.HasValue && date > EndDate.Value.Date)
                {
                    return false;
                }

                if (date.Date == new DateTime(dateTime.Year, dateTime.Month, date.Day))
                {
                    return true;
                }
            }

            return false;
        }
    }
}