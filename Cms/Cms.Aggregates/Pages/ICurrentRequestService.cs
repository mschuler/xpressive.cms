namespace Cms.Aggregates.Pages
{
    public interface ICurrentRequestService
    {
        int Port { get; }

        bool IsDefaultPort { get; }

        string Scheme { get; }

        string Language { get; }
    }
}