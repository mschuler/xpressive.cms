using System;
using System.Linq;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Commands;
using Cms.Aggregates.Pages.Events;
using Cms.EventSourcing.Contracts.Commands;
using Xunit;

namespace Cms.Aggregates.Tests
{
    public class Given_a_page_command_handler : IDisposable
    {
        private readonly PageRepository _repository;
        private readonly PageCommandHandler _commandHandler;
        private readonly PageEventHandler _eventHandler;

        public Given_a_page_command_handler()
        {
            _repository = new PageRepository();
            _commandHandler = new PageCommandHandler(_repository, null, null);
            _eventHandler = new PageEventHandler(_repository, null);

            Execute(new CreateNewPage { AggregateId = 10, Name = "P10", PublicId = 10, WebsiteId = 1, ParentPageId = 0 });
            Execute(new CreateNewPage { AggregateId = 11, Name = "P11", PublicId = 11, WebsiteId = 1, ParentPageId = 10 });
            Execute(new CreateNewPage { AggregateId = 12, Name = "P12", PublicId = 12, WebsiteId = 1, ParentPageId = 10 });
            Execute(new CreateNewPage { AggregateId = 20, Name = "P20", PublicId = 20, WebsiteId = 1, ParentPageId = 0 });
            Execute(new CreateNewPage { AggregateId = 21, Name = "P21", PublicId = 21, WebsiteId = 1, ParentPageId = 20 });
            Execute(new CreateNewPage { AggregateId = 22, Name = "P22", PublicId = 22, WebsiteId = 1, ParentPageId = 20 });
            Execute(new CreateNewPage { AggregateId = 30, Name = "P30", PublicId = 30, WebsiteId = 1, ParentPageId = 0 });
            Execute(new CreateNewPage { AggregateId = 31, Name = "P31", PublicId = 31, WebsiteId = 1, ParentPageId = 30 });
            Execute(new CreateNewPage { AggregateId = 32, Name = "P32", PublicId = 32, WebsiteId = 1, ParentPageId = 30 });
            Execute(new CreateNewPage { AggregateId = 40, Name = "P40", PublicId = 40, WebsiteId = 1, ParentPageId = 0 });
            Execute(new CreateNewPage { AggregateId = 41, Name = "P41", PublicId = 41, WebsiteId = 1, ParentPageId = 40 });
            Execute(new CreateNewPage { AggregateId = 42, Name = "P42", PublicId = 42, WebsiteId = 1, ParentPageId = 40 });
            Execute(new CreateNewPage { AggregateId = 50, Name = "P50", PublicId = 50, WebsiteId = 1, ParentPageId = 0 });
            Execute(new CreateNewPage { AggregateId = 51, Name = "P51", PublicId = 51, WebsiteId = 1, ParentPageId = 50 });
            Execute(new CreateNewPage { AggregateId = 52, Name = "P52", PublicId = 52, WebsiteId = 1, ParentPageId = 50 });
        }

        [Fact]
        public void Then_all_pages_are_existing()
        {
            var pages = _repository.GetAll().ToList();
            Assert.Equal(15, pages.Count);
        }

        [Fact]
        public void Then_it_can_change_page_sort_order()
        {
            Execute(new MovePage { AggregateId = 10, SortOrder = 3 });
            Execute(new MovePage { AggregateId = 10, SortOrder = 2 });
            Execute(new MovePage { AggregateId = 10, SortOrder = 4 });
            Execute(new MovePage { AggregateId = 10, SortOrder = 1 });

            var sorted = _repository
                .GetAll()
                .Where(p => p.ParentPageId == 0)
                .OrderBy(p => p.SortOrder)
                .ThenBy(p => p.Name, StringComparer.OrdinalIgnoreCase)
                .ToList();

            Assert.Equal("P20", sorted[0].Name);
            Assert.Equal("P10", sorted[1].Name);
            Assert.Equal("P30", sorted[2].Name);
            Assert.Equal("P40", sorted[3].Name);
            Assert.Equal("P50", sorted[4].Name);
        }

        [Fact]
        public void Then_it_can_not_create_page_with_existing_name()
        {
            Assert.ThrowsAny<Exception>(() => Execute(new CreateNewPage { AggregateId = 100, Name = "P10", PublicId = 100, WebsiteId = 1 }));
        }

        [Fact]
        public void Then_it_can_not_move_page_to_another_position_with_existing_name()
        {
            Execute(new CreateNewPage { AggregateId = 100, Name = "P10", PublicId = 100, WebsiteId = 1, ParentPageId = 50 });
            Assert.ThrowsAny<Exception>(() => Execute(new MovePage { AggregateId = 100 }));
        }

        [Fact]
        public void Then_it_can_move_page_to_another_position_under_root_with_existing_but_deleted_name()
        {
            Execute(new CreateNewPage { AggregateId = 100, Name = "P10", PublicId = 100, WebsiteId = 1, ParentPageId = 50 });
            Execute(new DeletePage { AggregateId = 10 });
            Execute(new MovePage { AggregateId = 100 });

            Assert.Equal(13, _repository.GetAll().Count());
        }

        [Fact]
        public void Then_it_can_move_page_to_another_position_under_existing_with_existing_but_deleted_name()
        {
            Execute(new CreateNewPage { AggregateId = 100, Name = "P11", PublicId = 100, WebsiteId = 1, ParentPageId = 50 });
            Execute(new DeletePage { AggregateId = 11 });
            Execute(new MovePage { AggregateId = 100, ParentPageId = 10 });

            Assert.Equal(15, _repository.GetAll().Count());
        }

        public void Dispose()
        {
            _repository.Clear();
        }

        private void Execute(ICommand command)
        {
            _commandHandler.Handle(command, _eventHandler.Handle);
        }
    }
}