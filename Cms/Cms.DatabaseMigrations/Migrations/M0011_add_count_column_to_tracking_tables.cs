using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0011_add_count_column_to_tracking_tables : DatabaseMigrationBase
    {
        public M0011_add_count_column_to_tracking_tables() : base(11) { }

        public override void Execute(Database database)
        {
            database.Execute("alter table FileTracking add Count int not null default 1");

            database.Execute("alter table PageTracking add Count int not null default 1");
        }
    }
}