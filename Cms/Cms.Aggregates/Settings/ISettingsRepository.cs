﻿namespace Cms.Aggregates.Settings
{
    public interface ISettingsRepository
    {
        Settings GetSettings(ulong tenantId);
    }
}