using Cms.Aggregates.Security;

namespace Cms.Aggregates.Calendars.Commands
{
    [RequiredPermission("calendarcategory", PermissionLevel.CanEverything)]
    public class RemoveCategory : CommandBase
    {
        public string Name { get; set; }
    }
}