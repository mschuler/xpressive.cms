﻿using System;
using Cms.Aggregates.Pages;
using Cms.Services.Contracts;

namespace Cms.Services.PageContentHandlers
{
    internal class ContentBooleanHandler : IContentHandler
    {
        public bool CanHandle(string pageContentType)
        {
            return pageContentType.Equals("boolean", StringComparison.OrdinalIgnoreCase);
        }

        public string ContentType
        {
            get { return "Boolean"; }
        }

        public string GetImage()
        {
            return "fa fa-check-square-o";
        }

        public string Render(PageContentValueBase value)
        {
            var booleanValue = value as PageContentBooleanValue;
            return (booleanValue != null && booleanValue.Value).ToString();
        }

        public object GetValueForInterpreter(PageContentValueBase value)
        {
            return Render(value);
        }
    }
}