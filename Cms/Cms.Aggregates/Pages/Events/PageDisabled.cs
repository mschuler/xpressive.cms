namespace Cms.Aggregates.Pages.Events
{
    public class PageDisabled : EventBase<PageBase> { }
}