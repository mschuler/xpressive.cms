using System.Threading.Tasks;

namespace Cms.Services.Contracts
{
    public interface IIpToCountryService
    {
        Task<string> GetCountryAsync(string ipAddress);
    }
}