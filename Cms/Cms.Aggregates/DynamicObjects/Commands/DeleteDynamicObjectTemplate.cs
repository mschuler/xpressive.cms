﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("objecttemplate", PermissionLevel.CanEverything)]
    public class DeleteDynamicObjectTemplate : CommandBase
    {
        [ValidId]
        public ulong TemplateId { get; set; }
    }
}