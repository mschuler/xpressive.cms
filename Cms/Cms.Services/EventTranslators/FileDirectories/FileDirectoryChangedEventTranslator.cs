using System;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.Files;
using Cms.Aggregates.Files.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.FileDirectories
{
    internal class FileDirectoryChangedEventTranslator : IEventTranslator
    {
        private readonly IFileRepository _repository;
        private readonly IRepository<FileDirectory> _fileDirectoryRepository;

        public FileDirectoryChangedEventTranslator(IFileRepository repository, IRepository<FileDirectory> fileDirectoryRepository)
        {
            _repository = repository;
            _fileDirectoryRepository = fileDirectoryRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FileDirectoryChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Datei verschoben";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FileDirectoryChanged)@event;
            var file = _repository.GetEvenIfDeleted(e.AggregateId);
            var directory = _fileDirectoryRepository.GetEvenIfDeleted(e.FileDirectoryId);
            return string.Format("Die Datei <strong>{0} (Nr. {1})</strong> wurde nach <strong>{2}</strong> verschoben.", file.Name, file.PublicId, directory.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var e = (FileDirectoryChanged)@event;
            return _fileDirectoryRepository.GetEvenIfDeleted(e.FileDirectoryId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "file";
        }
    }
}