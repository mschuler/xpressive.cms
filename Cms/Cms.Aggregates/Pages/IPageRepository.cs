using System.Collections.Generic;

namespace Cms.Aggregates.Pages
{
    public interface IPageRepository : IRepository<PageBase>
    {
        int GetNextPublicId();

        Page GetByPublicId(int publicId);

        IEnumerable<PageBase> GetAllByWebsiteId(ulong websiteId);
    }
}