﻿using System.IO;
using System.Threading.Tasks;

namespace Cms.Services.Contracts
{
    public interface IVirusTotalService
    {
        Task<EnqueueResult> EnqueueAsync(FileInfo file, string fileName);

        Task<ReportResult> GetReportAsync(FileInfo file);

        Task<ReportResult> GetReportAsync(string scanId);

        bool IsValidApiKey();
    }

    public class EnqueueResult
    {
        public bool Success { get; set; }

        public string ScanId { get; set; }
    }

    public class ReportResult
    {
        public bool Success { get; set; }

        public bool IsMalicious { get; set; }
    }
}
