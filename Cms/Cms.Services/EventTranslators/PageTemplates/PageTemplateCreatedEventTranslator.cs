using System;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.PageTemplates.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.PageTemplates
{
    internal class PageTemplateCreatedEventTranslator : IEventTranslator
    {
        private readonly IPageTemplateRepository _repository;

        public PageTemplateCreatedEventTranslator(IPageTemplateRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageTemplateCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seitenvorlage erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageTemplateCreated)@event;
            var template = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Die Seitenvorlage <strong>{0} (Nr. {1})</strong> wurde erstellt.", template.Name, template.PublicId);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var e = (PageTemplateCreated)@event;
            return e.TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "pagetemplate";
        }
    }
}