﻿namespace Cms.Aggregates.Calendars.Events
{
    public class CategoryNameChanged : EventBase<Calendar>
    {
        public string OldName { get; set; }
        public string NewName { get; set; }
    }
}