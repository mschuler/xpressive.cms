using System;
using System.Collections.Generic;

namespace Cms.Services.UrlShortening
{
    public interface IUrlShorteningRepository
    {
        IEnumerable<Tuple<string, string>> GetAll();

        void Add(string longUrl, string shortUrl);
    }
}