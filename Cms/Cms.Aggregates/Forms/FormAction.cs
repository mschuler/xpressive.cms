﻿namespace Cms.Aggregates.Forms
{
    public class FormAction
    {
        public ulong Id { get; set; }
        public FormActionType Type { get; set; }
        public FormActionConfiguration Configuration { get; set; }

        protected bool Equals(FormAction other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }
            if (obj.GetType() != GetType()) { return false; }
            return Equals((FormAction)obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}