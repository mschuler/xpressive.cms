using System.Linq;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Security;
using Cms.Aggregates.Tenants.Events;

namespace Cms.Aggregates.PageTemplates.Events
{
    public class PageTemplateEventHandler : EventHandlerBase
    {
        private readonly IPageTemplateRepository _repository;
        private readonly IPageTemplateService _pageTemplateService;
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;

        public PageTemplateEventHandler(
            IPageTemplateRepository repository,
            IPageTemplateService pageTemplateService,
            IUserRepository userRepository,
            IGroupRepository groupRepository)
        {
            _repository = repository;
            _pageTemplateService = pageTemplateService;
            _userRepository = userRepository;
            _groupRepository = groupRepository;
        }

        public void When(PageTemplateCreated @event)
        {
            var template = new PageTemplate
            {
                Id = @event.AggregateId,
                TenantId = @event.TenantId,
                Name = @event.Name,
                PublicId = @event.PublicId,
                ContentType = @event.ContentType,
            };

            _repository.Add(template);
        }

        public void When(PageTemplateNameChanged @event)
        {
            var template = _repository.Get(@event.AggregateId);
            template.Name = @event.Name;
        }

        public void When(PageTemplateContentTypeChanged @event)
        {
            var template = _repository.Get(@event.AggregateId);
            template.ContentType = @event.ContentType;
        }

        public void When(PageTemplateIconChanged @event)
        {
            var template = _repository.Get(@event.AggregateId);
            template.IconFileId = @event.IconFileId;
        }

        public void When(PageTemplateTemplateChanged @event)
        {
            var template = _repository.Get(@event.AggregateId);
            template.Template = @event.Template;

            _pageTemplateService.Reset(template);
        }

        public void When(PageTemplateShared @event)
        {
            var template = _repository.Get(@event.AggregateId);
            var user = _userRepository.Get(@event.UserId);
            var allowedTenantIds = user.GroupIds.Select(g => _groupRepository.Get(g).TenantId).ToList();
            var sharedTenantIds = @event.TenantIds.Union(template.SharedForTenants).Distinct().ToList();
            sharedTenantIds.RemoveAll(t => allowedTenantIds.Contains(t) && !@event.TenantIds.Contains(t));
            template.Share(sharedTenantIds);
        }

        public void When(PageTemplateDeleted @event)
        {
            _repository.Delete(@event.AggregateId);
        }

        public void When(TenantDeleted @event)
        {
            _repository.DeleteAllByTenantId(@event.AggregateId);
        }
    }
}
