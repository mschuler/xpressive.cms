using System;
using System.Linq;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.Aggregates.Tenants;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    internal class DynamicObjectDefinitionCommandHandler : CommandHandlerBase
    {
        private readonly IDynamicObjectDefinitionRepository _repository;
        private readonly ITenantRepository _tenantRepository;

        public DynamicObjectDefinitionCommandHandler(IDynamicObjectDefinitionRepository repository, ITenantRepository tenantRepository)
        {
            _repository = repository;
            _tenantRepository = tenantRepository;
        }

        public void When(CreateDynamicObjectDefinition command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            var name = command.Name.Trim().RemoveControlChars();
            var publicId = _repository.GetNextPublicId();

            var e = new DynamicObjectDefinitionCreated
            {
                AggregateId = command.AggregateId,
                TenantId = command.TenantId,
                UserId = command.UserId,
                PublicId = publicId,
                Name = name
            };

            eventHandler(e);
        }

        public void When(AddDynamicObjectDefinitionField command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var definition = _repository.Get(command.AggregateId);

            var any = definition.Fields.Any(f => string.Equals(f.Name, name, StringComparison.OrdinalIgnoreCase));

            if (any)
            {
                throw new ExceptionWithReason("There is already a field with this name.");
            }

            var e = new DynamicObjectDefinitionFieldAdded
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
                ContentType = command.ContentType,
                ContentTypeOptions = command.ContentTypeOptions,
            };

            eventHandler(e);
        }

        public void When(ChangeDynamicObjectDefinitionFieldContentTypeOptions command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var definition = _repository.Get(command.AggregateId);
            var field = definition.Fields.SingleOrDefault(f => string.Equals(f.Name, name, StringComparison.Ordinal));

            if (field == null)
            {
                throw new ExceptionWithReason("There is no field with this name.");
            }

            var e = new DynamicObjectDefinitionFieldContentTypeOptionsChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
                ContentTypeOptions = command.ContentTypeOptions,
            };

            eventHandler(e);
        }

        public void When(RemoveDynamicObjectDefinitionField command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var definition = _repository.Get(command.AggregateId);

            AssertField(definition, name);

            var e = new DynamicObjectDefinitionFieldRemoved
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(RenameDynamicObjectDefinitionField command, Action<IEvent> eventHandler)
        {
            var oldName = command.OldName.Trim().RemoveControlChars();
            var newName = command.NewName.Trim().RemoveControlChars();
            var definition = _repository.Get(command.AggregateId);

            AssertField(definition, oldName);

            if (oldName.Equals(newName, StringComparison.Ordinal))
            {
                return;
            }

            if (!oldName.Equals(newName, StringComparison.OrdinalIgnoreCase))
            {
                if (definition.Fields.Any(f => string.Equals(f.Name, newName, StringComparison.OrdinalIgnoreCase)))
                {
                    throw new ExceptionWithReason("There is already a field with this name.");
                }
            }

            var e = new DynamicObjectDefinitionFieldRenamed
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                OldName = oldName,
                NewName = newName,
            };

            eventHandler(e);
        }

        public void When(MoveDynamicObjectDefinitionFieldDown command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var definition = _repository.Get(command.AggregateId);

            AssertField(definition, name);

            var e = new DynamicObjectDefinitionFieldMovedDown
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(MoveDynamicObjectDefinitionFieldUp command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var definition = _repository.Get(command.AggregateId);

            AssertField(definition, name);

            var e = new DynamicObjectDefinitionFieldMovedUp
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(MarkDynamicObjectDefinitionFieldSortable command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var definition = _repository.Get(command.AggregateId);

            AssertField(definition, name);

            var e = new DynamicObjectDefinitionFieldMarkedSortable
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
                IsSortable = command.IsSortable,
            };

            eventHandler(e);
        }

        public void When(RenameDynamicObjectDefinition command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var definition = _repository.Get(command.AggregateId);

            if (string.Equals(definition.Name, name, StringComparison.Ordinal))
            {
                throw new ExceptionWithReason("This object definition is already named like this.");
            }

            var e = new DynamicObjectDefinitionNameChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(DeleteDynamicObjectDefinition command, Action<IEvent> eventHandler)
        {
            var definition = _repository.Get(command.AggregateId);

            var e = new DynamicObjectDefinitionDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                PublicId = definition.PublicId,
                Name = definition.Name,
            };

            eventHandler(e);
        }

        public void When(ShareDynamicObjectDefinition command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            foreach (var tenantId in command.TenantIds)
            {
                _tenantRepository.Get(tenantId);
            }

            var e = new DynamicObjectDefinitionShared
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TenantIds = command.TenantIds.ToList(),
            };

            eventHandler(e);
        }

        private static void AssertField(DynamicObjectDefinition definition, string name)
        {
            var any = definition.Fields.Any(f => string.Equals(f.Name, name, StringComparison.OrdinalIgnoreCase));

            if (!any)
            {
                throw new ExceptionWithReason("There is no field with this name.");
            }
        }
    }
}