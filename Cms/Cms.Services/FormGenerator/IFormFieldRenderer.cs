﻿using System.Text;
using Cms.Services.Contracts;

namespace Cms.Services.FormGenerator
{
    internal interface IFormFieldRenderer
    {
        void Render(FormLayoutType type, StringBuilder builder, string id);
    }
}