﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Cms.Aggregates;
using Cms.Aggregates.Files;
using Cms.Services.Contracts;
using log4net;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Newtonsoft.Json;
using File = Cms.Aggregates.Files.File;

namespace Cms.WebNew.FileSystem
{
    public class CmsFileSystem : IFileSystem
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(CmsFileSystem));

        private static readonly ConcurrentDictionary<string, FileInfo> _cache = new ConcurrentDictionary<string, FileInfo>(StringComparer.Ordinal);

        private readonly Regex _fileRegex = new Regex(
            @"^/(?<id>[0-9]+)/(?<version>[0-9]+)(?:/(?<settings>[a-zA-Z_=0-9-]+))?/(?<name>.+)",
            RegexOptions.Compiled | RegexOptions.Singleline,
            TimeSpan.FromSeconds(1));

        private readonly ILocalFileStore _localFileStore;
        private readonly IFileRepository _fileRepository;
        private readonly ITemporaryFileStoreService _temporaryFileStoreService;

        public CmsFileSystem(ILocalFileStore localFileStore, IFileRepository fileRepository, ITemporaryFileStoreService temporaryFileStoreService)
        {
            _localFileStore = localFileStore;
            _fileRepository = fileRepository;
            _temporaryFileStoreService = temporaryFileStoreService;
        }

        public bool TryGetFileInfo(string subpath, out IFileInfo fileInfo)
        {
            var match = _fileRegex.Match(subpath);
            if (match.Success)
            {
                var fileId = ulong.Parse(match.Groups["id"].Value);
                var fileVersion = ulong.Parse(match.Groups["version"].Value);
                var settings = match.Groups["settings"].Value;
                var imageSettings = new ImageSettingsDto();

                if (!string.IsNullOrEmpty(settings))
                {
                    imageSettings = ImageSettingsDto.Deserialize(settings);
                }

                File file;
                FileInfo cachedFile;

                if (_fileRepository.TryGet(fileId, out file))
                {
                    var internalFile = _localFileStore.Get(fileId, fileVersion);
                    var cacheKey = GetCacheKey(file, fileVersion, imageSettings);

                    if (_cache.TryGetValue(cacheKey, out cachedFile))
                    {
                        fileInfo = new CmsFileInfo(cachedFile, new ImageSettingsDto(), stream => { });
                        return true;
                    }

                    if (internalFile.Exists)
                    {
                        fileInfo = new CmsFileInfo(internalFile, imageSettings, stream =>
                        {
                            var temporaryFile = _temporaryFileStoreService.Create();
                            var buffer = new byte[1024 * 64];

                            using (var fs = temporaryFile.File.OpenWrite())
                            {
                                int read;
                                do
                                {
                                    read = stream.Read(buffer, 0, buffer.Length);
                                    fs.Write(buffer, 0, read);
                                } while (read == buffer.Length);
                            }

                            _cache.AddOrUpdate(cacheKey, temporaryFile.File, (s, o) =>
                            {
                                temporaryFile.File.Delete();
                                return o;
                            });

                            stream.Seek(0, SeekOrigin.Begin);
                        });
                        return true;
                    }
                }
            }

            fileInfo = null;
            return false;
        }

        public bool TryGetDirectoryContents(string subpath, out IEnumerable<IFileInfo> contents)
        {
            contents = null;
            return false;
        }

        internal void PrepareResponse(StaticFileResponseContext context)
        {
            context.OwinContext.Response.Headers.Add("Expires", new[] { DateTime.UtcNow.AddYears(1).ToString("r") });
        }

        private string GetCacheKey(File file, ulong version, ImageSettingsDto settings)
        {
            settings = settings ?? new ImageSettingsDto();

            var tempfile = new TempFile
            {
                FileId = file.Id,
                FileVersion = version,
                Size = settings.Size,
                Quality = settings.Quality,
                Alpha = settings.Alpha,
                Brightness = settings.Brightness,
                Contrast = settings.Contrast,
                Filter = settings.Filter,
                RoundedCorners = settings.RoundedCorners,
                Saturation = settings.Saturation,
                Tint = settings.Tint
            };

            return GetCacheKey(tempfile);
        }

        private string GetCacheKey(TempFile tempFile)
        {
            var json = JsonConvert.SerializeObject(tempFile, Formatting.None);
            var data = Encoding.UTF8.GetBytes(json);

            using (var algorithm = new SHA1Managed())
            {
                var hash = algorithm.ComputeHash(data, 0, data.Length);
                return Convert.ToBase64String(hash);
            }
        }

        private class TempFile
        {
            public ulong FileId { get; set; }
            public ulong FileVersion { get; set; }
            public int Size { get; set; }
            public int Quality { get; set; }
            public int Alpha { get; set; }
            public int Saturation { get; set; }
            public int RoundedCorners { get; set; }
            public int Brightness { get; set; }
            public int Contrast { get; set; }
            public string Tint { get; set; }
            public string Filter { get; set; }
        }
    }
}