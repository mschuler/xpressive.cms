namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0001_create_version_table : DatabaseSqlMigration
    {
        public M0001_create_version_table() : base(1) { }

        protected override string GetSql()
        {
            return "create table [Version] ([Version] int primary key)";
        }
    }
}