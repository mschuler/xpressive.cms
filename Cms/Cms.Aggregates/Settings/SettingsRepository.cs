﻿using System.Collections.Generic;

namespace Cms.Aggregates.Settings
{
    internal class SettingsRepository : ISettingsRepository
    {
        private static readonly IDictionary<ulong, Settings> _cache = new Dictionary<ulong, Settings>();
        private static readonly object _lock = new object();

        public Settings GetSettings(ulong tenantId)
        {
            Settings settings;
            if (_cache.TryGetValue(tenantId, out settings))
            {
                return settings;
            }

            lock (_lock)
            {
                if (_cache.TryGetValue(tenantId, out settings))
                {
                    return settings;
                }

                settings = new Settings(tenantId);
                _cache.Add(tenantId, settings);
            }

            return settings;
        }
    }
}
