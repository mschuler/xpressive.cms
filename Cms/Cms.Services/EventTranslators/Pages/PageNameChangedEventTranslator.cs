using System;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.Websites;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Pages
{
    internal class PageNameChangedEventTranslator : IEventTranslator
    {
        private readonly IPageRepository _repository;
        private readonly IWebsiteRepository _websiteRepository;

        public PageNameChangedEventTranslator(IPageRepository repository, IWebsiteRepository websiteRepository)
        {
            _repository = repository;
            _websiteRepository = websiteRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageNameChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seite umbenannt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageNameChanged)@event;
            var page = (Page)_repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Die Seite <strong>(Nr. {0})</strong> wurde in <strong>{1}</strong> umbenannt.", page.PublicId, e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var page = _repository.GetEvenIfDeleted(@event.AggregateId);
            return _websiteRepository.GetEvenIfDeleted(page.WebsiteId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "page";
        }
    }
}