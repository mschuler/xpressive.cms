using System.Linq;
using System.Reflection;
using Cms.EventSourcing;
using Cms.EventSourcing.Contracts.Commands;
using Xunit;

namespace Cms.Aggregates.Tests
{
    public class Given_a_command
    {
        [Fact]
        public void Then_there_is_a_command_handler()
        {
            var commands = typeof(CommandBase).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(ICommand)))
                .ToList();

            var commandHandlers = typeof(CommandBase).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(ICommandHandler)))
                .ToList();

            foreach (var commandType in commands)
            {
                var isHandled = false;
                foreach (var commandHandlerType in commandHandlers)
                {
                    if (RedirectToWhen.IsMethodAvailable(commandHandlerType, commandType))
                    {
                        isHandled = true;
                        break;
                    }
                }
                Assert.True(isHandled, string.Format("Command '{0}' doesn't have a CommandHandler.", commandType.Name));
            }
        }

        [Fact]
        public void Then_there_is_a_required_permission_attribute()
        {
            var commands = typeof(CommandBase).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(ICommand)))
                .ToList();

            foreach (var command in commands)
            {
                var attribute = command.GetCustomAttribute<RequiredPermissionAttribute>();

                var msg = string.Format("Command {0} don't have a RequiredPermission attribute.", command.Name);
                Assert.True(attribute != null, msg);

                if (attribute.IsRequired)
                {
                    msg = string.Format("RequiredPermission attribute from Command {0} has no ModuleId defined.", command.Name);
                    Assert.False(string.IsNullOrEmpty(attribute.ModuleId), msg);
                }
            }
        }
    }
}