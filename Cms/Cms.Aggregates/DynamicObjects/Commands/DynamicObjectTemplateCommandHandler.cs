using System;
using System.Linq;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    internal class DynamicObjectTemplateCommandHandler : CommandHandlerBase
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectTemplateCommandHandler(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public void When(CreateDynamicObjectTemplate command, Action<IEvent> eventHandler)
        {
            var definition = _repository.Get(command.AggregateId);
            var name = command.Name.Trim().RemoveControlChars();
            var publicId = _repository.GetNextPublicIdForTemplate();

            var any = definition.Templates.Any(t => t.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

            if (any)
            {
                throw new ExceptionWithReason("There is already a template with this name.");
            }

            var e = new DynamicObjectTemplateCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TemplateId = command.TemplateId,
                Name = name,
                PublicId = publicId,
            };

            eventHandler(e);
        }

        public void When(ChangeDynamicObjectTemplate command, Action<IEvent> eventHandler)
        {
            var definition = _repository.Get(command.AggregateId);
            var template = definition.Templates.SingleOrDefault(t => t.Id.Equals(command.TemplateId));

            if (template == null)
            {
                throw new ExceptionWithReason("There is no template with this id.");
            }

            var e = new DynamicObjectTemplateChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TemplateId = command.TemplateId,
                Template = command.Template.RemoveControlChars(),
                Name = template.Name,
            };

            eventHandler(e);
        }

        public void When(RenameDynamicObjectTemplate command, Action<IEvent> eventHandler)
        {
            var definition = _repository.Get(command.AggregateId);
            var name = command.Name.Trim().RemoveControlChars();
            var template = definition.Templates.SingleOrDefault(t => t.Id.Equals(command.TemplateId));

            if (template == null)
            {
                throw new ExceptionWithReason("There is no template with this id.");
            }

            var any = definition.Templates.Any(t => t.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            var equal = template.Name.Equals(name, StringComparison.OrdinalIgnoreCase);

            if (any && !equal)
            {
                throw new ExceptionWithReason("There is already a template with this name.");
            }

            var e = new DynamicObjectTemplateRenamed
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TemplateId = command.TemplateId,
                Name = name,
                OldName = template.Name,
            };

            eventHandler(e);
        }

        public void When(DeleteDynamicObjectTemplate command, Action<IEvent> eventHandler)
        {
            var definition = _repository.Get(command.AggregateId);
            var template = definition.Templates.SingleOrDefault(t => t.Id.Equals(command.TemplateId));

            if (template == null)
            {
                throw new ExceptionWithReason("There is no template with this id.");
            }

            var e = new DynamicObjectTemplateDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TemplateId = command.TemplateId,
                Name = template.Name,
            };

            eventHandler(e);
        }
    }
}