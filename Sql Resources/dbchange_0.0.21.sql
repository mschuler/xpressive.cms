
begin transaction

create table MediaContentImage (
	Id int not null identity(1, 1),
	FileId int not null,
	MediaContentId int not null,

	constraint PK_MediaContentImage primary key (Id),
	constraint FK_MediaContentImage_MediaContent foreign key (MediaContentId) references MediaContent (Id),
	constraint FK_MediaContentImage_File foreign key (FileId) references [File] (Id)
)

insert into Version (Major, Minor, Revision) values (0, 0, 21);

commit transaction
