﻿using System.Collections.Generic;
using System.Linq;
using Cms.Services.Contracts;

namespace Cms.Services.PageContentHandlers
{
    internal class ContentHandlerFactory : IContentHandlerFactory
    {
        private readonly IList<IContentHandler> _handlers;

        public ContentHandlerFactory(IEnumerable<IContentHandler> handlers)
        {
            _handlers = handlers.ToList();
        }

        public IContentHandler Get(string pageContentType)
        {
            return _handlers.SingleOrDefault(h => h.CanHandle(pageContentType));
        }

        public IEnumerable<string> ContentTypes
        {
            get { return _handlers.Select(h => h.ContentType); }
        }
    }
}
