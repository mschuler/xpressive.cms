using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cms.Services.Contracts;

namespace Cms.Services.FormGenerator
{
    internal class FormFieldRadioRenderer : IFormFieldRenderer
    {
        private readonly string _name;
        private readonly IList<string> _options;
        private readonly bool _isRequired;

        public FormFieldRadioRenderer(string name, bool isRequired, IEnumerable<string> options)
        {
            _name = name;
            _isRequired = isRequired;
            _options = options.ToList();
        }

        public void Render(FormLayoutType type, StringBuilder builder, string id)
        {
            var required = _isRequired ? "required " : string.Empty;

            foreach (var option in _options)
            {
                if (type == FormLayoutType.Above)
                {
                    builder.AppendLine("  <div class=\"radio\">");
                    builder.AppendLine("    <label>");
                    builder.AppendFormat("      <input type=\"radio\" name=\"{0}\" value=\"{1}\" {2}/> ", _name, option, required);
                    builder.AppendLine(_name);
                    builder.AppendLine("    </label>");
                    builder.AppendLine("  </div>");
                }
                else if (type == FormLayoutType.Asside)
                {
                    builder.AppendLine("  <div class=\"form-group\">");
                    builder.AppendLine("    <div class=\"col-sm-offset-2 col-sm-10\">");
                    builder.AppendLine("      <div class=\"radio\">");
                    builder.AppendLine("        <label>");
                    builder.AppendFormat("          <input type=\"radio\" name=\"{0}\" value=\"{1}\" {2}/> ", _name, option, required);
                    builder.AppendLine(_name);
                    builder.AppendLine("        </label>");
                    builder.AppendLine("      </div>");
                    builder.AppendLine("    </div>");
                    builder.AppendLine("  </div>");
                }
                else if (type == FormLayoutType.NoLabel)
                {
                    builder.AppendLine("  <label>");
                    builder.AppendFormat("    <input type=\"radio\" name=\"{0}\" value=\"{1}\" {2}/> ", _name, option, required);
                    builder.AppendLine(_name);
                    builder.AppendLine("  </label>");
                }
                else
                {
                    throw new NotSupportedException(type.ToString());
                }
            }
        }
    }
}