﻿namespace Cms.Aggregates.Websites.Events
{
    public class DomainAdded : EventBase<Website>
    {
        public string Host { get; set; }
    }
}