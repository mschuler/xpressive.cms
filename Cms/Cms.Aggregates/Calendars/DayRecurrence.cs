﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cms.Aggregates.Calendars
{
    [Flags, JsonConverter(typeof(StringEnumConverter))]
    public enum DayRecurrence
    {
        Mondays = 1,
        Tuesdays = 2,
        Wednesdays = 4,
        Thursdays = 8,
        Fridays = 16,
        Saturdays = 32,
        Sundays = 64,
        Daily = Workdays | WeekendDays,
        Workdays = Mondays | Tuesdays | Wednesdays | Thursdays | Fridays,
        WeekendDays = Saturdays | Sundays,
    }
}