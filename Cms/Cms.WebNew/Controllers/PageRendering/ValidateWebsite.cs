﻿using System.Net;
using System.Net.Http;
using Cms.Aggregates.Websites;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class ValidateWebsite : IFilter<RenderPageMessage>
    {
        private readonly IWebsiteRepository _repository;

        public ValidateWebsite(IWebsiteRepository repository)
        {
            _repository = repository;
        }

        public bool Execute(RenderPageMessage msg)
        {
            var host = msg.Request.RequestUri.Host;
            msg.Website = _repository.GetByHost(host);

            if (msg.Website == null)
            {
                msg.Response = new HttpResponseMessage(HttpStatusCode.NotFound);
                return false;
            }

            return true;
        }
    }
}