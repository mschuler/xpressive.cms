﻿using System.Collections.Generic;

namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDefinitionShared : EventBase<DynamicObjectDefinition>
    {
        public IList<ulong> TenantIds { get; set; }
    }
}