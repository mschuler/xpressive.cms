﻿using System.Collections.Generic;

namespace Cms.Aggregates.PageTemplates.Events
{
    public class PageTemplateShared : EventBase<PageTemplate>
    {
        public IList<ulong> TenantIds { get; set; }
    }
}