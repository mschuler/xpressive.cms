﻿using System.Collections.Generic;

namespace Cms.Aggregates.DynamicObjects
{
    internal class UserStarredDynamicObjectDefinitionRepository
        : UserStarredRepositoryBase<DynamicObjectDefinition>, IUserStarredDynamicObjectDefinitionRepository
    {
        private static readonly object _lock = new object();
        private static readonly Dictionary<ulong, List<ulong>> _definitionsByUser = new Dictionary<ulong, List<ulong>>();
        private readonly IDynamicObjectDefinitionRepository _repository;

        public UserStarredDynamicObjectDefinitionRepository(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        protected override IDictionary<ulong, List<ulong>> GetUserStarredRepository()
        {
            return _definitionsByUser;
        }

        protected override IRepository<DynamicObjectDefinition> GetRepository()
        {
            return _repository;
        }

        protected override object GetLockObject()
        {
            return _lock;
        }
    }
}