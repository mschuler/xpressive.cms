using System.Linq;
using System.Reflection;
using Cms.EventSourcing;
using Cms.EventSourcing.Contracts.Queries;
using Xunit;

namespace Cms.Aggregates.Tests
{
    public class Given_a_query
    {
        [Fact]
        public void Then_there_is_a_query_handler()
        {
            var queries = typeof(QueryBase).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(IQuery)))
                .ToList();

            var queryHandlers = typeof(QueryBase).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(IQueryHandler)))
                .ToList();

            foreach (var queryType in queries)
            {
                var isHandled = false;
                foreach (var queryHandlerType in queryHandlers)
                {
                    if (RedirectToWhen.IsMethodAvailable(queryHandlerType, queryType))
                    {
                        isHandled = true;
                        break;
                    }
                }
                Assert.True(isHandled, string.Format("Query '{0}' doesn't have a QueryHandler.", queryType.Name));
            }
        }

        [Fact]
        public void Then_there_is_a_required_permission_attribute()
        {
            var queries = typeof(QueryBase).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(IQuery)))
                .ToList();

            foreach (var query in queries)
            {
                var attributes = query.GetCustomAttributes<RequiredPermissionAttribute>().ToList();

                var msg = string.Format("Query {0} don't have a RequiredPermission attribute.", query.Name);
                Assert.True(attributes.Any(), msg);

                foreach (var attribute in attributes)
                {
                    if (attribute.IsRequired)
                    {
                        msg = string.Format("RequiredPermission attribute from Query {0} has no ModuleId defined.", query.Name);
                        Assert.False(string.IsNullOrEmpty(attribute.ModuleId), msg);
                    }
                }
            }
        }
    }
}