using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("objecttemplate", PermissionLevel.CanChange)]
    public class ChangeDynamicObjectDefinitionFieldContentTypeOptions : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string ContentTypeOptions { get; set; }
    }
}