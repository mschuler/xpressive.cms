using System;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class UserCreatedEventTranslator : IEventTranslator
    {
        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(UserCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Neuen Benutzer erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (UserCreated)@event;
            var username = string.Format("{0} {1}", e.FirstName, e.LastName).Trim();
            return string.Format("Der Benutzer <strong>{0}</strong> wurde neu erstellt.", username);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}