using System.Collections.Generic;

namespace Cms.Aggregates.Pages
{
    internal class UserStarredPageRepository : UserStarredRepositoryBase<PageBase>, IUserStarredPageRepository
    {
        private static readonly object _lock = new object();
        private static readonly Dictionary<ulong, List<ulong>> _pagesPerUser = new Dictionary<ulong, List<ulong>>();
        private readonly IPageRepository _pageRepository;

        public UserStarredPageRepository(IPageRepository pageRepository)
        {
            _pageRepository = pageRepository;
        }

        protected override IDictionary<ulong, List<ulong>> GetUserStarredRepository()
        {
            return _pagesPerUser;
        }

        protected override IRepository<PageBase> GetRepository()
        {
            return _pageRepository;
        }

        protected override object GetLockObject()
        {
            return _lock;
        }
    }
}