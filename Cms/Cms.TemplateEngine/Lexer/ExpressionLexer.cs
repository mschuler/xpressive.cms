﻿using System.Linq;

namespace Cms.TemplateEngine.Lexer
{
    internal static class ExpressionLexer
    {
        public static Token GenerateTree(string expresion)
        {
            var currentToken = new Token();
            var isText = false;
            var isEscaped = false;
            var textCharacter = '"';

            foreach (var c in expresion)
            {
                if (isEscaped)
                {
                    currentToken.Value += c;
                    isEscaped = false;
                }
                else if (c.Equals('(') && !isText)
                {
                    var newToken = new Token { Parent = currentToken };
                    currentToken.Children.Add(newToken);
                    currentToken = newToken;
                }
                else if (c.Equals(')') && !isText)
                {
                    currentToken = currentToken.Parent;
                }
                else if (".,".ToList().Contains(c) && !isText)
                {
                    var parent = currentToken.Parent;

                    if (parent == null)
                    {
                        parent = new Token();
                        parent.Children.Add(currentToken);
                        currentToken.Parent = parent;
                    }

                    var newToken = new Token { Parent = parent };
                    parent.Children.Add(newToken);
                    currentToken = newToken;

                    if (c.Equals('.'))
                    {
                        currentToken.IsBelongingToLeftToken = true;
                    }
                }
                else if (c.Equals('\\') && isText)
                {
                    isEscaped = true;
                }
                else
                {
                    if ("\"'".ToList().Contains(c))
                    {
                        if (!isText)
                        {
                            textCharacter = c;
                            isText = true;
                        }
                        else if (textCharacter == c)
                        {
                            isText = false;
                        }
                    }

                    currentToken.Value += c;
                }
            }

            while (currentToken.Parent != null)
            {
                currentToken = currentToken.Parent;
            }

            return currentToken;
        }
    }
}
