﻿namespace Cms.Aggregates.SystemMessages.Events
{
    public class SystemMessageDeleted : EventBase<SystemMessage> { }
}