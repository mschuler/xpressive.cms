﻿using System;
using Cms.Aggregates.Calendars;
using Xunit;

namespace Cms.Aggregates.Tests
{
    public class Given_an_appointment_with_a_single_occurence
    {
        private static readonly Appointment _appointment = new Appointment
        {
            StartEnd = new DateTimeRange(new DateTime(2016, 2, 29, 14, 30, 0), new DateTime(2016, 2, 29, 16, 0, 0)),
            Recurrence = new SingleOccurrence()
        };

        [Fact]
        public void Then_its_valid_only_at_this_date()
        {
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 2, 28)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 2, 20)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2016, 3, 1)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2016, 2, 28)));
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2016, 2, 29)));
        }
    }

    public class Given_an_appointment_with_a_daily_recurrence_without_end_date
    {
        private static readonly Appointment _appointment = new Appointment
        {
            StartEnd = new DateTimeRange(new DateTime(2015, 3, 20, 10, 0, 0), new DateTime(DateTime.MaxValue.Year, DateTime.MaxValue.Month, DateTime.MaxValue.Day, 12, 0, 0)),
        };

        [Fact]
        public void When_it_occurs_every_weekday_then_its_valid_on_monday_to_friday()
        {
            var appointment = new Appointment
            {
                StartEnd = _appointment.StartEnd,
                Recurrence = new DailyRecurrence { IsEveryXthDay = false },
            };

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 19))); // too early
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 20)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 21))); // saturday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 22))); // sunday
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 23))); // monday
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 24))); // tuesday
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 25))); // wednesday
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 26))); // thursday
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 27))); // friday
        }

        [Fact]
        public void When_it_occurs_every_third_day_then_its_valid_every_third_day_even_at_weekend()
        {
            var appointment = new Appointment
            {
                StartEnd = _appointment.StartEnd,
                Recurrence = new DailyRecurrence { IsEveryXthDay = true, EveryXthDay = 3 },
            };

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 19))); // too early
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 20)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 21))); // 1
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 22))); // 2
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 23))); // 3
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 24))); // 1
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 25))); // 2
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 26))); // 3
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 27))); // 1
        }
    }

    public class Given_an_appointment_with_a_daily_recurrence
    {
        private static readonly Appointment _appointment = new Appointment
        {
            StartEnd = new DateTimeRange(new DateTime(2015, 3, 1), new DateTime(2015, 3, 31)),
            Recurrence = new DailyRecurrence { IsEveryXthDay = true, EveryXthDay = 9 },
        };

        [Fact]
        public void When_it_occurs_every_ninth_day_then_its_valid_every_ninth_day()
        {
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 1)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 2)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 9)));
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 10)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 11)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 18)));
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 19)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 20)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 27)));
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 28)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 29)));
        }
    }

    public class Given_an_appointment_with_a_weekly_recurrence
    {
        private static readonly Appointment _appointment = new Appointment
        {
            StartEnd = new DateTimeRange(new DateTime(2015, 3, 1), new DateTime(2015, 3, 31)),
            Recurrence = new WeeklyRecurrence { EveryXthWeek = 2, IsOnMonday = true, IsOnTuesday = true },
        };

        [Fact]
        public void When_it_occurs_every_second_week_then_its_valid_every_second_week()
        {
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 2)));
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 3)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 9)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 10)));
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 16)));
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 17)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 23)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 24)));
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 30)));
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 31)));
        }

        [Fact]
        public void When_it_occurs_only_on_some_weekdays_then_its_valid_only_on_these_weekdays()
        {
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 2)));
            Assert.True(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 3)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 4)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 5)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 6)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 7)));
            Assert.False(_appointment.Recurrence.IsMatch(_appointment, new DateTime(2015, 3, 8)));
        }
    }

    public class Given_an_appointment_with_a_monthly_recurrence
    {
        private static readonly Appointment _appointment = new Appointment
        {
            StartEnd = new DateTimeRange(new DateTime(2015, 1, 1), new DateTime(2015, 12, 31)),
        };

        [Fact]
        public void When_it_occurs_every_third_day_of_every_fourth_month_then_its_valid_only_on_these_days()
        {
            var appointment = new Appointment
            {
                StartEnd = _appointment.StartEnd,
                Recurrence = new MonthlyRecurrence { IsEveryXthDay = true, XthDay = 3, EveryXthMonth = 4 },
            };

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 2)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 3)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 4)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 2)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 3)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 4)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 2)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 3)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 4)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 4, 2)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 4, 3)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 4, 4)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 5, 2)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 5, 3)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 5, 4)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 6, 2)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 6, 3)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 6, 4)));
        }

        [Fact]
        public void When_it_occurs_every_third_tuesday_then_its_valid_every_thid_tuesday()
        {
            var appointment = new Appointment
            {
                StartEnd = _appointment.StartEnd,
                Recurrence = new MonthlyRecurrence { DayRecurrence = DayRecurrence.Tuesdays, EveryXthMonth = 1, XthDay = 3 },
            };

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 6)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 13)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 20)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 27)));
        }

        [Fact]
        public void When_it_occurs_every_third_tuesday_every_second_month_then_its_valid_every_thid_tuesday_every_second_month()
        {
            var appointment = new Appointment
            {
                StartEnd = _appointment.StartEnd,
                Recurrence = new MonthlyRecurrence { DayRecurrence = DayRecurrence.Tuesdays, EveryXthMonth = 2, XthDay = 3 },
            };

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 20)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 17)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 17)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 4, 21)));
        }

        [Fact]
        public void When_it_occurs_every_last_workday_then_its_valid_only_once_per_month()
        {
            var appointment = new Appointment
            {
                StartEnd = _appointment.StartEnd,
                Recurrence = new MonthlyRecurrence { DayRecurrence = DayRecurrence.Workdays, EveryXthMonth = 1, XthDay = 5 },
            };

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 29)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 30)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 31)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 26)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 27)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 28)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 29)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 30)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 31)));
        }

        [Fact]
        public void When_it_occurs_on_the_second_workday_then_its_valid_only_on_the_second_workday()
        {
            var appointment = new Appointment
            {
                StartEnd = _appointment.StartEnd,
                Recurrence = new MonthlyRecurrence { DayRecurrence = DayRecurrence.Workdays, XthDay = 2, EveryXthMonth = 1 },
            };

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 3))); // saturday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 4))); // sunday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 10))); // saturday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 11))); // sunday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 17))); // saturday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 18))); // sunday

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 1))); // thursday
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 2))); // friday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 5))); // monday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 6))); // tuesday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 7))); // wednesday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 8))); // thursday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 9))); // friday

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 1))); // sunday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 2))); // monday
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 3))); // tuesday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 4))); // wednesday
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 5))); // thursday
        }
    }

    public class Given_an_appointment_with_a_yearly_recurrence
    {
        private static readonly Appointment _appointment = new Appointment
        {
            StartEnd = new DateTimeRange(new DateTime(2015, 1, 1), new DateTime(9999, 12, 1)),
        };

        [Fact]
        public void When_it_occurs_every_first_of_march_then_its_valid_then()
        {
            var appointment = new Appointment
            {
                StartEnd = _appointment.StartEnd,
                Recurrence = new YearlyRecurrence { EveryXthDay = 1, Month = 3, IsSpecificDay = true }
            };

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 1)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 4, 1)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2016, 3, 1)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2017, 3, 1)));
        }

        [Fact]
        public void When_it_occurs_every_last_weekday_in_march_then_its_valid_then()
        {
            var appointment = new Appointment
            {
                StartEnd = _appointment.StartEnd,
                Recurrence = new YearlyRecurrence { EveryXthDay = 5, Month = 3, DayRecurrence = DayRecurrence.Workdays }
            };

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 29)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 30)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 31)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2016, 3, 31)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2017, 3, 31)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2018, 3, 30)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2019, 3, 29)));
        }

        [Fact]
        public void When_it_occurs_every_first_of_march_every_second_year_then_its_valid_every_second_year()
        {
            var appointment = new Appointment
            {
                StartEnd = _appointment.StartEnd,
                Recurrence = new YearlyRecurrence { EveryXthDay = 1, Month = 3, IsSpecificDay = true, EveryXthYear = 2 }
            };

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 4, 1)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2016, 3, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2017, 3, 1)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2018, 3, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2019, 3, 1)));
        }

        [Fact]
        public void When_it_occurs_every_first_of_march_every_third_year_then_its_valid_every_third_year()
        {
            var appointment = new Appointment
            {
                StartEnd = _appointment.StartEnd,
                Recurrence = new YearlyRecurrence { EveryXthDay = 1, Month = 3, IsSpecificDay = true, EveryXthYear = 3 }
            };

            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 1, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 2, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 3, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2015, 4, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2016, 3, 1)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2017, 3, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2018, 3, 1)));
            Assert.False(appointment.Recurrence.IsMatch(appointment, new DateTime(2019, 3, 1)));
            Assert.True(appointment.Recurrence.IsMatch(appointment, new DateTime(2020, 3, 1)));
        }
    }
}
