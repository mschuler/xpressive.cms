namespace Cms.Aggregates.Pages
{
    public class PageContentMarkdownValue : PageContentValueBase
    {
        public string Value { get; set; }
    }
}