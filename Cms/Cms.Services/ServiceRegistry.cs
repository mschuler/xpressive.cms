﻿using Autofac;
using Cms.EventSourcing.Contracts.Commands;
using Cms.Services._2FA;
using Cms.Services.Contracts;
using Cms.Services.EventTranslators;
using Cms.Services.EventTranslators.Calendars;
using Cms.Services.EventTranslators.DynamicObjects;
using Cms.Services.EventTranslators.FileDirectories;
using Cms.Services.EventTranslators.Files;
using Cms.Services.EventTranslators.FileStores;
using Cms.Services.EventTranslators.Forms;
using Cms.Services.EventTranslators.Pages;
using Cms.Services.EventTranslators.PageTemplates;
using Cms.Services.EventTranslators.Security;
using Cms.Services.EventTranslators.System;
using Cms.Services.EventTranslators.SystemMessages;
using Cms.Services.EventTranslators.Tenants;
using Cms.Services.EventTranslators.Websites;
using Cms.Services.Files;
using Cms.Services.FormActions;
using Cms.Services.FormGenerator;
using Cms.Services.ObjectFieldContentTypeConverter;
using Cms.Services.PageContentHandlers;
using Cms.Services.RecurrentJobs;
using Cms.Services.TemplateEngineInterpreters;
using Cms.Services.Tracking;
using Cms.Services.UrlShortening;
using Cms.SharedKernel;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services
{
    public class ServiceRegistry : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UniqueIdProvider>().As<IUniqueIdProvider>();
            builder.RegisterType<ContentTypeService>().As<IContentTypeService>();
            builder.RegisterType<Utf8Checker>().As<IUtf8Checker>();
            builder.RegisterType<ApiKeyService>().As<IApiKeyService>();
            builder.RegisterType<EncryptionService>().As<IEncryptionService>();
            builder.RegisterType<LocalizationService>().As<ILocalizationService>();

            builder.RegisterType<TotpService>().As<ITotpService>();
            builder.RegisterType<U2FService>().As<IU2FService>();

            builder.RegisterType<FormActionHandler>().As<ICommandHandler>();

            builder.RegisterType<PageTrackingService>().As<IPageTrackingService>();
            builder.RegisterType<FileTrackingService>().As<IFileTrackingService>();
            builder.RegisterType<IpToCountryService>().As<IIpToCountryService>();

            builder.RegisterType<TemporaryFileStoreService>().As<ITemporaryFileStoreService>();
            builder.RegisterType<VirusTotalFileValidator>().As<ICommandHandler>();
            builder.RegisterType<VirusTotalService>().As<IVirusTotalService>();

            builder.RegisterType<FormGeneratorFactory>().As<IFormGeneratorFactory>();

            builder.RegisterType<PageRenderingService>().As<IPageRenderingService>();

            builder.RegisterType<UrlShorteningServiceProvider>().As<IUrlShorteningServiceProvider>();
            builder.RegisterType<BitlyShorteningService>().As<IUrlShorteningService>();
            builder.RegisterType<OwlyShorteningService>().As<IUrlShorteningService>();
            builder.RegisterType<GooGlShorteningService>().As<IUrlShorteningService>();
            builder.RegisterType<TinyCcShorteningService>().As<IUrlShorteningService>();
            builder.RegisterType<UrlShorteningRepository>().As<IUrlShorteningRepository>();

            builder.RegisterType<StopForumSpamService>().As<ISpamService>();

            RegisterRecurrentJobs(builder);

            RegisterObjectFieldContentTypeConverters(builder);

            RegisterPageContentHandler(builder);

            RegisterEventTranslators(builder);

            RegisterTemplateEngineInterpreters(builder);

            base.Load(builder);
        }

        private void RegisterRecurrentJobs(ContainerBuilder builder)
        {
            builder.RegisterType<RecurrentJobExecutor>().As<IRecurrentJobExecutor>();
            builder.RegisterType<PageTrackingMonthlyGroupingJob>().As<IRecurrentJob>();
            builder.RegisterType<PageTrackingDailyGroupingJob>().As<IRecurrentJob>();
            builder.RegisterType<FileTrackingMonthlyGroupingJob>().As<IRecurrentJob>();
            builder.RegisterType<FileTrackingDailyGroupingJob>().As<IRecurrentJob>();
            builder.RegisterType<MoveLogfilesToZipfile>().As<IRecurrentJob>();
        }

        private void RegisterTemplateEngineInterpreters(ContainerBuilder builder)
        {
            builder.RegisterType<YouTubeInterpreter>().As<IInterpreter>();
            builder.RegisterType<FileInterpreter>().As<IInterpreter>();
            builder.RegisterType<WebsiteInterpreter>().As<IInterpreter>();
            builder.RegisterType<PageInterpreter>().As<IInterpreter>();
            builder.RegisterType<FormInterpreter>().As<IInterpreter>();
            builder.RegisterType<RequestParameterInterpreter>().As<IInterpreter>();
            builder.RegisterType<VisitorInterpreter>().As<IInterpreter>();
            builder.RegisterType<ApiKeyInterpreter>().As<IInterpreter>();
            builder.RegisterType<PageListInterpreter>().As<IInterpreter>();
            builder.RegisterType<ObjectListInterpreter>().As<IInterpreter>();
            builder.RegisterType<ObjectInterpreter>().As<IInterpreter>();
            builder.RegisterType<TenantInterpreter>().As<IInterpreter>();
            builder.RegisterType<PageTemplateInterpreter>().As<IInterpreter>();
            builder.RegisterType<FileTypeInterpreter>().As<IInterpreter>();
            builder.RegisterType<StringTypeInterpreter>().As<IInterpreter>();
            builder.RegisterType<EmailInterpreter>().As<IInterpreter>();
        }

        private static void RegisterObjectFieldContentTypeConverters(ContainerBuilder builder)
        {
            builder.RegisterType<ObjectFieldContentTypeConverterService>().As<IObjectFieldContentTypeConverterService>();
            builder.RegisterType<ObjectFieldBooleanConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldDateConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldFileConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldHtmlConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldCodeConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldMarkdownConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldNumberConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldObjectConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldPageConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldTextConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldListConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldPageLinkConverter>().As<IObjectFieldContentTypeConverter>();
            builder.RegisterType<ObjectFieldFormConverter>().As<IObjectFieldContentTypeConverter>();
        }

        private static void RegisterPageContentHandler(ContainerBuilder builder)
        {
            builder.RegisterType<ContentHandlerFactory>().As<IContentHandlerFactory>();
            builder.RegisterType<ContentMarkdownHandler>().As<IContentHandler>();
            builder.RegisterType<ContentObjectHandler>().As<IContentHandler>();
            builder.RegisterType<ContentImageHandler>().As<IContentHandler>();
            builder.RegisterType<ContentHtmlHandler>().As<IContentHandler>();
            builder.RegisterType<ContentCodeHandler>().As<IContentHandler>();
            builder.RegisterType<ContentFormHandler>().As<IContentHandler>();
            builder.RegisterType<ContentTextHandler>().As<IContentHandler>();
            builder.RegisterType<ContentBooleanHandler>().As<IContentHandler>();
        }

        private static void RegisterEventTranslators(ContainerBuilder builder)
        {
            builder.RegisterType<EventTranslationService>().As<IEventTranslationService>();
            builder.RegisterType<TenantCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<TenantDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<TenantNameChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<SystemMessageCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<SystemMessageChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<SystemMessageDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<EmailSettingsChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<UrlShorteningSettingsChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FileStoreCreatedEventTranslator>().As<IEventTranslator>();

            RegisterSecurityEventTranslators(builder);
            RegisterFileEventTranslators(builder);
            RegisterWebsiteEventTranslators(builder);
            RegisterPageEventTranslators(builder);
            RegisterPageTemplateTranslators(builder);
            RegisterDynamicObjectEventTranslators(builder);
            RegisterCalendarEventTranslators(builder);
            RegisterFormEventTranslators(builder);
        }

        private static void RegisterSecurityEventTranslators(ContainerBuilder builder)
        {
            builder.RegisterType<UserCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<UserDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<UserNameChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PasswordChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<GroupCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<GroupDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<GroupNameChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<GroupMemberAddedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<GroupMemberRemovedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PermissionChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<UserEmailAddressChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<UserLockedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<UserUnlockedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<UserSysadminChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<Totp2FaDisabledEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<Totp2FaEnabledEventTranslator>().As<IEventTranslator>();
        }

        private static void RegisterFileEventTranslators(ContainerBuilder builder)
        {
            builder.RegisterType<FileCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FileDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FileNameChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FileTrackingEnabledEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FileTrackingDisabledEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FileVersionCreatedEventTranslator>().As<IEventTranslator>();

            builder.RegisterType<FileDirectoryChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FileDirectoryCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FileDirectoryDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FileDirectoryNameChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FileDirectoryParentChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FileDirectorySharedEventTranslator>().As<IEventTranslator>();
        }

        private static void RegisterWebsiteEventTranslators(ContainerBuilder builder)
        {
            builder.RegisterType<WebsiteCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<WebsiteDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<WebsiteNameChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DomainAddedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DefaultDomainChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DomainHostChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DefaultPageSetEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DomainRemovedEventTranslator>().As<IEventTranslator>();
        }

        private static void RegisterPageEventTranslators(ContainerBuilder builder)
        {
            builder.RegisterType<PageCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageEnabledEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageDisabledEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageStarredEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageUnstarredEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageTemplateChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageTitleChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageNameChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageContentChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageLinkCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageLinkChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageMovedEventTranslator>().As<IEventTranslator>();
        }

        private static void RegisterPageTemplateTranslators(ContainerBuilder builder)
        {
            builder.RegisterType<PageTemplateContentTypeChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageTemplateNameChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageTemplateIconChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageTemplateCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageTemplateTemplateChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageTemplateDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<PageTemplateSharedEventTranslator>().As<IEventTranslator>();
        }

        private static void RegisterDynamicObjectEventTranslators(ContainerBuilder builder)
        {
            builder.RegisterType<DynamicObjectDefinitionCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionFieldAddedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionFieldRemovedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionFieldMovedDownEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionFieldMovedUpEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionNameChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionStarredEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionUnstarredEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionFieldRenamedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionFieldContentTypeOptionsChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionFieldMarkedSortableEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDefinitionSharedEventTranslator>().As<IEventTranslator>();

            builder.RegisterType<DynamicObjectTemplateChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectTemplateCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectTemplateDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectTemplateRenamedEventTranslator>().As<IEventTranslator>();

            builder.RegisterType<DynamicObjectCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<DynamicObjectDeletedEventTranslator>().As<IEventTranslator>();
        }

        private static void RegisterCalendarEventTranslators(ContainerBuilder builder)
        {
            builder.RegisterType<AppointmentAddedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<AppointmentChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<AppointmentRemovedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<CategoryAddedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<CategoryNameChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<CategoryRemovedEventTranslator>().As<IEventTranslator>();
        }

        private static void RegisterFormEventTranslators(ContainerBuilder builder)
        {
            builder.RegisterType<NewFormCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormNameChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormFieldRemovedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormFieldChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormFieldAddedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormEntryCreatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormTemplateChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormActionAddedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormActionChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormActionRemovedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormEntryDeletedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormEntryChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormGeneratorFieldsChangedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormDuplicatedEventTranslator>().As<IEventTranslator>();
            builder.RegisterType<FormEntriesClearedEventTranslator>().As<IEventTranslator>();
        }
    }
}