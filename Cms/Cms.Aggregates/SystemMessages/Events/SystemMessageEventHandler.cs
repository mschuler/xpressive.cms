﻿namespace Cms.Aggregates.SystemMessages.Events
{
    internal class SystemMessageEventHandler : EventHandlerBase
    {
        private readonly ISystemMessageRepository _repository;

        public SystemMessageEventHandler(ISystemMessageRepository repository)
        {
            _repository = repository;
        }

        public void When(SystemMessageCreated @event)
        {
            var message = new SystemMessage
            {
                Id = @event.AggregateId,
                UserId = @event.UserId,
                Title = @event.Title,
                Message = @event.Message,
                Validity = @event.Validity,
            };

            _repository.Add(message);
        }

        public void When(SystemMessageChanged @event)
        {
            var message = _repository.Get(@event.AggregateId);
            message.UserId = @event.UserId;
            message.Title = @event.Title;
            message.Message = @event.Message;
            message.Validity = @event.Validity;
        }

        public void When(SystemMessageDeleted @event)
        {
            _repository.Delete(@event.AggregateId);
        }
    }
}