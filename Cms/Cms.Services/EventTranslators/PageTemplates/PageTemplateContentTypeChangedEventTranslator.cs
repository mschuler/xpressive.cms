using System;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.PageTemplates.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.PageTemplates
{
    internal class PageTemplateContentTypeChangedEventTranslator : IEventTranslator
    {
        private readonly IPageTemplateRepository _repository;

        public PageTemplateContentTypeChangedEventTranslator(IPageTemplateRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageTemplateContentTypeChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seitenvorlage ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageTemplateContentTypeChanged)@event;
            var template = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Der Inhaltstyp der Seitenvorlage <strong>{0}</strong> wurde nach <strong>{1}</strong> ge�ndert.", template.Name, e.ContentType);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "pagetemplate";
        }
    }
}