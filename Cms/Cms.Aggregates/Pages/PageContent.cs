﻿namespace Cms.Aggregates.Pages
{
    public class PageContent
    {
        public string Name { get; set; }
        public PageContentValueBase Value { get; set; }
    }
}
