using System.Linq;

namespace Cms.Aggregates.DynamicObjects.Events
{
    internal class DynamicObjectTemplateEventHandler : EventHandlerBase
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectTemplateEventHandler(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public void When(DynamicObjectTemplateCreated @event)
        {
            var definition = _repository.Get(@event.AggregateId);

            var template = new DynamicObjectTemplate
            {
                Id = @event.TemplateId,
                PublicId = @event.PublicId,
                Name = @event.Name,
                Template = string.Empty
            };

            definition.Add(template);
        }

        public void When(DynamicObjectTemplateChanged @event)
        {
            var definition = _repository.Get(@event.AggregateId);
            var template = definition.Templates.FirstOrDefault(t => t.Id.Equals(@event.TemplateId));

            if (template != null)
            {
                template.Template = @event.Template;
            }
        }

        public void When(DynamicObjectTemplateRenamed @event)
        {
            var definition = _repository.Get(@event.AggregateId);
            var template = definition.Templates.FirstOrDefault(t => t.Id.Equals(@event.TemplateId));

            if (template != null)
            {
                template.Name = @event.Name;
            }
        }

        public void When(DynamicObjectTemplateDeleted @event)
        {
            var definition = _repository.Get(@event.AggregateId);
            var template = definition.Templates.FirstOrDefault(t => t.Id.Equals(@event.TemplateId));

            if (template != null)
            {
                definition.Remove(template);
            }
        }
    }
}