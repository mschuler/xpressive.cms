using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Tenants.Commands
{
    [RequiredPermission("tenant", PermissionLevel.CanChange)]
    public class ChangeTenantName : CommandBase
    {
        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
    }
}