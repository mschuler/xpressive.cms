﻿namespace Cms.Aggregates.Forms
{
    public class PersistFormAction : FormActionConfiguration
    {
        public override FormActionConfiguration Clone()
        {
            return new PersistFormAction();
        }
    }
}