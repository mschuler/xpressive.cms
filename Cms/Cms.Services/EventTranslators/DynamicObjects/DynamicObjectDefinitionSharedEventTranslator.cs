using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.Aggregates.Tenants;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectDefinitionSharedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;
        private readonly ITenantRepository _tenantRepository;

        public DynamicObjectDefinitionSharedEventTranslator(IDynamicObjectDefinitionRepository definitionRepository, ITenantRepository tenantRepository)
        {
            _definitionRepository = definitionRepository;
            _tenantRepository = tenantRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return typeof(DynamicObjectDefinitionShared) == eventType;
        }

        public string GetHeader(IEvent @event)
        {
            var e = (DynamicObjectDefinitionShared)@event;
            return e.TenantIds.Any() ? "Objektdefinition freigegeben" : "Objektdefinition Freigabe aufgehoben";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectDefinitionShared)@event;
            var definition = _definitionRepository.GetEvenIfDeleted(@event.AggregateId);
            var tenants = new List<string>();

            foreach (var tenantId in e.TenantIds)
            {
                Tenant tenant;
                if (_tenantRepository.TryGetEvenIfDeleted(tenantId, out tenant))
                {
                    tenants.Add(tenant.Name);
                }
            }

            var tenantList = string.Join(", ", tenants);

            if (e.TenantIds.Any())
            {
                return string.Format("Die Objektdefinition <strong>{0}</strong> wurde f�r die Mandanten <strong>{1}</strong> freigegeben.", definition.Name, tenantList);
            }

            return string.Format("Die Freigabe f�r die Objektdefinition <strong>{0}</strong> wurde aufgehoben.", definition.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _definitionRepository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objectdefinition";
        }
    }
}