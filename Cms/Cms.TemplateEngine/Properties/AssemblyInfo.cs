﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Cms.TemplateEngine")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7f845fdb-84f2-4328-8e44-d0f158c6af3a")]

[assembly: InternalsVisibleTo("Cms.TemplateEngine.Tests")]
