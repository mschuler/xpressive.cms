using System;
using System.Linq;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Commands;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Websites;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using Cms.WebNew.Authentication;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class FormEntryController : ApiController
    {
        private readonly ICommandQueue _commandQueue;
        private readonly IFormRepository _repository;
        private readonly IUniqueIdProvider _idProvider;
        private readonly IPageUrlService _pageUrlService;
        private readonly IPageRepository _pageRepository;
        private readonly IWebsiteRepository _websiteRepository;
        private readonly ISpamService _spamService;

        public FormEntryController(ICommandQueue commandQueue, IFormRepository repository, IUniqueIdProvider idProvider, IPageUrlService pageUrlService, IPageRepository pageRepository, IWebsiteRepository websiteRepository, ISpamService spamService)
        {
            _commandQueue = commandQueue;
            _repository = repository;
            _idProvider = idProvider;
            _pageUrlService = pageUrlService;
            _pageRepository = pageRepository;
            _websiteRepository = websiteRepository;
            _spamService = spamService;
        }

        [HttpPost, AllowAnonymous, Route("api/form/{formId:ulong}/entry")]
        public async Task<IHttpActionResult> Create(ulong formId, FormDataCollection formData)
        {
            var form = _repository.Get(formId);
            var userId = User.GetId(false);

            var pairs = formData.ReadAsNameValueCollection();
            var values = pairs.AllKeys.ToDictionary(k => k, k => pairs[k], StringComparer.OrdinalIgnoreCase);

            foreach (var formField in form.Fields)
            {
                if (!values.ContainsKey(formField.Name))
                {
                    values.Add(formField.Name, string.Empty);
                }
            }

            var ipAddress = Request.GetClientIpAddress();
            var userAgent = Request.Headers?.UserAgent?.ToString();
            var isSpam = await _spamService.IsSpam(ipAddress, userAgent);

            await Task.Run(() => _commandQueue.Put(new CreateFormEntry
            {
                AggregateId = formId,
                TenantId = form.TenantId,
                FormEntryId = _idProvider.GetId(),
                IpAddress = ipAddress,
                UserId = userId == 0 ? Aggregates.Security.User.Guest.Id : userId,
                Values = values,
                RunFormActions = true,
                IsSpam = isSpam,
            }, Request));

            var redirectUri = GetRedirectUri(form);
            return Redirect(redirectUri);
        }

        private Uri GetRedirectUri(Form form)
        {
            var forwarding = form.Actions.SingleOrDefault(a => a.Type == FormActionType.PageForwarding);
            var uri = Request.Headers.Referrer;

            if (forwarding == null)
            {
                return uri;
            }

            var forwardingConfiguration = (PageForwardingFormAction)forwarding.Configuration;
            var pageId = Convert.ToUInt64(forwardingConfiguration.Id);

            PageBase page;
            Website website;

            if (!_pageRepository.TryGet(pageId, out page) ||
                !_websiteRepository.TryGet(page.WebsiteId, out website) ||
                website.DefaultDomain == null)
            {
                return uri;
            }

            var relativeUrl = _pageUrlService.GetUrl(pageId);
            var url = string.Format("{0}://{1}{2}", Request.RequestUri.Scheme, website.DefaultDomain.Host, relativeUrl);
            return new Uri(url, UriKind.Absolute);
        }
    }
}