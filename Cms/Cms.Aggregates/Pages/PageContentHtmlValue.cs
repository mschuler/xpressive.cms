﻿namespace Cms.Aggregates.Pages
{
    public class PageContentHtmlValue : PageContentValueBase
    {
        public string Value { get; set; }
    }
}