﻿using System;
using Cms.Aggregates.SystemMessages.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.SystemMessages
{
    internal class SystemMessageChangedEventTranslator : IEventTranslator
    {
        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(SystemMessageChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Systemnachricht geändert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (SystemMessageChanged)@event;
            return string.Format("Die Systemnachricht <strong>{0}</strong> wurde geändert.", e.Title);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}