﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.PageTemplates.Queries
{
    [RequiredPermission("pagetemplate", PermissionLevel.CanSee)]
    public class GetAllPageTemplates : QueryBase { }
}