﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDefinitionCreated : EventBase<DynamicObjectDefinition>
    {
        public ulong TenantId { get; set; }
        public int PublicId { get; set; }
        public string Name { get; set; }
    }
}
