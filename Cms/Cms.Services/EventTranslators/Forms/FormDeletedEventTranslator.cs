﻿using System;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Forms
{
    internal class FormDeletedEventTranslator : IEventTranslator
    {
        private readonly IFormRepository _repository;

        public FormDeletedEventTranslator(IFormRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FormDeleted);
        }

        public string GetHeader(IEvent @event)
        {
            return "Formular gelöscht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FormDeleted)@event;
            var form = _repository.GetEvenIfDeleted(e.AggregateId);
            var name = string.Format("{0} (Nr. {1})", form.Name, form.PublicId);
            return string.Format("Das Formular <strong>{0}</strong> wurde gelöscht.", name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "form";
        }
    }
}