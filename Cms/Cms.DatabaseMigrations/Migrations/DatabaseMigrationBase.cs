using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal abstract class DatabaseMigrationBase
    {
        private readonly int _id;

        protected DatabaseMigrationBase(int id)
        {
            _id = id;
        }

        public int Id { get { return _id; } }

        public abstract void Execute(Database database);
    }
}