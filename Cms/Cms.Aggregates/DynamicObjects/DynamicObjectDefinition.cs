﻿using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.DynamicObjects
{
    public class DynamicObjectDefinition : AggregateBase
    {
        private readonly object _templatesLock = new object();
        private readonly List<DynamicObjectTemplate> _templates;
        private readonly object _fieldsLock = new object();
        private readonly List<DynamicObjectDefinitionField> _fields;
        private readonly object _sharedForTenantsLock = new object();
        private readonly List<ulong> _sharedForTenants;

        public DynamicObjectDefinition()
        {
            _templates = new List<DynamicObjectTemplate>();
            _fields = new List<DynamicObjectDefinitionField>();
            _sharedForTenants = new List<ulong>();
        }

        public ulong TenantId { get; set; }
        public int PublicId { get; set; }
        public string Name { get; set; }

        public IEnumerable<DynamicObjectTemplate> Templates
        {
            get
            {
                lock (_templatesLock)
                {
                    return _templates.ToList();
                }
            }
        }

        public IEnumerable<DynamicObjectDefinitionField> Fields
        {
            get
            {
                lock (_fieldsLock)
                {
                    return _fields.OrderBy(f => f.SortOrder).ToList();
                }
            }
        }

        public IEnumerable<ulong> SharedForTenants
        {
            get
            {
                lock (_sharedForTenantsLock)
                {
                    return _sharedForTenants.ToList();
                }
            }
        }

        public void Add(DynamicObjectTemplate template)
        {
            lock (_templatesLock)
            {
                _templates.Add(template);
            }
        }

        public void Remove(DynamicObjectTemplate template)
        {
            lock (_templatesLock)
            {
                _templates.Remove(template);
            }
        }

        public void Add(DynamicObjectDefinitionField field)
        {
            lock (_fieldsLock)
            {
                _fields.Add(field);
            }
        }

        public void Remove(DynamicObjectDefinitionField field)
        {
            lock (_fieldsLock)
            {
                _fields.Remove(field);
            }
        }

        public void Share(IEnumerable<ulong> tenantIds)
        {
            lock (_sharedForTenantsLock)
            {
                _sharedForTenants.Clear();
                _sharedForTenants.AddRange(tenantIds);
            }
        }
    }
}
