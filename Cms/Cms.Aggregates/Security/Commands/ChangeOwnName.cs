using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission(false)]
    public class ChangeOwnName : CommandBase
    {
        [Required(AllowEmptyStrings = true)]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string LastName { get; set; }
    }
}