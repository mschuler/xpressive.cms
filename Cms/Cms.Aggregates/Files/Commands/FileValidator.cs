using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.Files.Commands
{
    internal static class FileValidator
    {
        public static string ValidateFile(string temporaryFilePath, string name)
        {
            ValidateContentType(temporaryFilePath, name);
            temporaryFilePath = ValidateImage(temporaryFilePath, name);
            return temporaryFilePath;
        }

        private static void ValidateContentType(string temporaryFilePath, string name)
        {
            if (!name.IsImage())
            {
                return;
            }

            // http://www.filesignatures.net/
            var validImageHeaderBytes = new List<byte[]>
            {
                new byte[] { 0xFF, 0xD8, 0xFF }, // JPG
                new byte[] { 0x89, 0x50, 0x4E, 0x47 }, // PNG
                new byte[] { 0x42, 0x4D }, // BMP
                new byte[] { 0x47, 0x49, 0x46, 0x38 }, // GIF
            };

            var fileData = new byte[8];

            using (var fs = new FileStream(temporaryFilePath, FileMode.Open, FileAccess.Read, FileShare.Read, 8))
            {
                fs.Read(fileData, 0, fileData.Length);
            }

            foreach (var headerBytes in validImageHeaderBytes)
            {
                if (headerBytes.SequenceEqual(fileData.Take(headerBytes.Length)))
                {
                    return;
                }
            }

            throw new ExceptionWithReason("It seems that this file is not a valid image").WithGuid();
        }

        private static string ValidateImage(string temporaryFilePath, string name)
        {
            if (!name.IsJpg())
            {
                return temporaryFilePath;
            }

            var newTemporaryFilePath = Path.GetTempFileName();

            using (var input = System.IO.File.OpenRead(temporaryFilePath))
            {
                using (var output = System.IO.File.OpenWrite(newTemporaryFilePath))
                {
                    JpegPatcher.PatchAwayExif(input, output);
                }
            }

            System.IO.File.Delete(temporaryFilePath);
            return newTemporaryFilePath;
        }
    }
}