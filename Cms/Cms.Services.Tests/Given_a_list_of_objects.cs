using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.Pages;
using Cms.Services.ObjectFieldContentTypeConverter;
using Cms.Services.TemplateEngineInterpreters;
using Cms.SharedKernel;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Interpreter;
using Moq;
using Xunit;

namespace Cms.Services.Tests
{
    public class Given_a_list_of_objects
    {
        private List<DynamicObject> _objects = new List<DynamicObject>();

        public Given_a_list_of_objects()
        {
            var objectRepositoryMock = new Mock<IDynamicObjectRepository>();
            var definitionRepositoryMock = new Mock<IDynamicObjectDefinitionRepository>();
            var pageRepositoryMock = new Mock<IPageRepository>();
            var pages = new List<PageBase>();
            var definitions = new List<DynamicObjectDefinition>();
            var converters = new List<IObjectFieldContentTypeConverter>();
            PageBase page1 = new Page { Id = 1, PublicId = 1, Title = "Abc" };
            PageBase page2 = new Page { Id = 2, PublicId = 2, Title = "A" };
            PageBase page3 = new Page { Id = 3, PublicId = 3, Title = "a" };
            PageBase page4 = new Page { Id = 4, PublicId = 4, Title = "bcd" };
            PageBase page5 = new Page { Id = 5, PublicId = 5, Title = "Cde" };
            pages.Add(page1);
            pages.Add(page2);
            pages.Add(page3);
            pages.Add(page4);
            pages.Add(page5);
            pageRepositoryMock.Setup(s => s.GetAll()).Returns(() => pages);
            pageRepositoryMock.Setup(s => s.TryGet(1, out page1)).Returns(true);
            pageRepositoryMock.Setup(s => s.TryGet(2, out page2)).Returns(true);
            pageRepositoryMock.Setup(s => s.TryGet(3, out page3)).Returns(true);
            pageRepositoryMock.Setup(s => s.TryGet(4, out page4)).Returns(true);
            pageRepositoryMock.Setup(s => s.TryGet(5, out page5)).Returns(true);
            var pageRepository = pageRepositoryMock.Object;
            definitions.Add(new DynamicObjectDefinition { PublicId = 1, Id = 1234567 });
            converters.Add(new ObjectFieldPageConverter(pageRepository, null, null));
            converters.Add(new ObjectFieldTextConverter());
            _objects.Add(Create(1, "object 1", 1, "Johannes Wirth"));
            _objects.Add(Create(2, "object 2", 2, "Reto Lussi"));
            _objects.Add(Create(3, "object 3", 3, "Dani Weber"));
            objectRepositoryMock.Setup(o => o.GetAll(0, 1234567)).Returns(() => _objects);
            definitionRepositoryMock.Setup(s => s.GetAll()).Returns(() => definitions);
            definitionRepositoryMock.Setup(m => m.Get(1234567)).Returns(definitions.Single);
            var objectRepository = objectRepositoryMock.Object;
            var definitionRepository = definitionRepositoryMock.Object;
            var contentTypeConverterService = new ObjectFieldContentTypeConverterService(converters);

            definitions[0].Add(new DynamicObjectTemplate
            {
                Id = 4,
                PublicId = 4,
                Name = "template",
                Template = "<ul>{objects(1).template('<li>{$(\"title\")}</li>')}</ul>",
            });
            definitions[0].Add(new DynamicObjectTemplate
            {
                Id = 3,
                PublicId = 3,
                Name = "template",
                Template = "{objects(1).template('<time>{$(\"date\").format(\"dd.MM.yy\")}</time><h1>{$(\"title\")}</h1><p>Mit {$(\"Pastor\")}</p>')}",
            });
            definitions[0].Add(new DynamicObjectTemplate
            {
                Id = 2,
                PublicId = 2,
                Name = "template",
                Template = "{$('title')}",
            });

            InterpreterFactory.Instance.Clear();
            InterpreterFactory.Instance.Register(new DateTimeInterpreter());
            InterpreterFactory.Instance.Register(new NumberFormatInterpreter());
            InterpreterFactory.Instance.Register(new WhereListInterpreter());
            InterpreterFactory.Instance.Register(new SortListInterpreter());
            InterpreterFactory.Instance.Register(new ObjectListInterpreter(objectRepository, definitionRepository));
            InterpreterFactory.Instance.Register(new PageInterpreter(null, null, null, null, null, null, null, null, null));
            InterpreterFactory.Instance.Register(new ObjectInterpreter(objectRepository, contentTypeConverterService));
        }

        private DynamicObject Create(int publicId, string title, int pageId, string pastor)
        {
            var o = new DynamicObject();
            o.Add(new DynamicObjectField { ContentType = "text", Name = "title", Value = title });
            o.Add(new DynamicObjectField { ContentType = "page", Name = "page", Value = pageId.ToString() });
            o.Add(new DynamicObjectField { ContentType = "text", Name = "pastor", Value = pastor });
            o.PublicId = publicId;
            o.DefinitionId = 1234567;
            o.CreationDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, publicId % 24, 0, 0);
            return o;
        }

        [Fact]
        public void When_sorting_by_date_desc_with_format_string()
        {
            var treeItem = Parse("{objects(1).sortDesc('date.format(\"yyyyMMdd HH:mm\")').template(2)}");
            var result = Calculate<string>(treeItem);

            Assert.NotNull(result);
            Assert.Equal("object 3object 2object 1", result);
        }

        [Fact]
        public void When_sorting_by_date_asc_with_format_string()
        {
            var treeItem = Parse("{objects(1).sortAsc('date.format(\"yyyyMMdd HH:mm\")').template(2)}");
            var result = Calculate<string>(treeItem);

            Assert.NotNull(result);
            Assert.Equal("object 1object 2object 3", result);
        }

        [Fact]
        public void When_render_list_in_list()
        {
            var treeItem = Parse("{objects(1).template(4)}");
            var result = Calculate<string>(treeItem);

            Assert.NotNull(result);
            Assert.Equal("<ul><li>object 1</li><li>object 2</li><li>object 3</li></ul><ul><li>object 1</li><li>object 2</li><li>object 3</li></ul><ul><li>object 1</li><li>object 2</li><li>object 3</li></ul>", result);
        }

        [Fact]
        public void When_render_other_list_in_list()
        {
            var treeItem = Parse("{objects(1).template(3)}");
            var result = Calculate<string>(treeItem);

            Assert.NotNull(result);

            var expected = "<time>DATE</time><h1>object 1</h1><p>Mit Johannes Wirth</p><time>DATE</time><h1>object 2</h1><p>Mit Reto Lussi</p><time>DATE</time><h1>object 3</h1><p>Mit Dani Weber</p><time>DATE</time><h1>object 1</h1><p>Mit Johannes Wirth</p><time>DATE</time><h1>object 2</h1><p>Mit Reto Lussi</p><time>DATE</time><h1>object 3</h1><p>Mit Dani Weber</p><time>DATE</time><h1>object 1</h1><p>Mit Johannes Wirth</p><time>DATE</time><h1>object 2</h1><p>Mit Reto Lussi</p><time>DATE</time><h1>object 3</h1><p>Mit Dani Weber</p>".Replace("DATE", DateTime.Today.ToString("dd.MM.yy"));
            Assert.Equal(expected, result);
        }

        [Fact]
        public void When_filtering_by_title_then_there_are_less_results()
        {
            var treeItem = Parse("objects(1).where('title', 'startswith', 'o')");
            var result = Calculate<IList<DynamicObject>>(treeItem);

            Assert.NotNull(result);
            Assert.Equal(3, result.Count);
        }

        [Fact]
        public void When_filtering_by_field_value_then_there_are_less_results()
        {
            var treeItem = Parse("objects(1).where(\"field(\\'page\\').title\", 'startswith', 'a')");
            var result = Calculate<IList<DynamicObject>>(treeItem);

            Assert.NotNull(result);
            Assert.Equal(3, result.Count);
        }

        [Fact]
        public void When_filtering_by_date_then_there_are_less_results()
        {
            _objects.ForEach(o => o.CreationDate = DateTime.Now);
            var treeItem = Parse("objects(1).where('date', '=', now())");
            var result = Calculate<IList<DynamicObject>>(treeItem);

            Assert.NotNull(result);
            Assert.Equal(3, result.Count);
        }

        [Fact]
        public void When_filtering_by_date_string_then_there_are_less_results()
        {
            _objects.ForEach(o => o.CreationDate = DateTime.Now);
            var treeItem = Parse("objects(1).where('date.torfc1123()', '=', '" + DateTime.Now.ToString("r") + "')");
            var result = Calculate<IList<DynamicObject>>(treeItem);

            Assert.NotNull(result);
            Assert.Equal(3, result.Count);
        }

        [Fact]
        public void When_filtering_with_greater_then()
        {
            _objects.ForEach(o => o.CreationDate = DateTime.Today);
            _objects.Last().CreationDate = DateTime.Today.AddDays(1);
            var treeItem = Parse("objects(1).where('date', '>', now())");
            var result = Calculate<IList<DynamicObject>>(treeItem);

            Assert.NotNull(result);
            Assert.Equal(1, result.Count);
        }

        [Fact]
        public void When_filtering_with_greater_or_equal_then()
        {
            _objects.ForEach(o => o.CreationDate = DateTime.Today);
            _objects.Last().CreationDate = DateTime.Today.AddDays(1);
            var treeItem = Parse("objects(1).where('date', '>=', now())");
            var result = Calculate<IList<DynamicObject>>(treeItem);

            Assert.NotNull(result);
            Assert.Equal(1, result.Count);
        }

        private T Calculate<T>(TreeItem treeItem) where T : class
        {
            var result = InterpreterFactory.Instance.Calculate(treeItem);
            return result as T;
        }

        private static TreeItem Parse(string expression)
        {
            return new Engine().ParseTreeItem(expression);
        }
    }
}