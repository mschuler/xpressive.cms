using System;
using System.Collections;
using System.Collections.Generic;

namespace Cms.TemplateEngine.Interfaces
{
    public static class ListExtensions
    {
        public static IList GetListOfSameType(this IEnumerable list)
        {
            var resultType = list.GetType().GetGenericArguments()[0];
            var listType = typeof(List<>);
            return (IList)Activator.CreateInstance(listType.MakeGenericType(resultType));
        }

        public static void AddRange(this IList list, IEnumerable source)
        {
            foreach (var o in source)
            {
                list.Add(o);
            }
        }
    }
}