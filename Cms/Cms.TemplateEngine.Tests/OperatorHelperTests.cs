﻿using Cms.TemplateEngine.Interpreter;
using Xunit;

namespace Cms.TemplateEngine.Tests
{
    public class Given_an_operator_helper
    {
        [Fact]
        public void When_using_in_then_different_types_are_possible()
        {
            Assert.True(OperatorHelper.IsMatch(15, "in", "1,15,22,23"));
        }
    }
}
