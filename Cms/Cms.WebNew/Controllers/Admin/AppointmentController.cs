using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Calendars;
using Cms.Aggregates.Calendars.Commands;
using Cms.Aggregates.Calendars.Queries;
using Cms.Aggregates.Links;
using Cms.Aggregates.Security;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;
using Newtonsoft.Json;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class AppointmentController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _idProvider;

        public AppointmentController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IUniqueIdProvider idProvider)
            : base(commandQueue, userRepository, queryHandler)
        {
            _idProvider = idProvider;
        }

        [HttpGet, Route("api/appointments/{tenantId:ulong}/{monthOffset:int}")]
        public IEnumerable<object> GetAppointmentOccurrences(ulong tenantId, int monthOffset)
        {
            var then = DateTime.Today.AddMonths(monthOffset);
            var start = new DateTime(then.Year, then.Month, 1);
            var end = new DateTime(then.Year, then.Month, DateTime.DaysInMonth(then.Year, then.Month));

            return ExecuteQuery(new GetAppointmentOccurrences
            {
                TenantId = tenantId,
                StartDate = start,
                EndDate = end,
            });
        }

        [HttpGet, Route("api/appointment/{tenantId:ulong}/{appointmentId:ulong}")]
        public object GetAppointment(ulong tenantId, ulong appointmentId)
        {
            var result = ExecuteQuery(new GetAppointment
            {
                TenantId = tenantId,
                AppointmentId = appointmentId,
            });
            return result.SingleOrDefault();
        }

        [HttpPost, Route("api/appointments/{tenantId:ulong}")]
        public void Create(ulong tenantId, [FromBody] AppointmentDto dto)
        {
            var recurrence = GetRecurrence(dto);
            var link = GetLink(dto);
            var startEnd = GetDateTimeRange(dto);
            var categories = dto.Categories.Select(c => c.Name).ToList();

            SendCommand(new AddAppointment
            {
                AggregateId = tenantId,
                TenantId = tenantId,
                AppointmentId = _idProvider.GetId(),
                Title = dto.Title,
                IsHighlight = dto.IsHighlight,
                Location = dto.Location,
                StartEnd = startEnd,
                Link = link,
                Recurrence = recurrence,
                Categories = categories,
            });
        }

        [HttpPost, Route("api/appointment/{tenantId:ulong}/{appointmentId:ulong}")]
        public void Update(ulong tenantId, ulong appointmentId, [FromBody] AppointmentDto dto)
        {
            var recurrence = GetRecurrence(dto);
            var link = GetLink(dto);
            var startEnd = GetDateTimeRange(dto);
            var categories = dto.Categories.Select(c => c.Name).ToList();

            SendCommand(new ChangeAppointment
            {
                AggregateId = tenantId,
                TenantId = tenantId,
                AppointmentId = appointmentId,
                Title = dto.Title,
                IsHighlight = dto.IsHighlight,
                Location = dto.Location,
                StartEnd = startEnd,
                Link = link,
                Recurrence = recurrence,
                Categories = categories,
            });
        }

        [HttpDelete, Route("api/appointment/{tenantId:ulong}/{appointmentId:ulong}")]
        public void Delete(ulong tenantId, ulong appointmentId)
        {
            SendCommand(new RemoveAppointment
            {
                TenantId = tenantId,
                AggregateId = tenantId,
                AppointmentId = appointmentId,
            });
        }

        private AppointmentRecurrence GetRecurrence(AppointmentDto dto)
        {
            var json = JsonConvert.SerializeObject(dto.RecurrenceOptions);
            if (dto.Recurrence.Equals("daily", StringComparison.OrdinalIgnoreCase))
            {
                return JsonConvert.DeserializeObject<DailyRecurrence>(json);
            }
            if (dto.Recurrence.Equals("weekly", StringComparison.OrdinalIgnoreCase))
            {
                return JsonConvert.DeserializeObject<WeeklyRecurrence>(json);
            }
            if (dto.Recurrence.Equals("monthly", StringComparison.OrdinalIgnoreCase))
            {
                return JsonConvert.DeserializeObject<MonthlyRecurrence>(json);
            }
            if (dto.Recurrence.Equals("yearly", StringComparison.OrdinalIgnoreCase))
            {
                return JsonConvert.DeserializeObject<YearlyRecurrence>(json);
            }
            return new SingleOccurrence();
        }

        private static ILink GetLink(AppointmentDto dto)
        {
            if (dto.Link == null || string.IsNullOrEmpty(dto.Link.LinkType))
            {
                return null;
            }

            if (dto.Link.LinkType.Equals("page", StringComparison.OrdinalIgnoreCase) && dto.Link.PageLink != null)
            {
                ulong pageId;
                if (ulong.TryParse(dto.Link.PageLink.Id, out pageId))
                {
                    return new PageReferenceLink { PageId = pageId };
                }
            }

            if (dto.Link.LinkType.Equals("external", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(dto.Link.ExternalLink))
            {
                return new Hyperlink
                {
                    Url = dto.Link.ExternalLink
                };
            }

            return null;
        }

        private DateTimeRange GetDateTimeRange(AppointmentDto dto)
        {
            TimeSpan startTime;
            TimeSpan endTime;

            if (!TimeSpan.TryParseExact(dto.StartTime, "g", CultureInfo.InvariantCulture, out startTime))
            {
                throw new ExceptionWithReason("Invalid start time.");
            }
            if (!TimeSpan.TryParseExact(dto.EndTime, "g", CultureInfo.InvariantCulture, out endTime))
            {
                throw new ExceptionWithReason("Invalid end time.");
            }

            var start = dto.StartDate.Date.Add(startTime);
            var end = dto.EndDate.Date.Add(endTime);

            return new DateTimeRange(start, end);
        }

        public class AppointmentDto
        {
            public string Title { get; set; }
            public bool IsHighlight { get; set; }
            public string Location { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public string Recurrence { get; set; }
            public dynamic RecurrenceOptions { get; set; }
            public Link Link { get; set; }
            public List<Category> Categories { get; set; }
        }

        public class Link
        {
            public string LinkType { get; set; }
            public PageLink PageLink { get; set; }
            public string ExternalLink { get; set; }
        }

        public class Category
        {
            public string Name { get; set; }
        }

        public class PageLink
        {
            public string Id { get; set; }
        }
    }
}