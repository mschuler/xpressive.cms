﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Cms.TemplateEngine
{
    internal class ExpressionExtractor
    {
        private static readonly Irony.Parsing.Parser _parser;

        static ExpressionExtractor()
        {
            var grammar = new ExpressionGrammar();
            _parser = new Irony.Parsing.Parser(grammar);
        }

        public List<Expression> GetAllExpressions(string templateContent)
        {
            var result = new List<Expression>();

            if (string.IsNullOrEmpty(templateContent))
            {
                return result;
            }

            return GetExpressionsFromLine(templateContent).ToList();
        }

        private IEnumerable<Expression> GetExpressionsFromLine(string line)
        {
            var startOffset = 0;

            while (true)
            {
                var start = line.IndexOf('{', startOffset);

                if (start < 0)
                {
                    yield break;
                }

                for (var endOffset = start; ; )
                {
                    var end = line.IndexOf('}', endOffset);
                    endOffset = end + 1;

                    if (end < 0)
                    {
                        yield break;
                    }

                    var expression = line.Substring(start, end - start + 1);
                    var trimmed = expression.Trim(' ', '{', '}');
                    bool isValid;

                    try
                    {
                        var parseTree = _parser.Parse(trimmed);
                        isValid = parseTree.Root != null;
                    }
                    catch (Exception e)
                    {
                        var message = string.Format("Unable to parse expression '{0}'.", trimmed);
                        throw new InvalidOperationException(message, e);
                    }

                    if (isValid)
                    {
                        yield return new Expression
                        {
                            FullName = expression,
                            Name = HtmlDecode(trimmed),
                            Position = start
                        };
                        startOffset = end;
                        break;
                    }
                }
            }
        }

        private string HtmlDecode(string expression)
        {
            var decoded = WebUtility.HtmlDecode(expression);
            return decoded;
        }
    }
}
