namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0002_create_event_table : DatabaseSqlMigration
    {
        public M0002_create_event_table() : base(2) { }

        protected override string GetSql()
        {
            return @"
create table [Event] (
    [Transaction] bigint not null,
    [Version] bigint not null,
    [AggregateType] nvarchar(64) not null,
    [AggregateId] bigint not null,
    [Event] nvarchar(64) not null,
    [UserId] bigint not null,
    [Data] nvarchar(max) not null,
    [IsDeleted] bit not null,

    constraint pk_events primary key (Version)
)
create nonclustered index ix_aggregateid on Event (AggregateId)
create nonclustered index ix_aggregatetype on Event (AggregateType)
";
        }
    }
}