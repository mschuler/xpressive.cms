﻿using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.PageTemplates
{
    public class PageTemplate : AggregateBase
    {
        private readonly object _sharedForTenantsLock = new object();
        private readonly List<ulong> _sharedForTenants = new List<ulong>();

        public ulong TenantId { get; set; }

        public string Name { get; set; }

        public int PublicId { get; set; }

        public ulong IconFileId { get; set; }

        public string ContentType { get; set; }

        public string Template { get; set; }

        public IEnumerable<ulong> SharedForTenants
        {
            get
            {
                lock (_sharedForTenantsLock)
                {
                    return _sharedForTenants.ToList();
                }
            }
        }

        public void Share(IEnumerable<ulong> tenantIds)
        {
            lock (_sharedForTenantsLock)
            {
                _sharedForTenants.Clear();
                _sharedForTenants.AddRange(tenantIds);
            }
        }
    }
}
