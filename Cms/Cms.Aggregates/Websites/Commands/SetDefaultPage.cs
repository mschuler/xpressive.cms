using Cms.Aggregates.Security;

namespace Cms.Aggregates.Websites.Commands
{
    [RequiredPermission("website", PermissionLevel.CanChange)]
    public class SetDefaultPage : CommandBase
    {
        [ValidId]
        public ulong PageId { get; set; }
    }
}