﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using TemplateEngine;
using TemplateEngine.Interpreter;

namespace Entities.Fixtures
{
	[TestFixture]
	public class FormInterpreterFixture
	{
		private Engine myEngine;

		[TestFixtureSetUp]
		public static void ClassSetUp()
		{
			InterpreterFactory.Instance.Register(new FormInterpreter());
		}

		[SetUp]
		public void SetUp()
		{
			myEngine = new Engine();
		}

		[Test]
		public void Calculate()
		{
			string result = myEngine.Generate("bla blub { form(2) } test form.", new List<Placeholder>());
			Console.WriteLine(result);
		}
	}
}
