namespace Cms.Aggregates.Pages.Events
{
    public class PageLinkChanged : EventBase<PageBase>
    {
        public ulong PageId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}