﻿(function () {
    angular.module("app").controller("PageDetailController", ["$scope", "$http", "$routeParams", "toastService", "toastr", "dialogService", "$log", "$translate", function (
        $scope, $http, $routeParams, toastService, toastr, dialogService, $log, $translate) {
        $scope.pageId = $routeParams.pageId;

        $http.get("/api/contenthandler", { cache: false }).success(function (ct) {
            $scope.contentTypes = ct;

            $http.get("/api/page/" + $scope.pageId, { cache: false }).success(function (data) {
                $scope.page = data;

                _.each(data.content, function (c) {
                    var type = _.find(ct, function (x) { return x.name.toLowerCase() == c.type.toLowerCase(); });
                    if (type) {
                        c.type = type.name;
                    }
                });
            });
        });

        var editPageTitle = "";
        var editPageName = "";
        var title = "";
        var name = "";
        var errorSaveName = "";
        var errorSaveTitle = "";
        var contentSaved = "";
        var contentNotSaved = "";

        $translate(["change_page_title", "change_page_name", "name", "title", "error_change_page_title", "error_change_page_name", "error_save_page_content", "success_save_page_content"]).then(function (t) {
            editPageTitle = t.change_page_title;
            editPageName = t.change_page_name;
            name = t.name;
            title = t.title;
            errorSaveName = t.error_change_page_name;
            errorSaveTitle = t.error_change_page_title;
            contentNotSaved = t.error_save_page_content;
            contentSaved = t.success_save_page_content;
        });

        $scope.editTitle = function () {
            dialogService.singleInputDialog(editPageTitle, title, $scope.page.title, saveTitle);
        };

        $scope.editName = function () {
            dialogService.singleInputDialog(editPageName, name, $scope.page.name, saveName);
        };

        var saveName = function (n) {
            var dto = {
                name: n
            };
            $http.put("/api/page/" + $scope.pageId + "/name", dto).success(function () {
                $scope.page.name = n;
            }).error(function (error) {
                $log.error(angular.toJson(error));
                toastService.error(errorSaveName, error);
            });
        };

        var saveTitle = function (t) {
            var dto = {
                title: t
            };
            $http.put("/api/page/" + $scope.pageId + "/title", dto).success(function () {
                $scope.page.title = t;
            }).error(function (error) {
                $log.error(angular.toJson(error));
                toastService.error(errorSaveTitle, error);
            });
        };

        $scope.saveContent = function(content) {
            var value = content.value;
            if (!value) {
                return null;
            }

            var save = {
                name: content.name,
                type: content.type,
                value: value,
            };

            return $http.put("/api/page/" + $scope.pageId + "/content", save).success(function () {
                var msg = String.format(contentSaved, save.name);
                toastr.success('', msg, { allowHtml: false, timeOut: 1000 });
                $log.info("saved page content " + save.name);
            }).error(function (error) {
                $log.error(angular.toJson(error));
                toastService.error(contentNotSaved, error);
            });
        };
    }]);
}());
