﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Cms.Aggregates.Pages;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.Websites;
using Cms.Services.Contracts;
using Cms.WebNew.Controllers.RoutingAndAttributes;
using log4net;
using Metrics;

namespace Cms.WebNew.Controllers.PageRendering
{
    [AllowAnonymous]
    public class PageRenderingController : ApiController
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(PageRenderingController));

        private static readonly HashSet<string> _notFoundList = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "favicon.ico",
        };

        private readonly Timer _timer = Metric.Timer("PageRenderingRequests", Unit.Requests);

        private readonly IWebsiteRepository _websiteRepository;
        private readonly IPageRepository _pageRepository;
        private readonly IPageTemplateRepository _pageTemplateRepository;
        private readonly IHtmlTagSearchService _htmlTagSearchService;
        private readonly IPageUrlService _pageUrlService;
        private readonly IPageTrackingService _pageTrackingService;
        private readonly IDisabledPageTokenService _disabledPageTokenService;
        private readonly IPageRenderingService _pageRenderingService;

        public PageRenderingController(IWebsiteRepository websiteRepository, IPageRepository pageRepository, IPageTemplateRepository pageTemplateRepository, IHtmlTagSearchService htmlTagSearchService, IPageUrlService pageUrlService, IPageTrackingService pageTrackingService, IDisabledPageTokenService disabledPageTokenService, IPageRenderingService pageRenderingService)
        {
            _websiteRepository = websiteRepository;
            _pageRepository = pageRepository;
            _pageTemplateRepository = pageTemplateRepository;
            _htmlTagSearchService = htmlTagSearchService;
            _pageUrlService = pageUrlService;
            _pageTrackingService = pageTrackingService;
            _disabledPageTokenService = disabledPageTokenService;
            _pageRenderingService = pageRenderingService;
        }

        [HttpGet, HttpHead]
        public async Task<HttpResponseMessage> Get([BindCatchAllRoute('/')]string[] pages)
        {
            if (Request.Method != HttpMethod.Get && Request.Method != HttpMethod.Head)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            if (pages == null)
            {
                pages = new string[0];
            }

            if (pages.Length > 0)
            {
                if (_notFoundList.Contains(pages[0]))
                {
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                }

                if (PageBase.InvalidRootPageNames.Contains(pages[0]))
                {
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
                }
            }

            return await Task.Run(() => RenderPage(pages));
        }

        private HttpResponseMessage RenderPage(string[] pages)
        {
            var pipeline = new PipeLine<RenderPageMessage>()
                .Register(new PunishHackingActivities())
                .Register(new IsKnownBot())
                .Register(new LogPageRequest())
                .Register(new ValidateWebsite(_websiteRepository))
                .Register(new ValidateDomain())
                .Register(new ValidateUrl(_pageRepository, _pageUrlService))
                .Register(new WriteTrackingCookie())
                .Register(new TrackPageView(_pageTrackingService))
                .Register(new ServeRequestFromCache())
                .Register(new ValidateIsEnabled(_disabledPageTokenService))
                .Register(new WriteTemplateStateMapVariables())
                .Register(new LoadPageTemplate(_pageTemplateRepository))
                .Register(new RenderPageContent(_pageRenderingService))
                .Register(new RunTemplateEngine())
                .Register(new WriteTitle())
                .Register(new WriteGeneratorMetatag())
                .Register(new AddNoFollowAttributeToExternalLinks(_htmlTagSearchService, _websiteRepository))
                .Register(new SaveRequestResultToCache());

            var message = new RenderPageMessage(Request);
            message.RouteParts = pages;

            bool success;
            var stopwatch = Stopwatch.StartNew();

            using (_timer.NewContext(string.Join("/", pages)))
            {
                success = pipeline.Execute(message);
            }

            stopwatch.Stop();
            _logger.DebugFormat("PageRendering: {0} - {1} ms {2}", string.Join("/", pages), stopwatch.ElapsedMilliseconds, success ? string.Empty : " (Failed with status code " + (int)message.Response.StatusCode + ")");

            if (!success)
            {
                return message.Response;
            }

            var response = new HttpResponseMessage(message.StatusCode);
            response.Content = new StringContent(message.Content, Encoding.UTF8, message.PageTemplate.ContentType);
            response.Headers.AddCookies(message.Cookies);
            return response;
        }
    }
}