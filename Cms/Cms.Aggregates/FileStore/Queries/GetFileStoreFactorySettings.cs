﻿using System;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.FileStore.Queries
{
    [RequiredPermission("sysadmin", PermissionLevel.CanEverything)]
    public class GetFileStoreFactorySettings : QueryBase
    {
        public Guid Id { get; set; }
    }
}