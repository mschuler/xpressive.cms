using System;
using Cms.Aggregates.Websites;
using Cms.Aggregates.Websites.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Websites
{
    internal class DomainAddedEventTranslator : IEventTranslator
    {
        private readonly IWebsiteRepository _repository;

        public DomainAddedEventTranslator(IWebsiteRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DomainAdded);
        }

        public string GetHeader(IEvent @event)
        {
            return "Domeinname hinzugefügt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DomainAdded)@event;
            var website = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Der Domainname <strong>{0}</strong> wurde der Website <strong>{1}</strong> hinzugefügt.", e.Host, website.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "website";
        }
    }
}