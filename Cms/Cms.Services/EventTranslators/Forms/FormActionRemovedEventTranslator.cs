﻿using System;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Forms
{
    internal class FormActionRemovedEventTranslator : IEventTranslator
    {
        private readonly IFormRepository _repository;

        public FormActionRemovedEventTranslator(IFormRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FormActionRemoved);
        }

        public string GetHeader(IEvent @event)
        {
            return "Formularaktion entfernt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FormActionRemoved)@event;
            var form = _repository.GetEvenIfDeleted(e.AggregateId);
            var name = string.Format("{0} (Nr. {1})", form.Name, form.PublicId);
            return string.Format("Dem Formular <strong>{0}</strong> wurde eine Aktion entfernt.", name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "form";
        }
    }
}