using System;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Forms
{
    internal class FormTemplateChangedEventTranslator : IEventTranslator
    {
        private readonly IFormRepository _repository;

        public FormTemplateChangedEventTranslator(IFormRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FormTemplateChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Formularvorlage ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FormTemplateChanged)@event;
            var form = _repository.GetEvenIfDeleted(e.AggregateId);
            var name = string.Format("{0} (Nr. {1})", form.Name, form.PublicId);
            return string.Format("Die Vorlage des Formulars <strong>{0}</strong> wurde ge�ndert.", name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "form";
        }
    }
}