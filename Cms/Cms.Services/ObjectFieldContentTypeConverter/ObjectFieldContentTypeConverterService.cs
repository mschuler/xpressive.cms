using System;
using System.Collections.Generic;
using System.Linq;
using Cms.SharedKernel;

namespace Cms.Services.ObjectFieldContentTypeConverter
{
    internal class ObjectFieldContentTypeConverterService : IObjectFieldContentTypeConverterService
    {
        private readonly IList<IObjectFieldContentTypeConverter> _converters;

        public ObjectFieldContentTypeConverterService(IEnumerable<IObjectFieldContentTypeConverter> converters)
        {
            _converters = converters.ToList();
        }

        public string Serialize(string type, string json)
        {
            return GetConverter(type).Serialize(json);
        }

        public object Deserialize(string type, string value)
        {
            return GetConverter(type).Deserialize(value);
        }

        public SortableObjectField DeserializeForSortableField(string type, string value)
        {
            return GetConverter(type).DeserializeForSortableField(value);
        }

        public object DeserializeForPublicApi(string type, string value)
        {
            return GetConverter(type).DeserializeForPublicApi(value);
        }

        public bool TryGetMetadataForPublicApi(string type, string value, out object metadata)
        {
            return GetConverter(type).TryGetMetadataForPublicApi(value, out metadata);
        }

        public object DeserializeForInterpreter(string type, string value)
        {
            return GetConverter(type).DeserializeForInterpreter(value);
        }

        public string SerializeForImport(string type, string json, string format = null, string options = null)
        {
            return GetConverter(type).SerializeForImport(json, format, options);
        }

        public string SerializeForImportPreview(string type, string json, string format = null, string options = null)
        {
            return GetConverter(type).SerializeForImportPreview(json, format, options);
        }

        public bool IsValidValueForImport(string type, string json, string format = null, string options = null)
        {
            return GetConverter(type).IsValidValueForImport(json, format, options);
        }

        private IObjectFieldContentTypeConverter GetConverter(string type)
        {
            var converter = _converters.SingleOrDefault(c => c.CanHandle(type));
            if (converter == null)
            {
                throw new NotSupportedException("Object field type '" + type + "' is not supported.").WithGuid();
            }
            return converter;
        }
    }
}