using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Links;

namespace Cms.Aggregates.Calendars
{
    public class Appointment
    {
        private readonly object _categoriesLock = new object();
        private readonly List<Category> _categories = new List<Category>();

        public Appointment()
        {
            _categories = new List<Category>();
        }

        public ulong Id { get; set; }
        public string Title { get; set; }
        public bool IsHighlight { get; set; }
        public DateTimeRange StartEnd { get; set; }
        public DateTime StartDate { get { return StartEnd.Start.Date; } }
        public DateTime EndDate { get { return StartEnd.End.Date; } }
        public string Location { get; set; }
        public ILink Link { get; set; }
        public AppointmentRecurrence Recurrence { get; set; }

        public IEnumerable<Category> Categories
        {
            get
            {
                lock (_categoriesLock)
                {
                    return _categories.ToList();
                }
            }
        }

        public void Add(Category category)
        {
            lock (_categoriesLock)
            {
                if (_categories.Contains(category))
                {
                    return;
                }
                _categories.Add(category);
            }
        }

        public void Remove(Category category)
        {
            lock (_categoriesLock)
            {
                _categories.Remove(category);
            }
        }

        public IEnumerable<AppointmentOccurrence> Occurrences(DateTimeRange fromTo)
        {
            var date = fromTo.Start.Date;

            while (date <= fromTo.End.Date)
            {
                var daysToAdd = 1;

                if (Recurrence.IsMatch(this, date))
                {
                    var start = date.Add(StartEnd.Start.TimeOfDay);
                    var isSingleOccurrence = Recurrence.GetType() == typeof(SingleOccurrence);

                    yield return new AppointmentOccurrence
                    {
                        AppointmentId = Id,
                        Title = Title,
                        IsHighlight = IsHighlight,
                        StartEnd = new DateTimeRange(start, StartEnd.Duration),
                        Location = Location,
                        Link = Link,
                        IsSingleOccurrence = isSingleOccurrence
                    };

                    daysToAdd = Math.Max(1, (int)Math.Ceiling(StartEnd.Duration.TotalDays));

                    if (StartEnd.Duration.TotalDays > 1 && Recurrence.IsMatch(this, date.AddDays(daysToAdd)))
                    {
                        daysToAdd += 1;
                    }
                }

                date = date.AddDays(daysToAdd);
            }
        }
    }
}