﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;

namespace Cms.Services.Contracts
{
    public static class ServiceLocator
    {
        private static readonly ContainerBuilder _builder;
        private static IContainer _container;

        static ServiceLocator()
        {
            _builder = new ContainerBuilder();
            _builder.RegisterAssemblyModules(Assembly.Load("Cms.Aggregates"));
            _builder.RegisterAssemblyModules(Assembly.Load("Cms.Services"));
            _builder.RegisterAssemblyModules(Assembly.Load("Cms.WebNew"));
            _builder.RegisterAssemblyModules(Assembly.Load("Cms.DatabaseMigrations"));
            _builder.RegisterAssemblyModules(Assembly.Load("Cms.EventSourcing"));

            var directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
            var plugins = Directory.GetFiles(directory, "Cms.Plugin.*.dll", SearchOption.TopDirectoryOnly);

            foreach (var plugin in plugins)
            {
                var pluginFileName = Path.GetFileName(plugin);
                pluginFileName = pluginFileName.Substring(0, pluginFileName.Length - 4);
                _builder.RegisterAssemblyModules(Assembly.Load(pluginFileName));
            }
        }

        public static IContainer ServiceContainer
        {
            get { return _container; }
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public static ICollection<T> ResolveAll<T>()
        {
            return _container.Resolve<IList<T>>();
        }

        public static void RegisterApiControllers()
        {
            _builder.RegisterApiControllers(Assembly.GetCallingAssembly());
        }

        public static void Build()
        {
            _container = _builder.Build();
        }
    }
}
