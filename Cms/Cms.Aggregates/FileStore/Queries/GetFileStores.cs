﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.FileStore.Queries
{
    [RequiredPermission("file", PermissionLevel.CanSee)]
    public class GetFileStores : QueryBase { }
}