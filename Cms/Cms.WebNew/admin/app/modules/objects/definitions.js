﻿(function() {
    angular.module("app").controller("ObjectDefinitionsController", ["$scope", "$rootScope", "$http", "$log", "$location", "tenantService", "dialogService", "toastService", "$translate", function ($scope, $rootScope, $http, $log, $location, tenantService, dialogService, toastService, $translate) {
        $scope.definitions = [];
        $scope.selectedDefinition = {};
        $scope.searchText = '';

        $rootScope.$on('logout', function () {
            $scope.definitions = [];
        });

        $scope.select = function (node) {
            $scope.selectedDefinition = node.node;
        };

        $http.get("/api/permission/objectdefinition/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        var deleteObjectDefinition = "";
        var confirmDeleteObjectDefinition = "";
        var errorDeleteObjectDefinition = "";
        var errorStarObjectDefinition = "";
        var errorUnstarObjectDefinition = "";
        var createObjectDefinition = "";
        var errorCreateObjectDefinition = "";
        var name = "";
        var renameObjectDefinition = "";
        var errorRenameObjectDefinition = "";

        $translate(["delete_object_definition", "confirm_delete_object_definition", "error_delete_object_definition", "error_star_object_definition", "error_unstar_object_definition", "create_object_definition", "error_create_object_definition", "name", "rename_object_definition", "error_rename_object_definition"]).then(function (t) {
            deleteObjectDefinition = t.delete_object_definition;
            confirmDeleteObjectDefinition = t.confirm_delete_object_definition;
            errorDeleteObjectDefinition = t.error_delete_object_definition;
            errorStarObjectDefinition = t.error_star_object_definition;
            errorUnstarObjectDefinition = t.error_unstar_object_definition;
            createObjectDefinition = t.create_object_definition;
            errorCreateObjectDefinition = t.error_create_object_definition;
            name = t.name;
            renameObjectDefinition = t.rename_object_definition;
            errorRenameObjectDefinition = t.error_rename_object_definition;
        });

        $scope.delete = function (node) {
            dialogService.confirmDialog(deleteObjectDefinition, String.format(confirmDeleteObjectDefinition, node.node.fullname), function () {
                $http.delete("/api/objectdefinition/" + node.node.id).success(function () {
                    var definitions = $scope.definitions.filter(function (item) {
                        return item.id !== node.node.id;
                    });
                    $scope.definitions = definitions;
                }).error(function(error) {
                    toastService.error(errorDeleteObjectDefinition, error);
                });
            });
        };

        $scope.rename = function (node) {
            dialogService.singleInputDialog(renameObjectDefinition, name, node.node.name, function (newName) {
                $http.put("/api/objectdefinition/" + node.node.id + "/name", { name: newName }).success(function () {
                    node.node.name = newName;
                    node.node.fullname = newName + " (Nr. " + node.node.nr+ ")";
                }).error(function (error) {
                    toastService.error(errorRenameObjectDefinition, error);
                });
            });
        };

        $scope.edit = function(node) {
            $location.path("/objects/definition/" + node.node.id);
        };

        $scope.unpin = function (node) {
            return $http.put("/api/objectdefinition/" + node.node.id + "/unstar").success(function () {
                node.node.isStarred = "false";
            }).error(function(error) {
                toastService.error(errorUnstarObjectDefinition, error);
            });
        };

        $scope.pin = function (node) {
            return $http.put("/api/objectdefinition/" + node.node.id + "/star").success(function () {
                node.node.isStarred = "true";
            }).error(function (error) {
                toastService.error(errorStarObjectDefinition, error);
            });
        };

        $scope.search = function() {
            
        };

        $scope.create = function() {
            dialogService.singleInputDialog(createObjectDefinition, name, "", function (newName) {
                var t = tenantService.getTenant().id;
                var dto = {
                    name: newName
                };
                $http.post("/api/objectdefinition/" + t, dto).success(function (definition) {
                    $scope.definitions.push(definition);
                    $location.path("/objects/definition/" + definition.id);
                }).error(function(error) {
                    toastService.error(errorCreateObjectDefinition, error);
                });
            });
        };

        var getDefinitions = function() {
            var tenantId = tenantService.getTenant().id;
            $scope.definitions = [];

            $http.get("/api/objectdefinition/" + tenantId, { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = false;
                    $scope.definitions.push(d);
                });
            });

            $http.get("/api/objectdefinition/" + tenantId + "/shared", { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = true;
                    $scope.definitions.push(d);
                });
            });
        };

        $scope.shareDefinition = function (definition) {
            var existingTenantIds = [];

            if (definition.shared) {
                existingTenantIds = _.map(definition.shared, function (t) { return t.id; });
            }

            dialogService.selectTenantsDialog(existingTenantIds, function (tenants) {
                var ids = _.map(tenants, function (t) {
                    return t.id;
                });
                var dto = {
                    tenantIds: ids
                };
                var url = String.format("/api/objectdefinition/{0}/share", definition.id);
                $http.put(url, dto).success(function () {
                    definition.shared = tenants;
                });
            });
        };

        $scope.donotshareDefinition = function (definition) {
            var url = String.format("/api/objectdefinition/{0}/share", definition.id);
            var dto = {
                tenantIds: []
            };
            return $http.put(url, dto).success(function () {
                definition.shared = [];
            });
        };

        $scope.getNames = function (tenants) {
            var names = "";
            _.each(tenants, function (t) {
                names += t.name + ", ";
            });
            names = names.trim();
            return names.substring(0, names.length - 1);
        };

        $rootScope.$on("tenantChanged", getDefinitions);

        getDefinitions();
    }]);

    angular.module("app").controller("ObjectDefinitionController", ["$scope", "$rootScope", "$routeParams", "$http", "$log", "$location", "$modal", "tenantService", "dialogService", "toastService", "$translate", "definitionFieldContentTypeService", function ($scope, $rootScope, $routeParams, $http, $log, $location, $modal, tenantService, dialogService, toastService, $translate, definitionFieldContentTypeService) {
        var definitionId = $routeParams.definitionId;
        $scope.definition = {};
        $scope.selectedField = "";

        $scope.select = function (field) {
            $scope.selectedField = field.name;
        };

        var errorMoveUp = "";
        var errorMoveDown = "";
        var addField = "";
        var errorAddField = "";
        var deleteField = "";
        var confirmDeleteField = "";
        var errorDeleteField = "";
        var changeField = "";
        var tName = "";
        var errorRenameField = "";

        $translate(["error_move_objectdefinitionfield_up", "error_move_objectdefinitionfield_down", "add_objectdefinitionfield", "error_add_objectdefinitionfield", "delete_objectdefinitionfield", "confirm_delete_objectdefinitionfield", "error_delete_objectdefinitionfield", "change_definition_field", "name", "error_rename_definition_field"]).then(function (t) {
            errorMoveUp = t.error_move_objectdefinitionfield_up;
            errorMoveDown = t.error_move_objectdefinitionfield_down;
            addField = t.add_objectdefinitionfield;
            errorAddField = t.error_add_objectdefinitionfield;
            deleteField = t.delete_objectdefinitionfield;
            confirmDeleteField = t.confirm_delete_objectdefinitionfield;
            errorDeleteField = t.error_delete_objectdefinitionfield;
            changeField = t.change_definition_field;
            tName = t.name;
            errorRenameField = t.error_rename_definition_field;
        });

        $scope.rename = function (field) {
            $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/app/modules/objects/definitionFieldDialog.min.html',
                controller: 'DefinitionFieldDialogController',
                resolve: {
                    subject: function () { return changeField; },
                    name: function () { return field.name; },
                    contentType: function () { return field.contentType; },
                    options: function () { return field.contentTypeOptions; },
                }
            }).result.then(function (f) {
                var dto = {
                    oldName: field.name,
                    newName: f.name,
                    contentTypeOptions: f.options,
                };

                $http.put("/api/objectdefinition/" + definitionId + "/field", dto).success(function () {
                    field.name = f.name;
                    field.contentTypeOptions = f.options;
                }).error(function (error) {
                    toastService.error(errorRenameField, error);
                });
            });
        };

        $scope.moveUp = function (field) {
            var dto = {
                name: field.name
            };
            return $http.put("/api/objectdefinition/" + definitionId + "/up", dto).success(function() {
                field.sortOrder -= 1.5;
                resetSortOrder();
            }).error(function(error) {
                toastService.error(errorMoveUp, error);
            });
        };

        $scope.moveDown = function (field) {
            var dto = {
                name: field.name
            };
            return $http.put("/api/objectdefinition/" + definitionId + "/down", dto).success(function () {
                field.sortOrder += 1.5;
                resetSortOrder();
            }).error(function (error) {
                toastService.error(errorMoveDown, error);
            });
        };

        $scope.markSortable = function(field) {
            var dto = {
                name: field.name
            };
            var sortable = field.isSortableField ? "notsortable" : "sortable";
            return $http.put("/api/objectdefinition/" + definitionId + "/" + sortable, dto).success(function () {
                field.isSortableField = !field.isSortableField;
            });
        };

        function resetSortOrder() {
            var fields = _.sortBy($scope.definition.fields, function (f) { return f.sortOrder; });
            _.each(fields, function (f, newIndex) {
                f.sortOrder = newIndex;
            });
            $scope.definition.fields = fields;
        }

        $scope.add = function () {
            $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/app/modules/objects/definitionFieldDialog.min.html',
                controller: 'DefinitionFieldDialogController',
                resolve: {
                    subject: function () { return addField; },
                    name: function () { return ""; },
                    contentType: function () { return ""; },
                    options: function() { return ""; },
                }
            }).result.then(function (field) {
                var f = {
                    name: field.name,
                    contentType: field.contentType,
                    contentTypeOptions: field.options
                };
                $http.put("/api/objectdefinition/" + definitionId + "/add", f).success(function () {
                    var sortOrder = 1;

                    _.each($scope.definition.fields, function(ef) {
                        if (ef.sortOrder > sortOrder) {
                            sortOrder = ef.sortOrder + 1;
                        }
                    });

                    $scope.definition.fields.push({
                        name: field.name,
                        contentType: field.contentType,
                        contentTypeTranslation: field.translation,
                        contentTypeIcon:field.icon,
                        sortOrder: sortOrder,
                        isTitle: false,
                    });
                    resetSortOrder();
                }).error(function(error) {
                    toastService.error(errorAddField, error);
                });
            });
        };

        $scope.remove = function (field) {
            dialogService.confirmDialog(deleteField, String.format(confirmDeleteField, field.name), function () {
                var dto = {
                    name: field.name
                };
                $http.put("/api/objectdefinition/" + definitionId + "/remove", dto).success(function () {
                    var others = _.filter($scope.definition.fields, function (f) {
                        return f.name !== field.name;
                    });
                    $scope.definition.fields = others;
                }).error(function(error) {
                    toastService.error(errorDeleteField, error);
                });
            });
        };

        var getDefinition = function () {
            $scope.definition = {};
            var url = String.format("/api/objectdefinition/{0}/{1}/detail", tenantService.getTenant().id, definitionId);
            $http.get(url, { cache: false }).success(function (data) {
                var contentTypes = definitionFieldContentTypeService.contentTypes;
                _.each(data.fields, function (d) {
                    var translation = _.find(contentTypes, function (ct) { return ct.value === d.contentType; });
                    if (translation) {
                        d.contentTypeTranslation = translation.name;
                        d.contentTypeIcon = translation.icon;
                    } else {
                        d.contentTypeTranslation = d.contentType;
                        d.contentTypeIcon = "";
                    }
                });
                $scope.definition = data;
            });
        };

        getDefinition();
    }]);

    angular.module("app").factory("definitionFieldContentTypeService", ["$translate", function($translate) {
        var contentTypes = [];

        $translate(["text", "html", "code", "markdown", "file", "boolean", "number", "date", "page", "object", "select_list", "page_link", "form"]).then(function (t) {
            contentTypes.push({ value: "Text", name: t.text, icon: "fa fa-font" });
            contentTypes.push({ value: "Html", name: t.html, icon: "fa fa-code" });
            contentTypes.push({ value: "Markdown", name: t.markdown, icon: "fa fa-medium" });
            contentTypes.push({ value: "File", name: t.file, icon: "fa fa-file-image-o" });
            contentTypes.push({ value: "Boolean", name: t.boolean, icon: "fa fa-check-square-o" });
            contentTypes.push({ value: "Number", name: t.number, icon: "ti-quote-right" });
            contentTypes.push({ value: "Date", name: t.date, icon: "fa fa-calendar" });
            contentTypes.push({ value: "Page", name: t.page, icon: "fa fa-file-o" });
            contentTypes.push({ value: "Object", name: t.object, icon: "ti-ticket" });
            contentTypes.push({ value: "List", name: t.select_list, icon: "fa fa-list" });
            contentTypes.push({ value: "PageLink", name: t.page_link, icon: "fa fa-external-link-square" });
            contentTypes.push({ value: "Form", name: t.form, icon: "fa fa-comments-o" });
            contentTypes.push({ value: "Code", name: t.code, icon: "fa fa-code" });
        });

        var factory = {};
        factory.contentTypes = contentTypes;
        return factory;
    }]);

    angular.module("app").controller("DefinitionFieldDialogController", ["$scope", "$modalInstance", "$log", "definitionFieldContentTypeService", "subject", "name", "contentType", "options", function ($scope, $modalInstance, $log, definitionFieldContentTypeService, subject, name, contentType, options) {
        $scope.subject = subject;
        $scope.name = name;
        $scope.options = options;
        $scope.result = {};
        $scope.isEdit = true;

        if (name === "") {
            $scope.isEdit = false;
        }

        $scope.contentTypes = definitionFieldContentTypeService.contentTypes;

        if (contentType) {
            $scope.result.contentType = _.find($scope.contentTypes, function (ct) { return ct.value === contentType; });
        }

        $scope.ok = function () {
            $modalInstance.close({
                name: $scope.name,
                contentType: $scope.result.contentType.value,
                options: $scope.options,
                translation: $scope.result.contentType.name,
                icon: $scope.result.contentType.icon
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());