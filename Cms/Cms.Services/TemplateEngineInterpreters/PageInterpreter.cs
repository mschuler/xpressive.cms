﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using Cms.Aggregates.Pages;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.Websites;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class PageInterpreter : IInterpreter
    {
        private readonly IPageRepository _pageRepository;
        private readonly IPageUrlService _pageUrlService;
        private readonly IPageTrackingService _pageTrackingService;
        private readonly IPageRenderingService _pageRenderingService;
        private readonly IContentHandlerFactory _contentHandlerFactory;
        private readonly IPageTemplateRepository _pageTemplateRepository;
        private readonly IUrlShorteningServiceProvider _urlShorteningServiceProvider;
        private readonly IWebsiteRepository _websiteRepository;
        private readonly IPageTemplateService _pageTemplateService;

        public PageInterpreter(
            IPageRepository pageRepository,
            IPageUrlService pageUrlService,
            IPageTrackingService pageTrackingService,
            IPageRenderingService pageRenderingService,
            IContentHandlerFactory contentHandlerFactory,
            IPageTemplateRepository pageTemplateRepository,
            IUrlShorteningServiceProvider urlShorteningServiceProvider,
            IWebsiteRepository websiteRepository, IPageTemplateService pageTemplateService)
        {
            _pageRepository = pageRepository;
            _pageUrlService = pageUrlService;
            _pageTrackingService = pageTrackingService;
            _pageRenderingService = pageRenderingService;
            _contentHandlerFactory = contentHandlerFactory;
            _pageTemplateRepository = pageTemplateRepository;
            _urlShorteningServiceProvider = urlShorteningServiceProvider;
            _websiteRepository = websiteRepository;
            _pageTemplateService = pageTemplateService;
        }

        public int Priority { get; set; }

        public string Name
        {
            get { return "Page"; }
        }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function)
            {
                CalculateFunction(tree);
            }
            else if (tree.ItemType == TreeItemType.Attribute && tree.LeftItem != null)
            {
                CalculateAttribute(tree);
            }
        }

        private void CalculateFunction(TreeItem tree)
        {
            if (tree.Value.Equals("page", StringComparison.OrdinalIgnoreCase))
            {
                CalculatePage(tree);
                return;
            }

            if (tree.LeftItem != null && tree.Children.Count == 1 && tree.Value.Equals("field"))
            {
                CalculateField(tree);
            }

            if (tree.LeftItem != null && tree.LeftItem.Result is PageBase)
            {
                if (tree.Value.Equals("render", StringComparison.OrdinalIgnoreCase) && tree.Children.Count == 0)
                {
                    var page = tree.LeftItem.Result as Page;
                    if (page != null)
                    {
                        tree.Result = _pageRenderingService.Render(page);
                    }
                }
            }

            if (tree.LeftItem != null && tree.LeftItem.Result is IEnumerable<object>)
            {
                if (tree.Value.Equals("render", StringComparison.OrdinalIgnoreCase) && tree.Children.Count == 0)
                {
                    var pageList = tree.LeftItem.Result as IEnumerable<object>;
                    var sb = new StringBuilder();
                    var engine = new Engine();

                    foreach (var p in pageList.OfType<Page>())
                    {
                        using (TemplateStateMap.Set(TemplateStateMap.CurrentObjectKey, p))
                        {
                            var pageContent = _pageRenderingService.Render(p);
                            var pageResult = engine.Generate(pageContent);
                            sb.Append(pageResult);
                        }
                    }

                    tree.Result = sb.ToString();
                }
            }

            if (tree.Value.Equals("$", StringComparison.Ordinal) && tree.Children.Count == 1 && tree.Children[0].Result is string)
            {
                PageBase currentPage = null;

                if (tree.LeftItem != null && tree.LeftItem.Result != null)
                {
                    currentPage = tree.LeftItem.Result as Page;
                }

                if (currentPage == null)
                {
                    currentPage = TemplateStateMap.Get<PageBase>(TemplateStateMap.CurrentObjectKey);
                }

                if (currentPage != null)
                {
                    CalculateCurrentPage(tree, currentPage);
                }
            }
        }

        private void CalculateField(TreeItem tree)
        {
            var o = tree.LeftItem.Result as Page;
            if (o == null)
            {
                return;
            }

            var name = tree.Children[0].Result as string;

            SetFieldValue(name, tree, o);
        }

        private void SetFieldValue(string name, TreeItem tree, Page page)
        {
            PageTemplate template;
            if (string.IsNullOrEmpty(name) || !_pageTemplateRepository.TryGetEvenIfDeleted(page.PageTemplateId, out template))
            {
                return;
            }

            var content = page.GetPageContent(template, _pageTemplateService);
            var field = content.SingleOrDefault(c => c.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
            if (field == null)
            {
                return;
            }

            var contentType = PageContentValueBase.GetType(field.Value);
            var handler = _contentHandlerFactory.Get(contentType);
            tree.Result = handler.GetValueForInterpreter(field.Value);
        }

        private void CalculatePage(TreeItem tree)
        {
            if (tree.Children.Count == 1)
            {
                var child = tree.Children[0];

                if (child.Result is int)
                {
                    var page = _pageRepository
                        .GetAll()
                        .OfType<Page>()
                        .FirstOrDefault(p => p.PublicId == (int)child.Result);
                    tree.Result = page;
                }
                else if (child.ItemType == TreeItemType.Function && child.Value.Equals("this", StringComparison.OrdinalIgnoreCase))
                {
                    tree.Result = TemplateStateMap.Get<PageBase>(TemplateStateMap.CurrentObjectKey);
                }
                else if (child.ItemType == TreeItemType.Function && child.Value.Equals("current", StringComparison.OrdinalIgnoreCase))
                {
                    tree.Result = TemplateStateMap.Get<Page>(TemplateStateMap.CurrentLoadedPage);
                }
            }
        }

        private void CalculateCurrentPage(TreeItem tree, PageBase page)
        {
            var fieldName = (string)tree.Children[0].Result;

            if (SetAttributeValue(fieldName, tree, page))
            {
                return;
            }

            var p = page as Page;
            if (p == null)
            {
                return;
            }

            SetFieldValue(fieldName, tree, p);
        }

        private void CalculateAttribute(TreeItem tree)
        {
            var pageBase = tree.LeftItem.Result as PageBase;

            if (pageBase != null)
            {
                SetAttributeValue(tree.Value, tree, pageBase);
            }
        }

        private bool SetAttributeValue(string fieldName, TreeItem tree, PageBase pageBase)
        {
            switch (fieldName.ToLowerInvariant())
            {
                case "parent":
                    PageBase parent;
                    _pageRepository.TryGet(pageBase.ParentPageId, out parent);
                    tree.Result = parent;
                    return true;
                case "name":
                    tree.Result = WebUtility.HtmlEncode(pageBase.Name);
                    return true;
                case "normalizedname":
                    tree.Result = pageBase.Name.ToValidUrlPart();
                    return true;
                case "url":
                    tree.Result = _pageUrlService.GetUrl(pageBase);
                    return true;
                case "shorturl":
                    tree.Result = Shorten(pageBase);
                    return true;
                case "hyperlink":
                    var url = _pageUrlService.GetUrl(pageBase);
                    tree.Result = string.Format("<a href=\"{0}\">{1}</a>", url, WebUtility.HtmlEncode(pageBase.Name));
                    return true;
                case "lastchange":
                    tree.Result = pageBase.LastChange;
                    return true;
                case "sortorder":
                    tree.Result = pageBase.SortOrder;
                    return true;
                case "islink":
                    tree.Result = (pageBase is PageLink).ToString();
                    return true;
            }

            var page = pageBase as Page;

            if (page != null)
            {
                switch (fieldName.ToLowerInvariant())
                {
                    case "id":
                        tree.Result = page.PublicId.ToString(CultureInfo.InvariantCulture);
                        return true;
                    case "permalink":
                    case "permalinkid":
                        tree.Result = page.Permalink.ToString("N");
                        return true;
                    case "title":
                        tree.Result = WebUtility.HtmlEncode(page.Title);
                        return true;
                    case "viewcount":
                        tree.Result = _pageTrackingService.GetTotalViewsPerPage(page.Id);
                        return true;
                    case "isenabled":
                        tree.Result = page.IsEnabled.ToString();
                        return true;
                    case "children":
                        tree.Result = GetChildren(page, false);
                        return true;
                    case "childrenwithlinks":
                        tree.Result = GetChildren(page, true);
                        return true;
                    case "template":
                        PageTemplate template;
                        if (!_pageTemplateRepository.TryGet(page.PageTemplateId, out template))
                        {
                            template = new PageTemplate();
                        }
                        tree.Result = template;
                        return true;
                }
            }

            return false;
        }

        private List<PageBase> GetChildren(PageBase page, bool withLinks)
        {
            var result = new List<PageBase>();

            result.AddRange(page.Children.OfType<Page>().Where(p => p.IsEnabled));

            if (withLinks)
            {
                result.AddRange(page.Children.OfType<PageLink>());
            }

            return result
                .OrderBy(p => p.SortOrder)
                .ThenBy(p => p.Name, StringComparer.OrdinalIgnoreCase)
                .ToList();
        }

        private string Shorten(PageBase page)
        {
            var website = _websiteRepository.Get(page.WebsiteId);
            var service = _urlShorteningServiceProvider.GetSelectedService(website.TenantId);
            var longUrl = _pageUrlService.GetUrl(page);

            if (service == null)
            {
                return longUrl;
            }

            return service.ShortenUrl(longUrl);
        }
    }
}
