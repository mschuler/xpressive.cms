using System;
using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0006_add_default_tenant : DatabaseMigrationBase
    {
        public M0006_add_default_tenant() : base(6) { }

        public override void Execute(Database database)
        {
            var json = string.Format("{{\"Name\":\"Default Tenant\",\"Date\":\"{0:O}\"}}", DateTime.UtcNow);
            var dto = new EventDto
            {
                Transaction = 62130516698087424,
                Version = 62130516698079232,
                AggregateType = "Tenant",
                AggregateId = 1234,
                Event = "TenantCreated",
                UserId = 100,
                Data = json,
                IsDeleted = false,
            };

            database.Insert(dto);
        }
    }
}