﻿using System;
using System.Collections.Generic;
using Cms.SharedKernel;

namespace Cms.Aggregates.Files
{
    public static class FileExtensions
    {
        private static readonly HashSet<string> _imageExtensions = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { "png", "jpg", "jpeg", "bmp", "gif" };

        public static string GetExtension(this File file)
        {
            var fileName = file.Name;
            return fileName.GetExtension();
        }

        public static bool IsImage(this File file)
        {
            var extension = file.GetExtension();
            return _imageExtensions.Contains(extension);
        }

        public static bool IsImage(this string fileName)
        {
            var extension = fileName.GetExtension();
            return _imageExtensions.Contains(extension);
        }

        public static bool IsJpg(this string fileName)
        {
            var extension = fileName.GetExtension().ToLowerInvariant();
            return extension.Equals("jpg", StringComparison.Ordinal) ||
                   extension.Equals("jpeg", StringComparison.Ordinal);
        }
    }
}