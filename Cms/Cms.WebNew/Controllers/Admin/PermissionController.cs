using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Commands;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class PermissionController : ApiControllerBase
    {
        private static readonly IDictionary<string, string> _modules = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            {"page", "Pages"},
            {"pagetemplate", "Page templates"},
            {"file", "Files and Directories"},
            {"form", "Forms"},
            {"object", "Objects"},
            {"objecttemplate", "Object templates"},
            {"objectdefinition", "Object definitions"},
            {"appointment", "Appointments"},
            {"calendarcategory", "Calendar categories"},
            {"calendartemplate", "Calendar templates"},
            {"tenant", "Tenants"},
            {"website", "Websites and Domains"},
            {"user", "Users"},
            {"group", "Groups"},
        };

        private readonly IGroupRepository _groupRepository;

        public PermissionController(ICommandQueue commandQueue, IUserRepository userRepository, IGroupRepository groupRepository, IQueryHandler queryHandler)
            : base(commandQueue, userRepository, queryHandler)
        {
            _groupRepository = groupRepository;
        }

        [HttpGet, Route("api/permissions/{groupId:ulong}")]
        public IEnumerable<GroupPermissionDto> Get(ulong groupId)
        {
            var group = _groupRepository.Get(groupId);
            var userPermissions = CmsUser.GetPermissions(group.TenantId, _groupRepository).ToDictionary(p => p.ModuleId, p => p.PermissionLevel, StringComparer.OrdinalIgnoreCase);

            foreach (var module in _modules)
            {
                PermissionLevel permission;
                if (CmsUser.IsSysadmin)
                {
                    permission = PermissionLevel.CanEverything;
                }
                else if (!userPermissions.TryGetValue(module.Key, out permission))
                {
                    permission = PermissionLevel.Forbidden;
                }

                if (permission == PermissionLevel.Forbidden)
                {
                    continue;
                }

                var canEdit = permission >= PermissionLevel.CanChange;
                var canAdmin = permission >= PermissionLevel.CanEverything;
                PermissionLevel currentLevel;
                group.TryGetPermission(module.Key, out currentLevel);

                yield return new GroupPermissionDto
                {
                    ModuleId = module.Key,
                    ModuleName = module.Value,
                    CanEdit = canEdit,
                    CanAdmin = canAdmin,
                    Permission = (int)currentLevel,
                };
            }
        }

        [HttpGet, Route("api/permission/{moduleId}/{tenantId:ulong}")]
        public GroupPermissionDto GetModulePermissionForCurrentUser(string moduleId, ulong tenantId)
        {
            var user = CmsUser;
            var dto = new GroupPermissionDto { ModuleId = moduleId };
            string moduleName;

            if (!_modules.TryGetValue(moduleId, out moduleName))
            {
                return dto;
            }

            dto.ModuleName = moduleName;
            dto.CanEdit = user.HasPermission(moduleId, tenantId, PermissionLevel.CanChange, _groupRepository);
            dto.CanAdmin = user.HasPermission(moduleId, tenantId, PermissionLevel.CanEverything, _groupRepository);

            return dto;
        }

        [HttpPost, Route("api/permissions/{groupId:ulong}")]
        public void Change(ulong groupId, [FromBody] GroupPermissionDto permission)
        {
            var tenantId = _groupRepository.Get(groupId).TenantId;
            SendCommand(new ChangePermission
            {
                AggregateId = groupId,
                ModuleId = permission.ModuleId,
                Permission = (PermissionLevel)permission.Permission,
                TenantId = tenantId,
            });
        }

        public class GroupPermissionDto
        {
            public string ModuleId { get; set; }
            public string ModuleName { get; set; }
            public bool CanEdit { get; set; }
            public bool CanAdmin { get; set; }
            public int Permission { get; set; }
        }
    }
}