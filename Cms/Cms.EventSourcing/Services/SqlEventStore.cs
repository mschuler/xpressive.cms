﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Cms.EventSourcing.Contracts.Events;
using Cms.SharedKernel;
using log4net;
using NPoco;
using UniqueIdGenerator.Net;

namespace Cms.EventSourcing.Services
{
    public sealed class SqlEventStore : IEventStore
    {
        private static readonly Generator _transactionIdGenerator = new Generator(2, new DateTime(2014, 10, 1));
        private static readonly ILog _log = LogManager.GetLogger(typeof(SqlEventStore));

        private readonly IEventSerializer _eventSerializer;
        private readonly IEventTypeResolver _eventTypeResolver;

        public SqlEventStore(IEventSerializer eventSerializer, IEventTypeResolver eventTypeResolver)
        {
            _eventSerializer = eventSerializer;
            _eventTypeResolver = eventTypeResolver;
        }

        public void Persist(IEnumerable<IEvent> events)
        {
            var transactionId = _transactionIdGenerator.NextLong();

            using (var database = new Database("CmsDatabaseConnection"))
            {
                database.BeginTransaction(IsolationLevel.ReadCommitted);

                try
                {
                    foreach (var @event in events)
                    {
                        var data = _eventSerializer.Serialize(@event);
                        database.Insert(new PersistedEvent(@event, transactionId, data));
                    }

                    database.CompleteTransaction();
                }
                catch (Exception e)
                {
                    database.AbortTransaction();
                    e.WithGuid();
                    _log.Error("Error persisting event.", e);
                    throw;
                }
            }
        }

        public IEnumerable<IEvent> Fetch(int skip, int take)
        {
            IList<PersistedEvent> events;

            using (var database = new Database("CmsDatabaseConnection"))
            {
                events = database.FetchBy<PersistedEvent>(s => s
                    .Where(e => !e.IsDeleted)
                    .OrderByDescending(e => e.Version)
                    .Limit(skip, take));
            }

            return ConvertDtoToEvent(events).ToList();
        }

        public IList<IEvent> Fetch()
        {
            IList<PersistedEvent> events;
            IList<IEvent> result;

            using (PerformanceMeasurer.Get("Load events from database", o => _log.Debug(o)))
            {
                using (var database = new Database("CmsDatabaseConnection"))
                {
                    events = database.FetchBy<PersistedEvent>(s => s
                        .Where(e => !e.IsDeleted)
                        .OrderBy(e => e.Version));
                }
            }

            using (PerformanceMeasurer.Get("Convert events", o => _log.Debug(o)))
            {
                result = ConvertDtoToEvent(events).ToList();
            }

            return result;
        }

        public IEnumerable<IEvent> Fetch(ulong aggregateId)
        {
            IList<PersistedEvent> events;

            var convertedId = Convert.ToInt64(aggregateId);

            using (var database = new Database("CmsDatabaseConnection"))
            {
                events = database.FetchBy<PersistedEvent>(s => s
                    .Where(e => !e.IsDeleted)
                    .Where(e => e.AggregateId == convertedId)
                    .OrderBy(e => e.Version));
            }

            return ConvertDtoToEvent(events).ToList();
        }

        public IEnumerable<ulong> GetAggregateIds<T>()
        {
            using (var database = new Database("CmsDatabaseConnection"))
            {
                return database.Fetch<ulong>(
                    "select distinct AggregateId from Events where AggregateType = @0",
                    typeof(T).Name);
            }
        }

        private IEnumerable<IEvent> ConvertDtoToEvent(IEnumerable<PersistedEvent> events)
        {
            foreach (var @event in events)
            {
                var e = ConvertDtoToEvent(@event);
                if (e != null)
                {
                    yield return e;
                }
            }
        }

        private IEvent ConvertDtoToEvent(PersistedEvent @event)
        {
            try
            {
                var type = _eventTypeResolver.GetEventType(@event.Event);
                var e = _eventSerializer.Deserialize(@event.Data, type);
                e.AggregateId = Convert.ToUInt64(@event.AggregateId);
                e.UserId = Convert.ToUInt64(@event.UserId);
                e.Version = Convert.ToUInt64(@event.Version);
                return e;
            }
            catch (Exception e)
            {
                var msg = string.Format("Unable to deserialize event {0:D} of type {1}", @event.Version, @event.Event);
                _log.Error(msg, e);
                return null;
            }
        }
    }
}
