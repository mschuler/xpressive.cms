﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Websites.Commands
{
    [RequiredPermission("website", PermissionLevel.CanChange)]
    public class CreateWebsite : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }

        [Required, Range(1, int.MaxValue)]
        public int PublicId { get; set; }
    }
}