﻿using System;
using System.Linq;
using System.Reflection;
using Cms.Aggregates;
using Cms.Aggregates.Security;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers
{
    internal static class CheckUserRequirementsForRequiredPermissionAttribute
    {
        public static bool IsAllowedToExecute(Type type, User user, ulong tenantId, IGroupRepository groupRepository)
        {
            if (user.IsSysadmin)
            {
                return true;
            }

            var attributes = GetRequiredPermission(type);
            if (attributes.All(a => !a.IsRequired))
            {
                return true;
            }

            var isAllowed = attributes.Any(a => user.HasPermission(a.ModuleId, tenantId, a.PermissionLevel, groupRepository));
            return isAllowed;
        }

        private static RequiredPermissionAttribute[] GetRequiredPermission(Type type)
        {
            var attributes = type.GetCustomAttributes<RequiredPermissionAttribute>(false);
            var array = attributes != null ? attributes.ToArray() : new RequiredPermissionAttribute[0];

            if (array.Length == 0)
            {
                var error = string.Format("No RequiredPermissionAttribute for type {0} set.", type.Name);
                throw new InvalidOperationException(error).WithGuid();
            }
            return array;
        }
    }
}