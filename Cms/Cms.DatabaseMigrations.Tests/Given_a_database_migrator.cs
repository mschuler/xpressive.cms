﻿using Xunit;

namespace Cms.DatabaseMigrations.Tests
{
    public class Given_a_database_migrator
    {
        [Fact(Skip = "This is a tool, not a test.")]
        public void Then_it_works()
        {
            var migrator = new DatabaseMigrator();

            migrator.Migrate();
        }
    }
}
