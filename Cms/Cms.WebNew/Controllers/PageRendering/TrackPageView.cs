using Cms.Services.Contracts;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class TrackPageView : IFilter<RenderPageMessage>
    {
        private readonly IPageTrackingService _trackingService;

        public TrackPageView(IPageTrackingService trackingService)
        {
            _trackingService = trackingService;
        }

        public bool Execute(RenderPageMessage msg)
        {
            if (msg.IsKnownBot)
            {
                return true;
            }

            _trackingService.TrackPageViewAsync(
                msg.Website.Id,
                msg.Page.Id,
                msg.IpAddress,
                msg.TrackingSessionId,
                msg.TrackingPersistedId);
            return true;
        }
    }
}