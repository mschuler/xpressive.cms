﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Calendars.Queries
{
    internal class CalendarCategoryQueryHandler : QueryHandlerBase
    {
        private readonly ICalendarRepository _repository;

        public CalendarCategoryQueryHandler(ICalendarRepository repository)
        {
            _repository = repository;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetAllCalendarCategories)
            };
        }

        public IEnumerable<object> When(GetAllCalendarCategories query)
        {
            return _repository
                .GetCategories(query.TenantId)
                .Where(c => string.IsNullOrEmpty(query.SearchText) || c.Name.ToUpperInvariant().Contains(query.SearchText.ToUpperInvariant()))
                .OrderBy(c => c.Name, StringComparer.OrdinalIgnoreCase)
                .Select(c => new { c.Name });
        }
    }
}
