using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using Cms.Services.Contracts;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class ApiKeyInterpreter : IInterpreter
    {
        private readonly IApiKeyService _apiKeyService;

        public ApiKeyInterpreter(IApiKeyService apiKeyService)
        {
            _apiKeyService = apiKeyService;
        }

        public int Priority { get; set; }

        public string Name
        {
            get { return "API Key"; }
        }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function)
            {
                if (tree.Value.Equals("apikey", StringComparison.OrdinalIgnoreCase) && tree.Children.Count == 0)
                {
                    var ipAddress = TemplateStateMap.Get<string>("Visitor.IpAddress");
                    var keyPair = _apiKeyService.GenerateNewKey(ipAddress);

                    var cookies = TemplateStateMap.Get<List<CookieHeaderValue>>("Cookies");

                    cookies.Add(new CookieHeaderValue("ak", keyPair.Cookie)
                    {
                        HttpOnly = true
                    });

                    tree.Result = keyPair.ApiKey;
                }
            }
        }
    }
}