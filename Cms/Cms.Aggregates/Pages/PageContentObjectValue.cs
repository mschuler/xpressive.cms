namespace Cms.Aggregates.Pages
{
    public class PageContentObjectValue : PageContentValueBase
    {
        public string DefinitionId { get; set; }
        public string ObjectId { get; set; }
        public string TemplateName { get; set; }
    }
}