﻿(function() {
    angular.module("app").controller("AppointmentController", ["$rootScope", "$scope", "$http", "$timeout", "$modal", "tenantService", "dialogService", "toastService", "$translate", function ($rootScope, $scope, $http, $timeout, $modal, tenantService, dialogService, toastService, $translate) {
        var monthOffset = 0;
        $scope.isBusy = false;
        $scope.appointments = [];
        $scope.selectedAppointment = {};

        $http.get("/api/permission/appointment/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.select = function(appointment) {
            $scope.selectedAppointment = appointment;
        }

        $scope.edit = function(appointment) {
            var url = String.format("/api/appointment/{1}/{0}", appointment.appointmentId, tenantService.getTenant().id);
            $http.get(url, { cache: false }).success(function (data) {
                if (appointment.isSingleOccurrence) {
                    $translate("edit_appointment").then(function(subject) {
                        openAppointmentDialog(subject, data, function (a) {
                            $http.post(url, a).success(function () {
                                reloadAppointmentsInFiveSeconds();
                            }).error(function (error) {
                                $translate("error_edit_appointment").then(function(msg) {
                                    toastService.error(msg, error);
                                });
                            });
                        });
                    });
                } else {
                    $translate("edit_recurrent_appointment").then(function(subject) {
                        openRecurrentAppointmentDialog(subject, data, function (a) {
                            $http.post(url, a).success(function () {
                                reloadAppointmentsInFiveSeconds();
                            }).error(function (error) {
                                $translate("error_edit_recurrent_appointment").then(function (msg) {
                                    toastService.error(msg, error);
                                });
                            });
                        });
                    });
                }
            });
        };

        $scope.delete = function (appointment) {
            $translate(["delete_appointment", "confirm_delete_appointment", "error_delete_appointment"]).then(function(t) {
                dialogService.confirmDialog(t.delete_appointment, String.format(t.confirm_delete_appointment, appointment.title), function () {
                    var url = String.format("/api/appointment/{1}/{0}", appointment.appointmentId, tenantService.getTenant().id);
                    $http.delete(url).success(function () {
                        $scope.appointments = _.reject($scope.appointments, function (a) { return a.appointmentId === appointment.appointmentId; });
                    }).error(function (error) {
                        toastService.error(t.error_delete_appointment, error);
                    });
                });
            });
        };

        $scope.nextPage = function () {
            if ($scope.isBusy) {
                return;
            }
            $scope.isBusy = true;

            if (monthOffset == 0) {
                loadUntilMonthOffset(monthOffset + 2);
            } else {
                loadUntilMonthOffset(monthOffset + 1);
            }
        };

        function loadUntilMonthOffset(maxMonthOffset) {
            var url = String.format("/api/appointments/{1}/{0}", monthOffset, tenantService.getTenant().id);
            $http.get(url, { cache: false }).success(function (data) {
                if (data) {
                    for (var i = 0; i < data.length; i++) {
                        $scope.appointments.push(data[i]);
                    }
                }
                monthOffset++;
                if (maxMonthOffset > monthOffset) {
                    loadUntilMonthOffset(maxMonthOffset);
                } else {
                    $scope.isBusy = false;
                }
            });
        };

        $scope.createAppointment = function () {
            var appointment = {
                recurrence: "single",
                recurrenceOptions: {},
                link: {
                    linkType: "none"
                }
            };
            $translate(["create_appointment", "error_create_appointment"]).then(function(t) {
                openAppointmentDialog(t.create_appointment, appointment, function (a) {
                    $http.post("/api/appointments/" + tenantService.getTenant().id, a).success(function () {
                        reloadAppointmentsInFiveSeconds();
                    }).error(function (error) {
                        toastService.error(t.error_create_appointment, error);
                    });
                });
            });
        };

        $scope.createRecurrentAppointment = function () {
            var appointment = {
                recurrence: "daily",
                recurrenceOptions: {
                    hasEndDate: false,
                    isEveryXthDay: false
                },
                link: {
                    linkType: "none"
                }
            };
            $translate(["create_recurrent_appointment", "error_create_recurrent_appointment"]).then(function(t) {
                openRecurrentAppointmentDialog(t.create_recurrent_appointment, appointment, function (a) {
                    $http.post("/api/appointments/" + tenantService.getTenant().id, a).success(function () {
                        reloadAppointmentsInFiveSeconds();
                    }).error(function (error) {
                        toastService.error(t.error_create_recurrent_appointment, error);
                    });
                });
            });
        };

        $scope.import = function() {
            $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/views/calendar/importDialog.min.html',
                controller: 'AppointmentImportDialogController',
                size: 'lg',
            });
        };

        function reloadAppointmentsInFiveSeconds() {
            $timeout(function () {
                monthOffset = 0;
                $scope.appointments = [];
                $scope.nextPage();
            }, 5000);
        };

        function openAppointmentDialog(subject, appointment, func) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/views/calendar/appointmentDialog.min.html',
                controller: 'AppointmentDialogController',
                size: 'lg',
                resolve: {
                    subject: function () {
                        return subject;
                    },
                    appointment: function () {
                        return angular.copy(appointment);
                    },
                }
            });

            modalInstance.result.then(function (a) {
                func(a);
            });
        };

        function openRecurrentAppointmentDialog(subject, appointment, func) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/views/calendar/recurrentAppointmentDialog.min.html',
                controller: 'AppointmentDialogController',
                size: 'lg',
                resolve: {
                    subject: function () {
                        return subject;
                    },
                    appointment: function() {
                        return angular.copy(appointment);
                    },
                }
            });

            modalInstance.result.then(function (a) {
                func(a);
            });
        };

        $rootScope.$on("tenantChanged", function () {
            $scope.appointments = [];
            monthOffset = 0;
            $scope.nextPage();
        });

        $rootScope.$on("logout", function() { $scope.appointments = []; });
    }]);

    angular.module("app").directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);

    angular.module("app").controller("AppointmentImportDialogController", ["$scope", "$modalInstance", "$http", "tenantService", "dialogService", function($scope, $modalInstance, $http, tenantService, dialogService) {
        $scope.isImportEnabled = false;
        $scope.isUploadEnabled = false;
        $scope.currentStep = 0;
        var id = 0;

        $scope.options = { delimiter: "," };

        $http.get("/api/appointment/import/start", { cache: false }).success(function(data) {
            id = data.id;
        });

        $http.get("/api/appointment/import/encodings").success(function(data) {
            $scope.encodings = data;
            $scope.options.encoding = "utf-8";
        });

        $scope.$watch("csvFile", function() {
            if (!$scope.csvFile) {
                return;
            }
            $scope.csvFileName = $scope.csvFile.name;
            $scope.isUploadEnabled = true;
        });

        $scope.goToStep = function(step) {
            if ($scope.currentStep < step) {
                return;
            }
            $scope.currentStep = step;
        };

        $scope.upload = function () {
            if (!$scope.csvFile) {
                return;
            }

            var fd = new FormData();
            fd.append('file', $scope.csvFile);
            $http.post("/api/appointment/import/" + id + "/file", fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            }).success(function () {
                $http.get("/api/appointment/import/" + id + "/encoding", { cache: false }).success(function(e) {
                    $scope.options.encoding = e;
                });

                $scope.currentStep = 1;
            });
        };

        $scope.goToMapping = function () {
            $http.post("/api/appointment/import/" + id + "/columns", $scope.options).success(function (data) {
                $scope.columns = data;
                $scope.currentStep = 2;
            });
        };

        $scope.goToPreview = function () {
            $http.post("/api/appointment/import/" + id + "/preview", $scope.options).success(function(data) {
                $scope.preview = data;
                $scope.currentStep = 3;
            });
        };

        $scope.import = function () {
            $http.post("/api/appointment/import/"+ tenantService.getTenant().id + "/" + id + "/import", $scope.options).success(function () {
                $modalInstance.close();
            });
        };

        $scope.back = function() {
            if ($scope.currentStep > 0) {
                $scope.currentStep--;
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);

    angular.module("app").controller("AppointmentDialogController", ["$filter", "$scope", "$modalInstance", "$http", "tenantService", "dialogService", "subject", "appointment", "$translate", function ($filter, $scope, $modalInstance, $http, tenantService, dialogService, subject, appointment, $translate) {
        $scope.subject = subject;
        $scope.appointment = appointment;
        $scope.times = [];

        for (var i = 0; i < 24; i++) {
            var hour = i + ":";
            if (hour.length < 3) {
                hour = "0" + hour;
            }

            for (var m = 0; m < 60; m+=5) {
                var minute = m;
                if (m < 10) {
                    minute = "0" + minute;
                }

                $scope.times.push(hour + minute);
            }
        }

        if ($scope.appointment.startDate) {
            $scope.appointment.startDatePicker = $scope.appointment.startDate;
        }
        if ($scope.appointment.endDate) {
            $scope.appointment.endDatePicker = $scope.appointment.endDate;
        }
        if ($scope.appointment.recurrenceOptions && $scope.appointment.recurrenceOptions.endDate) {
            $scope.appointment.recurrenceOptions.endDatePicker = $scope.appointment.recurrenceOptions.endDate;
        }

        $scope.$watch("appointment.startTime", updateDuration);
        $scope.$watch("appointment.endTime", updateDuration);
        $scope.$watch("appointment.startDatePicker", updateDuration);
        $scope.$watch("appointment.endDatePicker", updateDuration);

        function updateDuration() {
            $translate(["hours", "minutes", "days"]).then(function(t) {
                if (appointment.startDatePicker && appointment.endDatePicker && appointment.startTime && appointment.endTime) {
                    var s = appointment.startTime.split(':');
                    var e = appointment.endTime.split(':');

                    appointment.startDate = $filter('date')(appointment.startDatePicker, 'yyyy-MM-dd') + "T00:00:00.000Z";
                    appointment.endDate = $filter('date')(appointment.endDatePicker, 'yyyy-MM-dd') + "T00:00:00.000Z";

                    $scope.start = moment(appointment.startDate);
                    $scope.start.add(parseInt(s[0]), 'hours');
                    $scope.start.add(parseInt(s[1]), 'minutes');
                    $scope.end = moment(appointment.endDate);
                    $scope.end.add(parseInt(e[0]), 'hours');
                    $scope.end.add(parseInt(e[1]), 'minutes');

                    var minutes = $scope.end.diff($scope.start, 'minutes');
                    if (minutes < 60) {
                        $scope.duration = toReadableNumber(minutes) + " " + t.minutes;
                        return;
                    }
                    var hours = $scope.end.diff($scope.start, 'hours', true);
                    if (hours < 24) {
                        $scope.duration = toReadableNumber(hours) + " " + t.hours;
                        return;
                    }
                    var days = $scope.end.diff($scope.start, 'days', true);
                    $scope.duration = toReadableNumber(days) + " " + t.days;
                }
            });
        };

        function toReadableNumber(number) {
            var n = number.toFixed(2);
            if (n.charAt(n.length - 1) == "0") {
                n = n.substring(0, n.length - 1);
                if (n.charAt(n.length - 1) == "0") {
                    n = n.substring(0, n.length - 2);
                }
            }
            return n;
        };

        $scope.$watch("appointment.recurrenceOptions.endDatePicker", function() {
            if (appointment.recurrenceOptions.endDatePicker) {
                var d = $filter('date')(appointment.recurrenceOptions.endDatePicker, 'yyyy-MM-dd') + "T00:00:00.000Z";
                $scope.appointment.recurrenceOptions.endDate = d;
            }
        });

        $scope.searchCategories = function (searchText) {
            var url = String.format("/api/calendar/categories/{1}?search={0}", searchText, tenantService.getTenant().id);
            return $http.get(url, { cache: false });
        };

        $scope.openStartDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.startDateOpened = true;
        };

        $scope.openEndDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.endDateOpened = true;
        };

        $scope.openEndRecurrenceDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.endRecurrenceDateOpened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.getPages = function (value) {
            var url = String.format("/api/page/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, {cache:false}).then(function (response) {
                return response.data;
            });
        };

        $scope.browsePage = function () {
            var p = null;
            if ($scope.appointment.link) {
                p = $scope.appointment.link.pageLink;
            }
            dialogService.selectPageDialog(p, function (page) {
                if (!$scope.appointment.link) {
                    $scope.appointment.link = {};
                }
                if (!page) {
                    return;
                }
                page.children = [];
                $scope.appointment.link.pageLink = page;
            });
        };

        $scope.ok = function () {
            $modalInstance.close($scope.appointment);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());