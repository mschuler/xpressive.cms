using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Cms.Aggregates.Pages;
using Xunit;
using Xunit.Abstractions;

namespace Cms.Aggregates.Tests
{
    public class Given_a_disabled_page_token_service
    {
        private readonly ITestOutputHelper _outputWriter;

        public Given_a_disabled_page_token_service(ITestOutputHelper outputWriter)
        {
            _outputWriter = outputWriter;
        }

        [Fact]
        public void When_generating_thousand_ids_then_they_are_unique()
        {
            var service = new DisabledPageTokenService();
            var tokens = new HashSet<string>(StringComparer.Ordinal);
            var regex = new Regex("^[a-zA-Z0-9]+$", RegexOptions.Compiled);

            for (var i = 0; i < 1000; i++)
            {
                var token = service.GetToken(1);

                _outputWriter.WriteLine(token);
                _outputWriter.WriteLine("");
                Assert.False(tokens.Contains(token));
                Assert.True(regex.IsMatch(token));
                Assert.True(service.IsValidToken(1, token));
            }
        }
    }
}