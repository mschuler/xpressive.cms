namespace Cms.WebNew.Controllers.PageRendering
{
    public class WriteGeneratorMetatag : IFilter<RenderPageMessage>
    {
        public bool Execute(RenderPageMessage msg)
        {
            if (!msg.IsHtml)
            {
                return true;
            }

            msg.Content = msg.Content.Replace("</title>", "</title>\n<meta name=\"generator\" value=\"xpressive.cms\"/>");
            return true;
        }
    }
}