using System;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Forms
{
    internal class FormDuplicatedEventTranslator : IEventTranslator
    {
        private readonly IFormRepository _repository;

        public FormDuplicatedEventTranslator(IFormRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof (FormDuplicated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Formular dupliziert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FormDuplicated) @event;
            var form = _repository.GetEvenIfDeleted(e.FormIdToDuplicate);
            var oldName = string.Format("{0} (Nr. {1})", form.Name, form.PublicId);
            var newName = string.Format("{0} (Nr. {1})", e.Name, e.PublicId);
            return string.Format("Das Formular <strong>{0}</strong> wurde neu aus Formular <strong>{1}</strong> erstellt.", newName, oldName);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "form";
        }
    }
}