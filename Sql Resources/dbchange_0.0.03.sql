begin transaction

alter table Page alter column ParentId int null;

insert into Version (Major, Minor, Revision) values (0, 0, 3);

commit transaction