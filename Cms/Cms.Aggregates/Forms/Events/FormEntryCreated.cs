﻿using System;
using System.Collections.Generic;

namespace Cms.Aggregates.Forms.Events
{
    public class FormEntryCreated : EventBase<Form>
    {
        public ulong FormEntryId { get; set; }

        public DateTime Timestamp { get; set; }

        public string IpAddress { get; set; }

        public bool IsSpam { get; set; }

        public Dictionary<string, string> Values { get; set; }
    }
}