using Cms.Aggregates.Security;

namespace Cms.Aggregates.Files.Queries
{
    [RequiredPermission("file", PermissionLevel.CanSee)]
    public class GetFilesWithoutDirectory : QueryBase { }
}