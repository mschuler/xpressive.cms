using System.Linq;
using Cms.Aggregates.Files;

namespace Cms.Aggregates.FileDirectories
{
    internal class FileDirectoryRepository : RepositoryBase<FileDirectory>
    {
        private readonly IFileRepository _fileRepository;

        public FileDirectoryRepository(IFileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }

        public override void DeleteAllByTenantId(ulong tenantId)
        {
            var directories = GetAll().Where(d => d.TenantId == tenantId).ToList();

            foreach (var directory in directories)
            {
                var files = _fileRepository.GetByDirectory(directory.Id).ToList();
                files.ForEach(f => _fileRepository.Delete(f.Id));

                Delete(directory.Id);
            }
        }
    }
}