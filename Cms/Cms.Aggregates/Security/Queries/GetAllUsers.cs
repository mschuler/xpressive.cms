namespace Cms.Aggregates.Security.Queries
{
    [RequiredPermission("user", PermissionLevel.CanSee)]
    public class GetAllUsers : QueryBase { }
}