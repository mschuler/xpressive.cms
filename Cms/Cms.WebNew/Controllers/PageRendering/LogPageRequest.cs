using System;
using log4net;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class LogPageRequest : IFilter<RenderPageMessage>
    {
        private static readonly ILog _requestLog = LogManager.GetLogger("REQUESTS");

        public bool Execute(RenderPageMessage msg)
        {
            if (_requestLog.IsInfoEnabled)
            {
                try
                {
                    _requestLog.InfoFormat(
                        "IP=({0}) Bot={3} Url=({1}) Agent=({2})",
                        msg.Request.GetClientIpAddress(),
                        msg.Request.RequestUri.PathAndQuery,
                        msg.Request.Headers.UserAgent,
                        msg.IsKnownBot);
                }
                catch (Exception e)
                {
                    _requestLog.Error(e.Message, e);
                }
            }

            return true;
        }
    }
}