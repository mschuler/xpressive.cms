﻿using System.Collections.Generic;
using Cms.SharedKernel;

namespace Cms.Aggregates.DynamicObjects
{
    public interface IDynamicObjectDtoService
    {
        object GetDtoWithFields(DynamicObject obj, DynamicObjectDefinition definition, IObjectFieldContentTypeConverterService contentTypeConverter);

        object GetDtoWithOnlySortableFields(DynamicObject obj, DynamicObjectDefinition definition, ulong tenantId, IObjectFieldContentTypeConverterService contentTypeConverter);

        object GetDtoWithoutFields(DynamicObject obj, DynamicObjectDefinition definition);

        IDynamicObjectDtoForPublicApi GetDtoForPublicApi(DynamicObject obj, DynamicObjectDefinition definition, IObjectFieldContentTypeConverterService contentTypeConverter);
    }

    public interface IDynamicObjectDtoForPublicApi
    {
        string Id { get; }
        string Title { get; }
        int Nr { get; }
        IDictionary<string, object> Fields { get; }
        string DefinitionId { get; }
        string DefinitionName { get; }
    }
}