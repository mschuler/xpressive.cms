using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Cms.EventSourcing.Contracts.Commands;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Contracts.Queries;
using Cms.SharedKernel;

namespace Cms.EventSourcing
{
    public static class RedirectToWhen
    {
        private static readonly MethodInfo _internalPreserveStackTraceMethod =
            typeof(Exception).GetMethod("InternalPreserveStackTrace", BindingFlags.Instance | BindingFlags.NonPublic);

        private static class Cache
        {
            private static readonly object _lock = new object();
            private static readonly IDictionary<Type, IDictionary<Type, MethodInfo>> _dictionary;

            static Cache()
            {
                _dictionary = new Dictionary<Type, IDictionary<Type, MethodInfo>>();
            }

            public static bool HasMethod(Type type, Type parameterType)
            {
                var dictionary = GetMethodsFromType(type);
                return dictionary.ContainsKey(parameterType);
            }

            public static bool TryGetValue(object aggregate, Type type, out MethodInfo methodInfo)
            {
                var aggregateType = aggregate.GetType();
                var dictionary = GetMethodsFromType(aggregateType);
                return dictionary.TryGetValue(type, out methodInfo);
            }

            private static IDictionary<Type, MethodInfo> GetMethodsFromType(Type type)
            {
                IDictionary<Type, MethodInfo> dictionary;
                if (!_dictionary.TryGetValue(type, out dictionary))
                {
                    lock (_lock)
                    {
                        if (!_dictionary.TryGetValue(type, out dictionary))
                        {
                            var methods = type.GetMethods(
                                BindingFlags.Public |
                                BindingFlags.NonPublic |
                                BindingFlags.Instance |
                                BindingFlags.FlattenHierarchy);

                            var when = methods.Where(m =>
                                m.Name.Equals("When", StringComparison.Ordinal) &&
                                m.GetParameters().Length >= 1).ToList();

                            dictionary = when.ToDictionary(m => m.GetParameters().First().ParameterType, m => m);
                            _dictionary.Add(type, dictionary);
                        }
                    }
                }

                return dictionary;
            }
        }

        public static bool IsMethodAvailable(Type handlerType, Type parameterType)
        {
            return Cache.HasMethod(handlerType, parameterType);
        }

        /// <summary>
        /// Calls <see cref="IEventHandler"/>.When(SpecificEvent @event)
        /// </summary>
        /// <returns>Returns true if the Method exists.</returns>
        [DebuggerNonUserCode]
        public static bool InvokeEventHandler(IEventHandler instance, IEvent @event)
        {
            var info = TryGetMethodInfo(instance, @event.GetType(), false);
            if (info == null)
            {
                return false;
            }
            try
            {
                info.Invoke(instance, new object[] { @event });
                return true;
            }
            catch (TargetInvocationException ex)
            {
                if (null != _internalPreserveStackTraceMethod)
                {
                    _internalPreserveStackTraceMethod.Invoke(ex.InnerException, new object[0]);
                }
                throw ex.InnerException.WithGuid();
            }
        }

        /// <summary>
        /// Calls <see cref="ICommandHandler"/>.When(SpecificCommand command, Action&lt;IEvent&gt; eventHandler)
        /// </summary>
        /// <returns>Returns true if the Method exists.</returns>
        [DebuggerNonUserCode]
        public static bool InvokeCommandHandler(ICommandHandler instance, ICommand command, Action<IEvent> eventHandler)
        {
            var info = TryGetMethodInfo(instance, command.GetType(), false);
            if (info == null)
            {
                return false;
            }
            try
            {
                info.Invoke(instance, new object[] { command, eventHandler });
                return true;
            }
            catch (TargetInvocationException ex)
            {
                if (null != _internalPreserveStackTraceMethod)
                {
                    _internalPreserveStackTraceMethod.Invoke(ex.InnerException, new object[0]);
                }
                throw ex.InnerException.WithGuid();
            }
        }

        /// <summary>
        /// Calls <see cref="IQueryHandler"/>.When(SpecificQuery query)
        /// </summary>
        /// <returns>Returns true if the Method exists.</returns>
        [DebuggerNonUserCode]
        public static IEnumerable<object> InvokeQueryHandler(IQueryHandler instance, IQuery query)
        {
            var info = TryGetMethodInfo(instance, query.GetType(), false);
            if (info == null)
            {
                return null;
            }
            try
            {
                return (IEnumerable<object>)info.Invoke(instance, new object[] { query });
            }
            catch (TargetInvocationException ex)
            {
                if (_internalPreserveStackTraceMethod != null)
                {
                    _internalPreserveStackTraceMethod.Invoke(ex.InnerException, new object[0]);
                }
                throw ex.InnerException.WithGuid();
            }
        }

        [DebuggerNonUserCode]
        private static MethodInfo TryGetMethodInfo(object instance, Type parameterType, bool throwException)
        {
            if (instance == null)
            {
                throw new ArgumentNullException("instance").WithGuid();
            }

            MethodInfo info;
            if (!Cache.TryGetValue(instance, parameterType, out info))
            {
                if (throwException)
                {
                    var s = string.Format("Failed to locate {0}.When({1})", instance.GetType().Name, parameterType.Name);
                    throw new InvalidOperationException(s).WithGuid();
                }
            }
            return info;
        }
    }
}