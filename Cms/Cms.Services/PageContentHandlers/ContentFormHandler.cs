﻿using System;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Pages;
using Cms.Services.Contracts;

namespace Cms.Services.PageContentHandlers
{
    internal class ContentFormHandler : IContentHandler
    {
        private readonly IFormRepository _repository;

        public ContentFormHandler(IFormRepository repository)
        {
            _repository = repository;
        }

        public bool CanHandle(string pageContentType)
        {
            return pageContentType.Equals("form", StringComparison.OrdinalIgnoreCase);
        }

        public string ContentType
        {
            get { return "Form"; }
        }

        public string GetImage()
        {
            return "fa fa-comments-o";
        }

        public string Render(PageContentValueBase value)
        {
            var formValue = value as PageContentFormValue;

            if (formValue == null)
            {
                return string.Empty;
            }

            ulong formId;
            Form form;
            if (ulong.TryParse(formValue.FormId, out formId) && _repository.TryGet(formId, out form))
            {
                return form.Template;
            }

            return string.Empty;
        }

        public object GetValueForInterpreter(PageContentValueBase value)
        {
            var formValue = value as PageContentFormValue;

            if (formValue == null)
            {
                return null;
            }

            ulong formId;
            Form form;
            if (ulong.TryParse(formValue.FormId, out formId) && _repository.TryGet(formId, out form))
            {
                return form;
            }

            return null;
        }
    }
}