using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal abstract class DatabaseSqlMigration : DatabaseMigrationBase
    {
        protected DatabaseSqlMigration(int id)
            : base(id) { }

        public override void Execute(Database database)
        {
            database.Execute(GetSql());
        }

        protected abstract string GetSql();
    }
}