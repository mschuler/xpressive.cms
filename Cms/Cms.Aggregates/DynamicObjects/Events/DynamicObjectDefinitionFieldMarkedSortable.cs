﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDefinitionFieldMarkedSortable : EventBase<DynamicObjectDefinition>
    {
        public string Name { get; set; }

        public bool IsSortable { get; set; }
    }
}