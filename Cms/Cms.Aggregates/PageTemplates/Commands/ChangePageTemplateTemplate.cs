using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.PageTemplates.Commands
{
    [RequiredPermission("pagetemplate", PermissionLevel.CanChange)]
    public class ChangePageTemplateTemplate : CommandBase
    {
        [Required(AllowEmptyStrings = true)]
        public string Template { get; set; }
    }
}