using System;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.Websites;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Pages
{
    internal class PageTitleChangedEventTranslator : IEventTranslator
    {
        private readonly IPageRepository _repository;
        private readonly IWebsiteRepository _websiteRepository;

        public PageTitleChangedEventTranslator(IPageRepository repository, IWebsiteRepository websiteRepository)
        {
            _repository = repository;
            _websiteRepository = websiteRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageTitleChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seitentitel ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageTitleChanged)@event;
            var page = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Der Titel der Seite <strong>{0}</strong> wurde in <strong>{1}</strong> ge�ndert.", page.Fullname, e.Title);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var page = _repository.GetEvenIfDeleted(@event.AggregateId);
            return _websiteRepository.GetEvenIfDeleted(page.WebsiteId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "page";
        }
    }
}