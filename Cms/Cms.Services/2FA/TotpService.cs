﻿using System.Security.Cryptography;
using Base32;
using Cms.SharedKernel;
using OtpSharp;

namespace Cms.Services._2FA
{
    internal class TotpService : ITotpService
    {
        public ITotp Generate(string username)
        {
            var secretKey = new byte[20];

            using (var generator = new RNGCryptoServiceProvider())
            {
                generator.GetBytes(secretKey);
            }

            username = "xpressive.cms:" + username;

            var url = KeyUrl.GetTotpUrl(secretKey, username) + "&issuer=xpressive.cms";
            var secret = Base32Encoder.Encode(secretKey);

            return new SecretAndUrlPair
            {
                Secret = secretKey,
                SecretBase32 = secret,
                Url = url
            };
        }

        public bool Verify(byte[] secret, string code)
        {
            var totp = new Totp(secret);

            long window;
            return totp.VerifyTotp(code, out window, VerificationWindow.RfcSpecifiedNetworkDelay);
        }

        private class SecretAndUrlPair : ITotp
        {
            public byte[] Secret { get; set; }
            public string SecretBase32 { get; set; }
            public string Url { get; set; }
        }
    }
}
