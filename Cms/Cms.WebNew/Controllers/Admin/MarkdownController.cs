﻿using System;
using System.Collections.Specialized;
using System.Web.Http;
using Cms.TemplateEngine;
using log4net;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class MarkdownController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(MarkdownController));

        [HttpPost, Route("api/markdown")]
        public string Convert(MarkdownDto markdown)
        {
            var md = new MarkdownDeep.Markdown();
            //md.SafeMode = true;
            //md.ExtraMode = true;

            if (markdown == null || string.IsNullOrEmpty(markdown.Content))
            {
                return string.Empty;
            }

            var result = RunTemplateEngine(markdown.Content);
            result = md.Transform(result);
            return result;
        }

        private string RunTemplateEngine(string html)
        {
            try
            {
                TemplateStateMap.Set("Visitor.IpAddress", Request.GetClientIpAddress());
                TemplateStateMap.Set("QueryString", new NameValueCollection());

                var engine = new Engine();
                html = engine.Generate(html);
                return html;
            }
            catch (Exception e)
            {
                _log.Error(e.Message, e);
                return html;
            }
        }

        public class MarkdownDto
        {
            public string Content { get; set; }
        }
    }
}