﻿using System;
using System.Text;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class YouTubeInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name
        {
            get { return "YouTube and Vimeo"; }
        }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function)
            {
                if (tree.Value.Equals("youtube", StringComparison.OrdinalIgnoreCase) &&
                    tree.Children.Count >= 1 && tree.Children[0].ParameterType == ParameterType.String)
                {
                    var fileId = (string)tree.Children[0].Result;

                    int width = 1280;
                    int height = 745;

                    if (tree.Children.Count >= 3 &&
                        tree.Children[1].ParameterType == ParameterType.Integer &&
                        tree.Children[2].ParameterType == ParameterType.Integer)
                    {
                        width = (int)tree.Children[1].Result;
                        height = (int)tree.Children[2].Result;
                    }

                    tree.Result = GenerateYouTubeTag(fileId, width, height);
                }
                else if (tree.Value.Equals("vimeo", StringComparison.OrdinalIgnoreCase) &&
                    tree.Children.Count >= 1 && tree.Children[0].ParameterType == ParameterType.Integer)
                {
                    var id = (int)tree.Children[0].Result;

                    int width = 400;
                    int height = 225;
                    string color = string.Empty;

                    if (tree.Children.Count >= 3 &&
                        tree.Children[1].ParameterType == ParameterType.Integer &&
                        tree.Children[2].ParameterType == ParameterType.Integer)
                    {
                        width = (int)tree.Children[1].Result;
                        height = (int)tree.Children[2].Result;
                    }
                    if (tree.Children.Count >= 4 &&
                        tree.Children[3].ParameterType == ParameterType.String)
                    {
                        color = (string)tree.Children[3].Result;
                    }

                    tree.Result = GenerateVimeoTag(id, width, height, color);
                }
            }
        }

        private static string GenerateVimeoTag(int id, int width, int height, string color)
        {
            var sb = new StringBuilder();
            sb.Append("<iframe src=\"https://player.vimeo.com/video/");
            sb.Append(id);
            sb.Append("?title=0&amp;byline=0&amp;portrait=0");

            if (!string.IsNullOrEmpty(color))
            {
                sb.AppendFormat("&amp;color={0}", color);
            }

            sb.AppendFormat("\" width=\"{0}\" height=\"{1}\" ", width, height);
            sb.Append("frameborder=\"0\"></iframe>");

            return sb.ToString();
        }

        private static string GenerateYouTubeTag(string id, int width, int height)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("<object width=\"{0}\" height=\"{1}\">", width, height);
            sb.Append("<param name=\"movie\" value=\"https://www.youtube.com/v/");
            sb.Append(id);
            sb.Append("?fs=1&amp;hl=en_US\"></param>");
            sb.Append("<param name=\"allowFullScreen\" value=\"true\"></param>");
            sb.AppendLine("<param name=\"allowscriptaccess\" value=\"always\"></param>");
            sb.Append("<embed src=\"https://www.youtube.com/v/");
            sb.Append(id);
            sb.Append("?fs=1&amp;hl=en_US\" type=\"application/x-shockwave-flash\"");
            sb.Append(" allowscriptaccess=\"always\" allowfullscreen=\"true\"");
            sb.AppendFormat(" width=\"{0}\" height=\"{1}\"></embed>", width, height);
            sb.Append("</object>");

            return sb.ToString();
        }
    }
}
