using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0010_remove_ip_column_from_tracking_tables : DatabaseMigrationBase
    {
        public M0010_remove_ip_column_from_tracking_tables() : base(10) { }

        public override void Execute(Database database)
        {
            database.Execute("if (select COL_LENGTH('FileTracking', 'IpAddress')) is not null begin alter table FileTracking drop column IpAddress end");

            database.Execute("if (select COL_LENGTH('PageTracking', 'IpAddress')) is not null begin alter table PageTracking drop column IpAddress end");
        }
    }
}