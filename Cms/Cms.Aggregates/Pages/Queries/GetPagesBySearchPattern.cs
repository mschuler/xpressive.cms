using System;
using System.Linq;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Pages.Queries
{
    [RequiredPermission("page", PermissionLevel.CanSee)]
    public class GetPagesBySearchPattern : QueryBase
    {
        public string SearchPattern { get; set; }

        internal bool IsMatch(PageBase page)
        {
            var p = page as Page;
            var parts = SearchPattern.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return parts.All(a =>
                page.Name.ToUpperInvariant().Contains(a.ToUpperInvariant()) ||
                (p != null && p.PublicId.ToString("D").Contains(a)));
        }
    }
}