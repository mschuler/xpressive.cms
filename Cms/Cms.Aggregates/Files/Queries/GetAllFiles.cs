using Cms.Aggregates.Security;

namespace Cms.Aggregates.Files.Queries
{
    [RequiredPermission("file", PermissionLevel.CanSee)]
    public class GetAllFiles : QueryBase
    {
        public ulong DirectoryId { get; set; }
    }
}