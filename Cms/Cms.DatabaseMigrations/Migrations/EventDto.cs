using NPoco;

namespace Cms.DatabaseMigrations.Migrations
{
    [TableName("Event")]
    [PrimaryKey("Version", AutoIncrement = false)]
    internal class EventDto
    {
        public long Transaction { get; set; }
        public long Version { get; set; }
        public string AggregateType { get; set; }
        public long AggregateId { get; set; }
        public string Event { get; set; }
        public long UserId { get; set; }
        public string Data { get; set; }
        public bool IsDeleted { get; set; }
    }
}