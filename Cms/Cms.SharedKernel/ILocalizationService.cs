﻿namespace Cms.SharedKernel
{
    public interface ILocalizationService
    {
        string Get(string language, string key);
    }
}
