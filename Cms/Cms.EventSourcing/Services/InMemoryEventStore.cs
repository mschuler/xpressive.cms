using System;
using System.Collections.Generic;
using System.Linq;
using Cms.EventSourcing.Contracts.Events;
using UniqueIdGenerator.Net;

namespace Cms.EventSourcing.Services
{
    public sealed class InMemoryEventStore : IEventStore
    {
        private static readonly Generator _transactionIdGenerator = new Generator(2, new DateTime(2014, 10, 1));

        private readonly IList<PersistedEvent> _eventStore = new List<PersistedEvent>();

        public void Persist(IEnumerable<IEvent> events)
        {
            var transactionId = _transactionIdGenerator.NextLong();

            foreach (var @event in events)
            {
                _eventStore.Add(new PersistedEvent
                {
                    TransactionId = transactionId,
                    Event = @event,
                });
            }
        }

        public IList<IEvent> Fetch()
        {
            return _eventStore
                .Select(p => p.Event)
                .OrderBy(p => p.Version)
                .ToList();
        }

        public IEnumerable<IEvent> Fetch(int skip, int take)
        {
            return _eventStore
                .Select(p => p.Event)
                .OrderByDescending(p => p.Version)
                .Skip(skip)
                .Take(take);
        }

        public IEnumerable<IEvent> Fetch(ulong aggregateId)
        {
            return _eventStore.Select(p => p.Event).Where(e => e.AggregateId.Equals(aggregateId));
        }

        public IEnumerable<ulong> GetAggregateIds<T>()
        {
            var t = typeof(T);
            return _eventStore.Where(p => p.Event.AggregateType == t).Select(p => p.Event.AggregateId);
        }

        private class PersistedEvent
        {
            public ulong TransactionId { get; set; }
            public IEvent Event { get; set; }
        }
    }
}