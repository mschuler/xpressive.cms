using System.Net.Http;
using Cms.TemplateEngine;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class WriteTemplateStateMapVariables : IFilter<RenderPageMessage>
    {
        public bool Execute(RenderPageMessage msg)
        {
            TemplateStateMap.Set("CurrentWebsite", msg.Website);
            TemplateStateMap.Set(TemplateStateMap.CurrentObjectKey, msg.Page);
            TemplateStateMap.Set(TemplateStateMap.CurrentLoadedPage, msg.Page);
            TemplateStateMap.Set("QueryString", msg.Request.RequestUri.ParseQueryString());
            TemplateStateMap.Set("Visitor.IpAddress", msg.IpAddress);
            TemplateStateMap.Set("Cookies", msg.Cookies);
            return true;
        }
    }
}