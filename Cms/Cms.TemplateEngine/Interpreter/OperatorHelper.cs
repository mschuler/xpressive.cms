using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Cms.TemplateEngine.Interpreter
{
    public static class OperatorHelper
    {
        public static bool IsMatch(object firstValue, string @operator, object secondValue)
        {
            if (firstValue == null || secondValue == null)
            {
                if (@operator != "=" && @operator != "!=" && @operator != "isnull")
                {
                    return false;
                }
            }

            switch (@operator)
            {
                case "=":
                    return IsEqual(firstValue, secondValue);
                case "!=":
                    return IsNotEqual(firstValue, secondValue);
                case "in":
                    return IsIn(firstValue, secondValue);
                case "!in":
                    return IsNotIn(firstValue, secondValue);
                case "notin":
                    return IsNotIn(firstValue, secondValue);
                case "between":
                    return IsBetween(firstValue, secondValue);
                case "!between":
                    return IsNotBetween(firstValue, secondValue);
                case "notbetween":
                    return IsNotBetween(firstValue, secondValue);
                case "startswith":
                    return IsStartingWith(firstValue, secondValue);
                case "endswith":
                    return IsEndingWith(firstValue, secondValue);
                case "regex":
                    return IsRegexMatch(firstValue, secondValue);
                case ">":
                    return IsGreaterThen(firstValue, secondValue);
                case ">=":
                    return IsGreaterOrEqual(firstValue, secondValue);
                case "<":
                    return IsLessThen(firstValue, secondValue);
                case "<=":
                    return IsLessOrEqual(firstValue, secondValue);
                case "isnull":
                    return IsNull(firstValue, secondValue);
                default:
                    throw new NotSupportedException(@operator);
            }
        }

        private static bool IsNull(object firstValue, object secondValue)
        {
            var second = secondValue as string;
            if (second == null)
            {
                return false;
            }

            if (firstValue == null)
            {
                return "true".Equals(second, StringComparison.OrdinalIgnoreCase);
            }

            return "false".Equals(second, StringComparison.OrdinalIgnoreCase);
        }

        private static bool IsLessOrEqual(object firstValue, object secondValue)
        {
            var first = firstValue as IComparable;
            var second = secondValue as IComparable;

            if (firstValue is string && secondValue is string)
            {
                decimal firstNumber;
                decimal secondNumber;

                if (decimal.TryParse((string)firstValue, out firstNumber) &&
                    decimal.TryParse((string)secondValue, out secondNumber))
                {
                    return firstNumber <= secondNumber;
                }
            }

            if (firstValue is DateTime && secondValue is DateTime)
            {
                return ((DateTime)firstValue) <= ((DateTime)secondValue);
            }

            return first != null && second != null && first.CompareTo(second) <= 0;
        }

        private static bool IsLessThen(object firstValue, object secondValue)
        {
            var first = firstValue as IComparable;
            var second = secondValue as IComparable;

            if (firstValue is string && secondValue is string)
            {
                decimal firstNumber;
                decimal secondNumber;

                if (decimal.TryParse((string)firstValue, out firstNumber) &&
                    decimal.TryParse((string)secondValue, out secondNumber))
                {
                    return firstNumber < secondNumber;
                }
            }

            if (firstValue is DateTime && secondValue is DateTime)
            {
                return ((DateTime)firstValue) < ((DateTime)secondValue);
            }

            return first != null && second != null && first.CompareTo(second) < 0;
        }

        private static bool IsGreaterOrEqual(object firstValue, object secondValue)
        {
            var first = firstValue as IComparable;
            var second = secondValue as IComparable;

            if (firstValue is string && secondValue is string)
            {
                decimal firstNumber;
                decimal secondNumber;

                if (decimal.TryParse((string)firstValue, out firstNumber) &&
                    decimal.TryParse((string)secondValue, out secondNumber))
                {
                    return firstNumber >= secondNumber;
                }
            }

            if (firstValue is DateTime && secondValue is DateTime)
            {
                return ((DateTime)firstValue) >= ((DateTime)secondValue);
            }

            return first != null && second != null && first.CompareTo(second) >= 0;
        }

        private static bool IsGreaterThen(object firstValue, object secondValue)
        {
            var first = firstValue as IComparable;
            var second = secondValue as IComparable;

            if (firstValue is string && secondValue is string)
            {
                decimal firstNumber;
                decimal secondNumber;

                if (decimal.TryParse((string)firstValue, out firstNumber) &&
                    decimal.TryParse((string)secondValue, out secondNumber))
                {
                    return firstNumber > secondNumber;
                }
            }

            if (firstValue is DateTime && secondValue is DateTime)
            {
                return ((DateTime)firstValue) > ((DateTime)secondValue);
            }

            return first != null && second != null && first.CompareTo(second) > 0;
        }

        private static bool IsStartingWith(object firstValue, object secondValue)
        {
            var first = firstValue as string;
            var second = secondValue as string;

            if (first == null || second == null)
            {
                return false;
            }

            return first.StartsWith(second, StringComparison.OrdinalIgnoreCase);
        }

        private static bool IsEndingWith(object firstValue, object secondValue)
        {
            var first = firstValue as string;
            var second = secondValue as string;

            if (first == null || second == null)
            {
                return false;
            }

            return first.EndsWith(second, StringComparison.OrdinalIgnoreCase);
        }

        private static bool IsEqual(object firstValue, object secondValue)
        {
            if (firstValue == null && secondValue == null)
            {
                return true;
            }

            if (firstValue == null || secondValue == null)
            {
                return false;
            }

            if (firstValue is DateTime && secondValue is DateTime)
            {
                var first = (DateTime)firstValue;
                var second = (DateTime)secondValue;

                return first.ToString("g").Equals(second.ToString("g"), StringComparison.Ordinal);
            }

            if (firstValue is string || secondValue is string)
            {
                var first = firstValue.ToString();
                var second = secondValue.ToString();

                return first.Equals(second, StringComparison.OrdinalIgnoreCase);
            }

            return Equals(firstValue, secondValue);
        }

        private static bool IsNotEqual(object firstValue, object secondValue)
        {
            return !IsEqual(firstValue, secondValue);
        }

        private static bool IsIn(object firstValue, object secondValue)
        {
            var stringList = secondValue as string;
            var stringValue = Convert.ToString(firstValue);

            if (stringList != null)
            {
                var options = stringList.Split(new[] { ',' }, StringSplitOptions.None);
                return options.Contains(stringValue, StringComparer.OrdinalIgnoreCase);
            }

            return false;
        }

        private static bool IsNotIn(object firstValue, object secondValue)
        {
            return !IsIn(firstValue, secondValue);
        }

        private static bool IsBetween(object firstValue, object secondValue)
        {
            var s = secondValue as string;
            var first = Convert.ToDecimal(firstValue);

            if (s != null && s.IndexOf('|') > 0)
            {
                var options = s.Split(new[] { '|' }, StringSplitOptions.None);
                decimal o1, o2;

                if (options.Length == 2 && decimal.TryParse(options[0], out o1) && decimal.TryParse(options[1], out o2))
                {
                    return first >= o1 && first <= o2;
                }
            }

            return false;
        }

        private static bool IsNotBetween(object firstValue, object secondValue)
        {
            return !IsBetween(firstValue, secondValue);
        }

        private static bool IsRegexMatch(object firstValue, object secondValue)
        {
            var first = firstValue as string;
            var second = secondValue as string;

            if (first != null && second != null)
            {
                var regex = new Regex(second, RegexOptions.None, TimeSpan.FromSeconds(1));
                return regex.IsMatch(first);
            }

            return false;
        }
    }
}