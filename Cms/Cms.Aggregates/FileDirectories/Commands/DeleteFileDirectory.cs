using Cms.Aggregates.Security;

namespace Cms.Aggregates.FileDirectories.Commands
{
    [RequiredPermission("file", PermissionLevel.CanEverything)]
    public class DeleteFileDirectory : CommandBase { }
}