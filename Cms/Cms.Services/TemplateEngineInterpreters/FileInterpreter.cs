﻿using System;
using System.Linq;
using System.Text;
using Cms.Aggregates;
using Cms.Aggregates.Files;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class FileInterpreter : IInterpreter
    {
        private readonly IFileRepository _fileRepository;
        private readonly IContentTypeService _contentTypeService;
        private readonly IFileTrackingService _fileTrackingService;

        public FileInterpreter(IFileRepository fileRepository, IContentTypeService contentTypeService, IFileTrackingService fileTrackingService)
        {
            _fileRepository = fileRepository;
            _contentTypeService = contentTypeService;
            _fileTrackingService = fileTrackingService;
        }

        public int Priority { get; set; }

        public string Name
        {
            get { return "File"; }
        }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function &&
                tree.Value.Equals("file", StringComparison.OrdinalIgnoreCase))
            {
                if (tree.Children.Count >= 1 && tree.Children[0].ParameterType == ParameterType.Integer)
                {
                    var fileId = (int)tree.Children[0].Result;

                    tree.Result = _fileRepository.GetByPublicId(fileId);
                }
            }
            else if (tree.ItemType == TreeItemType.Attribute &&
                tree.LeftItem != null &&
                tree.LeftItem.Result is File)
            {
                var attribute = tree.Value.ToLowerInvariant();
                var file = (File)tree.LeftItem.Result;

                switch (attribute)
                {
                    case "url":
                        var extension = file.GetExtension().ToLowerInvariant();
                        var downloadUrl = file.ToDownloadUrl(true);
                        downloadUrl = extension.Equals("less") ? downloadUrl.Substring(0, downloadUrl.Length - 4) + "css" : downloadUrl;
                        downloadUrl = extension.Equals("sass") ? downloadUrl.Substring(0, downloadUrl.Length - 4) + "css" : downloadUrl;
                        downloadUrl = extension.Equals("scss") ? downloadUrl.Substring(0, downloadUrl.Length - 4) + "css" : downloadUrl;
                        tree.Result = downloadUrl;
                        break;
                    case "filename":
                    case "name":
                        tree.Result = file.Name;
                        break;
                    case "extension":
                        tree.Result = file.GetExtension();
                        break;
                    case "size":
                    case "length":
                        tree.Result = file.GetFileSizeLabel();
                        break;
                    case "imagetag":
                        tree.Result = GetImageTag(file);
                        break;
                    case "link":
                        tree.Result = BuildLink(file, file.Name);
                        break;
                    case "contenttype":
                        tree.Result = _contentTypeService.GetContentType(file.GetExtension());
                        break;
                    case "bytesize":
                        tree.Result = file.Versions.Last().Size;
                        break;
                    case "viewcount":
                        tree.Result = _fileTrackingService.GetTotalViewsPerFile(file.Id);
                        break;
                }
            }
        }

        private string GetImageTag(File file)
        {
            return string.Format("<img src=\"{0}\" alt=\"{1}\" /> ", file.ToDownloadUrl(true), file.Name);
        }

        private string BuildLink(File file, string label)
        {
            var sb = new StringBuilder();
            sb.Append("<a class=\"file ");
            sb.Append(file.GetExtension().ToLowerInvariant());
            sb.Append("\" href=\"");
            sb.Append(file.ToDownloadUrl(true));
            sb.Append("\" target=\"_blank\">");
            sb.Append(label);
            sb.Append("</a>");

            return sb.ToString();
        }
    }
}
