﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Tenants;

namespace Cms.Aggregates.FileDirectories.Queries
{
    internal class FileDirectoryQueryHandler : QueryHandlerBase
    {
        private readonly IRepository<FileDirectory> _repository;
        private readonly ITenantRepository _tenantRepository;

        public FileDirectoryQueryHandler(IRepository<FileDirectory> repository, ITenantRepository tenantRepository)
        {
            _repository = repository;
            _tenantRepository = tenantRepository;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetAllFileDirectoriesWithChildren),
                typeof (GetSharedFileDirectories)
            };
        }

        public IEnumerable<object> When(GetSharedFileDirectories query)
        {
            return _repository.GetAll()
                .Where(d => d.TenantId != query.TenantId)
                .Where(d => d.SharedForTenants.Contains(query.TenantId))
                .OrderBy(d => d.Name, StringComparer.OrdinalIgnoreCase)
                .Select(CreateDto);
        }

        public IEnumerable<object> When(GetAllFileDirectoriesWithChildren query)
        {
            return _repository
                .GetAll()
                .Where(d => d.TenantId.Equals(query.TenantId))
                .Where(d => d.Parent == null)
                .Where(d => d.FileStoreId == query.FileStoreId)
                .OrderBy(d => d.Name, StringComparer.OrdinalIgnoreCase)
                .Select(CreateDto);
        }

        public object CreateDto(FileDirectory directory)
        {
            var children = directory
                .Children
                .OrderBy(c => c.Name, StringComparer.OrdinalIgnoreCase)
                .Select(CreateDto).ToList();

            var tenants = new List<object>();
            foreach (var tenantId in directory.SharedForTenants)
            {
                Tenant tenant;
                if (_tenantRepository.TryGet(tenantId, out tenant))
                {
                    tenants.Add(new
                    {
                        Id = tenant.Id.ToJsonString(),
                        tenant.Name
                    });
                }
            }

            return new
            {
                Id = directory.Id.ToJsonString(),
                FileStoreId = directory.FileStoreId.ToJsonString(),
                directory.Name,
                Children = children,
                IsCollapsed = true,
                Shared = tenants
            };
        }
    }
}
