using System;
using System.Collections.Generic;
using System.Linq;
using Cms.EventSourcing;
using Cms.EventSourcing.Contracts.Queries;

namespace Cms.Aggregates
{
    public abstract class QueryHandlerBase : IQueryHandler
    {
        public bool CanHandle(IQuery query)
        {
            return GetQueryTypes().Contains(query.GetType());
        }

        public IEnumerable<object> Handle(IQuery query)
        {
            return RedirectToWhen.InvokeQueryHandler(this, query);
        }

        protected abstract Type[] GetQueryTypes();
    }
}