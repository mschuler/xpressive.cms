﻿using System;
using Cms.Aggregates.Pages;
using Cms.Services.Contracts;

namespace Cms.Services.PageContentHandlers
{
    internal class ContentHtmlHandler : IContentHandler
    {
        public bool CanHandle(string pageContentType)
        {
            return pageContentType.Equals("html", StringComparison.OrdinalIgnoreCase);
        }

        public string ContentType
        {
            get { return "Html"; }
        }

        public string GetImage()
        {
            return "fa fa-code";
        }

        public string Render(PageContentValueBase value)
        {
            var htmlValue = value as PageContentHtmlValue;
            if (htmlValue == null)
            {
                return string.Empty;
            }

            return htmlValue.Value;
        }

        public object GetValueForInterpreter(PageContentValueBase value)
        {
            return Render(value);
        }
    }
}