(function () {
    var app = angular.module("app");
    app.provider("userService", function() {
        var user = null;

        this.$get = ["$http", "$log", "$rootScope", "$location", "$q", "currentUser", function ($http, $log, $rootScope, $location, $q, currentUser) {

            user = currentUser;

            function setCurrentUser(u) {
                angular.module("app").value("currentUser", u);
                user = u;
                $rootScope.$broadcast("currentUserChanged", user);
            }

            return {
                logout: function() {
                    setCurrentUser(null);
                },
                lockout: function () {
                    // keep the user data.
                },
                currentUser: function() {
                    return user;
                },
                refreshLoginData: function () {
                    var deferred = $q.defer();
                    $http.get("/api/user/current", { cache: false })
                        .success(function (data) {
                            setCurrentUser(data);
                            deferred.resolve(data);
                        })
                        .error(function (err) {
                            setCurrentUser(null);
                            deferred.reject(err);
                        });
                    return deferred.promise;
                }
            };
        }];
    });
}());
(function () {
    angular.module('app').controller("AuthenticationController", ["$rootScope", "$scope", "$http", "$q", "$location", "$log", "localStorageService", "authService", "userService", "tenantService", function ($rootScope, $scope, $http, $q, $location, $log, localStorageService, authService, userService, tenantService) {
        $scope.loginForm = {
            username: "",
            password: ""
        };

        $scope.isError = false;

        function logout() {
            $http.post("/api/user/logout").success(function() {
                $rootScope.$broadcast('logout');
                localStorageService.clearAll();
            });
        }

        $scope.currentUser = userService.currentUser();

        $scope.logout = function () {
            logout();
            userService.logout();
            $location.path("/login");
        };

        $scope.lock = function () {
            logout();
            userService.lockout();
            $location.path("/lock");
        };

        $scope.unlock = function() {
            if (!$scope.loginForm.password) {
                return;
            }
            $scope.loginForm.username = $scope.currentUser.email;
            $scope.tryLogin();
        };

        $scope.tryLogin = function () {
            $scope.isError = false;

            var dto = {
                userName: $scope.loginForm.username,
                password: $scope.loginForm.password
            };

            if ($scope.isTotp) {
                dto.totp = $scope.loginForm.totp;
            }

            authService.login(dto).then(function () {
                $scope.isError = false;

                var qT = tenantService.reloadTenants();
                var qU = userService.refreshLoginData();

                $q.all([qT, qU]).then(function() {
                    $location.path("/dashboard");
                });
            }, function (err) {
                if (err && err.error && err.error === "totp_enabled") {
                    $scope.isTotp = true;
                    $scope.loginForm.totp = "";
                } else if (err && err.error && err.error === "u2f_enabled") {
                    $scope.isU2F = true;
                    $scope.loginForm.u2fRequest = err.description;
                    $scope.loginForm.u2f = "";
                } else {
                    $scope.isError = true;
                }
            });
        };

        $rootScope.$on("currentUserChanged", function() {
            $scope.currentUser = userService.currentUser();
        });
    }]);

    angular.module('app').config(["$httpProvider", function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    }]);
}());
(function() {
    angular.module('app').factory('authInterceptorService', ["$q", "$injector", "$location", "localStorageService", "$log", function ($q, $injector, $location, localStorageService, $log) {
        var authInterceptorServiceFactory = {};
        var $http;

        var _request = function (config) {

            config.headers = config.headers || {};

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        }

        var _responseError = function (rejection) {
            var deferred = $q.defer();
            if (rejection.status === 401) {
                var authService = $injector.get('authService');
                authService.refreshToken().then(function (response) {
                    _retryHttpRequest(rejection.config, deferred);
                }, function () {
                    authService.logout();
                    var path = $location.path();
                    if (path != "/pages/lock-screen") {
                        $location.path('/login');
                    }
                    deferred.reject(rejection);
                });
            } else if (rejection.status === 500) {
                var path = "/500";
                if (typeof (rejection.data) === "string") {
                    var isGuid = /^[0-9a-f\-]+$/i;
                    if (isGuid.test(rejection.data)) {
                        path += "/" + rejection.data;
                    }
                }
                $location.path(path);
                deferred.reject(rejection);
            } else {
                deferred.reject(rejection);
            }
            return deferred.promise;
        }

        var _retryHttpRequest = function (config, deferred) {
            $http = $http || $injector.get('$http');
            $http(config).then(function (response) {
                deferred.resolve(response);
            }, function (response) {
                deferred.reject(response);
            });
        }

        authInterceptorServiceFactory.request = _request;
        authInterceptorServiceFactory.responseError = _responseError;

        return authInterceptorServiceFactory;
    }]);
}());
(function() {
    angular.module('app').factory('authService', ['$q', '$injector', 'localStorageService', '$log', function ($q, $injector, localStorageService, $log) {
        var $http;
        var authServiceFactory = {};

        var _authentication = {
            isAuth: false,
            userName: "",
            useRefreshTokens: false
        };

        var _saveRegistration = function (registration) {
            _logout();

            $http = $http || $injector.get('$http');
            return $http.post('/api/account/register', registration).then(function (response) {
                return response;
            });
        };

        var _login = function (loginData) {
            var password = loginData.password;
            password = CryptoJS.enc.Utf8.parse(password);
            password = CryptoJS.enc.Base64.stringify(password);
            var data = "grant_type=password&username=" + loginData.userName + "&password=" + password;

            if (loginData.totp) {
                data += "&totp=" + loginData.totp;
            }

            var deferred = $q.defer();
            $http = $http || $injector.get('$http');
            $http.post('/api/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
                localStorageService.set('authorizationData', {
                    token: response.access_token,
                    userName: loginData.userName,
                    useRefreshTokens: false,
                    expires: response[".expires"]
                });

                _authentication.isAuth = true;
                _authentication.userName = loginData.userName;
                _authentication.useRefreshTokens = loginData.useRefreshTokens;

                deferred.resolve(response);
            }).error(function (err) {
                _logout();
                deferred.reject(err);
            });

            return deferred.promise;

        };

        var _logout = function () {
            localStorageService.remove('authorizationData');

            _authentication.isAuth = false;
            _authentication.userName = "";
            _authentication.useRefreshTokens = false;

        };

        var _fillAuthData = function () {
            var authData = localStorageService.get('authorizationData');
            if (authData) {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
                _authentication.useRefreshTokens = authData.useRefreshTokens;
            }
        };

        authServiceFactory.saveRegistration = _saveRegistration;
        authServiceFactory.login = _login;
        authServiceFactory.logout = _logout;
        authServiceFactory.fillAuthData = _fillAuthData;
        authServiceFactory.authentication = _authentication;

        return authServiceFactory;
    }]);
}());
(function() {
    angular.module("app").controller("AppointmentController", ["$rootScope", "$scope", "$http", "$timeout", "$modal", "tenantService", "dialogService", "toastService", "$translate", function ($rootScope, $scope, $http, $timeout, $modal, tenantService, dialogService, toastService, $translate) {
        var monthOffset = 0;
        $scope.isBusy = false;
        $scope.appointments = [];
        $scope.selectedAppointment = {};

        $http.get("/api/permission/appointment/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.select = function(appointment) {
            $scope.selectedAppointment = appointment;
        }

        $scope.edit = function(appointment) {
            var url = String.format("/api/appointment/{1}/{0}", appointment.appointmentId, tenantService.getTenant().id);
            $http.get(url, { cache: false }).success(function (data) {
                if (appointment.isSingleOccurrence) {
                    $translate("edit_appointment").then(function(subject) {
                        openAppointmentDialog(subject, data, function (a) {
                            $http.post(url, a).success(function () {
                                reloadAppointmentsInFiveSeconds();
                            }).error(function (error) {
                                $translate("error_edit_appointment").then(function(msg) {
                                    toastService.error(msg, error);
                                });
                            });
                        });
                    });
                } else {
                    $translate("edit_recurrent_appointment").then(function(subject) {
                        openRecurrentAppointmentDialog(subject, data, function (a) {
                            $http.post(url, a).success(function () {
                                reloadAppointmentsInFiveSeconds();
                            }).error(function (error) {
                                $translate("error_edit_recurrent_appointment").then(function (msg) {
                                    toastService.error(msg, error);
                                });
                            });
                        });
                    });
                }
            });
        };

        $scope.delete = function (appointment) {
            $translate(["delete_appointment", "confirm_delete_appointment", "error_delete_appointment"]).then(function(t) {
                dialogService.confirmDialog(t.delete_appointment, String.format(t.confirm_delete_appointment, appointment.title), function () {
                    var url = String.format("/api/appointment/{1}/{0}", appointment.appointmentId, tenantService.getTenant().id);
                    $http.delete(url).success(function () {
                        $scope.appointments = _.reject($scope.appointments, function (a) { return a.appointmentId === appointment.appointmentId; });
                    }).error(function (error) {
                        toastService.error(t.error_delete_appointment, error);
                    });
                });
            });
        };

        $scope.nextPage = function () {
            if ($scope.isBusy) {
                return;
            }
            $scope.isBusy = true;

            if (monthOffset == 0) {
                loadUntilMonthOffset(monthOffset + 2);
            } else {
                loadUntilMonthOffset(monthOffset + 1);
            }
        };

        function loadUntilMonthOffset(maxMonthOffset) {
            var url = String.format("/api/appointments/{1}/{0}", monthOffset, tenantService.getTenant().id);
            $http.get(url, { cache: false }).success(function (data) {
                if (data) {
                    for (var i = 0; i < data.length; i++) {
                        $scope.appointments.push(data[i]);
                    }
                }
                monthOffset++;
                if (maxMonthOffset > monthOffset) {
                    loadUntilMonthOffset(maxMonthOffset);
                } else {
                    $scope.isBusy = false;
                }
            });
        };

        $scope.createAppointment = function () {
            var appointment = {
                recurrence: "single",
                recurrenceOptions: {},
                link: {
                    linkType: "none"
                }
            };
            $translate(["create_appointment", "error_create_appointment"]).then(function(t) {
                openAppointmentDialog(t.create_appointment, appointment, function (a) {
                    $http.post("/api/appointments/" + tenantService.getTenant().id, a).success(function () {
                        reloadAppointmentsInFiveSeconds();
                    }).error(function (error) {
                        toastService.error(t.error_create_appointment, error);
                    });
                });
            });
        };

        $scope.createRecurrentAppointment = function () {
            var appointment = {
                recurrence: "daily",
                recurrenceOptions: {
                    hasEndDate: false,
                    isEveryXthDay: false
                },
                link: {
                    linkType: "none"
                }
            };
            $translate(["create_recurrent_appointment", "error_create_recurrent_appointment"]).then(function(t) {
                openRecurrentAppointmentDialog(t.create_recurrent_appointment, appointment, function (a) {
                    $http.post("/api/appointments/" + tenantService.getTenant().id, a).success(function () {
                        reloadAppointmentsInFiveSeconds();
                    }).error(function (error) {
                        toastService.error(t.error_create_recurrent_appointment, error);
                    });
                });
            });
        };

        $scope.import = function() {
            $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/views/calendar/importDialog.min.html',
                controller: 'AppointmentImportDialogController',
                size: 'lg',
            });
        };

        function reloadAppointmentsInFiveSeconds() {
            $timeout(function () {
                monthOffset = 0;
                $scope.appointments = [];
                $scope.nextPage();
            }, 5000);
        };

        function openAppointmentDialog(subject, appointment, func) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/views/calendar/appointmentDialog.min.html',
                controller: 'AppointmentDialogController',
                size: 'lg',
                resolve: {
                    subject: function () {
                        return subject;
                    },
                    appointment: function () {
                        return angular.copy(appointment);
                    },
                }
            });

            modalInstance.result.then(function (a) {
                func(a);
            });
        };

        function openRecurrentAppointmentDialog(subject, appointment, func) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/views/calendar/recurrentAppointmentDialog.min.html',
                controller: 'AppointmentDialogController',
                size: 'lg',
                resolve: {
                    subject: function () {
                        return subject;
                    },
                    appointment: function() {
                        return angular.copy(appointment);
                    },
                }
            });

            modalInstance.result.then(function (a) {
                func(a);
            });
        };

        $rootScope.$on("tenantChanged", function () {
            $scope.appointments = [];
            monthOffset = 0;
            $scope.nextPage();
        });

        $rootScope.$on("logout", function() { $scope.appointments = []; });
    }]);

    angular.module("app").directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);

    angular.module("app").controller("AppointmentImportDialogController", ["$scope", "$modalInstance", "$http", "tenantService", "dialogService", function($scope, $modalInstance, $http, tenantService, dialogService) {
        $scope.isImportEnabled = false;
        $scope.isUploadEnabled = false;
        $scope.currentStep = 0;
        var id = 0;

        $scope.options = { delimiter: "," };

        $http.get("/api/appointment/import/start", { cache: false }).success(function(data) {
            id = data.id;
        });

        $http.get("/api/appointment/import/encodings").success(function(data) {
            $scope.encodings = data;
            $scope.options.encoding = "utf-8";
        });

        $scope.$watch("csvFile", function() {
            if (!$scope.csvFile) {
                return;
            }
            $scope.csvFileName = $scope.csvFile.name;
            $scope.isUploadEnabled = true;
        });

        $scope.goToStep = function(step) {
            if ($scope.currentStep < step) {
                return;
            }
            $scope.currentStep = step;
        };

        $scope.upload = function () {
            if (!$scope.csvFile) {
                return;
            }

            var fd = new FormData();
            fd.append('file', $scope.csvFile);
            $http.post("/api/appointment/import/" + id + "/file", fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            }).success(function () {
                $http.get("/api/appointment/import/" + id + "/encoding", { cache: false }).success(function(e) {
                    $scope.options.encoding = e;
                });

                $scope.currentStep = 1;
            });
        };

        $scope.goToMapping = function () {
            $http.post("/api/appointment/import/" + id + "/columns", $scope.options).success(function (data) {
                $scope.columns = data;
                $scope.currentStep = 2;
            });
        };

        $scope.goToPreview = function () {
            $http.post("/api/appointment/import/" + id + "/preview", $scope.options).success(function(data) {
                $scope.preview = data;
                $scope.currentStep = 3;
            });
        };

        $scope.import = function () {
            $http.post("/api/appointment/import/"+ tenantService.getTenant().id + "/" + id + "/import", $scope.options).success(function () {
                $modalInstance.close();
            });
        };

        $scope.back = function() {
            if ($scope.currentStep > 0) {
                $scope.currentStep--;
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);

    angular.module("app").controller("AppointmentDialogController", ["$filter", "$scope", "$modalInstance", "$http", "tenantService", "dialogService", "subject", "appointment", "$translate", function ($filter, $scope, $modalInstance, $http, tenantService, dialogService, subject, appointment, $translate) {
        $scope.subject = subject;
        $scope.appointment = appointment;
        $scope.times = [];

        for (var i = 0; i < 24; i++) {
            var hour = i + ":";
            if (hour.length < 3) {
                hour = "0" + hour;
            }

            for (var m = 0; m < 60; m+=5) {
                var minute = m;
                if (m < 10) {
                    minute = "0" + minute;
                }

                $scope.times.push(hour + minute);
            }
        }

        if ($scope.appointment.startDate) {
            $scope.appointment.startDatePicker = $scope.appointment.startDate;
        }
        if ($scope.appointment.endDate) {
            $scope.appointment.endDatePicker = $scope.appointment.endDate;
        }
        if ($scope.appointment.recurrenceOptions && $scope.appointment.recurrenceOptions.endDate) {
            $scope.appointment.recurrenceOptions.endDatePicker = $scope.appointment.recurrenceOptions.endDate;
        }

        $scope.$watch("appointment.startTime", updateDuration);
        $scope.$watch("appointment.endTime", updateDuration);
        $scope.$watch("appointment.startDatePicker", updateDuration);
        $scope.$watch("appointment.endDatePicker", updateDuration);

        function updateDuration() {
            $translate(["hours", "minutes", "days"]).then(function(t) {
                if (appointment.startDatePicker && appointment.endDatePicker && appointment.startTime && appointment.endTime) {
                    var s = appointment.startTime.split(':');
                    var e = appointment.endTime.split(':');

                    appointment.startDate = $filter('date')(appointment.startDatePicker, 'yyyy-MM-dd') + "T00:00:00.000Z";
                    appointment.endDate = $filter('date')(appointment.endDatePicker, 'yyyy-MM-dd') + "T00:00:00.000Z";

                    $scope.start = moment(appointment.startDate);
                    $scope.start.add(parseInt(s[0]), 'hours');
                    $scope.start.add(parseInt(s[1]), 'minutes');
                    $scope.end = moment(appointment.endDate);
                    $scope.end.add(parseInt(e[0]), 'hours');
                    $scope.end.add(parseInt(e[1]), 'minutes');

                    var minutes = $scope.end.diff($scope.start, 'minutes');
                    if (minutes < 60) {
                        $scope.duration = toReadableNumber(minutes) + " " + t.minutes;
                        return;
                    }
                    var hours = $scope.end.diff($scope.start, 'hours', true);
                    if (hours < 24) {
                        $scope.duration = toReadableNumber(hours) + " " + t.hours;
                        return;
                    }
                    var days = $scope.end.diff($scope.start, 'days', true);
                    $scope.duration = toReadableNumber(days) + " " + t.days;
                }
            });
        };

        function toReadableNumber(number) {
            var n = number.toFixed(2);
            if (n.charAt(n.length - 1) == "0") {
                n = n.substring(0, n.length - 1);
                if (n.charAt(n.length - 1) == "0") {
                    n = n.substring(0, n.length - 2);
                }
            }
            return n;
        };

        $scope.$watch("appointment.recurrenceOptions.endDatePicker", function() {
            if (appointment.recurrenceOptions.endDatePicker) {
                var d = $filter('date')(appointment.recurrenceOptions.endDatePicker, 'yyyy-MM-dd') + "T00:00:00.000Z";
                $scope.appointment.recurrenceOptions.endDate = d;
            }
        });

        $scope.searchCategories = function (searchText) {
            var url = String.format("/api/calendar/categories/{1}?search={0}", searchText, tenantService.getTenant().id);
            return $http.get(url, { cache: false });
        };

        $scope.openStartDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.startDateOpened = true;
        };

        $scope.openEndDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.endDateOpened = true;
        };

        $scope.openEndRecurrenceDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.endRecurrenceDateOpened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.getPages = function (value) {
            var url = String.format("/api/page/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, {cache:false}).then(function (response) {
                return response.data;
            });
        };

        $scope.browsePage = function () {
            var p = null;
            if ($scope.appointment.link) {
                p = $scope.appointment.link.pageLink;
            }
            dialogService.selectPageDialog(p, function (page) {
                if (!$scope.appointment.link) {
                    $scope.appointment.link = {};
                }
                if (!page) {
                    return;
                }
                page.children = [];
                $scope.appointment.link.pageLink = page;
            });
        };

        $scope.ok = function () {
            $modalInstance.close($scope.appointment);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());
(function() {
    angular.module("app").controller("CalendarCategoriesController", ["$rootScope", "$scope", "$http", "toastService", "dialogService", "tenantService", "$translate", function ($rootScope, $scope, $http, toastService, dialogService, tenantService, $translate) {
        $scope.categories = [];
        $scope.selectedCategory = {};

        function reloadCategories() {
            $http.get("/api/calendar/categories/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
                $scope.categories = data;
            });
        };

        $http.get("/api/permission/calendarcategory/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.select = function(category) {
            $scope.selectedCategory = category;
        }

        $scope.rename = function (category) {
            $translate(["rename_calendar_category", "name", "error_rename_calendar_category"]).then(function(t) {
                dialogService.singleInputDialog(t.rename_calendar_category, t.name, category.name, function (name) {
                    var dto = {
                        oldName: category.name,
                        newName: name
                    };
                    $http.post("/api/calendar/categories/" + tenantService.getTenant().id + "/rename", dto).success(function () {
                        category.name = name;
                    }).error(function (error) {
                        toastService.error(t.error_rename_calendar_category, error);
                    });
                });
            });
        };

        $scope.delete = function (category) {
            $translate(["delete_calendar_category", "confirm_delete_calendar_category", "error_delete_calendar_category"]).then(function(t) {
                dialogService.confirmDialog(t.delete_calendar_category, String.format(t.confirm_delete_calendar_category, category.name), function () {
                    var url = String.format("/api/calendar/categories/{0}?name={1}", tenantService.getTenant().id, encodeURIComponent(category.name));
                    $http.delete(url).success(function () {
                        $scope.categories = _.reject($scope.categories, function (c) {
                            return c.name === category.name;
                        });
                    }).error(function (error) {
                        toastService.error(t.error_delete_calendar_category, error);
                    });
                });
            });
        };

        $scope.create = function () {
            $translate(["create_calendar_category", "name", "error_create_calendar_category"]).then(function(t) {
                dialogService.singleInputDialog(t.create_calendar_category, t.name, "", function (name) {
                    var dto = {
                        name: name
                    };
                    $http.post("/api/calendar/categories/" + tenantService.getTenant().id + "/new", dto).success(function() {
                        $scope.categories.push({name: name});
                    }).error(function (error) {
                        toastService.error(t.error_create_calendar_category, error);
                    });
                });
            });
            
        };

        $rootScope.$on("tenantChanged", reloadCategories);

        $rootScope.$on('logout', function () { $scope.categories = {}; });

        reloadCategories();
    }]);
}());
(function () {
    angular.module("app").controller("CalendarTemplatesController", ["$rootScope", "$scope", "$http", "dialogService", "tenantService", function ($rootScope, $scope, $http, dialogService, tenantService) {
        $scope.templates = [];

        function reloadTemplates() {
            $http.get("/api/calendar/templates/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
                $scope.templates = data;
            });
        };

        $rootScope.$on("tenantChanged", reloadTemplates);

        $rootScope.$on('logout', function () { $scope.templates = {}; });

        reloadTemplates();
    }]);
}());
(function() {
    angular.module("app").controller("PageContentEditorHtmlController", ["$scope", function ($scope) {
        if (!$scope.c || !$scope.c.value || !$scope.c.value.value || !_.isString($scope.c.value.value)) {
            $scope.c.value = { value: "" };
        }

        $scope.tinymceOptions = {
            theme: "modern",
            //relative_urls: true,
            remove_linebreaks: false,
            convert_urls: false,
            forced_root_blocks: false,
            allow_script_urls: true,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code hr wordcount textcolor colorpicker",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media"
        };
    }]);

    angular.module("app").controller("PageContentEditorCodeController", ["$scope", function($scope) {
        if (!$scope.c || !$scope.c.value || !$scope.c.value.value || !_.isString($scope.c.value.value)) {
            $scope.c.value = { value: "" };
        }

        $scope.codemirrorOptions = {
            lineNumbers: true,
            theme: 'neo',
            lineWrapping: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            matchTags: { bothTags: true },
            autoCloseTags: true,
            styleActiveLine: true,
            viewportMargin: Infinity,
            mode: "htmlmixed"
        };
    }]);
}());
(function() {
    angular.module("app").controller("PageContentEditorImageController", ["$scope", "$http", "$log", "$interval", "dialogService", "tenantService", function ($scope, $http, $log, $interval, dialogService, tenantService) {
        var previousImageSettings = "";
        $scope.previewUrl = null;
        $scope.fileStores = [];

        $scope.$watch("c.value.linkType", function (newVal) {
            if (newVal != 'page' && $scope.c.value) {
                $scope.c.value.pageLink = null;
            }
            if (newVal != 'external' && $scope.c.value) {
                $scope.c.value.externalLink = null;
            }
        });

        $http.get("/api/filestores/" + tenantService.getTenant().id, { cache: false }).success(function(fileStores) {
            $scope.fileStores = fileStores;
            updateIsLocalFile();
        });

        $scope.$watch("c.value.file", function (newVal) {
            if (newVal) {
                $scope.c.value.fileId = newVal.id;
            }
            updateIsLocalFile();
        });

        var updateIsLocalFile = function() {
            $scope.isLocalFile = false;

            if ($scope.c.value && $scope.c.value.file) {
                _.each($scope.fileStores, function (fs) {
                    if ($scope.c.value.file.fileStoreId === fs.id) {
                        $scope.isLocalFile = fs.isLocalFilesystem;
                    }
                });
            }
        };

        $scope.$watch("c.value.pageLink", function(newVal) {
            if (newVal) {
                $scope.c.value.pageId = newVal.id;
            }
        });

        $scope.$watch("c.value.imageSettings", function(newVal) {
            if (newVal) {
                $scope.c.value.alpha = newVal.alpha;
                $scope.c.value.saturation = newVal.saturation;
                $scope.c.value.roundedCorners = newVal.roundedCorners;
                $scope.c.value.brightness = newVal.brightness;
                $scope.c.value.contrast = newVal.contrast;
                $scope.c.value.filter = newVal.filter;
                $scope.c.value.tint = newVal.tint;
            }
        }, true);

        if ($scope.c.value && $scope.c.type && $scope.c.type.toUpperCase() == "IMAGE") {
            if ($scope.c.value.fileId) {
                var fileUrl = String.format("/api/file/{0}/detail", $scope.c.value.fileId);
                $http.get(fileUrl, { cache: false }).success(function(data) {
                    $scope.c.value.file = data;
                });
            }
            if ($scope.c.value.linkType && $scope.c.value.linkType.toUpperCase() == "PAGE" && $scope.c.value.pageId) {
                var pageUrl = String.format("/api/page/{0}", $scope.c.value.pageId);
                $http.get(pageUrl, { cache: false }).success(function(data) {
                    data.content = null;
                    $scope.c.value.pageLink = data;
                });
            }

            $scope.c.value.imageSettings = {
                alpha: $scope.c.value.alpha,
                saturation: $scope.c.value.saturation,
                roundedCorners: $scope.c.value.roundedCorners,
                brightness: $scope.c.value.brightness,
                contrast: $scope.c.value.contrast,
                filter: $scope.c.value.filter,
                tint: $scope.c.value.tint,
            };
        }

        $scope.matrixFilters = [ '', 'BlackWhite', 'Comic', 'Gotham', 'GreyScale', 'HiSatch', 'Invert', 'Lomograph', 'LoSatch', 'Polaroid', 'Sepia' ];

        $http.get("/api/preview/colors").success(function(data) {
            $scope.tintColors = data;
            $scope.tintColors.splice(0, 0, '');
        });

        $scope.getFiles = function (value) {
            var url = String.format("/api/file/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, {cache: false}).then(function (response) {
                return response.data;
            });
        };

        $scope.getPages = function (value) {
            var url = String.format("/api/page/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, { cache: false}).then(function (response) {
                return response.data;
            });
        };

        $scope.browseImage = function () {
            var f = null;
            if ($scope.c.value) {
                f = $scope.c.value.file;
            }
            dialogService.selectFileDialog(f, function (file) {
                if (!$scope.c.value) {
                    $scope.c.value = {};
                }
                if (!file) {
                    return;
                }
                $scope.c.value.file = file;
            });
        };

        $scope.browsePage = function () {
            var p = null;
            if ($scope.c.value) {
                p = $scope.c.value.pageLink;
            }
            dialogService.selectPageDialog(p, function (page) {
                if (!$scope.c.value) {
                    $scope.c.value = {};
                }
                if (!page) {
                    return;
                }
                page.children = [];
                $scope.c.value.pageLink = page;
            });
        };

        var updatePreviewUrl = function () {
            if (!$scope.c.value) {
                $scope.previewUrl = '';
                return;
            }
            var file = $scope.c.value.file;
            if (!file || !file.previewUrl) {
                $scope.previewUrl = '';
                return;
            }

            if (!$scope.c.value.imageSettings) {
                $scope.c.value.imageSettings = {
                    alpha: 100,
                    saturation: 0,
                    roundedCorners: 0,
                    brightness: 0,
                    contrast: 0,
                    filter: "",
                    tint: ""
                };
            }

            var imageSettingsKey = String.format("{0}+{1}", file.previewUrl, angular.toJson($scope.c.value.imageSettings));
            if (previousImageSettings == imageSettingsKey) {
                return;
            }
            previousImageSettings = imageSettingsKey;

            var url = String.format("{0}?size=500", file.previewUrl);
            var imageSettings = $scope.c.value.imageSettings;
            if (imageSettings.alpha != 100) {
                url += "&alpha=" + imageSettings.alpha;
            }
            if (imageSettings.saturation != 0) {
                url += "&saturation=" + imageSettings.saturation;
            }
            if (imageSettings.roundedCorners != 0) {
                url += "&roundedCorners=" + imageSettings.roundedCorners;
            }
            if (imageSettings.brightness != 0) {
                url += "&brightness=" + imageSettings.brightness;
            }
            if (imageSettings.contrast != 0) {
                url += "&contrast=" + imageSettings.contrast;
            }
            if (imageSettings.filter) {
                url += "&filter=" + imageSettings.filter;
            }
            if (imageSettings.tint) {
                url += "&tint=" + imageSettings.tint;
            }
            $scope.previewUrl = url;
        };

        var updateImagePreviewInterval = null;

        if (updateImagePreviewInterval == null) {
            updateImagePreviewInterval = $interval(updatePreviewUrl, 1000);
        }

        $scope.$on('$destroy', function () {
            $interval.cancel(updateImagePreviewInterval);
        });
    }]);
}());
(function() {
    angular.module("app").controller("PageContentEditorMarkdownController", ["$scope", "$http", "$log", "$interval", function ($scope, $http, $log, $interval) {
        $scope.preview = { html: "" };
        $scope.previous = null;
        var isUpdating = false;

        if ($scope.c.value && $scope.c.value.value && _.isString($scope.c.value.value)) {
            $scope.preview.html = $scope.c.value;
        } else {
            $scope.c.value = { value: "" };
        }

        var updatePreview = function() {
            if ($scope.previous == $scope.c.value.value) {
                return;
            }
            if (!$scope.c.value || !$scope.c.value.value) {
                return;
            }
            if (isUpdating) {
                return;
            }
            isUpdating = true;

            $scope.previous = $scope.c.value.value;
            $http.post("/api/markdown", { content: $scope.c.value.value }).success(function (data) {
                $scope.preview.html = data;
                isUpdating = false;
            });
        };

        var updatePreviewInterval = $interval(updatePreview, 1000);
        $scope.$on('$destroy', function (e) {
            $interval.cancel(updatePreviewInterval);
        });

        updatePreview();
    }]);
}());
(function() {
    angular.module("app").controller("PageContentEditorObjectController", ["$scope", "$http", "tenantService", function($scope, $http, tenantService) {
        $scope.types = [];
        $scope.objects = [];
        $scope.templates = [];

        if (!$scope.c.value.value) {
            $scope.c.value.value = {};
        }

        $scope.c.value.selectedType = null;
        $scope.c.value.selectedObject = null;
        $scope.c.value.selectedTemplate = null;

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
            $scope.types = data;

            if ($scope.c.value.definitionId) {
                var t = _.find(data, function (d) { return d.id === $scope.c.value.definitionId; });
                if (t) {
                    $scope.c.value.selectedType = t;
                }
            }
        });

        $scope.$watch('c.value.selectedType', function (newVal) {
            if (newVal) {
                $scope.c.value.definitionId = newVal.id;
                var tenantId = tenantService.getTenant().id;

                $http.get("/api/objects/" + tenantId + "/" + newVal.id, { cache: false }).success(function(data) {
                    $scope.objects = data;

                    if ($scope.c.value.objectId) {
                        var o = _.find(data, function (d) { return d.id === $scope.c.value.objectId; });
                        if (o) {
                            $scope.c.value.selectedObject = o;
                        }
                    }
                });
                $http.get("/api/objecttemplates/" + tenantId + "/" + newVal.id, { cache: false }).success(function(data) {
                    $scope.templates = data;

                    if ($scope.c.value.templateName) {
                        var t = _.find(data, function (d) { return d.name === $scope.c.value.templateName; });
                        if (t) {
                            $scope.c.value.selectedTemplate = t;
                        }
                    }
                });
            }
        });

        $scope.$watch("c.value.selectedObject", function(newVal) {
            if (newVal) {
                $scope.c.value.objectId = newVal.id;
            }
        });

        $scope.$watch("c.value.selectedTemplate", function (newVal) {
            if (newVal) {
                $scope.c.value.templateName = newVal.name;
            }
        });
    }]);

    angular.module("app").controller("PageContentEditorFormController", ["$scope", "$http", "tenantService", function($scope, $http, tenantService) {
        $scope.forms = [];
        $scope.templates = [];

        var oldValue = $scope.c.value;

        if (!$scope.c.value) {
            $scope.c.value = {};
        }

        $scope.c.value.selectedForm = null;
        $scope.c.value.selectedTemplate = null;

        $scope.$watch("c.value.selectedForm", function(newVal) {
            if (newVal) {
                $scope.c.value.formId = $scope.c.value.selectedForm.id;
            }
        });

        $scope.$watch("c.value.selectedTemplate", function (newVal) {
            if (newVal) {
                $scope.c.value.templateId = $scope.c.value.selectedTemplate.id;
            }
        });

        $http.get("/api/forms/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
            $scope.forms = data;

            if ($scope.c.value.formId) {
                var f = _.find(data, function (d) { return d.id === $scope.c.value.formId; });
                if (f) {
                    $scope.c.value.selectedForm = f;
                }
            }
        });
    }]);
}());
(function () {
    angular.module("app").controller("PageContentEditorTextController", ["$scope", "$log", function ($scope, $log) {
        $scope.preview = { html: "" };
        $scope.previous = null;

        if ($scope.c.value && $scope.c.value.value && _.isString($scope.c.value.value)) {
            $scope.preview.html = $scope.c.value;
        } else {
            $scope.c.value = { value: "" };
        }
    }]);
}());
(function() {
    angular.module("app").factory("dialogService", ["$modal", function ($modal) {
        var instance = {};

        var singleInputDialog = function (subject, label, value, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/defaultInputDialog.min.html",
                controller: "SingleInputDialogController",
                resolve: {
                    subject: function () { return subject; },
                    value: function () { return value; },
                    label: function () { return label; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var twoInputsDialog = function (subject, labelOne, valueOne, labelTwo, valueTwo, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/twoInputsDialog.min.html",
                controller: "TwoInputsDialogController",
                resolve: {
                    subject: function () { return subject; },
                    valueOne: function () { return valueOne; },
                    labelOne: function () { return labelOne; },
                    valueTwo: function () { return valueTwo; },
                    labelTwo: function () { return labelTwo; }
                }
            }).result.then(function (result) {
                onAccept(result.valueOne, result.valueTwo);
            });
        };

        var singlePasswordDialog = function (subject, label, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/singlePasswordDialog.min.html",
                controller: "SinglePasswordDialogController",
                resolve: {
                    subject: function () { return subject; },
                    label: function () { return label; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var confirmDialog = function (subject, label, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/defaultConfirmDialog.min.html",
                controller: "ConfirmDialogController",
                resolve: {
                    subject: function () { return subject; },
                    label: function () { return label; }
                }
            }).result.then(function () {
                onAccept();
            });
        };

        var messageDialog = function (subject, label) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/messageDialog.min.html",
                controller: "MessageDialogController",
                resolve: {
                    subject: function () { return subject; },
                    label: function () { return label; }
                }
            });
        };

        var fileUploadDialog = function (subject, directoryId, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/fileUploadDialog.min.html",
                controller: "FileUploadDialogController",
                resolve: {
                    subject: function () { return subject; },
                    uploadUrl: function () { return "/api/file/new/" + directoryId; },
                    maxFileCount: function() { return 10000; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var fileReplaceDialog = function (subject, fileId, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/fileUploadDialog.min.html",
                controller: "FileUploadDialogController",
                resolve: {
                    subject: function () { return subject; },
                    uploadUrl: function () { return "/api/file/replace/" + fileId; },
                    maxFileCount: function () { return 1; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var selectPageDialog = function(page, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectPageDialog.min.html",
                controller: "SelectPageDialogController",
                resolve: {
                    page: function () { return page; }
                }
            }).result.then(function(result) {
                onAccept(result);
            });
        };

        var selectFileDialog = function (file, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectFileDialog.min.html",
                controller: "SelectFileDialogController",
                size: "lg",
                resolve: {
                    file: function () { return file; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var selectFormDialog = function (form, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectFormDialog.min.html",
                controller: "SelectFormDialogController",
                resolve: {
                    form: function () { return form; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var selectObjectDialog = function(object, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectObjectDialog.min.html",
                controller: "SelectObjectDialogController",
                resolve: {
                    selectedObject: function () { return object; }
                }
            }).result.then(function(result) {
                onAccept(result);
            });
        };

        var selectUserDialog = function(onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectUserDialog.min.html",
                controller: "SelectUserDialogController"
            }).result.then(function(result) {
                onAccept(result);
            });
        };

        var selectTenantsDialog = function(tenantIds, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectTenantsDialog.min.html",
                controller: "SelectTenantsDialogController",
                resolve: {
                    tenantIds: function() { return tenantIds; }
                }
            }).result.then(function(result) {
                onAccept(result);
            });
        }

        var fileEditorDialog = function(filename, content, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/fileEditorDialog.min.html",
                controller: "FileEditorDialogController",
                size: "lg",
                resolve: {
                    filename: function() { return filename; },
                    content: function () { return content; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        instance.singleInputDialog = singleInputDialog;
        instance.singlePasswordDialog = singlePasswordDialog;
        instance.twoInputsDialog = twoInputsDialog;
        instance.confirmDialog = confirmDialog;
        instance.messageDialog = messageDialog;
        instance.fileUploadDialog = fileUploadDialog;
        instance.fileReplaceDialog = fileReplaceDialog;
        instance.selectPageDialog = selectPageDialog;
        instance.selectFileDialog = selectFileDialog;
        instance.selectFormDialog = selectFormDialog;
        instance.selectUserDialog = selectUserDialog;
        instance.selectObjectDialog = selectObjectDialog;
        instance.selectTenantsDialog = selectTenantsDialog;
        instance.fileEditorDialog = fileEditorDialog;

        return instance;
    }]);

    angular.module("app").controller("FileEditorDialogController", ["$scope", "$modalInstance", "filename", "content", function($scope, $modalInstance, filename, content) {
        $scope.subject = filename;
        $scope.content = content;
        $scope.isFullscreen = false;

        $scope.codemirrorOptions = {
            lineNumbers: true,
            theme: "neo",
            lineWrapping: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            matchTags: { bothTags: true },
            autoCloseTags: true,
            styleActiveLine: true,
            viewportMargin: Infinity,
            mode: "htmlmixed"
        };

        $scope.expand = function () {
            $scope.isFullscreen = true;
            angular.element(".modal-dialog").addClass("modal-fullscreen");
        };

        $scope.compress = function () {
            $scope.isFullscreen = false;
            angular.element(".modal-dialog").removeClass("modal-fullscreen");
        };

        $scope.ok = function () {
            $scope.compress();
            $modalInstance.close($scope.content);
        };

        $scope.cancel = function () {
            $scope.compress();
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SelectTenantsDialogController", ["$scope", "$modalInstance", "tenantService", "tenantIds", function ($scope, $modalInstance, tenantService, tenantIds) {
        var tenant = tenantService.getTenant();
        var tenants = _.reject(tenantService.getTenants(), function(t) {
            return t.id === tenant.id;
        });

        $scope.tenants = _.map(tenants, function (t) {
            var c = angular.copy(t);
            c.isSelected = _.some(tenantIds, function(id) { return id === c.id; });
            return c;
        });

        $scope.ok = function () {
            var selected = _.filter($scope.tenants, function(t) {
                return t.isSelected;
            });
            $modalInstance.close(selected);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SelectUserDialogController", ["$scope", "$modalInstance", "$http", "tenantService", function ($scope, $modalInstance, $http, tenantService) {
        $http.get("/api/users/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
            $scope.users = data;
        });

        $scope.select = function (user) {
            $scope.selectedUser = user;
        }

        $scope.ok = function () {
            $modalInstance.close($scope.selectedUser);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SelectPageDialogController", ["$scope", "$modalInstance", "$http", "$timeout", "tenantService", "page", function ($scope, $modalInstance, $http, $timeout, tenantService, page) {

        function expandParent(website, pge) {
            var parent = _.find(website.pages, function (p) { return p.id === pge.parentId; });
            if (parent) {
                parent.isCollapsed = false;
                expandParent(website, parent);
            }
        }

        function searchPages(website, pageId) {
            _.each(website.pages, function (p) { p.isCollapsed = true; });
            var pages = _.filter(website.pages, function (p) { return p.id === pageId; });
            _.each(pages, function (p) { expandParent(website, p); });
        };

        var addPagesToCollection = function (website, pages) {
            angular.forEach(pages, function (value) {
                website.allPages.push(value);
            });
        };

        var openWebsite = function (website) {
            if (!website.isLoaded) {
                website.isLoaded = true;
                $http.get("/api/pages/" + website.id, { cache: false }).success(function (pageData) {
                    website.pages = pageData;
                    addPagesToCollection(website, website.pages);
                    $scope.$broadcast("collapseAll");
                });
                $http.get("/api/pages/" + website.id + "/starred", { cache: false }).success(function (starredData) {
                    website.starred = starredData;
                    addPagesToCollection(website, website.starred);
                });
            }
        };

        $http.get("/api/websites/" + tenantService.getTenant().id, { cache: false })
            .success(function (data) {
                $scope.websites = data;

                angular.forEach($scope.websites, function (value, key) {
                    angular.extend(value, {
                        isOpen: false,
                        isLoaded: false,
                        pages: [],
                        starred: [],
                        allPages: []
                    });
                    $scope.$watch("websites[" + key + "].isOpen", function () {
                        if (value.isOpen && !value.isLoaded) {
                            openWebsite(value);
                        }
                    });
                });

                if (page) {
                    var website = _.find($scope.websites, function (w) { return w.id === page.websiteId; });
                    if (!website) {
                        return;
                    }
                    website.isOpen = true;
                    $timeout(function() {
                        $scope.selectedPage = page;
                        searchPages(website, page.id);
                    }, 1000);

                }

            }).error(function () {
                $scope.websites = {};
            });

        $scope.getChildren = function (websiteId, parentPageId) {
            if (!$scope.websites) {
                return [];
            }
            var website = _.find($scope.websites, function (w) { return w.id === websiteId; });
            if (!website) {
                return [];
            }

            var children = _.filter(website.pages, function (p) { return p.parentId === parentPageId; });
            children = _.sortBy(children, "name");
            children = _.sortBy(children, "sortOrder");
            return children;
        };

        $scope.selectPage = function (node) {
            $scope.selectedPage = node.node;
        };

        $scope.ok = function () {
            $modalInstance.close($scope.selectedPage);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };

        // websites
        // pages

        // falls page vorhanden, entsprechende website und alle page parents aufklappen und seite selektieren
    }]);

    angular.module("app").controller("SelectObjectDialogController", ["$scope", "$modalInstance", "$http", "$timeout", "tenantService", "selectedObject", function ($scope, $modalInstance, $http, $timeout, tenantService, selectedObject) {
        $scope.definitions = [];
        $scope.objects = [];
        $scope.step = 0;
        $scope.selectedDefinition = {};
        $scope.selectedObject = {};

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
            _.each(data, function (d) {
                d.isShared = false;
                $scope.definitions.push(d);
            });

            var fd = _.find($scope.definitions, function (d) { return d.id === selectedObject.definitionId; });
            if (fd) {
                $scope.selectedObject = selectedObject;
                $scope.selectDefinition(fd);
            }
        });

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id + "/shared", { cache: false }).success(function (data) {
            _.each(data, function (d) {
                d.isShared = true;
                $scope.definitions.push(d);
            });

            var fd = _.find($scope.definitions, function (d) { return d.id === selectedObject.definitionId; });
            if (fd) {
                $scope.selectedObject = selectedObject;
                $scope.selectDefinition(fd);
            }
        });

        $scope.goBack = function() {
            $scope.step = 0;
            $scope.selectedObject = {};
        };

        $scope.selectDefinition = function(definition) {
            $scope.selectedDefinition = definition;
            var tenantId = tenantService.getTenant().id;

            $http.get("/api/objects/" + tenantId + "/" + definition.id, { cache: false }).success(function(data) {
                $scope.objects = data;
                $scope.step = 1;
            });
        };

        $scope.selectObject = function(o) {
            $scope.selectedObject = o;
        };

        $scope.ok = function () {
            if ($scope.step === 1 && $scope.selectedObject) {
                $modalInstance.close($scope.selectedObject);
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SelectFileDialogController", ["$scope", "$modalInstance", "$http", "$timeout", "tenantService", "file", function ($scope, $modalInstance, $http, $timeout, tenantService, file) {
        $scope.files = [];
        $scope.selectedFile = {};
        $scope.sharedDirectoryRoot = { id: 12345, name: "Shared directories" };
        $scope.shared = [$scope.sharedDirectoryRoot];

        function getRootNodesScope(treeId) {
            return angular.element(document.getElementById(treeId)).scope().$nodesScope.childNodes();
        };

        function getScopePathIter(comparer, search, scope, parentScopeList) {
            if (!scope) {
                return null;
            }

            var newParentScopeList = parentScopeList.slice();
            newParentScopeList.push(scope);

            if (scope.$modelValue && comparer(scope.$modelValue, search)) {
                $scope.selectDirectory(scope.$modelValue);
                return newParentScopeList;
            }

            var foundScopesPath = null;
            var childNodes = scope.childNodes();
            var children = [];

            if (scope.$modelValue && scope.$modelValue.children) {
                children = scope.$modelValue.children;
            }

            for (var i = 0; foundScopesPath === null && i < childNodes.length; i++) {
                foundScopesPath = getScopePathIter(comparer, search, childNodes[i], newParentScopeList);
            }
            for (var c = 0; foundScopesPath === null && c < children.length; c++) {
                if (comparer(children[c], search)) {
                    $scope.selectDirectory(children[c]);
                    return newParentScopeList;
                }
            }

            return foundScopesPath;
        }

        function fileIdComparer(f, id) {
            return f.id === id;
        }

        function getScopePath(comparer, search, scope) {
            return getScopePathIter(comparer, search, scope, []);
        }

        var expandSelectedDirectory = function(treeId) {
            var rootNodesScope = getRootNodesScope(treeId);
            for (var s = 0; s < rootNodesScope.length; s++) {
                var rootNodeScope = rootNodesScope[s];
                var parentScopes = getScopePath(fileIdComparer, file.directoryId, rootNodeScope);

                if (parentScopes) {
                    for (var i = 0; i < parentScopes.length; i++) {
                        parentScopes[i].expand();
                    }
                }
            }
        };

        $modalInstance.opened.then(function () {
            if (file) {
                $timeout(function() {
                    $scope.selectedFile = file;
                    expandSelectedDirectory("tree-root");
                    expandSelectedDirectory("tree-root-shared");
                }, 1000);
            }
        });

        var tenantId = tenantService.getTenant().id;
        $http.get("/api/filestores/" + tenantId, { cache: false }).success(function(fileStores) {
            $scope.directories = fileStores;
            _.each(fileStores, function (fileStore) {
                fileStore.isFileStore = true;
                $http.get("/api/filedirectory/" + tenantId + "/" + fileStore.id, { cache: false }).success(function (data) {
                    fileStore.children = data;
                });
            });
        });

        function markDirectoryAsShared(directory) {
            directory.isShared = true;

            if (directory.children && directory.children.length > 0) {
                _.each(directory.children, markDirectoryAsShared);
            }
        };

        $http.get("/api/filedirectory/" + tenantId + "/shared", { cache: false }).success(function (directories) {
            $scope.sharedDirectoryRoot.children = directories;

            _.each(directories, markDirectoryAsShared);

            var treeRootScope = angular.element(document.getElementById("tree-root-shared")).scope();
            treeRootScope.collapseAll();
        });

        var getFiles = function (directoryId) {
            $scope.files = [];
            $http.get("/api/file/" + directoryId, { cache: false }).success(function (data) {
                $scope.files = data;
            });
        };

        var getSharedFiles = function (directoryId) {
            $scope.files = [];
            var url = String.format("/api/files/{0}/{1}/shared", tenantService.getTenant().id, directoryId);
            $http.get(url, { cache: false }).success(function (data) {
                $scope.files = data;
            });
        }

        $scope.selectDirectory = function (directory) {
            $scope.selectedDirectory = directory;

            if (directory.isFileStore) {
                $scope.files = [];
                return;
            }

            if (directory.isShared) {
                getSharedFiles($scope.selectedDirectory.id);
                return;
            }

            getFiles($scope.selectedDirectory.id);
        };

        $scope.selectFile = function(f) {
            $scope.selectedFile = f;
        };

        $scope.ok = function () {
            $modalInstance.close($scope.selectedFile);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SelectFormDialogController", ["$scope", "$modalInstance", "$http", "tenantService", "form", function($scope, $modalInstance, $http, tenantService, form) {
        $scope.forms = [];
        $scope.selectedForm = form;

        $http.get("/api/forms/" + tenantService.getTenant().id, { cache: false }).success(function(forms) {
            $scope.forms = forms;
        });

        $scope.select = function (f) {
            $scope.selectedForm = f;
        };

        $scope.ok = function () {
            $modalInstance.close($scope.selectedForm);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("FileUploadDialogController", ["$scope", "$modalInstance", "$log", "subject", "uploadUrl", "maxFileCount", function ($scope, $modalInstance, $log, subject, uploadUrl, maxFileCount) {
        $scope.subject = subject;
        $scope.uploadUrl = uploadUrl;
        $scope.maxFileCount = maxFileCount;

        $scope.ok = function () {
            $modalInstance.close("ok");
        };
    }]);

    angular.module("app").controller("SingleInputDialogController", ["$scope", "$modalInstance", "$log", "subject", "label", "value", function ($scope, $modalInstance, $log, subject, label, value) {
        $scope.subject = subject;
        $scope.label = label;
        $scope.value = value;

        $scope.ok = function () {
            $modalInstance.close($scope.value);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SinglePasswordDialogController", ["$scope", "$modalInstance", "$log", "$translate", "subject", "label", function ($scope, $modalInstance, $log, $translate, subject, label) {
        $scope.subject = subject;
        $scope.label = label;
        $scope.oldPassword = "";
        $scope.newPassword = "";

        var ridiculous = "";
        var veryWeak = "";
        var weak = "";
        var good = "";
        var strong = "";
        var veryStrong = "";

        $translate(["ridiculous", "very_weak", "weak", "good", "strong", "very_strong"]).then(function (t) {
            ridiculous = t.ridiculous;
            veryWeak = t.very_weak;
            weak = t.weak;
            good = t.good;
            strong = t.strong;
            veryStrong = t.very_strong;
        });

        $scope.$watch("newPassword", function (newValue) {
            var score = new Score(newValue);
            var entropy = score.calculateEntropyScore() * 1.25;
            entropy = entropy > 100 ? 100 : (entropy < 0 ? 0 : entropy);

            $scope.pwStrength = entropy;
            $scope.pwStrengthColor = "danger";

            if (entropy < 25) {
                $scope.pwStrengthColor = "danger";
            } else if (entropy < 50) {
                $scope.pwStrengthColor = "warning";
            } else {
                $scope.pwStrengthColor = "success";
            }

            if (entropy < 10) {
                $scope.pwStrengthDisplay = ridiculous;
            } else if (entropy < 20) {
                $scope.pwStrengthDisplay = veryWeak;
            } else if (entropy < 30) {
                $scope.pwStrengthDisplay = weak;
            } else if (entropy < 40) {
                $scope.pwStrengthDisplay = good;
            } else if (entropy < 50) {
                $scope.pwStrengthDisplay = strong;
            } else {
                $scope.pwStrengthDisplay = veryStrong;
            }
        });

        $scope.showOrHidePassword = function () {
            if (document.getElementById("password").type === "password") {
                document.getElementById("password").type = "text";
            } else {
                document.getElementById("password").type = "password";
            }
        };

        $scope.generatePassword = function () {
            document.getElementById("password").type = "text";
            $scope.newPassword = generatePassword(12, false);
        };

        $scope.ok = function () {
            $modalInstance.close({
                oldPassword: $scope.oldPassword,
                newPassword: $scope.newPassword
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("TwoInputsDialogController", ["$scope", "$modalInstance", "$log", "subject", "labelOne", "valueOne", "labelTwo", "valueTwo", function ($scope, $modalInstance, $log, subject, labelOne, valueOne, labelTwo, valueTwo) {
        $scope.subject = subject;
        $scope.labelOne = labelOne;
        $scope.valueOne = valueOne;
        $scope.labelTwo = labelTwo;
        $scope.valueTwo = valueTwo;

        $scope.ok = function () {
            $modalInstance.close({
                valueOne: $scope.valueOne,
                valueTwo: $scope.valueTwo
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("ConfirmDialogController", ["$scope", "$modalInstance", "subject", "label", function ($scope, $modalInstance, subject, label) {
        $scope.subject = subject;
        $scope.text = label;

        $scope.ok = function () {
            $modalInstance.close("ok");
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("MessageDialogController", ["$scope", "$modalInstance", "subject", "label", function ($scope, $modalInstance, subject, label) {
        $scope.subject = subject;
        $scope.text = label;

        $scope.ok = function () {
            $modalInstance.close("ok");
        };
    }]);
}());
(function() {
    angular.module("app").controller("FileController", ["$rootScope", "$scope", "$log", "$http", "tenantService", "dialogService", "toastService", "$translate", function ($rootScope, $scope, $log, $http, tenantService, dialogService, toastService, $translate) {
        $scope.directories = [];
        $scope.trashDirectory = { id: 123456, name: "Trash" };
        $scope.trashDirectories = [$scope.trashDirectory];
        $scope.sharedDirectoryRoot = { id: 12345, name: "Shared directories" };
        $scope.shared = [$scope.sharedDirectoryRoot];
        $scope.files = [];
        $scope.selectedDirectory = null;
        $scope.fileSearchText = "";

        $scope.treeOptions = {
            dropped: function (event) {
                var thisNr = event.source.nodeScope.node.id;
                var oldParent = event.source.nodesScope == null || event.source.nodesScope.node == null ? null : event.source.nodesScope.node.id;
                var newParent = event.dest.nodesScope == null || event.dest.nodesScope.node == null ? null : event.dest.nodesScope.node.id;

                if ((oldParent === newParent || (!oldParent && !newParent))) {
                    return false;
                }

                newParent = newParent || 0;

                $http.put('/api/filedirectory/' + thisNr + '/move/' + newParent).success(function () {
                    $log.debug('dropped ' + thisNr + " to " + newParent);
                });

                return true;
            }
        };

        $http.get("/api/permission/file/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.selectDirectory = function (directory) {
            $scope.selectedDirectory = directory;

            if (directory.isFileStore) {
                $scope.files = [];
                return;
            }

            if (directory.isShared) {
                getSharedFiles($scope.selectedDirectory.id);
            } else {
                getFiles($scope.selectedDirectory.id);
            }
        };

        $scope.selectTrashDirectory = function() {
            $scope.selectedDirectory = $scope.trashDirectory;
            getTrashFiles();
        };

        var getTrashFiles = function () {
            $scope.files = [];
            var url = String.format("/api/file/{0}/trash", tenantService.getTenant().id);
            $http.get(url, { cache: false }).success(function (data) {
                $scope.files = data;
            });
        };

        var getSharedFiles = function(directoryId) {
            $scope.files = [];
            var url = String.format("/api/files/{0}/{1}/shared", tenantService.getTenant().id, directoryId);
            $http.get(url, { cache: false }).success(function (data) {
                $scope.files = data;
            });
        }

        var moveFile = "";
        var confirmMoveFile = "";
        var errorMoveFile = "";
        var renameFile = "";
        var errorRenameFile = "";
        var name = "";
        var uploadFiles = "";
        var deleteFile = "";
        var confirmDeleteFile = "";
        var errorDeleteFile = "";
        var replaceFileSubject = "";
        var createDirectory = "";
        var errorCreateDirectory = "";
        var deleteDirectory = "";
        var confirmDeleteDirectory = "";
        var errorDeleteDirectory = "";
        var renameDirectory = "";
        var errorRenameDirectory = "";
        var errorEnableFileTracking = "";
        var errorDisableFileTracking = "";

        $translate(["move_file", "confirm_move_file", "error_move_file", "rename_file", "error_rename_file", "name", "upload_files", "delete_file", "confirm_delete_file", "error_delete_file", "replace_file_subject", "create_directory", "error_create_directory", "delete_directory", "confirm_delete_directory", "error_delete_directory", "rename_directory", "error_rename_directory", "error_enable_file_tracking", "error_disable_file_tracking", "shared_directories"]).then(function (t) {
            moveFile = t.move_file;
            confirmMoveFile = t.confirm_move_file;
            errorMoveFile = t.error_move_file;
            renameFile = t.rename_file;
            errorRenameFile = t.error_rename_file;
            name = t.name;
            uploadFiles = t.upload_files;
            deleteFile = t.delete_file;
            confirmDeleteFile = t.confirm_delete_file;
            errorDeleteFile = t.error_delete_file;
            replaceFileSubject = t.replace_file_subject;
            createDirectory = t.create_directory;
            errorCreateDirectory = t.error_create_directory;
            deleteDirectory = t.delete_directory;
            confirmDeleteDirectory = t.confirm_delete_directory;
            errorDeleteDirectory = t.error_delete_directory;
            renameDirectory = t.rename_directory;
            errorRenameDirectory = t.error_rename_directory;
            errorEnableFileTracking = t.error_enable_file_tracking;
            errorDisableFileTracking = t.error_disable_file_tracking;

            $scope.sharedDirectoryRoot.name = t.shared_directories;
        });

        $scope.onDrop = function (target, source) {
            dialogService.confirmDialog(moveFile, String.format(confirmMoveFile, source.fullname, target.name), function () {
                $http.put("/api/file/" + source.id + "/move/" + target.id).success(function () {
                    $scope.files = _.reject($scope.files, function (o) { return o.id === source.id; });
                }).error(function(error) {
                    toastService.error(errorMoveFile, error);
                });
            });
        };

        var getFiles = function (directoryId) {
            $scope.files = [];
            $http.get("/api/file/" + directoryId, { cache: false }).success(function (data) {
                $scope.files = data;
            });
        };

        var markDirectoriesWhenParentsAreShared = function (directory, isShared) {
            if (directory.shared && directory.shared.length > 0) {
                isShared = true;
            }

            _.each(directory.children, function (d) {
                d.parentIsShared = isShared;
                markDirectoriesWhenParentsAreShared(d, isShared);
            });
        };

        var markDirectoryIsShared = function(directory) {
            directory.isShared = true;
            _.each(directory.children, markDirectoryIsShared);
        };

        var getDirectories = function () {
            var tenantId = tenantService.getTenant().id;
            $http.get("/api/filestores/" + tenantId, { cache: false }).success(function (fileStores) {
                $scope.directories = fileStores;
                _.each(fileStores, function (fileStore) {
                    fileStore.isFileStore = true;
                    $http.get("/api/filedirectory/" + tenantId + "/" + fileStore.id, { cache: false }).success(function (data) {
                        fileStore.children = data;

                        _.each(data, function(d) {
                            d.parentIsShared = false;
                            markDirectoriesWhenParentsAreShared(d, false);
                        });
                    });
                });
            });

            $http.get("/api/filedirectory/" + tenantId + "/shared", { cache: false }).success(function (directories) {
                $scope.sharedDirectoryRoot.children = directories;

                _.each(directories, function (d) {
                    markDirectoryIsShared(d);
                });

                var treeRootScope = angular.element(document.getElementById("tree-root-shared")).scope();
                treeRootScope.collapseAll();
            });
        };

        $scope.renameFile = function(file) {
            dialogService.singleInputDialog(renameFile, name, file.name, function (newName) {
                var dto = {
                    name: newName
                };
                $http.put("/api/file/" + file.id + "/rename", dto).success(function() {
                    file.name = newName;
                }).error(function(error) {
                    toastService.error(errorRenameFile, error);
                });
            });
        };

        $scope.editFile = function (file) {
            $http.get(file.downloadUrl, { cache: false }).success(function (content) {
                dialogService.fileEditorDialog(file.fullname, content, function (newContent) {
                    var url = String.format("/api/file/replace/{0}/content", file.id);
                    var dto = {
                        content: newContent
                    };
                    $http.post(url, dto).success(function (newVersion) {
                        var downloadUrl = String.format("/download/{0}/{1}/{2}?isTracked=false", file.id, newVersion, file.name);
                        file.downloadUrl = downloadUrl;
                    }).error(function(error) {
                        $log.error(error);
                    });
                });
            }).error(function(error) {
                $log.error(error);
            });
        };

        $scope.uploadFiles = function() {
            dialogService.fileUploadDialog(uploadFiles, $scope.selectedDirectory.id, function() {
                getFiles($scope.selectedDirectory.id);
            });
        };

        $scope.deleteFile = function (file) {
            dialogService.confirmDialog(deleteFile, String.format(confirmDeleteFile, file.fullname), function() {
                $http.delete("/api/file/" + file.id).success(function () {
                    $scope.files = _.reject($scope.files, function (o) { return o.id === file.id; });
                }).error(function(error) {
                    toastService.error(errorDeleteFile, error);
                });
            });
        };

        $scope.replaceFile = function (file) {
            dialogService.fileReplaceDialog(String.format(replaceFileSubject, file.fullname), file.id, function () {
                getFiles($scope.selectedDirectory.id);
            });
        };

        $scope.shareDirectory = function (directory) {
            var existingTenantIds = [];

            if (directory.shared) {
                existingTenantIds = _.map(directory.shared, function(t) { return t.id; });
            }

            dialogService.selectTenantsDialog(existingTenantIds, function (tenants) {
                var ids = _.map(tenants, function (t) {
                    return t.id;
                });
                var dto = {
                    tenantIds: ids
                };
                var url = String.format("/api/filedirectory/{0}/share", directory.id);
                $http.put(url, dto).success(function () {
                    directory.shared = tenants;
                });
            });
        };

        $scope.donotshareDirectory = function(directory) {
            var url = String.format("/api/filedirectory/{0}/share", directory.id);
            var dto = {
                tenantIds: []
            };
            return $http.put(url, dto).success(function () {
                directory.shared = [];
            });
        };

        $scope.getNames = function(tenants) {
            var names = "";
            _.each(tenants, function(t) {
                names += t.name + ", ";
            });
            names = names.trim();
            return names.substring(0, names.length - 1);
        };

        $scope.createDirectory = function() {
            dialogService.singleInputDialog(createDirectory, name, "", function(newName) {
                var t = tenantService.getTenant().id;
                var p = "0";
                var fs = "0";

                if (!$scope.selectedDirectory.isFileStore) {
                    p = $scope.selectedDirectory.id || "0";
                    fs = $scope.selectedDirectory.fileStoreId;
                } else {
                    fs = $scope.selectedDirectory.id;
                }

                var dto = {
                    name: newName,
                    fileStoreId: fs,
                };

                $http.post("/api/filedirectory/" + t + "/" + p, dto).success(function (newDir) {
                    newDir.children = [];
                    if ($scope.selectedDirectory) {
                        $scope.selectedDirectory.children.push(newDir);
                    } else {
                        $scope.directories.push(newDir);
                    }

                    // TODO: potentieller parent aufklappen
                    // TODO: neuer Knoten selektieren
                }).error(function(error) {
                    toastService.error(errorCreateDirectory, error);
                });
            });
        };

        $scope.searchFiles = function() {
            if ($scope.fileSearchText) {
                $scope.selectedDirectory = null;
                getFiles("search/" + tenantService.getTenant().id + "?search=" + $scope.fileSearchText);
            }
        };

        $scope.deleteDirectory = function(directory) {
            dialogService.confirmDialog(deleteDirectory, String.format(confirmDeleteDirectory, directory.node.name), function () {
                $http.delete("/api/filedirectory/" + directory.node.id).success(function () {
                    directory.remove();
                }).error(function(error) {
                    toastService.error(errorDeleteDirectory, error);
                });
            });
        };

        $scope.renameDirectory = function(directory) {
            dialogService.singleInputDialog(renameDirectory, name, directory.name, function (newName) {
                var dto = {
                    name: newName
                };
                $http.put("/api/filedirectory/" + directory.id, dto).success(function () {
                    directory.name = newName;
                }).error(function(error) {
                    toastService.error(errorRenameDirectory, error);
                });
            });
        };

        $scope.trackFile = function (file) {
            var url = String.format("/api/file/{0}/track", file.id);
            return $http.post(url).success(function() {
                file.isTracked = true;
            }).error(function () {
                toastService.error(errorEnableFileTracking, error);
            });
        };

        $scope.untrackFile = function (file) {
            var url = String.format("/api/file/{0}/untrack", file.id);
            return $http.post(url).success(function () {
                file.isTracked = false;
            }).error(function () {
                toastService.error(errorDisableFileTracking, error);
            });
        };

        $rootScope.$on("tenantChanged", function () {
            $scope.files = [];
            getDirectories();
        });

        $rootScope.$on("logout", function() { $scope.directories = []; });

        getDirectories();
    }]);
}());
(function() {
    angular.module("app").controller("FormsController", ["$rootScope", "$scope", "$http", "$location", "tenantService", "dialogService", "toastService", "$translate", function ($rootScope, $scope, $http, $location, tenantService, dialogService, toastService, $translate) {
        $scope.forms = [];

        $scope.select = function(form) {
            $scope.selectedForm = form;
        };

        $scope.edit = function (form) {
            $location.path("/content/form/" + form.id);
        };

        $http.get("/api/permission/form/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        var tName = "";
        var renameForm = "";
        var errorRenameForm = "";
        var createForm = "";
        var errorCreateForm = "";
        var deleteForm = "";
        var confirmDeleteForm = "";
        var errorDeleteForm = "";
        var duplicateForm = "";
        var errorDuplicateForm = "";

        $translate(["rename_form", "error_rename_form", "create_form", "error_create_form", "duplicate_form", "error_duplicate_form", "name", "delete_form", "confirm_delete_form", "error_delete_form"]).then(function(t) {
            tName = t.name;
            renameForm = t.rename_form;
            errorRenameForm = t.error_rename_form;
            createForm = t.create_form;
            errorCreateForm = t.error_create_form;
            deleteForm = t.delete_form;
            confirmDeleteForm = t.confirm_delete_form;
            errorDeleteForm = t.error_delete_form;
            duplicateForm = t.duplicate_form;
            errorDuplicateForm = t.error_duplicate_form;
        });

        $scope.rename = function (form) {
            dialogService.singleInputDialog(renameForm, tName, form.name, function (name) {
                var dto = {
                    name: name
                };
                $http.post("/api/form/" + form.id + "/name", dto).success(function () {
                    form.name = name;
                }).error(function (error) {
                    toastService.error(errorRenameForm, error);
                });
            });
        };

        $scope.create = function() {
            dialogService.singleInputDialog(createForm, tName, "", function (name) {
                var dto = {
                    name: name
                };
                $http.post("/api/forms/" + tenantService.getTenant().id, dto).success(function (data) {
                    $scope.forms.push(data);
                }).error(function (error) {
                    toastService.error(errorCreateForm, error);
                });
            });
        };

        $scope.duplicate = function (form) {
            dialogService.singleInputDialog(duplicateForm, tName, form.name, function (name) {
                var dto = {
                    name: name
                };
                $http.post("/api/form/" + form.id + "/duplicate", dto).success(function (data) {
                    $scope.forms.push(data);
                }).error(function (error) {
                    toastService.error(errorDuplicateForm, error);
                });
            });
        };

        $scope.delete = function(form) {
            dialogService.confirmDialog(deleteForm, String.format(confirmDeleteForm, form.fullname), function() {
                $http.delete("/api/form/" + form.id).success(function() {
                    $scope.forms = _.reject($scope.forms, function(f) { return f.id === form.id; });
                }).error(function(error) {
                    toastService.error(errorDeleteForm, error);
                });
            });
        };

        var getForms = function () {
            $scope.forms = [];
            $http.get("/api/forms/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
                $scope.forms = data;
            });
        };

        $rootScope.$on('logout', function () {
            $scope.forms = [];
        });

        $rootScope.$on("tenantChanged", getForms);

        getForms();
    }]);

    angular.module("app").controller("FormController", ["$rootScope", "$scope", "$http", "$routeParams", "dialogService", "toastService", "$translate", function ($rootScope, $scope, $http, $routeParams, dialogService, toastService, $translate) {
        var formId = $routeParams.formId;
        $scope.formId = formId;

        $http.get("/api/form/" + formId, { cache: false }).success(function(data) {
            $scope.form = data;

            $http.get("/api/form/" + $scope.formId + "/fields", { cache: false }).success(function (f) {
                $scope.form.fields = f;
            });
        });

        var tName = "";
        var renameForm = "";
        var errorRenameForm = "";

        $translate(["rename_form", "error_rename_form", "name"]).then(function(t) {
            tName = t.name;
            renameForm = t.rename_form;
            errorRenameForm = t.error_rename_form;
        });

        $scope.editName = function() {
            dialogService.singleInputDialog(renameForm, tName, $scope.form.name, function (name) {
                var dto = {
                    name: name
                };
                $http.post("/api/form/" + formId + "/name", dto).success(function() {
                    $scope.form.name = name;
                }).error(function(error) {
                    toastService.error(errorRenameForm, error);
                });
            });
        };

        $rootScope.$on('logout', function () { $scope.form = {}; });
    }]);

    angular.module("app").controller("FormEntriesController", ["$scope", "$http", "$modal", "$timeout", "dialogService", "toastService", "$translate", function ($scope, $http, $modal, $timeout, dialogService, toastService, $translate) {
        var isLoading = true;
        $scope.entriesLoaded = false;
        $scope.selectedRow = {};
        $scope.columnFields = [];

        $scope.select = function(entry) {
            $scope.selectedRow = entry;
        };

        var editFormEntry = "";
        var errorEditFormEntry = "";
        var deleteFormEntry = "";
        var confirmDeleteFormEntry = "";
        var errorDeleteFormEntry = "";
        var deleteAllFormEntries = "";
        var confirmDeleteAllFormEntries = "";
        var errorDeleteAllFormEntries = "";

        $translate(["edit_form_entry", "error_edit_form_entry", "delete_form_entry", "confirm_delete_form_entry", "error_delete_form_entry", "delete_all_form_entries", "confirm_delete_all_form_entries", "error_delete_all_form_entries"]).then(function (t) {
            editFormEntry = t.edit_form_entry;
            errorEditFormEntry = t.error_edit_form_entry;
            deleteFormEntry = t.delete_form_entry;
            confirmDeleteFormEntry = t.confirm_delete_form_entry;
            errorDeleteFormEntry = t.error_delete_form_entry;
            deleteAllFormEntries = t.delete_all_form_entries;
            confirmDeleteAllFormEntries = t.confirm_delete_all_form_entries;
            errorDeleteAllFormEntries = t.error_delete_all_form_entries;
        });

        $scope.create = function () {
            var fields = [];
            _.each($scope.form.fields, function (f) {
                fields.push(f.name);
            });

            var copy = {};
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/forms/editFormEntryDialog.min.html",
                controller: "EditFormEntryDialogController",
                resolve: {
                    entry: function () { return copy; },
                    subject: function () { return editFormEntry; },
                    fields: function () { return fields; },
                }
            });

            modalInstance.result.then(function (e) {
                var url = String.format("/api/form/{0}/entries", $scope.formId);
                $http.post(url, e).success(function () {
                    //isLoading = true;
                    //$scope.entriesLoaded = false;
                    //$timeout(function () {
                    //    $scope.loadEntries();
                    //}, 3000);
                }).error(function (error) {
                    toastService.error(errorEditFormEntry, error);
                });
            });
        };

        $scope.edit = function (entry) {
            var urlGet = String.format("/api/form/{0}/fields", $scope.formId);
            $http.get(urlGet, { cache: false }).success(function (data) {
                var fields = [];
                _.each(data, function(f) {
                    fields.push(f.name);
                });
                _.each(_.keys(entry.values), function (k) {
                    if (!_.find(fields, function (f) { return f.toUpperCase() === k.toUpperCase(); })) {
                        fields.push(k);
                    }
                });

                var copy = angular.copy(entry);
                var modalInstance = $modal.open({
                    backdrop: 'static',
                    templateUrl: "/admin/views/forms/editFormEntryDialog.min.html",
                    controller: "EditFormEntryDialogController",
                    resolve: {
                        entry: function () { return copy; },
                        subject: function () { return editFormEntry; },
                        fields: function () { return fields; },
                    }
                });

                modalInstance.result.then(function (e) {
                    var url = String.format("/api/form/{0}/entry/{1}", $scope.formId, entry.id);
                    $http.post(url, e).success(function () {
                        //isLoading = true;
                        //$scope.entriesLoaded = false;
                        //$timeout(function () {
                        //    $scope.loadEntries();
                        //}, 3000);
                    }).error(function (error) {
                        toastService.error(errorEditFormEntry, error);
                    });
                });
            });
        };

        $scope.downloadCsv = function() {
            $http.get("/api/form/" + $scope.formId + "/entries/csv", { responseType: 'arraybuffer' }).success(function (data, status, headers) {
                headers = headers();
                var filename = headers['x-filename'] || headers['filename'] || 'form_entries.csv';
                var blob = new Blob([data], { type: "text/csv" });
                saveAs(blob, filename);
            });
        };

        $scope.clearEntries = function () {
            dialogService.confirmDialog(deleteAllFormEntries, confirmDeleteAllFormEntries, function() {
                $http.delete("/api/form/" + $scope.formId + "/entries").success(function () {
                    $scope.entries = [];
                }).error(function(error) {
                    toastService.error(errorDeleteAllFormEntries, error);
                });
            });
        }

        $scope.delete = function(entry) {
            dialogService.confirmDialog(deleteFormEntry, confirmDeleteFormEntry, function() {
                $http.delete("/api/form/" + $scope.formId + "/entry/" + entry.id).success(function() {
                    $scope.entries = _.reject($scope.entries, function(e) { return e.id === entry.id; });
                }).error(function(error) {
                    toastService.error(errorDeleteFormEntry, error);
                });
            });
        };

        $scope.loadEntries = function() {
            if (!isLoading) {
                return;
            }
            isLoading = false;

            $http.get("/api/form/" + $scope.formId + "/entries", { cache: false }).success(function(data) {
                $scope.entries = data;

                _.each($scope.entries, function(e) {
                    var pairs = _.pairs(e.values);
                    _.each(pairs, function (p) {
                        var k = p[0];
                        var v = p[1];
                        e[k] = v;
                        if (!_.find($scope.columnFields, function (f) { return f === k; })) {
                            $scope.columnFields.push(k);
                        }
                    });
                });

                $scope.entriesLoaded = true;
            });
        };
    }]);

    angular.module("app").controller("FormTemplateController", ["$scope", "$http", "$log", "toastService", "toastr", "$timeout", "$translate", "$modal", function ($scope, $http, $log, toastService, toastr, $timeout, $translate, $modal) {
        var isLoading = true;
        $scope.templateLoaded = false;
        $scope.loadTemplate = function() {
            if (!isLoading) {
                return;
            }
            isLoading = false;

            $http.get("/api/form/" + $scope.formId + "/template", { cache: false }).success(function (template) {
                $scope.form.template = template;

                $scope.templateLoaded = true;
            });
        };

        var successMsg = "";
        var errorMsg = "";
        var generateFormTemplateError = "";

        $translate(["save_form_template_success", "save_form_template_error", "generate_form_template_error"]).then(function (t) {
            successMsg = t.save_form_template_success;
            errorMsg = t.save_form_template_error;
            generateFormTemplateError = t.generate_form_template_error;
        });

        $scope.openGenerator = function() {
            var url = String.format("/api/form/{0}/templategenerator", $scope.formId);

            $http.get(url, { cache: false }).success(function (generator) {

                var modalInstance = $modal.open({
                    backdrop: 'static',
                    templateUrl: "/admin/views/forms/editFormTemplate.min.html",
                    controller: "FormTemplateGeneratorController",
                    resolve: {
                        generator: function () { return generator; },
                    }
                });

                modalInstance.result.then(function (g) {
                    $http.post(url, g).success(function(html) {
                        $scope.form.template = html;
                    }).error(function(error) {
                        toastService.error(generateFormTemplateError, error);
                    });
                });
            });
        };

        $scope.saveTemplate = function() {
            var dto = {
                template: $scope.form.template,
            };
            return $http.post("/api/form/" + $scope.formId + "/template", dto).success(function () {
                toastr.success('', successMsg, { allowHtml: false, timeOut: 1000 });
            }).error(function (error) {
                $log.error(angular.toJson(error));
                toastService.error(errorMsg, error);
            });
        };
    }]);

    angular.module("app").controller("FormTemplateGeneratorController", ["$scope", "$modalInstance", "$http", "$log", "generator", function($scope, $modalInstance, $http, $log, generator) {
        $scope.generator = generator;

        $scope.ok = function () {
            $modalInstance.close($scope.generator);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);

    angular.module("app").controller("FormActionsController", ["$scope", "$http", "$log", "$modal", "toastService", "dialogService", "$translate", function ($scope, $http, $log, $modal, toastService, dialogService, $translate) {
        var isLoading = true;
        $scope.actionLoaded = false;
        $scope.loadActions = function () {
            if (!isLoading) {
                return;
            }
            isLoading = false;

            $http.get("/api/form/" + $scope.formId + "/actions", { cache: false }).success(function(data) {
                $scope.actions = data;
                _.each($scope.actions, setDescriptionForAction);

                $scope.actionLoaded = true;
            });
        };

        $scope.selectAction = function(action) {
            $scope.selectedAction = action;
        };

        var createAction = "";
        var errorCreateAction = "";
        var editAction = "";
        var errorEditAction = "";
        var deleteAction = "";
        var confirmDeleteAction = "";
        var errorDeleteAction = "";
        var typeUndefined = "";
        var descEmailToAdd = "";
        var descEmailToField = "";
        var descPersist = "";
        var descForward = "";
        var descCreateObject = "";

        $translate(["create_form_action", "error_create_form_action", "edit_form_action", "error_edit_form_action", "delete_form_action", "confirm_delete_form_action", "error_delete_form_action", "form_action_type_undefined", "form_action_description_send_email_to_address", "form_action_description_send_email_to_field", "form_action_description_persist", "form_action_description_forward", "form_action_description_create_object"]).then(function(t) {
            createAction = t.create_form_action;
            errorCreateAction = t.error_create_form_action;
            editAction = t.edit_form_action;
            errorEditAction = t.error_edit_form_action;
            deleteAction = t.delete_form_action;
            confirmDeleteAction = t.confirm_delete_form_action;
            errorDeleteAction = t.error_delete_form_action;
            typeUndefined = t.form_action_type_undefined;
            descEmailToAdd = t.form_action_description_send_email_to_address;
            descEmailToField = t.form_action_description_send_email_to_field;
            descPersist = t.form_action_description_persist;
            descForward = t.form_action_description_forward;
            descCreateObject = t.form_action_description_create_object;
        });

        $scope.addAction = function () {
            var action = { type: "", configuration: null };
            openActionDialog(createAction, action, function(a) {
                $http.post("/api/form/" + $scope.form.id + "/action", a).success(function (data) {
                    setDescriptionForAction(data);
                    $scope.actions.push(data);
                }).error(function(error) {
                    toastService.error(errorCreateAction, error);
                });
            });
        };

        $scope.editAction = function (action) {
            openActionDialog(editAction, action, function(a) {
                $http.post("/api/form/" + $scope.form.id + "/action/" + action.id, a).success(function() {
                    action.type = a.type;
                    action.configuration = a.configuration;
                    setDescriptionForAction(action);
                }).error(function (error) {
                    toastService.error(errorEditAction, error);
                });
            });
        };

        $scope.removeAction = function(action) {
            dialogService.confirmDialog(deleteAction, confirmDeleteAction, function() {
                $http.delete("/api/form/" + $scope.form.id + "/action/" + action.id).success(function() {
                    $scope.actions = _.reject($scope.actions, function(a) { return a.id === action.id; });
                }).error(function (error) {
                    toastService.error(errorDeleteAction, error);
                });
            });
        };

        function openActionDialog(subject, action, onAccept) {
            var copy = angular.copy(action);
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/forms/editFormActionDialog.min.html",
                controller: "EditFormActionDialogController",
                size: 'lg',
                resolve: {
                    action: function () { return copy; },
                    subject: function () { return subject; },
                    fields: function () { return $scope.form.fields; },
                }
            });

            modalInstance.result.then(onAccept);
        };

        function setDescriptionForAction(action) {
            if (!action.type) {
                action.description = typeUndefined;
                return;
            }

            try {
                if (action.type === "SendEmailToAddress") {
                    action.description = String.format(descEmailToAdd, action.configuration.emailAddress);
                } else if (action.type === "Persist") {
                    action.description = descPersist;
                } else if (action.type === "SendEmailToField") {
                    action.description = String.format(descEmailToField, action.configuration.fieldName);
                } else if (action.type === "PageForwarding") {
                    action.description = String.format(descForward, action.configuration.fullname);
                } else if (action.type === "CreateObject") {
                    action.description = descCreateObject;
                } else {
                    action.description = action.type + " is not supported";
                }
            } catch (e) {
                action.description = "Error while generating description.";
                $log.error(e);
            } 
        };
    }]);

    angular.module("app").controller("FormFieldsController", ["$scope", "$http", "$modal", "toastService", "dialogService", "$translate", function ($scope, $http, $modal, toastService, dialogService, $translate) {
        $scope.selectedField = function(field) {
            $scope.selectedField = field;
        };

        var tName = "";
        var createField = "";
        var errorCreateField = "";
        var renameField = "";
        var errorRenameField = "";
        var deleteField = "";
        var confirmDeleteField = "";
        var errorDeleteField = "";

        $translate(["name", "create_form_field", "error_create_form_field", "rename_form_field", "error_rename_form_field", "delete_form_field", "confirm_delete_form_field", "error_delete_form_field"]).then(function (t) {
            tName = t.name;
            createField = t.create_form_field;
            errorCreateField = t.error_create_form_field;
            renameField = t.rename_form_field;
            errorRenameField = t.error_rename_form_field;
            deleteField = t.delete_form_field;
            confirmDeleteField = t.confirm_delete_form_field;
            errorDeleteField = t.error_delete_form_field;
        });

        $scope.addField = function() {
            dialogService.singleInputDialog(createField, tName, "", function (name) {
                var field = { name: name };
                $http.post("/api/form/" + $scope.form.id + "/fields", field).success(function (data) {
                    $scope.form.fields.push(data);
                }).error(function(error) {
                    toastService.error(errorCreateField, error);
                });
            });
        };

        $scope.editField = function (field) {
            dialogService.singleInputDialog(renameField, tName, field.name, function (name) {
                var f = { id: field.id, name: name };
                $http.post("/api/form/" + $scope.form.id + "/field/" + field.id, f).success(function () {
                    field.name = name;
                }).error(function (error) {
                    toastService.error(errorRenameField, error);
                });
            });
        };

        $scope.removeField = function (field) {
            dialogService.confirmDialog(deleteField, String.format(confirmDeleteField, field.name), function () {
                $http.delete("/api/form/" + $scope.form.id + "/field/" + field.id).success(function () {
                    $scope.form.fields = _.reject($scope.form.fields, function (f) { return f.id === field.id; });
                }).error(function (error) {
                    toastService.error(errorDeleteField, error);
                });
            });
        };
    }]);

    angular.module("app").controller("EditFormActionDialogController", ["$scope", "$modalInstance", "$http", "$log", "tenantService", "$translate", "subject", "action", "dialogService", "fields", function ($scope, $modalInstance, $http, $log, tenantService, $translate, subject, action, dialogService, fields) {
        $scope.action = action;
        $scope.subject = subject;
        $scope.fields = fields;

        $scope.tinymceOptions = {
            theme: "modern",
            relative_urls: true,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code hr wordcount textcolor colorpicker",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media"
        };

        $scope.$watch("action.type", function (newValue, oldValue) {
            if ($scope.action.type.toUpperCase() === "PERSIST") {
                $scope.action.configuration = {};
                return;
            }

            if (!oldValue || newValue === oldValue) {
                return;
            }

            $scope.action.configuration = null;
        });

        $scope.$watch("action.configuration.fieldId", function() {
            if (action && action.configuration && action.configuration.fieldId) {
                var field = _.find($scope.fields, function (f) { return f.id === action.configuration.fieldId; });
                if (field) {
                    action.configuration.fieldName = field.name;
                }
            }
        });

        $translate(["form_action_send_email_to_address", "form_action_send_email_to_field", "form_action_persist", "form_action_forward", "form_action_create_object"]).then(function(t) {
            $scope.types = [
                { name: "SendEmailToField", description: t.form_action_send_email_to_field },
                { name: "SendEmailToAddress", description: t.form_action_send_email_to_address },
                { name: "PageForwarding", description: t.form_action_forward },
                { name: "CreateObject", description: t.form_action_create_object },
                { name: "Persist", description: t.form_action_persist }
            ];
        });

        $scope.getPages = function (value) {
            var url = String.format("/api/page/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, { cache: false}).then(function (response) {
                return response.data;
            });
        };

        $scope.browsePage = function () {
            var p = action.configuration;
            dialogService.selectPageDialog(p, function (page) {
                if (!action.configuration) {
                    action.configuration = {};
                }
                if (!page) {
                    return;
                }
                page.children = [];
                action.configuration = page;
            });
        };

        $scope.definitions = [];

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
            $scope.definitions = data;
        });

        $scope.$watch("action.configuration.objectDefinitionId", function() {
            if (action && action.configuration && action.configuration.objectDefinitionId) {
                var definition = _.find($scope.definitions, function (d) { return d.id === action.configuration.objectDefinitionId; });
                if (definition) {
                    var url = String.format("/api/objectdefinition/{1}/{0}/detail", definition.id, tenantService.getTenant().id);

                    action.configuration.objectDefinitionName = definition.name;

                    $http.get(url, { cache: false }).success(function(data) {
                        $scope.definition = data;
                    });
                }
            } else {
                $scope.definition = {};
            }
        });

        $scope.ok = function () {
            if (!$scope.action.configuration) {
                $scope.action.configuration = {};
            }
            $modalInstance.close($scope.action);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);

    angular.module("app").controller("EditFormEntryDialogController", ["$scope", "$modalInstance", "$http", "subject", "entry", "fields", function($scope, $modalInstance, $http, subject, entry, fields) {
        $scope.subject = subject;
        $scope.entry = entry;
        $scope.fields = fields;

        $scope.values = {};

        _.each(fields, function (f) {
            _.each(_.pairs(entry.values), function(pair) {
                var key = pair[0];
                var value = pair[1];

                if (f.toUpperCase() == key.toUpperCase()) {
                    $scope.values[f] = value;
                }
            });
        });

        $scope.ok = function () {
            $modalInstance.close($scope.values);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());
(function() {
    angular.module("app").controller("TenantController", [
        "$rootScope", "$scope", "$http", "$log", "dialogService", "tenantService", "$translate", "toastService",
        function ($rootScope, $scope, $http, $log, dialogService, tenantService, $translate, toastService) {

        $scope.selectedTenant = [];

        function getScopeDataFromTenantService() {
            $scope.tenants = tenantService.getTenants();
            $scope.currentTenant = tenantService.getTenant();

            if ($scope.currentTenant) {
                $http.get("/api/permission/tenant/" + $scope.currentTenant.id, { cache: false }).success(function (permission) {
                    $scope.canEdit = permission.canEdit;
                    $scope.canAdmin = permission.canAdmin;
                });
            }
        };

        getScopeDataFromTenantService();

        $scope.setTenant = function (tenant) {
            $scope.currentTenant = tenant;
            tenantService.setTenant($scope.currentTenant);
        };

        $scope.selectTenant = function(tenant) {
            $scope.selectedTenant = tenant;
        }

        var createTenant = "";
        var name = "";
        var errorCreateTenant = "";
        var renameTenant = "";
        var errorRenameTenant = "";
        var deleteTenant = "";
        var confirmDeleteTenant = "";
        var errorDeleteTenant = "";

        $translate(["create_tenant", "name", "error_create_tenant", "rename_tenant", "error_rename_tenant", "delete_tenant", "confirm_delete_tenant", "error_delete_tenant"]).then(function(t) {
            createTenant = t.create_tenant;
            name = t.name;
            errorCreateTenant = t.error_create_tenant;
            renameTenant = t.rename_tenant;
            errorRenameTenant = t.error_rename_tenant;
            deleteTenant = t.delete_tenant;
            confirmDeleteTenant = t.confirm_delete_tenant;
            errorDeleteTenant = t.error_delete_tenant;
        });

        $scope.createTenant = function() {
            dialogService.singleInputDialog(createTenant, name, "", function (n) {
                var dto = {
                    name: n
                };
                $http.put("/api/tenant", dto).success(function(tenant) {
                    $scope.tenants.push(tenant);
                    tenantService.addTenant(tenant);
                }).error(function(error) {
                    toastService.error(errorCreateTenant, error);
                });
            });
        };

        $scope.renameTenant = function (tenant) {
            dialogService.singleInputDialog(renameTenant, name, tenant.name, function (newName) {
                var dto = {
                    name: newName
                };
                $http.post("/api/tenant/" + tenant.id, dto).success(function() {
                    tenant.name = newName;
                }).error(function(error) {
                    toastService.error(errorRenameTenant, error);
                });
            });
        };

        $scope.deleteTenant = function(tenant) {
            dialogService.confirmDialog(deleteTenant, String.format(confirmDeleteTenant, tenant.name), function() {
                $http.delete("/api/tenant/" + tenant.id).success(function() {
                    $scope.tenants = _.reject($scope.tenants, function(t) { return t.id === tenant.id; });
                }).error(function(error) {
                    toastService.error(errorDeleteTenant, error);
                });
            });
        };

        $rootScope.$on('tenantChanged', getScopeDataFromTenantService);
        $rootScope.$on('tenantsChanged', getScopeDataFromTenantService);
        }]);
}());
(function() {
    angular.module("app").provider("tenantService", function() {
        var current = null;
        var allTenants = [];

        this.$get = [
            "$rootScope", "$http", "$log", "$q", "localStorageService", "currentTenant", "tenants", "systemMessageService",
            function ($rootScope, $http, $log, $q, localStorageService, currentTenant, tenants, systemMessageService) {

                $rootScope.$on("logout", function() {
                    allTenants = [];
                    localStorageService.set("tenants", allTenants);
                });

                allTenants = tenants;
                current = currentTenant;

                return {
                    reloadTenants: function () {
                        var deferred = $q.defer();
                        allTenants = [];
                        localStorageService.set("tenants", allTenants);
                        $http.get("/api/tenants", { cache: false }).success(function (data) {
                            allTenants = data;
                            localStorageService.set("tenants", allTenants);
                            var storedCurrentTenant = localStorageService.get('currentTenant');
                            if (storedCurrentTenant) {
                                current = _.find(allTenants, function (t) { return t.id == storedCurrentTenant.id; });
                            }
                            if (current == null) {
                                current = allTenants[0];
                            }
                            $rootScope.$broadcast('tenantChanged');
                            deferred.resolve(current);
                        }).error(function (error) {
                            deferred.reject(error);
                        });
                        return deferred.promise;
                    },
                    setTenant: function (tenant) {
                        current = tenant;
                        angular.module("app").value("currentTenant", tenant);
                        localStorageService.set('currentTenant', tenant);
                        $rootScope.$broadcast('tenantChanged', tenant);
                    },
                    getTenant: function () {
                        if (current) {
                            return current;
                        }
                        if (!allTenants || allTenants.length == 0) {
                            allTenants = localStorageService.get("tenants");
                        }
                        if (allTenants && allTenants.length > 0) {
                            var storedCurrentTenant = localStorageService.get('currentTenant');
                            if (storedCurrentTenant) {
                                current = _.find(allTenants, function (t) { return t.id == storedCurrentTenant.id; });
                                return current;
                            }
                        }
                        return null;
                    },
                    getTenants: function () {
                        if (allTenants && allTenants.length > 0) {
                            return allTenants.slice(0);
                        }
                        allTenants = localStorageService.get("tenants");
                        return allTenants && allTenants.length > 0 ? allTenants.slice(0) : [];
                    },
                    addTenant: function(tenant) {
                        allTenants.push(tenant);
                        localStorageService.set("tenants", allTenants);
                        $rootScope.$broadcast('tenantsChanged');
                    },
                };
            }
        ];
    });
}());
(function() {
    angular.module("app").controller("WebsitesController", [
        "$rootScope", "$scope", "$http", "$log", "tenantService", "$location", "dialogService", "toastService", "$translate",
        function ($rootScope, $scope, $http, $log, tenantService, $location, dialogService, toastService, $translate) {

        $scope.websites = [];
        $scope.selectedWebsite = {};

        var getWebsites = function () {
            $scope.websites = [];
            $http.get("/api/websites/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
                $scope.websites = data;
            });
        };

        $http.get("/api/permission/website/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.selectWebsite = function(website) {
            $scope.selectedWebsite = website;
        };

        var renameWebsite = "";
        var name = "";
        var errorRenameWebsite = "";
        var createWebsite = "";
        var errorCreateWebsite = "";
        var deleteWebsite = "";
        var confirmDeleteWebsite = "";
        var errorDeleteWebsite = "";

        $translate(["name", "rename_website", "error_rename_website", "create_website", "error_create_website", "delete_website", "confirm_delete_website", "error_delete_website"]).then(function(t) {
            renameWebsite = t.rename_website;
            name = t.name;
            errorRenameWebsite = t.error_rename_website;
            createWebsite = t.create_website;
            errorCreateWebsite = t.error_create_website;
            deleteWebsite = t.delete_website;
            confirmDeleteWebsite = t.confirm_delete_website;
            errorDeleteWebsite = t.error_delete_website;
        });

        $scope.renameWebsite = function (website) {
            dialogService.singleInputDialog(renameWebsite, name, website.name, function (newName) {
                var dto = {
                    name: newName
                };
                $http.post("/api/website/" + website.id, dto).success(function() {
                    website.name = newName;
                });
            });
        };

        $scope.createWebsite = function () {
            dialogService.singleInputDialog(createWebsite, name, "", function(n) {
                var dto = {
                    name: n
                };
                $http.put("/api/website/" + tenantService.getTenant().id, dto).success(function (data) {
                    $scope.websites.push(data);
                }).error(function(error) {
                    toastService.error(errorCreateWebsite, error);
                });
            });
        };

        $scope.deleteWebsite = function (website) {
            var n = String.format("{0} (Nr. {1})", website.name, website.nr);
            dialogService.confirmDialog(deleteWebsite, String.format(confirmDeleteWebsite, n), function () {
                $http.delete("/api/website/" + website.id).success(function() {
                    $scope.websites = _.reject($scope.websites, function(w) { return w.id === website.id; });
                }).error(function(error) {
                    toastService.error(errorDeleteWebsite, error);
                });
            });
        };

        $scope.editWebsite = function(website) {
            $location.path("/master/website/" + website.id);
        };

        $rootScope.$on("tenantChanged", getWebsites);

        $rootScope.$on("logout", function () {
            $scope.websites = [];
        });

        getWebsites();
    }]);

    angular.module("app").controller("WebsiteController", ["$scope", "$http", "$log", "$routeParams", "dialogService", "$translate", "toastService", function ($scope, $http, $log, $routeParams, dialogService, $translate, toastService) {
        var websiteId = $routeParams.websiteId;
        $scope.website = {};
        $scope.selectedHost = [];

        var tAddDomain = "";
        var tErrorAddDomain = "";
        var tRenameDomain = "";
        var tErrorRenameDomain = "";
        var tDeleteDomain = "";
        var tConfirmDeleteDomain = "";
        var tErrorDeleteDomain = "";

        $translate(["add_domain", "error_add_domain", "rename_domain", "error_rename_domain", "delete_domain", "confirm_delete_domain", "error_delete_domain"]).then(function (t) {
            tAddDomain = t.add_domain;
            tErrorAddDomain = t.error_add_domain;
            tRenameDomain = t.rename_domain;
            tErrorRenameDomain = t.error_rename_domain;
            tDeleteDomain = t.delete_domain;
            tConfirmDeleteDomain = t.confirm_delete_domain;
            tErrorDeleteDomain = t.error_delete_domain;
        });

        $http.get("/api/website/" + websiteId, { cache: false }).success(function (data) {
            $scope.website = data;
        });

        $scope.selectHost = function(domain) {
            $scope.selectedHost = domain.host;
        };

        $scope.setDefault = function (domain) {
            var dto = {
                domain: domain.host
            };
            return $http.post("/api/website/" + websiteId + "/default/", dto).success(function() {
                angular.forEach($scope.website.domains, function(d) {
                    d.isDefault = false;
                });
                domain.isDefault = true;
            });
        };

        $scope.addDomain = function() {
            dialogService.singleInputDialog(tAddDomain, "Host", "", function (name) {
                var dto = {
                    domain: name
                };
                $http.put("/api/website/" + websiteId + "/domain/", dto).success(function () {
                    $scope.website.domains.push({ host: name, isDefault: false });
                }).error(function (error) {
                    toastService.error(tErrorAddDomain, error);
                });
            });
        };

        $scope.deleteDomain = function (domain) {
            dialogService.confirmDialog(tDeleteDomain, String.format(tConfirmDeleteDomain, domain.host), function () {
                var url = String.format("/api/website/{0}/domain?name={1}", websiteId, domain.host);
                $http.delete(url).success(function () {
                    $scope.website.domains = _.reject($scope.website.domains, function (d) {
                        return d.host === domain.host;
                    });
                }).error(function (error) {
                    toastService.error(tErrorDeleteDomain, error);
                });
            });
        };

        $scope.renameDomain = function(domain) {
            dialogService.singleInputDialog(tRenameDomain, "Host", domain.host, function (newName) {
                var dto = {
                    oldDomain: domain.host,
                    newDomain: newName
                };
                $http.post("/api/website/" + websiteId + "/domain/", dto).success(function() {
                    domain.host = newName;
                }).error(function (error) {
                    toastService.error(tErrorRenameDomain, error);
                });
            });
        };
    }]);
}());

(function() {
    angular.module("app").controller("ObjectDefinitionsController", ["$scope", "$rootScope", "$http", "$log", "$location", "tenantService", "dialogService", "toastService", "$translate", function ($scope, $rootScope, $http, $log, $location, tenantService, dialogService, toastService, $translate) {
        $scope.definitions = [];
        $scope.selectedDefinition = {};
        $scope.searchText = '';

        $rootScope.$on('logout', function () {
            $scope.definitions = [];
        });

        $scope.select = function (node) {
            $scope.selectedDefinition = node.node;
        };

        $http.get("/api/permission/objectdefinition/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        var deleteObjectDefinition = "";
        var confirmDeleteObjectDefinition = "";
        var errorDeleteObjectDefinition = "";
        var errorStarObjectDefinition = "";
        var errorUnstarObjectDefinition = "";
        var createObjectDefinition = "";
        var errorCreateObjectDefinition = "";
        var name = "";
        var renameObjectDefinition = "";
        var errorRenameObjectDefinition = "";

        $translate(["delete_object_definition", "confirm_delete_object_definition", "error_delete_object_definition", "error_star_object_definition", "error_unstar_object_definition", "create_object_definition", "error_create_object_definition", "name", "rename_object_definition", "error_rename_object_definition"]).then(function (t) {
            deleteObjectDefinition = t.delete_object_definition;
            confirmDeleteObjectDefinition = t.confirm_delete_object_definition;
            errorDeleteObjectDefinition = t.error_delete_object_definition;
            errorStarObjectDefinition = t.error_star_object_definition;
            errorUnstarObjectDefinition = t.error_unstar_object_definition;
            createObjectDefinition = t.create_object_definition;
            errorCreateObjectDefinition = t.error_create_object_definition;
            name = t.name;
            renameObjectDefinition = t.rename_object_definition;
            errorRenameObjectDefinition = t.error_rename_object_definition;
        });

        $scope.delete = function (node) {
            dialogService.confirmDialog(deleteObjectDefinition, String.format(confirmDeleteObjectDefinition, node.node.fullname), function () {
                $http.delete("/api/objectdefinition/" + node.node.id).success(function () {
                    var definitions = $scope.definitions.filter(function (item) {
                        return item.id !== node.node.id;
                    });
                    $scope.definitions = definitions;
                }).error(function(error) {
                    toastService.error(errorDeleteObjectDefinition, error);
                });
            });
        };

        $scope.rename = function (node) {
            dialogService.singleInputDialog(renameObjectDefinition, name, node.node.name, function (newName) {
                $http.put("/api/objectdefinition/" + node.node.id + "/name", { name: newName }).success(function () {
                    node.node.name = newName;
                    node.node.fullname = newName + " (Nr. " + node.node.nr+ ")";
                }).error(function (error) {
                    toastService.error(errorRenameObjectDefinition, error);
                });
            });
        };

        $scope.edit = function(node) {
            $location.path("/objects/definition/" + node.node.id);
        };

        $scope.unpin = function (node) {
            return $http.put("/api/objectdefinition/" + node.node.id + "/unstar").success(function () {
                node.node.isStarred = "false";
            }).error(function(error) {
                toastService.error(errorUnstarObjectDefinition, error);
            });
        };

        $scope.pin = function (node) {
            return $http.put("/api/objectdefinition/" + node.node.id + "/star").success(function () {
                node.node.isStarred = "true";
            }).error(function (error) {
                toastService.error(errorStarObjectDefinition, error);
            });
        };

        $scope.search = function() {
            
        };

        $scope.create = function() {
            dialogService.singleInputDialog(createObjectDefinition, name, "", function (newName) {
                var t = tenantService.getTenant().id;
                var dto = {
                    name: newName
                };
                $http.post("/api/objectdefinition/" + t, dto).success(function (definition) {
                    $scope.definitions.push(definition);
                    $location.path("/objects/definition/" + definition.id);
                }).error(function(error) {
                    toastService.error(errorCreateObjectDefinition, error);
                });
            });
        };

        var getDefinitions = function() {
            var tenantId = tenantService.getTenant().id;
            $scope.definitions = [];

            $http.get("/api/objectdefinition/" + tenantId, { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = false;
                    $scope.definitions.push(d);
                });
            });

            $http.get("/api/objectdefinition/" + tenantId + "/shared", { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = true;
                    $scope.definitions.push(d);
                });
            });
        };

        $scope.shareDefinition = function (definition) {
            var existingTenantIds = [];

            if (definition.shared) {
                existingTenantIds = _.map(definition.shared, function (t) { return t.id; });
            }

            dialogService.selectTenantsDialog(existingTenantIds, function (tenants) {
                var ids = _.map(tenants, function (t) {
                    return t.id;
                });
                var dto = {
                    tenantIds: ids
                };
                var url = String.format("/api/objectdefinition/{0}/share", definition.id);
                $http.put(url, dto).success(function () {
                    definition.shared = tenants;
                });
            });
        };

        $scope.donotshareDefinition = function (definition) {
            var url = String.format("/api/objectdefinition/{0}/share", definition.id);
            var dto = {
                tenantIds: []
            };
            return $http.put(url, dto).success(function () {
                definition.shared = [];
            });
        };

        $scope.getNames = function (tenants) {
            var names = "";
            _.each(tenants, function (t) {
                names += t.name + ", ";
            });
            names = names.trim();
            return names.substring(0, names.length - 1);
        };

        $rootScope.$on("tenantChanged", getDefinitions);

        getDefinitions();
    }]);

    angular.module("app").controller("ObjectDefinitionController", ["$scope", "$rootScope", "$routeParams", "$http", "$log", "$location", "$modal", "tenantService", "dialogService", "toastService", "$translate", "definitionFieldContentTypeService", function ($scope, $rootScope, $routeParams, $http, $log, $location, $modal, tenantService, dialogService, toastService, $translate, definitionFieldContentTypeService) {
        var definitionId = $routeParams.definitionId;
        $scope.definition = {};
        $scope.selectedField = "";

        $scope.select = function (field) {
            $scope.selectedField = field.name;
        };

        var errorMoveUp = "";
        var errorMoveDown = "";
        var addField = "";
        var errorAddField = "";
        var deleteField = "";
        var confirmDeleteField = "";
        var errorDeleteField = "";
        var changeField = "";
        var tName = "";
        var errorRenameField = "";

        $translate(["error_move_objectdefinitionfield_up", "error_move_objectdefinitionfield_down", "add_objectdefinitionfield", "error_add_objectdefinitionfield", "delete_objectdefinitionfield", "confirm_delete_objectdefinitionfield", "error_delete_objectdefinitionfield", "change_definition_field", "name", "error_rename_definition_field"]).then(function (t) {
            errorMoveUp = t.error_move_objectdefinitionfield_up;
            errorMoveDown = t.error_move_objectdefinitionfield_down;
            addField = t.add_objectdefinitionfield;
            errorAddField = t.error_add_objectdefinitionfield;
            deleteField = t.delete_objectdefinitionfield;
            confirmDeleteField = t.confirm_delete_objectdefinitionfield;
            errorDeleteField = t.error_delete_objectdefinitionfield;
            changeField = t.change_definition_field;
            tName = t.name;
            errorRenameField = t.error_rename_definition_field;
        });

        $scope.rename = function (field) {
            $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/app/modules/objects/definitionFieldDialog.min.html',
                controller: 'DefinitionFieldDialogController',
                resolve: {
                    subject: function () { return changeField; },
                    name: function () { return field.name; },
                    contentType: function () { return field.contentType; },
                    options: function () { return field.contentTypeOptions; },
                }
            }).result.then(function (f) {
                var dto = {
                    oldName: field.name,
                    newName: f.name,
                    contentTypeOptions: f.options,
                };

                $http.put("/api/objectdefinition/" + definitionId + "/field", dto).success(function () {
                    field.name = f.name;
                    field.contentTypeOptions = f.options;
                }).error(function (error) {
                    toastService.error(errorRenameField, error);
                });
            });
        };

        $scope.moveUp = function (field) {
            var dto = {
                name: field.name
            };
            return $http.put("/api/objectdefinition/" + definitionId + "/up", dto).success(function() {
                field.sortOrder -= 1.5;
                resetSortOrder();
            }).error(function(error) {
                toastService.error(errorMoveUp, error);
            });
        };

        $scope.moveDown = function (field) {
            var dto = {
                name: field.name
            };
            return $http.put("/api/objectdefinition/" + definitionId + "/down", dto).success(function () {
                field.sortOrder += 1.5;
                resetSortOrder();
            }).error(function (error) {
                toastService.error(errorMoveDown, error);
            });
        };

        $scope.markSortable = function(field) {
            var dto = {
                name: field.name
            };
            var sortable = field.isSortableField ? "notsortable" : "sortable";
            return $http.put("/api/objectdefinition/" + definitionId + "/" + sortable, dto).success(function () {
                field.isSortableField = !field.isSortableField;
            });
        };

        function resetSortOrder() {
            var fields = _.sortBy($scope.definition.fields, function (f) { return f.sortOrder; });
            _.each(fields, function (f, newIndex) {
                f.sortOrder = newIndex;
            });
            $scope.definition.fields = fields;
        }

        $scope.add = function () {
            $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/app/modules/objects/definitionFieldDialog.min.html',
                controller: 'DefinitionFieldDialogController',
                resolve: {
                    subject: function () { return addField; },
                    name: function () { return ""; },
                    contentType: function () { return ""; },
                    options: function() { return ""; },
                }
            }).result.then(function (field) {
                var f = {
                    name: field.name,
                    contentType: field.contentType,
                    contentTypeOptions: field.options
                };
                $http.put("/api/objectdefinition/" + definitionId + "/add", f).success(function () {
                    var sortOrder = 1;

                    _.each($scope.definition.fields, function(ef) {
                        if (ef.sortOrder > sortOrder) {
                            sortOrder = ef.sortOrder + 1;
                        }
                    });

                    $scope.definition.fields.push({
                        name: field.name,
                        contentType: field.contentType,
                        contentTypeTranslation: field.translation,
                        contentTypeIcon:field.icon,
                        sortOrder: sortOrder,
                        isTitle: false,
                    });
                    resetSortOrder();
                }).error(function(error) {
                    toastService.error(errorAddField, error);
                });
            });
        };

        $scope.remove = function (field) {
            dialogService.confirmDialog(deleteField, String.format(confirmDeleteField, field.name), function () {
                var dto = {
                    name: field.name
                };
                $http.put("/api/objectdefinition/" + definitionId + "/remove", dto).success(function () {
                    var others = _.filter($scope.definition.fields, function (f) {
                        return f.name !== field.name;
                    });
                    $scope.definition.fields = others;
                }).error(function(error) {
                    toastService.error(errorDeleteField, error);
                });
            });
        };

        var getDefinition = function () {
            $scope.definition = {};
            var url = String.format("/api/objectdefinition/{0}/{1}/detail", tenantService.getTenant().id, definitionId);
            $http.get(url, { cache: false }).success(function (data) {
                var contentTypes = definitionFieldContentTypeService.contentTypes;
                _.each(data.fields, function (d) {
                    var translation = _.find(contentTypes, function (ct) { return ct.value === d.contentType; });
                    if (translation) {
                        d.contentTypeTranslation = translation.name;
                        d.contentTypeIcon = translation.icon;
                    } else {
                        d.contentTypeTranslation = d.contentType;
                        d.contentTypeIcon = "";
                    }
                });
                $scope.definition = data;
            });
        };

        getDefinition();
    }]);

    angular.module("app").factory("definitionFieldContentTypeService", ["$translate", function($translate) {
        var contentTypes = [];

        $translate(["text", "html", "code", "markdown", "file", "boolean", "number", "date", "page", "object", "select_list", "page_link", "form"]).then(function (t) {
            contentTypes.push({ value: "Text", name: t.text, icon: "fa fa-font" });
            contentTypes.push({ value: "Html", name: t.html, icon: "fa fa-code" });
            contentTypes.push({ value: "Markdown", name: t.markdown, icon: "fa fa-medium" });
            contentTypes.push({ value: "File", name: t.file, icon: "fa fa-file-image-o" });
            contentTypes.push({ value: "Boolean", name: t.boolean, icon: "fa fa-check-square-o" });
            contentTypes.push({ value: "Number", name: t.number, icon: "ti-quote-right" });
            contentTypes.push({ value: "Date", name: t.date, icon: "fa fa-calendar" });
            contentTypes.push({ value: "Page", name: t.page, icon: "fa fa-file-o" });
            contentTypes.push({ value: "Object", name: t.object, icon: "ti-ticket" });
            contentTypes.push({ value: "List", name: t.select_list, icon: "fa fa-list" });
            contentTypes.push({ value: "PageLink", name: t.page_link, icon: "fa fa-external-link-square" });
            contentTypes.push({ value: "Form", name: t.form, icon: "fa fa-comments-o" });
            contentTypes.push({ value: "Code", name: t.code, icon: "fa fa-code" });
        });

        var factory = {};
        factory.contentTypes = contentTypes;
        return factory;
    }]);

    angular.module("app").controller("DefinitionFieldDialogController", ["$scope", "$modalInstance", "$log", "definitionFieldContentTypeService", "subject", "name", "contentType", "options", function ($scope, $modalInstance, $log, definitionFieldContentTypeService, subject, name, contentType, options) {
        $scope.subject = subject;
        $scope.name = name;
        $scope.options = options;
        $scope.result = {};
        $scope.isEdit = true;

        if (name === "") {
            $scope.isEdit = false;
        }

        $scope.contentTypes = definitionFieldContentTypeService.contentTypes;

        if (contentType) {
            $scope.result.contentType = _.find($scope.contentTypes, function (ct) { return ct.value === contentType; });
        }

        $scope.ok = function () {
            $modalInstance.close({
                name: $scope.name,
                contentType: $scope.result.contentType.value,
                options: $scope.options,
                translation: $scope.result.contentType.name,
                icon: $scope.result.contentType.icon
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());
(function() {
    angular.module("app").controller("ObjectsDefinitionsController", ["$scope", "$rootScope", "$http", "$location", "tenantService", "$translate", "dialogService", function ($scope, $rootScope, $http, $location, tenantService, $translate, dialogService) {
        $scope.definitions = [];

        $scope.title = "";

        $translate(["Objects"]).then(function (t) {
            $scope.title = t.Objects;
        });

        $rootScope.$on("logout", function() {
            $scope.definitions = [];
        });

        $scope.select = function (node) {
            $location.path("/objects/objects/" + node.id);
        };

        var getDefinitions = function () {
            var tenantId = tenantService.getTenant().id;
            $scope.definitions = [];

            $http.get("/api/objectdefinition/" + tenantId, { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = false;
                    $scope.definitions.push(d);
                });
            });

            $http.get("/api/objectdefinition/" + tenantId + "/shared", { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = true;
                    $scope.definitions.push(d);
                });
            });
        };

        $rootScope.$on("tenantChanged", getDefinitions);

        getDefinitions();
    }]);

    angular.module("app").controller("ObjectsController", ["$rootScope", "$scope", "$routeParams", "$http", "$log", "$location", "$modal", "dialogService", "toastService", "tenantService", "$translate", "localStorageService", function ($rootScope, $scope, $routeParams, $http, $log, $location, $modal, dialogService, toastService, tenantService, $translate, localStorageService) {
        var definitionId = $routeParams.definitionId;
        $scope.selectedObject = {};
        $scope.sortableFields = [];
        $scope.isShared = false;
        var firstTime = true;

        function load() {
            var tenantId = tenantService.getTenant().id;
            var filters = loadFiltersFromCookie();

            $http.get("/api/permission/object/" + tenantId, { cache: false }).success(function (permission) {
                $scope.canEdit = permission.canEdit;
                $scope.canAdmin = permission.canAdmin;
            });

            var getObjectsUrl = String.format("/api/objects/{0}/{1}", tenantId, definitionId);

            if (filters && filters.length > 0) {
                var f = [];

                _.each(filters, function(i) {
                    f.push({
                        fieldName: i.fieldName,
                        operator: i.operator,
                        value: i.value,
                    });
                });

                var filterJson = angular.toJson(f);
                filterJson = CryptoJS.enc.Utf8.parse(filterJson);
                filterJson = CryptoJS.enc.Base64.stringify(filterJson);
                filterJson = filterJson.replace(/\//g, '_');
                getObjectsUrl += "?filter=" + filterJson;
            }

            $http.get(getObjectsUrl, { cache: false }).success(function (data) {
                $scope.objects = data;

                if (data.length > 0 && firstTime) {
                    firstTime = false;
                    _.each(data[0].fields, function (f) {
                        if (f.isSortableField) {
                            $scope.sortableFields.push(f);
                        }
                    });
                }

                if (data.length > 0) {
                    _.each(data, function (o) {
                        o.sortableFields = {};
                        _.each(o.fields, function (f) {
                            if (f.isSortableField) {
                                o.sortableFields["f" + f.sortOrder] = {
                                    view: f.sortableFieldVisibleValue,
                                    sort: f.sortableFieldSortableValue
                                };
                            }
                        });
                    });
                }
            });

            $http.get("/api/objectdefinition/" + tenantId + "/shared", { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    if (d.id === definitionId) {
                        $scope.isShared = true;
                    }
                });
            });
        };

        $rootScope.$on("tenantChanged", load);

        load();

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id + "/" + definitionId + "/detail", { cache: false }).success(function (data) {
            $scope.definition = data;
            $scope.definitionName = data.fullname;
        });

        $scope.select = function(o) {
            $scope.selectedObject = o;
        };

        var settings = localStorageService.get("settings");
        var settingsKey = String.format("objects.{0}.sortOrder", definitionId);

        if (settings) {
            if (settings[settingsKey]) {
                $scope.sortField = settings[settingsKey];
            } else {
                $scope.sortField = "title";
            }
        } else {
            settings = {};
            $scope.sortField = "title";
        }

        $scope.setSortOrder = function (fieldName) {
            $scope.sortField = fieldName;
            settings[settingsKey] = fieldName;
            localStorageService.set("settings", settings);

            $log.debug("sortField=" + fieldName);
        }

        var editObject = "";
        var errorEditObject = "";
        var createObject = "";
        var errorCreateObject = "";
        var deleteObject = "";
        var confirmDeleteObject = "";
        var errorDeleteObject = "";

        $translate(["edit_object", "error_edit_object", "create_object", "error_create_object", "delete_object", "confirm_delete_object", "error_delete_object"]).then(function(t) {
            editObject = t.edit_object;
            errorEditObject = t.error_edit_object;
            createObject = t.create_object;
            errorCreateObject = t.error_create_object;
            deleteObject = t.delete_object;
            confirmDeleteObject = t.confirm_delete_object;
            errorDeleteObject = t.error_delete_object;
        });

        function openEditDialog(subject, entity, onSave) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/views/objects/editObjectDialog.min.html",
                controller: "EditObjectDialogController",
                size: "lg",
                resolve: {
                    subject: function () { return subject; },
                    entry: function () { return entity; },
                    definitionId: function () { return definitionId; }
                }
            }).result.then(function (result) {
                onSave(result);
            });
        };

        $scope.edit = function (o) {
            var url = String.format("/api/object/{0}", o.id);
            $http.get(url, { cache: false }).success(function (entry) {

                _.each(entry.fields, function (ef) {
                    var df = _.find($scope.definition.fields, function (edf) { return edf.name === ef.name; });
                    if (df) {
                        ef.contentTypeOptions = df.contentTypeOptions;

                        if (ef.contentTypeOptions) {
                            ef.contentTypeOptions = ef.contentTypeOptions.split("\n");
                            ef.contentTypeOptions.splice(0, 0, "");
                        } else {
                            ef.contentTypeOptions = [];
                        }
                    }
                });

                openEditDialog(editObject, entry, function(result) {
                    url = String.format("/api/object/{0}/{1}", tenantService.getTenant().id, o.id);
                    $http.post(url, result).success(function () {
                        var titleField = _.find(result.fields, function (f) { return f.sortOrder === 0; });
                        o.title = titleField.value;
                    }).error(function(error) {
                        toastService.error(errorEditObject, error);
                    });
                });
            });
        };

        $scope.duplicate = function(o) {
            var url = String.format("/api/object/{0}", o.id);
            $http.get(url, { cache: false }).success(function (entry) {

                _.each(entry.fields, function (ef) {
                    var df = _.find($scope.definition.fields, function (edf) { return edf.name === ef.name; });
                    if (df) {
                        ef.contentTypeOptions = df.contentTypeOptions;

                        if (ef.contentTypeOptions) {
                            ef.contentTypeOptions = ef.contentTypeOptions.split("\n");
                        } else {
                            ef.contentTypeOptions = [];
                        }
                    }
                });

                openEditDialog(createObject, entry, function (result) {
                    url = String.format("/api/objects/{0}/{1}", tenantService.getTenant().id, definitionId);
                    $http.post(url, result).success(function (no) {
                        var titleField = _.find(result.fields, function (f) { return f.sortOrder === 0; });
                        no.title = titleField.value;
                        $scope.objects.push(no);
                    }).error(function (error) {
                        toastService.error(errorEditObject, error);
                    });
                });
            });
        };

        $scope.delete = function (o) {
            var fullname = String.format("{0} (Nr. {1})", o.title, o.nr);
            dialogService.confirmDialog(deleteObject, String.format(confirmDeleteObject, fullname), function() {
                var url = String.format("/api/object/{0}/{1}", tenantService.getTenant().id, o.id);
                $http.delete(url).success(function () {
                    $scope.objects = _.reject($scope.objects, function (obj) { return obj.id === o.id; });
                }).error(function (error) {
                    toastService.error(errorDeleteObject, error);
                });
            });
        };

        $scope.create = function() {
            var entry = {
                title: "",
                fields: []
            };

            _.each($scope.definition.fields, function(f) {
                entry.fields.push({
                    name: f.name,
                    value: "",
                    contentType: f.contentType,
                    contentTypeOptions: f.contentTypeOptions ? f.contentTypeOptions.split("\n") : [],
                    sortOrder: f.sortOrder
                });
            });

            openEditDialog(createObject, entry, function (result) {
                var tenantId = tenantService.getTenant().id;
                var url = String.format("/api/objects/{0}/{1}", tenantId, definitionId);
                $http.post(url, result).success(function (o) {
                    var titleField = _.find(result.fields, function(f) { return f.sortOrder === 0; });
                    o.title = titleField.value;
                    $scope.objects.push(o);
                }).error(function(error) {
                    toastService.error(errorCreateObject, error);
                });
            });
        };

        $scope.share = function (o) {
            var existingTenantIds = [];

            if (o.shared) {
                existingTenantIds = _.map(o.shared, function (t) { return t.id; });
            }

            dialogService.selectTenantsDialog(existingTenantIds, function (tenants) {
                var ids = _.map(tenants, function (t) {
                    return t.id;
                });
                var dto = {
                    tenantIds: ids
                };
                var url = String.format("/api/object/{0}/share", o.id);
                $http.put(url, dto).success(function () {
                    o.shared = tenants;
                });
            });
        };

        $scope.donotshare = function (o) {
            var url = String.format("/api/object/{0}/share", o.id);
            var dto = {
                tenantIds: []
            };
            return $http.put(url, dto).success(function () {
                o.shared = [];
            });
        };

        $scope.getNames = function (tenants) {
            var names = "";
            _.each(tenants, function (t) {
                names += t.name + ", ";
            });
            names = names.trim();
            return names.substring(0, names.length - 1);
        };

        $scope.import = function () {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/views/objects/import.min.html",
                controller: "ObjectImportDialogController",
                size: "lg",
                resolve: {
                    definition: function () { return $scope.definition; }
                }
            });
        };

        $scope.addFilter = function () {
            var fields = [];

            _.each($scope.definition.fields, function (f) {
                fields.push({
                    name: f.name,
                    contentType: f.contentType,
                    contentTypeOptions: f.contentTypeOptions ? f.contentTypeOptions.split("\n") : [],
                    sortOrder: f.sortOrder
                });
            });

            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/views/objects/addFilterDialog.min.html",
                controller: "AddObjectFilterDialogController",
                resolve: {
                    fields: function () { return fields; },
                }
            }).result.then(function (result) {
                addFilter(result);
            });
        };

        $scope.removeFilter = function(filter) {
            var filters = loadFiltersFromCookie();

            filters = _.reject(filters, function(f) {
                return f.fieldName === filter.fieldName &&
                    f.operator === filter.operator &&
                    f.value === filter.value;
            });

            var storageKey = String.format("objects.{0}.filters", definitionId);
            localStorageService.set(storageKey, filters);
            $scope.filters = filters;
            load();
        };

        function addFilter(filter) {
            var filters = loadFiltersFromCookie();
            filters.push(filter);

            var storageKey = String.format("objects.{0}.filters", definitionId);
            localStorageService.set(storageKey, filters);
            $scope.filters = filters;
            load();
        };

        function loadFiltersFromCookie() {
            var storageKey = String.format("objects.{0}.filters", definitionId);
            var filters = localStorageService.get(storageKey);

            if (!filters) {
                filters = [];
            }

            $scope.filters = filters;
            return filters;
        };
    }]);

    angular.module("app").controller("AddObjectFilterDialogController", ["$modalInstance", "$scope", "$log", "fields", function ($modalInstance, $scope, $log, fields) {
        $scope.fields = fields;
        $scope.field = {};
        $scope.operations = [];
        $scope.filterType = "";

        $scope.$watch("field", function() {
            if (!$scope.field || !$scope.field.contentType) {
                $scope.filterType = "";
                return;
            }

            $scope.filter.fieldName = $scope.field.name;
            $scope.filter.displayValue = "";
            $scope.filter.value = "";
            $scope.filter.operator = "=";
            $scope.operations = [];

            if ($scope.field.contentType.toUpperCase() === "TEXT" ||
                $scope.field.contentType.toUpperCase() === "CODE" ||
                $scope.field.contentType.toUpperCase() === "MARKDOWN" ||
                $scope.field.contentType.toUpperCase() === "HTML") {
                $scope.operations = ["=", "contains"];
                $scope.filterType = "text";
            } else if ($scope.field.contentType.toUpperCase() === "DATE") {
                $scope.operations = [">=", ">", "=", "<", "<="];
                $scope.filterType = "date";
            } else if ($scope.field.contentType.toUpperCase() === "LIST") {
                $scope.operations = ["="];
                $scope.filterType = "list";
            } else if ($scope.field.contentType.toUpperCase() === "NUMBER") {
                $scope.operations = [">=", ">", "=", "<", "<="];
                $scope.filterType = "number";
            } else if ($scope.field.contentType.toUpperCase() === "BOOLEAN") {
                $scope.operations = ["="];
                $scope.filterType = "boolean";
            } else {
                $scope.filter.operator = "";
                $scope.filterType = "invalid";
            }
        });

        $scope.filter = {
            fieldName: "",
            operator: "",
            value: "",
            displayName: ""
        };

        $scope.openStartDate = function (e, field) {
            e.preventDefault();
            e.stopPropagation();
            field.isDateOpened = true;
        };

        $scope.times = [];

        for (var i = 0; i < 24; i++) {
            var hour = i + ":";
            if (hour.length < 3) {
                hour = "0" + hour;
            }

            for (var m = 0; m < 60; m += 5) {
                var minute = m;
                if (m < 10) {
                    minute = "0" + minute;
                }

                $scope.times.push(hour + minute);
            }
        }

        $scope.dateOptions = {
            formatYear: "yy",
            startingDay: 1
        };

        $scope.$watch("filter.dateValue", function() {
            updateDateTimeValue();
        });

        $scope.$watch("filter.timeValue", function() {
            updateDateTimeValue();
        });

        function updateDateTimeValue() {
            var tpatt = /[0-9]{1,2}:[0-9]{2}/i;
            var dpatt = /[0-9]{1,2}.[0-9]{1,2}.[0-9]{4}/i;
            var v = "";
            var d = "";

            if ($scope.filter.dateValue) {
                v += moment($scope.filter.dateValue).format("YYYY-MM-DD");
                d += moment($scope.filter.dateValue).format("DD.MM.YYYY");

                if ($scope.filter.timeValue && tpatt.test($scope.filter.timeValue)) {
                    v += "T" + $scope.filter.timeValue + ":00";
                    d += $scope.filter.timeValue;
                } else {
                    v += "T00:00:00";
                }
            }

            $scope.filter.value = v;
            $scope.filter.displayValue = d;
        };

        $scope.ok = function () {
            var v = $scope.filter.value;

            if ($scope.filter.displayValue) {
                v = $scope.filter.displayValue;
            }

            if (!$scope.filter.value || !$scope.filter.operator) {
                $modalInstance.dismiss("cancel");
                return;
            }

            $modalInstance.close({
                fieldName: $scope.filter.fieldName,
                operator: $scope.filter.operator,
                value: $scope.filter.value,
                displayName: $scope.filter.fieldName + " " + $scope.filter.operator + " " + v
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("ObjectImportDialogController", ["$modalInstance", "$scope", "$http", "tenantService", "definition", function ($modalInstance, $scope, $http, tenantService, definition) {
        $scope.isImportEnabled = false;
        $scope.isUploadEnabled = false;
        $scope.currentStep = 0;
        var id = 0;

        $scope.options = { delimiter: ",", mapping:[] };
        $scope.definition = definition;

        _.each(definition.fields, function (f) {
            $scope.options.mapping.push({
                column: "",
                objectField: f.name,
                sortOrder: f.sortOrder,
                format: "",
                contentType: f.contentType
            });
        });

        $http.get("/api/objects/import/start", { cache: false }).success(function (data) {
            id = data.id;
        });

        $http.get("/api/objects/import/encodings").success(function (data) {
            $scope.encodings = data;
            $scope.options.encoding = "utf-8";
        });

        $scope.$watch("csvFile", function () {
            if (!$scope.csvFile) {
                return;
            }
            $scope.csvFileName = $scope.csvFile.name;
            $scope.isUploadEnabled = true;
        });

        $scope.goToStep = function (step) {
            if ($scope.currentStep < step) {
                return;
            }
            $scope.currentStep = step;
        };

        $scope.upload = function () {
            if (!$scope.csvFile) {
                return;
            }

            var fd = new FormData();
            fd.append("file", $scope.csvFile);
            $http.post("/api/objects/import/" + id + "/file", fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            }).success(function () {
                $http.get("/api/objects/import/" + id + "/encoding", { cache: false }).success(function (e) {
                    $scope.options.encoding = e;
                });

                $scope.currentStep = 1;
            });
        };

        $scope.goToMapping = function () {
            var url = String.format("/api/objects/import/{0}/columns", id);
            $http.post(url, $scope.options).success(function (data) {
                $scope.columns = data;
                $scope.currentStep = 2;
            });
        };

        $scope.goToPreview = function () {
            var url = String.format("/api/objects/{0}/import/{1}/preview", definition.id, id);
            $http.post(url, $scope.options).success(function (data) {
                $scope.preview = data;
                $scope.currentStep = 3;
                $scope.previewHeaders = [];

                if (data.length > 0) {
                    _.each(data[0].cells, function(c) {
                        $scope.previewHeaders.push(c.header);
                    });
                }
            });
        };

        $scope.import = function () {
            var tenantId = tenantService.getTenant().id;
            var url = String.format("/api/objects/{0}/{1}/import/{2}/import", tenantId, definition.id, id);
            $http.post(url, $scope.options).success(function () {
                $modalInstance.close();
            });
        };

        $scope.back = function () {
            if ($scope.currentStep > 0) {
                $scope.currentStep--;
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("ObjectController", [function () { }]);

    angular.module("app").controller("EditObjectDialogController", ["$modalInstance", "$scope", "$http", "$log", "dialogService", "tenantService", "subject", "entry", "definitionId", function ($modalInstance, $scope, $http, $log, dialogService, tenantService, subject, entry, definitionId) {
        $scope.entry = entry;
        $scope.subject = subject;
        $scope.templates = [];
        $scope.selectedTemplate = { id: 0, name: "" };
        $scope.preview = "";
        $scope.objectIsValid = true;

        $http.get("/api/objecttemplates/" + tenantService.getTenant().id + "/" + definitionId, { cache: false }).success(function (templates) {
            $scope.templates = templates;
            $scope.templates.push($scope.selectedTemplate);
        });

        function updatePreview() {
            if (!$scope.selectedTemplate || !$scope.selectedTemplate.id) {
                return;
            }

            $http.post("/api/object/template/" + $scope.selectedTemplate.id, entry).success(function (html) {
                $scope.preview = html;
            });
        };

        $scope.$watch("entry", function () {
            updatePreview();
        }, true);

        $scope.$watch("selectedTemplate", function () {
            updatePreview();
        });

        $scope.tinymceOptions = {
            theme: "modern",
            relative_urls: true,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code hr wordcount textcolor colorpicker",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media"
        };

        $scope.codemirrorOptions = {
            lineNumbers: true,
            theme: "neo",
            lineWrapping: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            matchTags: { bothTags: true },
            autoCloseTags: true,
            styleActiveLine: true,
            viewportMargin: Infinity,
            mode: "htmlmixed"
        };

        $scope.getObjects = function(value) {
            var url = String.format("/api/object/search/{0}/{1}", tenantService.getTenant().id, value);
            return $http.get(url, { cache: false }).then(function(response) {
                return response.data;
            });
        };

        $scope.browseObject = function(field) {
            dialogService.selectObjectDialog(field.value, function(o) {
                if (!o) {
                    return;
                }
                field.value = o;
            });
        };

        $scope.getFiles = function (value) {
            var url = String.format("/api/file/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, {cache:false}).then(function (response) {
                return response.data;
            });
        };

        $scope.browseFile = function (field) {
            dialogService.selectFileDialog(field.value, function (file) {
                if (!file) {
                    return;
                }
                field.value = file;
            });
        };

        $scope.getPages = function (value) {
            var url = String.format("/api/page/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, { cache: false }).then(function (response) {
                return response.data;
            });
        };

        $scope.browsePage = function (field) {
            dialogService.selectPageDialog(field.value, function (page) {
                if (!page) {
                    return;
                }
                page.children = [];
                field.value = page;
            });
        };

        $scope.browsePageLink = function (field) {
            var pl = field.value && field.value.pageLink ? field.value.pageLink : {};
            dialogService.selectPageDialog(pl, function (page) {
                if (!page) {
                    return;
                }
                page.children = [];
                if (!field.value) {
                    field.value = {};
                }
                field.value.pageLink = page;
            });
        };

        $scope.getForms = function(value) {
            var url = String.format("/api/form/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, { cache: false }).then(function(response) {
                return response.data;
            });
        };

        $scope.browseForm = function(field) {
            dialogService.selectFormDialog(field.value, function(form) {
                if (!form) {
                    return;
                }
                field.value = form;
            });
        };

        $scope.openStartDate = function (e, field) {
            e.preventDefault();
            e.stopPropagation();
            field.isDateOpened = true;
        };

        $scope.times = [];

        for (var i = 0; i < 24; i++) {
            var hour = i + ":";
            if (hour.length < 3) {
                hour = "0" + hour;
            }

            for (var m = 0; m < 60; m += 5) {
                var minute = m;
                if (m < 10) {
                    minute = "0" + minute;
                }

                $scope.times.push(hour + minute);
            }
        }

        $scope.dateOptions = {
            formatYear: "yy",
            startingDay: 1
        };

        function updateDateTimeValue(field) {
            var tpatt = /[0-9]{1,2}:[0-9]{2}/i;
            var dpatt = /[0-9]{1,2}.[0-9]{1,2}.[0-9]{4}/i;
            var v = "";

            if (field.dateValue) {
                if (dpatt.test(field.dateValue)) {
                    v += field.dateValue;
                } else {
                    v += moment(field.dateValue).format("DD.MM.YYYY");
                }

                if (field.timeValue && tpatt.test(field.timeValue)) {
                    v += " " + field.timeValue;
                }
            }

            field.value = v;
        };

        _.each(entry.fields, function(f, i) {
            if (f.contentType.toUpperCase() === "DATE") {
                if (f.value) {
                    var patt = /[0-9]{1,2}.[0-9]{1,2}.[0-9]{4}\s[0-9]{1,2}:[0-9]{1,2}/i;
                    if (patt.test(f.value)) {
                        var parts = f.value.split(" ");
                        var dateParts = parts[0].split(".");
                        var timeParts = parts[1].split(":");
                        var day = Number(dateParts[0]);
                        var month = Number(dateParts[1]);
                        var year = Number(dateParts[2]);
                        var hours = Number(timeParts[0]);
                        var minutes = Number(timeParts[1]);

                        month = month < 10 ? "0" + month : month;
                        day = day < 10 ? "0" + day : day;
                        hours = hours < 10 ? "0" + hours : hours;
                        minutes = minutes < 10 ? "0" + minutes : minutes;

                        f.dateValue = day + "." + month + "." + year;
                        f.timeValue = hours + ":" + minutes;
                    }
                }

                $scope.$watch("entry.fields[" + i + "].dateValue", function() {
                    updateDateTimeValue(f);
                });

                $scope.$watch("entry.fields[" + i + "].timeValue", function() {
                    updateDateTimeValue(f);
                });

                updateDateTimeValue(f);
            };
        });

        $scope.ok = function () {
            var isValid = true;
            _.each(entry.fields, function (f) {
                f.isValid = true;
                if (f.contentType.toUpperCase() === "DATE") {
                    if (!f.value) {
                        f.isValid = false;
                        isValid = false;
                    }
                }
                if (f.sortOrder === 0) {
                    if (!f.value) {
                        f.isValid = false;
                        isValid = false;
                    }
                }
            });

            $scope.objectIsValid = isValid;
            if (isValid) {
                $modalInstance.close($scope.entry);
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);
}());
(function() {
    angular.module("app").controller("ObjectTemplatesController", ["$rootScope", "$scope", "$routeParams", "$http", "$location", "$modal", "dialogService", "toastService", "tenantService", "$translate", function ($rootScope, $scope, $routeParams, $http, $location, $modal, dialogService, toastService, tenantService, $translate) {
        var definitionId = $routeParams.definitionId;

        $scope.templates = [];
        $scope.selectedTemplate = {};
        $scope.isShared = false;

        var tCreateTemplate = "";
        var tErrorCreateTemplate = "";
        var tName = "";
        var tRenameTemplate = "";
        var tDeleteTemplate = "";
        var tErrorRenameTemplate = "";
        var tConfirmDeleteTemplate = "";
        var tErrorDeleteTemplate = "";

        $http.get("/api/permission/objecttemplate/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $translate(["create_template", "error_create_template", "error_delete_template", "confirm_delete_template", "name", "rename_template", "delete_template", "error_rename_template"]).then(function (t) {
            tCreateTemplate = t.create_template;
            tName = t.name;
            tRenameTemplate = t.rename_template;
            tDeleteTemplate = t.delete_template;
            tErrorCreateTemplate = t.error_create_template;
            tErrorRenameTemplate = t.error_rename_template;
            tConfirmDeleteTemplate = t.confirm_delete_template;
            tErrorDeleteTemplate = t.error_delete_template;
        });

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id + "/shared", { cache: false }).success(function (data) {
            _.each(data, function (d) {
                if (d.id === definitionId) {
                    $scope.isShared = true;
                }
            });
        });

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id + "/" + definitionId + "/detail", { cache: false }).success(function (data) {
            $scope.definition = data;
            $scope.definitionName = data.fullname;
        });

        $scope.select = function(template) {
            $scope.selectedTemplate = template;
        };

        $scope.create = function() {
            dialogService.singleInputDialog(tCreateTemplate, tName, "", function (name) {
                $http.put("/api/objecttemplate/" + definitionId, { name: name }).success(function(dto) {
                    $scope.templates.push(dto);
                }).error(function (error) {
                    toastService.error(tErrorCreateTemplate, error);
                });
            });
        };

        $scope.edit = function (template) {
            $location.path("/objects/template/" + definitionId + "/" + template.id);
        };

        $scope.rename = function(template) {
            dialogService.singleInputDialog(tRenameTemplate, tName, template.name, function (newName) {
                $http.post("/api/objecttemplate/" + definitionId + "/" + template.id + "/rename", { name: newName }).success(function () {
                    template.name = newName;
                }).error(function (error) {
                    toastService.error(tErrorRenameTemplate, error);
                });
            });
        };

        $scope.delete = function (template) {
            var msg = String.format(tConfirmDeleteTemplate, template.name + " (Nr. " + template.publicId + ")");
            dialogService.confirmDialog(tDeleteTemplate, msg, function () {
                $http.delete("/api/objecttemplate/" + definitionId + "/" + template.id).success(function() {
                    $scope.templates = _.reject($scope.templates, function (t) { return t.id === template.id; });
                }).error(function(error) {
                    toastService.error(tErrorDeleteTemplate, error);
                });
            });
        };

        var getTemplates = function () {
            $http.get("/api/objecttemplates/" + tenantService.getTenant().id + "/" + definitionId, { cache: false }).success(function (data) {
                $scope.templates = data;
            });
        };

        $rootScope.$on('logout', function () { $scope.templates = []; });

        getTemplates();
    }]);

    angular.module("app").controller("ObjectTemplateController", ["$rootScope", "$scope", "$routeParams", "$http", "$log", "$translate", "dialogService", "toastService", "toastr", function ($rootScope, $scope, $routeParams, $http, $log, $translate, dialogService, toastService, toastr) {
        var definitionId = $routeParams.definitionId;
        var templateId = $routeParams.templateId;

        var tRenameTemplate = "";
        var tErrorRenameTemplate = "";
        var tName = "";
        var tErrorSaveTemplate = "";
        var tSuccessSaveTemplate = "";

        $scope.codemirrorOptions = {
            lineNumbers: true,
            theme: 'neo',
            lineWrapping: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            matchTags: { bothTags: true },
            autoCloseTags: true,
            styleActiveLine: true,
            viewportMargin: Infinity,
            mode: "htmlmixed",
        };

        $translate(["rename_template", "name", "error_rename_template", "error_save_template", "success_save_template"]).then(function (t) {
            tRenameTemplate = t.rename_template;
            tName = t.name;
            tErrorRenameTemplate = t.error_rename_template;
            tErrorSaveTemplate = t.error_save_template;
            tSuccessSaveTemplate = t.success_save_template;
        });

        $http.get("/api/objecttemplate/" + definitionId + "/" + templateId, { cache: false }).success(function(template) {
            $scope.template = template;
        });

        $scope.saveTemplate = function() {
            $http.post("/api/objecttemplate/" + definitionId + "/" + templateId + "/template", { template: $scope.template.template }).success(function() {
                toastr.success('', tSuccessSaveTemplate, { allowHtml: false, timeOut: 1000 });
            }).error(function(error) {
                toastService.error(tErrorSaveTemplate, error);
            });
        };

        $scope.editName = function() {
            dialogService.singleInputDialog(tRenameTemplate, tName, $scope.template.name, function (newName) {
                $http.post("/api/objecttemplate/" + definitionId + "/" + templateId + "/rename", { name: newName }).success(function () {
                    $scope.template.name = newName;
                }).error(function (error) {
                    toastService.error(tErrorRenameTemplate, error);
                });
            });
        };

        //$rootScope.$on('logout', function () { $scope.template = {}; });
    }]);

    angular.module("app").controller("ObjectDefinitionsForTemplatesController", ["$scope", "$rootScope", "$http", "$location", "tenantService", "$translate", function ($scope, $rootScope, $http, $location, tenantService, $translate) {
        $scope.definitions = [];

        $scope.title = "";

        $translate(["Templates"]).then(function (t) {
            $scope.title = t.Templates;
        });

        $rootScope.$on('logout', function () {
            $scope.definitions = [];
        });

        $scope.select = function (node) {
            $location.path("/objects/templates/" + node.id);
        };

        var getDefinitions = function () {
            var tenantId = tenantService.getTenant().id;
            $scope.definitions = [];

            $http.get("/api/objectdefinition/" + tenantId, { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = false;
                    $scope.definitions.push(d);
                });
            });

            $http.get("/api/objectdefinition/" + tenantId + "/shared", { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = true;
                    $scope.definitions.push(d);
                });
            });
        };

        $rootScope.$on("tenantChanged", getDefinitions);

        getDefinitions();
    }]);
}());
(function () {
    angular.module("app").controller("PageDetailController", ["$scope", "$http", "$routeParams", "toastService", "toastr", "dialogService", "$log", "$translate", function (
        $scope, $http, $routeParams, toastService, toastr, dialogService, $log, $translate) {
        $scope.pageId = $routeParams.pageId;

        $http.get("/api/contenthandler", { cache: false }).success(function (ct) {
            $scope.contentTypes = ct;

            $http.get("/api/page/" + $scope.pageId, { cache: false }).success(function (data) {
                $scope.page = data;

                _.each(data.content, function (c) {
                    var type = _.find(ct, function (x) { return x.name.toLowerCase() == c.type.toLowerCase(); });
                    if (type) {
                        c.type = type.name;
                    }
                });
            });
        });

        var editPageTitle = "";
        var editPageName = "";
        var title = "";
        var name = "";
        var errorSaveName = "";
        var errorSaveTitle = "";
        var contentSaved = "";
        var contentNotSaved = "";

        $translate(["change_page_title", "change_page_name", "name", "title", "error_change_page_title", "error_change_page_name", "error_save_page_content", "success_save_page_content"]).then(function (t) {
            editPageTitle = t.change_page_title;
            editPageName = t.change_page_name;
            name = t.name;
            title = t.title;
            errorSaveName = t.error_change_page_name;
            errorSaveTitle = t.error_change_page_title;
            contentNotSaved = t.error_save_page_content;
            contentSaved = t.success_save_page_content;
        });

        $scope.editTitle = function () {
            dialogService.singleInputDialog(editPageTitle, title, $scope.page.title, saveTitle);
        };

        $scope.editName = function () {
            dialogService.singleInputDialog(editPageName, name, $scope.page.name, saveName);
        };

        var saveName = function (n) {
            var dto = {
                name: n
            };
            $http.put("/api/page/" + $scope.pageId + "/name", dto).success(function () {
                $scope.page.name = n;
            }).error(function (error) {
                $log.error(angular.toJson(error));
                toastService.error(errorSaveName, error);
            });
        };

        var saveTitle = function (t) {
            var dto = {
                title: t
            };
            $http.put("/api/page/" + $scope.pageId + "/title", dto).success(function () {
                $scope.page.title = t;
            }).error(function (error) {
                $log.error(angular.toJson(error));
                toastService.error(errorSaveTitle, error);
            });
        };

        $scope.saveContent = function(content) {
            var value = content.value;
            if (!value) {
                return null;
            }

            var save = {
                name: content.name,
                type: content.type,
                value: value,
            };

            return $http.put("/api/page/" + $scope.pageId + "/content", save).success(function () {
                var msg = String.format(contentSaved, save.name);
                toastr.success('', msg, { allowHtml: false, timeOut: 1000 });
                $log.info("saved page content " + save.name);
            }).error(function (error) {
                $log.error(angular.toJson(error));
                toastService.error(contentNotSaved, error);
            });
        };
    }]);
}());

(function() {
    angular.module('app').controller('PageController', ["$rootScope", "$scope", "$log", "$http", "$modal", "$location", "toastService", "tenantService", "dialogService", "$translate", function ($rootScope, $scope, $log, $http, $modal, $location, toastService, tenantService, dialogService, $translate) {
        $scope.selectedPage = {};

        var errorMovePage = "";
        var changePageLink = "";
        var errorChangePageLink = "";
        var createPageLink = "";
        var errorCreatePageLink = "";
        var createPage = "";
        var tName = "";
        var noPageTemplate = "";
        var noPageTemplateDescription = "";
        var deletePage = "";
        var confirmDeletePage = "";
        var errorDeletePage = "";
        var errorChangePageTemplate = "";
        var errorPinPage = "";
        var errorUnpinPage = "";
        var errorEnablePage = "";
        var errorDisablePage = "";
        var errorMarkAsDefaultPage = "";

        $translate(["error_move_page", "change_page_link", "error_change_page_link", "create_page_link", "error_create_page_link", "create_page", "name", "no_page_template", "no_page_template_description", "delete_page", "confirm_delete_page", "error_delete_page", "error_change_page_template", "error_pin_page", "error_unpin_page", "error_enable_page", "error_disable_page", "error_mark_as_default_page"]).then(function (t) {
            errorMovePage = t.error_move_page;
            changePageLink = t.change_page_link;
            errorChangePageLink = t.error_change_page_link;
            createPageLink = t.create_page_link;
            errorCreatePageLink = t.error_create_page_link;
            createPage = t.create_page;
            tName = t.name;
            noPageTemplate = t.no_page_template;
            noPageTemplateDescription = t.no_page_template_description;
            deletePage = t.delete_page;
            confirmDeletePage = t.confirm_delete_page;
            errorDeletePage = t.error_delete_page;
            errorChangePageTemplate = t.error_change_page_template;
            errorPinPage = t.error_pin_page;
            errorUnpinPage = t.error_unpin_page;
            errorEnablePage = t.error_enable_page;
            errorDisablePage = t.error_disable_page;
            errorMarkAsDefaultPage = t.error_mark_as_default_page;
        });

        $http.get("/api/permission/page/" + tenantService.getTenant().id, { cache: false }).success(function(permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.treeOptions = {
            dropped: function (event) {
                var thisNode = event.source.nodeScope.node;
                var thisNr = thisNode.id;
                var oldParent = event.source.nodesScope && event.source.nodesScope.node ? event.source.nodesScope.node.id : null;
                var newParent = event.dest.nodesScope && event.dest.nodesScope.node ? event.dest.nodesScope.node.id : null;
                var oldData = {};
                oldData[thisNr] = { parentId: thisNode.parentId, sortOrder: thisNode.sortOrder };

                if ((oldParent === newParent || (!oldParent && !newParent))) {
                    newParent = thisNode.parentId;
                } else {
                    newParent = newParent || 0;
                }

                var website = _.find($scope.websites, function (w) { return w.id === thisNode.websiteId; });
                var siblings = _.filter(website.allPages, function (p) { return p.parentId === newParent; });
                siblings = _.sortBy(siblings, "name");
                siblings = _.sortBy(siblings, "sortOrder");
                siblings = _.reject(siblings, function (p) { return p.id === thisNr; });
                siblings.splice(event.dest.index, 0, thisNode);
                thisNode.parentId = newParent;

                var i = 0;
                _.each(siblings, function (p) {
                    if (p.id != thisNr) {
                        oldData[p.id] = { parentId: p.parentId, sortOrder: p.sortOrder };
                    }

                    p.sortOrder = i;
                    i++;
                });

                resetChildren(website);

                var url = String.format("/api/page/{0}/move/{1}/{2}", thisNr, newParent, event.dest.index);
                $http.post(url).success(function () {
                }).error(function (e) {
                    toastService.error(errorMovePage, e);

                    _.each(oldData, function(v, k) {
                        var s = _.find(siblings, function (p) { return p.id === k; });
                        if (s) {
                            s.sortOrder = v.sortOrder;
                            s.parentId = v.parentId;
                        }
                    });

                    resetChildren(website);
                });

                return true;
            }
        };

        function resetChildren(website) {
            website.rootPages = getPagesByParentId(website, "0");

            _.each(website.pages, function (p) {
                p.children = getPagesByParentId(website, p.id);
            });
        };

        function getPagesByParentId(website, parentId) {
            var children = _.filter(website.pages, function (p) { return p.parentId == parentId; });
            children = _.sortBy(children, "name");
            children = _.sortBy(children, "sortOrder");
            return children;
        }

        $scope.searchPages = function (website) {
            var searchText = website.searchText.toUpperCase();

            _.each(website.pages, function(p) { p.isCollapsed = true; });

            var pages = _.filter(website.pages, function(p) {
                 return pageLabelComparer(p, searchText);
            });

            _.each(pages, function (p) { expandParent(website, p); });
        };

        function expandParent(website, page) {
            var parent = _.find(website.pages, function (p) { return p.id === page.parentId; });
            if (parent) {
                parent.isCollapsed = false;
                expandParent(website, parent);
            }
        }

        function pageLabelComparer(page, searchText) {
            return page.fullname.toUpperCase().indexOf(searchText) >= 0;
        }

        function openLinkDialog(subject, name, page, url, onSave) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/content/pageLinkDialog.min.html",
                controller: "PageLinkDialogController",
                resolve: {
                    page: function () { return page; },
                    subject: function () { return subject; },
                    name: function () { return name; },
                    url: function() { return url; },
                }
            });

            modalInstance.result.then(onSave);
        };

        $scope.editLink = function(website, node) {
            openLinkDialog(changePageLink, node.node.name, node.node.link, node.node.link.url, function (result) {
                var name = result.name;
                var url = result.url;
                var pageId = 0;

                if (result.page) {
                    pageId = result.page.id;
                }

                var dto = {
                    pageId: pageId,
                    name: name,
                    url: url,
                };

                $http.put("/api/page/" + node.node.id + "/link", dto).success(function (data) {
                    _.each(website.allPages, function(p) {
                        if (p.id === pageId) {
                            p.name = name;
                            p.link = data.link;
                        }
                    });
                }).error(function (error) {
                    toastService.error(errorChangePageLink, error);
                });
            });
        };

        $scope.createPageLink = function(website) {
            var parent = null;
            var parentId = 0;
            if ($scope.selectedPage && $scope.selectedPage.id) {
                parent = $scope.selectedPage;
                parentId = $scope.selectedPage.id;
            }

            openLinkDialog(createPageLink, "", null, "", function (result) {
                var name = result.name;
                var url = result.url;
                var pageId = 0;

                if (result.page) {
                    pageId = result.page.id;
                }

                var dto = {
                    name: name,
                    pageId: pageId,
                    parentPageId: parentId,
                    url: url,
                };
                $http.put("/api/pages/" + website.id + "/createlink", dto).success(function (data) {
                    data.children = [];
                    website.allPages.push(data);
                    website.pages.push(data);
                    website.isOpen = true;

                    if (parent) {
                        parent.isCollapsed = false;
                    }

                    resetChildren(website);
                    $scope.selectedPage = data;
                }).error(function (error) {
                    toastService.error(errorCreatePageLink, error);
                });
            });
        };

        $scope.createPage = function (website) {
            var parent = null;
            var parentId = 0;
            if ($scope.selectedPage && $scope.selectedPage.id) {
                parent = $scope.selectedPage;
                parentId = $scope.selectedPage.id;
            }

            dialogService.singleInputDialog(createPage, tName, "", function (name) {
                var url = "/api/pages/" + website.id + "/create";
                var dto = {
                    parentPageId : parentId,
                    name: name
                };
                $http.put(url, dto).success(function(data) {
                    data.nr = "n/a";
                    data.children = [];
                    website.allPages.push(data);
                    website.pages.push(data);
                    website.isOpen = true;

                    if (parent) {
                        parent.isCollapsed = false;
                    }

                    resetChildren(website);
                    $scope.selectedPage = data;
                });
            });
        };

        $scope.selectPage = function(node) {
            $scope.selectedPage = node.node;
        };

        $scope.edit = function (node) {
            if (node.node && node.node.templateId && node.node.templateId != "0") {
                $location.path("/content/page/" + node.node.id);
            } else {
                dialogService.messageDialog(noPageTemplate, noPageTemplateDescription);
            }
        };

        $scope.delete = function (node) {
            var website = findWebsite(node.node.websiteId);
            dialogService.confirmDialog(deletePage, String.format(confirmDeletePage, node.node.fullname), function () {
                $http.delete("/api/page/" + node.node.id).success(function () {
                    website.pages = _.reject(website.pages, function(p) { return p.id === node.node.id; });
                    website.allPages = _.reject(website.allPages, function (p) { return p.id === node.node.id; });
                    resetChildren(website);
                }).error(function(error) {
                    toastService.error(errorDeletePage, error);
                });
            });
        };

        $scope.changeLayout = function (node) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/content/editPageLayoutTemplate.min.html",
                controller: "PageEditLayoutController",
                resolve: {
                    page: function () {
                        return node.node;
                    }
                }
            });

            modalInstance.result.then(function (selectedTemplate) {
                if (selectedTemplate && selectedTemplate.id) {
                    $http.put("/api/page/" + node.node.id + "/template/" + selectedTemplate.id).success(function () {
                        node.node.templateId = selectedTemplate.id;
                    }).error(function (error) {
                        toastService.error(errorChangePageTemplate, error);
                    });
                }
            });
        };

        function findWebsite(websiteId) {
            var websites = $scope.websites.filter(function (item) {
                return item.id === websiteId;
            });
            return websites[0];
        };

        // TODO: bei enable/disable etc. auch bei pinned seiten anwenden!
        function findPages(pageId) {
            var pages = [];
            angular.forEach($scope.websites, function(website) {
                angular.forEach(website.allPages, function(page) {
                    if (page.id === pageId) {
                        pages.push(page);
                    }
                });
            });
            return pages;
        };

        $scope.unpin = function (node) {
            return $http.put("/api/page/" + node.node.id + "/unstar").success(function () {
                var website = findWebsite(node.node.websiteId);

                if (!website) {
                    return;
                }

                website.starred = website.starred.filter(function (item) {
                    return item.id !== node.node.id;
                });
            }).error(function(error) {
                toastService.error(errorUnpinPage, error);
            });
        };

        $scope.pin = function (node) {
            return $http.put("/api/page/" + node.node.id + "/star").success(function () {
                var website = findWebsite(node.node.websiteId);

                if (!website) {
                    return;
                }

                var alreadyStarred = false;
                angular.forEach(website.starred, function (value, key) {
                    if (value.id === node.node.id) {
                        alreadyStarred = true;
                    }
                });

                if (alreadyStarred) {
                    return;
                }

                var copy = angular.copy(node.node);
                copy.children = [];
                website.allPages.push(copy);
                website.starred.push(copy);
            }).error(function(error) {
                toastService.error(errorPinPage, error);
            });
        };

        $scope.setDefault = function (page) {
            var websiteId = page.websiteId;
            var pageId = page.id;
            return $http.post("/api/website/" + websiteId + "/defaultpage/" + pageId).success(function () {
                var website = findWebsite(websiteId);
                if (!website) {
                    return;
                }
                _.each(website.allPages, function(p) {
                    p.isDefault = p.id === page.id;
                });
            }).error(function(error) {
                toastService.error(errorMarkAsDefaultPage, error);
            });
        };

        $scope.enable = function (node) {
            return $http.put("/api/page/" + node.node.id + "/enable").success(function () {
                var pages = findPages(node.node.id);
                angular.forEach(pages, function (page) { page.disabled = false; });
            }).error(function(error) {
                toastService.error(errorEnablePage, error);
            });
        };

        $scope.disable = function (node) {
            return $http.put("/api/page/" + node.node.id + "/disable").success(function () {
                var pages = findPages(node.node.id);
                angular.forEach(pages, function (page) { page.disabled = true; });
            }).error(function (error) {
                toastService.error(errorDisablePage, error);
            });
        };

        $scope.toggle = function (node) {
            node.toggle();
        };

        $scope.collapseAll = function () {
            for (var i = 0; i < $scope.websites.length; i++) {
                var website = $scope.websites[i];
                if (website.isOpen) {
                    collapse(website);
                }
            }
        };

        $scope.expandAll = function () {
            for (var i = 0; i < $scope.websites.length; i++) {
                var website = $scope.websites[i];
                if (website.isOpen) {
                    _.each(website.pages, function (p) { p.isCollapsed = false; });
                }
            }
        };

        var collapse = function (website) {
            _.each(website.pages, function (p) { p.isCollapsed = true; });
        };

        var addPagesToCollection = function (website, pages) {
            angular.forEach(pages, function (value, key) {
                website.allPages.push(value);
            });
        };

        var reloadWebsites = function () {
            $http.get("/api/websites/" + tenantService.getTenant().id, { cache: false })
                .success(function (data) {
                    $scope.websites = data;

                    angular.forEach($scope.websites, function (value, key) {
                        angular.extend(value, {
                            isOpen: false,
                            isLoaded: false,
                            pages: [],
                            starred: [],
                            allPages: [],
                            searchText: '',
                        });

                        $scope.$watch("websites[" + key + "].isOpen", function () {
                            if (value.isOpen && !value.isLoaded) {
                                openWebsite(value);
                            }
                        });

                        $scope.$watch("websites[" + key + "].searchText", function() {
                            $scope.searchPages(value);
                            if (!value.searchText) {
                                collapse(value);
                            }
                        });
                    });

                }).error(function (err) {
                    $scope.websites = {};
                });
        };

        var openWebsite = function (website) {
            if (!website.isLoaded) {
                website.isLoaded = true;
                $http.get('/api/pages/' + website.id, { cache: false }).success(function (pageData) {
                    website.pages = pageData;
                    addPagesToCollection(website, website.pages);
                    resetChildren(website);
                });
                $http.get('/api/pages/' + website.id + '/starred', { cache: false }).success(function (starredData) {
                    website.starred = starredData;
                    addPagesToCollection(website, website.starred);
                });
            }
        };

        $rootScope.$on("tenantChanged", reloadWebsites);

        $rootScope.$on('logout', function () {
            $scope.websites = {};
        });

        reloadWebsites();
    }]);

    angular.module('app').controller("PageEditLayoutController", ["$scope", "$modalInstance", "$http", "tenantService", "page", function ($scope, $modalInstance, $http, tenantService, page) {
        $scope.scope = { selectedTemplate: {} };
        $scope.templates = [];

        $scope.ok = function () {
            $modalInstance.close($scope.scope.selectedTemplate);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.selectTemplate = function (e) {
            $scope.selectedTemplate = $(e.target).data('id');
        };

        var tenantId = tenantService.getTenant().id;

        $http.get('/api/pages/templates/' + tenantId, { cache: false }).success(function (data) {
            _.each(data, function (d) {
                d.isShared = false;
                $scope.templates.push(d);

                if (d.id === page.templateId) {
                    $scope.scope.selectedTemplate = d;
                }
            });
        });

        $http.get("/api/pagetemplates/" + tenantId + "/shared", { cache: false }).success(function (data) {
            _.each(data, function (d) {
                d.isShared = true;
                $scope.templates.push(d);

                if (d.id === page.templateId) {
                    $scope.scope.selectedTemplate = d;
                }
            });
        });
    }]);

    angular.module("app").controller("PageLinkDialogController", ["$scope", "$modalInstance", "$http", "tenantService", "dialogService", "page", "subject", "name", "url", function ($scope, $modalInstance, $http, tenantService, dialogService, page, subject, name, url) {
        $scope.page = page;
        $scope.name = name;
        $scope.subject = subject;
        $scope.url = url;

        $scope.getPages = function (value) {
            var q = String.format("/api/page/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(q, { cache: false }).then(function (response) {
                return response.data;
            });
        };

        $scope.browsePage = function () {
            var p = null;
            if ($scope.page) {
                p = $scope.page;
            }
            dialogService.selectPageDialog(p, function (r) {
                $scope.page = {};
                if (!r) {
                    return;
                }
                r.children = [];
                $scope.page = r;
            });
        };

        $scope.ok = function () {
            $modalInstance.close({
                name: $scope.name,
                page: $scope.page,
                url: $scope.url,
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());
(function() {
    angular.module("app").controller("PageTemplateController", ["$scope", "$http", "$log", "$routeParams", "$timeout", "$modal", "toastr", "toastService", "dialogService", "$translate", function ($scope, $http, $log, $routeParams, $timeout, $modal, toastr, toastService, dialogService, $translate) {
        $scope.templateId = $routeParams.templateId;

        var renamePageTemplate = "";
        var errorRenamePageTemplate = "";
        var changePageTemplate = "";
        var errorChangePageTemplate = "";
        var tName = "";

        $translate(["rename_page_template", "error_rename_page_template", "error_change_page_template", "success_change_page_template", "name"]).then(function (t) {
            renamePageTemplate = t.rename_page_template;
            errorRenamePageTemplate = t.error_rename_page_template;
            changePageTemplate = t.success_change_page_template;
            errorChangePageTemplate = t.error_change_page_template;
            tName = t.name;
        });

        $scope.codemirrorOptions = {
            lineNumbers: true,
            theme: 'neo',
            lineWrapping: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            matchTags: { bothTags: true },
            autoCloseTags: true,
            styleActiveLine: true,
            viewportMargin: Infinity,
            mode: "htmlmixed",
        };

        function updateEditorMode() {
            if ($scope.template) {
                if ($scope.template.contentType === "text/html") {
                    $scope.codemirrorOptions.mode = "htmlmixed";
                } else if ($scope.template.contentType === "text/plain") {
                    $scope.codemirrorOptions.mode = "";
                } else if ($scope.template.contentType === "text/calendar") {
                    $scope.codemirrorOptions.mode = "";
                } else if ($scope.template.contentType === "application/xml") {
                    $scope.codemirrorOptions.mode = "xml";
                } else if ($scope.template.contentType === "application/xhtml+xml") {
                    $scope.codemirrorOptions.mode = "xml";
                } else if ($scope.template.contentType === "application/atom+xml") {
                    $scope.codemirrorOptions.mode = "xml";
                } else if ($scope.template.contentType === "application/rss+xml") {
                    $scope.codemirrorOptions.mode = "xml";
                } else if ($scope.template.contentType === "application/json") {
                    $scope.codemirrorOptions.mode = "javascript";
                } else if ($scope.template.contentType === "text/csv") {
                    $scope.codemirrorOptions.mode = "";
                } else if ($scope.template.contentType === "application/javascript") {
                    $scope.codemirrorOptions.mode = "javascript";
                } else if ($scope.template.contentType === "text/css") {
                    $scope.codemirrorOptions.mode = "css";
                } else {
                    $scope.codemirrorOptions.mode = "";
                }
            } else {
                $scope.codemirrorOptions.mode = "htmlmixed";
            }
        };

        $http.get("/api/pagetemplate/" + $scope.templateId, { cache: false }).success(function (data) {
            $scope.template = data;

            $http.get("/api/pagetemplates/contenttypes", { cache: false }).success(function (ct) {
                $scope.contentTypes = ct;
                var s = _.find(ct, function (c) { return c.contentType === $scope.template.contentType; });
                if (s) {
                    $scope.contentTypeName = s.name;
                }
            });

            $http.get("/api/file/" + $scope.template.iconId + "/detail", { cache: false }).success(function (f) {
                $scope.fileName = f.fullname;
            }).error(function(error) {
                $scope.fileName = "n/a";
            });

            updateEditorMode();
        });

        $scope.editName = function() {
            dialogService.singleInputDialog(renamePageTemplate, tName, $scope.template.name, function (name) {
                var dto = {
                    name: name
                };
                $http.post("/api/pagetemplate/" + $scope.template.id + "/name", dto).success(function () {
                    $scope.template.name = name;
                }).error(function(error) {
                    $log.error(angular.toJson(error));
                    toastService.error(errorRenamePageTemplate, error);
                });
            });
        };

        $scope.editContentType = function() {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/masterdata/changePageTemplateContentTypeTemplate.min.html",
                controller: "ChangePageTemplateContentTypeController",
                resolve: {
                    contentType: function() { return $scope.template.contentType; }
                }
            });

            modalInstance.result.then(function (contentType) {
                if (contentType && contentType.name && contentType.contentType) {
                    var dto = {
                        contentType: contentType.contentType,
                    };
                    $http.post("/api/pagetemplate/" + $scope.template.id + "/contenttype", dto).success(function (data) {
                        $scope.template.contentType = contentType.contentType;
                        $scope.contentTypeName = contentType.name;
                        updateEditorMode();
                    }).error(function (error) {
                        $log.error(angular.toJson(error));
                        toastService.error(errorChangePageTemplate, error);
                    });
                }
            });
        };

        $scope.editImage = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/masterdata/changePageTemplateImageTemplate.min.html",
                controller: "ChangePageTemplateImageController",
                resolve: {
                    fileId: function () { return $scope.template.iconId; }
                }
            });

            modalInstance.result.then(function (file) {
                if (file && file.name && file.id) {
                    $http.post("/api/pagetemplate/" + $scope.template.id + "/icon/" + file.id).success(function (data) {
                        $scope.template.icon = file.downloadUrl;
                        $scope.template.iconId = file.id;
                        $scope.fileName = file.fullname;
                    }).error(function (error) {
                        $log.error(angular.toJson(error));
                        toastService.error(errorChangePageTemplate, error);
                    });
                }
            });
        };

        $scope.saveTemplate = function () {
            return $http.post("/api/pagetemplate/" + $scope.templateId + "/template", $scope.template).success(function () {
                toastr.success('', changePageTemplate, { allowHtml: false, timeOut: 1000 });
                $log.info("saved page template");
            }).error(function (error) {
                $log.error(angular.toJson(error));
                toastService.error(errorChangePageTemplate, error);
            });
        };
    }]);

    angular.module('app').controller("ChangePageTemplateContentTypeController", ["$scope", "$modalInstance", "$http", "contentType", function ($scope, $modalInstance, $http, contentType) {
        $scope.scope = {
            name: "",
            contentType: {}
        };

        $scope.ok = function () {
            $modalInstance.close($scope.scope.contentType);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $http.get("/api/pagetemplates/contenttypes", { cache: false }).success(function (data) {
            $scope.contentTypes = data;
            var ct = _.find(data, function (c) { return c.contentType === contentType; });
            if (ct) {
                $scope.scope.contentType = ct;
            }
        });
    }]);

    angular.module('app').controller("ChangePageTemplateImageController", ["$scope", "$modalInstance", "$http", "dialogService", "tenantService", "fileId", function ($scope, $modalInstance, $http, dialogService, tenantService, fileId) {
        $scope.scope = {
            file: {},
        };

        $scope.browseFile = function () {
            dialogService.selectFileDialog($scope.scope.file, function (f) {
                if (!f) {
                    return;
                }
                $scope.scope.file = f;
            });
        };

        $scope.ok = function () {
            $modalInstance.close($scope.scope.file);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $http.get("/api/file/" + fileId + "/detail", { cache: false }).success(function (data) {
            $scope.scope.file = data;
        });

        $scope.getFiles = function (value) {
            var url = String.format("/api/file/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, {cache:false}).then(function (response) {
                return response.data;
            });
        };
    }]);
}());
(function() {
    angular.module("app").controller("PageTemplatesController", [
        "$rootScope", "$scope", "$http", "$log", "$location", "tenantService", "dialogService", "$modal", "$translate", "toastService",
        function ($rootScope, $scope, $http, $log, $location, tenantService, dialogService, $modal, $translate, toastService) {
            $scope.templates = [];
            $scope.selectedTemplate = {};

            function reloadTemplates() {
                $scope.templates = [];
                $scope.selectedTemplate = {};

                var tenantId = tenantService.getTenant().id;

                $http.get("/api/pagetemplates/" + tenantId, { cache: false }).success(function (data) {
                    _.each(data, function (d) {
                        d.isShared = false;
                        $scope.templates.push(d);
                    });
                });

                $http.get("/api/pagetemplates/" + tenantId + "/shared", { cache: false }).success(function (data) {
                    _.each(data, function (d) {
                        d.isShared = true;
                        $scope.templates.push(d);
                    });
                });
            };

            $http.get("/api/permission/pagetemplate/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
                $scope.canEdit = permission.canEdit;
                $scope.canAdmin = permission.canAdmin;
            });

            $scope.select = function (template) {
                $scope.selectedTemplate = template;
            };

            $scope.shareTemplate = function (template) {
                var existingTenantIds = [];

                if (template.shared) {
                    existingTenantIds = _.map(template.shared, function (t) { return t.id; });
                }

                dialogService.selectTenantsDialog(existingTenantIds, function (tenants) {
                    var ids = _.map(tenants, function (t) {
                        return t.id;
                    });
                    var dto = {
                        tenantIds: ids
                    };
                    var url = String.format("/api/pagetemplate/{0}/share", template.id);
                    $http.put(url, dto).success(function () {
                        template.shared = tenants;
                    });
                });
            };

            $scope.donotshareTemplate = function (template) {
                var url = String.format("/api/pagetemplate/{0}/share", template.id);
                var dto = {
                    tenantIds: []
                };
                return $http.put(url, dto).success(function () {
                    template.shared = [];
                });
            };

            $scope.getNames = function (tenants) {
                var names = "";
                _.each(tenants, function (t) {
                    names += t.name + ", ";
                });
                names = names.trim();
                return names.substring(0, names.length - 1);
            };

            var errorCreatePageTemplate = "";
            var renamePageTemplate = "";
            var name = "";
            var errorRenamePageTemplate = "";
            var deletePageTemplate = "";
            var confirmDeletePageTemplate = "";
            var errorDeletePageTemplate = "";

            $translate(["error_create_page_template", "rename_page_template", "name", "error_rename_page_template", "delete_page_template", "confirm_delete_page_template", "error_delete_page_template"]).then(function (t) {
                errorCreatePageTemplate = t.error_create_page_template;
                renamePageTemplate = t.rename_page_template;
                name = t.name;
                errorRenamePageTemplate = t.error_rename_page_template;
                deletePageTemplate = t.delete_page_template;
                confirmDeletePageTemplate = t.confirm_delete_page_template;
                errorDeletePageTemplate = t.error_delete_page_template;
            });

            $scope.create = function () {
                var modalInstance = $modal.open({
                    backdrop: 'static',
                    templateUrl: "/admin/views/masterdata/createPageTemplateTemplate.min.html",
                    controller: "CreatePageTemplateController"
                });

                modalInstance.result.then(function (template) {
                    if (template && template.name && template.contentType) {
                        var dto = {
                            name: template.name,
                            contentType: template.contentType.contentType,
                        };
                        $http.put("/api/pagetemplates/" + tenantService.getTenant().id, dto).success(function (data) {
                            $scope.templates.push(data);
                            $scope.selectedTemplate = data;
                        }).error(function (error) {
                            toastService.error(errorCreatePageTemplate, error);
                        });
                    }
                });
            };

            $scope.edit = function (template) {
                $location.path("/master/pagetemplate/" + template.id);
            };

            $scope.rename = function (template) {
                dialogService.singleInputDialog(renamePageTemplate, name, template.name, function (n) {
                    var dto = {
                        name: n
                    };
                    $http.post("/api/pagetemplate/" + template.id + "/name", dto).success(function() {
                        template.name = n;
                    }).error(function(error) {
                        toastService.error(errorRenamePageTemplate, error);
                    });
                });
            };

            $scope.delete = function (template) {
                dialogService.confirmDialog(deletePageTemplate, String.format(confirmDeletePageTemplate, template.name), function () {
                    $http.delete("/api/pagetemplate/" + template.id).success(function () {
                        var others = _.reject($scope.templates, function(t) {
                            return t.id === template.id;
                        });
                        $scope.templates = others;
                    }).error(function (error) {
                        toastService.error(errorDeletePageTemplate, error);
                    });
                });
            };

            $rootScope.$on("tenantChanged", reloadTemplates);

            $rootScope.$on('logout', function () {
                $scope.templates = [];
            });

            reloadTemplates();
        }
    ]);

    angular.module('app').controller("CreatePageTemplateController", ["$scope", "$modalInstance", "$http", function ($scope, $modalInstance, $http) {
        $scope.scope = {
            name: "",
            contentTyp: {}
        };

        $scope.ok = function () {
            $modalInstance.close($scope.scope);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $http.get("/api/pagetemplates/contenttypes", { cache: false }).success(function (data) {
            $scope.contentTypes = data;
        });
    }]);
}());
(function() {
    angular.module("app").controller("EventlogController", ["$rootScope", "$scope", "$http", "tenantService", function ($rootScope, $scope, $http, tenantService) {
        $scope.events = [];
        $scope.isBusy = false;
        var isFinish = false;

        $scope.nextPage = function() {
            if ($scope.isBusy || isFinish) {
                return;
            }
            $scope.isBusy = true;

            var count = $scope.events.length;
            var url = String.format("/api/events/{1}?$skip={0}&$top=20&$orderby=Id desc", count, tenantService.getTenant().id);
            $http.get(url, { cache: false }).success(function (data) {
                if (data.length < 20) {
                    isFinish = true;
                }

                for (var i = 0; i < data.length; i++) {
                    $scope.events.push(data[i]);
                }
                $scope.isBusy = false;
            });
        };

        $rootScope.$on("logout", function () { $scope.events = []; });

        $rootScope.$on("tenantChanged", function() {
            $scope.events = [];
            isFinish = false;
            $scope.isBusy = false;
            $scope.nextPage();
        });
    }]);

    angular.module("app").controller("AggregateEventlogController", ["$rootScope", "$scope", "$http", "$routeParams", "tenantService", function ($rootScope, $scope, $http, $routeParams, tenantService) {
        var aggregateId = $routeParams.aggregateId;
        var aggregateType = $routeParams.aggregateType;
        $scope.events = [];
        $scope.isBusy = false;
        var isFinish = false;

        $scope.nextPage = function () {
            if ($scope.isBusy || isFinish) {
                return;
            }
            $scope.isBusy = true;

            var count = $scope.events.length;
            var url = String.format("/api/events/{1}/object/{3}/{2}?$skip={0}&$top=20&$orderby=Id desc", count, tenantService.getTenant().id, aggregateId, aggregateType);
            $http.get(url, { cache: false }).success(function (data) {
                if (data.length < 20) {
                    isFinish = true;
                }

                for (var i = 0; i < data.length; i++) {
                    $scope.events.push(data[i]);
                }
                $scope.isBusy = false;
            });
        };

        $rootScope.$on("logout", function () { $scope.events = []; });

        $rootScope.$on("tenantChanged", function () {
            $scope.events = [];
            isFinish = false;
            $scope.isBusy = false;
            $scope.nextPage();
        });
    }]);
}());
(function() {
    angular.module("app").service("systemMessageService", ["$http", "$log", "$interval", "$timeout", "toastr", function($http, $log, $interval, $timeout, toastr) {
        function getMessages() {
            $http.get("/api/systemmessages/current", { cache: false }).success(function(data) {
                _.each(data, function (msg) {
                    var body = msg.message + "<br/> - " + msg.user;
                    toastr.warning(body, msg.title, {
                        allowHtml: true,
                        closeButton: true,
                        timeOut: 0,
                        extendedTimeOut: 0
                    });
                });
            });
        };

        $timeout(getMessages, 10000); // show messages the first time after 10 seconds
        $interval(getMessages, 600000); // show messages all 10 minutes
    }]);

    angular.module("app").controller("SystemMessagesController", ["$rootScope", "$scope", "$http", "$modal", "tenantService", "toastService", "dialogService", "$translate", function ($rootScope, $scope, $http, $modal, tenantService, toastService, dialogService, $translate) {
        $scope.selectedMessage = {};

        $http.get("/api/systemmessages/", { cache: false }).success(function (data) {
            $scope.messages = data;
        });

        $scope.select = function(message) {
            $scope.selectedMessage = message;
        };

        var createMessage = "";
        var errorCreateMessage = "";
        var editMessage = "";
        var errorEditMessage = "";
        var deleteMessage = "";
        var confirmDeleteMessage = "";
        var errorDeleteMessage = "";

        $translate(["create_system_message", "error_create_system_message", "edit_system_message", "error_edit_system_message", "delete_system_message", "confirm_delete_system_message", "error_delete_system_message"]).then(function(t) {
            createMessage = t.create_system_message;
            errorCreateMessage = t.error_create_system_message;
            editMessage = t.edit_system_message;
            errorEditMessage = t.error_edit_system_message;
            deleteMessage = t.delete_system_message;
            confirmDeleteMessage = t.confirm_delete_system_message;
            errorDeleteMessage = t.error_delete_system_message;
        });

        $scope.create = function() {
            openMessageDialog(createMessage, {}, function(m) {
                $http.post("/api/systemmessages/" + tenantService.getTenant().id, m).success(function(message) {
                    $scope.messages.push(message);
                }).error(function(error) {
                    toastService.error(errorCreateMessage, error);
                });
            });
        };

        $scope.edit = function(message) {
            openMessageDialog(editMessage, message, function(m) {
                $http.post("/api/systemmessages/" + tenantService.getTenant().id + "/" + message.id, m).success(function (data) {
                    message.title = data.title;
                    message.message = data.message;
                    message.validity = data.validity;
                    message.startDate = data.startDate;
                    message.endDate = data.endDate;
                    message.startTime = data.startTime;
                    message.endTime = data.endTime;
                }).error(function (error) {
                    toastService.error(errorEditMessage , error);
                });
            });
        };

        $scope.delete = function (message) {
            dialogService.confirmDialog(deleteMessage, String.format(confirmDeleteMessage, message.title), function() {
                $http.delete("/api/systemmessages/" + tenantService.getTenant().id + "/" + message.id).success(function () {
                    $scope.messages = _.reject($scope.messages, function (m) { return m.id === message.id; });
                }).error(function (error) {
                    toastService.error(errorDeleteMessage, error);
                });
            });
        };

        function openMessageDialog(subject, message, func) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/views/system/messageDialog.min.html',
                controller: 'SystemMessageDialogController',
                resolve: {
                    subject: function () {
                        return subject;
                    },
                    message: function () {
                        return angular.copy(message);
                    },
                }
            });

            modalInstance.result.then(function (a) {
                func(a);
            });
        };

        $rootScope.$on("logout", function() { $scope.messages = []; });
    }]);

    angular.module("app").controller("SystemMessageDialogController", ["$scope", "$modalInstance", "$filter", "$translate", "subject", "message", function ($scope, $modalInstance, $filter, $translate, subject, message) {
        $scope.subject = subject;
        $scope.message = message;
        $scope.times = [];

        for (var i = 0; i < 24; i++) {
            var hour = i + ":";
            if (hour.length < 3) {
                hour = "0" + hour;
            }

            for (var m = 0; m < 60; m += 5) {
                var minute = m;
                if (m < 10) {
                    minute = "0" + minute;
                }

                $scope.times.push(hour + minute);
            }
        }

        if ($scope.message.startDate) {
            $scope.message.startDatePicker = $scope.message.startDate;
        }
        if ($scope.message.endDate) {
            $scope.message.endDatePicker = $scope.message.endDate;
        }

        $scope.$watch("message.startTime", updateDuration);
        $scope.$watch("message.endTime", updateDuration);
        $scope.$watch("message.startDatePicker", updateDuration);
        $scope.$watch("message.endDatePicker", updateDuration);

        var t_minutes = "";
        var t_hours = "";
        var t_days = "";

        $translate(["minutes", "hours", "days"]).then(function(t) {
            t_minutes = t.minutes;
            t_hours = t.hours;
            t_days = t.days;
        });

        function updateDuration() {
            if ($scope.message.startDatePicker && $scope.message.endDatePicker && $scope.message.startTime && $scope.message.endTime) {
                var s = $scope.message.startTime.split(':');
                var e = $scope.message.endTime.split(':');

                $scope.message.startDate = $filter('date')($scope.message.startDatePicker, 'yyyy-MM-dd') + "T00:00:00.000Z";
                $scope.message.endDate = $filter('date')($scope.message.endDatePicker, 'yyyy-MM-dd') + "T00:00:00.000Z";

                $scope.start = moment($scope.message.startDate);
                $scope.start.add(parseInt(s[0]), 'hours');
                $scope.start.add(parseInt(s[1]), 'minutes');
                $scope.end = moment($scope.message.endDate);
                $scope.end.add(parseInt(e[0]), 'hours');
                $scope.end.add(parseInt(e[1]), 'minutes');

                var minutes = $scope.end.diff($scope.start, 'minutes');
                if (minutes < 60) {
                    $scope.duration = toReadableNumber(minutes) + " " + t_minutes;
                    return;
                }
                var hours = $scope.end.diff($scope.start, 'hours', true);
                if (hours < 24) {
                    $scope.duration = toReadableNumber(hours) + " " + t_hours;
                    return;
                }
                var days = $scope.end.diff($scope.start, 'days', true);
                $scope.duration = toReadableNumber(days) + " " + t_days;
            }
        };

        function toReadableNumber(number) {
            var n = number.toFixed(2);
            if (n.charAt(n.length - 1) == "0") {
                n = n.substring(0, n.length - 1);
                if (n.charAt(n.length - 1) == "0") {
                    n = n.substring(0, n.length - 2);
                }
            }
            return n;
        };

        $scope.openStartDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.startDateOpened = true;
        };

        $scope.openEndDate = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.endDateOpened = true;
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };

        $scope.ok = function () {
            $modalInstance.close($scope.message);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());
(function () {
    angular.module("app").constant('$', window.jQuery);

    angular.module("app").controller("ServerLogController", ["$", function ($) {
        var connection = $.hubConnection();
        var hubProxy = connection.createHubProxy('signalrAppenderHub');

        hubProxy.on('onLoggedEvent', function(msg, event) {
            var dateCell = $("<td>").css("white-space", "nowrap").text(event.TimeStamp);
            var levelCell = $("<td>").text(event.Level);
            var detailsCell = $("<td>").text(event.Message);
            var row = $("<tr>").append(dateCell, levelCell, detailsCell);

            if (event.Level === "WARN") {
                row.css("background-color", "#FFFFCC");
            } else if (event.Level === "DEBUG") {
                row.css("color", "lightgray");
            } else if (event.Level === "ERROR") {
                row.css("background-color", "#FF9966");
            }

            $('#log-table tbody').append(row);
        });

        connection.start();
    }]);
}());
(function() {
    angular.module("app").controller("SettingsController", ["$rootScope", "$scope", "$http", "$timeout", "tenantService", "toastService", "$translate", "$log", "toastr", function($rootScope, $scope, $http, $timeout, tenantService, toastService, $translate, $log, toastr) {
        var emailUrl = String.format("/api/settings/{0}/email", tenantService.getTenant().id);
        var urlUrl = String.format("/api/url/settings/{0}", tenantService.getTenant().id);
        var urlWatchSet = false;

        function getSettings() {
            $scope.email = {};
            $scope.urlShortener = {};

            $http.get(emailUrl, { cache: false }).success(function (data) {
                $scope.email = data;
            });

            $http.get(urlUrl, { cache: false }).success(function (data) {
                $scope.urlShortener = data;

                if (data && data.selectedService) {
                    $scope.urlShortener.selection = _.find(data.services, function(s) { return s.name === data.selectedService; });
                }

                if (urlWatchSet) {
                    return;
                }

                urlWatchSet = true;

                $scope.$watch("urlShortener.selection", function (newValue) {
                    if (newValue) {
                        $scope.urlShortener.selectedService = newValue.name;
                        $scope.urlShortener.configuration = newValue.configuration;
                    } else {
                        $scope.urlShortener.selectedService = null;
                        $scope.urlShortener.configuration = [];
                    }
                });
            });
        };

        $scope.saveEmail = function() {
            return $http.post(emailUrl, $scope.email).success(function() {
                $translate("success_save_email_settings").then(function (t) {
                    toastr.success('', t);
                });
            }).error(function (error) {
                $translate("error_save_email_settings").then(function(t) {
                    toastService.error(t, error);
                });
            });
        };

        $scope.saveUrl = function() {
            return $http.post(urlUrl, $scope.urlShortener).success(function() {
                _.each($scope.urlShortener.configuration, function(c) {
                    c.value = "";
                });
                $translate("success_save_url_shortening_configuration").then(function(t) {
                    toastr.success('', t);
                });
            }).error(function (error) {
                $translate("error_save_url_shortening_configuration").then(function (t) {
                    toastService.error(t, error);
                });
            });
        };

        $scope.verifyUrl = function() {
            return $http.post(urlUrl + "/validate").success(function () {
                $translate("success_verify_url_shortening_configuration").then(function (t) {
                    toastr.success('', t);
                });
            }).error(function () {
                $translate("error_verify_url_shortening_configuration").then(function (t) {
                    toastr.error('', t);
                });
            });
        };

        $timeout(function() {
            $scope.$watch("email.emailAddress", function(newValue) {
                if (newValue && !$scope.email.responseAddress) {
                    $scope.email.responseAddress = $scope.email.emailAddress;
                }
            });
        }, 3000);

        getSettings();

        $rootScope.$on("logout", function() {
            $scope.email = {};
            $scope.urlShortener = {};
        });

        $rootScope.$on("tenantChanged", function() {
            emailUrl = String.format("/api/settings/{0}/email", tenantService.getTenant().id);
            urlUrl = String.format("/api/url/settings/{0}", tenantService.getTenant().id);
            getSettings();
        });
    }]);
}());
(function() {
    angular.module("app").controller("FileStoreController", ["$scope", "$http", "$log", "$modal", "toastService", "toastr", "$translate", "tenantService", function ($scope, $http, $log, $modal, toastService, toastr, $translate, tenantService) {

        $scope.fileStores = [];
        $scope.selectedFileStore = {};

        $http.get("/api/filestores/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
            $scope.fileStores = data;
        });

        $scope.selectFileStore = function(fileStore) {
            $scope.selectedFileStore = fileStore;
        };

        var errorCreatingFileStore = "";

        $translate(["error_creating_file_store"]).then(function(t) {
            errorCreatingFileStore = t.error_creating_file_store;
        });

        $scope.create = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/views/system/filestoreDialog.min.html',
                controller: 'FileStoreDialogController',
            });

            modalInstance.result.then(function (a) {
                $http.post("/api/filestores/factory/" + a.factoryId, a).success(function(data) {
                    $scope.fileStores.push(data);
                }).error(function(error) {
                    toastService.error(errorCreatingFileStore, error);
                });
            });
        };
    }]);

    angular.module("app").controller("FileStoreDialogController", ["$scope", "$modalInstance", "$http", "$translate", function ($scope, $modalInstance, $http, $translate) {

        $scope.factories = [];
        $scope.settings = [];
        $scope.selectedFactory = {};
        $scope.fileStore = {
            name: "",
            factoryId: "",
            settings: {}
        };
        $scope.validationClass = "btn-default";

        $http.get("/api/filestores/factories", { cache: false }).success(function(data) {
            $scope.factories = data;
        });

        $scope.$watch("selectedFactory", function() {
            if (!$scope.selectedFactory || !$scope.selectedFactory.id) {
                return;
            }

            $scope.fileStore.factoryId = $scope.selectedFactory.id;
            $http.get("/api/filestores/factory/" + $scope.selectedFactory.id, { cache: false }).success(function(data) {
                $scope.settings = data;
                $scope.fileStore.settings = {};

                _.each(data, function(s) {
                    $scope.fileStore.settings[s] = "";
                });
            });
        });

        $scope.validate = function () {
            $http.post("/api/filestores/factory/" + $scope.selectedFactory.id + "/validate", $scope.fileStore).success(function (result) {
                if (result.isValid) {
                    $scope.validationClass = "btn-success";
                } else {
                    $scope.validationClass = "btn-danger";
                }
            });
        };

        $scope.ok = function () {
            $modalInstance.close($scope.fileStore);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());
(function() {
    angular.module("app").controller("GroupsController", ["$rootScope", "$scope", "$http", "$location", "tenantService", "dialogService", "toastService", "$translate", function ($rootScope, $scope, $http, $location, tenantService, dialogService, toastService, $translate) {
        var createGroup = "";
        var name = "";
        var errorCreateGroup = "";
        var renameGroup = "";
        var errorRenameGroup = "";
        var deleteGroup = "";
        var confirmDeleteGroup = "";
        var errorDeleteGroup = "";

        $translate(["create_group", "name", "error_create_group", "rename_group", "error_rename_group", "delete_group", "confirm_delete_group", "error_delete_group"]).then(function(t) {
            createGroup = t.create_group;
            name = t.name;
            errorCreateGroup = t.error_create_group;
            renameGroup = t.rename_group;
            errorRenameGroup = t.error_rename_group;
            deleteGroup = t.delete_group;
            confirmDeleteGroup = t.confirm_delete_group;
            errorDeleteGroup = t.error_delete_group;
        });

        $http.get("/api/permission/group/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        $scope.create = function() {
            dialogService.singleInputDialog(createGroup, name, "", function (n) {
                var dto = {
                    name: n
                };
                $http.post("/api/groups/" + tenantService.getTenant().id, dto).success(function(group) {
                    $scope.groups.push(group);
                }).error(function (e) {
                    toastService.error(errorCreateGroup, e);
                });
            });
        };

        $scope.select = function(group) {
            $scope.selectedGroup = group;
        };

        $scope.rename = function(group) {
            dialogService.singleInputDialog(renameGroup, name, group.name, function (n) {
                var dto = {
                    name: n
                };
                $http.post("/api/group/" + group.id + "/name", dto).success(function() {
                    group.name = n;
                }).error(function(e) {
                    toastService.error(errorRenameGroup, e);
                });
            });
        };

        $scope.delete = function(group) {
            dialogService.confirmDialog(deleteGroup, String.format(confirmDeleteGroup, group.name), function() {
                $http.delete("/api/group/" + group.id).success(function() {
                    $scope.groups = _.reject($scope.groups, function(g) { return g.id === group.id; });
                }).error(function(e) {
                    toastService.error(errorDeleteGroup, e);
                });
            });
        };

        $scope.editMembers = function(group) {
            $location.path("/user/group/" + group.id + "/members");
        };

        $scope.editPermissions = function(group) {
            $location.path("/user/group/" + group.id + "/permissions");
        };

        function loadGroups() {
            $http.get("/api/groups/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
                $scope.groups = data;
            });
        };

        $rootScope.$on("tenantChanged", loadGroups);

        $rootScope.$on('logout', function () {
            $scope.groups = [];
        });

        loadGroups();
    }]);

    angular.module("app").controller("MemberController", ["$rootScope", "$scope", "$routeParams", "$http", "$log", "toastService", "dialogService", "$translate", function ($rootScope, $scope, $routeParams, $http, $log, toastService, dialogService, $translate) {
        var groupId = $routeParams.groupId;
        var errorAddMember = "";
        var errorRemoveMember = "";

        $scope.selectedUser = {};

        $translate(["error_add_member", "error_remove_member"]).then(function (t) {
            errorAddMember = t.error_add_member;
            errorRemoveMember = t.error_remove_member;
        });

        function loadMembers() {
            $http.get("/api/members/" + groupId, { cache: false }).success(function(data) {
                $scope.members = data;
            });
        };

        $scope.select = function(user) {
            $scope.selectedUser = user;
        };

        $scope.add = function() {
            dialogService.selectUserDialog(function(user) {
                $http.post("/api/members/" + groupId + "/add/" + user.id).success(function () {
                    $scope.members.push(user);
                }).error(function (e) {
                    toastService.error(errorAddMember, e);
                });
            });
        };

        $scope.remove = function(user) {
            return $http.post("/api/members/" + groupId + "/remove/" + user.id).success(function() {
                $scope.members = _.reject($scope.members, function(m) { return m.id === user.id; });
            }).error(function(e) {
                toastService.error(errorRemoveMember, e);
            });
        };

        $rootScope.$on('logout', function () {
            $scope.members = [];
        });

        loadMembers();
    }]);

    angular.module("app").controller("PermissionController", ["$rootScope", "$scope", "$routeParams", "$http", "$log", "toastService", "$translate", function ($rootScope, $scope, $routeParams, $http, $log, toastService, $translate) {
        var groupId = $routeParams.groupId;

        $scope.p0 = 0;
        $scope.p1 = 1;
        $scope.p2 = 2;
        $scope.p3 = 3;

        function loadPermissions() {
            $http.get("/api/permissions/" + groupId, { cache: false }).success(function(data) {
                $scope.permissions = data;

                for (var i = 0; i < data.length; i++) {
                    $scope.$watch("permissions[" + i + "]", function (newValue, oldValue) {
                        if (!newValue || !oldValue || newValue.permission === oldValue.permission) {
                            return;
                        }

                        $log.debug(angular.toJson(newValue));

                        $http.post("/api/permissions/" + groupId, newValue).error(function (e) {
                            newValue.permission = oldValue.permission;

                            $translate("error_save_permission").then(function(t) {
                                toastService.error(t, e);
                            });
                        });
                    }, true);
                }
            });
        };

        $rootScope.$on('logout', function () {
            $scope.permissions = [];
        });

        loadPermissions();
    }]);
}());
(function() {
    angular.module("app").controller("ProfileController", ["$rootScope", "$scope", "$http", "$timeout", "$log", "toastService", "userService", "dialogService", "tenantService", "$translate", "$modal", function ($rootScope, $scope, $http, $timeout, $log, toastService, userService, dialogService, tenantService, $translate, $modal) {
        var user = userService.currentUser();

        $scope.events = [];
        $scope.isBusy = false;
        $scope.isChrome = false;
        var isFinish = false;

        if (window.chrome) {
            $scope.isChrome = true;
        }

        var changeName = "";
        var firstName = "";
        var lastName = "";
        var errorSaveName = "";
        var errorSavePassword = "";
        var changePassword = "";
        var enterNewPassword = "";

        $translate(["change_name", "first_name", "last_name", "error_save_name", "error_save_password", "change_password", "enter_new_password"]).then(function (t) {
            changeName = t.change_name;
            firstName = t.first_name;
            lastName = t.last_name;
            errorSaveName = t.error_save_name;
            errorSavePassword = t.error_save_password;
            changePassword = t.change_password;
            enterNewPassword = t.enter_new_password;
        });

        $scope.nextPage = function () {
            if ($scope.isBusy || isFinish) {
                return;
            }
            $scope.isBusy = true;

            var count = $scope.events.length;
            var url = String.format("/api/events/{1}/user/{2}?$skip={0}&$top=20&$orderby=Id desc", count, tenantService.getTenant().id, user.id);
            $http.get(url, { cache: false }).success(function (data) {
                if (data.length < 20) {
                    isFinish = true;
                }

                for (var i = 0; i < data.length; i++) {
                    $scope.events.push(data[i]);
                }
                $scope.isBusy = false;
            });
        };

        $scope.changeName = function () {
            dialogService.twoInputsDialog(changeName, firstName, user.firstName, lastName, user.lastName, function (firstName, lastName) {
                var url = String.format("/api/users/{0}/user/name", tenantService.getTenant().id);
                $http.post(url, { firstName: firstName, lastName: lastName }).success(function() {
                    user.firstName = firstName;
                    user.lastName = lastName;
                    user.name = (firstName + " " + lastName).trim();
                    $timeout($scope.nextPage, 5000);
                }).error(function (error) {
                    toastService.error(errorSaveName, error);
                });
            });
        };

        $scope.changePassword = function () {
            dialogService.singlePasswordDialog(changePassword, enterNewPassword, function (passwords) {
                passwords.oldPassword = CryptoJS.enc.Utf8.parse(passwords.oldPassword);
                passwords.oldPassword = CryptoJS.enc.Base64.stringify(passwords.oldPassword);
                passwords.newPassword = CryptoJS.enc.Utf8.parse(passwords.newPassword);
                passwords.newPassword = CryptoJS.enc.Base64.stringify(passwords.newPassword);
                var url = String.format("/api/users/{0}/user/password", tenantService.getTenant().id);
                $http.post(url, passwords).success(function () {
                    $timeout($scope.nextPage, 5000);
                }).error(function (error) {
                    toastService.error(errorSavePassword, error);
                });
            });
        };

        $scope.enable2ndFactor = function() {
            var modalInstance = $modal.open({
                backdrop: "static",
                templateUrl: "/admin/views/pages/enable2ndFactorDialog.min.html",
                controller: "Enable2ndFactorController",
                size: "lg",
                resolve: { }
            });

            modalInstance.result.then(function (a) {
                user.isTotp = true;
            });
        };

        $scope.disable2ndFactor = function () {
            // todo: nachfragen
            $http.delete("/api/2fa/totp").success(function () {
                user.isTotp = false;
            });
        };

        $http.get("/api/u2f/devices", { cache: false }).success(function(devices) {
            $scope.devices = devices.length;
        });

        $scope.addU2FDevice = function () {
            $http.get("/api/u2f/register", { cache: false }).success(function(req) {
                var modalInstance = $modal.open({
                    backdrop: "static",
                    templateUrl: "/admin/views/pages/addU2FDevice.min.html",
                    controller: "AddU2FDeviceController",
                    resolve: {
                        u2frequest: function () { return req; }
                    }
                });

                modalInstance.result.then(function (a) {
                    $scope.devices += 1;
                });
            });
        };

        $scope.removeU2FDevices = function() {
            // todo: open modal dialog with list of all keys
        };

        $rootScope.$on("logout", function () { $scope.events = []; });

        $rootScope.$on("tenantChanged", function () {
            $scope.events = [];
            isFinish = false;
            $scope.isBusy = false;
            $scope.nextPage();
        });
    }]);

    angular.module("app").controller("AddU2FDeviceController", ["$modalInstance", "$scope", "$http", "$log", "u2frequest", function($modalInstance, $scope, $http, $log, u2frequest) {
        $scope.currentStep = 0;
        $scope.isError = false;
        $scope.isValid = false;

        $scope.goToStep = function (step) {
            if ($scope.currentStep < step) {
                return;
            }
            $scope.currentStep = step;
        };

        $scope.save = function () {
            $modalInstance.close();
        };

        $scope.back = function () {
            if ($scope.currentStep > 0) {
                $scope.currentStep--;
            }
        };

        $scope.next = function () {
            if ($scope.currentStep < 1) {
                $scope.currentStep = 1;

                u2f.register([u2frequest], [], function (data) {
                    var dto = {
                        password: $scope.password,
                        jsonData: angular.toJson(data)
                    };

                    if (data.errorCode) {
                        // https://developers.yubico.com/U2F/Libraries/Client_error_codes.html
                        $log.debug("Unable to register device: error code " + data.errorCode);

                        $scope.isError = true;

                        if (data.errorCode === 5) {
                            // timeout
                            // show error message
                        }

                        $scope.isValid = false;
                        return;
                    }

                    $http.post("/api/u2f/register", dto).success(function () {
                        $scope.isValid = true;
                    }).error(function() {
                        $scope.isError = true;
                        // show error message
                    });
                }, 120);
            }
        }

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("Enable2ndFactorController", ["$scope", "$http", "$modalInstance", "$log", "$translate", "currentUser", function ($scope, $http, $modalInstance, $log, $translate, currentUser) {
        $scope.currentStep = 0;
        $scope.isError = false;
        var dto = {};

        $http.get("/api/2fa/totp", { cache: false }).success(function (data) {
            dto = data;
            var qrdata = encodeURIComponent(data.url);
            $scope.qr = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=" + qrdata;
            $scope.secret = data.secret;
        });

        $scope.goToStep = function (step) {
            if ($scope.currentStep < step) {
                return;
            }
            $scope.currentStep = step;
        };

        $scope.save = function () {
            $scope.isError = false;
            dto.code = $scope.code;
            dto.password = $scope.password;

            $http.put("/api/2fa/totp", dto).success(function (data) {
                if (data.isSuccessful) {
                    $modalInstance.close();
                } else {
                    $scope.isError = true;
                }
            });
        };

        $scope.back = function () {
            if ($scope.currentStep > 0) {
                $scope.currentStep--;
            }
        };

        $scope.next = function() {
            if ($scope.currentStep < 1) {
                $scope.currentStep = 1;
            }
        }

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);
}());
(function() {
    angular.module("app").controller("UsersController", ["$rootScope", "$scope", "$http", "$log", "$modal", "$location", "dialogService", "toastService", "tenantService", "$translate", "currentUser", function ($rootScope, $scope, $http, $log, $modal, $location, dialogService, toastService, tenantService, $translate, currentUser) {
        function loadUsers() {
            $http.get("/api/users/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
                $scope.users = data;
            });
        };

        $scope.currentUser = currentUser;

        $http.get("/api/permission/user/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        function showUserModalDialog(subject, user, onAccept) {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/user/editUserDialog.min.html",
                controller: "EditUserController",
                resolve: {
                    user: function () { return user; },
                    subject: function () { return subject; },
                    currentUser: function() { return currentUser; }
                }
            });

            modalInstance.result.then(onAccept);
        };

        $scope.select = function(user) {
            $scope.selectedUser = user;
        };

        var createUser = "";
        var errorCreateUser = "";
        var editUser = "";
        var errorEditUser = "";
        var deleteUser = "";
        var confirmDeleteUser = "";
        var errorDeleteUser = "";
        var errorLockUser = "";
        var errorUnlockUser = "";
        var errorMakeSysadmin = "";
        var errorMakeNotSysadmin = "";
        var errorRemove2Fa = "";

        $translate(["create_user", "error_create_user", "edit_user", "error_edit_user", "delete_user", "confirm_delete_user", "error_delete_user", "error_lock_user", "error_unlock_user", "error_make_sysadmin", "error_make_not_sysadmin", "error_remove_2fa"]).then(function (t) {
            createUser = t.create_user;
            errorCreateUser = t.error_create_user;
            editUser = t.edit_user;
            errorEditUser = t.error_edit_user;
            deleteUser = t.delete_user;
            confirmDeleteUser = t.confirm_delete_user;
            errorDeleteUser = t.error_delete_user;
            errorLockUser = t.error_lock_user;
            errorUnlockUser = t.error_unlock_user;
            errorMakeSysadmin = t.error_make_sysadmin;
            errorMakeNotSysadmin = t.error_make_not_sysadmin;
            errorRemove2Fa = t.error_remove_2fa;
        });

        $scope.create = function() {
            showUserModalDialog(createUser, {}, function (u) {
                if (u.password && u.password.length > 0) {
                    var parsed = CryptoJS.enc.Utf8.parse(u.password);
                    u.password = CryptoJS.enc.Base64.stringify(parsed);
                }
                $http.post("/api/users/" + tenantService.getTenant().id, u).success(function(result) {
                    $scope.users.push(result);
                }).error(function(error) {
                    toastService.error(errorCreateUser, error);
                });
            });
        };

        $scope.show = function(user) {
            $location.path("/user/user/" + user.id);
        };

        $scope.edit = function (user) {
            var copy = angular.copy(user);
            showUserModalDialog(editUser, copy, function (u) {
                if (u.password && u.password.length > 0) {
                    var parsed = CryptoJS.enc.Utf8.parse(u.password);
                    u.password = CryptoJS.enc.Base64.stringify(parsed);
                }
                var url = String.format("/api/users/{0}/user/{1}", tenantService.getTenant().id, copy.id);
                $http.post(url, u).success(function() {
                    user.firstName = u.firstName;
                    user.lastName = u.lastName;
                    user.email = u.email;
                }).error(function (error) {
                    toastService.error(errorEditUser, error);
                });
            });
        };

        $scope.delete = function (user) {
            dialogService.confirmDialog(deleteUser, String.format(confirmDeleteUser, user.name), function () {
                var url = String.format("/api/users/{0}/user/{1}", tenantService.getTenant().id, user.id);
                $http.delete(url).success(function() {
                    $scope.users = _.reject($scope.users, function(u) { return u.id === user.id; });
                }).error(function(e) {
                    toastService.error(errorDeleteUser, e);
                });
            });
        };

        $scope.makeSysadmin = function (user) {
            var url = String.format("/api/users/{0}/user/{1}/sysadmin", tenantService.getTenant().id, user.id);
            return $http.post(url).success(function () {
                user.isSysadmin = true;
            }).error(function (error) {
                toastService.error(errorMakeSysadmin, error);
            });
        };

        $scope.makeNotSysadmin = function(user) {
            var url = String.format("/api/users/{0}/user/{1}/notsysadmin", tenantService.getTenant().id, user.id);
            return $http.post(url).success(function() {
                user.isSysadmin = false;
            }).error(function(error) {
                toastService.error(errorMakeNotSysadmin, error);
            });
        };

        $scope.lock = function (user) {
            var url = String.format("/api/users/{0}/user/{1}/lock", tenantService.getTenant().id, user.id);
            return $http.post(url).success(function() {
                user.isLocked = true;
            }).error(function(error) {
                toastService.error(errorLockUser, error);
            });
        };

        $scope.unlock = function (user) {
            var url = String.format("/api/users/{0}/user/{1}/unlock", tenantService.getTenant().id, user.id);
            return $http.post(url).success(function () {
                user.isLocked = false;
            }).error(function (error) {
                toastService.error(errorUnlockUser, error);
            });
        };

        $scope.disable2fa = function(user) {
            var url = String.format("/api/2fa/totp/{0}", user.id);
            return $http.delete(url).success(function() {
                user.isTotp = false;
            }).error(function(error) {
                toastService.error(errorRemove2Fa, error);
            });
        };

        $rootScope.$on("tenantChanged", loadUsers);

        $rootScope.$on('logout', function () { $scope.users = []; });

        loadUsers();
    }]);

    angular.module("app").controller("UserController", ["$rootScope", "$scope", "$routeParams", "$http", "tenantService", function ($rootScope, $scope, $routeParams, $http, tenantService) {
        var userId = $routeParams.userId;
        var tenantId = tenantService.getTenant().id;

        $scope.events = [];
        $scope.isBusy = false;
        var isFinish = false;

        var url = String.format("/api/users/{0}/user/{1}", tenantId, userId);
        $http.get(url, { cache: false }).success(function(data) {
            $scope.user = data;
        });

        $scope.nextPage = function () {
            if ($scope.isBusy || isFinish) {
                return;
            }
            $scope.isBusy = true;

            var count = $scope.events.length;
            var url = String.format("/api/events/{1}/user/{2}?$skip={0}&$top=20&$orderby=Id desc", count, tenantId, userId);
            $http.get(url, { cache: false }).success(function (data) {
                if (data.length == 0) {
                    isFinish = true;
                }

                for (var i = 0; i < data.length; i++) {
                    $scope.events.push(data[i]);
                }
                $scope.isBusy = false;
            });
        };
    }]);

    angular.module("app").controller("EditUserController", ["$scope", "$modalInstance", "$log", "$translate", "currentUser", "subject", "user", function ($scope, $modalInstance, $log, $translate, currentUser, subject, user) {
        $scope.user = user;
        $scope.subject = subject;
        $scope.currentUser = currentUser;
        $scope.isEditing = user.id ? true : false;

        var ridiculous = "";
        var veryWeak = "";
        var weak = "";
        var good = "";
        var strong = "";
        var veryStrong = "";

        $translate(["ridiculous", "very_weak", "weak", "good", "strong", "very_strong"]).then(function (t) {
            ridiculous = t.ridiculous;
            veryWeak = t.very_weak;
            weak = t.weak;
            good = t.good;
            strong = t.strong;
            veryStrong = t.very_strong;
        });

        $scope.$watch("user.password", function(newValue) {
            var score = new Score(newValue);
            var entropy = score.calculateEntropyScore() * 1.25;
            entropy = entropy > 100 ? 100 : (entropy < 0 ? 0 : entropy);

            $scope.pwStrength = entropy;
            $scope.pwStrengthColor = "danger";

            if (entropy < 25) {
                $scope.pwStrengthColor = "danger";
            } else if (entropy < 50) {
                $scope.pwStrengthColor = "warning";
            } else {
                $scope.pwStrengthColor = "success";
            }

            if (entropy < 10) {
                $scope.pwStrengthDisplay = ridiculous;
            } else if (entropy < 20) {
                $scope.pwStrengthDisplay = veryWeak;
            } else if (entropy < 30) {
                $scope.pwStrengthDisplay = weak;
            } else if (entropy < 40) {
                $scope.pwStrengthDisplay = good;
            } else if (entropy < 50) {
                $scope.pwStrengthDisplay = strong;
            } else {
                $scope.pwStrengthDisplay = veryStrong;
            }
        });

        $scope.showOrHidePassword = function () {
            if (document.getElementById("password").type == "password") {
                document.getElementById("password").type = "text";
            } else {
                document.getElementById("password").type = "password";
            }
        };

        $scope.generatePassword = function () {
            document.getElementById("password").type = "text";
            $scope.user.password = generatePassword(12, false);
        };

        $scope.ok = function () {
            $modalInstance.close($scope.user);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());
(function() {
    angular.module("app").controller("NavigationController", ["$rootScope", "$scope", "$log", "$http", "tenantService", function ($rootScope, $scope, $log, $http, tenantService) {

        $scope.visibility = {};

        function getNavigation() {
            var tenant = tenantService.getTenant();
            if (!tenant || !tenant.id) {
                return;
            }

            $http.get("/api/navigation/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
                var visibility = angular.fromJson(data);
                $scope.visibility = visibility;
            });
        };

        $rootScope.$on("logout", function () { $scope.navigations = []; });

        $rootScope.$on("tenantChanged", getNavigation);

        getNavigation();
    }]);
}());
(function() {
    angular.module("app").controller("ErrorController", ["$scope", "$routeParams", function ($scope, $routeParams) {
        var id = $routeParams.id;

        $scope.showMessage = false;

        if (id) {
            $scope.id = id;
            $scope.showMessage = true;
        }
    }]);
}());
(function() {
    angular.module("app").controller("LanguageController", ["$rootScope", "$scope", "$translate", "$log", "userService", function($rootScope, $scope, $translate, $log, userService) {

        function setLanguageByUser() {
            var user = userService.currentUser();
            if (user && user.uiLanguage) {
                $scope.setLang(user.uiLanguage);
            }
        };

        $rootScope.$on("currentUserChanged", setLanguageByUser);

        $scope.setLang = function (lang) {
            $translate.use(lang);
            $scope.lang = lang.substring(0, 2).toLowerCase();
        };

        $scope.getFlag = function() {
            switch ($scope.lang) {
                case "en": return "flags-american";
                case "de": return "flags-germany";
                case "fr": return "flags-france";
                case "it": return "flags-italy";
            }
            return "flags-american";
        };

        setLanguageByUser();
    }]);
}());
(function() {
    angular.module("app").controller("StatsController", ["$rootScope", "$scope", "$http", "tenantService", "$log", "currentUser", "$timeout", function ($rootScope, $scope, $http, tenantService, $log, currentUser, $timeout) {
        var timer;
        var gaugeCpu;
        var gaugeDisc;
        var gaugeRam;
        var gaugeCache;

        $scope.monthOptions = {
            responsive: true,
            maintainAspectRatio: true
        };

        function getMetrics(t) {
            timer = $timeout(function() {
                $http.get("/metrics/json", { cache: false }).success(function (data) {
                    if (data.Timers.PageRenderingRequests) {
                        var h = data.Timers.PageRenderingRequests.Histogram;
                        $scope.metricData[0].push(h.Percentile95);
                        $scope.metricData[1].push(h.Percentile75);
                        $scope.metricData[2].push(h.Median);
                        $scope.metricLabels.push(new Date(data.Timestamp).toLocaleTimeString());
                    }
                    if (data.Units.Timers.PageRenderingRequests) {
                        var d = data.Units.Timers.PageRenderingRequests.Duration;
                        $scope.metricSeries = [
                            "95% [" + d + "]",
                            "75% [" + d + "]",
                            "Median [" + d + "]"
                        ];
                    }
                    if (data.Meters && data.Meters.ServedFromCache) {
                        if (!gaugeCache) {
                            gaugeCache = new JustGage({
                                id: "gaugeCache",
                                value: 0,
                                min: 0,
                                max: 100,
                                title: "Cache",
                                label: "%",
                                levelColorsGradient: false
                            });
                        }

                        if (data.Timers && data.Timers.PageRenderingRequests) {
                            var max = data.Timers.PageRenderingRequests.Rate.OneMinuteRate * 100;

                            if (max <= 0) {
                                gaugeCache.refresh(0);
                            } else {
                                var current = data.Meters.ServedFromCache.OneMinuteRate * 100;
                                var percent = ((current / max) * 100).toFixed(0);
                                gaugeCache.refresh(percent);
                            }
                        }
                    }
                    if (data.Gauges) {
                        var cpuUsed = data.Gauges["[System] CPU Usage"];
                        var cpuAlternative = data.Gauges["[Alternative] CPU Usage"];

                        var diskFree = data.Gauges["[System] Physical Disk Free Space"];
                        var diskTotal = data.Gauges["[System] Physical Disk Total Space"];
                        var diskUsed = diskTotal - diskFree;

                        var ramFree = data.Gauges["[System] Available RAM"];
                        var ramTotal = data.Gauges["[System] Total RAM"];
                        var ramUsed = ramTotal - ramFree;

                        if (!gaugeDisc) {
                            gaugeDisc = new JustGage({
                                id: "gaugeDisc",
                                value: 0,
                                min: 0,
                                max: (diskTotal / 1024).toFixed(0),
                                title: "Disc",
                                label: "GB",
                                levelColorsGradient: false
                            });
                        }

                        if (!gaugeCpu) {
                            gaugeCpu = new JustGage({
                                id: "gaugeCpu",
                                value: 0,
                                min: 0,
                                max: 100,
                                title: "CPU",
                                label: "%",
                                levelColorsGradient: false
                            });
                        }

                        if (!gaugeRam) {
                            gaugeRam = new JustGage({
                                id: "gaugeRam",
                                value: 0,
                                min: 0,
                                max: (ramTotal / 1024).toFixed(0),
                                title: "RAM",
                                label: "GB",
                                levelColorsGradient: false
                            });
                        }

                        if ((!cpuUsed || cpuUsed <= 0) && cpuAlternative) {
                            cpuUsed = cpuAlternative;
                        }

                        gaugeDisc.refresh((diskUsed / 1024).toFixed(1));
                        gaugeRam.refresh((ramUsed / 1024).toFixed(1));
                        gaugeCpu.refresh(cpuUsed.toFixed(1));
                    }

                    while ($scope.metricData[0].length > 30) {
                        $scope.metricData[0] = $scope.metricData[0].slice(1);
                        $scope.metricData[1] = $scope.metricData[1].slice(1);
                        $scope.metricData[2] = $scope.metricData[2].slice(1);
                    }
                    while ($scope.metricLabels.length > 30) {
                        $scope.metricLabels = $scope.metricLabels.slice(1);
                    }

                    getMetrics(5000);
                });
            }, t);
        };

        function updateMetrics() {
            if (!currentUser) {
                $scope.showMetrics = false;
                return;
            }

            $scope.showMetrics = currentUser.isSysadmin ? true : false;

            if (currentUser.isSysadmin) {
                $scope.metricData = [[], [], []];
                $scope.metricLabels = [];
                $scope.metricSeries = [
                    "95%",
                    "75%",
                    "median"
                ];

                $scope.options = {
                    animation: false,
                };

                getMetrics(0);
            }
        };

        function getStats() {
            var tenant = tenantService.getTenant();

            $scope.monthData = [];
            $scope.monthLabels = [];
            $scope.monthSeries = [];
            $scope.dayData = [];
            $scope.dayLabels = [];
            $scope.daySeries = [];
            $scope.returningUsers = 0;
            $scope.totalPageViews = 0;
            $scope.totalSessions = 0;
            $scope.viewsPerVisit = 0;

            if (!tenant || !tenant.id) {
                return;
            }

            var url = String.format("/api/stats/{0}", tenant.id);
            $http.get(url, { cache: false }).success(function (data) {
                $scope.data = data;

                var isFirst = true;
                _.each(data, function (pw) {
                    $scope.monthSeries.push(pw.websiteName);
                    $scope.daySeries.push(pw.websiteName);

                    $scope.returningUsers += pw.returningUsers;
                    $scope.totalPageViews += pw.totalPageViews;
                    $scope.totalSessions += pw.totalSessions;
                    $scope.viewsPerVisit += pw.viewsPerVisit;

                    if (isFirst) {
                        isFirst = false;

                        _.each(pw.month, function(m) {
                            $scope.monthLabels.push(m.label);
                        });
                        _.each(pw.day, function(d) {
                            $scope.dayLabels.push(d.label);
                        });
                    }

                    var mv = [];
                    _.each(pw.month, function(m) {
                        mv.push(m.value);
                    });
                    $scope.monthData.push(mv);

                    var dv = [];
                    _.each(pw.day, function(d) {
                        dv.push(d.value);
                    });
                    $scope.dayData.push(dv);

                    var pwPageCountries = {};
                    _.each(pw.pageCountries, function(c) {
                        pwPageCountries[c.label] = c.value;
                    });

                    var pwFileCountries = {};
                    _.each(pw.fileCountries, function(c) {
                        pwFileCountries[c.label] = c.value;
                    });

                    pw.pageMap = {
                        map: "world_mill_en",
                        zoomOnScroll: false,
                        backgroundColor: null,
                        regionStyle: {
                            initial: { fill: "#EEEFF3" }
                        },
                        series: {
                            regions: [{
                                values: pwPageCountries,
                                attribute: 'fill',
                                scale: ['#C8EEFF', '#0071A4'],
                                normalizeFunction: 'polynomial'
                            }]
                        },
                        onRegionTipShow: function (e, el, code) {
                            var count = 0;
                            if (pwPageCountries[code]) {
                                count = pwPageCountries[code];
                            }
                            el.html(el.html() + ': ' + count);
                        }
                    };

                    pw.fileMap = {
                        map: "world_mill_en",
                        zoomOnScroll: false,
                        backgroundColor: null,
                        regionStyle: {
                            initial: { fill: "#EEEFF3" }
                        },
                        series: {
                            regions: [{
                                values: pwFileCountries,
                                scale: ['#C8EEFF', '#0071A4'],
                                normalizeFunction: 'polynomial'
                            }]
                        },
                        onRegionTipShow: function (e, el, code) {
                            var count = 0;
                            if (pwFileCountries[code]) {
                                count = pwFileCountries[code];
                            }
                            el.html(el.html() + ': ' + count);
                        }
                    };
                });
            }).error(function(error) {
                $log.error(error);
            });
        };

        updateMetrics();

        $rootScope.$on("tenantChanged", getStats);

        $rootScope.$on("currentUserChanged", updateMetrics);

        $scope.$on("$destroy", function () {
            if (timer) {
                $timeout.cancel(timer);
            }
        });

        getStats();
    }]);
}());