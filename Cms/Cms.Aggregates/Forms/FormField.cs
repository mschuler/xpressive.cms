namespace Cms.Aggregates.Forms
{
    public class FormField
    {
        public ulong Id { get; set; }

        public string Name { get; set; }

        protected bool Equals(FormField other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }
            if (obj.GetType() != GetType()) { return false; }
            return Equals((FormField)obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}