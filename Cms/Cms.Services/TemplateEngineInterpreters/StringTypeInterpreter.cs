using System;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class StringTypeInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "String Type"; } }

        public void Calculate(TreeItem treeItem)
        {
            if (treeItem.ItemType == TreeItemType.Function &&
                (treeItem.Value.Equals("isstring", StringComparison.OrdinalIgnoreCase) ||
                treeItem.Value.Equals("istext", StringComparison.OrdinalIgnoreCase)) &&
                treeItem.Children != null &&
                treeItem.Children.Count == 1)
            {
                var child = treeItem.Children[0];
                var value = child.Result as string;

                treeItem.Result = (value != null).ToString();
            }
        }
    }
}