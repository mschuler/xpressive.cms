using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Forms.Commands
{
    [RequiredPermission(false)]
    public class CreateFormEntry : CommandBase
    {
        public CreateFormEntry()
        {
            Values = new Dictionary<string, string>();
        }

        [Required]
        [ValidId]
        public ulong FormEntryId { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string IpAddress { get; set; }

        public bool IsSpam { get; set; }

        [Required]
        public Dictionary<string, string> Values { get; set; }

        public bool RunFormActions { get; set; }
    }
}