﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Tenants;
using Cms.SharedKernel;

namespace Cms.Aggregates.DynamicObjects
{
    internal class DynamicObjectDtoService : IDynamicObjectDtoService
    {
        private readonly ITenantRepository _tenantRepository;

        public DynamicObjectDtoService(ITenantRepository tenantRepository)
        {
            _tenantRepository = tenantRepository;
        }

        public object GetDtoWithFields(DynamicObject obj, DynamicObjectDefinition definition, IObjectFieldContentTypeConverterService contentTypeConverter)
        {
            return GetDto(obj, definition, obj.TenantId, FieldSelection.All, contentTypeConverter);
        }

        public object GetDtoWithOnlySortableFields(DynamicObject obj, DynamicObjectDefinition definition, ulong tenantId, IObjectFieldContentTypeConverterService contentTypeConverter)
        {
            return GetDto(obj, definition, tenantId, FieldSelection.OnlySortable, contentTypeConverter);
        }

        public object GetDtoWithoutFields(DynamicObject obj, DynamicObjectDefinition definition)
        {
            return GetDto(obj, definition, obj.TenantId, FieldSelection.None, null);
        }

        public IDynamicObjectDtoForPublicApi GetDtoForPublicApi(DynamicObject obj, DynamicObjectDefinition definition, IObjectFieldContentTypeConverterService contentTypeConverter)
        {
            var fields = new Dictionary<string, object>();
            var metadata = new Dictionary<string, object>();

            foreach (var f in obj.Fields)
            {
                var value = contentTypeConverter.DeserializeForPublicApi(f.ContentType, f.Value);
                fields.Add(f.Name, value);

                object fieldMetadata;
                if (contentTypeConverter.TryGetMetadataForPublicApi(f.ContentType, f.Value, out fieldMetadata))
                {
                    metadata.Add(f.Name, fieldMetadata);
                }
            }

            fields.Add("__metadata", metadata);

            return new DynamicObjectDtoForPublicApi
            {
                Id = obj.Id.ToJsonString(),
                Title = obj.Title,
                Nr = obj.PublicId,
                Fields = fields,
                DefinitionId = obj.DefinitionId.ToJsonString(),
                DefinitionName = definition.Name
            };
        }

        private enum FieldSelection
        {
            None = 0,
            OnlySortable = 1,
            All = 2,
        }

        private object GetDto(DynamicObject obj, DynamicObjectDefinition definition, ulong tenantId, FieldSelection fieldSelection, IObjectFieldContentTypeConverterService contentTypeConverter)
        {
            var fields = new List<object>();
            var so = 100;

            foreach (var f in obj.Fields)
            {
                if (fieldSelection == FieldSelection.None)
                {
                    continue;
                }

                var definitionField = definition.Fields.SingleOrDefault(df => df.Name.Equals(f.Name, StringComparison.OrdinalIgnoreCase));
                int sortOrder;
                var isSortableField = false;

                if (fieldSelection == FieldSelection.OnlySortable)
                {
                    if (definitionField == null || !definitionField.IsSortableField)
                    {
                        continue;
                    }
                }

                if (definitionField != null)
                {
                    sortOrder = definitionField.SortOrder;
                    isSortableField = definitionField.IsSortableField;
                }
                else
                {
                    sortOrder = so;
                    so++;
                }

                var value = contentTypeConverter.Deserialize(f.ContentType, f.Value);
                var sortableFieldSortableValue = string.Empty;
                var sortableFieldVisibleValue = string.Empty;

                if (isSortableField)
                {
                    var fieldValue = contentTypeConverter.DeserializeForSortableField(f.ContentType, f.Value);
                    sortableFieldSortableValue = fieldValue.SortableValue;
                    sortableFieldVisibleValue = fieldValue.VisibleValue;
                }

                fields.Add(new
                {
                    f.Name,
                    f.ContentType,
                    Value = value,
                    SortOrder = sortOrder,
                    IsSortableField = isSortableField,
                    SortableFieldVisibleValue = sortableFieldVisibleValue,
                    SortableFieldSortableValue = sortableFieldSortableValue,
                });
            }

            var tenants = new List<object>();
            foreach (var tid in obj.SharedForTenants)
            {
                Tenant tenant;
                if (_tenantRepository.TryGet(tid, out tenant))
                {
                    tenants.Add(new
                    {
                        Id = tenant.Id.ToJsonString(),
                        tenant.Name
                    });
                }
            }

            return new
            {
                Id = obj.Id.ToJsonString(),
                obj.Title,
                Nr = obj.PublicId,
                Fields = fields,
                Fullname = string.Format("{0} (Nr. {1}) - {2}", obj.Title, obj.PublicId, definition.Name),
                DefinitionId = obj.DefinitionId.ToJsonString(),
                DefinitionName = definition.Name,
                IsShared = tenantId != obj.TenantId,
                Shared = tenants
            };
        }

        private class DynamicObjectDtoForPublicApi : IDynamicObjectDtoForPublicApi
        {
            public string Id { get; set; }
            public string Title { get; set; }
            public int Nr { get; set; }
            public IDictionary<string, object> Fields { get; set; }
            public string DefinitionId { get; set; }
            public string DefinitionName { get; set; }
        }
    }
}