﻿using Autofac;
using Cms.Aggregates.Pages;
using Cms.WebNew.Controllers;

namespace Cms.WebNew
{
    public class ServiceRegistry : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CommandQueue>().As<ICommandQueue>();
            builder.RegisterType<QueryHandler>().As<IQueryHandler>();
            builder.RegisterType<CurrentRequestService>().As<ICurrentRequestService>();

            base.Load(builder);
        }
    }
}