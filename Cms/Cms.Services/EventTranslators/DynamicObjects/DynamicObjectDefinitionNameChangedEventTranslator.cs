using System;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectDefinitionNameChangedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectDefinitionNameChangedEventTranslator(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectDefinitionNameChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Objektdefinition ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectDefinitionNameChanged)@event;
            var definition = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format(
                "Die Objektdefinition <strong>Nr. {0}</strong> wurde in <strong>{1}</strong> umbenannt.",
                definition.PublicId,
                e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objectdefinition";
        }
    }
}