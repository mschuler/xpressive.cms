using System;
using Cms.Aggregates.Tenants.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Tenants
{
    internal class TenantDeletedEventTranslator : IEventTranslator
    {
        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(TenantDeleted);
        }

        public string GetHeader(IEvent @event)
        {
            return "Mandant gel�scht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (TenantDeleted)@event;
            return string.Format("Der Mandant <strong>{0}</strong> wurde gel�scht.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return @event.AggregateId;
        }

        public string GetAffectedModuleId()
        {
            return "tenant";
        }
    }
}