﻿namespace Cms.Aggregates.Websites.Events
{
    public class DomainRemoved : EventBase<Website>
    {
        public string Host { get; set; }
    }
}