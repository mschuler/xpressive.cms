using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.FileDirectories.Commands
{
    [RequiredPermission("file", PermissionLevel.CanChange)]
    public class ChangeFileDirectoryParent : CommandBase
    {
        [Required]
        public ulong ParentId { get; set; }
    }
}