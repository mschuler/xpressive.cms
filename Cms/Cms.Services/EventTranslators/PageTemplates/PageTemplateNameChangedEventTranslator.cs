﻿using System;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.PageTemplates.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.PageTemplates
{
    internal class PageTemplateNameChangedEventTranslator : IEventTranslator
    {
        private readonly IPageTemplateRepository _repository;

        public PageTemplateNameChangedEventTranslator(IPageTemplateRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageTemplateNameChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seitenvorlage umbenannt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageTemplateNameChanged)@event;
            var template = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Die Seitenvorlage <strong>{0}</strong> wurde in <strong>{1}</strong> umbenannt.", template.Name, e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "pagetemplate";
        }
    }
}