using Cms.Aggregates.Security;

namespace Cms.Aggregates.Files.Commands
{
    [RequiredPermission("file", PermissionLevel.CanEverything)]
    public class DeleteFile : CommandBase { }
}