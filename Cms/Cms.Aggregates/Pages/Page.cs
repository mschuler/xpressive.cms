﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.PageTemplates;

namespace Cms.Aggregates.Pages
{
    public class Page : PageBase
    {
        private readonly object _contentLock = new object();
        private readonly IList<PageContent> _contents;

        public Page()
        {
            _contents = new List<PageContent>();
        }

        public int PublicId { get; set; }
        public string Title { get; set; }
        public Guid Permalink { get; set; }
        public ulong PageTemplateId { get; set; }
        public bool IsEnabled { get; set; }

        public override string Fullname
        {
            get
            {
                var fullname = string.Format("{0} (Nr. {1})", Name, PublicId);
                if (!IsEnabled)
                {
                    fullname += " [inaktiv]";
                }
                return fullname;
            }
        }

        public IEnumerable<PageContent> GetPageContent(PageTemplate template, IPageTemplateService templateService)
        {
            if (PageTemplateId != template.Id)
            {
                throw new ArgumentException("Invalid template", "template");
            }

            List<PageContent> content;
            lock (_contentLock)
            {
                content = _contents.ToList();
            }

            var fields = templateService.GetPageFields(template);
            var toRemove = content
                .Where(c => !fields.Any(f => f.Name.Equals(c.Name, StringComparison.OrdinalIgnoreCase)))
                .ToList();

            lock (_contentLock)
            {
                foreach (var pageContent in toRemove)
                {
                    _contents.Remove(pageContent);
                    content.Remove(pageContent);
                }
            }

            return content;
        }

        public void Add(PageContent content)
        {
            lock (_contentLock)
            {
                foreach (var pageContent in _contents)
                {
                    if (pageContent.Name.Equals(content.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        pageContent.Value = content.Value;
                        return;
                    }
                }

                _contents.Add(content);
            }
        }

        public void Remove(PageContent content)
        {
            lock (_contentLock)
            {
                _contents.Remove(content);
            }
        }
    }
}
