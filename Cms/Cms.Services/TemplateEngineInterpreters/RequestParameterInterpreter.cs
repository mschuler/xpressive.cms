﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class RequestParameterInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name
        {
            get { return "RequestParameter"; }
        }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function &&
                tree.Value.Equals("param", StringComparison.OrdinalIgnoreCase) &&
                tree.Children.Count == 1 &&
                tree.Children[0].ParameterType == ParameterType.String)
            {
                var name = (string)tree.Children[0].Result;
                var collection = TemplateStateMap.Get<NameValueCollection>("QueryString");
                var key = collection.AllKeys.SingleOrDefault(k => k.Equals(name, StringComparison.OrdinalIgnoreCase));

                if (key != null)
                {
                    var result = collection[key];
                    int i;
                    if (int.TryParse(result, NumberStyles.Integer, CultureInfo.InvariantCulture, out i))
                    {
                        tree.Result = i;
                    }
                    else
                    {
                        tree.Result = result;
                    }
                }
            }
        }
    }
}
