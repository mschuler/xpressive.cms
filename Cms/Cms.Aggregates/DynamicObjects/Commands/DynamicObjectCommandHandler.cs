using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.Aggregates.Tenants;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    internal class DynamicObjectCommandHandler : CommandHandlerBase
    {
        private readonly IDynamicObjectRepository _repository;
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;
        private readonly ITenantRepository _tenantRepository;

        public DynamicObjectCommandHandler(IDynamicObjectRepository repository, IDynamicObjectDefinitionRepository definitionRepository, ITenantRepository tenantRepository)
        {
            _repository = repository;
            _definitionRepository = definitionRepository;
            _tenantRepository = tenantRepository;
        }

        public void When(CreateDynamicObject command, Action<IEvent> eventHandler)
        {
            var definition = _definitionRepository.Get(command.DefinitionId);
            var values = new Dictionary<string, string>(command.Values.Count);
            var title = string.Empty;

            foreach (var pair in command.Values)
            {
                values[pair.Key] = pair.Value.RemoveControlChars();
            }

            foreach (var field in definition.Fields)
            {
                if (field.SortOrder == 0)
                {
                    title = values[field.Name];
                }

                if (!values.ContainsKey(field.Name))
                {
                    throw new ExceptionWithReason("There is no value provided for field " + field.Name);
                }
            }

            if (definition.TenantId != command.TenantId)
            {
                if (definition.SharedForTenants == null || !definition.SharedForTenants.Contains(command.TenantId))
                {
                    throw new ExceptionWithReason("This tenant is not allowed to create objects for this definition.");
                }
            }

            eventHandler(new DynamicObjectCreated
            {
                AggregateId = command.AggregateId,
                TenantId = command.TenantId,
                DefinitionId = command.DefinitionId,
                PublicId = command.PublicId,
                Title = title,
                Values = values,
                UserId = command.UserId,
            });
        }

        public void When(ChangeDynamicObject command, Action<IEvent> eventHandler)
        {
            var o = _repository.Get(command.AggregateId);
            var values = new Dictionary<string, string>(command.Values.Count);

            foreach (var pair in command.Values)
            {
                values[pair.Key] = pair.Value.RemoveControlChars();
            }

            foreach (var field in o.Fields)
            {
                if (!values.ContainsKey(field.Name))
                {
                    throw new ExceptionWithReason("There is no value provided for field " + field.Name);
                }
            }

            foreach (var pair in values)
            {
                if (!o.Fields.Any(f => f.Name.Equals(pair.Key, StringComparison.OrdinalIgnoreCase)))
                {
                    throw new ExceptionWithReason("There is no field named " + pair.Key);
                }
            }

            eventHandler(new DynamicObjectChanged
            {
                AggregateId = command.AggregateId,
                Values = values,
                UserId = command.UserId,
            });
        }

        public void When(ShareDynamicObject command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            foreach (var tenantId in command.TenantIds)
            {
                _tenantRepository.Get(tenantId);
            }

            var e = new DynamicObjectShared
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TenantIds = command.TenantIds.ToList(),
            };

            eventHandler(e);
        }

        public void When(DeleteDynamicObject command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            eventHandler(new DynamicObjectDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            });
        }
    }
}