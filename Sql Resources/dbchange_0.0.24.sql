
begin transaction

alter table MediaContent add Guid uniqueidentifier not null default newid();

insert into Version (Major, Minor, Revision) values (0, 0, 24);

commit transaction

