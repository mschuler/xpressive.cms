﻿using System.Collections.Generic;

namespace Cms.EventSourcing.Contracts.Events
{
    public interface IEventStore
    {
        void Persist(IEnumerable<IEvent> events);

        IList<IEvent> Fetch();

        IEnumerable<IEvent> Fetch(int skip, int take);

        IEnumerable<IEvent> Fetch(ulong aggregateId);

        IEnumerable<ulong> GetAggregateIds<T>();
    }
}