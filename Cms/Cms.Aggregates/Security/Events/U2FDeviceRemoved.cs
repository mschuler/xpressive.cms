﻿namespace Cms.Aggregates.Security.Events
{
    public class U2FDeviceRemoved : EventBase<User>
    {
        public string PublicKey { get; set; }
    }
}