﻿using System;
using System.Globalization;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectTemplateCreatedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectTemplateCreatedEventTranslator(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectTemplateCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Objektvorlage erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectTemplateCreated)@event;
            var d = _repository.GetEvenIfDeleted(@event.AggregateId);
            return string.Format(
                CultureInfo.InvariantCulture,
                "Der Objektdefinition <strong>{0} (Nr. {1})</strong> wurde die Objektvorlage <strong>{2}</strong> hinzugefügt.",
                d.Name,
                d.PublicId,
                e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objecttemplate";
        }
    }
}
