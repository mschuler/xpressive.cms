﻿namespace Cms.Aggregates.Tenants.Events
{
    public class TenantCreated : EventBase<Tenant>
    {
        public string Name { get; set; }
    }
}