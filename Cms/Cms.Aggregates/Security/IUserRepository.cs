﻿namespace Cms.Aggregates.Security
{
    public interface IUserRepository : IRepository<User>
    {
        User GetByEmailAddress(string emailAddress);

        User GetByUsernameAndPassword(string emailAddress, string password);
    }
}