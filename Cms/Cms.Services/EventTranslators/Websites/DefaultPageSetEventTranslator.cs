﻿using System;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Websites;
using Cms.Aggregates.Websites.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Websites
{
    internal class DefaultPageSetEventTranslator : IEventTranslator
    {
        private readonly IWebsiteRepository _websiteRepository;
        private readonly IPageRepository _pageRepository;

        public DefaultPageSetEventTranslator(IWebsiteRepository websiteRepository, IPageRepository pageRepository)
        {
            _websiteRepository = websiteRepository;
            _pageRepository = pageRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DefaultPageSet);
        }

        public string GetHeader(IEvent @event)
        {
            return "Standardseite geändert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DefaultPageSet)@event;
            var website = _websiteRepository.GetEvenIfDeleted(e.AggregateId).Name;
            var page = _pageRepository.GetEvenIfDeleted(e.PageId);
            return string.Format("Die Standardseite der Website <strong>{0}</strong> wurde nach <strong>{1}</strong> geändert.", website, page.Fullname);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _websiteRepository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "website";
        }
    }
}