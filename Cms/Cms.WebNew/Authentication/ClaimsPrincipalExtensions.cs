using System;
using System.Security.Claims;
using System.Security.Principal;
using Cms.SharedKernel;

namespace Cms.WebNew.Authentication
{
    public static class ClaimsPrincipalExtensions
    {
        public static ulong GetId(this IPrincipal principal)
        {
            return principal.GetId(true);
        }

        public static ulong GetId(this IPrincipal principal, bool throwException)
        {
            var claimsPrincipal = (ClaimsPrincipal)principal;
            return GetId(claimsPrincipal, throwException);
        }

        public static ulong GetId(this ClaimsPrincipal principal, bool throwException)
        {
            var idClaim = principal.FindFirst(ClaimTypes.Sid);
            if (idClaim == null)
            {
                if (throwException)
                {
                    throw new InvalidOperationException("There is no SID claim defined.").WithGuid();
                }
                return 0;
            }
            return Convert.ToUInt64(idClaim.Value);
        }

        public static ulong GetId(this ClaimsPrincipal principal)
        {
            return GetId(principal, true);
        }
    }
}