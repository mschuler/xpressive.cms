using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using log4net;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class ValidateDomain : IFilter<RenderPageMessage>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ValidateDomain));

        public bool Execute(RenderPageMessage msg)
        {
            var host = msg.Request.RequestUri.Host;
            msg.Domain = msg.Website.Domains.FirstOrDefault(d => d.Host.Equals(host, StringComparison.OrdinalIgnoreCase));

            if (msg.Domain == null)
            {
                _log.WarnFormat("Domain {0} not found.", host);
                msg.Response = new HttpResponseMessage(HttpStatusCode.NotFound);
                return false;
            }

            return true;
        }
    }
}