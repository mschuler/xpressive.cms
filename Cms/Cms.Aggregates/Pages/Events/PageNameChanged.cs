namespace Cms.Aggregates.Pages.Events
{
    public class PageNameChanged : EventBase<PageBase>
    {
        public string Name { get; set; }
    }
}