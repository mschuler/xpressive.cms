﻿namespace Cms.Aggregates.Tenants.Queries
{
    [RequiredPermission(false)]
    public class GetTenants : QueryBase { }
}