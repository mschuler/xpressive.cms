﻿(function() {
    angular.module("app").controller("ObjectsDefinitionsController", ["$scope", "$rootScope", "$http", "$location", "tenantService", "$translate", "dialogService", function ($scope, $rootScope, $http, $location, tenantService, $translate, dialogService) {
        $scope.definitions = [];

        $scope.title = "";

        $translate(["Objects"]).then(function (t) {
            $scope.title = t.Objects;
        });

        $rootScope.$on("logout", function() {
            $scope.definitions = [];
        });

        $scope.select = function (node) {
            $location.path("/objects/objects/" + node.id);
        };

        var getDefinitions = function () {
            var tenantId = tenantService.getTenant().id;
            $scope.definitions = [];

            $http.get("/api/objectdefinition/" + tenantId, { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = false;
                    $scope.definitions.push(d);
                });
            });

            $http.get("/api/objectdefinition/" + tenantId + "/shared", { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    d.isShared = true;
                    $scope.definitions.push(d);
                });
            });
        };

        $rootScope.$on("tenantChanged", getDefinitions);

        getDefinitions();
    }]);

    angular.module("app").controller("ObjectsController", ["$rootScope", "$scope", "$routeParams", "$http", "$log", "$location", "$modal", "dialogService", "toastService", "tenantService", "$translate", "localStorageService", function ($rootScope, $scope, $routeParams, $http, $log, $location, $modal, dialogService, toastService, tenantService, $translate, localStorageService) {
        var definitionId = $routeParams.definitionId;
        $scope.selectedObject = {};
        $scope.sortableFields = [];
        $scope.isShared = false;
        var firstTime = true;

        function load() {
            var tenantId = tenantService.getTenant().id;
            var filters = loadFiltersFromCookie();

            $http.get("/api/permission/object/" + tenantId, { cache: false }).success(function (permission) {
                $scope.canEdit = permission.canEdit;
                $scope.canAdmin = permission.canAdmin;
            });

            var getObjectsUrl = String.format("/api/objects/{0}/{1}", tenantId, definitionId);

            if (filters && filters.length > 0) {
                var f = [];

                _.each(filters, function(i) {
                    f.push({
                        fieldName: i.fieldName,
                        operator: i.operator,
                        value: i.value,
                    });
                });

                var filterJson = angular.toJson(f);
                filterJson = CryptoJS.enc.Utf8.parse(filterJson);
                filterJson = CryptoJS.enc.Base64.stringify(filterJson);
                filterJson = filterJson.replace(/\//g, '_');
                getObjectsUrl += "?filter=" + filterJson;
            }

            $http.get(getObjectsUrl, { cache: false }).success(function (data) {
                $scope.objects = data;

                if (data.length > 0 && firstTime) {
                    firstTime = false;
                    _.each(data[0].fields, function (f) {
                        if (f.isSortableField) {
                            $scope.sortableFields.push(f);
                        }
                    });
                }

                if (data.length > 0) {
                    _.each(data, function (o) {
                        o.sortableFields = {};
                        _.each(o.fields, function (f) {
                            if (f.isSortableField) {
                                o.sortableFields["f" + f.sortOrder] = {
                                    view: f.sortableFieldVisibleValue,
                                    sort: f.sortableFieldSortableValue
                                };
                            }
                        });
                    });
                }
            });

            $http.get("/api/objectdefinition/" + tenantId + "/shared", { cache: false }).success(function (data) {
                _.each(data, function (d) {
                    if (d.id === definitionId) {
                        $scope.isShared = true;
                    }
                });
            });
        };

        $rootScope.$on("tenantChanged", load);

        load();

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id + "/" + definitionId + "/detail", { cache: false }).success(function (data) {
            $scope.definition = data;
            $scope.definitionName = data.fullname;
        });

        $scope.select = function(o) {
            $scope.selectedObject = o;
        };

        var settings = localStorageService.get("settings");
        var settingsKey = String.format("objects.{0}.sortOrder", definitionId);

        if (settings) {
            if (settings[settingsKey]) {
                $scope.sortField = settings[settingsKey];
            } else {
                $scope.sortField = "title";
            }
        } else {
            settings = {};
            $scope.sortField = "title";
        }

        $scope.setSortOrder = function (fieldName) {
            $scope.sortField = fieldName;
            settings[settingsKey] = fieldName;
            localStorageService.set("settings", settings);

            $log.debug("sortField=" + fieldName);
        }

        var editObject = "";
        var errorEditObject = "";
        var createObject = "";
        var errorCreateObject = "";
        var deleteObject = "";
        var confirmDeleteObject = "";
        var errorDeleteObject = "";

        $translate(["edit_object", "error_edit_object", "create_object", "error_create_object", "delete_object", "confirm_delete_object", "error_delete_object"]).then(function(t) {
            editObject = t.edit_object;
            errorEditObject = t.error_edit_object;
            createObject = t.create_object;
            errorCreateObject = t.error_create_object;
            deleteObject = t.delete_object;
            confirmDeleteObject = t.confirm_delete_object;
            errorDeleteObject = t.error_delete_object;
        });

        function openEditDialog(subject, entity, onSave) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/views/objects/editObjectDialog.min.html",
                controller: "EditObjectDialogController",
                size: "lg",
                resolve: {
                    subject: function () { return subject; },
                    entry: function () { return entity; },
                    definitionId: function () { return definitionId; }
                }
            }).result.then(function (result) {
                onSave(result);
            });
        };

        $scope.edit = function (o) {
            var url = String.format("/api/object/{0}", o.id);
            $http.get(url, { cache: false }).success(function (entry) {

                _.each(entry.fields, function (ef) {
                    var df = _.find($scope.definition.fields, function (edf) { return edf.name === ef.name; });
                    if (df) {
                        ef.contentTypeOptions = df.contentTypeOptions;

                        if (ef.contentTypeOptions) {
                            ef.contentTypeOptions = ef.contentTypeOptions.split("\n");
                            ef.contentTypeOptions.splice(0, 0, "");
                        } else {
                            ef.contentTypeOptions = [];
                        }
                    }
                });

                openEditDialog(editObject, entry, function(result) {
                    url = String.format("/api/object/{0}/{1}", tenantService.getTenant().id, o.id);
                    $http.post(url, result).success(function () {
                        var titleField = _.find(result.fields, function (f) { return f.sortOrder === 0; });
                        o.title = titleField.value;
                    }).error(function(error) {
                        toastService.error(errorEditObject, error);
                    });
                });
            });
        };

        $scope.duplicate = function(o) {
            var url = String.format("/api/object/{0}", o.id);
            $http.get(url, { cache: false }).success(function (entry) {

                _.each(entry.fields, function (ef) {
                    var df = _.find($scope.definition.fields, function (edf) { return edf.name === ef.name; });
                    if (df) {
                        ef.contentTypeOptions = df.contentTypeOptions;

                        if (ef.contentTypeOptions) {
                            ef.contentTypeOptions = ef.contentTypeOptions.split("\n");
                        } else {
                            ef.contentTypeOptions = [];
                        }
                    }
                });

                openEditDialog(createObject, entry, function (result) {
                    url = String.format("/api/objects/{0}/{1}", tenantService.getTenant().id, definitionId);
                    $http.post(url, result).success(function (no) {
                        var titleField = _.find(result.fields, function (f) { return f.sortOrder === 0; });
                        no.title = titleField.value;
                        $scope.objects.push(no);
                    }).error(function (error) {
                        toastService.error(errorEditObject, error);
                    });
                });
            });
        };

        $scope.delete = function (o) {
            var fullname = String.format("{0} (Nr. {1})", o.title, o.nr);
            dialogService.confirmDialog(deleteObject, String.format(confirmDeleteObject, fullname), function() {
                var url = String.format("/api/object/{0}/{1}", tenantService.getTenant().id, o.id);
                $http.delete(url).success(function () {
                    $scope.objects = _.reject($scope.objects, function (obj) { return obj.id === o.id; });
                }).error(function (error) {
                    toastService.error(errorDeleteObject, error);
                });
            });
        };

        $scope.create = function() {
            var entry = {
                title: "",
                fields: []
            };

            _.each($scope.definition.fields, function(f) {
                entry.fields.push({
                    name: f.name,
                    value: "",
                    contentType: f.contentType,
                    contentTypeOptions: f.contentTypeOptions ? f.contentTypeOptions.split("\n") : [],
                    sortOrder: f.sortOrder
                });
            });

            openEditDialog(createObject, entry, function (result) {
                var tenantId = tenantService.getTenant().id;
                var url = String.format("/api/objects/{0}/{1}", tenantId, definitionId);
                $http.post(url, result).success(function (o) {
                    var titleField = _.find(result.fields, function(f) { return f.sortOrder === 0; });
                    o.title = titleField.value;
                    $scope.objects.push(o);
                }).error(function(error) {
                    toastService.error(errorCreateObject, error);
                });
            });
        };

        $scope.share = function (o) {
            var existingTenantIds = [];

            if (o.shared) {
                existingTenantIds = _.map(o.shared, function (t) { return t.id; });
            }

            dialogService.selectTenantsDialog(existingTenantIds, function (tenants) {
                var ids = _.map(tenants, function (t) {
                    return t.id;
                });
                var dto = {
                    tenantIds: ids
                };
                var url = String.format("/api/object/{0}/share", o.id);
                $http.put(url, dto).success(function () {
                    o.shared = tenants;
                });
            });
        };

        $scope.donotshare = function (o) {
            var url = String.format("/api/object/{0}/share", o.id);
            var dto = {
                tenantIds: []
            };
            return $http.put(url, dto).success(function () {
                o.shared = [];
            });
        };

        $scope.getNames = function (tenants) {
            var names = "";
            _.each(tenants, function (t) {
                names += t.name + ", ";
            });
            names = names.trim();
            return names.substring(0, names.length - 1);
        };

        $scope.import = function () {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/views/objects/import.min.html",
                controller: "ObjectImportDialogController",
                size: "lg",
                resolve: {
                    definition: function () { return $scope.definition; }
                }
            });
        };

        $scope.addFilter = function () {
            var fields = [];

            _.each($scope.definition.fields, function (f) {
                fields.push({
                    name: f.name,
                    contentType: f.contentType,
                    contentTypeOptions: f.contentTypeOptions ? f.contentTypeOptions.split("\n") : [],
                    sortOrder: f.sortOrder
                });
            });

            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/views/objects/addFilterDialog.min.html",
                controller: "AddObjectFilterDialogController",
                resolve: {
                    fields: function () { return fields; },
                }
            }).result.then(function (result) {
                addFilter(result);
            });
        };

        $scope.removeFilter = function(filter) {
            var filters = loadFiltersFromCookie();

            filters = _.reject(filters, function(f) {
                return f.fieldName === filter.fieldName &&
                    f.operator === filter.operator &&
                    f.value === filter.value;
            });

            var storageKey = String.format("objects.{0}.filters", definitionId);
            localStorageService.set(storageKey, filters);
            $scope.filters = filters;
            load();
        };

        function addFilter(filter) {
            var filters = loadFiltersFromCookie();
            filters.push(filter);

            var storageKey = String.format("objects.{0}.filters", definitionId);
            localStorageService.set(storageKey, filters);
            $scope.filters = filters;
            load();
        };

        function loadFiltersFromCookie() {
            var storageKey = String.format("objects.{0}.filters", definitionId);
            var filters = localStorageService.get(storageKey);

            if (!filters) {
                filters = [];
            }

            $scope.filters = filters;
            return filters;
        };
    }]);

    angular.module("app").controller("AddObjectFilterDialogController", ["$modalInstance", "$scope", "$log", "fields", function ($modalInstance, $scope, $log, fields) {
        $scope.fields = fields;
        $scope.field = {};
        $scope.operations = [];
        $scope.filterType = "";

        $scope.$watch("field", function() {
            if (!$scope.field || !$scope.field.contentType) {
                $scope.filterType = "";
                return;
            }

            $scope.filter.fieldName = $scope.field.name;
            $scope.filter.displayValue = "";
            $scope.filter.value = "";
            $scope.filter.operator = "=";
            $scope.operations = [];

            if ($scope.field.contentType.toUpperCase() === "TEXT" ||
                $scope.field.contentType.toUpperCase() === "CODE" ||
                $scope.field.contentType.toUpperCase() === "MARKDOWN" ||
                $scope.field.contentType.toUpperCase() === "HTML") {
                $scope.operations = ["=", "contains"];
                $scope.filterType = "text";
            } else if ($scope.field.contentType.toUpperCase() === "DATE") {
                $scope.operations = [">=", ">", "=", "<", "<="];
                $scope.filterType = "date";
            } else if ($scope.field.contentType.toUpperCase() === "LIST") {
                $scope.operations = ["="];
                $scope.filterType = "list";
            } else if ($scope.field.contentType.toUpperCase() === "NUMBER") {
                $scope.operations = [">=", ">", "=", "<", "<="];
                $scope.filterType = "number";
            } else if ($scope.field.contentType.toUpperCase() === "BOOLEAN") {
                $scope.operations = ["="];
                $scope.filterType = "boolean";
            } else {
                $scope.filter.operator = "";
                $scope.filterType = "invalid";
            }
        });

        $scope.filter = {
            fieldName: "",
            operator: "",
            value: "",
            displayName: ""
        };

        $scope.openStartDate = function (e, field) {
            e.preventDefault();
            e.stopPropagation();
            field.isDateOpened = true;
        };

        $scope.times = [];

        for (var i = 0; i < 24; i++) {
            var hour = i + ":";
            if (hour.length < 3) {
                hour = "0" + hour;
            }

            for (var m = 0; m < 60; m += 5) {
                var minute = m;
                if (m < 10) {
                    minute = "0" + minute;
                }

                $scope.times.push(hour + minute);
            }
        }

        $scope.dateOptions = {
            formatYear: "yy",
            startingDay: 1
        };

        $scope.$watch("filter.dateValue", function() {
            updateDateTimeValue();
        });

        $scope.$watch("filter.timeValue", function() {
            updateDateTimeValue();
        });

        function updateDateTimeValue() {
            var tpatt = /[0-9]{1,2}:[0-9]{2}/i;
            var dpatt = /[0-9]{1,2}.[0-9]{1,2}.[0-9]{4}/i;
            var v = "";
            var d = "";

            if ($scope.filter.dateValue) {
                v += moment($scope.filter.dateValue).format("YYYY-MM-DD");
                d += moment($scope.filter.dateValue).format("DD.MM.YYYY");

                if ($scope.filter.timeValue && tpatt.test($scope.filter.timeValue)) {
                    v += "T" + $scope.filter.timeValue + ":00";
                    d += $scope.filter.timeValue;
                } else {
                    v += "T00:00:00";
                }
            }

            $scope.filter.value = v;
            $scope.filter.displayValue = d;
        };

        $scope.ok = function () {
            var v = $scope.filter.value;

            if ($scope.filter.displayValue) {
                v = $scope.filter.displayValue;
            }

            if (!$scope.filter.value || !$scope.filter.operator) {
                $modalInstance.dismiss("cancel");
                return;
            }

            $modalInstance.close({
                fieldName: $scope.filter.fieldName,
                operator: $scope.filter.operator,
                value: $scope.filter.value,
                displayName: $scope.filter.fieldName + " " + $scope.filter.operator + " " + v
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("ObjectImportDialogController", ["$modalInstance", "$scope", "$http", "tenantService", "definition", function ($modalInstance, $scope, $http, tenantService, definition) {
        $scope.isImportEnabled = false;
        $scope.isUploadEnabled = false;
        $scope.currentStep = 0;
        var id = 0;

        $scope.options = { delimiter: ",", mapping:[] };
        $scope.definition = definition;

        _.each(definition.fields, function (f) {
            $scope.options.mapping.push({
                column: "",
                objectField: f.name,
                sortOrder: f.sortOrder,
                format: "",
                contentType: f.contentType
            });
        });

        $http.get("/api/objects/import/start", { cache: false }).success(function (data) {
            id = data.id;
        });

        $http.get("/api/objects/import/encodings").success(function (data) {
            $scope.encodings = data;
            $scope.options.encoding = "utf-8";
        });

        $scope.$watch("csvFile", function () {
            if (!$scope.csvFile) {
                return;
            }
            $scope.csvFileName = $scope.csvFile.name;
            $scope.isUploadEnabled = true;
        });

        $scope.goToStep = function (step) {
            if ($scope.currentStep < step) {
                return;
            }
            $scope.currentStep = step;
        };

        $scope.upload = function () {
            if (!$scope.csvFile) {
                return;
            }

            var fd = new FormData();
            fd.append("file", $scope.csvFile);
            $http.post("/api/objects/import/" + id + "/file", fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            }).success(function () {
                $http.get("/api/objects/import/" + id + "/encoding", { cache: false }).success(function (e) {
                    $scope.options.encoding = e;
                });

                $scope.currentStep = 1;
            });
        };

        $scope.goToMapping = function () {
            var url = String.format("/api/objects/import/{0}/columns", id);
            $http.post(url, $scope.options).success(function (data) {
                $scope.columns = data;
                $scope.currentStep = 2;
            });
        };

        $scope.goToPreview = function () {
            var url = String.format("/api/objects/{0}/import/{1}/preview", definition.id, id);
            $http.post(url, $scope.options).success(function (data) {
                $scope.preview = data;
                $scope.currentStep = 3;
                $scope.previewHeaders = [];

                if (data.length > 0) {
                    _.each(data[0].cells, function(c) {
                        $scope.previewHeaders.push(c.header);
                    });
                }
            });
        };

        $scope.import = function () {
            var tenantId = tenantService.getTenant().id;
            var url = String.format("/api/objects/{0}/{1}/import/{2}/import", tenantId, definition.id, id);
            $http.post(url, $scope.options).success(function () {
                $modalInstance.close();
            });
        };

        $scope.back = function () {
            if ($scope.currentStep > 0) {
                $scope.currentStep--;
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("ObjectController", [function () { }]);

    angular.module("app").controller("EditObjectDialogController", ["$modalInstance", "$scope", "$http", "$log", "dialogService", "tenantService", "subject", "entry", "definitionId", function ($modalInstance, $scope, $http, $log, dialogService, tenantService, subject, entry, definitionId) {
        $scope.entry = entry;
        $scope.subject = subject;
        $scope.templates = [];
        $scope.selectedTemplate = { id: 0, name: "" };
        $scope.preview = "";
        $scope.objectIsValid = true;

        $http.get("/api/objecttemplates/" + tenantService.getTenant().id + "/" + definitionId, { cache: false }).success(function (templates) {
            $scope.templates = templates;
            $scope.templates.push($scope.selectedTemplate);
        });

        function updatePreview() {
            if (!$scope.selectedTemplate || !$scope.selectedTemplate.id) {
                return;
            }

            $http.post("/api/object/template/" + $scope.selectedTemplate.id, entry).success(function (html) {
                $scope.preview = html;
            });
        };

        $scope.$watch("entry", function () {
            updatePreview();
        }, true);

        $scope.$watch("selectedTemplate", function () {
            updatePreview();
        });

        $scope.tinymceOptions = {
            theme: "modern",
            relative_urls: true,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code hr wordcount textcolor colorpicker",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media"
        };

        $scope.codemirrorOptions = {
            lineNumbers: true,
            theme: "neo",
            lineWrapping: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            matchTags: { bothTags: true },
            autoCloseTags: true,
            styleActiveLine: true,
            viewportMargin: Infinity,
            mode: "htmlmixed"
        };

        $scope.getObjects = function(value) {
            var url = String.format("/api/object/search/{0}/{1}", tenantService.getTenant().id, value);
            return $http.get(url, { cache: false }).then(function(response) {
                return response.data;
            });
        };

        $scope.browseObject = function(field) {
            dialogService.selectObjectDialog(field.value, function(o) {
                if (!o) {
                    return;
                }
                field.value = o;
            });
        };

        $scope.getFiles = function (value) {
            var url = String.format("/api/file/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, {cache:false}).then(function (response) {
                return response.data;
            });
        };

        $scope.browseFile = function (field) {
            dialogService.selectFileDialog(field.value, function (file) {
                if (!file) {
                    return;
                }
                field.value = file;
            });
        };

        $scope.getPages = function (value) {
            var url = String.format("/api/page/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, { cache: false }).then(function (response) {
                return response.data;
            });
        };

        $scope.browsePage = function (field) {
            dialogService.selectPageDialog(field.value, function (page) {
                if (!page) {
                    return;
                }
                page.children = [];
                field.value = page;
            });
        };

        $scope.browsePageLink = function (field) {
            var pl = field.value && field.value.pageLink ? field.value.pageLink : {};
            dialogService.selectPageDialog(pl, function (page) {
                if (!page) {
                    return;
                }
                page.children = [];
                if (!field.value) {
                    field.value = {};
                }
                field.value.pageLink = page;
            });
        };

        $scope.getForms = function(value) {
            var url = String.format("/api/form/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, { cache: false }).then(function(response) {
                return response.data;
            });
        };

        $scope.browseForm = function(field) {
            dialogService.selectFormDialog(field.value, function(form) {
                if (!form) {
                    return;
                }
                field.value = form;
            });
        };

        $scope.openStartDate = function (e, field) {
            e.preventDefault();
            e.stopPropagation();
            field.isDateOpened = true;
        };

        $scope.times = [];

        for (var i = 0; i < 24; i++) {
            var hour = i + ":";
            if (hour.length < 3) {
                hour = "0" + hour;
            }

            for (var m = 0; m < 60; m += 5) {
                var minute = m;
                if (m < 10) {
                    minute = "0" + minute;
                }

                $scope.times.push(hour + minute);
            }
        }

        $scope.dateOptions = {
            formatYear: "yy",
            startingDay: 1
        };

        function updateDateTimeValue(field) {
            var tpatt = /[0-9]{1,2}:[0-9]{2}/i;
            var dpatt = /[0-9]{1,2}.[0-9]{1,2}.[0-9]{4}/i;
            var v = "";

            if (field.dateValue) {
                if (dpatt.test(field.dateValue)) {
                    v += field.dateValue;
                } else {
                    v += moment(field.dateValue).format("DD.MM.YYYY");
                }

                if (field.timeValue && tpatt.test(field.timeValue)) {
                    v += " " + field.timeValue;
                }
            }

            field.value = v;
        };

        _.each(entry.fields, function(f, i) {
            if (f.contentType.toUpperCase() === "DATE") {
                if (f.value) {
                    var patt = /[0-9]{1,2}.[0-9]{1,2}.[0-9]{4}\s[0-9]{1,2}:[0-9]{1,2}/i;
                    if (patt.test(f.value)) {
                        var parts = f.value.split(" ");
                        var dateParts = parts[0].split(".");
                        var timeParts = parts[1].split(":");
                        var day = Number(dateParts[0]);
                        var month = Number(dateParts[1]);
                        var year = Number(dateParts[2]);
                        var hours = Number(timeParts[0]);
                        var minutes = Number(timeParts[1]);

                        month = month < 10 ? "0" + month : month;
                        day = day < 10 ? "0" + day : day;
                        hours = hours < 10 ? "0" + hours : hours;
                        minutes = minutes < 10 ? "0" + minutes : minutes;

                        f.dateValue = day + "." + month + "." + year;
                        f.timeValue = hours + ":" + minutes;
                    }
                }

                $scope.$watch("entry.fields[" + i + "].dateValue", function() {
                    updateDateTimeValue(f);
                });

                $scope.$watch("entry.fields[" + i + "].timeValue", function() {
                    updateDateTimeValue(f);
                });

                updateDateTimeValue(f);
            };
        });

        $scope.ok = function () {
            var isValid = true;
            _.each(entry.fields, function (f) {
                f.isValid = true;
                if (f.contentType.toUpperCase() === "DATE") {
                    if (!f.value) {
                        f.isValid = false;
                        isValid = false;
                    }
                }
                if (f.sortOrder === 0) {
                    if (!f.value) {
                        f.isValid = false;
                        isValid = false;
                    }
                }
            });

            $scope.objectIsValid = isValid;
            if (isValid) {
                $modalInstance.close($scope.entry);
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);
}());