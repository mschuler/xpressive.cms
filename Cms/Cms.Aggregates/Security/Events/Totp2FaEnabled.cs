﻿namespace Cms.Aggregates.Security.Events
{
    public class Totp2FaEnabled : EventBase<User>
    {
        public string TotpSecret { get; set; }
    }
}