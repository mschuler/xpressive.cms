using System;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class UserDeletedEventTranslator : IEventTranslator
    {
        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(UserDeleted);
        }

        public string GetHeader(IEvent @event)
        {
            return "Benutzer gel�scht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (UserDeleted)@event;
            return string.Format("Der Benutzer <strong>{0}</strong> wurde gel�scht.", e.FullName);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}