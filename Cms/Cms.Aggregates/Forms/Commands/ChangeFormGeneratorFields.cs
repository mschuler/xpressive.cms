﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Commands
{
    [RequiredPermission("form", PermissionLevel.CanChange)]
    public class ChangeFormGeneratorFields : CommandBase
    {
        public ChangeFormGeneratorFields()
        {
            Fields = new List<GeneratorField>();
        }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string LabelPosition { get; set; }

        [Required]
        public IList<GeneratorField> Fields { get; set; }
    }
}