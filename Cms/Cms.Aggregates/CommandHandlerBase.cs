﻿using System;
using Cms.EventSourcing;
using Cms.EventSourcing.Contracts.Commands;
using Cms.EventSourcing.Contracts.Events;

namespace Cms.Aggregates
{
    public abstract class CommandHandlerBase : ICommandHandler
    {
        public void Handle(ICommand command, Action<IEvent> eventHandler)
        {
            RedirectToWhen.InvokeCommandHandler(this, command, eventHandler);
        }
    }
}