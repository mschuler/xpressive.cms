namespace Cms.EventSourcing
{
    using Autofac;

    using Cms.EventSourcing.Contracts.Events;
    using Cms.EventSourcing.Services;

    public class ServiceRegistry : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SqlEventStore>().As<IEventStore>().SingleInstance();
            builder.RegisterType<JsonEventSerializer>().As<IEventSerializer>();

            base.Load(builder);
        }
    }
}