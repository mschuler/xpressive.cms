using System;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.Websites;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Pages
{
    internal class PageTemplateChangedEventTranslator : IEventTranslator
    {
        private readonly IPageRepository _repository;
        private readonly IPageTemplateRepository _pageTemplateRepository;
        private readonly IWebsiteRepository _websiteRepository;

        public PageTemplateChangedEventTranslator(IPageRepository repository, IPageTemplateRepository pageTemplateRepository, IWebsiteRepository websiteRepository)
        {
            _repository = repository;
            _pageTemplateRepository = pageTemplateRepository;
            _websiteRepository = websiteRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageTemplateChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Vorlage von Seite ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageTemplateChanged)@event;
            var p = (Page)_repository.GetEvenIfDeleted(e.AggregateId);
            var t = _pageTemplateRepository.GetEvenIfDeleted(e.PageTemplateId);
            return string.Format("Die Vorlage der Seite <strong>{0}</strong> wurde nach <strong>{1}</strong> ge�ndert.", p.Fullname, t.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var page = _repository.GetEvenIfDeleted(@event.AggregateId);
            return _websiteRepository.GetEvenIfDeleted(page.WebsiteId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "page";
        }
    }
}