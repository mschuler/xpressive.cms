﻿namespace Cms.Aggregates.FileDirectories.Events
{
    public class FileDirectoryDeleted : EventBase<FileDirectory>
    {
        public string Name { get; set; }
    }
}