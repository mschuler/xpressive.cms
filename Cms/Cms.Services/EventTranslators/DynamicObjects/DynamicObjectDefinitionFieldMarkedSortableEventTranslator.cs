using System;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectDefinitionFieldMarkedSortableEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectDefinitionFieldMarkedSortableEventTranslator(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectDefinitionFieldMarkedSortable);
        }

        public string GetHeader(IEvent @event)
        {
            var e = (DynamicObjectDefinitionFieldMarkedSortable)@event;
            return e.IsSortable ? "Objektdefinitionsfeld als sortierbar markiert" : "Objektdefinitionsfeld als nicht sortierbar markiert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectDefinitionFieldMarkedSortable)@event;
            var msg = e.IsSortable
                ? "Das Objektdefinitionsfeld <strong>{0}</strong> wurde als sortierbar markiert."
                : "Das Objektdefinitionsfeld <strong>{0}</strong> wurde als nicht sortierbar markiert.";
            return string.Format(msg, e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objectdefinition";
        }
    }
}