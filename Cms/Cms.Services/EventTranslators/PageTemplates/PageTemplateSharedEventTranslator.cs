using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.PageTemplates.Events;
using Cms.Aggregates.Tenants;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.PageTemplates
{
    internal class PageTemplateSharedEventTranslator : IEventTranslator
    {
        private readonly IPageTemplateRepository _repository;
        private readonly ITenantRepository _tenantRepository;

        public PageTemplateSharedEventTranslator(IPageTemplateRepository repository, ITenantRepository tenantRepository)
        {
            _repository = repository;
            _tenantRepository = tenantRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageTemplateShared);
        }

        public string GetHeader(IEvent @event)
        {
            var e = (PageTemplateShared)@event;
            return e.TenantIds.Any() ? "Seitenvorlage freigegeben" : "Seitenvorlage Freigabe aufgehoben";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageTemplateShared)@event;
            var template = _repository.GetEvenIfDeleted(e.AggregateId);
            var tenants = new List<string>();

            foreach (var tenantId in e.TenantIds)
            {
                Tenant tenant;
                if (_tenantRepository.TryGetEvenIfDeleted(tenantId, out tenant))
                {
                    tenants.Add(tenant.Name);
                }
            }

            var tenantList = string.Join(", ", tenants);

            if (e.TenantIds.Any())
            {
                return string.Format("Die Seitenvorlage <strong>{0}</strong> wurde f�r die Mandanten <strong>{1}</strong> freigegeben.", template.Name, tenantList);
            }

            return string.Format("Die Freigabe f�r die Seitenvorlage <strong>{0}</strong> wurde aufgehoben.", template.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "pagetemplate";
        }
    }
}