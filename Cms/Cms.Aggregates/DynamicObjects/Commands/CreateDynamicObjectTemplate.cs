﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("objecttemplate", PermissionLevel.CanChange)]
    public class CreateDynamicObjectTemplate : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }

        [ValidId]
        public ulong TemplateId { get; set; }
    }
}