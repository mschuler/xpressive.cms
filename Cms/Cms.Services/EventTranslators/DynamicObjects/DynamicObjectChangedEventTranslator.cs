using System;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectChangedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectRepository _repository;
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;

        public DynamicObjectChangedEventTranslator(IDynamicObjectRepository repository, IDynamicObjectDefinitionRepository definitionRepository)
        {
            _repository = repository;
            _definitionRepository = definitionRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Objekt ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var o = _repository.GetEvenIfDeleted(@event.AggregateId);
            return string.Format("Das Objekt <strong>{0} (Nr. {1})</strong> wurde ge�ndert.", o.Title, o.PublicId);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var o = _repository.GetEvenIfDeleted(@event.AggregateId);
            return _definitionRepository.GetEvenIfDeleted(o.DefinitionId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "object";
        }
    }
}