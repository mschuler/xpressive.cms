﻿using System.Collections.Generic;
using Cms.Aggregates.Settings;
using Cms.Services.Contracts;
using log4net;

namespace Cms.Services.UrlShortening
{
    internal class UrlShorteningServiceProvider : IUrlShorteningServiceProvider
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(UrlShorteningServiceProvider));

        private readonly List<IUrlShorteningService> _services = new List<IUrlShorteningService>();
        private readonly ISettingsRepository _settingsRepository;

        public UrlShorteningServiceProvider(IEnumerable<IUrlShorteningService> services, ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
            _services.AddRange(services);
        }

        public IEnumerable<IUrlShorteningService> GetServices()
        {
            return _services.AsReadOnly();
        }

        public IUrlShorteningService GetSelectedService(ulong tenantId)
        {
            var settings = _settingsRepository.GetSettings(tenantId).UrlShorteningSettings;

            if (settings == null)
            {
                _log.Warn("There is no url shortening service configured.");
                return null;
            }

            var selectedService = settings.ServiceName;

            if (string.IsNullOrEmpty(selectedService))
            {
                _log.Warn("There is no url shortening service configured.");
                return null;
            }

            foreach (var service in _services)
            {
                if (service.Name.Equals(selectedService))
                {
                    foreach (var tuple in settings.Configuration)
                    {
                        service.SetConfigurationValue(tuple.Item1, tuple.Item2);
                    }

                    return service;
                }
            }

            _log.Warn("No url shortening service with name " + selectedService + " found.");
            return null;
        }
    }
}
