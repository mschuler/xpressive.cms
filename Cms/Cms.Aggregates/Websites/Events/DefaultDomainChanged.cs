﻿namespace Cms.Aggregates.Websites.Events
{
    public class DefaultDomainChanged : EventBase<Website>
    {
        public string Host { get; set; }
    }
}