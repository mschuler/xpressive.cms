﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Cms.Aggregates.Security;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using log4net;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;

namespace Cms.WebNew.Authentication
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(SimpleAuthorizationServerProvider));
        private static readonly ConcurrentDictionary<string, List<DateTime>> _invalidLoginAttempts = new ConcurrentDictionary<string, List<DateTime>>(StringComparer.OrdinalIgnoreCase);
        private readonly IUserRepository _userRepository;
        private readonly ITotpService _totpService;
        private readonly IU2FService _u2FService;

        public SimpleAuthorizationServerProvider(IUserRepository userRepository, ITotpService totpService, IU2FService u2FService)
        {
            _userRepository = userRepository;
            _totpService = totpService;
            _u2FService = u2FService;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId;
            string clientSecret;

            if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var password = context.Password;
            var passwordBytes = Convert.FromBase64String(password);
            password = Encoding.UTF8.GetString(passwordBytes);

            var user = _userRepository.GetByUsernameAndPassword(context.UserName, password);
            List<DateTime> attempts;

            if (user == null || user.IsLocked || User.System.Equals(user) || User.Guest.Equals(user))
            {
                if (_invalidLoginAttempts.TryGetValue(context.UserName, out attempts))
                {
                    var oneHourAgo = DateTime.Now - TimeSpan.FromHours(1);
                    attempts.RemoveAll(a => a < oneHourAgo);
                    var count = attempts.Count;
                    var secondsToSleep = 0.5 * count * count;

                    _logger.WarnFormat(
                        "Login for user {0} failed. {1} failed logins attempts within the last hour. Delay for {2} seconds. (IP Address = {3})",
                        context.UserName,
                        count,
                        secondsToSleep,
                        context.OwinContext.Request.RemoteIpAddress);

                    await Task.Delay(TimeSpan.FromSeconds(secondsToSleep));
                }

                _invalidLoginAttempts.AddOrUpdate(
                    context.UserName,
                    s => new List<DateTime> { DateTime.Now },
                    (s, list) => new List<DateTime>(list) { DateTime.Now });

                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            var form = await context.Request.ReadFormAsync();

            if (!IsValidTotp(form, user))
            {
                context.SetError("totp_enabled");
                return;
            }

            if (!IsValidU2F(form, user))
            {
                SetU2FChallenge(context, user);
                return;
            }

            _logger.InfoFormat(
                "Login for user {0} successful. (IP Address = {1})",
                context.UserName,
                context.OwinContext.Request.RemoteIpAddress);

            _invalidLoginAttempts.TryRemove(context.UserName, out attempts);

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, string.Format("{0} {1}", user.FirstName, user.LastName).Trim()));
            identity.AddClaim(new Claim(ClaimTypes.Email, user.EmailAddress));
            identity.AddClaim(new Claim(ClaimTypes.Sid, user.Id.ToString("D")));

            var props = new AuthenticationProperties(new Dictionary<string, string>
            {
                {
                    "as:client_id", context.ClientId ?? string.Empty
                },
                {
                    "userName", context.UserName
                }
            });

            var ticket = new AuthenticationTicket(identity, props);
            context.Validated(ticket);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
            var currentClient = context.ClientId;

            if (originalClient != currentClient)
            {
                context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
                return Task.FromResult<object>(null);
            }

            // Change auth ticket for refresh token requests
            var identity = new ClaimsIdentity(context.Ticket.Identity);
            var ticket = new AuthenticationTicket(identity, context.Ticket.Properties);
            context.Validated(ticket);

            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        private bool IsValidTotp(IFormCollection form, User user)
        {
            if (!user.IsTotpEnabled)
            {
                return true;
            }

            if (form == null)
            {
                return false;
            }

            var totp = form.GetValues("totp");

            if (totp == null || totp.Count != 1 || string.IsNullOrEmpty(totp[0]) || !_totpService.Verify(user.GetTotpSecret(), totp[0]))
            {
                return false;
            }

            return true;
        }

        private bool IsValidU2F(IFormCollection form, User user)
        {
            if (!user.IsU2FEnabled)
            {
                return true;
            }

            if (form == null)
            {
                return false;
            }

            var u2FJson = form.GetValues("u2f");

            if (u2FJson == null || u2FJson.Count != 1 || string.IsNullOrEmpty(u2FJson[0]) || !_u2FService.TryFinishVerification(user, u2FJson[0]))
            {
                return false;
            }

            return true;
        }

        private void SetU2FChallenge(OAuthGrantResourceOwnerCredentialsContext context, User user)
        {
            var uri = context.Request.Uri;
            var appId = U2FController.GetAppId(uri.Scheme, uri.Host, uri.Port);
            var verification = _u2FService.StartVerification(user, appId);
            var json = JsonConvert.SerializeObject(verification);
            context.SetError("u2f_enabled", json);
        }
    }
}