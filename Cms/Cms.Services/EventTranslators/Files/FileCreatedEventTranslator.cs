using System;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.Files;
using Cms.Aggregates.Files.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Files
{
    internal class FileCreatedEventTranslator : IEventTranslator
    {
        private readonly IFileRepository _repository;
        private readonly IRepository<FileDirectory> _fileDirectoryRepository;

        public FileCreatedEventTranslator(IFileRepository repository, IRepository<FileDirectory> fileDirectoryRepository)
        {
            _repository = repository;
            _fileDirectoryRepository = fileDirectoryRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FileCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Neue Datei erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FileCreated)@event;
            return string.Format("Die Datei <strong>{0} (Nr. {1})</strong> wurde neu erstellt.", e.Name, e.PublicId);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var file = _repository.GetEvenIfDeleted(@event.AggregateId);
            FileDirectory directory;
            if (_fileDirectoryRepository.TryGetEvenIfDeleted(file.FileDirectoryId, out directory))
            {
                return directory.TenantId;
            }
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "file";
        }
    }
}