using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Calendars;
using Cms.Aggregates.Calendars.Commands;
using Cms.Aggregates.Security;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using CsvHelper;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class AppointmentImportController : CsvImportControllerBase
    {
        private readonly IUniqueIdProvider _idProvider;

        public AppointmentImportController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IUniqueIdProvider idProvider, IUtf8Checker utf8Checker, ITemporaryFileStoreService temporaryFileStoreService)
            : base(commandQueue, userRepository, queryHandler, utf8Checker, temporaryFileStoreService)
        {
            _idProvider = idProvider;
        }

        [HttpGet, Route("api/appointment/import/start")]
        public override object StartImport()
        {
            return base.StartImport();
        }

        [HttpGet, Route("api/appointment/import/encodings")]
        public override IEnumerable<object> GetEncodings()
        {
            return base.GetEncodings();
        }

        [HttpGet, Route("api/appointment/import/{id:ulong}/encoding")]
        public override string GetEncoding(ulong id)
        {
            return base.GetEncoding(id);
        }

        [HttpPost, Route("api/appointment/import/{id:ulong}/file")]
        public override Task UploadFile(ulong id)
        {
            return base.UploadFile(id);
        }

        [HttpPost, Route("api/appointment/import/{id:ulong}/columns")]
        public IEnumerable<object> GetColumns(ulong id, [FromBody]CsvSettingsDto settings)
        {
            return base.GetColumns(id, settings.Delimiter, settings.HasHeaderRecord, settings.GetEncoding());
        }

        [HttpPost, Route("api/appointment/import/{id:ulong}/preview")]
        public IEnumerable<object> GetPreview(ulong id, [FromBody]CsvSettingsDto settings)
        {
            FileInfo file;
            if (!TryGetTemporaryFile(id, out file) || !file.Exists)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (settings == null || settings.Mapping == null)
            {
                yield break;
            }

            using (var textReader = new StreamReader(file.FullName, settings.GetEncoding()))
            {
                using (var csvReader = new CsvReader(textReader))
                {
                    csvReader.Configuration.Delimiter = settings.Delimiter;
                    csvReader.Configuration.HasHeaderRecord = settings.HasHeaderRecord;

                    while (csvReader.Read())
                    {
                        var title = GetField(csvReader, settings.Mapping.Title, settings.HasHeaderRecord);
                        var location = GetField(csvReader, settings.Mapping.Location, settings.HasHeaderRecord);
                        var start = settings.GetStart(GetField(csvReader, settings.Mapping.Start, settings.HasHeaderRecord));
                        var end = settings.GetEnd(GetField(csvReader, settings.Mapping.End, settings.HasHeaderRecord));

                        yield return new
                        {
                            Title = title,
                            Location = location,
                            Start = start,
                            End = end,
                            IsValid = !string.IsNullOrEmpty(title) && start.HasValue && end.HasValue
                        };
                    }
                }
            }
        }

        [HttpPost, Route("api/appointment/import/{tenantId:ulong}/{id:ulong}/import")]
        public void Import(ulong tenantId, ulong id, [FromBody]CsvSettingsDto settings)
        {
            FileInfo file;
            if (!TryGetTemporaryFile(id, out file) || !file.Exists)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (settings == null || settings.Mapping == null)
            {
                return;
            }

            using (var textReader = new StreamReader(file.FullName, settings.GetEncoding()))
            {
                using (var csvReader = new CsvReader(textReader))
                {
                    csvReader.Configuration.Delimiter = settings.Delimiter;
                    csvReader.Configuration.HasHeaderRecord = settings.HasHeaderRecord;

                    while (csvReader.Read())
                    {
                        var title = GetField(csvReader, settings.Mapping.Title, settings.HasHeaderRecord);
                        var location = GetField(csvReader, settings.Mapping.Location, settings.HasHeaderRecord);
                        var start = settings.GetStart(GetField(csvReader, settings.Mapping.Start, settings.HasHeaderRecord));
                        var end = settings.GetEnd(GetField(csvReader, settings.Mapping.End, settings.HasHeaderRecord));

                        if (start.HasValue && end.HasValue && !string.IsNullOrEmpty(title))
                        {
                            SendCommand(new AddAppointment
                            {
                                AggregateId = tenantId,
                                TenantId = tenantId,
                                AppointmentId = _idProvider.GetId(),
                                Title = title,
                                Location = location,
                                StartEnd = new DateTimeRange(start.Value, end.Value),
                                Recurrence = new SingleOccurrence(),
                            });
                        }
                    }
                }
            }
        }

        public class CsvSettingsDto
        {
            private string _delimiter;

            public string Delimiter
            {
                get { return _delimiter; }
                set
                {
                    _delimiter = value;
                    if (value == "\\t")
                    {
                        _delimiter = "\t";
                    }
                }
            }

            public bool HasHeaderRecord { get; set; }

            public string Encoding { get; set; }

            public CsvMappingDto Mapping { get; set; }
        }

        public class CsvMappingDto
        {
            public string Start { get; set; }
            public string StartFormat { get; set; }
            public string End { get; set; }
            public string EndFormat { get; set; }
            public string Title { get; set; }
            public string Location { get; set; }
        }
    }
}