using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.Files;
using Cms.Aggregates.Files.Commands;
using Cms.Aggregates.Files.Queries;
using Cms.Aggregates.Security;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class FileController : ApiControllerBase
    {
        private readonly IRepository<FileDirectory> _fileDirectoryRepository;
        private readonly IFileRepository _repository;

        public FileController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IRepository<FileDirectory> fileDirectoryRepository, IFileRepository repository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _fileDirectoryRepository = fileDirectoryRepository;
            _repository = repository;
        }

        [HttpGet, Route("api/file/{directoryId:ulong}")]
        public IEnumerable<object> GetAll(ulong directoryId)
        {
            var tenantId = _fileDirectoryRepository.Get(directoryId).TenantId;
            return ExecuteQuery(new GetAllFiles
            {
                DirectoryId = directoryId,
                TenantId = tenantId,
            });
        }

        [HttpGet, Route("api/files/{tenantId:ulong}/{directoryId:ulong}/shared")]
        public IEnumerable<object> GetForSharedDirectory(ulong tenantId, ulong directoryId)
        {
            return ExecuteQuery(new GetAllFiles
            {
                DirectoryId = directoryId,
                TenantId = tenantId,
            });
        }

        [HttpGet, Route("api/file/search/{tenantId:ulong}")]
        public IEnumerable<object> Search(ulong tenantId, [FromUri]string search)
        {
            return ExecuteQuery(new GetFilesBySearchPattern
            {
                SearchPattern = search,
                TenantId = tenantId,
            });
        }

        [HttpGet, Route("api/file/{fileId:ulong}/detail")]
        public object Get(ulong fileId)
        {
            File file;
            if (!_repository.TryGet(fileId, out file))
            {
                return new NotFoundResult(this);
            }

            var directoryId = file.FileDirectoryId;

            if (directoryId == 0)
            {
                return null;
            }

            var tenantId = _fileDirectoryRepository.Get(directoryId).TenantId;
            return ExecuteQuery(new GetFile
            {
                FileId = fileId,
                TenantId = tenantId,
            }).SingleOrDefault();
        }

        [HttpGet, Route("api/file/{tenantId:ulong}/trash")]
        public IEnumerable<object> GetTrash(ulong tenantId)
        {
            return ExecuteQuery(new GetFilesWithoutDirectory
            {
                TenantId = tenantId
            });
        }

        [HttpPut, Route("api/file/{fileId:ulong}/rename")]
        public void Rename(ulong fileId, [FromBody]NameDto dto)
        {
            var directoryId = _repository.Get(fileId).FileDirectoryId;
            var tenantId = _fileDirectoryRepository.Get(directoryId).TenantId;
            SendCommand(new ChangeFileName
            {
                AggregateId = fileId,
                Name = dto.Name,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/file/{fileId:ulong}/track")]
        public void Track(ulong fileId)
        {
            var directoryId = _repository.Get(fileId).FileDirectoryId;
            var tenantId = _fileDirectoryRepository.Get(directoryId).TenantId;

            SendCommand(new EnableFileTracking
            {
                AggregateId = fileId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/file/{fileId:ulong}/untrack")]
        public void Untrack(ulong fileId)
        {
            var directoryId = _repository.Get(fileId).FileDirectoryId;
            var tenantId = _fileDirectoryRepository.Get(directoryId).TenantId;

            SendCommand(new DisableFileTracking
            {
                AggregateId = fileId,
                TenantId = tenantId,
            });
        }

        [HttpPut, Route("api/file/{fileId:ulong}/move/{directoryId:ulong}")]
        public void Move(ulong fileId, ulong directoryId)
        {
            var tenantId = _fileDirectoryRepository.Get(directoryId).TenantId;
            SendCommand(new ChangeFileDirectory
            {
                AggregateId = fileId,
                FileDirectoryId = directoryId,
                TenantId = tenantId,
            });
        }

        [HttpDelete, Route("api/file/{fileId:ulong}")]
        public void Delete(ulong fileId)
        {
            var directoryId = _repository.Get(fileId).FileDirectoryId;
            var tenantId = _fileDirectoryRepository.Get(directoryId).TenantId;
            SendCommand(new DeleteFile
            {
                AggregateId = fileId,
                TenantId = tenantId,
            });
        }

        public class NameDto
        {
            public string Name { get; set; }
        }
    }
}