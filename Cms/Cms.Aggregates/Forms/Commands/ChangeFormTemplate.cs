using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Commands
{
    [RequiredPermission("form", PermissionLevel.CanChange)]
    public class ChangeFormTemplate : CommandBase
    {
        [Required(AllowEmptyStrings = true)]
        public string Template { get; set; }
    }
}