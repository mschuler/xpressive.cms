using System.Linq;
using Cms.Aggregates.Pages;
using Xunit;

namespace Cms.Aggregates.Tests
{
    public class Given_a_template_with_fields
    {
        private readonly PageTemplateService _pageTemplateService = new PageTemplateService();

        [Fact]
        public void When_there_are_two_fields_within_one_single_line()
        {
            var template = "[ { 'name': 'Feld 1', 'type': 'HTML', 'sortOrder': 1 }, { 'name': 'Feld 2', 'type': 'Text', 'sortOrder': 2 } ]";
            var result = _pageTemplateService.Parse(template);

            Assert.NotNull(result.Item2);
            var fields = result.Item2.ToList();
            Assert.Equal(2, fields.Count);
            Assert.Equal("", result.Item1);
        }

        [Fact]
        public void When_there_are_two_fields_within_multiple_lines()
        {
            var template = @"
[
    {
        'name': 'Feld 1',
        'type': 'HTML',
        'sortOrder': 1
    },
    {
        'name': 'Feld 2',
        'type': 'Text',
        'sortOrder': 2
    }
]
<p>
    { $('Feld 1') }
</p>
";

            var result = _pageTemplateService.Parse(template);

            Assert.NotNull(result.Item2);
            var fields = result.Item2.ToList();
            Assert.Equal(2, fields.Count);
            Assert.Equal(@"<p>
    { $('Feld 1') }
</p>", result.Item1);

            var html = _pageTemplateService.ReplaceField(result.Item1, "Feld 1", "Hello world!");
            Assert.Equal(@"<p>
    Hello world!
</p>", html);
        }

        [Fact]
        public void When_there_are_no_fields()
        {
            var template = @"<p>
    Hello world!
</p>";

            var result = _pageTemplateService.Parse(template);
            Assert.NotNull(result.Item2);
            var fields = result.Item2.ToList();
            Assert.Equal(0, fields.Count);
            Assert.Equal(template, result.Item1);
        }
    }
}