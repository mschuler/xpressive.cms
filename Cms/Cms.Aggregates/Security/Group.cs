﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Security
{
    public class Group : AggregateBase
    {
        private readonly ConcurrentDictionary<string, Permission> _permissions;
        private readonly object _memberLock = new object();
        private readonly List<User> _members;

        public Group()
        {
            _permissions = new ConcurrentDictionary<string, Permission>(StringComparer.OrdinalIgnoreCase);
            _members = new List<User>();
        }

        public ulong TenantId { get; set; }

        public string Name { get; set; }

        public IEnumerable<Permission> Permissions { get { return _permissions.Values.ToList(); } }

        public IEnumerable<User> Members
        {
            get
            {
                lock (_memberLock)
                {
                    return _members.ToList();
                }
            }
        }

        internal void AddMember(User user)
        {
            lock (_memberLock)
            {
                if (_members.Contains(user))
                {
                    return;
                }
                _members.Add(user);
            }
        }

        internal void RemoveMember(ulong userId)
        {
            lock (_memberLock)
            {
                _members.RemoveAll(u => u.Id == userId);
            }
        }

        internal void RemoveMember(User user)
        {
            lock (_memberLock)
            {
                _members.Remove(user);
            }
        }

        internal void SetPermission(string moduleId, PermissionLevel permissionLevel)
        {
            _permissions.AddOrUpdate(
                moduleId,
                (m) => new Permission(moduleId, permissionLevel),
                (m, p) => new Permission(moduleId, permissionLevel));
        }

        public bool HasPermission(string moduleId, PermissionLevel permissionLevel)
        {
            Permission permission;
            if (_permissions.TryGetValue(moduleId, out permission))
            {
                return permission.PermissionLevel >= permissionLevel;
            }
            return false;
        }

        public bool TryGetPermission(string moduleId, out PermissionLevel permissionLevel)
        {
            Permission permission;
            if (_permissions.TryGetValue(moduleId, out permission))
            {
                permissionLevel = permission.PermissionLevel;
                return true;
            }
            permissionLevel = PermissionLevel.Forbidden;
            return false;
        }
    }
}