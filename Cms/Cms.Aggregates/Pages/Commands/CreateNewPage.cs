using System;
using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Pages.Commands
{
    [RequiredPermission("page", PermissionLevel.CanChange)]
    public class CreateNewPage : CommandBase
    {
        [Required, ValidId]
        public ulong WebsiteId { get; set; }

        [Range(1, int.MaxValue)]
        public int PublicId { get; set; }

        public ulong ParentPageId { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string Title { get; set; }

        public Guid Permalink { get; set; }
    }
}