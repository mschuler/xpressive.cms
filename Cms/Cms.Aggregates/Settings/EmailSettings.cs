namespace Cms.Aggregates.Settings
{
    public class EmailSettings
    {
        public EmailSettings() { }

        public EmailSettings(
            string emailAddress,
            string responseAddress,
            string host,
            int port,
            bool isSmtpAuthenticationRequired,
            bool isSslUsedForAuthentication,
            string smtpUsername,
            string smtpPassword)
        {
            SmtpPassword = smtpPassword;
            SmtpUsername = smtpUsername;
            IsSslUsedForAuthentication = isSslUsedForAuthentication;
            IsSmtpAuthenticationRequired = isSmtpAuthenticationRequired;
            Port = port;
            Host = host;
            ResponseAddress = responseAddress;
            EmailAddress = emailAddress;
        }


        public string Host { get; private set; }
        public int Port { get; private set; }
        public string EmailAddress { get; private set; }
        public string ResponseAddress { get; private set; }
        public bool IsSmtpAuthenticationRequired { get; private set; }
        public bool IsSslUsedForAuthentication { get; private set; }
        public string SmtpUsername { get; private set; }
        public string SmtpPassword { get; private set; }
    }
}