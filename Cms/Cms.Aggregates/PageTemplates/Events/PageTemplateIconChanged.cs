﻿namespace Cms.Aggregates.PageTemplates.Events
{
    public class PageTemplateIconChanged : EventBase<PageTemplate>
    {
        public ulong IconFileId { get; set; }
    }
}