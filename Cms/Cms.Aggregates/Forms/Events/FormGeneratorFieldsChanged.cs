﻿using System.Collections.Generic;

namespace Cms.Aggregates.Forms.Events
{
    public class FormGeneratorFieldsChanged : EventBase<Form>
    {
        public string LabelPosition { get; set; }

        public IList<GeneratorField> Fields { get; set; }
    }
}