﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Settings.Commands
{
    [RequiredPermission("sysadmin", PermissionLevel.CanEverything)]
    public class ChangeEmailSettings : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Host { get; set; }

        [Required, Range(1, 65535)]
        public int Port { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(5)]
        public string EmailAddress { get; set; }

        public string ResponseAddress { get; set; }

        public bool IsSmtpAuthenticationRequired { get; set; }

        public bool IsSslUsedForAuthentication { get; set; }

        public string SmtpUsername { get; set; }

        public string SmtpPassword { get; set; }
    }
}
