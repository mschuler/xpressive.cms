﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cms.Aggregates.Security;
using Cms.EventSourcing.Contracts.Commands;
using Cms.SharedKernel;
using log4net;

namespace Cms.WebNew.Controllers
{
    public class CommandQueue : ICommandQueue
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(CommandQueue));
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;

        public CommandQueue(IUserRepository userRepository, IGroupRepository groupRepository)
        {
            _userRepository = userRepository;
            _groupRepository = groupRepository;
        }

        public void Put(ICommand command, HttpRequestMessage request)
        {
            var user = _userRepository.Get(command.UserId);
            var type = command.GetType();
            if (user.IsLocked || !CheckUserRequirementsForRequiredPermissionAttribute.IsAllowedToExecute(type, user, command.TenantId, _groupRepository))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized).WithGuid();
            }

            ThrowExceptionIfRequestIsNull(request);
            ThrowExceptionIfReferrerIsNotTheHost(request);

            LogCommand(command, user, request);

            LocalEventStore.Command(command);
        }

        private void LogCommand(ICommand command, User user, HttpRequestMessage request)
        {
            _logger.InfoFormat(
                "Execute command {0} for aggregate {4:D} from user {1} with ip address {2} and referrer {3} and agent {5}",
                command.GetType().Name,
                user.FullName,
                GetIpAddress(request),
                GetReferrer(request),
                command.AggregateId,
                request.Headers.UserAgent);
        }

        [Conditional("RELEASE")]
        private void ThrowExceptionIfRequestIsNull(HttpRequestMessage request)
        {
            if (request == null)
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized).WithGuid();
            }
        }

        private void ThrowExceptionIfReferrerIsNotTheHost(HttpRequestMessage request)
        {
            if (request == null)
            {
                return;
            }

            var referrer = GetReferrer(request);
            var host = request.RequestUri.Authority;

            if (string.IsNullOrEmpty(referrer))
            {
                return;
            }

            if (!string.Equals(referrer, host, StringComparison.OrdinalIgnoreCase))
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden).WithGuid();
            }
        }

        private string GetReferrer(HttpRequestMessage request)
        {
            if (request == null)
            {
                return null;
            }
            return request.GetReferrer();
        }

        private string GetIpAddress(HttpRequestMessage request)
        {
            if (request == null)
            {
                return null;
            }
            return request.GetClientIpAddress();
        }
    }
}