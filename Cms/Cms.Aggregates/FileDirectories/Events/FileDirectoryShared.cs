﻿using System.Collections.Generic;

namespace Cms.Aggregates.FileDirectories.Events
{
    public class FileDirectoryShared : EventBase<FileDirectory>
    {
        public IList<ulong> TenantIds { get; set; }
    }
}