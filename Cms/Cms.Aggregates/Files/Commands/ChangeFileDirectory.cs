﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Files.Commands
{
    [RequiredPermission("file", PermissionLevel.CanChange)]
    public class ChangeFileDirectory : CommandBase
    {
        [Required, ValidId]
        public ulong FileDirectoryId { get; set; }
    }
}