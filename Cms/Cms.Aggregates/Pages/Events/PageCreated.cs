using System;

namespace Cms.Aggregates.Pages.Events
{
    public class PageCreated : EventBase<PageBase>
    {
        public ulong WebsiteId { get; set; }
        public int PublicId { get; set; }
        public ulong ParentPageId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public Guid Permalink { get; set; }
    }
}