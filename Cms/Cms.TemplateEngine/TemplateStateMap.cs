﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Messaging;

namespace Cms.TemplateEngine
{
    public static class TemplateStateMap
    {
        public const string CurrentObjectKey = "CurrentObject";
        public const string CurrentLoadedPage = "CurrentLoadedPage";

        public static CultureInfo CurrentCulture
        {
            get { return GetInternal<CultureInfo>("TemplateStateMap.Own.CurrentCulture"); }
        }

        public static IDisposable SetCurrentCulture(CultureInfo cultureInfo)
        {
            return SetInternal("TemplateStateMap.Own.CurrentCulture", cultureInfo);
        }

        public static T Get<T>(string key)
        {
            return GetInternal<T>("TemplateStateMap." + key);
        }

        public static IDisposable Set<T>(string key, T value)
        {
            return SetInternal("TemplateStateMap." + key, value);
        }

        public static T GetInternal<T>(string key)
        {
            var list = GetOrCreateStack(key);
            return (T)list.FirstOrDefault(i => i is T);
        }

        private static IDisposable SetInternal(string key, object value)
        {
            var list = GetOrCreateStack(key);
            list.AddFirst(value);
            return new StateMapPopDisposable(list);
        }

        private static LinkedList<object> GetOrCreateStack(string key)
        {
            var list = CallContext.LogicalGetData(key) as LinkedList<object>;
            if (list == null)
            {
                list = new LinkedList<object>();
                CallContext.LogicalSetData(key, list);
            }
            return list;
        }

        private class StateMapPopDisposable : IDisposable
        {
            private readonly LinkedList<object> _list;
            private bool _disposed;

            public StateMapPopDisposable(LinkedList<object> list)
            {
                _list = list;
            }

            public void Dispose()
            {
                if (_disposed)
                {
                    return;
                }
                _disposed = true;

                _list.RemoveFirst();
            }
        }
    }
}
