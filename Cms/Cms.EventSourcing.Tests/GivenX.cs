﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Cms.EventSourcing.Contracts.Commands;
using Cms.EventSourcing.Contracts.Events;
using Moq;
using Xunit;

namespace Cms.EventSourcing.Tests
{
    public class GivenX
    {
        [Fact]
        public void Test1()
        {
            var eventStore = new Mock<IEventStore>();
            var command = new Mock<ICommand>();

            eventStore.Setup(es => es.Persist(It.IsAny<IEnumerable<IEvent>>())).Verifiable();
            var commandHandler = new FakeCommandHandler();
            var countdownEvent = new CountdownEvent(1);
            var eventHandler = new FakeEventHandler(countdownEvent);

            EventSourcing.SetCommandHandlerResolver(() => new[] { commandHandler });
            EventSourcing.SetEventHandlerResolver(() => new[] { eventHandler });
            var sut = new EventSourcing(eventStore.Object);

            sut.Invoke(command.Object);
            countdownEvent.Wait();

            Assert.Equal(1, commandHandler.Count);
            Assert.Equal(1, eventHandler.Count);
            eventStore.Verify();
        }

        [Fact]
        public void Test2()
        {
            var eventStore = new Mock<IEventStore>();
            var commands = new List<ICommand>();

            for (int i = 0; i < 10000; i++)
            {
                commands.Add(new Mock<ICommand>().Object);
            }

            eventStore.Setup(es => es.Persist(It.IsAny<IEnumerable<IEvent>>()));
            var commandHandler = new FakeCommandHandler();
            var countdownEvent = new CountdownEvent(commands.Count);
            var eventHandler = new FakeEventHandler(countdownEvent);

            EventSourcing.SetCommandHandlerResolver(() => new[] { commandHandler });
            EventSourcing.SetEventHandlerResolver(() => new[] { eventHandler });
            var sut = new EventSourcing(eventStore.Object);

            var stopwatch = Stopwatch.StartNew();
            foreach (var cmd in commands)
            {
                sut.Invoke(cmd);
            }
            countdownEvent.Wait();
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed);
            Assert.Equal(eventHandler.Count, commands.Count);
        }

        private class FakeCommandHandler : ICommandHandler
        {
            public int Count { get; set; }

            public void Handle(ICommand command, Action<IEvent> eventHandler)
            {
                Count++;
                eventHandler(new Mock<IEvent>().Object);
            }
        }

        private class FakeEventHandler : IEventHandler
        {
            private readonly CountdownEvent _countdownEvent;
            private int _count;

            public FakeEventHandler(CountdownEvent countdownEvent)
            {
                _countdownEvent = countdownEvent;
            }

            public int Count { get { return _count; } }

            public void Handle(IEvent @event)
            {
                Interlocked.Increment(ref _count);

                if (_countdownEvent != null)
                {
                    _countdownEvent.Signal();
                }
            }
        }
    }
}
