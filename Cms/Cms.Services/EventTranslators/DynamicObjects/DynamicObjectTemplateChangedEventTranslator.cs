﻿using System;
using System.Globalization;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectTemplateChangedEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectTemplateChangedEventTranslator(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectTemplateChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Objektvorlage geändert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectTemplateChanged)@event;
            var d = _repository.GetEvenIfDeleted(@event.AggregateId);

            return string.Format(
                CultureInfo.InvariantCulture,
                "Die Objektvorlage <strong>{2}</strong> der Objektdefinition <strong>{0} (Nr. {1})</strong> wurde geändert.",
                d.Name,
                d.PublicId,
                e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objecttemplate";
        }
    }
}