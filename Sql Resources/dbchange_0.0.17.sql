
begin transaction

alter table Appointment add Responsible nvarchar(128) not null default '';
alter table Appointment add Comment nvarchar(128) not null default '';

insert into Version (Major, Minor, Revision) values (0, 0, 17);

commit transaction

