using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Pages.Commands
{
    [RequiredPermission("page", PermissionLevel.CanChange)]
    public class ChangePageTitle : CommandBase
    {
        [Required(AllowEmptyStrings = true)]
        public string Title { get; set; }
    }
}