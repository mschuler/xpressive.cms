using System.Collections.Generic;

namespace Cms.Aggregates.Files
{
    public interface IFileRepository : IRepository<File>
    {
        int GetNextPublicId();

        File GetByPublicId(int publicId);

        IEnumerable<File> GetByDirectory(ulong fileDirectoryId);
    }
}