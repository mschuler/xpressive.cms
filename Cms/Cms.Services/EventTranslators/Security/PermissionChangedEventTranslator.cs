using System;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class PermissionChangedEventTranslator : IEventTranslator
    {
        private readonly IGroupRepository _groupRepository;

        public PermissionChangedEventTranslator(IGroupRepository groupRepository)
        {
            _groupRepository = groupRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PermissionChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Berechtigung ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PermissionChanged)@event;
            var group = _groupRepository.GetEvenIfDeleted(e.AggregateId);
            return string.Format(
                "Die Berechtigung der Gruppe <strong>{0}</strong> f�r das Modul <strong>{1}</strong> wurde auf <strong>{2}</strong> ge�ndert.",
                group.Name,
                e.ModuleId,
                e.Permission);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _groupRepository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}