
begin transaction

create table Website (
	Id int identity(1, 1),
	[Name] nvarchar(200) not null,
	Guid uniqueidentifier not null,
	DefaultPageId int not null default 0,
	
	constraint PK_Website primary key (Id),
	constraint UX_Website_Name unique ([Name]),
	constraint UX_Website_Guid unique (Guid),
	constraint FK_Website_Page foreign key (DefaultPageId) references Page (Id)
);

create table Domain (
	Id int identity(1, 1),
	[Name] nvarchar(200) not null,
	WebsiteId int not null,
	
	constraint PK_Domain primary key (Id),
	constraint FK_Domain_Website foreign key (WebsiteId) references Website (Id),
	constraint UX_Domain unique ([Name])
);

alter table Page drop column Domain;
alter table Page add WebsiteId int null;
alter table Page add constraint FK_Page_Website foreign key (WebsiteId) references Website (Id);

insert into Website ([Name], Guid, DefaultPageId) values ('Winterthur', NEWID(), 5);
update Page set WebsiteId = 1;

insert into Domain (WebsiteId, Name) values (1, 'gvc-winterthur.ch');
insert into Domain (WebsiteId, Name) values (1, 'www.gvc-winterthur.ch');
insert into Domain (WebsiteId, Name) values (1, 'gvc-winti.ch');
insert into Domain (WebsiteId, Name) values (1, 'www.gvc-winti.ch');

insert into Version (Major, Minor, Revision) values (0, 0, 26);

commit transaction

