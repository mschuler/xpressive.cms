﻿using Cms.TemplateEngine.Enums;
using Xunit;

namespace Cms.TemplateEngine.Tests
{
    public class DollarFunctionTests
    {
        [Fact]
        public void Given_a_dollar_function()
        {
            var engine = new Engine();
            var result = engine.ParseTreeItem("{ $(fieldname) }");

            Assert.Equal("$", result.Value);
            Assert.Equal(TreeItemType.Function, result.ItemType);

            Assert.Equal(1, result.Children.Count);
            Assert.Equal("fieldname", result.Children[0].Value);
            Assert.Equal(TreeItemType.Attribute, result.Children[0].ItemType);
        }
    }
}
