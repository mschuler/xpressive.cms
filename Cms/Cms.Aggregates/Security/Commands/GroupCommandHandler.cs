﻿using System;
using System.Linq;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.Security.Commands
{
    public class GroupCommandHandler : CommandHandlerBase
    {
        private readonly IGroupRepository _repository;
        private readonly IUserRepository _userRepository;

        public GroupCommandHandler(IGroupRepository repository, IUserRepository userRepository)
        {
            _repository = repository;
            _userRepository = userRepository;
        }

        public void When(CreateGroup command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            var name = command.Name.Trim().RemoveControlChars();
            if (_repository.GetAll(command.TenantId).Any(g => g.Name.Equals(name, StringComparison.OrdinalIgnoreCase)))
            {
                throw new ExceptionWithReason("There is already a group with this name within this tenant.");
            }

            var e = new GroupCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TenantId = command.TenantId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(DeleteGroup command, Action<IEvent> eventHandler)
        {
            var group = _repository.Get(command.AggregateId);

            var e = new GroupDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = group.Name,
            };

            eventHandler(e);
        }

        public void When(ChangeGroupName command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            var e = new GroupNameChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = command.Name.RemoveControlChars(),
            };

            eventHandler(e);
        }

        public void When(ChangePermission command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);
            var user = _userRepository.Get(command.UserId);

            if (!user.HasPermission(command.ModuleId, command.TenantId, command.Permission, _repository))
            {
                throw new ExceptionWithReason("You can't give more permissions than you have.");
            }

            var e = new PermissionChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                ModuleId = command.ModuleId,
                Permission = command.Permission,
            };

            eventHandler(e);
        }

        public void When(AddGroupMember command, Action<IEvent> eventHandler)
        {
            var group = _repository.Get(command.AggregateId);

            if (group.Members.Any(m => m.Id == command.MemberId))
            {
                throw new ExceptionWithReason("This user is already member of this group.");
            }

            var e = new GroupMemberAdded
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                MemberId = command.MemberId,
            };

            eventHandler(e);
        }

        public void When(RemoveGroupMember command, Action<IEvent> eventHandler)
        {
            var group = _repository.Get(command.AggregateId);

            if (group.Members.All(m => m.Id != command.MemberId))
            {
                throw new ExceptionWithReason("This user isn't member of this group.");
            }

            var e = new GroupMemberRemoved
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                MemberId = command.MemberId,
            };

            eventHandler(e);
        }
    }
}
