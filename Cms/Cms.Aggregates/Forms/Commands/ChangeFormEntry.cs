using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Commands
{
    [RequiredPermission("form", PermissionLevel.CanChange)]
    public class ChangeFormEntry : CommandBase
    {
        public ChangeFormEntry()
        {
            Values = new Dictionary<string, string>();
        }

        [Required]
        [ValidId]
        public ulong FormEntryId { get; set; }

        [Required]
        public Dictionary<string, string> Values { get; set; }
    }
}