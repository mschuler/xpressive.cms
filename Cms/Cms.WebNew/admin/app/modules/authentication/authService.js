﻿(function() {
    angular.module('app').factory('authService', ['$q', '$injector', 'localStorageService', '$log', function ($q, $injector, localStorageService, $log) {
        var $http;
        var authServiceFactory = {};

        var _authentication = {
            isAuth: false,
            userName: "",
            useRefreshTokens: false
        };

        var _saveRegistration = function (registration) {
            _logout();

            $http = $http || $injector.get('$http');
            return $http.post('/api/account/register', registration).then(function (response) {
                return response;
            });
        };

        var _login = function (loginData) {
            var password = loginData.password;
            password = CryptoJS.enc.Utf8.parse(password);
            password = CryptoJS.enc.Base64.stringify(password);
            var data = "grant_type=password&username=" + loginData.userName + "&password=" + password;

            if (loginData.totp) {
                data += "&totp=" + loginData.totp;
            }

            var deferred = $q.defer();
            $http = $http || $injector.get('$http');
            $http.post('/api/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
                localStorageService.set('authorizationData', {
                    token: response.access_token,
                    userName: loginData.userName,
                    useRefreshTokens: false,
                    expires: response[".expires"]
                });

                _authentication.isAuth = true;
                _authentication.userName = loginData.userName;
                _authentication.useRefreshTokens = loginData.useRefreshTokens;

                deferred.resolve(response);
            }).error(function (err) {
                _logout();
                deferred.reject(err);
            });

            return deferred.promise;

        };

        var _logout = function () {
            localStorageService.remove('authorizationData');

            _authentication.isAuth = false;
            _authentication.userName = "";
            _authentication.useRefreshTokens = false;

        };

        var _fillAuthData = function () {
            var authData = localStorageService.get('authorizationData');
            if (authData) {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
                _authentication.useRefreshTokens = authData.useRefreshTokens;
            }
        };

        authServiceFactory.saveRegistration = _saveRegistration;
        authServiceFactory.login = _login;
        authServiceFactory.logout = _logout;
        authServiceFactory.fillAuthData = _fillAuthData;
        authServiceFactory.authentication = _authentication;

        return authServiceFactory;
    }]);
}());