begin transaction

alter table FormAction add TemplateId int not null default 0;

insert into Version (Major, Minor, Revision) values (0, 0, 7);

commit transaction