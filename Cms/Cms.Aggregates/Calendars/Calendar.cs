﻿using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Calendars
{
    public class Calendar : AggregateBase
    {
        private readonly object _categoriesLock = new object();
        private readonly List<Category> _categories;
        private readonly object _appointmentsLock = new object();
        private readonly List<Appointment> _appointments;

        public Calendar()
        {
            _categories = new List<Category>();
            _appointments = new List<Appointment>();
        }

        public ulong TenantId { get; set; }

        public IEnumerable<Category> Categories
        {
            get
            {
                lock (_categoriesLock)
                {
                    return _categories.ToList();
                }
            }
        }

        public IEnumerable<Appointment> Appointments
        {
            get
            {
                lock (_appointmentsLock)
                {
                    return _appointments.ToList();
                }
            }
        }

        public void Add(Category category)
        {
            lock (_categoriesLock)
            {
                _categories.Add(category);
            }
        }

        public void Remove(Category category)
        {
            lock (_categoriesLock)
            {
                _categories.Remove(category);
            }
        }

        public void Add(Appointment appointment)
        {
            lock (_appointmentsLock)
            {
                _appointments.Add(appointment);
            }
        }

        public void Remove(Appointment appointment)
        {
            lock (_appointmentsLock)
            {
                _appointments.Remove(appointment);
            }
        }
    }
}
