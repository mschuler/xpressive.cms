using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cms.Services.Contracts
{
    public interface IPageTrackingService
    {
        Task TrackPageViewAsync(ulong websiteId, ulong pageId, string ipAddress, string sessionId, string persistedId);

        IEnumerable<Tuple<ulong, int>> GetTheTenMostViewedPagesForOneMonth(ulong websiteId);

        int GetTotalPageViewsForOneMonth(ulong websiteId);

        IEnumerable<Tuple<string, int>> GetPageViewsPerCountryForOneMonth(ulong websiteId);

        int GetTotalSessionsForOneMonth(ulong websiteId);

        IEnumerable<Tuple<DateTime, int>> GetTotalPageViewsPerMonth(ulong websiteId);

        IEnumerable<Tuple<DateTime, int>> GetTotalPageViewsPerDay(ulong websiteId);

        int GetAveragePageViewsPerVisit(ulong websiteId);

        int GetReturningVisitorsInPercent(ulong websiteId);

        int GetTotalViewsPerPage(ulong pageId);
    }
}