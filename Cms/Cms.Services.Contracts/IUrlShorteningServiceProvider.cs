﻿using System.Collections.Generic;

namespace Cms.Services.Contracts
{
    public interface IUrlShorteningServiceProvider
    {
        IEnumerable<IUrlShorteningService> GetServices();

        IUrlShorteningService GetSelectedService(ulong tenantId);
    }
}