﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Files;
using Cms.Aggregates.Tenants;

namespace Cms.Aggregates.PageTemplates.Queries
{
    internal class PageTemplateQueryHandler : QueryHandlerBase
    {
        private readonly IPageTemplateRepository _repository;
        private readonly IFileRepository _fileRepository;
        private readonly ITenantRepository _tenantRepository;

        public PageTemplateQueryHandler(IPageTemplateRepository repository, IFileRepository fileRepository, ITenantRepository tenantRepository)
        {
            _repository = repository;
            _fileRepository = fileRepository;
            _tenantRepository = tenantRepository;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetAllPageTemplates),
                typeof (GetPageTemplate),
                typeof (GetSharedPageTemplates)
            };
        }

        public IEnumerable<object> When(GetAllPageTemplates query)
        {
            return _repository
                .GetAll(query.TenantId)
                .OrderBy(d => d.Name, StringComparer.OrdinalIgnoreCase)
                .Select(t => GetTemplate(t, false));
        }

        public IEnumerable<object> When(GetPageTemplate query)
        {
            yield return GetTemplate(_repository.Get(query.Id), true);
        }

        public IEnumerable<object> When(GetSharedPageTemplates query)
        {
            return _repository
                .GetAll()
                .Where(p => p.TenantId != query.TenantId)
                .Where(p => p.SharedForTenants.Contains(query.TenantId))
                .OrderBy(d => d.Name, StringComparer.OrdinalIgnoreCase)
                .Select(p => GetTemplate(p, false));
        }

        private object GetTemplate(PageTemplate template, bool withTemplate)
        {
            string iconId = null;
            string iconUrl = null;

            File file;
            if (_fileRepository.TryGet(template.IconFileId, out file))
            {
                iconUrl = file.ToDownloadUrl(withTracking: false);
                iconId = file.Id.ToJsonString();
            }

            var tenants = new List<object>();
            foreach (var tenantId in template.SharedForTenants)
            {
                Tenant tenant;
                if (_tenantRepository.TryGet(tenantId, out tenant))
                {
                    tenants.Add(new
                    {
                        Id = tenant.Id.ToJsonString(),
                        tenant.Name
                    });
                }
            }

            return new
            {
                Id = template.Id.ToJsonString(),
                template.Name,
                template.PublicId,
                Template = withTemplate ? template.Template : null,
                template.ContentType,
                Icon = iconUrl,
                IconId = iconId,
                Shared = tenants,
            };
        }
    }
}
