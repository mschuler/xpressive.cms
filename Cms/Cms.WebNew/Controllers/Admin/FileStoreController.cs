using System;
using System.Collections.Generic;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.FileStore;
using Cms.Aggregates.FileStore.Commands;
using Cms.Aggregates.FileStore.Queries;
using Cms.Aggregates.Security;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class FileStoreController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _idProvider;
        private readonly IFileStoreProvider _fileStoreProvider;

        public FileStoreController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IUniqueIdProvider idProvider, IFileStoreProvider fileStoreProvider)
            : base(commandQueue, userRepository, queryHandler)
        {
            _idProvider = idProvider;
            _fileStoreProvider = fileStoreProvider;
        }

        [HttpGet, Route("api/filestores/factories")]
        public IEnumerable<object> GetFactories()
        {
            return ExecuteQuery(new GetFileStoreFactories());
        }

        [HttpGet, Route("api/filestores/factory/{fileStoreFactoryId}")]
        public IEnumerable<object> GetFileStoreFactorySettings(string fileStoreFactoryId)
        {
            var id = Guid.Parse(fileStoreFactoryId);
            return ExecuteQuery(new GetFileStoreFactorySettings { Id = id });
        }

        [HttpPost, Route("api/filestores/factory/{fileStoreFactoryId}/validate")]
        public object Validate(string fileStoreFactoryId, [FromBody] CreateFileStoreDto dto)
        {
            var id = Guid.Parse(fileStoreFactoryId);
            var factory = _fileStoreProvider.GetFactory(id);

            return new
            {
                IsValid = factory.IsValidConfiguration(dto.Settings),
            };
        }

        [HttpPost, Route("api/filestores/factory/{fileStoreFactoryId}")]
        public object Create(string fileStoreFactoryId, [FromBody]CreateFileStoreDto dto)
        {
            var id = _idProvider.GetId();
            var factoryId = Guid.Parse(fileStoreFactoryId);

            SendCommand(new CreateFileStore
            {
                AggregateId = id,
                FileStoreFactoryId = factoryId,
                Name = dto.Name,
                Settings = dto.Settings,
                TenantId = 1 // only sysadmins are able to create file stores, so tenantId is not used.
            });

            var factory = _fileStoreProvider.GetFactory(factoryId);
            var fileStore = factory.Create();

            return new
            {
                Id = id.ToJsonString(),
                dto.Name,
                fileStore.IsLocalFilesystem,
                Icon = fileStore.GetIcon(),
            };
        }

        [HttpGet, Route("api/filestores/{tenantId:ulong}")]
        public IEnumerable<object> Get(ulong tenantId)
        {
            return ExecuteQuery(new GetFileStores { TenantId = tenantId });
        }

        public class CreateFileStoreDto
        {
            public string Name { get; set; }
            public Dictionary<string, string> Settings { get; set; }
        }
    }
}