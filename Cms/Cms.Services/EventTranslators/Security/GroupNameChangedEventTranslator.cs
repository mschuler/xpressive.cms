using System;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class GroupNameChangedEventTranslator : IEventTranslator
    {
        private readonly IGroupRepository _repository;

        public GroupNameChangedEventTranslator(IGroupRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(GroupNameChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Benutzergruppe umbenannt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (GroupNameChanged)@event;
            return string.Format("Eine Benutzergruppe wurde in <strong>{0}</strong> umbenannt.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "group";
        }
    }
}