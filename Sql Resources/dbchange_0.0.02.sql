begin transaction

alter table Article alter column TemplateVersionId int null

insert into Version (Major, Minor, Revision) values (0, 0, 2);

commit transaction