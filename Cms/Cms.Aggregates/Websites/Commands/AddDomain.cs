using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Websites.Commands
{
    [RequiredPermission("website", PermissionLevel.CanChange)]
    public class AddDomain : CommandBase
    {
        [Required(AllowEmptyStrings = false), RegularExpression(WebsiteCommandHandler.DomainValidationRegex)]
        public string Host { get; set; }
    }
}