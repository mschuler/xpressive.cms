using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;

namespace Cms.EventSourcing.Services
{
    internal static class CommandValidator
    {
        public static bool IsValid(object obj, out List<string> results)
        {
            var validationContext = new ValidationContext(obj);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(obj, validationContext, validationResults, true);

            results = new List<string>();
            foreach (var validationResult in validationResults)
            {
                var members = validationResult.MemberNames != null ? string.Join(", ", validationResult.MemberNames) : string.Empty;
                results.Add(string.Format(CultureInfo.InvariantCulture, "{0}: {1}", members, validationResult.ErrorMessage));
            }
            return isValid;
        }

        public static void AssertIsValid(object obj)
        {
            List<string> result;
            if (!IsValid(obj, out result))
            {
                var messages = result.Where(r => !string.IsNullOrEmpty(r)).ToArray();
                var error = string.Join(", ", messages);
                var type = obj.GetType().Name;
                throw new ExceptionWithReason("Invalid command " + type + ": " + error);
            }
        }
    }
}