﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("objecttemplate", PermissionLevel.CanChange)]
    public class ChangeDynamicObjectTemplate : CommandBase
    {
        [ValidId]
        public ulong TemplateId { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Template { get; set; }
    }
}