﻿namespace Cms.Aggregates.FileDirectories.Events
{
    public class FileDirectoryParentChanged : EventBase<FileDirectory>
    {
        public ulong ParentId { get; set; }
    }
}