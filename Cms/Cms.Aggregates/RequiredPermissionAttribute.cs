﻿using System;
using Cms.Aggregates.Security;

namespace Cms.Aggregates
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = false)]
    public class RequiredPermissionAttribute : Attribute
    {
        public RequiredPermissionAttribute(bool isRequired = true)
        {
            IsRequired = isRequired;
        }

        public RequiredPermissionAttribute(string moduleId, PermissionLevel permissionLevel)
        {
            IsRequired = true;
            PermissionLevel = permissionLevel;
            ModuleId = moduleId;
        }

        public bool IsRequired { get; private set; }
        public string ModuleId { get; private set; }
        public PermissionLevel PermissionLevel { get; private set; }
    }
}