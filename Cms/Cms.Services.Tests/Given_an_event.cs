﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;
using Xunit;

namespace Cms.Services.Tests
{
    public class Given_an_event
    {
        [Fact]
        public void Then_there_is_an_event_translator()
        {
            var events = GetEventTypes();
            var eventTranslators = GetEventTranslatorTypes();
            var untranslatedEvents = new List<string>();

            foreach (var @event in events)
            {
                var isHandled = false;
                foreach (var eventTranslator in eventTranslators)
                {
                    var instance = CreateInstance(eventTranslator);

                    if (instance.IsResponsibleFor(@event))
                    {
                        isHandled = true;
                        break;
                    }
                }

                if (!isHandled)
                {
                    untranslatedEvents.Add(@event.Name);
                }
            }

            Assert.True(untranslatedEvents.Count == 0, string.Format("Some events doesn't have an EventTranslator.\n{0}", string.Join("\n", untranslatedEvents)));
        }

        private static IEventTranslator CreateInstance(Type eventTranslatorType)
        {
            var ctor = eventTranslatorType.GetConstructors().First();
            var parameters = new object[ctor.GetParameters().Length];
            return (IEventTranslator)Activator.CreateInstance(eventTranslatorType, parameters);
        }

        private static List<Type> GetEventTranslatorTypes()
        {
            return typeof(ContentTypeService).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(IEventTranslator)))
                .ToList();
        }

        private static IEnumerable<Type> GetEventTypes()
        {
            return typeof(EventBase<>).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(IEvent)))
                .ToList();
        }
    }
}
