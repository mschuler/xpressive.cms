﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Commands
{
    [RequiredPermission("form", PermissionLevel.CanChange)]
    public class AddFormField : CommandBase
    {
        [ValidId]
        public ulong Id { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        [RegularExpression("^[a-zA-Z][a-zA-Z_0-9- ]*$", ErrorMessage = "Special characters are not allowed. Must start with a letter.")]
        public string Name { get; set; }
    }
}