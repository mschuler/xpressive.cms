using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("objectdefinition", PermissionLevel.CanChange)]
    public class AddDynamicObjectDefinitionField : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string ContentType { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string ContentTypeOptions { get; set; }
    }
}