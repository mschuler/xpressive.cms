﻿using System;
using Cms.Aggregates.FileStore;
using Cms.Aggregates.FileStore.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.FileStores
{
    internal class FileStoreCreatedEventTranslator : IEventTranslator
    {
        private readonly IFileStoreProvider _fileStoreProvider;

        public FileStoreCreatedEventTranslator(IFileStoreProvider fileStoreProvider)
        {
            _fileStoreProvider = fileStoreProvider;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FileStoreCreated);
        }

        public string GetHeader(IEvent @event)
        {
            return "Neuer Speicherort erstellt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FileStoreCreated)@event;
            var factory = _fileStoreProvider.GetFactory(e.FileStoreFactoryId);
            var factoryName = factory != null ? factory.Name : "n/A";

            return string.Format(
                "Der Speicherort <strong>{0}</strong> vom Typ <strong>{1}</strong> wurde neu erstellt.",
                e.Name,
                factoryName);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "filestore";
        }
    }
}
