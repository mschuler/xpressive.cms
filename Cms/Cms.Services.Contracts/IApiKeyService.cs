﻿namespace Cms.Services.Contracts
{
    public interface IApiKeyService
    {
        IApiKeyPair GenerateNewKey(string ipAddress);

        bool IsValidApiKey(string key, string cookie, string ipAddress);
    }

    public interface IApiKeyPair
    {
        string ApiKey { get; }

        string Cookie { get; }
    }
}