﻿using System.Collections.Generic;

namespace Cms.Aggregates.Security
{
    public interface IGroupRepository : IRepository<Group>
    {
        IEnumerable<Group> GetAll(ulong tenantId);
    }
}