using System;
using Cms.SharedKernel;

namespace Cms.Aggregates.Forms
{
    public abstract class FormActionConfiguration
    {
        public static Type GetFormActionConfigurationType(string type)
        {
            var t = (FormActionType)Enum.Parse(typeof(FormActionType), type, true);
            return GetFormActionConfigurationType(t);
        }

        public static Type GetFormActionConfigurationType(FormActionType type)
        {
            switch (type)
            {
                case FormActionType.CreateObject:
                    return typeof(CreateObjectFormAction);
                case FormActionType.PageForwarding:
                    return typeof(PageForwardingFormAction);
                case FormActionType.Persist:
                    return typeof(PersistFormAction);
                case FormActionType.SendEmailToAddress:
                    return typeof(SendEmailToAddressFormAction);
                case FormActionType.SendEmailToField:
                    return typeof(SendEmailToFieldFormAction);
                default:
                    throw new NotSupportedException(type.ToString()).WithGuid();
            }
        }

        public abstract FormActionConfiguration Clone();
    }
}