using System;
using System.Collections.Generic;
using Cms.SharedKernel;

namespace Cms.Services.ObjectFieldContentTypeConverter
{
    internal class ObjectFieldBooleanConverter : IObjectFieldContentTypeConverter
    {
        public bool CanHandle(string type)
        {
            return type.Equals("boolean", StringComparison.OrdinalIgnoreCase);
        }

        public string Serialize(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return "false";
            }
            if (json.Equals("true", StringComparison.OrdinalIgnoreCase))
            {
                return "true";
            }
            return "false";
        }

        public object Deserialize(string value)
        {
            bool result;
            if (string.IsNullOrEmpty(value) || !bool.TryParse(value, out result))
            {
                return false;
            }
            return result;
        }

        public SortableObjectField DeserializeForSortableField(string value)
        {
            var result = (bool)Deserialize(value);
            var visible = result ? "Yes" : "No";
            return new SortableObjectField(visible);
        }

        public object DeserializeForPublicApi(string value)
        {
            return Deserialize(value);
        }

        public bool TryGetMetadataForPublicApi(string value, out object metadata)
        {
            metadata = null;
            return false;
        }

        public object DeserializeForInterpreter(string value)
        {
            return Deserialize(value);
        }

        public string SerializeForImport(string json, string format, string options)
        {
            return ParseValueForInput(json).ToString();
        }

        public string SerializeForImportPreview(string json, string format, string options)
        {
            return ParseValueForInput(json).ToString();
        }

        public bool IsValidValueForImport(string json, string format, string options)
        {
            var validInputs = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
            {
                "true", "false", "yes", "no", "ja", "nein", "1", "0"
            };

            return validInputs.Contains(json);
        }

        private static bool ParseValueForInput(string json)
        {
            var trueInputs = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
            {
                "true", "yes", "ja", "1"
            };

            return trueInputs.Contains(json);
        }
    }
}