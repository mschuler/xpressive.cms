﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.SharedKernel;

namespace Cms.Aggregates.Websites
{
    public class Website : AggregateBase
    {
        private readonly object _domainLock = new object();
        private readonly List<Domain> _domains = new List<Domain>();

        public ulong TenantId { get; set; }
        public string Name { get; set; }
        public Domain DefaultDomain { get; set; }
        public ulong DefaultPageId { get; set; }
        public int PublicId { get; set; }

        public IEnumerable<Domain> Domains
        {
            get
            {
                IList<Domain> copy;
                lock (_domainLock)
                {
                    copy = _domains.ToList();
                }
                return copy;
            }
        }

        public void AddDomain(Domain d)
        {
            if (d == null)
            {
                throw new ArgumentNullException("d").WithGuid();
            }

            lock (_domainLock)
            {
                _domains.Add(d);

                if (_domains.Count == 1)
                {
                    DefaultDomain = d;
                }
            }
        }

        public void RemoveDomain(Domain d)
        {
            if (d == null)
            {
                throw new ArgumentNullException("d").WithGuid();
            }

            lock (_domainLock)
            {
                _domains.Remove(d);

                if (d.Equals(DefaultDomain))
                {
                    DefaultDomain = null;
                }
            }
        }
    }
}
