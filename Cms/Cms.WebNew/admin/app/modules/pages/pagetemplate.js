﻿(function() {
    angular.module("app").controller("PageTemplateController", ["$scope", "$http", "$log", "$routeParams", "$timeout", "$modal", "toastr", "toastService", "dialogService", "$translate", function ($scope, $http, $log, $routeParams, $timeout, $modal, toastr, toastService, dialogService, $translate) {
        $scope.templateId = $routeParams.templateId;

        var renamePageTemplate = "";
        var errorRenamePageTemplate = "";
        var changePageTemplate = "";
        var errorChangePageTemplate = "";
        var tName = "";

        $translate(["rename_page_template", "error_rename_page_template", "error_change_page_template", "success_change_page_template", "name"]).then(function (t) {
            renamePageTemplate = t.rename_page_template;
            errorRenamePageTemplate = t.error_rename_page_template;
            changePageTemplate = t.success_change_page_template;
            errorChangePageTemplate = t.error_change_page_template;
            tName = t.name;
        });

        $scope.codemirrorOptions = {
            lineNumbers: true,
            theme: 'neo',
            lineWrapping: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            matchTags: { bothTags: true },
            autoCloseTags: true,
            styleActiveLine: true,
            viewportMargin: Infinity,
            mode: "htmlmixed",
        };

        function updateEditorMode() {
            if ($scope.template) {
                if ($scope.template.contentType === "text/html") {
                    $scope.codemirrorOptions.mode = "htmlmixed";
                } else if ($scope.template.contentType === "text/plain") {
                    $scope.codemirrorOptions.mode = "";
                } else if ($scope.template.contentType === "text/calendar") {
                    $scope.codemirrorOptions.mode = "";
                } else if ($scope.template.contentType === "application/xml") {
                    $scope.codemirrorOptions.mode = "xml";
                } else if ($scope.template.contentType === "application/xhtml+xml") {
                    $scope.codemirrorOptions.mode = "xml";
                } else if ($scope.template.contentType === "application/atom+xml") {
                    $scope.codemirrorOptions.mode = "xml";
                } else if ($scope.template.contentType === "application/rss+xml") {
                    $scope.codemirrorOptions.mode = "xml";
                } else if ($scope.template.contentType === "application/json") {
                    $scope.codemirrorOptions.mode = "javascript";
                } else if ($scope.template.contentType === "text/csv") {
                    $scope.codemirrorOptions.mode = "";
                } else if ($scope.template.contentType === "application/javascript") {
                    $scope.codemirrorOptions.mode = "javascript";
                } else if ($scope.template.contentType === "text/css") {
                    $scope.codemirrorOptions.mode = "css";
                } else {
                    $scope.codemirrorOptions.mode = "";
                }
            } else {
                $scope.codemirrorOptions.mode = "htmlmixed";
            }
        };

        $http.get("/api/pagetemplate/" + $scope.templateId, { cache: false }).success(function (data) {
            $scope.template = data;

            $http.get("/api/pagetemplates/contenttypes", { cache: false }).success(function (ct) {
                $scope.contentTypes = ct;
                var s = _.find(ct, function (c) { return c.contentType === $scope.template.contentType; });
                if (s) {
                    $scope.contentTypeName = s.name;
                }
            });

            $http.get("/api/file/" + $scope.template.iconId + "/detail", { cache: false }).success(function (f) {
                $scope.fileName = f.fullname;
            }).error(function(error) {
                $scope.fileName = "n/a";
            });

            updateEditorMode();
        });

        $scope.editName = function() {
            dialogService.singleInputDialog(renamePageTemplate, tName, $scope.template.name, function (name) {
                var dto = {
                    name: name
                };
                $http.post("/api/pagetemplate/" + $scope.template.id + "/name", dto).success(function () {
                    $scope.template.name = name;
                }).error(function(error) {
                    $log.error(angular.toJson(error));
                    toastService.error(errorRenamePageTemplate, error);
                });
            });
        };

        $scope.editContentType = function() {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/masterdata/changePageTemplateContentTypeTemplate.min.html",
                controller: "ChangePageTemplateContentTypeController",
                resolve: {
                    contentType: function() { return $scope.template.contentType; }
                }
            });

            modalInstance.result.then(function (contentType) {
                if (contentType && contentType.name && contentType.contentType) {
                    var dto = {
                        contentType: contentType.contentType,
                    };
                    $http.post("/api/pagetemplate/" + $scope.template.id + "/contenttype", dto).success(function (data) {
                        $scope.template.contentType = contentType.contentType;
                        $scope.contentTypeName = contentType.name;
                        updateEditorMode();
                    }).error(function (error) {
                        $log.error(angular.toJson(error));
                        toastService.error(errorChangePageTemplate, error);
                    });
                }
            });
        };

        $scope.editImage = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/masterdata/changePageTemplateImageTemplate.min.html",
                controller: "ChangePageTemplateImageController",
                resolve: {
                    fileId: function () { return $scope.template.iconId; }
                }
            });

            modalInstance.result.then(function (file) {
                if (file && file.name && file.id) {
                    $http.post("/api/pagetemplate/" + $scope.template.id + "/icon/" + file.id).success(function (data) {
                        $scope.template.icon = file.downloadUrl;
                        $scope.template.iconId = file.id;
                        $scope.fileName = file.fullname;
                    }).error(function (error) {
                        $log.error(angular.toJson(error));
                        toastService.error(errorChangePageTemplate, error);
                    });
                }
            });
        };

        $scope.saveTemplate = function () {
            return $http.post("/api/pagetemplate/" + $scope.templateId + "/template", $scope.template).success(function () {
                toastr.success('', changePageTemplate, { allowHtml: false, timeOut: 1000 });
                $log.info("saved page template");
            }).error(function (error) {
                $log.error(angular.toJson(error));
                toastService.error(errorChangePageTemplate, error);
            });
        };
    }]);

    angular.module('app').controller("ChangePageTemplateContentTypeController", ["$scope", "$modalInstance", "$http", "contentType", function ($scope, $modalInstance, $http, contentType) {
        $scope.scope = {
            name: "",
            contentType: {}
        };

        $scope.ok = function () {
            $modalInstance.close($scope.scope.contentType);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $http.get("/api/pagetemplates/contenttypes", { cache: false }).success(function (data) {
            $scope.contentTypes = data;
            var ct = _.find(data, function (c) { return c.contentType === contentType; });
            if (ct) {
                $scope.scope.contentType = ct;
            }
        });
    }]);

    angular.module('app').controller("ChangePageTemplateImageController", ["$scope", "$modalInstance", "$http", "dialogService", "tenantService", "fileId", function ($scope, $modalInstance, $http, dialogService, tenantService, fileId) {
        $scope.scope = {
            file: {},
        };

        $scope.browseFile = function () {
            dialogService.selectFileDialog($scope.scope.file, function (f) {
                if (!f) {
                    return;
                }
                $scope.scope.file = f;
            });
        };

        $scope.ok = function () {
            $modalInstance.close($scope.scope.file);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $http.get("/api/file/" + fileId + "/detail", { cache: false }).success(function (data) {
            $scope.scope.file = data;
        });

        $scope.getFiles = function (value) {
            var url = String.format("/api/file/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, {cache:false}).then(function (response) {
                return response.data;
            });
        };
    }]);
}());