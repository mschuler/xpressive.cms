﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    internal class CollectionInterpreter : IInterpreter
    {
        public int Priority { get; set; }
        public string Name { get { return "Collection"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.LeftItem == null ||
                tree.LeftItem.Result == null ||
                tree.ItemType != TreeItemType.Function)
            {
                return;
            }

            var collection = tree.LeftItem.Result as IEnumerable<object>;

            if (collection == null)
            {
                return;
            }

            if (string.Equals("first", tree.Value, StringComparison.OrdinalIgnoreCase))
            {
                tree.Result = collection.FirstOrDefault();
            }
            else if (string.Equals("last", tree.Value, StringComparison.OrdinalIgnoreCase))
            {
                tree.Result = collection.LastOrDefault();
            }
            else if (string.Equals("take", tree.Value, StringComparison.OrdinalIgnoreCase) && tree.Children.Count == 1 && tree.Children[0].Result is int)
            {
                var result = ((IEnumerable)tree.LeftItem.Result).GetListOfSameType();
                result.AddRange(collection.Take((int)tree.Children[0].Result));
                tree.Result = result;
            }
            else if (string.Equals("skip", tree.Value, StringComparison.OrdinalIgnoreCase) && tree.Children.Count == 1 && tree.Children[0].Result is int)
            {
                var result = ((IEnumerable)tree.LeftItem.Result).GetListOfSameType();
                result.AddRange(collection.Skip((int)tree.Children[0].Result));
                tree.Result = result;
            }
            else if (string.Equals("random", tree.Value, StringComparison.OrdinalIgnoreCase))
            {
                var random = new Random();
                tree.Result = collection.OrderBy(e => random.Next()).FirstOrDefault();
            }
        }
    }
}
