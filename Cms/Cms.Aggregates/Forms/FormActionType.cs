﻿namespace Cms.Aggregates.Forms
{
    public enum FormActionType
    {
        SendEmailToField,
        SendEmailToAddress,
        PageForwarding,
        CreateObject,
        Persist,
    }
}