﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Cms.Aggregates")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("33e40a86-e58d-4197-874c-fcf7dd87724c")]

[assembly: InternalsVisibleTo("Cms.Aggregates.Tests")]
