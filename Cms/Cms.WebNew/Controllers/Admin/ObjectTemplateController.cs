using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Commands;
using Cms.Aggregates.DynamicObjects.Queries;
using Cms.Aggregates.Security;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class ObjectTemplateController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _uniqueIdProvider;
        private readonly IDynamicObjectDefinitionRepository _repository;

        public ObjectTemplateController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IDynamicObjectDefinitionRepository repository, IUniqueIdProvider uniqueIdProvider)
            : base(commandQueue, userRepository, queryHandler)
        {
            _repository = repository;
            _uniqueIdProvider = uniqueIdProvider;
        }

        [HttpGet, Route("api/objecttemplates/{tenantId:ulong}/{definitionId:ulong}")]
        public IEnumerable<object> GetAll(ulong tenantId, ulong definitionId)
        {
            return ExecuteQuery(new GetAllObjectTemplates
            {
                ObjectDefinitionId = definitionId,
                TenantId = tenantId
            });
        }

        [HttpGet, Route("api/objecttemplate/{definitionId:ulong}/{templateId:ulong}")]
        public object Get(ulong definitionId, ulong templateId)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            return ExecuteQuery(new GetObjectTemplate
            {
                ObjectDefinitionId = definitionId,
                TemplateId = templateId,
                TenantId = tenantId
            }).FirstOrDefault();
        }

        [HttpPut, Route("api/objecttemplate/{definitionId:ulong}")]
        public object Create(ulong definitionId, [FromBody] TemplateDto dto)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            var id = _uniqueIdProvider.GetId();

            SendCommand(new CreateDynamicObjectTemplate
            {
                AggregateId = definitionId,
                Name = dto.Name,
                TenantId = tenantId,
                TemplateId = id,
            });

            return new
            {
                Id = id.ToJsonString(),
                dto.Name,
            };
        }

        [HttpPost, Route("api/objecttemplate/{definitionId:ulong}/{templateId:ulong}/rename")]
        public void Rename(ulong definitionId, ulong templateId, [FromBody]TemplateDto dto)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            SendCommand(new RenameDynamicObjectTemplate
            {
                AggregateId = definitionId,
                TemplateId = templateId,
                Name = dto.Name,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/objecttemplate/{definitionId:ulong}/{templateId:ulong}/template")]
        public void Change(ulong definitionId, ulong templateId, [FromBody] TemplateDto dto)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            SendCommand(new ChangeDynamicObjectTemplate
            {
                AggregateId = definitionId,
                TemplateId = templateId,
                Template = dto.Template,
                TenantId = tenantId,
            });
        }

        [HttpDelete, Route("api/objecttemplate/{definitionId:ulong}/{templateId:ulong}")]
        public void Delete(ulong definitionId, ulong templateId)
        {
            var tenantId = _repository.Get(definitionId).TenantId;
            SendCommand(new DeleteDynamicObjectTemplate
            {
                AggregateId = definitionId,
                TemplateId = templateId,
                TenantId = tenantId,
            });
        }

        public class TemplateDto
        {
            public string Name { get; set; }
            public string Template { get; set; }
        }
    }
}