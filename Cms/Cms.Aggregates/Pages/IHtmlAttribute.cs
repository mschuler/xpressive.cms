namespace Cms.Aggregates.Pages
{
    public interface IHtmlAttribute
    {
        string Name { get; }
        string Value { get; }
        string FullAttribute { get; }
    }
}