﻿using System.Linq;

namespace Cms.TemplateEngine
{
    internal static class Extensions
    {
        public static bool IsNumber(this string s)
        {
            return s.All(char.IsDigit);
        }

        public static bool IsSplitChar(this char c)
        {
            return c.Equals('.');
        }
    }
}
