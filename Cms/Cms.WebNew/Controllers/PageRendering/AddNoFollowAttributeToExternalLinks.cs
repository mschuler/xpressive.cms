using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Websites;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class AddNoFollowAttributeToExternalLinks : IFilter<RenderPageMessage>
    {
        private readonly IHtmlTagSearchService _htmlTagSearchService;
        private readonly IWebsiteRepository _websiteRepository;

        public AddNoFollowAttributeToExternalLinks(IHtmlTagSearchService htmlTagSearchService, IWebsiteRepository websiteRepository)
        {
            _htmlTagSearchService = htmlTagSearchService;
            _websiteRepository = websiteRepository;
        }

        public bool Execute(RenderPageMessage msg)
        {
            msg.Content = AddNofollowToAnchorTags(msg.Content);
            return true;
        }

        private string AddNofollowToAnchorTags(string html)
        {
            var anchorTags = _htmlTagSearchService.Find("a", html);
            var domains = new HashSet<string>(
                _websiteRepository
                    .GetAll()
                    .SelectMany(w => w.Domains)
                    .Select(d => d.Host),
                StringComparer.OrdinalIgnoreCase);

            foreach (var anchorTag in anchorTags)
            {
                var attributes = anchorTag.Attributes
                    .Where(a => !a.Name.Equals("rel", StringComparison.OrdinalIgnoreCase))
                    .ToList();

                var link = attributes.FirstOrDefault(a => a.Name.Equals("href", StringComparison.OrdinalIgnoreCase));
                if (link == null)
                {
                    continue;
                }

                Uri url;
                if (!Uri.TryCreate(link.Value, UriKind.RelativeOrAbsolute, out url))
                {
                    continue;
                }

                if (!url.IsAbsoluteUri || domains.Contains(url.Host))
                {
                    continue;
                }

                var joined = string.Join(" ", attributes.Select(a => a.FullAttribute));
                joined += " rel=\"nofollow\"";

                if (!attributes.Any(a => a.Name.Equals("target", StringComparison.OrdinalIgnoreCase)))
                {
                    joined += " target=\"_blank\"";
                }

                if (anchorTag.Tag.EndsWith("/>"))
                {
                    joined = "<a " + joined + "/>";
                }
                else
                {
                    joined = "<a " + joined + ">";
                }

                html = html.Replace(anchorTag.Tag, joined);
            }

            return html;
        }
    }
}