using System;
using RestSharp;
using RestSharp.Deserializers;

namespace Cms.Services.UrlShortening
{
    internal class TinyCcShorteningService : UrlShorteningServiceBase
    {
        private const string LoginKey = "Login";
        private const string ApiKeyKey = "API Key";

        public TinyCcShorteningService(IUrlShorteningRepository urlShorteningRepository)
            : base(new[] { LoginKey, ApiKeyKey }, urlShorteningRepository) { }

        public override string Name { get { return "tiny.cc"; } }

        protected override string ShortenInternal(string longUrl)
        {
            string apiKey;
            string login;
            if (!TryGetConfigurationValue(ApiKeyKey, out apiKey) || string.IsNullOrEmpty(apiKey) ||
                !TryGetConfigurationValue(LoginKey, out login) || string.IsNullOrEmpty(login))
            {
                throw new InvalidOperationException("Invalid configuration.");
            }

            var client = new RestClient("http://tiny.cc/");

            var request = new RestRequest();
            request.AddQueryParameter("c", "rest_api");
            request.AddQueryParameter("m", "shorten");
            request.AddQueryParameter("version", "2.0.3");
            request.AddQueryParameter("format", "json");
            request.AddQueryParameter("longUrl", longUrl);
            request.AddQueryParameter("login", login);
            request.AddQueryParameter("apiKey", apiKey);

            var response = client.Execute<TinyCcResult>(request);

            if (response == null)
            {
                throw new InvalidOperationException("Unable to access service.");
            }

            var deserializer = new JsonDeserializer();
            var result = deserializer.Deserialize<TinyCcResult>(response);

            if (result == null || result.Results == null)
            {
                throw new InvalidOperationException("Unable to parse JSON result.");
            }

            return result.Results.ShortUrl;
        }

        public class TinyCcResult
        {
            [DeserializeAs(Name = "errorCode")]
            public string ErrorCode { get; set; }

            [DeserializeAs(Name = "errorMessage")]
            public string ErrorMessage { get; set; }

            [DeserializeAs(Name = "results")]
            public TinyCcResultData Results { get; set; }

            [DeserializeAs(Name = "statusCode")]
            public string Status { get; set; }
        }

        public class TinyCcResultData
        {
            [DeserializeAs(Name = "hash")]
            public string Hash { get; set; }

            [DeserializeAs(Name = "short_url")]
            public string ShortUrl { get; set; }

            [DeserializeAs(Name = "userHash")]
            public string UserHash { get; set; }
        }
    }
}