﻿namespace Cms.Aggregates.DynamicObjects
{
    public interface IUserStarredDynamicObjectDefinitionRepository
        : IUserStarredRepository<DynamicObjectDefinition>
    {
    }
}