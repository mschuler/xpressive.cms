﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.Calendars.Queries
{
    [RequiredPermission("calendarcategory", PermissionLevel.CanSee)]
    public class GetAllCalendarCategories : QueryBase
    {
        public string SearchText { get; set; }
    }
}