﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("objectdefinition", PermissionLevel.CanEverything)]
    public class ShareDynamicObjectDefinition : CommandBase
    {
        [Required]
        public ulong[] TenantIds { get; set; }
    }
}