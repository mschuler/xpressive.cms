﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Commands;
using Cms.Aggregates.Security.Queries;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class UserController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _uniqueIdProvider;
        private readonly IGroupRepository _groupRepository;

        public UserController(IUniqueIdProvider uniqueIdProvider, ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IGroupRepository groupRepository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _uniqueIdProvider = uniqueIdProvider;
            _groupRepository = groupRepository;
        }

        [HttpGet, Route("api/user/current")]
        public object GetCurrentUser()
        {
            return CmsUser.ToDto();
        }

        [HttpGet, Route("api/users/{tenantId:ulong}/user/{userId:ulong}")]
        public object GetUser(ulong tenantId, ulong userId)
        {
            var user = CmsUser;
            if (user.Id != userId)
            {
                if (!user.HasPermission("user", tenantId, PermissionLevel.CanSee, _groupRepository))
                {
                    throw new HttpResponseException(HttpStatusCode.Unauthorized).WithGuid();
                }
            }

            return ExecuteQuery(new GetUser
            {
                AggregateId = userId,
                TenantId = tenantId,
            }).SingleOrDefault();
        }

        [HttpGet, Route("api/users/{tenantId:ulong}")]
        public IEnumerable<object> Get(ulong tenantId)
        {
            return ExecuteQuery(new GetAllUsers { TenantId = tenantId });
        }

        [HttpDelete, Route("api/users/{tenantId:ulong}/user/{userId:ulong}")]
        public void Delete(ulong tenantId, ulong userId)
        {
            SendCommand(new DeleteUser
            {
                AggregateId = userId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/users/{tenantId:ulong}/user/{userId:ulong}")]
        public void Update(ulong tenantId, ulong userId, [FromBody] UpdateUserDto dto)
        {
            SendCommand(new ChangeUserName
            {
                AggregateId = userId,
                TenantId = tenantId,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
            });

            SendCommand(new ChangeUserEmailAddress
            {
                AggregateId = userId,
                TenantId = tenantId,
                EmailAddress = dto.Email,
            });

            if (!string.IsNullOrWhiteSpace(dto.Password))
            {
                var bytes = Convert.FromBase64String(dto.Password);
                dto.Password = Encoding.UTF8.GetString(bytes);

                SendCommand(new ChangeUserPassword
                {
                    AggregateId = userId,
                    TenantId = tenantId,
                    Password = dto.Password,
                });
            }
        }

        [HttpPost, Route("api/users/{tenantId:ulong}/user/{userId:ulong}/sysadmin")]
        public void MakeSysadmin(ulong tenantId, ulong userId)
        {
            SendCommand(new ChangeUserSysadmin
            {
                AggregateId = userId,
                TenantId = tenantId,
                IsSysadmin = true,
            });
        }

        [HttpPost, Route("api/users/{tenantId:ulong}/user/{userId:ulong}/notsysadmin")]
        public void MakeNotSysadmin(ulong tenantId, ulong userId)
        {
            SendCommand(new ChangeUserSysadmin
            {
                AggregateId = userId,
                TenantId = tenantId,
                IsSysadmin = false,
            });
        }

        [HttpPost, Route("api/users/{tenantId:ulong}/user/{userId:ulong}/lock")]
        public void Lock(ulong tenantId, ulong userId)
        {
            SendCommand(new LockUser
            {
                AggregateId = userId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/users/{tenantId:ulong}/user/{userId:ulong}/unlock")]
        public void Unlock(ulong tenantId, ulong userId)
        {
            SendCommand(new UnlockUser
            {
                AggregateId = userId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/users/{tenantId:ulong}")]
        public UpdateUserDto Create(ulong tenantId, [FromBody] UpdateUserDto dto)
        {
            var id = _uniqueIdProvider.GetId();

            if (!string.IsNullOrWhiteSpace(dto.Password))
            {
                var bytes = Convert.FromBase64String(dto.Password);
                dto.Password = Encoding.UTF8.GetString(bytes);
            }

            SendCommand(new CreateUser
            {
                AggregateId = id,
                TenantId = tenantId,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                EmailAddress = dto.Email,
                Password = dto.Password,
                IsSysadmin = dto.IsSysadmin,
            });

            return new UpdateUserDto
            {
                Id = id.ToJsonString(),
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Email = dto.Email,
                IsSysadmin = dto.IsSysadmin,
            };
        }

        [HttpPost, Route("api/user/logout")]
        public void Logout()
        {
        }

        [HttpPost, Route("api/users/{tenantId:ulong}/user/name")]
        public void ChangeName(ulong tenantId, [FromBody] ChangeNameDto dto)
        {
            SendCommand(new ChangeOwnName
            {
                AggregateId = CmsUser.Id,
                TenantId = tenantId,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
            });
        }

        [HttpPost, Route("api/users/{tenantId:ulong}/user/password")]
        public void ChangePassword(ulong tenantId, [FromBody] ChangePasswordDto dto)
        {
            var oldPassword = Encoding.UTF8.GetString(Convert.FromBase64String(dto.OldPassword));
            var newPassword = Encoding.UTF8.GetString(Convert.FromBase64String(dto.NewPassword));

            SendCommand(new ChangeOwnPassword
            {
                AggregateId = CmsUser.Id,
                TenantId = tenantId,
                OldPassword = oldPassword,
                NewPassword = newPassword,
            });
        }

        public class ChangeNameDto
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }

        public class ChangePasswordDto
        {
            public string OldPassword { get; set; }
            public string NewPassword { get; set; }
        }

        public class UpdateUserDto
        {
            public string Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            public bool IsSysadmin { get; set; }
        }
    }
}