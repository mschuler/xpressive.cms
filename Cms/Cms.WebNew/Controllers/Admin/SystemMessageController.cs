using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Security;
using Cms.Aggregates.SystemMessages;
using Cms.Aggregates.SystemMessages.Commands;
using Cms.SharedKernel;
using Cms.WebNew.Authentication;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class SystemMessageController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _uniqueIdProvider;
        private readonly ISystemMessageRepository _repository;
        private readonly IUserRepository _userRepository;

        public SystemMessageController(ICommandQueue commandQueue, ISystemMessageRepository repository, IUserRepository userRepository, IUniqueIdProvider uniqueIdProvider, IQueryHandler queryHandler)
            : base(commandQueue, userRepository, queryHandler)
        {
            _repository = repository;
            _userRepository = userRepository;
            _uniqueIdProvider = uniqueIdProvider;
        }

        [AllowAnonymous, HttpGet, Route("api/systemmessages/current")]
        public IEnumerable<SystemMessageDto> GetCurrentValidMessages()
        {
            var messages = _repository
                .GetAll()
                .Where(m => m.Validity.Contains(DateTime.Now))
                .OrderBy(m => m.Validity.Start)
                .Select(Create)
                .ToList();
            return messages;
        }

        [HttpGet, Route("api/systemmessages")]
        public IEnumerable<SystemMessageDto> Get()
        {
            var messages = _repository
                .GetAll()
                .Where(m => m.Validity.End >= DateTime.Now)
                .OrderBy(m => m.Validity.Start)
                .Select(Create)
                .ToList();
            return messages;
        }

        [HttpPost, Route("api/systemmessages/{tenantId:ulong}")]
        public SystemMessageDto Create(ulong tenantId, [FromBody] SystemMessageCreationDto dto)
        {
            var start = dto.StartDate.Date.Add(TimeSpan.ParseExact(dto.StartTime, "g", CultureInfo.InvariantCulture));
            var end = dto.EndDate.Date.Add(TimeSpan.ParseExact(dto.EndTime, "g", CultureInfo.InvariantCulture));

            var id = _uniqueIdProvider.GetId();

            SendCommand(new CreateSystemMessage
            {
                AggregateId = id,
                Title = dto.Title,
                Message = dto.Message,
                Validity = new DateTimeRange(start, end),
                TenantId = tenantId,
            });

            return Create(new SystemMessage
            {
                Id = id,
                Message = dto.Message,
                Title = dto.Title,
                UserId = User.GetId(false),
                Validity = new DateTimeRange(start, end)
            });
        }

        [HttpPost, Route("api/systemmessages/{tenantId:ulong}/{id:ulong}")]
        public SystemMessageDto Change(ulong tenantId, ulong id, [FromBody] SystemMessageCreationDto dto)
        {
            var start = dto.StartDate.Date.Add(TimeSpan.ParseExact(dto.StartTime, "g", CultureInfo.InvariantCulture));
            var end = dto.EndDate.Date.Add(TimeSpan.ParseExact(dto.EndTime, "g", CultureInfo.InvariantCulture));

            SendCommand(new ChangeSystemMessage
            {
                AggregateId = id,
                Title = dto.Title,
                Message = dto.Message,
                Validity = new DateTimeRange(start, end),
                TenantId = tenantId,
            });

            return Create(new SystemMessage
            {
                Id = id,
                Message = dto.Message,
                Title = dto.Title,
                UserId = User.GetId(false),
                Validity = new DateTimeRange(start, end)
            });
        }

        [HttpDelete, Route("api/systemmessages/{tenantId:ulong}/{id:ulong}")]
        public void Delete(ulong tenantId, ulong id)
        {
            SendCommand(new DeleteSystemMessage
            {
                AggregateId = id,
                TenantId = tenantId,
            });
        }

        private SystemMessageDto Create(SystemMessage message)
        {
            var user = _userRepository.Get(message.UserId);
            var start = GetDate(message.Validity.Start) + string.Format(" {0:t}", message.Validity.Start);
            var end = GetDate(message.Validity.End);

            if (message.Validity.Start.Date.Equals(message.Validity.End.Date))
            {
                end = string.Format("{0:t}", message.Validity.End);
            }

            var validity = string.Format("{0} - {1}", start, end);

            return new SystemMessageDto
            {
                Id = message.Id.ToJsonString(),
                User = user.FullName,
                Title = message.Title,
                Message = message.Message,
                Validity = validity,
                StartDate = message.Validity.Start.Date,
                EndDate = message.Validity.End.Date,
                StartTime = message.Validity.Start.ToShortTimeString(),
                EndTime = message.Validity.End.ToShortTimeString()
            };
        }

        private string GetDate(DateTime date)
        {
            if (date.Date.Equals(DateTime.Today.AddDays(-2)))
            {
                return "vorgestern";
            }
            if (date.Date.Equals(DateTime.Today.AddDays(-1)))
            {
                return "gestern";
            }
            if (date.Date.Equals(DateTime.Today))
            {
                return "heute";
            }
            if (date.Date.Equals(DateTime.Today.AddDays(1)))
            {
                return "morgen";
            }
            if (date.Date.Equals(DateTime.Today.AddDays(2)))
            {
                return "übermorgen";
            }
            return date.ToLongDateString();
        }

        public class SystemMessageDto
        {
            public string Id { get; set; }
            public string User { get; set; }
            public string Title { get; set; }
            public string Message { get; set; }
            public string Validity { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
        }

        public class SystemMessageCreationDto
        {
            public string Title { get; set; }
            public string Message { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
        }
    }
}