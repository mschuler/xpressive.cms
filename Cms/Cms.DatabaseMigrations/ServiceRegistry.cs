﻿using Autofac;

namespace Cms.DatabaseMigrations
{
    public class ServiceRegistry : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DatabaseMigrator>().As<IDatabaseMigrator>();

            base.Load(builder);
        }
    }
}
