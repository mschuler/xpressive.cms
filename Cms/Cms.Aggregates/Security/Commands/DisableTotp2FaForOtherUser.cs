namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("user", PermissionLevel.CanEverything)]
    public class DisableTotp2FaForOtherUser : CommandBase { }
}