﻿using System.Collections.Generic;

namespace Cms.Aggregates.Websites
{
    public interface IWebsiteRepository : IRepository<Website>
    {
        int GetNextPublicId();

        Website GetByHost(string host);

        IEnumerable<Website> GetAll(ulong tenantId);
    }
}