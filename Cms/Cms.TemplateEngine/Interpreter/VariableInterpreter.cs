using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    public class VariableInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "Variables"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType != TreeItemType.Function ||
                !tree.Value.Equals("var") ||
                tree.Children.Count < 1 ||
                tree.Children.Count > 2)
            {
                return;
            }

            var name = tree.Children[0].Result as string;
            var dictionary = GetOrCreateDictionary();

            if (string.IsNullOrEmpty(name))
            {
                return;
            }

            if (tree.Children.Count == 1)
            {
                object value;
                if (dictionary.TryGetValue(name, out value))
                {
                    tree.Result = value;
                }
            }

            if (tree.Children.Count == 2)
            {
                dictionary[name] = tree.Children[1].Result;
                tree.Result = string.Empty;
            }
        }

        private static IDictionary<string, object> GetOrCreateDictionary()
        {
            var key = "VariableInterpreter.Variables";
            var dictionary = CallContext.LogicalGetData(key) as Dictionary<string, object>;

            if (dictionary == null)
            {
                dictionary = new Dictionary<string, object>(StringComparer.Ordinal);
                CallContext.LogicalSetData(key, dictionary);
            }
            return dictionary;
        }
    }
}