using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Files;
using Cms.Aggregates.FileStore;
using Cms.Aggregates.Security;
using Cms.SharedKernel;
using ImageProcessor;
using WebApiContrib.ResponseMessages;
using File = Cms.Aggregates.Files.File;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class FilePreviewController : ApiControllerBase
    {
        private readonly IContentTypeService _contentTypeService;
        private readonly ILocalFileStore _localFileStore;
        private readonly IFileRepository _fileRepository;
        private readonly IFileStoreRepository _fileStoreRepository;

        public FilePreviewController(IContentTypeService contentTypeService, ILocalFileStore localFileStore, ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IFileRepository fileRepository, IFileStoreRepository fileStoreRepository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _contentTypeService = contentTypeService;
            _localFileStore = localFileStore;
            _fileRepository = fileRepository;
            _fileStoreRepository = fileStoreRepository;
        }

        [AllowAnonymous, HttpGet, Route("api/preview/colors")]
        public IEnumerable<string> GetKnownColors()
        {
            var colorNames = new List<string>();

            var r = new Regex(@"(?<=[A-Z])(?=[A-Z][a-z])|(?<=[^A-Z])(?=[A-Z])|(?<=[A-Za-z])(?=[^A-Za-z])");

            foreach (KnownColor knownColor in Enum.GetValues(typeof(KnownColor)))
            {
                if (!Color.FromKnownColor(knownColor).IsSystemColor)
                {
                    var name = Enum.GetName(typeof(KnownColor), knownColor);
                    name = r.Replace(name ?? string.Empty, " ");
                    colorNames.Add(name);
                }
            }

            return colorNames;
        }

        [AllowAnonymous, HttpGet, Route("api/preview/{fileId:ulong}/{version:ulong}/{name}")]
        public HttpResponseMessage Preview(ulong fileId, ulong version, string name, [FromUri]ImageSettingsDto settings = null)
        {
            File file;

            if (!_fileRepository.TryGet(fileId, out file))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var fileVersion = file.Versions.SingleOrDefault(v => v.Version == version);

            if (fileVersion == null)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            if (!file.IsImage())
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var store = _fileStoreRepository.Get(file.FileStoreId);
            if (!store.IsLocalFilesystem)
            {
                var redirectUrl = store.GetUrl(file);
                return new RedirectResponse(redirectUrl);
            }

            var fileInfo = _localFileStore.Get(fileId, version);

            if (!fileInfo.Exists)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var size = new Size(150, 150);

            if (settings == null || settings.Equals(new ImageSettingsDto()))
            {
                settings = new ImageSettingsDto();
                settings.Size = 150;
                settings.Quality = 50;
                size = new Size(150, 150);
            }
            else
            {
                size = settings.Size > 0 ? new Size(settings.Size, 0) : new Size(150, 150);

                if (settings.RoundedCorners > 0)
                {
                    name = name.Substring(0, name.Length - 4).TrimEnd('.') + ".png";
                }
            }

            var stream = GetImageStream(file, fileInfo, settings, factory =>
            {
                factory.Resize(size);
                factory.BackgroundColor(Color.White);
            });

            return GetStreamResponseMessage(stream, name);
        }

        private Stream GetImageStream(File file, FileInfo fileInfo, ImageSettingsDto settings, Action<ImageFactory> changeSettings)
        {
            if (!file.IsImage() || settings == null || settings.Equals(new ImageSettingsDto()))
            {
                return fileInfo.OpenRead();
            }

            return ImageFactoryService.GetImageStream(fileInfo, settings, changeSettings);
        }

        private HttpResponseMessage GetStreamResponseMessage(Stream stream, string name, long fileLength = 0)
        {
            var contentType = _contentTypeService.GetContentType(name.GetExtension());

            if (Request.Headers.Range != null)
            {
                try
                {
                    var partialResponse = Request.CreateResponse(HttpStatusCode.PartialContent);
                    partialResponse.Content = new ByteRangeStreamContent(stream, Request.Headers.Range, contentType);
                    return partialResponse;
                }
                catch (InvalidByteRangeException invalidByteRangeException)
                {
                    return Request.CreateErrorResponse(invalidByteRangeException);
                }
            }

            stream.Seek(0, SeekOrigin.Begin);
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue(contentType);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = name;
            result.Content.Headers.Expires = new DateTimeOffset(DateTime.UtcNow.AddDays(360));
            result.Content.Headers.ContentLength = fileLength > 0 ? fileLength : (long?)null;
            return result;
        }
    }
}