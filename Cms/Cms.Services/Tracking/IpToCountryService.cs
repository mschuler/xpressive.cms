﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Cms.Services.Contracts;
using log4net;
using Newtonsoft.Json;
using Nito.AsyncEx;

namespace Cms.Services.Tracking
{
    internal class IpToCountryService : IIpToCountryService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(IpToCountryService));
        private static readonly Dictionary<string, Tuple<string, DateTime>> _cache = new Dictionary<string, Tuple<string, DateTime>>(StringComparer.OrdinalIgnoreCase);
        private static readonly Dictionary<string, AsyncLock> _lockObjectPerIpAddress = new Dictionary<string, AsyncLock>(StringComparer.OrdinalIgnoreCase);
        private static readonly object _dictionaryLock = new object();
        private static readonly object _clearLock = new object();
        private static bool _isClearing;

        public async Task<string> GetCountryAsync(string ipAddress)
        {
            ClearOldCacheEntries();

            Tuple<string, DateTime> tuple;
            if (_cache.TryGetValue(ipAddress, out tuple))
            {
                return tuple.Item1;
            }

            try
            {
                using (await GetLock(ipAddress).LockAsync())
                {
                    if (_cache.TryGetValue(ipAddress, out tuple))
                    {
                        return tuple.Item1;
                    }

                    var country = await GetCountryByIpAddressAsync(ipAddress).ConfigureAwait(true);

                    _log.DebugFormat("IP address {0} belongs to country {1}", ipAddress, country);
                    tuple = Tuple.Create(country, DateTime.Now.AddDays(14));
                    _cache.Add(ipAddress, tuple);

                    return country;
                }
            }
            catch (Exception e)
            {
                _log.Error(e.Message, e);
                return string.Empty;
            }
        }

        private async Task<string> GetCountryByIpAddressAsync(string ipAddress)
        {
            // alternative to ipinfo.io: http://api.hostip.info/country.php?ip=12.215.42.19 returns XX in error case
            // another alternative: http://api.db-ip.com/addrinfo?addr=173.194.67.1&api_key=123456789
            // another alternative: http://ipinfodb.com/ip_location_api.php

            var httpClient = new HttpClient();
            var url = string.Format("http://ipinfo.io/{0}/json", ipAddress);

            if (ipAddress.Equals("::1") || ipAddress.Equals("127.0.0.1"))
            {
                url = "http://ipinfo.io/json";
            }

            var json = await httpClient.GetStringAsync(url).ConfigureAwait(false);
            var dto = JsonConvert.DeserializeObject<IpInfoDto>(json);

            if (dto == null || string.IsNullOrEmpty(dto.Country))
            {
                return string.Empty;
            }

            return dto.Country.ToUpperInvariant();
        }

        private static AsyncLock GetLock(string ipAddress)
        {
            AsyncLock @lock;

            if (_lockObjectPerIpAddress.TryGetValue(ipAddress, out @lock))
            {
                return @lock;
            }

            lock (_dictionaryLock)
            {
                if (_lockObjectPerIpAddress.TryGetValue(ipAddress, out @lock))
                {
                    return @lock;
                }


                @lock = new AsyncLock();
                _lockObjectPerIpAddress.Add(ipAddress, @lock);
            }

            return @lock;
        }

        private void ClearOldCacheEntries()
        {
            if (_isClearing)
            {
                return;
            }

            lock (_clearLock)
            {
                if (_isClearing)
                {
                    return;
                }
                _isClearing = true;
            }

            var keysToRemove = new List<string>();

            foreach (var pair in _cache)
            {
                if (pair.Value.Item2 < DateTime.Now)
                {
                    keysToRemove.Add(pair.Key);
                }
            }

            foreach (var key in keysToRemove)
            {
                _cache.Remove(key);
            }

            _isClearing = false;
        }

        public class IpInfoDto
        {
            public string Country { get; set; }
        }
    }
}