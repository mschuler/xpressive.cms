﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("object", PermissionLevel.CanChange)]
    public class CreateDynamicObject : CommandBase
    {
        [Required, ValidId]
        public ulong DefinitionId { get; set; }

        [Range(1, int.MaxValue)]
        public int PublicId { get; set; }

        [Required]
        public Dictionary<string, string> Values { get; set; }
    }
}