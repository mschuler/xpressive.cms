﻿(function () {
    angular.module("app").constant('$', window.jQuery);

    angular.module("app").controller("ServerLogController", ["$", function ($) {
        var connection = $.hubConnection();
        var hubProxy = connection.createHubProxy('signalrAppenderHub');

        hubProxy.on('onLoggedEvent', function(msg, event) {
            var dateCell = $("<td>").css("white-space", "nowrap").text(event.TimeStamp);
            var levelCell = $("<td>").text(event.Level);
            var detailsCell = $("<td>").text(event.Message);
            var row = $("<tr>").append(dateCell, levelCell, detailsCell);

            if (event.Level === "WARN") {
                row.css("background-color", "#FFFFCC");
            } else if (event.Level === "DEBUG") {
                row.css("color", "lightgray");
            } else if (event.Level === "ERROR") {
                row.css("background-color", "#FF9966");
            }

            $('#log-table tbody').append(row);
        });

        connection.start();
    }]);
}());