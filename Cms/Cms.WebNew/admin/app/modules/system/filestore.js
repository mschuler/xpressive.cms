﻿(function() {
    angular.module("app").controller("FileStoreController", ["$scope", "$http", "$log", "$modal", "toastService", "toastr", "$translate", "tenantService", function ($scope, $http, $log, $modal, toastService, toastr, $translate, tenantService) {

        $scope.fileStores = [];
        $scope.selectedFileStore = {};

        $http.get("/api/filestores/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
            $scope.fileStores = data;
        });

        $scope.selectFileStore = function(fileStore) {
            $scope.selectedFileStore = fileStore;
        };

        var errorCreatingFileStore = "";

        $translate(["error_creating_file_store"]).then(function(t) {
            errorCreatingFileStore = t.error_creating_file_store;
        });

        $scope.create = function () {
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: '/admin/views/system/filestoreDialog.min.html',
                controller: 'FileStoreDialogController',
            });

            modalInstance.result.then(function (a) {
                $http.post("/api/filestores/factory/" + a.factoryId, a).success(function(data) {
                    $scope.fileStores.push(data);
                }).error(function(error) {
                    toastService.error(errorCreatingFileStore, error);
                });
            });
        };
    }]);

    angular.module("app").controller("FileStoreDialogController", ["$scope", "$modalInstance", "$http", "$translate", function ($scope, $modalInstance, $http, $translate) {

        $scope.factories = [];
        $scope.settings = [];
        $scope.selectedFactory = {};
        $scope.fileStore = {
            name: "",
            factoryId: "",
            settings: {}
        };
        $scope.validationClass = "btn-default";

        $http.get("/api/filestores/factories", { cache: false }).success(function(data) {
            $scope.factories = data;
        });

        $scope.$watch("selectedFactory", function() {
            if (!$scope.selectedFactory || !$scope.selectedFactory.id) {
                return;
            }

            $scope.fileStore.factoryId = $scope.selectedFactory.id;
            $http.get("/api/filestores/factory/" + $scope.selectedFactory.id, { cache: false }).success(function(data) {
                $scope.settings = data;
                $scope.fileStore.settings = {};

                _.each(data, function(s) {
                    $scope.fileStore.settings[s] = "";
                });
            });
        });

        $scope.validate = function () {
            $http.post("/api/filestores/factory/" + $scope.selectedFactory.id + "/validate", $scope.fileStore).success(function (result) {
                if (result.isValid) {
                    $scope.validationClass = "btn-success";
                } else {
                    $scope.validationClass = "btn-danger";
                }
            });
        };

        $scope.ok = function () {
            $modalInstance.close($scope.fileStore);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());