namespace Cms.Aggregates.Calendars.Events
{
    public class CategoryAdded : EventBase<Calendar>
    {
        public string Name { get; set; }
    }
}