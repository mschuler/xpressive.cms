﻿using System;
using Cms.EventSourcing.Contracts.Events;

namespace Cms.Services.Contracts
{
    public interface IEventTranslator
    {
        bool IsResponsibleFor(Type eventType);

        string GetHeader(IEvent @event);

        string GetContent(IEvent @event);

        ulong GetAffectedTenantId(IEvent @event);

        string GetAffectedModuleId();
    }
}