﻿namespace Cms.Aggregates.Tenants.Events
{
    public class TenantEventHandler : EventHandlerBase
    {
        private readonly ITenantRepository _repository;

        public TenantEventHandler(ITenantRepository repository)
        {
            _repository = repository;
        }

        public void When(TenantCreated @event)
        {
            var tenant = new Tenant
            {
                Id = @event.AggregateId,
                Name = @event.Name,
                Created = @event.Date,
            };

            _repository.Add(tenant);
        }

        public void When(TenantNameChanged @event)
        {
            var tenant = _repository.Get(@event.AggregateId);
            tenant.Name = @event.Name;
        }

        public void When(TenantDeleted @event)
        {
            _repository.Delete(@event.AggregateId);
        }
    }
}