using System;
using System.Web;
using System.Web.Http.ExceptionHandling;
using Cms.EventSourcing.Services;
using log4net;

namespace Cms.WebNew
{
    public class Log4NetExceptionLogger : ExceptionLogger
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Log4NetExceptionLogger));

        public override void Log(ExceptionLoggerContext context)
        {
            string guid;
            if (TryGetGuid(context.Exception, out guid))
            {
                using (ThreadContext.Stacks["NDC"].Push(guid))
                {
                    LogException(context);
                }
            }
            else
            {
                var httpException = context.Exception as HttpException;
                if (httpException != null)
                {
                    if (httpException.Message.Contains("0x800704CD") ||
                        httpException.Message.Contains("0x800703E3"))
                    {
                        _log.Debug(httpException.Message);
                        return;
                    }
                }
                LogException(context);
            }
        }

        private static void LogException(ExceptionLoggerContext context)
        {
            var exception = context.Exception;
            var method = context.Request.Method;
            var url = context.Request.RequestUri.PathAndQuery;
            var ip = context.Request.GetClientIpAddress();
            var referrer = context.Request.GetReferrer();

            var msg = exception.Message;
            msg = string.Format("{0} - {1} {2} ({3} {4})", msg, method, url, ip, referrer);

            if (exception is ExceptionWithReason || url.StartsWith("/Rejected-By-UrlScan?", StringComparison.OrdinalIgnoreCase))
            {
                _log.Error(msg);
            }
            else
            {
                _log.Error(msg, exception);
            }
        }

        private static bool TryGetGuid(Exception exception, out string guid)
        {
            if (exception == null)
            {
                guid = null;
                return false;
            }

            if (TryGetGuid(exception.InnerException, out guid))
            {
                return true;
            }

            if (exception.Data.Contains("guid"))
            {
                guid = (string)exception.Data["guid"];
                return true;
            }

            guid = null;
            return false;
        }
    }
}