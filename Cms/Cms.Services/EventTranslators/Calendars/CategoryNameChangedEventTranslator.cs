using System;
using Cms.Aggregates.Calendars;
using Cms.Aggregates.Calendars.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Calendars
{
    internal class CategoryNameChangedEventTranslator : IEventTranslator
    {
        private readonly ICalendarRepository _repository;

        public CategoryNameChangedEventTranslator(ICalendarRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(CategoryNameChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Kalenderkategorie umbenannt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (CategoryNameChanged)@event;
            return string.Format("Die Kalenderkategorie <strong>{0}</strong> wurde in <strong>{1}</strong> umbenannt.", e.OldName, e.NewName);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var calendar = _repository.GetEvenIfDeleted(@event.AggregateId);
            return calendar.TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "calendarcategory";
        }
    }
}