﻿
namespace Cms.TemplateEngine.Enums
{
	public enum TreeItemType
	{
		Parameter,
		Expression,
		Function,
		Attribute
	}
}
