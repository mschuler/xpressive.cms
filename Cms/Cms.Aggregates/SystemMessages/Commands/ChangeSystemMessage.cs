﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.SystemMessages.Commands
{
    [RequiredPermission("sysadmin", PermissionLevel.CanChange)]
    public class ChangeSystemMessage : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Title { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Message { get; set; }

        [Required]
        public DateTimeRange Validity { get; set; }
    }
}