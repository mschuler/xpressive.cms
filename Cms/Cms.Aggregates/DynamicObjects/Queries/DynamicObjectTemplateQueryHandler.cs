﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.DynamicObjects.Queries
{
    internal class DynamicObjectTemplateQueryHandler : QueryHandlerBase
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectTemplateQueryHandler(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetAllObjectTemplates),
                typeof (GetObjectTemplate)
            };
        }

        public IEnumerable<object> When(GetAllObjectTemplates query)
        {
            var definition = _repository.Get(query.ObjectDefinitionId);

            if (definition.TenantId != query.TenantId && !definition.SharedForTenants.Contains(query.TenantId))
            {
                return Enumerable.Empty<object>();
            }

            return definition.Templates.Select(t => new
            {
                Id = t.Id.ToJsonString(),
                t.Name,
                t.PublicId,
            });
        }

        public IEnumerable<object> When(GetObjectTemplate query)
        {
            var definition = _repository.Get(query.ObjectDefinitionId);
            var template = definition.Templates.FirstOrDefault(t => t.Id.Equals(query.TemplateId));

            if (template == null)
            {
                yield break;
            }

            yield return new
            {
                Id = template.Id.ToJsonString(),
                template.Name,
                template.PublicId,
                template.Template,
            };
        }
    }
}
