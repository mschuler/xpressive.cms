﻿namespace Cms.Aggregates.Security.Events
{
    public class UserSysadminChanged : EventBase<User>
    {
        public bool IsSysadmin { get; set; }
    }
}