using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.FileDirectories.Events;
using Cms.Aggregates.Tenants;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.FileDirectories
{
    internal class FileDirectorySharedEventTranslator : IEventTranslator
    {
        private readonly IRepository<FileDirectory> _repository;
        private readonly ITenantRepository _tenantRepository;

        public FileDirectorySharedEventTranslator(IRepository<FileDirectory> repository, ITenantRepository tenantRepository)
        {
            _repository = repository;
            _tenantRepository = tenantRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FileDirectoryShared);
        }

        public string GetHeader(IEvent @event)
        {
            var e = (FileDirectoryShared)@event;
            return e.TenantIds.Any() ? "Dateiverzeichnis freigegeben" : "Dateiverzeichnis Freigabe aufgehoben";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FileDirectoryShared)@event;
            var directory = _repository.GetEvenIfDeleted(@event.AggregateId);
            var tenants = new List<string>();

            foreach (var tenantId in e.TenantIds)
            {
                Tenant tenant;
                if (_tenantRepository.TryGetEvenIfDeleted(tenantId, out tenant))
                {
                    tenants.Add(tenant.Name);
                }
            }

            var tenantList = string.Join(", ", tenants);

            if (e.TenantIds.Any())
            {
                return string.Format("Das Dateiverzeichnis <strong>{0}</strong> wurde f�r die Mandanten <strong>{1}</strong> freigegeben.", directory.Name, tenantList);
            }

            return string.Format("Die Freigabe f�r das Dateiverzeichnis <strong>{0}</strong> wurde aufgehoben.", directory.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "file";
        }
    }
}