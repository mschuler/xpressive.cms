﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.Files.Queries
{
    [RequiredPermission("file", PermissionLevel.CanSee)]
    public class GetFile : QueryBase
    {
        public ulong FileId { get; set; }
    }
}