using System.Collections.Generic;

namespace Cms.Aggregates.PageTemplates
{
    public interface IPageTemplateRepository : IRepository<PageTemplate>
    {
        int GetNextPublicId();

        PageTemplate GetByPublicId(int publicId);

        IEnumerable<PageTemplate> GetAll(ulong tenantId);
    }
}