﻿(function() {
    angular.module("app").controller("PageContentEditorObjectController", ["$scope", "$http", "tenantService", function($scope, $http, tenantService) {
        $scope.types = [];
        $scope.objects = [];
        $scope.templates = [];

        if (!$scope.c.value.value) {
            $scope.c.value.value = {};
        }

        $scope.c.value.selectedType = null;
        $scope.c.value.selectedObject = null;
        $scope.c.value.selectedTemplate = null;

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
            $scope.types = data;

            if ($scope.c.value.definitionId) {
                var t = _.find(data, function (d) { return d.id === $scope.c.value.definitionId; });
                if (t) {
                    $scope.c.value.selectedType = t;
                }
            }
        });

        $scope.$watch('c.value.selectedType', function (newVal) {
            if (newVal) {
                $scope.c.value.definitionId = newVal.id;
                var tenantId = tenantService.getTenant().id;

                $http.get("/api/objects/" + tenantId + "/" + newVal.id, { cache: false }).success(function(data) {
                    $scope.objects = data;

                    if ($scope.c.value.objectId) {
                        var o = _.find(data, function (d) { return d.id === $scope.c.value.objectId; });
                        if (o) {
                            $scope.c.value.selectedObject = o;
                        }
                    }
                });
                $http.get("/api/objecttemplates/" + tenantId + "/" + newVal.id, { cache: false }).success(function(data) {
                    $scope.templates = data;

                    if ($scope.c.value.templateName) {
                        var t = _.find(data, function (d) { return d.name === $scope.c.value.templateName; });
                        if (t) {
                            $scope.c.value.selectedTemplate = t;
                        }
                    }
                });
            }
        });

        $scope.$watch("c.value.selectedObject", function(newVal) {
            if (newVal) {
                $scope.c.value.objectId = newVal.id;
            }
        });

        $scope.$watch("c.value.selectedTemplate", function (newVal) {
            if (newVal) {
                $scope.c.value.templateName = newVal.name;
            }
        });
    }]);

    angular.module("app").controller("PageContentEditorFormController", ["$scope", "$http", "tenantService", function($scope, $http, tenantService) {
        $scope.forms = [];
        $scope.templates = [];

        var oldValue = $scope.c.value;

        if (!$scope.c.value) {
            $scope.c.value = {};
        }

        $scope.c.value.selectedForm = null;
        $scope.c.value.selectedTemplate = null;

        $scope.$watch("c.value.selectedForm", function(newVal) {
            if (newVal) {
                $scope.c.value.formId = $scope.c.value.selectedForm.id;
            }
        });

        $scope.$watch("c.value.selectedTemplate", function (newVal) {
            if (newVal) {
                $scope.c.value.templateId = $scope.c.value.selectedTemplate.id;
            }
        });

        $http.get("/api/forms/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
            $scope.forms = data;

            if ($scope.c.value.formId) {
                var f = _.find(data, function (d) { return d.id === $scope.c.value.formId; });
                if (f) {
                    $scope.c.value.selectedForm = f;
                }
            }
        });
    }]);
}());