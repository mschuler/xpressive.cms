namespace Cms.Aggregates.Pages.Events
{
    public class PageVersionRestored : EventBase<PageBase>
    {
        public ulong VersionToRestore { get; set; }
    }
}