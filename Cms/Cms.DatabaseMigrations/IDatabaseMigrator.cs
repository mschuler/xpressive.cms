﻿namespace Cms.DatabaseMigrations
{
    public interface IDatabaseMigrator
    {
        void Migrate();
    }
}