﻿using System;
using System.Collections.Concurrent;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Xunit;

namespace Cms.EventSourcing.Tests
{
    public class DapperTransactionTests : DatabasePerformanceTestBase
    {
        private const string ConnectionString = "Server=localhost\\sqlexpress;Database=xpressive.cms;Trusted_Connection=True;";

        [Fact]
        public void Given_ten_threads_and_thousands_of_rows_then_all_rows_are_inserted()
        {
            StartTest();
        }

        protected override void ClearTable()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                connection.Execute("delete from TestTable");
                connection.Close();
            }
        }

        protected override int GetNumberOfRowsInTestTable()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var result = connection.ExecuteScalar<int>("select count(*) from TestTable");
                connection.Close();

                return result;
            }
        }

        protected override int GetNumberOfDistinctThreads()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var result = connection.ExecuteScalar<int>("select count(distinct Thread) from TestTable");
                connection.Close();

                return result;
            }
        }

        protected override async Task InsertIntoDatabase(BlockingCollection<ulong> queue)
        {
            ulong id;
            while (queue.TryTake(out id, TimeSpan.FromMilliseconds(100)))
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    await connection.OpenAsync();

                    using (var transaction = connection.BeginTransaction())
                    {
                        await connection.ExecuteAsync(
                            "insert into TestTable (Id, Thread) values (@id, @thread)",
                            transaction: transaction,
                            param: new
                            {
                                id = Convert.ToInt64(id),
                                thread = Thread.CurrentThread.ManagedThreadId
                            });

                        transaction.Commit();
                    }

                    connection.Close();
                }
            }
        }
    }
}
