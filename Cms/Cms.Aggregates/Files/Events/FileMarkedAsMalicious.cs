﻿namespace Cms.Aggregates.Files.Events
{
    public class FileMarkedAsMalicious : EventBase<File>
    {
        public ulong FileVersion { get; set; }
    }
}