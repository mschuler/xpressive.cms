using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Queries
{
    [RequiredPermission("form", PermissionLevel.CanSee)]
    public class GetFormDetail : QueryBase
    {
        public ulong FormId { get; set; }
    }
}