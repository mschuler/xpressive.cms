﻿using System;
using Cms.Aggregates.SystemMessages;
using Cms.Aggregates.SystemMessages.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.SystemMessages
{
    internal class SystemMessageDeletedEventTranslator : IEventTranslator
    {
        private readonly ISystemMessageRepository _repository;

        public SystemMessageDeletedEventTranslator(ISystemMessageRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(SystemMessageDeleted);
        }

        public string GetHeader(IEvent @event)
        {
            return "Systemnachricht gelöscht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (SystemMessageDeleted)@event;
            var msg = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Die Systemnachricht <strong>{0}</strong> wurde gelöscht.", msg.Title);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}