﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Commands
{
    [RequiredPermission("form", PermissionLevel.CanChange)]
    public class DeleteFormEntry : CommandBase
    {
        public ulong FormEntryId { get; set; }
    }
}