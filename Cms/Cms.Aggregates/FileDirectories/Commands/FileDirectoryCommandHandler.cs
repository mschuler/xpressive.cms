using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.FileDirectories.Events;
using Cms.Aggregates.Tenants;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.FileDirectories.Commands
{
    public class FileDirectoryCommandHandler : CommandHandlerBase
    {
        private readonly IRepository<FileDirectory> _repository;
        private readonly ITenantRepository _tenantRepository;

        public FileDirectoryCommandHandler(IRepository<FileDirectory> repository, ITenantRepository tenantRepository)
        {
            _repository = repository;
            _tenantRepository = tenantRepository;
        }

        public void When(CreateFileDirectory command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            var name = command.Name.Trim().RemoveControlChars();
            var parent = command.ParentId > 0 ? _repository.Get(command.ParentId) : null;

            if (parent != null)
            {
                if (!parent.TenantId.Equals(command.TenantId))
                {
                    throw new ExceptionWithReason("The parent directory belongs to another tenant.");
                }
            }

            var siblings = GetSiblings(parent, command.AggregateId, command.TenantId, command.FileStoreId);

            if (siblings.Any(c => c.Name.Equals(name)))
            {
                throw new ExceptionWithReason("This parent directory already has a child with this name.");
            }

            var e = new FileDirectoryCreated
            {
                AggregateId = command.AggregateId,
                FileStoreId = command.FileStoreId,
                UserId = command.UserId,
                TenantId = command.TenantId,
                ParentId = command.ParentId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(ChangeFileDirectoryName command, Action<IEvent> eventHandler)
        {
            var name = command.Name.Trim().RemoveControlChars();
            var directory = _repository.Get(command.AggregateId);

            var siblings = GetSiblings(directory.Parent, directory.Id, directory.TenantId, directory.FileStoreId);

            if (siblings.Any(c => c.Name.Equals(name)))
            {
                throw new ExceptionWithReason("This parent directory already has a child with this name.");
            }

            var e = new FileDirectoryNameChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(ChangeFileDirectoryParent command, Action<IEvent> eventHandler)
        {
            var directory = _repository.Get(command.AggregateId);
            var newParent = command.ParentId > 0 ? _repository.Get(command.ParentId) : null;

            var siblings = GetSiblings(newParent, directory.Id, directory.TenantId, directory.FileStoreId);

            if (siblings.Any(c => c.Name.Equals(directory.Name)))
            {
                throw new ExceptionWithReason("This parent directory already has a child with this name.");
            }

            if (newParent != null && newParent.TenantId != directory.TenantId)
            {
                throw new ExceptionWithReason("You can't move this directory to another tenant.");
            }

            if (newParent != null && newParent.FileStoreId != directory.FileStoreId)
            {
                throw new ExceptionWithReason("You can't move this directory to another storage.");
            }

            var e = new FileDirectoryParentChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                ParentId = command.ParentId,
            };

            eventHandler(e);
        }

        public void When(DeleteFileDirectory command, Action<IEvent> eventHandler)
        {
            var directory = _repository.Get(command.AggregateId);
            var e = new FileDirectoryDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = directory.Name,
            };

            eventHandler(e);
        }

        public void When(ShareFileDirectory command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            foreach (var tenantId in command.TenantIds)
            {
                _tenantRepository.Get(tenantId);
            }

            var e = new FileDirectoryShared
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                TenantIds = command.TenantIds.ToList(),
            };

            eventHandler(e);
        }

        private IEnumerable<FileDirectory> GetSiblings(FileDirectory parent, ulong directoryId, ulong tenantId, ulong fileStoreId)
        {
            List<FileDirectory> siblings;

            if (parent != null)
            {
                siblings = parent.Children.ToList();
            }
            else
            {
                siblings = _repository
                    .GetAll()
                    .Where(d => d.Parent == null && d.TenantId.Equals(tenantId) && d.FileStoreId.Equals(fileStoreId))
                    .ToList();
            }

            siblings.RemoveAll(d => d.Id.Equals(directoryId));

            return siblings;
        }
    }
}