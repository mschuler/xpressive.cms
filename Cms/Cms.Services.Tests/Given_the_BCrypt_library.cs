﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Sodium;
using Xunit;
using Xunit.Abstractions;

namespace Cms.Services.Tests
{
    public class Given_the_libsodium_library
    {
        private readonly ITestOutputHelper _outputWriter;

        public Given_the_libsodium_library(ITestOutputHelper outputWriter)
        {
            _outputWriter = outputWriter;
        }

        [Fact]
        public void Then_it_can_hash_passwords()
        {
            var password = "ein eher längeres passwort";

            var hashed = PasswordHash.ScryptHashString(password, PasswordHash.Strength.Moderate);

            _outputWriter.WriteLine(hashed);
        }

        [Fact]
        public void Then_verify_works()
        {
            const string hashed = "$7$E6....2....zgWyJ8oK7ripcccrqVivgsBfWInbHKnmv0gbCkcSSy/$rfqG4DORCeopIj00DOrsBcosfAiCKgUaAV7gTv7zSl9";
            const string password = "ein eher längeres passwort";

            Assert.True(PasswordHash.ScryptHashStringVerify(hashed, password));
        }

        [Fact]
        public void Then_it_generates_every_time_a_new_hash()
        {
            const string password = "ein anderes passwort";
            var hashes = new HashSet<string>(StringComparer.Ordinal);

            for (var i = 0; i < 20; i++)
            {
                var hashed = PasswordHash.ScryptHashString(password, PasswordHash.Strength.Moderate);

                Assert.False(hashes.Contains(hashed));
                hashes.Add(hashed);
            }
        }
    }

    public class Given_the_BCrypt_library
    {
        [Fact]
        public void Then_it_can_hash_passwords()
        {
            var password = "ein eher längeres passwort";

            var salt = BCrypt.Net.BCrypt.GenerateSalt();
            var hashed = BCrypt.Net.BCrypt.HashPassword(password, salt);

            Debug.WriteLine(salt);
            Debug.WriteLine(hashed);
        }

        [Fact]
        public void Then_verify_works()
        {
            const string hashed = "$2a$10$LPWAf5rDEQ2Ra6s0fK3Lfe6ZGprrOw5eomdgP24euduQq/bIQJ99W";
            const string password = "ein eher längeres passwort";

            Assert.True(BCrypt.Net.BCrypt.Verify(password, hashed));
        }

        [Fact]
        public void Then_different_salts_with_same_password_generates_different_hashes()
        {
            const string password = "ein anderes passwort";
            var hashes = new HashSet<string>(StringComparer.Ordinal);

            for (int i = 0; i < 20; i++)
            {
                var salt = BCrypt.Net.BCrypt.GenerateSalt();
                var hashed = BCrypt.Net.BCrypt.HashPassword(password, salt);

                Assert.False(hashes.Contains(hashed));
                hashes.Add(hashed);
            }
        }

        [Fact]
        public void Then_different_passwords_with_same_salt_generates_different_hashes()
        {
            var salt = "$2a$10$LPWAf5rDEQ2Ra6s0fK3Lfe";
            var hashes = new HashSet<string>(StringComparer.Ordinal);
            var random = new Random(1);

            for (int i = 0; i < 20; i++)
            {
                var password = random.Next(99999999, int.MaxValue).ToString("D");
                var hashed = BCrypt.Net.BCrypt.HashPassword(password, salt);

                Assert.False(hashes.Contains(hashed));
                hashes.Add(hashed);
            }
        }
    }
}
