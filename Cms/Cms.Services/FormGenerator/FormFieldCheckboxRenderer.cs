using System;
using System.Text;
using Cms.Services.Contracts;

namespace Cms.Services.FormGenerator
{
    internal class FormFieldCheckboxRenderer : IFormFieldRenderer
    {
        private readonly string _name;
        private readonly bool _isRequired;

        public FormFieldCheckboxRenderer(string name, bool isRequired)
        {
            _name = name;
            _isRequired = isRequired;
        }

        public void Render(FormLayoutType type, StringBuilder builder, string id)
        {
            var required = _isRequired ? "required " : string.Empty;

            if (type == FormLayoutType.Above)
            {
                builder.AppendLine("  <div class=\"checkbox\">");
                builder.AppendLine("    <label>");
                builder.AppendFormat("      <input type=\"checkbox\" name=\"{0}\" {1}/> ", _name, required);
                builder.AppendLine(_name);
                builder.AppendLine("    </label>");
                builder.AppendLine("  </div>");
            }
            else if (type == FormLayoutType.Asside)
            {
                builder.AppendLine("  <div class=\"form-group\">");
                builder.AppendLine("    <div class=\"col-sm-offset-2 col-sm-10\">");
                builder.AppendLine("      <div class=\"checkbox\">");
                builder.AppendLine("        <label>");
                builder.AppendFormat("          <input type=\"checkbox\" name=\"{0}\" {1}/> ", _name, required);
                builder.AppendLine(_name);
                builder.AppendLine("        </label>");
                builder.AppendLine("      </div>");
                builder.AppendLine("    </div>");
                builder.AppendLine("  </div>");
            }
            else if (type == FormLayoutType.NoLabel)
            {
                builder.AppendLine("  <label>");
                builder.AppendFormat("    <input type=\"checkbox\" name=\"{0}\" {1}/> ", _name, required);
                builder.AppendLine(_name);
                builder.AppendLine("  </label>");
            }
            else
            {
                throw new NotSupportedException(type.ToString());
            }
        }
    }
}