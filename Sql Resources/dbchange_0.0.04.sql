begin transaction

alter table FormEntry add FormId int not null;

insert into Version (Major, Minor, Revision) values (0, 0, 4);

commit transaction