﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.PageTemplates.Commands;
using Cms.Aggregates.PageTemplates.Queries;
using Cms.Aggregates.Security;
using Cms.Services.Contracts;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class PageTemplateController : ApiControllerBase
    {
        private readonly IUniqueIdProvider _uniqueIdProvider;
        private readonly IContentTypeService _contentTypeService;
        private readonly IPageTemplateRepository _repository;

        public PageTemplateController(ICommandQueue commandQueue, IUserRepository userRepository, IUniqueIdProvider uniqueIdProvider, IContentTypeService contentTypeService, IQueryHandler queryHandler, IPageTemplateRepository repository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _uniqueIdProvider = uniqueIdProvider;
            _contentTypeService = contentTypeService;
            _repository = repository;
        }

        [HttpGet, Route("api/pagetemplates/{tenantId:ulong}")]
        public IEnumerable<object> GetAll(ulong tenantId)
        {
            return ExecuteQuery(new GetAllPageTemplates { TenantId = tenantId });
        }

        [HttpGet, Route("api/pagetemplates/{tenantId:ulong}/shared")]
        public IEnumerable<object> GetShared(ulong tenantId)
        {
            return ExecuteQuery(new GetSharedPageTemplates { TenantId = tenantId });
        }

        [HttpGet, Route("api/pagetemplate/{templateId:ulong}")]
        public object Get(ulong templateId)
        {
            var tenantId = _repository.Get(templateId).TenantId;
            var result = ExecuteQuery(new GetPageTemplate
            {
                Id = templateId,
                TenantId = tenantId,
            });
            return result.SingleOrDefault();
        }

        [AllowAnonymous, HttpGet, Route("api/pagetemplates/contenttypes")]
        public IEnumerable<ContentTypeDto> GetContentTypes()
        {
            var contentTypes = _contentTypeService.GetContentTypesForPages();

            var translations = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                {"text/html", "Website"},
                {"text/plain", "Normaler Text"},
                {"text/calendar", "Kalender im ical/ics Format"},
                {"application/xml", "XML"},
                {"application/xhtml+xml", "XHTML"},
                {"application/atom+xml", "Feed im Atom Format"},
                {"application/rss+xml", "Feed im RSS Format"},
                {"application/json", "Javascript Objekt"},
                {"text/csv", "Komma-separierte Tabelle"},
                {"application/javascript", "Javascript Datei"},
                {"text/css", "Cascading Style Sheet"},
            };

            foreach (var contentType in contentTypes)
            {
                string translation;
                if (!translations.TryGetValue(contentType, out translation))
                {
                    translation = contentType;
                }

                yield return new ContentTypeDto(translation, contentType);
            }
        }

        [HttpPut, Route("api/pagetemplates/{tenantId:ulong}")]
        public PageTemplateDto Create(ulong tenantId, [FromBody]PageTemplateDto template)
        {
            var id = _uniqueIdProvider.GetId();
            var publicId = _repository.GetNextPublicId();

            SendCommand(new CreatePageTemplate
            {
                AggregateId = id,
                TenantId = tenantId,
                Name = template.Name,
                PublicId = publicId,
                ContentType = template.ContentType,
            });

            return new PageTemplateDto
            {
                Id = id.ToJsonString(),
                Name = template.Name,
                PublicId = publicId,
                ContentType = template.ContentType,
            };
        }

        [HttpPost, Route("api/pagetemplate/{templateId:ulong}/template")]
        public void ChangeTemplate(ulong templateId, [FromBody] PageTemplateDto template)
        {
            var tenantId = _repository.Get(templateId).TenantId;
            SendCommand(new ChangePageTemplateTemplate
            {
                AggregateId = templateId,
                Template = template.Template,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/pagetemplate/{templateId:ulong}/name")]
        public void ChangeName(ulong templateId, [FromBody]ChangeNameDto dto)
        {
            var tenantId = _repository.Get(templateId).TenantId;
            SendCommand(new ChangePageTemplateName
            {
                AggregateId = templateId,
                Name = dto.Name,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/pagetemplate/{templateId:ulong}/icon/{fileId:ulong}")]
        public void ChangeIcon(ulong templateId, ulong fileId)
        {
            var tenantId = _repository.Get(templateId).TenantId;
            SendCommand(new ChangePageTemplateIcon
            {
                AggregateId = templateId,
                IconFileId = fileId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/pagetemplate/{templateId:ulong}/contenttype")]
        public void ChangeContentType(ulong templateId, [FromBody]PageTemplateDto template)
        {
            var contentType = template.ContentType;
            var tenantId = _repository.Get(templateId).TenantId;

            if (!GetContentTypes().Any(t => t.ContentType.Equals(contentType, StringComparison.OrdinalIgnoreCase)))
            {
                return;
            }

            SendCommand(new ChangePageTemplateContentType
            {
                AggregateId = templateId,
                ContentType = contentType,
                TenantId = tenantId,
            });
        }

        [HttpPut, Route("api/pagetemplate/{definitionId:ulong}/share")]
        public void Share(ulong definitionId, [FromBody] TenantListDto dto)
        {
            var tenantId = _repository.Get(definitionId).TenantId;

            SendCommand(new SharePageTemplate
            {
                AggregateId = definitionId,
                TenantId = tenantId,
                TenantIds = dto.TenantIds.ToArray()
            });
        }

        [HttpDelete, Route("api/pagetemplate/{templateId:ulong}")]
        public void Delete(ulong templateId)
        {
            var tenantId = _repository.Get(templateId).TenantId;
            SendCommand(new DeletePageTemplate
            {
                AggregateId = templateId,
                TenantId = tenantId,
            });
        }

        public class ContentTypeDto
        {
            public ContentTypeDto(string name, string contentType)
            {
                Name = name;
                ContentType = contentType;
            }

            public string Name { get; set; }
            public string ContentType { get; set; }
        }

        public class ChangeNameDto
        {
            public string Name { get; set; }
        }

        public class PageTemplateDto
        {
            public string Id { get; set; }

            public string Icon { get; set; }

            public string IconId { get; set; }

            public string Name { get; set; }

            public int PublicId { get; set; }

            public string Template { get; set; }

            public string ContentType { get; set; }
        }

        public class TenantListDto
        {
            public List<ulong> TenantIds { get; set; }
        }
    }
}