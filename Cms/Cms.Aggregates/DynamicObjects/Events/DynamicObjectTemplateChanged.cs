﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectTemplateChanged : EventBase<DynamicObjectDefinition>
    {
        public ulong TemplateId { get; set; }

        public string Template { get; set; }

        public string Name { get; set; }
    }
}