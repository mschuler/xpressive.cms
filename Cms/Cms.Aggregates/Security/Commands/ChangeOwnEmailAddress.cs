using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission(false)]
    public class ChangeOwnEmailAddress : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(5)]
        public string EmailAddress { get; set; }
    }
}