﻿(function() {
    angular.module("app").controller("PageContentEditorMarkdownController", ["$scope", "$http", "$log", "$interval", function ($scope, $http, $log, $interval) {
        $scope.preview = { html: "" };
        $scope.previous = null;
        var isUpdating = false;

        if ($scope.c.value && $scope.c.value.value && _.isString($scope.c.value.value)) {
            $scope.preview.html = $scope.c.value;
        } else {
            $scope.c.value = { value: "" };
        }

        var updatePreview = function() {
            if ($scope.previous == $scope.c.value.value) {
                return;
            }
            if (!$scope.c.value || !$scope.c.value.value) {
                return;
            }
            if (isUpdating) {
                return;
            }
            isUpdating = true;

            $scope.previous = $scope.c.value.value;
            $http.post("/api/markdown", { content: $scope.c.value.value }).success(function (data) {
                $scope.preview.html = data;
                isUpdating = false;
            });
        };

        var updatePreviewInterval = $interval(updatePreview, 1000);
        $scope.$on('$destroy', function (e) {
            $interval.cancel(updatePreviewInterval);
        });

        updatePreview();
    }]);
}());