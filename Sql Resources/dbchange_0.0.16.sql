
begin transaction

create table Calendar (
	Id int not null identity(1, 1),
	Guid uniqueidentifier not null,
	[Name] nvarchar(128) not null,

	constraint PK_Calendar primary key (Id),
	constraint UX_Calendar_Name unique ([Name]),
	constraint UX_Calendar_Guid unique (Guid)
)

create table CalendarCategory (
	Id int not null identity (1, 1),
	Guid uniqueidentifier not null,
	CalendarId int not null,
	[Name] nvarchar(128) not null,

	constraint PK_CalendarCategory primary key (Id),
	constraint UX_CalendarCategory unique (CalendarId, [Name]),
	constraint UX_CalendarCategory_Guid unique (Guid)
)

create table Appointment (
	Id int not null identity (1, 1),
	CalendarId int not null,
	Title nvarchar(128) not null,
	Start datetime not null,
	[End] datetime not null,
	Location nvarchar(128) not null,
	PageId int not null default 0,

	constraint PK_Appointment primary key (Id),
	constraint FK_Appointment_Calendar foreign key (CalendarId) references Calendar (Id)
)

create table AppointmentCategories (
	AppointmentId int not null,
	CategoryId int not null,

	constraint PK_AppointmentCategories primary key (AppointmentId, CategoryId),
	constraint FK_AppointmentCategories_Appointment foreign key (AppointmentId) references Appointment (Id),
	constraint FK_AppointmentCategories_Category foreign key (CategoryId) references CalendarCategory (Id)
)

alter table [File] drop column [Content];

insert into Version (Major, Minor, Revision) values (0, 0, 16);

commit transaction

