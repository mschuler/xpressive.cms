﻿namespace Cms.Aggregates.Pages
{
    public class PageContentBooleanValue : PageContentValueBase
    {
        public bool Value { get; set; }
    }
}