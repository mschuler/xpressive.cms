using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Cms.Aggregates.PageTemplates;
using Newtonsoft.Json;

namespace Cms.Aggregates.Pages
{
    internal class PageTemplateService : IPageTemplateService
    {
        private static readonly IDictionary<ulong, Tuple<string, IEnumerable<IPageField>>> _cache = new Dictionary<ulong, Tuple<string, IEnumerable<IPageField>>>();
        private static readonly object _cacheLock = new object();

        public void Reset(PageTemplate pageTemplate)
        {
            lock (_cacheLock)
            {
                _cache.Remove(pageTemplate.Id);
            }
        }

        public string GetTemplate(PageTemplate pageTemplate)
        {
            return GetOrCreateResult(pageTemplate).Item1;
        }

        public IEnumerable<IPageField> GetPageFields(PageTemplate pageTemplate)
        {
            return GetOrCreateResult(pageTemplate).Item2;
        }

        public string ReplaceField(string template, string fieldName, string replacement)
        {
            var regexSingleQuote = string.Format("{{\\s*\\$\\(\\s*'{0}'\\s*\\)\\s*}}", Regex.Escape(fieldName));
            var regexDoubleQuote = string.Format("{{\\s*\\$\\(\\s*\"{0}\"\\s*\\)\\s*}}", Regex.Escape(fieldName));
            template = Regex.Replace(template, regexSingleQuote, replacement, RegexOptions.IgnoreCase, TimeSpan.FromSeconds(2));
            template = Regex.Replace(template, regexDoubleQuote, replacement, RegexOptions.IgnoreCase, TimeSpan.FromSeconds(2));
            return template;
        }

        private Tuple<string, IEnumerable<IPageField>> GetOrCreateResult(PageTemplate pageTemplate)
        {
            Tuple<string, IEnumerable<IPageField>> result;
            if (_cache.TryGetValue(pageTemplate.Id, out result))
            {
                return result;
            }

            lock (_cacheLock)
            {
                if (_cache.TryGetValue(pageTemplate.Id, out result))
                {
                    return result;
                }

                result = Parse(pageTemplate.Template);
                _cache[pageTemplate.Id] = result;

                return result;
            }
        }

        internal Tuple<string, IEnumerable<IPageField>> Parse(string template)
        {
            if (string.IsNullOrEmpty(template))
            {
                return Tuple.Create(string.Empty, (IEnumerable<IPageField>)new IPageField[0]);
            }

            var lines = template.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            var json = string.Empty;
            PageField[] pageFields = null;
            var lineIndex = 0;

            for (; lineIndex < lines.Length; lineIndex++)
            {
                var line = lines[lineIndex];
                json += line;

                try
                {
                    pageFields = JsonConvert.DeserializeObject<PageField[]>(json);
                    lineIndex++;
                    break;
                }
                catch (JsonSerializationException) { }
                catch (JsonReaderException) { }
            }

            if (pageFields == null)
            {
                pageFields = new PageField[0];
            }
            else
            {
                template = string.Join(Environment.NewLine, lines.Skip(lineIndex));
                pageFields = pageFields.OrderBy(f => f.SortOrder).ThenBy(f => f.Name).ToArray();
            }

            return Tuple.Create(template, (IEnumerable<IPageField>)pageFields);
        }
    }
}