﻿using System.Threading.Tasks;

namespace Cms.Services.Contracts
{
    public interface ISpamService
    {
        Task<bool> IsSpam(string ipAddress, string userAgent);
    }
}