namespace Cms.Aggregates.Security.Events
{
    public class PermissionChanged : EventBase<Group>
    {
        public string ModuleId { get; set; }

        public PermissionLevel Permission { get; set; }
    }
}