using System;
using Cms.Aggregates.Websites;
using Cms.Aggregates.Websites.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Websites
{
    internal class WebsiteDeletedEventTranslator : IEventTranslator
    {
        private readonly IWebsiteRepository _repository;

        public WebsiteDeletedEventTranslator(IWebsiteRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(WebsiteDeleted);
        }

        public string GetHeader(IEvent @event)
        {
            return "Website gel�scht";
        }

        public string GetContent(IEvent @event)
        {
            var e = (WebsiteDeleted)@event;
            return string.Format("Die Website <strong>{0}</strong> wurde gel�scht.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "website";
        }
    }
}