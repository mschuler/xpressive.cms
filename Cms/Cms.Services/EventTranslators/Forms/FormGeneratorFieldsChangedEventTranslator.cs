﻿using System;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Forms
{
    internal class FormGeneratorFieldsChangedEventTranslator : IEventTranslator
    {
        private readonly IFormRepository _repository;

        public FormGeneratorFieldsChangedEventTranslator(IFormRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return typeof(FormGeneratorFieldsChanged) == eventType;
        }

        public string GetHeader(IEvent @event)
        {
            return "Einstellungen Formulargenerator geändert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FormGeneratorFieldsChanged)@event;
            var form = _repository.GetEvenIfDeleted(e.AggregateId);
            var name = string.Format("{0} (Nr. {1})", form.Name, form.PublicId);
            return string.Format("Die Einstellungen am Formulargenerator für das Formular <strong>{0}</strong> wurden geändert.", name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "form";
        }
    }
}
