using System;
using Cms.Aggregates.Files;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.PageTemplates.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.PageTemplates
{
    internal class PageTemplateIconChangedEventTranslator : IEventTranslator
    {
        private readonly IPageTemplateRepository _repository;
        private readonly IFileRepository _fileRepository;

        public PageTemplateIconChangedEventTranslator(IPageTemplateRepository repository, IFileRepository fileRepository)
        {
            _repository = repository;
            _fileRepository = fileRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageTemplateIconChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seitenvorlage ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageTemplateIconChanged)@event;
            var template = _repository.GetEvenIfDeleted(e.AggregateId);

            File icon;
            if (_fileRepository.TryGetEvenIfDeleted(e.IconFileId, out icon))
            {
                return string.Format("Das Icon der Seitenvorlage <strong>{0}</strong> wurde nach <strong>{1} (Nr. {2})</strong> ge�ndert.", template.Name, icon.Name, icon.PublicId);
            }

            return string.Format("Das Icon der Seitenvorlage <strong>{0}</strong> wurde entfernt.", template.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "pagetemplate";
        }
    }
}