﻿using System.Collections.Generic;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Queries
{
    [RequiredPermission("object", PermissionLevel.CanSee)]
    public class GetAllObjects : QueryBase
    {
        public ulong ObjectDefinitionId { get; set; }

        public IList<GetObjectFilter> Filters { get; set; }
    }
}