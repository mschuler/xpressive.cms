using System;
using System.Collections.Generic;
using System.IO;
using Cms.SharedKernel;
using Newtonsoft.Json;

namespace Cms.Services
{
    internal class LocalizationService : ILocalizationService
    {
        public string Get(string language, string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException("key");
            }

            string filePath;
            if (!TryGetLanguageFilePath(language, out filePath))
            {
                TryGetLanguageFilePath("en", out filePath);
            }

            var json = File.ReadAllText(filePath);
            var resource = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

            string value;
            if (resource.TryGetValue(key, out value))
            {
                return value;
            }

            return key;
        }

        private bool TryGetLanguageFilePath(string language, out string path)
        {
            if (string.IsNullOrEmpty(language))
            {
                throw new ArgumentNullException("language");
            }

            if (language.Length < 2)
            {
                throw new ArgumentException("language must be at least 2 characters (iso code)", "language");
            }

            language = language.Substring(0, 2).ToLowerInvariant();
            var filePath = string.Format(@"admin\i18n\{0}.json", language);

            path = Path.Combine(GetDirectoryLocation(), filePath);
            return File.Exists(path);
        }

        private static string GetDirectoryLocation()
        {
            var rootDirectory = AppDomain.CurrentDomain.BaseDirectory;
            return rootDirectory;
        }
    }
}