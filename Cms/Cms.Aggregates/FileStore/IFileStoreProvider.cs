using System;
using System.Collections.Generic;

namespace Cms.Aggregates.FileStore
{
    public interface IFileStoreProvider
    {
        IFileStoreFactory GetFactory(Guid id);

        IEnumerable<IFileStoreFactory> GetFactories();
    }
}