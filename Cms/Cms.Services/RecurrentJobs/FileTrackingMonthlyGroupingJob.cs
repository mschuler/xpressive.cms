using System;
using System.Linq;
using Cms.Services.Contracts;
using NPoco;

namespace Cms.Services.RecurrentJobs
{
    internal class FileTrackingMonthlyGroupingJob : IRecurrentJob
    {
        public string CronTab { get { return "0 0 3 1 * ?"; } }

        public void Execute()
        {
            var today = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            var maxDate = today.AddMonths(-1);
            var minDate = today.AddMonths(-2);

            using (var database = new Database("CmsDatabaseConnection"))
            {
                var entries = database.Query<FileTrackingDto>().Where(d => d.Date < maxDate && d.Date >= minDate).ToList();
                var groups = entries.GroupBy(e => Tuple.Create(e.FileId, e.WebsiteId, e.SessionId, e.Country)).ToList();

                var date = minDate.AddHours(12);

                foreach (var group in groups)
                {
                    var dtos = group.ToList();
                    var sum = dtos.Sum(d => d.Count);

                    database.Insert(new FileTrackingDto
                    {
                        Date = date,
                        FileId = group.Key.Item1,
                        WebsiteId = group.Key.Item2,
                        SessionId = group.Key.Item3,
                        Country = group.Key.Item4,
                        Count = sum,
                    });
                }

                foreach (var entry in entries)
                {
                    Delete(database, entry);
                }
            }
        }

        private void Delete(Database database, FileTrackingDto dto)
        {
            database.Execute(
                "delete from FileTracking where Date=@0 and FileId=@1 and WebsiteId=@2 and SessionId=@3 and Country=@4 and Count=@5",
                dto.Date,
                dto.FileId,
                dto.WebsiteId,
                dto.SessionId,
                dto.Country,
                dto.Count);
        }

        [TableName("FileTracking")]
        [PrimaryKey("Date", AutoIncrement = false)]
        private class FileTrackingDto
        {
            public DateTime Date { get; set; }
            public long FileId { get; set; }
            public long WebsiteId { get; set; }
            public string SessionId { get; set; }
            public string Country { get; set; }
            public int Count { get; set; }
        }
    }
}