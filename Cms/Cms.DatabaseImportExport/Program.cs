﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using NPoco;
using ProtoBuf;

namespace Cms.DatabaseImportExport
{
    public class Program
    {
        static void Main(string[] args)
        {
            var arguments = Args.Configuration.Configure<Arguments>().CreateAndBind(args);

            try
            {
                if (arguments.Type == Type.Import)
                {
                    Import(arguments.File);
                }
                else
                {
                    Export(arguments.File);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private static void Import(string filePath)
        {
            var events = LoadFromFile(filePath);
            SaveToDatabase(events);
        }

        private static void Export(string filePath)
        {
            var events = LoadFromDatabase();
            SaveToFile(filePath, events);
        }

        private static IList<PersistedEvent> LoadFromFile(string filePath)
        {
            EventsFile eventsFile;
            using (var file = File.OpenRead(filePath))
            {
                using (var zip = new GZipStream(file, CompressionMode.Decompress))
                {
                    eventsFile = Serializer.Deserialize<EventsFile>(zip);
                }
            }

            return eventsFile.Events;
        }

        private static void SaveToFile(string filePath, IEnumerable<PersistedEvent> events)
        {
            var eventsFile = new EventsFile
            {
                Events = events.ToArray()
            };

            using (var file = File.Create(filePath))
            {
                using (var zip = new GZipStream(file, CompressionMode.Compress))
                {
                    Serializer.Serialize(zip, eventsFile);
                }
            }
        }

        private static IList<PersistedEvent> LoadFromDatabase()
        {
            IList<PersistedEvent> events;

            using (var database = new Database("CmsDatabaseConnection"))
            {
                events = database.FetchBy<PersistedEvent>(s => s
                    .Where(e => !e.IsDeleted)
                    .OrderBy(e => e.Version));
            }

            return events;
        }

        private static void SaveToDatabase(IEnumerable<PersistedEvent> events)
        {
            using (var database = new Database("CmsDatabaseConnection"))
            {
                database.BeginTransaction(IsolationLevel.ReadCommitted);

                database.Execute("truncate table Event");

                try
                {
                    foreach (var @event in events)
                    {
                        database.Insert(@event);
                    }

                    database.CompleteTransaction();
                }
                catch (Exception)
                {
                    database.AbortTransaction();
                    throw;
                }
            }
        }
    }

    internal class Arguments
    {
        public string File { get; set; }
        public Type Type { get; set; }
    }

    internal enum Type
    {
        Import = 1,
        Export = 2
    }

    [ProtoContract]
    internal class EventsFile
    {
        [ProtoMember(1)]
        public PersistedEvent[] Events { get; set; }
    }


    [TableName("Event")]
    [PrimaryKey("Version", AutoIncrement = false)]
    [ProtoContract]
    internal class PersistedEvent
    {
        [ProtoMember(1)]
        public long Transaction { get; set; }

        [ProtoMember(2)]
        public long Version { get; set; }

        [ProtoMember(3)]
        public string AggregateType { get; set; }

        [ProtoMember(4)]
        public long AggregateId { get; set; }

        [ProtoMember(5)]
        public string Event { get; set; }

        [ProtoMember(6)]
        public long UserId { get; set; }

        [ProtoMember(7)]
        public string Data { get; set; }

        [ProtoMember(8)]
        public bool IsDeleted { get; set; }
    }
}
