namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("user", PermissionLevel.CanEverything)]
    public class UnlockUser : CommandBase { }
}