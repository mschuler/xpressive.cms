﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Commands
{
    [RequiredPermission("form", PermissionLevel.CanChange)]
    public class AddFormAction : CommandBase
    {
        [ValidId]
        public ulong Id { get; set; }

        [Required]
        public FormActionType Type { get; set; }

        [Required]
        public FormActionConfiguration Configuration { get; set; }
    }
}