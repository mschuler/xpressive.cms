
begin transaction

alter table MediaContent add CreationDate datetime not null default getdate();

insert into Version (Major, Minor, Revision) values (0, 0, 23);

commit transaction
