﻿namespace Cms.EventSourcing.Contracts.Events
{
    public interface IEventHandler
    {
        void Handle(IEvent @event);
    }
}
