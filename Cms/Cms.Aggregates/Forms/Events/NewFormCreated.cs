﻿namespace Cms.Aggregates.Forms.Events
{
    public class NewFormCreated : EventBase<Form>
    {
        public ulong TenantId { get; set; }

        public string Name { get; set; }

        public int PublicId { get; set; }
    }
}
