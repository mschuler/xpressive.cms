﻿namespace Cms.Aggregates.Tenants.Events
{
    public class TenantDeleted : EventBase<Tenant>
    {
        public string Name { get; set; }
    }
}