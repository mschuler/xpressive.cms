﻿using Autofac;
using Cms.Aggregates.Calendars;
using Cms.Aggregates.Calendars.Commands;
using Cms.Aggregates.Calendars.Events;
using Cms.Aggregates.Calendars.Queries;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Commands;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.Aggregates.DynamicObjects.Queries;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.FileDirectories.Commands;
using Cms.Aggregates.FileDirectories.Events;
using Cms.Aggregates.FileDirectories.Queries;
using Cms.Aggregates.Files;
using Cms.Aggregates.Files.Commands;
using Cms.Aggregates.Files.Events;
using Cms.Aggregates.Files.Queries;
using Cms.Aggregates.FileStore;
using Cms.Aggregates.FileStore.Commands;
using Cms.Aggregates.FileStore.Events;
using Cms.Aggregates.FileStore.Queries;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Commands;
using Cms.Aggregates.Forms.Events;
using Cms.Aggregates.Forms.Queries;
using Cms.Aggregates.Links;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Commands;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.Pages.Queries;
using Cms.Aggregates.PageTemplates;
using Cms.Aggregates.PageTemplates.Commands;
using Cms.Aggregates.PageTemplates.Events;
using Cms.Aggregates.PageTemplates.Queries;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Commands;
using Cms.Aggregates.Security.Events;
using Cms.Aggregates.Security.Queries;
using Cms.Aggregates.Settings;
using Cms.Aggregates.Settings.Commands;
using Cms.Aggregates.Settings.Events;
using Cms.Aggregates.SystemMessages;
using Cms.Aggregates.SystemMessages.Commands;
using Cms.Aggregates.SystemMessages.Events;
using Cms.Aggregates.Tenants;
using Cms.Aggregates.Tenants.Commands;
using Cms.Aggregates.Tenants.Events;
using Cms.Aggregates.Tenants.Queries;
using Cms.Aggregates.Websites;
using Cms.Aggregates.Websites.Commands;
using Cms.Aggregates.Websites.Events;
using Cms.Aggregates.Websites.Queries;
using Cms.EventSourcing.Contracts.Commands;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Contracts.Queries;

namespace Cms.Aggregates
{
    public class ServiceRegistry : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PageRepository>().As<IPageRepository>().SingleInstance();
            builder.RegisterType<PageEventHandler>().As<IEventHandler>();
            builder.RegisterType<PageCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<PageQueryHandler>().As<IQueryHandler>();
            builder.RegisterType<PageDtoService>().As<IPageDtoService>();

            builder.RegisterType<PageTemplateRepository>().As<IPageTemplateRepository>().SingleInstance();
            builder.RegisterType<PageTemplateCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<PageTemplateEventHandler>().As<IEventHandler>();
            builder.RegisterType<PageTemplateQueryHandler>().As<IQueryHandler>();

            builder.RegisterType<UserRepository>().As<IUserRepository>().SingleInstance();
            builder.RegisterType<UserCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<UserEventHandler>().As<IEventHandler>();
            builder.RegisterType<UserQueryHandler>().As<IQueryHandler>();
            builder.RegisterType<GroupRepository>().As<IGroupRepository>().SingleInstance();
            builder.RegisterType<GroupCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<GroupEventHandler>().As<IEventHandler>();
            builder.RegisterType<GroupQueryHandler>().As<IQueryHandler>();

            builder.RegisterType<UserStarredPageRepository>().As<IUserStarredPageRepository>().SingleInstance();
            builder.RegisterType<UserStarredPageEventHandler>().As<IEventHandler>();
            builder.RegisterType<UserStarredPageCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<TenantRepository>().As<ITenantRepository>().SingleInstance();
            builder.RegisterType<TenantEventHandler>().As<IEventHandler>();
            builder.RegisterType<TenantCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<TenantQueryHandler>().As<IQueryHandler>();

            builder.RegisterType<FileRepository>().As<IFileRepository>().SingleInstance();
            builder.RegisterType<FileEventHandler>().As<IEventHandler>();
            builder.RegisterType<FileCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<FileQueryHandler>().As<IQueryHandler>();

            builder.RegisterType<FileStoreProvider>().As<IFileStoreProvider>();
            builder.RegisterType<FileHashCalculator>().As<IFileHashCalculator>();
            builder.RegisterType<LocalFileStoreFactory>().As<IFileStoreFactory>();
            builder.RegisterType<FileStoreCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<FileStoreEventHandler>().As<IEventHandler>();
            builder.RegisterType<FileStoreRepository>().As<IFileStoreRepository>();
            builder.RegisterType<FileStoreQueryHandler>().As<IQueryHandler>();

            builder.RegisterType<FileDirectoryRepository>().As<IRepository<FileDirectory>>().SingleInstance();
            builder.RegisterType<FileDirectoryEventHandler>().As<IEventHandler>();
            builder.RegisterType<FileDirectoryCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<FileDirectoryQueryHandler>().As<IQueryHandler>();

            builder.RegisterType<WebsiteRepository>().As<IWebsiteRepository>().SingleInstance();
            builder.RegisterType<WebsiteCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<WebsiteEventHandler>().As<IEventHandler>();
            builder.RegisterType<WebsiteQueryHandler>().As<IQueryHandler>();

            builder.RegisterType<DynamicObjectDefinitionRepository>().As<IDynamicObjectDefinitionRepository>().SingleInstance();
            builder.RegisterType<DynamicObjectDefinitionCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<DynamicObjectDefinitionEventHandler>().As<IEventHandler>();
            builder.RegisterType<DynamicObjectDefinitionQueryHandler>().As<IQueryHandler>();
            builder.RegisterType<UserStarredDynamicObjectDefinitionRepository>().As<IUserStarredDynamicObjectDefinitionRepository>().SingleInstance();
            builder.RegisterType<UserStarredDynamicObjectDefinitionCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<UserStarredDynamicObjectDefinitionEventHandler>().As<IEventHandler>();

            builder.RegisterType<DynamicObjectTemplateCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<DynamicObjectTemplateEventHandler>().As<IEventHandler>();
            builder.RegisterType<DynamicObjectTemplateQueryHandler>().As<IQueryHandler>();

            builder.RegisterType<DynamicObjectRepository>().As<IDynamicObjectRepository>().SingleInstance();
            builder.RegisterType<DynamicObjectCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<DynamicObjectEventHandler>().As<IEventHandler>();
            builder.RegisterType<DynamicObjectQueryHandler>().As<IQueryHandler>();
            builder.RegisterType<DynamicObjectDtoService>().As<IDynamicObjectDtoService>();

            builder.RegisterType<SystemMessageRepository>().As<ISystemMessageRepository>().SingleInstance();
            builder.RegisterType<SystemMessageCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<SystemMessageEventHandler>().As<IEventHandler>();

            builder.RegisterType<CalendarRepository>().As<ICalendarRepository>().SingleInstance();
            builder.RegisterType<CalendarCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<CalendarEventHandler>().As<IEventHandler>();
            builder.RegisterType<CalendarCategoryQueryHandler>().As<IQueryHandler>();
            builder.RegisterType<AppointmentQueryHandler>().As<IQueryHandler>();

            builder.RegisterType<FormRepository>().As<IFormRepository>().SingleInstance();
            builder.RegisterType<FormCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<FormEventHandler>().As<IEventHandler>();
            builder.RegisterType<FormQueryHandler>().As<IQueryHandler>();

            builder.RegisterType<SettingsRepository>().As<ISettingsRepository>().SingleInstance();
            builder.RegisterType<SettingsCommandHandler>().As<ICommandHandler>();
            builder.RegisterType<SettingsEventHandler>().As<IEventHandler>();

            // services
            builder.RegisterType<LinkResolver>().As<ILinkResolver>();
            builder.RegisterType<HyperlinkResolver>().As<ILinkResolverStrategy>();
            builder.RegisterType<PageReferenceLinkResolver>().As<ILinkResolverStrategy>();
            builder.RegisterType<PageUrlService>().As<IPageUrlService>();
            builder.RegisterType<Files.LocalFileStore>().As<ILocalFileStore>();
            builder.RegisterType<HtmlTagSearchService>().As<IHtmlTagSearchService>();
            builder.RegisterType<PageTemplateService>().As<IPageTemplateService>();

            builder.RegisterType<EventTypeResolver>().As<IEventTypeResolver>();

            builder.RegisterType<DisabledPageTokenService>().As<IDisabledPageTokenService>();

            base.Load(builder);
        }
    }
}
