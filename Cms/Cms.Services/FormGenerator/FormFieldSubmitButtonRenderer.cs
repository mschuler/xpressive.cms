using System;
using System.Text;
using Cms.Aggregates.Pages;
using Cms.Services.Contracts;
using Cms.SharedKernel;

namespace Cms.Services.FormGenerator
{
    internal class FormFieldSubmitButtonRenderer : IFormFieldRenderer
    {
        private readonly ILocalizationService _localizationService;
        private readonly ICurrentRequestService _currentRequestService;

        public FormFieldSubmitButtonRenderer(ILocalizationService localizationService, ICurrentRequestService currentRequestService)
        {
            _localizationService = localizationService;
            _currentRequestService = currentRequestService;
        }

        public void Render(FormLayoutType type, StringBuilder builder, string id)
        {
            var language = _currentRequestService.Language;
            var send = _localizationService.Get(language, "send");

            if (type == FormLayoutType.Above)
            {
                builder.Append("  <button type=\"submit\" class=\"btn btn-default\">");
                builder.Append(send);
                builder.AppendLine("</button>");
            }
            else if (type == FormLayoutType.Asside)
            {
                builder.AppendLine("  <div class=\"form-group\">");
                builder.AppendLine("    <div class=\"col-sm-offset-2 col-sm-10\">");
                builder.Append("      <button type=\"submit\" class=\"btn btn-default\">");
                builder.Append(send);
                builder.AppendLine("</button>");
                builder.AppendLine("    </div>");
                builder.AppendLine("  </div>");
            }
            else if (type == FormLayoutType.NoLabel)
            {
                builder.Append("  <button type=\"submit\" class=\"btn btn-default\">");
                builder.Append(send);
                builder.AppendLine("</button>");
            }
            else
            {
                throw new NotSupportedException(type.ToString());
            }
        }
    }
}