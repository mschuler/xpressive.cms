﻿
using System.Globalization;

namespace Cms.TemplateEngine
{
    public class Expression
    {
        public string Name { get; set; }
        public string FullName { get; set; }
        public string CalculatedValue { get; set; }
        public int Position { get; set; }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(CalculatedValue))
            {
                return FullName;
            }

            return string.Format(CultureInfo.InvariantCulture, "{0} = {1}", FullName, CalculatedValue);
        }
    }
}
