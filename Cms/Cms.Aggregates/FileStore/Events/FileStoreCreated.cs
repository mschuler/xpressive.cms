﻿using System;
using System.Collections.Generic;

namespace Cms.Aggregates.FileStore.Events
{
    public class FileStoreCreated : EventBase<IFileStore>
    {
        public Guid FileStoreFactoryId { get; set; }
        public string Name { get; set; }
        public IDictionary<string, string> Settings { get; set; }
    }
}