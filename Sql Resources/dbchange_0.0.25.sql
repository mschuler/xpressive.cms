
begin transaction

alter table MediaCenter add ImageId int;
alter table MediaCenter add constraint FK_MediaCenter_File foreign key (ImageId) references [File] (Id);
alter table MediaCenter add Description nvarchar(1000);

insert into Version (Major, Minor, Revision) values (0, 0, 25);

commit transaction

