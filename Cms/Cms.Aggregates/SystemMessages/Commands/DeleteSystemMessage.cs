﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.SystemMessages.Commands
{
    [RequiredPermission("sysadmin", PermissionLevel.CanEverything)]
    public class DeleteSystemMessage : CommandBase { }
}