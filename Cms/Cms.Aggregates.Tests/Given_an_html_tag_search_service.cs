﻿using System.Linq;
using Cms.Aggregates.Pages;
using Xunit;

namespace Cms.Aggregates.Tests
{
    public class Given_an_html_tag_search_service
    {
        [Fact]
        public void When_searching_for_anchor_tags_then_they_are_found()
        {
            var html = "<p><strong>New!</strong> check out our latest Builder addon - <a href='http://themify.me/addons/bar-chart'>Bar Chart</a></p>";
            var service = new HtmlTagSearchService();

            var tags = service.Find("a", html);

            Assert.Equal(1, tags.Count());
        }

        [Fact]
        public void When_searching_for_div_tags_then_they_are_found()
        {
            var html = "<!-- /footer-logo-wrapper --><div class=\"social-share\"><div class=\"facebook-share sharrre\" data-url=\"http://themify.me/\" data-text=\"Check out Themify responsive themes\"></div>";
            var service = new HtmlTagSearchService();

            var tags = service.Find("div", html);

            Assert.Equal(2, tags.Count());
        }

        [Fact]
        public void When_searching_for_attributes_then_they_are_found()
        {
            var html = "<div class=\"social-share\"><div class='facebook-share sharrre' data-url=\"http://themify.me/\" data-text=\"Check out Themify responsive themes\"></div>";
            var service = new HtmlTagSearchService();

            var tags = service.Find("div", html).ToList();

            Assert.Equal(1, tags[0].Attributes.Count());
            Assert.Equal(3, tags[1].Attributes.Count());

            var a1 = tags[0].Attributes.ToList();
            var a2 = tags[1].Attributes.ToList();
            Assert.Equal("class", a1[0].Name);
            Assert.Equal("social-share", a1[0].Value);
            Assert.Equal("class", a2[0].Name);
            Assert.Equal("facebook-share sharrre", a2[0].Value);
            Assert.Equal("data-url", a2[1].Name);
            Assert.Equal("http://themify.me/", a2[1].Value);
            Assert.Equal("data-text", a2[2].Name);
            Assert.Equal("Check out Themify responsive themes", a2[2].Value);
        }
    }
}
