﻿namespace Cms.Aggregates.Forms.Events
{
    public class FormDuplicated : EventBase<Form>
    {
        public ulong FormIdToDuplicate { get; set; }

        public string Name { get; set; }

        public int PublicId { get; set; }
    }
}