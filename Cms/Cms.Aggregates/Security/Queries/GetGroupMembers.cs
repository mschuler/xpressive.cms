﻿namespace Cms.Aggregates.Security.Queries
{
    [RequiredPermission("group", PermissionLevel.CanSee)]
    public class GetGroupMembers : QueryBase
    {
        public ulong GroupId { get; set; }
    }
}