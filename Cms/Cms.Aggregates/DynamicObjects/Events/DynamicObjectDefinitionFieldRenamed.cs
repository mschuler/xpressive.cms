﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDefinitionFieldRenamed : EventBase<DynamicObjectDefinition>
    {
        public string OldName { get; set; }
        public string NewName { get; set; }
    }
}