using System;
using Cms.Aggregates.DynamicObjects;
using Cms.Aggregates.DynamicObjects.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.DynamicObjects
{
    internal class DynamicObjectDefinitionStarredEventTranslator : IEventTranslator
    {
        private readonly IDynamicObjectDefinitionRepository _repository;

        public DynamicObjectDefinitionStarredEventTranslator(IDynamicObjectDefinitionRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(DynamicObjectDefinitionStarred);
        }

        public string GetHeader(IEvent @event)
        {
            return "Objektdefinition gepinnt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (DynamicObjectDefinitionStarred)@event;
            var definition = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Der Benutzer hat sich die Objektdefinition <strong>{0} (Nr. {1})</strong> gepinnt.", definition.Name, definition.PublicId);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "objectdefinition";
        }
    }
}