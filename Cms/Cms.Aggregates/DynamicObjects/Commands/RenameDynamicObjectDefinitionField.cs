using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Commands
{
    [RequiredPermission("objectdefinition", PermissionLevel.CanChange)]
    public class RenameDynamicObjectDefinitionField : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string OldName { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string NewName { get; set; }
    }
}