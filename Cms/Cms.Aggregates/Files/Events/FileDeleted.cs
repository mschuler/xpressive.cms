﻿namespace Cms.Aggregates.Files.Events
{
    public class FileDeleted : EventBase<File>
    {
        public int PublicId { get; set; }
        public string Name { get; set; }
    }
}