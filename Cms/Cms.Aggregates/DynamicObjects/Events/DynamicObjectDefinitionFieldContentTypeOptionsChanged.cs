﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDefinitionFieldContentTypeOptionsChanged : EventBase<DynamicObjectDefinition>
    {
        public string Name { get; set; }
        public string ContentTypeOptions { get; set; }
    }
}