﻿using System;
using Cms.SharedKernel;

namespace Cms.Services.ObjectFieldContentTypeConverter
{
    internal class ObjectFieldTextConverter : IObjectFieldContentTypeConverter
    {
        public bool CanHandle(string type)
        {
            return type.Equals("text", StringComparison.OrdinalIgnoreCase);
        }

        public string Serialize(string json)
        {
            return json ?? string.Empty;
        }

        public object Deserialize(string value)
        {
            return value ?? string.Empty;
        }

        public SortableObjectField DeserializeForSortableField(string value)
        {
            value = value ?? string.Empty;
            return new SortableObjectField(value);
        }

        public object DeserializeForPublicApi(string value)
        {
            return Deserialize(value);
        }

        public bool TryGetMetadataForPublicApi(string value, out object metadata)
        {
            metadata = null;
            return false;
        }

        public object DeserializeForInterpreter(string value)
        {
            return Deserialize(value);
        }

        public string SerializeForImport(string json, string format, string options)
        {
            return json ?? string.Empty;
        }

        public string SerializeForImportPreview(string json, string format, string options)
        {
            return json ?? string.Empty;
        }

        public bool IsValidValueForImport(string json, string format, string options)
        {
            return true;
        }
    }
}