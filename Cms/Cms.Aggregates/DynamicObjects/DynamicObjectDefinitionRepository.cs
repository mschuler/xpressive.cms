using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.DynamicObjects
{
    internal class DynamicObjectDefinitionRepository : RepositoryBase<DynamicObjectDefinition>,
        IDynamicObjectDefinitionRepository
    {
        private static readonly object _lock = new object();
        private static int _currentMaxPublicId;
        private static int _currentMaxPublicIdForTemplate;
        private readonly IDynamicObjectRepository _dynamicObjectRepository;

        public DynamicObjectDefinitionRepository(IDynamicObjectRepository dynamicObjectRepository)
        {
            _dynamicObjectRepository = dynamicObjectRepository;
        }

        public int GetNextPublicId()
        {
            lock (_lock)
            {
                if (_currentMaxPublicId == 0)
                {
                    var allDefinitions = GetAll().ToList();
                    _currentMaxPublicId = allDefinitions.Count == 0 ? 0 : allDefinitions.Max(p => p.PublicId);
                }

                _currentMaxPublicId++;
                return _currentMaxPublicId;
            }
        }

        public int GetNextPublicIdForTemplate()
        {
            lock (_lock)
            {
                if (_currentMaxPublicIdForTemplate == 0)
                {
                    var allTemplates = GetAll().SelectMany(d => d.Templates).ToList();
                    _currentMaxPublicIdForTemplate = allTemplates.Count == 0 ? 0 : allTemplates.Max(t => t.PublicId);
                }

                _currentMaxPublicIdForTemplate++;
                return _currentMaxPublicIdForTemplate;
            }
        }

        public IEnumerable<DynamicObjectDefinition> GetAll(ulong tenantId)
        {
            return GetAll()
                .ToList()
                .Where(d => d.TenantId == tenantId);
        }

        public override void DeleteAllByTenantId(ulong tenantId)
        {
            var dynamicObjectDefinitions = GetAll(tenantId).ToList();

            foreach (var definition in dynamicObjectDefinitions)
            {
                var objects = _dynamicObjectRepository.GetAll().ToList().Where(o => o.DefinitionId == definition.Id).ToList();
                objects.ForEach(o => _dynamicObjectRepository.Delete(o.Id));

                Delete(definition.Id);
            }

            var otherObjects = _dynamicObjectRepository.GetAll().Where(o => o.TenantId == tenantId).ToList();
            otherObjects.ForEach(o => _dynamicObjectRepository.Delete(o.Id));
        }
    }
}