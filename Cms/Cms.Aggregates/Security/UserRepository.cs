﻿using System;
using System.Linq;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.Security
{
    internal class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public override User Get(ulong aggregateId)
        {
            if (aggregateId == User.System.Id)
            {
                return User.System;
            }
            if (aggregateId == User.Guest.Id)
            {
                return User.Guest;
            }

            return base.Get(aggregateId);
        }

        public override bool TryGet(ulong aggregateId, out User t)
        {
            if (aggregateId == User.System.Id)
            {
                t = User.System;
                return true;
            }
            if (aggregateId == User.Guest.Id)
            {
                t = User.Guest;
                return true;
            }

            return base.TryGet(aggregateId, out t);
        }

        public override User GetEvenIfDeleted(ulong aggregateId)
        {
            if (aggregateId == User.System.Id)
            {
                return User.System;
            }
            if (aggregateId == User.Guest.Id)
            {
                return User.Guest;
            }

            return base.GetEvenIfDeleted(aggregateId);
        }

        public override bool TryGetEvenIfDeleted(ulong aggregateId, out User t)
        {
            if (aggregateId == User.System.Id)
            {
                t = User.System;
                return true;
            }
            if (aggregateId == User.Guest.Id)
            {
                t = User.Guest;
                return true;
            }

            return base.TryGetEvenIfDeleted(aggregateId, out t);
        }

        public User GetByEmailAddress(string emailAddress)
        {
            emailAddress = emailAddress.Trim();

            if (string.IsNullOrEmpty(emailAddress))
            {
                return null;
            }

            if (string.Equals(emailAddress, User.System.EmailAddress, StringComparison.OrdinalIgnoreCase) ||
                string.Equals(emailAddress, User.Guest.EmailAddress, StringComparison.OrdinalIgnoreCase))
            {
                return null;
            }

            return GetAll().SingleOrDefault(u => u.EmailAddress.Trim().Equals(emailAddress, StringComparison.OrdinalIgnoreCase));
        }

        public User GetByUsernameAndPassword(string emailAddress, string password)
        {
            var user = GetByEmailAddress(emailAddress);
            if (user == null)
            {
                return null;
            }

            if (BCrypt.Net.BCrypt.Verify(password, user.Password))
            {
                return user;
            }

            return null;
        }

        public override void Add(User aggregate)
        {
            if (GetByEmailAddress(aggregate.EmailAddress) != null)
            {
                throw new ExceptionWithReason("There is already a user with this email address.");
            }

            base.Add(aggregate);
        }

        public override void DeleteAllByTenantId(ulong tenantId)
        {
            throw new NotSupportedException("Users don't belong to tenants.").WithGuid();
        }
    }
}
