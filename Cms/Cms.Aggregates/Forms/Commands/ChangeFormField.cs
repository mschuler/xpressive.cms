using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Commands
{
    [RequiredPermission("form", PermissionLevel.CanChange)]
    public class ChangeFormField : CommandBase
    {
        [ValidId]
        public ulong Id { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }
    }
}