﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.FileDirectories.Commands
{
    [RequiredPermission("file", PermissionLevel.CanChange)]
    public class CreateFileDirectory : CommandBase
    {
        [Required, ValidId]
        public ulong FileStoreId { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Name { get; set; }

        [Required]
        public ulong ParentId { get; set; }
    }
}