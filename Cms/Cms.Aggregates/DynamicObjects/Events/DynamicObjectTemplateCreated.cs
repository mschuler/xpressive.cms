﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectTemplateCreated : EventBase<DynamicObjectDefinition>
    {
        public string Name { get; set; }

        public ulong TemplateId { get; set; }

        public int PublicId { get; set; }
    }
}