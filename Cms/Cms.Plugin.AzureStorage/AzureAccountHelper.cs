﻿using System;
using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Cms.Plugin.AzureStorage
{
    internal static class AzureAccountHelper
    {
        public static CloudBlobClient GetClient(IDictionary<string, string> settings)
        {
            string accountName;
            string accountKey;

            if (!settings.TryGetValue("Account name", out accountName) ||
                !settings.TryGetValue("Primary key", out accountKey))
            {
                throw new ArgumentException("Account name or primary key not found");
            }

            var storageAccount = new CloudStorageAccount(new StorageCredentials(accountName, accountKey), true);

            return storageAccount.CreateCloudBlobClient();
        }
    }
}
