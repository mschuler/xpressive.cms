﻿(function() {
    angular.module("app").controller("FormsController", ["$rootScope", "$scope", "$http", "$location", "tenantService", "dialogService", "toastService", "$translate", function ($rootScope, $scope, $http, $location, tenantService, dialogService, toastService, $translate) {
        $scope.forms = [];

        $scope.select = function(form) {
            $scope.selectedForm = form;
        };

        $scope.edit = function (form) {
            $location.path("/content/form/" + form.id);
        };

        $http.get("/api/permission/form/" + tenantService.getTenant().id, { cache: false }).success(function (permission) {
            $scope.canEdit = permission.canEdit;
            $scope.canAdmin = permission.canAdmin;
        });

        var tName = "";
        var renameForm = "";
        var errorRenameForm = "";
        var createForm = "";
        var errorCreateForm = "";
        var deleteForm = "";
        var confirmDeleteForm = "";
        var errorDeleteForm = "";
        var duplicateForm = "";
        var errorDuplicateForm = "";

        $translate(["rename_form", "error_rename_form", "create_form", "error_create_form", "duplicate_form", "error_duplicate_form", "name", "delete_form", "confirm_delete_form", "error_delete_form"]).then(function(t) {
            tName = t.name;
            renameForm = t.rename_form;
            errorRenameForm = t.error_rename_form;
            createForm = t.create_form;
            errorCreateForm = t.error_create_form;
            deleteForm = t.delete_form;
            confirmDeleteForm = t.confirm_delete_form;
            errorDeleteForm = t.error_delete_form;
            duplicateForm = t.duplicate_form;
            errorDuplicateForm = t.error_duplicate_form;
        });

        $scope.rename = function (form) {
            dialogService.singleInputDialog(renameForm, tName, form.name, function (name) {
                var dto = {
                    name: name
                };
                $http.post("/api/form/" + form.id + "/name", dto).success(function () {
                    form.name = name;
                }).error(function (error) {
                    toastService.error(errorRenameForm, error);
                });
            });
        };

        $scope.create = function() {
            dialogService.singleInputDialog(createForm, tName, "", function (name) {
                var dto = {
                    name: name
                };
                $http.post("/api/forms/" + tenantService.getTenant().id, dto).success(function (data) {
                    $scope.forms.push(data);
                }).error(function (error) {
                    toastService.error(errorCreateForm, error);
                });
            });
        };

        $scope.duplicate = function (form) {
            dialogService.singleInputDialog(duplicateForm, tName, form.name, function (name) {
                var dto = {
                    name: name
                };
                $http.post("/api/form/" + form.id + "/duplicate", dto).success(function (data) {
                    $scope.forms.push(data);
                }).error(function (error) {
                    toastService.error(errorDuplicateForm, error);
                });
            });
        };

        $scope.delete = function(form) {
            dialogService.confirmDialog(deleteForm, String.format(confirmDeleteForm, form.fullname), function() {
                $http.delete("/api/form/" + form.id).success(function() {
                    $scope.forms = _.reject($scope.forms, function(f) { return f.id === form.id; });
                }).error(function(error) {
                    toastService.error(errorDeleteForm, error);
                });
            });
        };

        var getForms = function () {
            $scope.forms = [];
            $http.get("/api/forms/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
                $scope.forms = data;
            });
        };

        $rootScope.$on('logout', function () {
            $scope.forms = [];
        });

        $rootScope.$on("tenantChanged", getForms);

        getForms();
    }]);

    angular.module("app").controller("FormController", ["$rootScope", "$scope", "$http", "$routeParams", "dialogService", "toastService", "$translate", function ($rootScope, $scope, $http, $routeParams, dialogService, toastService, $translate) {
        var formId = $routeParams.formId;
        $scope.formId = formId;

        $http.get("/api/form/" + formId, { cache: false }).success(function(data) {
            $scope.form = data;

            $http.get("/api/form/" + $scope.formId + "/fields", { cache: false }).success(function (f) {
                $scope.form.fields = f;
            });
        });

        var tName = "";
        var renameForm = "";
        var errorRenameForm = "";

        $translate(["rename_form", "error_rename_form", "name"]).then(function(t) {
            tName = t.name;
            renameForm = t.rename_form;
            errorRenameForm = t.error_rename_form;
        });

        $scope.editName = function() {
            dialogService.singleInputDialog(renameForm, tName, $scope.form.name, function (name) {
                var dto = {
                    name: name
                };
                $http.post("/api/form/" + formId + "/name", dto).success(function() {
                    $scope.form.name = name;
                }).error(function(error) {
                    toastService.error(errorRenameForm, error);
                });
            });
        };

        $rootScope.$on('logout', function () { $scope.form = {}; });
    }]);

    angular.module("app").controller("FormEntriesController", ["$scope", "$http", "$modal", "$timeout", "dialogService", "toastService", "$translate", function ($scope, $http, $modal, $timeout, dialogService, toastService, $translate) {
        var isLoading = true;
        $scope.entriesLoaded = false;
        $scope.selectedRow = {};
        $scope.columnFields = [];

        $scope.select = function(entry) {
            $scope.selectedRow = entry;
        };

        var editFormEntry = "";
        var errorEditFormEntry = "";
        var deleteFormEntry = "";
        var confirmDeleteFormEntry = "";
        var errorDeleteFormEntry = "";
        var deleteAllFormEntries = "";
        var confirmDeleteAllFormEntries = "";
        var errorDeleteAllFormEntries = "";

        $translate(["edit_form_entry", "error_edit_form_entry", "delete_form_entry", "confirm_delete_form_entry", "error_delete_form_entry", "delete_all_form_entries", "confirm_delete_all_form_entries", "error_delete_all_form_entries"]).then(function (t) {
            editFormEntry = t.edit_form_entry;
            errorEditFormEntry = t.error_edit_form_entry;
            deleteFormEntry = t.delete_form_entry;
            confirmDeleteFormEntry = t.confirm_delete_form_entry;
            errorDeleteFormEntry = t.error_delete_form_entry;
            deleteAllFormEntries = t.delete_all_form_entries;
            confirmDeleteAllFormEntries = t.confirm_delete_all_form_entries;
            errorDeleteAllFormEntries = t.error_delete_all_form_entries;
        });

        $scope.create = function () {
            var fields = [];
            _.each($scope.form.fields, function (f) {
                fields.push(f.name);
            });

            var copy = {};
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/forms/editFormEntryDialog.min.html",
                controller: "EditFormEntryDialogController",
                resolve: {
                    entry: function () { return copy; },
                    subject: function () { return editFormEntry; },
                    fields: function () { return fields; },
                }
            });

            modalInstance.result.then(function (e) {
                var url = String.format("/api/form/{0}/entries", $scope.formId);
                $http.post(url, e).success(function () {
                    //isLoading = true;
                    //$scope.entriesLoaded = false;
                    //$timeout(function () {
                    //    $scope.loadEntries();
                    //}, 3000);
                }).error(function (error) {
                    toastService.error(errorEditFormEntry, error);
                });
            });
        };

        $scope.edit = function (entry) {
            var urlGet = String.format("/api/form/{0}/fields", $scope.formId);
            $http.get(urlGet, { cache: false }).success(function (data) {
                var fields = [];
                _.each(data, function(f) {
                    fields.push(f.name);
                });
                _.each(_.keys(entry.values), function (k) {
                    if (!_.find(fields, function (f) { return f.toUpperCase() === k.toUpperCase(); })) {
                        fields.push(k);
                    }
                });

                var copy = angular.copy(entry);
                var modalInstance = $modal.open({
                    backdrop: 'static',
                    templateUrl: "/admin/views/forms/editFormEntryDialog.min.html",
                    controller: "EditFormEntryDialogController",
                    resolve: {
                        entry: function () { return copy; },
                        subject: function () { return editFormEntry; },
                        fields: function () { return fields; },
                    }
                });

                modalInstance.result.then(function (e) {
                    var url = String.format("/api/form/{0}/entry/{1}", $scope.formId, entry.id);
                    $http.post(url, e).success(function () {
                        //isLoading = true;
                        //$scope.entriesLoaded = false;
                        //$timeout(function () {
                        //    $scope.loadEntries();
                        //}, 3000);
                    }).error(function (error) {
                        toastService.error(errorEditFormEntry, error);
                    });
                });
            });
        };

        $scope.downloadCsv = function() {
            $http.get("/api/form/" + $scope.formId + "/entries/csv", { responseType: 'arraybuffer' }).success(function (data, status, headers) {
                headers = headers();
                var filename = headers['x-filename'] || headers['filename'] || 'form_entries.csv';
                var blob = new Blob([data], { type: "text/csv" });
                saveAs(blob, filename);
            });
        };

        $scope.clearEntries = function () {
            dialogService.confirmDialog(deleteAllFormEntries, confirmDeleteAllFormEntries, function() {
                $http.delete("/api/form/" + $scope.formId + "/entries").success(function () {
                    $scope.entries = [];
                }).error(function(error) {
                    toastService.error(errorDeleteAllFormEntries, error);
                });
            });
        }

        $scope.delete = function(entry) {
            dialogService.confirmDialog(deleteFormEntry, confirmDeleteFormEntry, function() {
                $http.delete("/api/form/" + $scope.formId + "/entry/" + entry.id).success(function() {
                    $scope.entries = _.reject($scope.entries, function(e) { return e.id === entry.id; });
                }).error(function(error) {
                    toastService.error(errorDeleteFormEntry, error);
                });
            });
        };

        $scope.loadEntries = function() {
            if (!isLoading) {
                return;
            }
            isLoading = false;

            $http.get("/api/form/" + $scope.formId + "/entries", { cache: false }).success(function(data) {
                $scope.entries = data;

                _.each($scope.entries, function(e) {
                    var pairs = _.pairs(e.values);
                    _.each(pairs, function (p) {
                        var k = p[0];
                        var v = p[1];
                        e[k] = v;
                        if (!_.find($scope.columnFields, function (f) { return f === k; })) {
                            $scope.columnFields.push(k);
                        }
                    });
                });

                $scope.entriesLoaded = true;
            });
        };
    }]);

    angular.module("app").controller("FormTemplateController", ["$scope", "$http", "$log", "toastService", "toastr", "$timeout", "$translate", "$modal", function ($scope, $http, $log, toastService, toastr, $timeout, $translate, $modal) {
        var isLoading = true;
        $scope.templateLoaded = false;
        $scope.loadTemplate = function() {
            if (!isLoading) {
                return;
            }
            isLoading = false;

            $http.get("/api/form/" + $scope.formId + "/template", { cache: false }).success(function (template) {
                $scope.form.template = template;

                $scope.templateLoaded = true;
            });
        };

        var successMsg = "";
        var errorMsg = "";
        var generateFormTemplateError = "";

        $translate(["save_form_template_success", "save_form_template_error", "generate_form_template_error"]).then(function (t) {
            successMsg = t.save_form_template_success;
            errorMsg = t.save_form_template_error;
            generateFormTemplateError = t.generate_form_template_error;
        });

        $scope.openGenerator = function() {
            var url = String.format("/api/form/{0}/templategenerator", $scope.formId);

            $http.get(url, { cache: false }).success(function (generator) {

                var modalInstance = $modal.open({
                    backdrop: 'static',
                    templateUrl: "/admin/views/forms/editFormTemplate.min.html",
                    controller: "FormTemplateGeneratorController",
                    resolve: {
                        generator: function () { return generator; },
                    }
                });

                modalInstance.result.then(function (g) {
                    $http.post(url, g).success(function(html) {
                        $scope.form.template = html;
                    }).error(function(error) {
                        toastService.error(generateFormTemplateError, error);
                    });
                });
            });
        };

        $scope.saveTemplate = function() {
            var dto = {
                template: $scope.form.template,
            };
            return $http.post("/api/form/" + $scope.formId + "/template", dto).success(function () {
                toastr.success('', successMsg, { allowHtml: false, timeOut: 1000 });
            }).error(function (error) {
                $log.error(angular.toJson(error));
                toastService.error(errorMsg, error);
            });
        };
    }]);

    angular.module("app").controller("FormTemplateGeneratorController", ["$scope", "$modalInstance", "$http", "$log", "generator", function($scope, $modalInstance, $http, $log, generator) {
        $scope.generator = generator;

        $scope.ok = function () {
            $modalInstance.close($scope.generator);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);

    angular.module("app").controller("FormActionsController", ["$scope", "$http", "$log", "$modal", "toastService", "dialogService", "$translate", function ($scope, $http, $log, $modal, toastService, dialogService, $translate) {
        var isLoading = true;
        $scope.actionLoaded = false;
        $scope.loadActions = function () {
            if (!isLoading) {
                return;
            }
            isLoading = false;

            $http.get("/api/form/" + $scope.formId + "/actions", { cache: false }).success(function(data) {
                $scope.actions = data;
                _.each($scope.actions, setDescriptionForAction);

                $scope.actionLoaded = true;
            });
        };

        $scope.selectAction = function(action) {
            $scope.selectedAction = action;
        };

        var createAction = "";
        var errorCreateAction = "";
        var editAction = "";
        var errorEditAction = "";
        var deleteAction = "";
        var confirmDeleteAction = "";
        var errorDeleteAction = "";
        var typeUndefined = "";
        var descEmailToAdd = "";
        var descEmailToField = "";
        var descPersist = "";
        var descForward = "";
        var descCreateObject = "";

        $translate(["create_form_action", "error_create_form_action", "edit_form_action", "error_edit_form_action", "delete_form_action", "confirm_delete_form_action", "error_delete_form_action", "form_action_type_undefined", "form_action_description_send_email_to_address", "form_action_description_send_email_to_field", "form_action_description_persist", "form_action_description_forward", "form_action_description_create_object"]).then(function(t) {
            createAction = t.create_form_action;
            errorCreateAction = t.error_create_form_action;
            editAction = t.edit_form_action;
            errorEditAction = t.error_edit_form_action;
            deleteAction = t.delete_form_action;
            confirmDeleteAction = t.confirm_delete_form_action;
            errorDeleteAction = t.error_delete_form_action;
            typeUndefined = t.form_action_type_undefined;
            descEmailToAdd = t.form_action_description_send_email_to_address;
            descEmailToField = t.form_action_description_send_email_to_field;
            descPersist = t.form_action_description_persist;
            descForward = t.form_action_description_forward;
            descCreateObject = t.form_action_description_create_object;
        });

        $scope.addAction = function () {
            var action = { type: "", configuration: null };
            openActionDialog(createAction, action, function(a) {
                $http.post("/api/form/" + $scope.form.id + "/action", a).success(function (data) {
                    setDescriptionForAction(data);
                    $scope.actions.push(data);
                }).error(function(error) {
                    toastService.error(errorCreateAction, error);
                });
            });
        };

        $scope.editAction = function (action) {
            openActionDialog(editAction, action, function(a) {
                $http.post("/api/form/" + $scope.form.id + "/action/" + action.id, a).success(function() {
                    action.type = a.type;
                    action.configuration = a.configuration;
                    setDescriptionForAction(action);
                }).error(function (error) {
                    toastService.error(errorEditAction, error);
                });
            });
        };

        $scope.removeAction = function(action) {
            dialogService.confirmDialog(deleteAction, confirmDeleteAction, function() {
                $http.delete("/api/form/" + $scope.form.id + "/action/" + action.id).success(function() {
                    $scope.actions = _.reject($scope.actions, function(a) { return a.id === action.id; });
                }).error(function (error) {
                    toastService.error(errorDeleteAction, error);
                });
            });
        };

        function openActionDialog(subject, action, onAccept) {
            var copy = angular.copy(action);
            var modalInstance = $modal.open({
                backdrop: 'static',
                templateUrl: "/admin/views/forms/editFormActionDialog.min.html",
                controller: "EditFormActionDialogController",
                size: 'lg',
                resolve: {
                    action: function () { return copy; },
                    subject: function () { return subject; },
                    fields: function () { return $scope.form.fields; },
                }
            });

            modalInstance.result.then(onAccept);
        };

        function setDescriptionForAction(action) {
            if (!action.type) {
                action.description = typeUndefined;
                return;
            }

            try {
                if (action.type === "SendEmailToAddress") {
                    action.description = String.format(descEmailToAdd, action.configuration.emailAddress);
                } else if (action.type === "Persist") {
                    action.description = descPersist;
                } else if (action.type === "SendEmailToField") {
                    action.description = String.format(descEmailToField, action.configuration.fieldName);
                } else if (action.type === "PageForwarding") {
                    action.description = String.format(descForward, action.configuration.fullname);
                } else if (action.type === "CreateObject") {
                    action.description = descCreateObject;
                } else {
                    action.description = action.type + " is not supported";
                }
            } catch (e) {
                action.description = "Error while generating description.";
                $log.error(e);
            } 
        };
    }]);

    angular.module("app").controller("FormFieldsController", ["$scope", "$http", "$modal", "toastService", "dialogService", "$translate", function ($scope, $http, $modal, toastService, dialogService, $translate) {
        $scope.selectedField = function(field) {
            $scope.selectedField = field;
        };

        var tName = "";
        var createField = "";
        var errorCreateField = "";
        var renameField = "";
        var errorRenameField = "";
        var deleteField = "";
        var confirmDeleteField = "";
        var errorDeleteField = "";

        $translate(["name", "create_form_field", "error_create_form_field", "rename_form_field", "error_rename_form_field", "delete_form_field", "confirm_delete_form_field", "error_delete_form_field"]).then(function (t) {
            tName = t.name;
            createField = t.create_form_field;
            errorCreateField = t.error_create_form_field;
            renameField = t.rename_form_field;
            errorRenameField = t.error_rename_form_field;
            deleteField = t.delete_form_field;
            confirmDeleteField = t.confirm_delete_form_field;
            errorDeleteField = t.error_delete_form_field;
        });

        $scope.addField = function() {
            dialogService.singleInputDialog(createField, tName, "", function (name) {
                var field = { name: name };
                $http.post("/api/form/" + $scope.form.id + "/fields", field).success(function (data) {
                    $scope.form.fields.push(data);
                }).error(function(error) {
                    toastService.error(errorCreateField, error);
                });
            });
        };

        $scope.editField = function (field) {
            dialogService.singleInputDialog(renameField, tName, field.name, function (name) {
                var f = { id: field.id, name: name };
                $http.post("/api/form/" + $scope.form.id + "/field/" + field.id, f).success(function () {
                    field.name = name;
                }).error(function (error) {
                    toastService.error(errorRenameField, error);
                });
            });
        };

        $scope.removeField = function (field) {
            dialogService.confirmDialog(deleteField, String.format(confirmDeleteField, field.name), function () {
                $http.delete("/api/form/" + $scope.form.id + "/field/" + field.id).success(function () {
                    $scope.form.fields = _.reject($scope.form.fields, function (f) { return f.id === field.id; });
                }).error(function (error) {
                    toastService.error(errorDeleteField, error);
                });
            });
        };
    }]);

    angular.module("app").controller("EditFormActionDialogController", ["$scope", "$modalInstance", "$http", "$log", "tenantService", "$translate", "subject", "action", "dialogService", "fields", function ($scope, $modalInstance, $http, $log, tenantService, $translate, subject, action, dialogService, fields) {
        $scope.action = action;
        $scope.subject = subject;
        $scope.fields = fields;

        $scope.tinymceOptions = {
            theme: "modern",
            relative_urls: true,
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code hr wordcount textcolor colorpicker",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "undo redo | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media"
        };

        $scope.$watch("action.type", function (newValue, oldValue) {
            if ($scope.action.type.toUpperCase() === "PERSIST") {
                $scope.action.configuration = {};
                return;
            }

            if (!oldValue || newValue === oldValue) {
                return;
            }

            $scope.action.configuration = null;
        });

        $scope.$watch("action.configuration.fieldId", function() {
            if (action && action.configuration && action.configuration.fieldId) {
                var field = _.find($scope.fields, function (f) { return f.id === action.configuration.fieldId; });
                if (field) {
                    action.configuration.fieldName = field.name;
                }
            }
        });

        $translate(["form_action_send_email_to_address", "form_action_send_email_to_field", "form_action_persist", "form_action_forward", "form_action_create_object"]).then(function(t) {
            $scope.types = [
                { name: "SendEmailToField", description: t.form_action_send_email_to_field },
                { name: "SendEmailToAddress", description: t.form_action_send_email_to_address },
                { name: "PageForwarding", description: t.form_action_forward },
                { name: "CreateObject", description: t.form_action_create_object },
                { name: "Persist", description: t.form_action_persist }
            ];
        });

        $scope.getPages = function (value) {
            var url = String.format("/api/page/search/{0}?search={1}", tenantService.getTenant().id, value);
            return $http.get(url, { cache: false}).then(function (response) {
                return response.data;
            });
        };

        $scope.browsePage = function () {
            var p = action.configuration;
            dialogService.selectPageDialog(p, function (page) {
                if (!action.configuration) {
                    action.configuration = {};
                }
                if (!page) {
                    return;
                }
                page.children = [];
                action.configuration = page;
            });
        };

        $scope.definitions = [];

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id, { cache: false }).success(function (data) {
            $scope.definitions = data;
        });

        $scope.$watch("action.configuration.objectDefinitionId", function() {
            if (action && action.configuration && action.configuration.objectDefinitionId) {
                var definition = _.find($scope.definitions, function (d) { return d.id === action.configuration.objectDefinitionId; });
                if (definition) {
                    var url = String.format("/api/objectdefinition/{1}/{0}/detail", definition.id, tenantService.getTenant().id);

                    action.configuration.objectDefinitionName = definition.name;

                    $http.get(url, { cache: false }).success(function(data) {
                        $scope.definition = data;
                    });
                }
            } else {
                $scope.definition = {};
            }
        });

        $scope.ok = function () {
            if (!$scope.action.configuration) {
                $scope.action.configuration = {};
            }
            $modalInstance.close($scope.action);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);

    angular.module("app").controller("EditFormEntryDialogController", ["$scope", "$modalInstance", "$http", "subject", "entry", "fields", function($scope, $modalInstance, $http, subject, entry, fields) {
        $scope.subject = subject;
        $scope.entry = entry;
        $scope.fields = fields;

        $scope.values = {};

        _.each(fields, function (f) {
            _.each(_.pairs(entry.values), function(pair) {
                var key = pair[0];
                var value = pair[1];

                if (f.toUpperCase() == key.toUpperCase()) {
                    $scope.values[f] = value;
                }
            });
        });

        $scope.ok = function () {
            $modalInstance.close($scope.values);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
}());