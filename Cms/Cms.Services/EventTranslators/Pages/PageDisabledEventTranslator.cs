using System;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Pages.Events;
using Cms.Aggregates.Websites;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Pages
{
    internal class PageDisabledEventTranslator : IEventTranslator
    {
        private readonly IPageRepository _repository;
        private readonly IWebsiteRepository _websiteRepository;

        public PageDisabledEventTranslator(IPageRepository repository, IWebsiteRepository websiteRepository)
        {
            _repository = repository;
            _websiteRepository = websiteRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(PageDisabled);
        }

        public string GetHeader(IEvent @event)
        {
            return "Seite deaktiviert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (PageDisabled)@event;
            var page = _repository.GetEvenIfDeleted(e.AggregateId);
            return string.Format("Die Seite <strong>{0}</strong> wurde <strong>deaktiviert</strong>.", page.Fullname);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var page = _repository.GetEvenIfDeleted(@event.AggregateId);
            return _websiteRepository.GetEvenIfDeleted(page.WebsiteId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "page";
        }
    }
}