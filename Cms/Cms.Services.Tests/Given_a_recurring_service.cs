﻿using Cms.Services.RecurrentJobs;
using Xunit;

namespace Cms.Services.Tests
{
    public class Given_a_recurring_service
    {
        [Fact(Skip = "This is a tool, not a test.")]
        public void Run_job_to_move_logfiles_to_zipfile()
        {
            var service = new MoveLogfilesToZipfile();

            service.Execute();
        }
    }
}
