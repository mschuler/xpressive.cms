﻿namespace Cms.Aggregates.DynamicObjects.Queries
{
    public class GetObjectFilter
    {
        public string FieldName { get; set; }
        
        public string Operator { get; set; }

        public string Value { get; set; }
    }
}