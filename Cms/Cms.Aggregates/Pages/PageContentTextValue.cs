namespace Cms.Aggregates.Pages
{
    public class PageContentTextValue : PageContentValueBase
    {
        public string Value { get; set; }
    }
}