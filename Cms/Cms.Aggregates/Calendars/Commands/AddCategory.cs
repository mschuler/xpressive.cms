using Cms.Aggregates.Security;

namespace Cms.Aggregates.Calendars.Commands
{
    [RequiredPermission("calendarcategory", PermissionLevel.CanChange)]
    public class AddCategory : CommandBase
    {
        public string Name { get; set; }
    }
}