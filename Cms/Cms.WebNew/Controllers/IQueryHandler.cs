﻿using System.Collections.Generic;
using System.Net.Http;
using Cms.EventSourcing.Contracts.Queries;

namespace Cms.WebNew.Controllers
{
    public interface IQueryHandler
    {
        IEnumerable<object> Handle(IQuery query, HttpRequestMessage request);
    }
}