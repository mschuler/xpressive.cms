using System;
using System.Globalization;
using Cms.SharedKernel;

namespace Cms.Services.ObjectFieldContentTypeConverter
{
    internal class ObjectFieldDateConverter : IObjectFieldContentTypeConverter
    {
        public bool CanHandle(string type)
        {
            return type.Equals("date", StringComparison.OrdinalIgnoreCase);
        }

        public string Serialize(string json)
        {
            DateTime value;
            var culture = CultureInfo.GetCultureInfo("de-CH");
            if (!string.IsNullOrEmpty(json) && DateTime.TryParse(json, culture, DateTimeStyles.AssumeLocal, out value))
            {
                value = value.ToUniversalTime();
                return value.ToString("O", CultureInfo.InvariantCulture);
            }
            return string.Empty;
        }

        public object Deserialize(string value)
        {
            DateTime date;
            if (DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out date))
            {
                var culture = CultureInfo.GetCultureInfo("de-CH");
                return date.ToString("G", culture);
            }
            return string.Empty;
        }

        public SortableObjectField DeserializeForSortableField(string value)
        {
            DateTime date;
            if (DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out date))
            {
                var culture = CultureInfo.GetCultureInfo("de-CH");
                return new SortableObjectField
                {
                    VisibleValue = date.ToString("G", culture),
                    SortableValue = date.ToString("s", culture),
                };
            }
            return new SortableObjectField();
        }

        public object DeserializeForPublicApi(string value)
        {
            DateTime date;
            if (DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out date))
            {
                return date;
            }
            return null;
        }

        public bool TryGetMetadataForPublicApi(string value, out object metadata)
        {
            metadata = null;
            return false;
        }

        public object DeserializeForInterpreter(string value)
        {
            DateTime date;
            if (DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out date))
            {
                return date;
            }
            return null;
        }

        public string SerializeForImport(string json, string format, string options)
        {
            DateTime date;
            if (TryGetDate(json, format, out date))
            {
                date = date.ToUniversalTime();
                return date.ToString("O", CultureInfo.InvariantCulture);
            }
            return string.Empty;
        }

        public string SerializeForImportPreview(string json, string format, string options)
        {
            DateTime date;
            if (TryGetDate(json, format, out date))
            {
                return date.ToString("dd.MM.yyyy HH:mm");
            }
            return json;
        }

        public bool IsValidValueForImport(string json, string format, string options)
        {
            DateTime date;
            return TryGetDate(json, format, out date);
        }

        private static bool TryGetDate(string value, string format, out DateTime date)
        {
            date = DateTime.MinValue;
            try
            {
                var d = GetDate(value, format);
                if (d.HasValue)
                {
                    date = d.Value;
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        private static DateTime? GetDate(string value, string format)
        {
            if (string.IsNullOrEmpty(format) || string.IsNullOrEmpty(value))
            {
                return null;
            }

            var formatInfo = new DateTimeFormatInfo();
            var formatParts = format.Split(' ');
            formatInfo.ShortDatePattern = formatParts[0];

            if (formatParts.Length > 1)
            {
                formatInfo.ShortTimePattern = formatParts[1];
            }

            DateTime dateTime;
            if (DateTime.TryParse(value, formatInfo, DateTimeStyles.AssumeLocal, out dateTime))
            {
                return dateTime;
            }

            return null;
        }
    }
}