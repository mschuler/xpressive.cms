﻿namespace Cms.Aggregates.Settings.Events
{
    public class EmailSettingsChanged : EventBase<Settings>
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string EmailAddress { get; set; }
        public string ResponseAddress { get; set; }
        public bool IsSmtpAuthenticationRequired { get; set; }
        public bool IsSslUsedForAuthentication { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
    }
}
