﻿namespace Cms.Aggregates.Websites.Events
{
    public class DefaultPageSet : EventBase<Website>
    {
        public ulong PageId { get; set; }
    }
}