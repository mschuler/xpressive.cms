using System;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.FileDirectories.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.FileDirectories
{
    internal class FileDirectoryParentChangedEventTranslator : IEventTranslator
    {
        private readonly IRepository<FileDirectory> _repository;

        public FileDirectoryParentChangedEventTranslator(IRepository<FileDirectory> repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FileDirectoryParentChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Dateiverzeichnis verschoben";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FileDirectoryParentChanged)@event;
            var directory = _repository.GetEvenIfDeleted(e.AggregateId);
            var parent = e.ParentId > 0 ? _repository.GetEvenIfDeleted(e.ParentId).Name : "ROOT";
            return string.Format("Das Dateiverzeichnis <strong>{0}</strong> wurde nach <strong>{1}</strong> verschoben.", directory.Name, parent);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "file";
        }
    }
}