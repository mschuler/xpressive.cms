using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Forms.Queries
{
    internal class FormQueryHandler : QueryHandlerBase
    {
        private readonly IFormRepository _formRepository;

        public FormQueryHandler(IFormRepository formRepository)
        {
            _formRepository = formRepository;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetAllForms),
                typeof (GetFormDetail),
                typeof (GetFormActions),
                typeof (GetFormFields),
                typeof (GetFormEntries),
                typeof (GetFormTemplate),
                typeof (GetFormGenerator),
                typeof (GetFormsBySearchPattern)
            };
        }

        public IEnumerable<object> When(GetAllForms query)
        {
            var forms = _formRepository.GetAll(query.TenantId);

            foreach (var form in forms)
            {
                yield return form.ToDto();
            }
        }

        public IEnumerable<object> When(GetFormsBySearchPattern query)
        {
            var forms = _formRepository.GetAll(query.TenantId);

            foreach (var form in forms)
            {
                if (query.IsMatch(form))
                {
                    yield return form.ToDto();
                }
            }
        }

        public IEnumerable<object> When(GetFormDetail query)
        {
            var form = _formRepository.Get(query.FormId);
            yield return form.ToDto();
        }

        public IEnumerable<object> When(GetFormActions query)
        {
            var form = _formRepository.Get(query.FormId);

            foreach (var action in form.Actions)
            {
                yield return new
                {
                    Id = action.Id.ToJsonString(),
                    Type = Enum.GetName(typeof(FormActionType), action.Type),
                    action.Configuration,
                };
            }
        }

        public IEnumerable<object> When(GetFormFields query)
        {
            var form = _formRepository.Get(query.FormId);

            foreach (var field in form.Fields)
            {
                yield return new
                {
                    Id = field.Id.ToJsonString(),
                    field.Name,
                };
            }
        }

        public IEnumerable<object> When(GetFormEntries query)
        {
            var form = _formRepository.Get(query.FormId);

            foreach (var entry in form.Entries.OrderByDescending(e => e.Timestamp))
            {
                yield return new
                {
                    Id = entry.Id.ToJsonString(),
                    entry.Timestamp,
                    entry.IpAddress,
                    entry.IsSpam,
                    Values = new Dictionary<string, string>(entry.Values),
                };
            }
        }

        public IEnumerable<object> When(GetFormTemplate query)
        {
            var form = _formRepository.Get(query.FormId);
            yield return form.Template;
        }

        public IEnumerable<object> When(GetFormGenerator query)
        {
            var form = _formRepository.Get(query.FormId);

            var fields = new List<FormGeneratorFieldDto>();

            foreach (var formField in form.Fields)
            {
                var generatorField = form.GeneratorFields.SingleOrDefault(f => f.FieldId == formField.Id);

                var dto = new FormGeneratorFieldDto();
                fields.Add(dto);

                dto.FieldName = formField.Name;
                dto.FieldId = formField.Id.ToJsonString();
                dto.Type = "text";
                dto.SortOrder = 1000;
                dto.Options = string.Empty;

                if (generatorField != null)
                {
                    dto.SortOrder = generatorField.SortOrder;
                    dto.Type = generatorField.Type;
                    dto.Options = string.Join(Environment.NewLine, generatorField.Options);
                    dto.Multiple = generatorField.IsMultiselect;
                    dto.Required = generatorField.IsRequired;
                }
            }

            yield return new
            {
                Type = form.GeneratorLabelPosition,
                Fields = fields.OrderBy(f => f.SortOrder).ThenBy(f => f.FieldName).ToList(),
            };
        }

        public class FormEntryDto
        {
            public string Id { get; set; }
            public DateTime Timestamp { get; set; }
            public string IpAddress { get; set; }
            public bool IsSpam { get; set; }
            public Dictionary<string, string> Values { get; set; }
        }

        public class FormGeneratorFieldDto
        {
            public string FieldId { get; set; }
            public string FieldName { get; set; }
            public int SortOrder { get; set; }
            public string Type { get; set; }
            public string Options { get; set; }
            public bool Multiple { get; set; }
            public bool Required { get; set; }
        }
    }
}