namespace Cms.Aggregates.Pages
{
    internal class PageField : IPageField
    {
        public PageField()
        {
            CanChangeType = true;
        }

        public string Name { get; set; }
        public string Type { get; set; }
        public int SortOrder { get; set; }
        public bool CanChangeType { get; set; }
    }
}