﻿using System;
using System.Text;
using Cms.Aggregates;
using Cms.Aggregates.Files;
using Cms.Aggregates.Pages;
using Cms.Services.Contracts;

namespace Cms.Services.PageContentHandlers
{
    internal class ContentImageHandler : IContentHandler
    {
        private readonly IFileRepository _fileRepository;
        private readonly IPageRepository _pageRepository;
        private readonly IPageUrlService _urlService;

        public ContentImageHandler(IFileRepository fileRepository, IPageRepository pageRepository, IPageUrlService urlService)
        {
            _fileRepository = fileRepository;
            _pageRepository = pageRepository;
            _urlService = urlService;
        }

        public bool CanHandle(string pageContentType)
        {
            return pageContentType.Equals("image", StringComparison.OrdinalIgnoreCase);
        }

        public string ContentType
        {
            get { return "Image"; }
        }

        public string GetImage()
        {
            return "fa fa-picture-o";
        }

        public string Render(PageContentValueBase value)
        {
            var imageValue = value as PageContentImageValue;

            if (imageValue == null)
            {
                return string.Empty;
            }

            var file = GetFile(imageValue);

            if (file == null)
            {
                return string.Empty;
            }

            var link = GetHyperlink(imageValue);
            var url = GetFileUrl(file, imageValue);

            var result = new StringBuilder();

            if (!string.IsNullOrEmpty(link))
            {
                result.AppendFormat("<a href=\"{0}\"", link);

                if ("external".Equals(imageValue.LinkType, StringComparison.OrdinalIgnoreCase))
                {
                    result.Append(" target=\"_blank\"");
                }

                result.Append(">");
            }

            result.AppendFormat("<img src=\"{0}\" alt=\"{1}\"/>", url, file.Name);

            if (!string.IsNullOrEmpty(link))
            {
                result.Append("</a>");
            }

            return result.ToString();
        }

        public object GetValueForInterpreter(PageContentValueBase value)
        {
            var imageValue = value as PageContentImageValue;

            if (imageValue == null)
            {
                return string.Empty;
            }

            return GetFile(imageValue);
        }

        private string GetFileUrl(File file, PageContentImageValue value)
        {
            var dto = new ImageSettingsDto();

            if (value.Alpha != 100) { dto.Alpha = value.Alpha; }
            if (value.Saturation != 0) { dto.Saturation = value.Saturation; }
            if (value.Brightness != 0) { dto.Brightness = value.Brightness; }
            if (value.RoundedCorners != 0) { dto.RoundedCorners = value.RoundedCorners; }
            if (value.Contrast != 0) { dto.Contrast = value.Contrast; }
            if (!string.IsNullOrEmpty(value.Tint)) { dto.Tint = value.Tint; }
            if (!string.IsNullOrEmpty(value.Filter)) { dto.Filter = value.Filter; }

            return file.ToDownloadUrl(true, dto);
        }

        private File GetFile(PageContentImageValue value)
        {
            ulong fileId;
            File file;

            if (ulong.TryParse(value.FileId, out fileId) && _fileRepository.TryGet(fileId, out file))
            {
                return file;
            }

            return null;
        }

        private string GetHyperlink(PageContentImageValue value)
        {
            if ("page".Equals(value.LinkType, StringComparison.OrdinalIgnoreCase))
            {
                ulong pageId;
                PageBase page;

                if (ulong.TryParse(value.PageId, out pageId) && _pageRepository.TryGet(pageId, out page))
                {
                    return _urlService.GetUrl(page);
                }
            }
            if ("external".Equals(value.LinkType, StringComparison.OrdinalIgnoreCase))
            {
                return value.ExternalLink;
            }

            return null;
        }
    }
}