﻿using System;
using Cms.EventSourcing.Contracts.Events;

namespace Cms.Aggregates
{
    public abstract class EventBase<T> : IEvent
    {
        protected EventBase()
        {
            Date = DateTime.UtcNow;
        }

        public ulong UserId { get; set; }

        public ulong Version { get; set; }

        public ulong AggregateId { get; set; }

        public DateTime Date { get; set; }

        Type IEvent.AggregateType { get { return typeof(T); } }
    }
}