﻿using System;
using Cms.Aggregates.Tenants;
using Cms.Aggregates.Websites;
using Cms.SharedKernel;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    public class TenantInterpreter : IInterpreter
    {
        private readonly ITenantRepository _repository;

        public TenantInterpreter(ITenantRepository repository)
        {
            _repository = repository;
        }

        public int Priority { get; set; }

        public string Name
        {
            get { return "Tenant"; }
        }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function)
            {
                if (tree.Value.Equals("tenant", StringComparison.OrdinalIgnoreCase))
                {
                    if (tree.Children.Count == 1)
                    {
                        var child = tree.Children[0];
                        if (child.ItemType == TreeItemType.Function)
                        {
                            if (child.Value.Equals("this", StringComparison.OrdinalIgnoreCase))
                            {
                                var website = TemplateStateMap.Get<Website>("CurrentWebsite");
                                tree.Result = _repository.Get(website.TenantId);
                            }
                        }
                    }
                }
            }
            else if (tree.ItemType == TreeItemType.Attribute && tree.LeftItem != null)
            {
                var tenant = tree.LeftItem.Result as Tenant;
                if (tenant != null)
                {
                    switch (tree.Value.ToLowerInvariant())
                    {
                        case "name":
                            tree.Result = tenant.Name;
                            break;
                        case "normalizedname":
                            tree.Result = tenant.Name.ToValidUrlPart();
                            break;
                    }
                }
            }
        }
    }
}