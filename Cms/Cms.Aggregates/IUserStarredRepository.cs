using System.Collections.Generic;

namespace Cms.Aggregates
{
    public interface IUserStarredRepository<T> where T : IAggregate
    {
        ICollection<T> Get(ulong userId);

        void Add(ulong userId, ulong aggregateId);

        void Remove(ulong userId, ulong aggregateId);

        bool IsStarred(ulong userId, ulong aggregateId);
    }
}