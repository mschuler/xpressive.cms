﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Queries
{
    [RequiredPermission("objecttemplate", PermissionLevel.CanSee)]
    public class GetObjectTemplate : QueryBase
    {
        public ulong ObjectDefinitionId { get; set; }
        public ulong TemplateId { get; set; }
    }
}