﻿using System;
using System.Collections.Generic;
using Cms.EventSourcing.Contracts.Events;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Cms.EventSourcing.Services
{
    public sealed class JsonEventSerializer : IEventSerializer
    {
        private static readonly Lazy<JsonSerializerSettings> _serializerSettings = new Lazy<JsonSerializerSettings>(() => new JsonSerializerSettings
        {
            ContractResolver = new ContractResolver(),
            TypeNameHandling = TypeNameHandling.Auto,
        });

        public string Serialize(IEvent @event)
        {
            return JsonConvert.SerializeObject(@event, @event.GetType(), Formatting.None, _serializerSettings.Value);
        }

        public IEvent Deserialize(string data, Type type)
        {
            return (IEvent)JsonConvert.DeserializeObject(data, type, _serializerSettings.Value);
        }
    }

    internal class ContractResolver : DefaultContractResolver
    {
        private static readonly HashSet<string> _ignoredProperties = new HashSet<string>(StringComparer.Ordinal)
        {
            "UserId", "Version", "AggregateId",
        };

        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            var list = base.CreateProperties(type, memberSerialization);

            foreach (var prop in list)
            {
                if (prop.Ignored && prop.Writable)
                {
                    prop.Ignored = false;
                }
                if (_ignoredProperties.Contains(prop.PropertyName))
                {
                    prop.Ignored = true;
                }
            }

            return list;
        }
    }
}