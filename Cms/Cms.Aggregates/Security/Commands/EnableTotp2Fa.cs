using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission(false)]
    public class EnableTotp2Fa : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Password { get; set; }

        [Required]
        public byte[] TotpSecret { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(6), MaxLength(6)]
        public string TotpCode { get; set; }
    }
}