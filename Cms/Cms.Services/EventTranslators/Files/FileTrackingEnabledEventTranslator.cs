using System;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.Files;
using Cms.Aggregates.Files.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Files
{
    internal class FileTrackingEnabledEventTranslator : IEventTranslator
    {
        private readonly IFileRepository _repository;
        private readonly IRepository<FileDirectory> _fileDirectoryRepository;

        public FileTrackingEnabledEventTranslator(IFileRepository repository, IRepository<FileDirectory> fileDirectoryRepository)
        {
            _repository = repository;
            _fileDirectoryRepository = fileDirectoryRepository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FileTrackingEnabled);
        }

        public string GetHeader(IEvent @event)
        {
            return "Dateiaufruf-Verfolgung aktiviert";
        }

        public string GetContent(IEvent @event)
        {
            var file = _repository.GetEvenIfDeleted(@event.AggregateId);
            return string.Format("Die Dateiaufruf-Verfolgung f�r die Datei <strong>{0}</strong> wurde <strong>aktiviert</strong>.", file.Fullname);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            var file = _repository.GetEvenIfDeleted(@event.AggregateId);
            return _fileDirectoryRepository.GetEvenIfDeleted(file.FileDirectoryId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "file";
        }
    }
}