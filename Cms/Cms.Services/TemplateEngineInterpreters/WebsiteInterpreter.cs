﻿using System;
using Cms.Aggregates.Pages;
using Cms.Aggregates.Websites;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class WebsiteInterpreter : IInterpreter
    {
        private readonly IPageRepository _pageRepository;

        public WebsiteInterpreter(IPageRepository pageRepository)
        {
            _pageRepository = pageRepository;
        }

        public int Priority { get; set; }

        public string Name
        {
            get { return "Website"; }
        }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function)
            {
                if (tree.Value.Equals("website", StringComparison.OrdinalIgnoreCase))
                {
                    if (tree.Children.Count == 1)
                    {
                        var child = tree.Children[0];
                        if (child.ItemType == TreeItemType.Function)
                        {
                            if (child.Value.Equals("this", StringComparison.OrdinalIgnoreCase))
                            {
                                tree.Result = TemplateStateMap.Get<Website>("CurrentWebsite");
                            }
                        }
                    }
                }
            }
            else if (tree.ItemType == TreeItemType.Attribute && tree.LeftItem != null)
            {
                var website = tree.LeftItem.Result as Website;
                if (website != null)
                {
                    switch (tree.Value.ToLowerInvariant())
                    {
                        case "title":
                        case "name":
                            tree.Result = website.Name;
                            break;
                        case "defaultpage":
                            PageBase defaultPage;
                            _pageRepository.TryGet(website.DefaultPageId, out defaultPage);
                            tree.Result = defaultPage;
                            break;
                        case "id":
                            tree.Result = website.PublicId;
                            break;
                        case "host":
                            tree.Result = website.DefaultDomain != null ? website.DefaultDomain.Host : null;
                            break;
                    }
                }
            }
        }
    }
}
