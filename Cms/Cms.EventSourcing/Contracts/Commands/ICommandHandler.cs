﻿using System;
using Cms.EventSourcing.Contracts.Events;

namespace Cms.EventSourcing.Contracts.Commands
{
    public interface ICommandHandler
    {
        void Handle(ICommand command, Action<IEvent> eventHandler);
    }
}
