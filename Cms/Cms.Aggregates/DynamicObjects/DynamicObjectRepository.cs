using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.DynamicObjects
{
    internal class DynamicObjectRepository : RepositoryBase<DynamicObject>, IDynamicObjectRepository
    {
        private static readonly object _lock = new object();
        private static int _currentMaxPublicId;

        public int GetNextPublicId()
        {
            lock (_lock)
            {
                if (_currentMaxPublicId == 0)
                {
                    var allObjects = GetAll().ToList();
                    _currentMaxPublicId = allObjects.Count == 0 ? 0 : allObjects.Max(o => o.PublicId);
                }

                _currentMaxPublicId++;
                return _currentMaxPublicId;
            }
        }

        public DynamicObject GetByPublicId(int publicId)
        {
            return GetAll().SingleOrDefault(o => o.PublicId.Equals(publicId));
        }

        public IEnumerable<DynamicObject> GetAll(ulong tenantId, ulong definitionId)
        {
            return GetAll()
                .ToList()
                .Where(o => o.DefinitionId == definitionId)
                .Where(o => IsVisibleForTenant(o, tenantId));
        }

        public override void DeleteAllByTenantId(ulong tenantId)
        {
            throw new NotSupportedException("DynamicObjectDefinitionRepository is deleting these files.");
        }

        private bool IsVisibleForTenant(DynamicObject dynamicObject, ulong tenantId)
        {
            if (dynamicObject.TenantId == tenantId)
            {
                return true;
            }

            return dynamicObject.SharedForTenants.Contains(tenantId);
        }
    }
}