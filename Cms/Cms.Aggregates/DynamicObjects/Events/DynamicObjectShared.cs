using System.Collections.Generic;

namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectShared : EventBase<DynamicObject>
    {
        public IList<ulong> TenantIds { get; set; }
    }
}