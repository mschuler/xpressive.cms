using System.ComponentModel.DataAnnotations;

namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("user", PermissionLevel.CanChange)]
    public class ChangeUserName : CommandBase
    {
        [Required(AllowEmptyStrings = true)]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string LastName { get; set; }
    }
}