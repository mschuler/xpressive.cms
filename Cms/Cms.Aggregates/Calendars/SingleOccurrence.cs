using System;

namespace Cms.Aggregates.Calendars
{
    public class SingleOccurrence : AppointmentRecurrence
    {
        public override bool IsMatch(Appointment appointment, DateTime dateTime)
        {
            return appointment.StartDate.Date <= dateTime.Date &&
                   appointment.EndDate.Date >= dateTime.Date;
        }
    }
}