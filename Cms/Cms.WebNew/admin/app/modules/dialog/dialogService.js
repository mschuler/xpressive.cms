﻿(function() {
    angular.module("app").factory("dialogService", ["$modal", function ($modal) {
        var instance = {};

        var singleInputDialog = function (subject, label, value, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/defaultInputDialog.min.html",
                controller: "SingleInputDialogController",
                resolve: {
                    subject: function () { return subject; },
                    value: function () { return value; },
                    label: function () { return label; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var twoInputsDialog = function (subject, labelOne, valueOne, labelTwo, valueTwo, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/twoInputsDialog.min.html",
                controller: "TwoInputsDialogController",
                resolve: {
                    subject: function () { return subject; },
                    valueOne: function () { return valueOne; },
                    labelOne: function () { return labelOne; },
                    valueTwo: function () { return valueTwo; },
                    labelTwo: function () { return labelTwo; }
                }
            }).result.then(function (result) {
                onAccept(result.valueOne, result.valueTwo);
            });
        };

        var singlePasswordDialog = function (subject, label, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/singlePasswordDialog.min.html",
                controller: "SinglePasswordDialogController",
                resolve: {
                    subject: function () { return subject; },
                    label: function () { return label; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var confirmDialog = function (subject, label, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/defaultConfirmDialog.min.html",
                controller: "ConfirmDialogController",
                resolve: {
                    subject: function () { return subject; },
                    label: function () { return label; }
                }
            }).result.then(function () {
                onAccept();
            });
        };

        var messageDialog = function (subject, label) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/messageDialog.min.html",
                controller: "MessageDialogController",
                resolve: {
                    subject: function () { return subject; },
                    label: function () { return label; }
                }
            });
        };

        var fileUploadDialog = function (subject, directoryId, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/fileUploadDialog.min.html",
                controller: "FileUploadDialogController",
                resolve: {
                    subject: function () { return subject; },
                    uploadUrl: function () { return "/api/file/new/" + directoryId; },
                    maxFileCount: function() { return 10000; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var fileReplaceDialog = function (subject, fileId, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/fileUploadDialog.min.html",
                controller: "FileUploadDialogController",
                resolve: {
                    subject: function () { return subject; },
                    uploadUrl: function () { return "/api/file/replace/" + fileId; },
                    maxFileCount: function () { return 1; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var selectPageDialog = function(page, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectPageDialog.min.html",
                controller: "SelectPageDialogController",
                resolve: {
                    page: function () { return page; }
                }
            }).result.then(function(result) {
                onAccept(result);
            });
        };

        var selectFileDialog = function (file, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectFileDialog.min.html",
                controller: "SelectFileDialogController",
                size: "lg",
                resolve: {
                    file: function () { return file; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var selectFormDialog = function (form, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectFormDialog.min.html",
                controller: "SelectFormDialogController",
                resolve: {
                    form: function () { return form; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        var selectObjectDialog = function(object, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectObjectDialog.min.html",
                controller: "SelectObjectDialogController",
                resolve: {
                    selectedObject: function () { return object; }
                }
            }).result.then(function(result) {
                onAccept(result);
            });
        };

        var selectUserDialog = function(onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectUserDialog.min.html",
                controller: "SelectUserDialogController"
            }).result.then(function(result) {
                onAccept(result);
            });
        };

        var selectTenantsDialog = function(tenantIds, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/selectTenantsDialog.min.html",
                controller: "SelectTenantsDialogController",
                resolve: {
                    tenantIds: function() { return tenantIds; }
                }
            }).result.then(function(result) {
                onAccept(result);
            });
        }

        var fileEditorDialog = function(filename, content, onAccept) {
            $modal.open({
                backdrop: "static",
                templateUrl: "/admin/app/modules/dialog/fileEditorDialog.min.html",
                controller: "FileEditorDialogController",
                size: "lg",
                resolve: {
                    filename: function() { return filename; },
                    content: function () { return content; }
                }
            }).result.then(function (result) {
                onAccept(result);
            });
        };

        instance.singleInputDialog = singleInputDialog;
        instance.singlePasswordDialog = singlePasswordDialog;
        instance.twoInputsDialog = twoInputsDialog;
        instance.confirmDialog = confirmDialog;
        instance.messageDialog = messageDialog;
        instance.fileUploadDialog = fileUploadDialog;
        instance.fileReplaceDialog = fileReplaceDialog;
        instance.selectPageDialog = selectPageDialog;
        instance.selectFileDialog = selectFileDialog;
        instance.selectFormDialog = selectFormDialog;
        instance.selectUserDialog = selectUserDialog;
        instance.selectObjectDialog = selectObjectDialog;
        instance.selectTenantsDialog = selectTenantsDialog;
        instance.fileEditorDialog = fileEditorDialog;

        return instance;
    }]);

    angular.module("app").controller("FileEditorDialogController", ["$scope", "$modalInstance", "filename", "content", function($scope, $modalInstance, filename, content) {
        $scope.subject = filename;
        $scope.content = content;
        $scope.isFullscreen = false;

        $scope.codemirrorOptions = {
            lineNumbers: true,
            theme: "neo",
            lineWrapping: true,
            matchBrackets: true,
            autoCloseBrackets: true,
            matchTags: { bothTags: true },
            autoCloseTags: true,
            styleActiveLine: true,
            viewportMargin: Infinity,
            mode: "htmlmixed"
        };

        $scope.expand = function () {
            $scope.isFullscreen = true;
            angular.element(".modal-dialog").addClass("modal-fullscreen");
        };

        $scope.compress = function () {
            $scope.isFullscreen = false;
            angular.element(".modal-dialog").removeClass("modal-fullscreen");
        };

        $scope.ok = function () {
            $scope.compress();
            $modalInstance.close($scope.content);
        };

        $scope.cancel = function () {
            $scope.compress();
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SelectTenantsDialogController", ["$scope", "$modalInstance", "tenantService", "tenantIds", function ($scope, $modalInstance, tenantService, tenantIds) {
        var tenant = tenantService.getTenant();
        var tenants = _.reject(tenantService.getTenants(), function(t) {
            return t.id === tenant.id;
        });

        $scope.tenants = _.map(tenants, function (t) {
            var c = angular.copy(t);
            c.isSelected = _.some(tenantIds, function(id) { return id === c.id; });
            return c;
        });

        $scope.ok = function () {
            var selected = _.filter($scope.tenants, function(t) {
                return t.isSelected;
            });
            $modalInstance.close(selected);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SelectUserDialogController", ["$scope", "$modalInstance", "$http", "tenantService", function ($scope, $modalInstance, $http, tenantService) {
        $http.get("/api/users/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
            $scope.users = data;
        });

        $scope.select = function (user) {
            $scope.selectedUser = user;
        }

        $scope.ok = function () {
            $modalInstance.close($scope.selectedUser);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SelectPageDialogController", ["$scope", "$modalInstance", "$http", "$timeout", "tenantService", "page", function ($scope, $modalInstance, $http, $timeout, tenantService, page) {

        function expandParent(website, pge) {
            var parent = _.find(website.pages, function (p) { return p.id === pge.parentId; });
            if (parent) {
                parent.isCollapsed = false;
                expandParent(website, parent);
            }
        }

        function searchPages(website, pageId) {
            _.each(website.pages, function (p) { p.isCollapsed = true; });
            var pages = _.filter(website.pages, function (p) { return p.id === pageId; });
            _.each(pages, function (p) { expandParent(website, p); });
        };

        var addPagesToCollection = function (website, pages) {
            angular.forEach(pages, function (value) {
                website.allPages.push(value);
            });
        };

        var openWebsite = function (website) {
            if (!website.isLoaded) {
                website.isLoaded = true;
                $http.get("/api/pages/" + website.id, { cache: false }).success(function (pageData) {
                    website.pages = pageData;
                    addPagesToCollection(website, website.pages);
                    $scope.$broadcast("collapseAll");
                });
                $http.get("/api/pages/" + website.id + "/starred", { cache: false }).success(function (starredData) {
                    website.starred = starredData;
                    addPagesToCollection(website, website.starred);
                });
            }
        };

        $http.get("/api/websites/" + tenantService.getTenant().id, { cache: false })
            .success(function (data) {
                $scope.websites = data;

                angular.forEach($scope.websites, function (value, key) {
                    angular.extend(value, {
                        isOpen: false,
                        isLoaded: false,
                        pages: [],
                        starred: [],
                        allPages: []
                    });
                    $scope.$watch("websites[" + key + "].isOpen", function () {
                        if (value.isOpen && !value.isLoaded) {
                            openWebsite(value);
                        }
                    });
                });

                if (page) {
                    var website = _.find($scope.websites, function (w) { return w.id === page.websiteId; });
                    if (!website) {
                        return;
                    }
                    website.isOpen = true;
                    $timeout(function() {
                        $scope.selectedPage = page;
                        searchPages(website, page.id);
                    }, 1000);

                }

            }).error(function () {
                $scope.websites = {};
            });

        $scope.getChildren = function (websiteId, parentPageId) {
            if (!$scope.websites) {
                return [];
            }
            var website = _.find($scope.websites, function (w) { return w.id === websiteId; });
            if (!website) {
                return [];
            }

            var children = _.filter(website.pages, function (p) { return p.parentId === parentPageId; });
            children = _.sortBy(children, "name");
            children = _.sortBy(children, "sortOrder");
            return children;
        };

        $scope.selectPage = function (node) {
            $scope.selectedPage = node.node;
        };

        $scope.ok = function () {
            $modalInstance.close($scope.selectedPage);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };

        // websites
        // pages

        // falls page vorhanden, entsprechende website und alle page parents aufklappen und seite selektieren
    }]);

    angular.module("app").controller("SelectObjectDialogController", ["$scope", "$modalInstance", "$http", "$timeout", "tenantService", "selectedObject", function ($scope, $modalInstance, $http, $timeout, tenantService, selectedObject) {
        $scope.definitions = [];
        $scope.objects = [];
        $scope.step = 0;
        $scope.selectedDefinition = {};
        $scope.selectedObject = {};

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id, { cache: false }).success(function(data) {
            _.each(data, function (d) {
                d.isShared = false;
                $scope.definitions.push(d);
            });

            var fd = _.find($scope.definitions, function (d) { return d.id === selectedObject.definitionId; });
            if (fd) {
                $scope.selectedObject = selectedObject;
                $scope.selectDefinition(fd);
            }
        });

        $http.get("/api/objectdefinition/" + tenantService.getTenant().id + "/shared", { cache: false }).success(function (data) {
            _.each(data, function (d) {
                d.isShared = true;
                $scope.definitions.push(d);
            });

            var fd = _.find($scope.definitions, function (d) { return d.id === selectedObject.definitionId; });
            if (fd) {
                $scope.selectedObject = selectedObject;
                $scope.selectDefinition(fd);
            }
        });

        $scope.goBack = function() {
            $scope.step = 0;
            $scope.selectedObject = {};
        };

        $scope.selectDefinition = function(definition) {
            $scope.selectedDefinition = definition;
            var tenantId = tenantService.getTenant().id;

            $http.get("/api/objects/" + tenantId + "/" + definition.id, { cache: false }).success(function(data) {
                $scope.objects = data;
                $scope.step = 1;
            });
        };

        $scope.selectObject = function(o) {
            $scope.selectedObject = o;
        };

        $scope.ok = function () {
            if ($scope.step === 1 && $scope.selectedObject) {
                $modalInstance.close($scope.selectedObject);
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SelectFileDialogController", ["$scope", "$modalInstance", "$http", "$timeout", "tenantService", "file", function ($scope, $modalInstance, $http, $timeout, tenantService, file) {
        $scope.files = [];
        $scope.selectedFile = {};
        $scope.sharedDirectoryRoot = { id: 12345, name: "Shared directories" };
        $scope.shared = [$scope.sharedDirectoryRoot];

        function getRootNodesScope(treeId) {
            return angular.element(document.getElementById(treeId)).scope().$nodesScope.childNodes();
        };

        function getScopePathIter(comparer, search, scope, parentScopeList) {
            if (!scope) {
                return null;
            }

            var newParentScopeList = parentScopeList.slice();
            newParentScopeList.push(scope);

            if (scope.$modelValue && comparer(scope.$modelValue, search)) {
                $scope.selectDirectory(scope.$modelValue);
                return newParentScopeList;
            }

            var foundScopesPath = null;
            var childNodes = scope.childNodes();
            var children = [];

            if (scope.$modelValue && scope.$modelValue.children) {
                children = scope.$modelValue.children;
            }

            for (var i = 0; foundScopesPath === null && i < childNodes.length; i++) {
                foundScopesPath = getScopePathIter(comparer, search, childNodes[i], newParentScopeList);
            }
            for (var c = 0; foundScopesPath === null && c < children.length; c++) {
                if (comparer(children[c], search)) {
                    $scope.selectDirectory(children[c]);
                    return newParentScopeList;
                }
            }

            return foundScopesPath;
        }

        function fileIdComparer(f, id) {
            return f.id === id;
        }

        function getScopePath(comparer, search, scope) {
            return getScopePathIter(comparer, search, scope, []);
        }

        var expandSelectedDirectory = function(treeId) {
            var rootNodesScope = getRootNodesScope(treeId);
            for (var s = 0; s < rootNodesScope.length; s++) {
                var rootNodeScope = rootNodesScope[s];
                var parentScopes = getScopePath(fileIdComparer, file.directoryId, rootNodeScope);

                if (parentScopes) {
                    for (var i = 0; i < parentScopes.length; i++) {
                        parentScopes[i].expand();
                    }
                }
            }
        };

        $modalInstance.opened.then(function () {
            if (file) {
                $timeout(function() {
                    $scope.selectedFile = file;
                    expandSelectedDirectory("tree-root");
                    expandSelectedDirectory("tree-root-shared");
                }, 1000);
            }
        });

        var tenantId = tenantService.getTenant().id;
        $http.get("/api/filestores/" + tenantId, { cache: false }).success(function(fileStores) {
            $scope.directories = fileStores;
            _.each(fileStores, function (fileStore) {
                fileStore.isFileStore = true;
                $http.get("/api/filedirectory/" + tenantId + "/" + fileStore.id, { cache: false }).success(function (data) {
                    fileStore.children = data;
                });
            });
        });

        function markDirectoryAsShared(directory) {
            directory.isShared = true;

            if (directory.children && directory.children.length > 0) {
                _.each(directory.children, markDirectoryAsShared);
            }
        };

        $http.get("/api/filedirectory/" + tenantId + "/shared", { cache: false }).success(function (directories) {
            $scope.sharedDirectoryRoot.children = directories;

            _.each(directories, markDirectoryAsShared);

            var treeRootScope = angular.element(document.getElementById("tree-root-shared")).scope();
            treeRootScope.collapseAll();
        });

        var getFiles = function (directoryId) {
            $scope.files = [];
            $http.get("/api/file/" + directoryId, { cache: false }).success(function (data) {
                $scope.files = data;
            });
        };

        var getSharedFiles = function (directoryId) {
            $scope.files = [];
            var url = String.format("/api/files/{0}/{1}/shared", tenantService.getTenant().id, directoryId);
            $http.get(url, { cache: false }).success(function (data) {
                $scope.files = data;
            });
        }

        $scope.selectDirectory = function (directory) {
            $scope.selectedDirectory = directory;

            if (directory.isFileStore) {
                $scope.files = [];
                return;
            }

            if (directory.isShared) {
                getSharedFiles($scope.selectedDirectory.id);
                return;
            }

            getFiles($scope.selectedDirectory.id);
        };

        $scope.selectFile = function(f) {
            $scope.selectedFile = f;
        };

        $scope.ok = function () {
            $modalInstance.close($scope.selectedFile);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SelectFormDialogController", ["$scope", "$modalInstance", "$http", "tenantService", "form", function($scope, $modalInstance, $http, tenantService, form) {
        $scope.forms = [];
        $scope.selectedForm = form;

        $http.get("/api/forms/" + tenantService.getTenant().id, { cache: false }).success(function(forms) {
            $scope.forms = forms;
        });

        $scope.select = function (f) {
            $scope.selectedForm = f;
        };

        $scope.ok = function () {
            $modalInstance.close($scope.selectedForm);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("FileUploadDialogController", ["$scope", "$modalInstance", "$log", "subject", "uploadUrl", "maxFileCount", function ($scope, $modalInstance, $log, subject, uploadUrl, maxFileCount) {
        $scope.subject = subject;
        $scope.uploadUrl = uploadUrl;
        $scope.maxFileCount = maxFileCount;

        $scope.ok = function () {
            $modalInstance.close("ok");
        };
    }]);

    angular.module("app").controller("SingleInputDialogController", ["$scope", "$modalInstance", "$log", "subject", "label", "value", function ($scope, $modalInstance, $log, subject, label, value) {
        $scope.subject = subject;
        $scope.label = label;
        $scope.value = value;

        $scope.ok = function () {
            $modalInstance.close($scope.value);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("SinglePasswordDialogController", ["$scope", "$modalInstance", "$log", "$translate", "subject", "label", function ($scope, $modalInstance, $log, $translate, subject, label) {
        $scope.subject = subject;
        $scope.label = label;
        $scope.oldPassword = "";
        $scope.newPassword = "";

        var ridiculous = "";
        var veryWeak = "";
        var weak = "";
        var good = "";
        var strong = "";
        var veryStrong = "";

        $translate(["ridiculous", "very_weak", "weak", "good", "strong", "very_strong"]).then(function (t) {
            ridiculous = t.ridiculous;
            veryWeak = t.very_weak;
            weak = t.weak;
            good = t.good;
            strong = t.strong;
            veryStrong = t.very_strong;
        });

        $scope.$watch("newPassword", function (newValue) {
            var score = new Score(newValue);
            var entropy = score.calculateEntropyScore() * 1.25;
            entropy = entropy > 100 ? 100 : (entropy < 0 ? 0 : entropy);

            $scope.pwStrength = entropy;
            $scope.pwStrengthColor = "danger";

            if (entropy < 25) {
                $scope.pwStrengthColor = "danger";
            } else if (entropy < 50) {
                $scope.pwStrengthColor = "warning";
            } else {
                $scope.pwStrengthColor = "success";
            }

            if (entropy < 10) {
                $scope.pwStrengthDisplay = ridiculous;
            } else if (entropy < 20) {
                $scope.pwStrengthDisplay = veryWeak;
            } else if (entropy < 30) {
                $scope.pwStrengthDisplay = weak;
            } else if (entropy < 40) {
                $scope.pwStrengthDisplay = good;
            } else if (entropy < 50) {
                $scope.pwStrengthDisplay = strong;
            } else {
                $scope.pwStrengthDisplay = veryStrong;
            }
        });

        $scope.showOrHidePassword = function () {
            if (document.getElementById("password").type === "password") {
                document.getElementById("password").type = "text";
            } else {
                document.getElementById("password").type = "password";
            }
        };

        $scope.generatePassword = function () {
            document.getElementById("password").type = "text";
            $scope.newPassword = generatePassword(12, false);
        };

        $scope.ok = function () {
            $modalInstance.close({
                oldPassword: $scope.oldPassword,
                newPassword: $scope.newPassword
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("TwoInputsDialogController", ["$scope", "$modalInstance", "$log", "subject", "labelOne", "valueOne", "labelTwo", "valueTwo", function ($scope, $modalInstance, $log, subject, labelOne, valueOne, labelTwo, valueTwo) {
        $scope.subject = subject;
        $scope.labelOne = labelOne;
        $scope.valueOne = valueOne;
        $scope.labelTwo = labelTwo;
        $scope.valueTwo = valueTwo;

        $scope.ok = function () {
            $modalInstance.close({
                valueOne: $scope.valueOne,
                valueTwo: $scope.valueTwo
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("ConfirmDialogController", ["$scope", "$modalInstance", "subject", "label", function ($scope, $modalInstance, subject, label) {
        $scope.subject = subject;
        $scope.text = label;

        $scope.ok = function () {
            $modalInstance.close("ok");
        };

        $scope.cancel = function () {
            $modalInstance.dismiss("cancel");
        };
    }]);

    angular.module("app").controller("MessageDialogController", ["$scope", "$modalInstance", "subject", "label", function ($scope, $modalInstance, subject, label) {
        $scope.subject = subject;
        $scope.text = label;

        $scope.ok = function () {
            $modalInstance.close("ok");
        };
    }]);
}());