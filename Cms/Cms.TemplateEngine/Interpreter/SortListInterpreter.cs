﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    public sealed class SortListInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "sort"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.LeftItem == null ||
                !(tree.LeftItem.Result is IEnumerable) ||
                tree.Children.Count != 1 ||
                tree.Children[0].ParameterType != ParameterType.String ||
                (!tree.Value.Equals("sortAsc", StringComparison.OrdinalIgnoreCase) &&
                 !tree.Value.Equals("sortDesc", StringComparison.OrdinalIgnoreCase)))
            {
                return;
            }

            var engine = new Engine();
            var tuples = new List<Tuple<object, object>>();
            var sortExpression = (string)tree.Children[0].Result;
            var result = ((IEnumerable)tree.LeftItem.Result).GetListOfSameType();

            foreach (var item in (IEnumerable)tree.LeftItem.Result)
            {
                var treeItem = engine.ParseTreeItem(sortExpression);

                if (treeItem.ItemType == TreeItemType.Attribute || treeItem.ItemType == TreeItemType.Function)
                {
                    var parent = new TreeItem
                    {
                        ItemType = TreeItemType.Expression,
                        Value = string.Empty,
                    };
                    parent.Children.Add(treeItem);
                    treeItem.Parent = parent;
                    treeItem = parent;
                }

                treeItem.Children.Insert(0, new TreeItem
                {
                    ItemType = TreeItemType.Function,
                    Parent = treeItem,
                    Result = item,
                });

                var sortResult = InterpreterFactory.Instance.Calculate(treeItem);
                tuples.Add(Tuple.Create(sortResult, item));
            }

            var sorted = new List<object>(0);

            if (tree.Value.Equals("sortAsc", StringComparison.OrdinalIgnoreCase))
            {
                sorted = tuples.OrderBy(t => t.Item1).Select(t => t.Item2).ToList();
            }
            else if (tree.Value.Equals("sortDesc", StringComparison.OrdinalIgnoreCase))
            {
                sorted = tuples.OrderByDescending(t => t.Item1).Select(t => t.Item2).ToList();
            }

            result.AddRange(sorted);
            tree.Result = result;
        }
    }
}