﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDefinitionFieldMovedDown : EventBase<DynamicObjectDefinition>
    {
        public string Name { get; set; }
    }
}