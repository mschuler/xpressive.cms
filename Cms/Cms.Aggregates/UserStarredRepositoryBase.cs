using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Cms.Aggregates
{
    internal abstract class UserStarredRepositoryBase<T> : IUserStarredRepository<T> where T : IAggregate
    {
        public ICollection<T> Get(ulong userId)
        {
            var userStarredRepository = GetUserStarredRepository();

            List<ulong> aggregateIds;
            if (!userStarredRepository.TryGetValue(userId, out aggregateIds))
            {
                return new Collection<T>();
            }

            var repository = GetRepository();
            var result = new List<T>(aggregateIds.Count);

            foreach (var aggregateId in aggregateIds)
            {
                T aggregate;
                if (repository.TryGet(aggregateId, out aggregate))
                {
                    result.Add(aggregate);
                }
            }

            return result;
        }

        public void Add(ulong userId, ulong aggregateId)
        {
            var userStarredRepository = GetUserStarredRepository();

            List<ulong> aggregateIds;
            if (!userStarredRepository.TryGetValue(userId, out aggregateIds))
            {
                var @lock = GetLockObject();
                lock (@lock)
                {
                    if (!userStarredRepository.TryGetValue(userId, out aggregateIds))
                    {
                        aggregateIds = new List<ulong>();
                        userStarredRepository.Add(userId, aggregateIds);
                    }
                }
            }

            if (!aggregateIds.Contains(aggregateId))
            {
                aggregateIds.Add(aggregateId);
            }
        }

        public void Remove(ulong userId, ulong aggregateId)
        {
            var userStarredRepository = GetUserStarredRepository();

            List<ulong> aggregateIds;
            if (userStarredRepository.TryGetValue(userId, out aggregateIds))
            {
                aggregateIds.RemoveAll(id => id.Equals(aggregateId));
            }
        }

        public bool IsStarred(ulong userId, ulong aggregateId)
        {
            var userStarredRepository = GetUserStarredRepository();

            List<ulong> aggregateIds;
            if (userStarredRepository.TryGetValue(userId, out aggregateIds))
            {
                return aggregateIds.Contains(aggregateId);
            }
            return false;
        }

        protected abstract IDictionary<ulong, List<ulong>> GetUserStarredRepository();

        protected abstract IRepository<T> GetRepository();

        protected abstract object GetLockObject();
    }
}