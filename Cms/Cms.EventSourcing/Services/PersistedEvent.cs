using System;
using Cms.EventSourcing.Contracts.Events;
using NPoco;

namespace Cms.EventSourcing.Services
{
    [TableName("Event")]
    [PrimaryKey("Version", AutoIncrement = false)]
    internal class PersistedEvent
    {
        public PersistedEvent() { }

        public PersistedEvent(IEvent @event, ulong transactionId, string data)
        {
            Transaction = Convert.ToInt64(transactionId);
            Version = Convert.ToInt64(@event.Version);
            AggregateType = @event.AggregateType.Name;
            AggregateId = Convert.ToInt64(@event.AggregateId);
            Event = @event.GetType().Name;
            UserId = Convert.ToInt64(@event.UserId);
            Data = data;
        }

        public long Transaction { get; set; }
        public long Version { get; set; }
        public string AggregateType { get; set; }
        public long AggregateId { get; set; }
        public string Event { get; set; }
        public long UserId { get; set; }
        public string Data { get; set; }
        public bool IsDeleted { get; set; }
    }
}