﻿using Cms.Aggregates.Security;
using Cms.EventSourcing.Contracts.Events;

namespace Cms.Services.Contracts
{
    public interface IEventTranslationService
    {
        string GetHeader(IEvent @event);

        string GetContent(IEvent @event);

        bool CanUserSeeEvent(User user, IEvent @event);

        bool BelongsToTenant(ulong tenantId, IEvent @event);
    }
}
