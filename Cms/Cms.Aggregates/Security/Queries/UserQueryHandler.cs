﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Security.Queries
{
    internal class UserQueryHandler : QueryHandlerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;

        public UserQueryHandler(IUserRepository userRepository, IGroupRepository groupRepository)
        {
            _userRepository = userRepository;
            _groupRepository = groupRepository;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof(GetUser),
                typeof(GetAllUsers),
            };
        }

        public IEnumerable<object> When(GetAllUsers query)
        {
            return _userRepository
                .GetAll()
                .Select(u => u.ToDto());
        }

        public IEnumerable<object> When(GetUser query)
        {
            var user = _userRepository.Get(query.AggregateId);
            var requester = _userRepository.Get(query.UserId);
            var groupNames = new List<string>();

            if (requester.HasPermission("group", query.TenantId, PermissionLevel.CanSee, _groupRepository))
            {
                var groupIds = new HashSet<ulong>(user.GroupIds);

                foreach (var group in _groupRepository.GetAll(query.TenantId))
                {
                    if (groupIds.Contains(group.Id))
                    {
                        groupNames.Add(group.Name);
                    }
                }
            }

            return new[] { user.ToDto(groupNames) };
        }
    }
}
