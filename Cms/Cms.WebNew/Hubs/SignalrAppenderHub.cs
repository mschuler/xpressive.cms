﻿using System;
using System.Threading.Tasks;
using log4net.Appender;
using log4net.Core;
using log4net.Util;
using Microsoft.AspNet.SignalR;

namespace Cms.WebNew.Hubs
{
    public class SignalrAppenderHub : Hub
    {
        public SignalrAppenderHub()
        {
            SignalrAppender.LocalInstance.MessageLogged = OnMessageLogged;
        }

        public void OnMessageLogged(LogEntry e)
        {
            Clients.All.onLoggedEvent(e.FormattedEvent, e.LoggingEvent);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            return base.OnDisconnected(stopCalled);
        }
    }

    public class SignalrAppender : AppenderSkeleton
    {
        public Action<LogEntry> MessageLogged;
        //private FixFlags _fixFlags = FixFlags.All;

        public SignalrAppender()
        {
            System.Diagnostics.Debug.WriteLine("Instantiating");
            LocalInstance = this;
        }

        public static SignalrAppender LocalInstance { get; private set; }

        //public virtual FixFlags Fix
        //{
        //    get { return _fixFlags; }
        //    set { _fixFlags = value; }
        //}

        protected override void Append(LoggingEvent loggingEvent)
        {
            //// LoggingEvent may be used beyond the lifetime of the Append()
            //// so we must fix any volatile data in the event
            //loggingEvent.Fix = Fix;

            var formattedEvent = RenderLoggingEvent(loggingEvent);

            var logEntry = new LogEntry(formattedEvent, new JsonLoggingEventData(loggingEvent));

            if (MessageLogged != null)
            {
                MessageLogged(logEntry);
            }
        }
    }


    public class LogEntry
    {
        public LogEntry(string formttedEvent, JsonLoggingEventData loggingEvent)
        {
            FormattedEvent = formttedEvent;
            LoggingEvent = loggingEvent;
        }

        public string FormattedEvent { get; set; }
        public JsonLoggingEventData LoggingEvent { get; set; }
    }

    public class JsonLoggingEventData
    {
        private const FixFlags Flags = FixFlags.Exception | FixFlags.UserName | FixFlags.Partial;

        public JsonLoggingEventData()
        {
        }

        public JsonLoggingEventData(LoggingEvent loggingEvent)
        {
            var loggingEventData = loggingEvent.GetLoggingEventData(Flags);
            Domain = loggingEventData.Domain;
            ExceptionString = loggingEventData.ExceptionString;
            //Identity = loggingEventData.Identity;
            Level = loggingEventData.Level.DisplayName;
            LoggerName = loggingEventData.LoggerName;
            Message = loggingEventData.Message;
            Properties = loggingEventData.Properties;
            ThreadName = loggingEventData.ThreadName;
            TimeStamp = string.Format("{0:u}", loggingEventData.TimeStamp);
            //UserName = loggingEventData.UserName;
        }

        public string Domain { get; set; }

        public string ExceptionString { get; set; }

        //public string Identity { get; set; }

        public string Level { get; set; }

        public string LoggerName { get; set; }

        public string Message { get; set; }

        public PropertiesDictionary Properties { get; set; }

        public string ThreadName { get; set; }

        public string TimeStamp { get; set; }

        //public string UserName { get; set; }
    }
}