namespace Cms.Aggregates.Links
{
    public interface ILinkResolver
    {
        string GetHyperlink(ILink link);
    }
}