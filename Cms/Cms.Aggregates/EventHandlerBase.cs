﻿using Cms.EventSourcing;
using Cms.EventSourcing.Contracts.Events;

namespace Cms.Aggregates
{
    public abstract class EventHandlerBase : IEventHandler
    {
        public virtual void Handle(IEvent @event)
        {
            RedirectToWhen.InvokeEventHandler(this, @event);
        }
    }
}