namespace Cms.Aggregates.Tenants.Events
{
    public class TenantNameChanged : EventBase<Tenant>
    {
        public string Name { get; set; }
    }
}