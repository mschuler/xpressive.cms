﻿namespace Cms.Aggregates.Tenants
{
    public interface ITenantRepository : IRepository<Tenant>
    {

    }
}