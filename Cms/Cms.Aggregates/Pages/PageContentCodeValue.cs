﻿namespace Cms.Aggregates.Pages
{
    public class PageContentCodeValue : PageContentValueBase
    {
        public string Value { get; set; }
    }
}