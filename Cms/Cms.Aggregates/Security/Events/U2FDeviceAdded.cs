namespace Cms.Aggregates.Security.Events
{
    public class U2FDeviceAdded : EventBase<User>
    {
        public string PublicKey { get; set; }
        public string AttestationCert { get; set; }
        public string KeyHandle { get; set; }
    }
}