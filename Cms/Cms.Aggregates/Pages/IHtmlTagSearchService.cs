using System.Collections.Generic;

namespace Cms.Aggregates.Pages
{
    public interface IHtmlTagSearchService
    {
        IEnumerable<IHtmlTag> Find(string tagName, string html);
    }
}