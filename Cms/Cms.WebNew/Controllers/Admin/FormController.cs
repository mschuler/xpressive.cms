using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Commands;
using Cms.Aggregates.Forms.Queries;
using Cms.Aggregates.Security;
using Cms.EventSourcing.Services;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using CsvHelper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class FormController : ApiControllerBase
    {
        private readonly IFormRepository _formRepository;
        private readonly IUniqueIdProvider _idProvider;
        private readonly IFormGeneratorFactory _formGenerator;
        private readonly IGroupRepository _groupRepository;

        public FormController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IFormRepository formRepository, IUniqueIdProvider idProvider, IFormGeneratorFactory formGenerator, IGroupRepository groupRepository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _formRepository = formRepository;
            _idProvider = idProvider;
            _formGenerator = formGenerator;
            _groupRepository = groupRepository;
        }

        [HttpGet, Route("api/forms/{tenantId:ulong}")]
        public IEnumerable<object> GetAll(ulong tenantId)
        {
            return ExecuteQuery(new GetAllForms
            {
                TenantId = tenantId,
            });
        }

        [HttpGet, Route("api/form/{formId:ulong}")]
        public object Get(ulong formId)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            var result = ExecuteQuery(new GetFormDetail
            {
                FormId = formId,
                TenantId = tenantId,
            });
            return result != null ? result.FirstOrDefault() : null;
        }

        [HttpGet, Route("api/form/search/{tenantId:ulong}")]
        public IEnumerable<object> Search(ulong tenantId, [FromUri]string search)
        {
            return ExecuteQuery(new GetFormsBySearchPattern
            {
                SearchPattern = search,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/forms/{tenantId:ulong}")]
        public object Create(ulong tenantId, [FromBody]NameDto dto)
        {
            var publicId = _formRepository.GetNextPublicId();
            var id = _idProvider.GetId();

            SendCommand(new CreateNewForm
            {
                AggregateId = id,
                Name = dto.Name,
                PublicId = publicId,
                TenantId = tenantId,
            });

            return new
            {
                Id = id.ToJsonString(),
                Name = dto.Name,
                PublicId = publicId,
            };
        }

        [HttpPost, Route("api/form/{formId:ulong}/duplicate")]
        public object Duplicate(ulong formId, [FromBody] NameDto dto)
        {
            var publicId = _formRepository.GetNextPublicId();
            var id = _idProvider.GetId();
            var form = _formRepository.Get(formId);

            SendCommand(new DuplicateForm
            {
                AggregateId = id,
                FormIdToDuplicate = formId,
                PublicId = publicId,
                Name = dto.Name,
                TenantId = form.TenantId
            });

            return new
            {
                Id = id.ToJsonString(),
                Name = dto.Name,
                PublicId = publicId,
            };
        }

        [HttpDelete, Route("api/form/{formId:ulong}")]
        public void Delete(ulong formId)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            SendCommand(new DeleteForm
            {
                AggregateId = formId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/form/{formId:ulong}/name")]
        public void Rename(ulong formId, [FromBody]NameDto dto)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            SendCommand(new ChangeFormName
            {
                AggregateId = formId,
                Name = dto.Name,
                TenantId = tenantId,
            });
        }

        [HttpGet, Route("api/form/{formId:ulong}/entries")]
        public IEnumerable<object> GetEntries(ulong formId)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            return ExecuteQuery(new GetFormEntries
            {
                FormId = formId,
                TenantId = tenantId,
            });
        }

        [HttpGet, Route("api/form/{formId:ulong}/entries/csv")]
        public HttpResponseMessage GetEntriesAsCsv(ulong formId)
        {
            var form = _formRepository.Get(formId);
            var columns = new HashSet<string>(form.Entries.SelectMany(e => e.Values.Keys).Distinct(StringComparer.OrdinalIgnoreCase));
            var entries = form.Entries.ToList();
            var csv = new StringBuilder();

            if (!CmsUser.HasPermission("form", form.TenantId, PermissionLevel.CanSee, _groupRepository))
            {
                columns = new HashSet<string>();
                entries = new List<FormEntry>();
            }

            using (var textWriter = new StringWriter(csv))
            {
                using (var csvWriter = new CsvWriter(textWriter))
                {
                    csvWriter.Configuration.Delimiter = "\t";
                    csvWriter.WriteField("Date & Time");
                    csvWriter.WriteField("IP Address");

                    foreach (var column in columns)
                    {
                        csvWriter.WriteField(column);
                    }
                    csvWriter.NextRecord();

                    foreach (var formEntry in entries)
                    {
                        csvWriter.WriteField(formEntry.Timestamp.ToString("dd.MM.yyyy HH:mm:ss"));
                        csvWriter.WriteField(formEntry.IpAddress);

                        foreach (var column in columns)
                        {
                            string value;
                            if (!formEntry.Values.TryGetValue(column, out value))
                            {
                                value = string.Empty;
                            }
                            value = value.Replace('\r', ' ');
                            value = value.Replace('\n', ' ');
                            value = value.Replace('\t', ' ');
                            value = value.Replace("  ", " ");
                            csvWriter.WriteField(value);
                        }
                        csvWriter.NextRecord();
                    }
                }
            }

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StringContent(csv.ToString());
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = "form_entries.csv";
            return result;
        }

        [HttpDelete, Route("api/form/{formId:ulong}/entry/{entryId:ulong}")]
        public void DeleteEntry(ulong formId, ulong entryId)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            SendCommand(new DeleteFormEntry
            {
                AggregateId = formId,
                FormEntryId = entryId,
                TenantId = tenantId,
            });
        }

        [HttpDelete, Route("api/form/{formId:ulong}/entries")]
        public void ClearFormEntries(ulong formId)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            SendCommand(new ClearFormEntries
            {
                AggregateId = formId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/form/{formId:ulong}/entry/{entryId:ulong}")]
        public void ChangeEntry(ulong formId, ulong entryId, [FromBody]dynamic values)
        {
            var tenantId = _formRepository.Get(formId).TenantId;

            var entryValues = ((IEnumerable<KeyValuePair<string, JToken>>)values)
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());

            SendCommand(new ChangeFormEntry
            {
                AggregateId = formId,
                FormEntryId = entryId,
                TenantId = tenantId,
                Values = entryValues,
            });
        }

        [HttpPost, Route("api/form/{formId:ulong}/entries")]
        public void CreateEntry(ulong formId, [FromBody] dynamic values)
        {
            var tenantId = _formRepository.Get(formId).TenantId;

            var entryValues = ((IEnumerable<KeyValuePair<string, JToken>>)values)
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());

            SendCommand(new CreateFormEntry
            {
                AggregateId = formId,
                FormEntryId = _idProvider.GetId(),
                TenantId = tenantId,
                IsSpam = false,
                RunFormActions = false,
                IpAddress = Request.GetClientIpAddress(),
                Values = entryValues,
            });
        }

        [HttpGet, Route("api/form/{formId:ulong}/fields")]
        public IEnumerable<object> GetFields(ulong formId)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            return ExecuteQuery(new GetFormFields
            {
                FormId = formId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/form/{formId:ulong}/fields")]
        public object CreateField(ulong formId, [FromBody] dynamic field)
        {
            var id = _idProvider.GetId();
            string name = field.name;
            var tenantId = _formRepository.Get(formId).TenantId;

            SendCommand(new AddFormField
            {
                AggregateId = formId,
                Id = id,
                Name = name,
                TenantId = tenantId,
            });

            field.id = id.ToJsonString();
            return field;
        }

        [HttpDelete, Route("api/form/{formId:ulong}/field/{fieldId:ulong}")]
        public void DeleteField(ulong formId, ulong fieldId)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            SendCommand(new RemoveFormField
            {
                AggregateId = formId,
                Id = fieldId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/form/{formId:ulong}/field/{fieldId:ulong}")]
        public void UpdateField(ulong formId, ulong fieldId, [FromBody] dynamic field)
        {
            string name = field.name;
            var tenantId = _formRepository.Get(formId).TenantId;

            SendCommand(new ChangeFormField
            {
                AggregateId = formId,
                Id = fieldId,
                Name = name,
                TenantId = tenantId,
            });
        }

        [HttpGet, Route("api/form/{formId:ulong}/actions")]
        public IEnumerable<object> GetActions(ulong formId)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            return ExecuteQuery(new GetFormActions
            {
                FormId = formId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/form/{formId:ulong}/action")]
        public object CreateAction(ulong formId, [FromBody]dynamic action)
        {
            FormActionType type;
            string t = action.type;
            if (!Enum.TryParse(t, true, out type))
            {
                throw new ExceptionWithReason("Unable to parse FormActionType");
            }

            var configurationType = FormActionConfiguration.GetFormActionConfigurationType(type);
            var typeJson = JsonConvert.SerializeObject(action.configuration);
            var configuration = JsonConvert.DeserializeObject(typeJson, configurationType);

            var id = _idProvider.GetId();
            var tenantId = _formRepository.Get(formId).TenantId;

            SendCommand(new AddFormAction
            {
                AggregateId = formId,
                Id = id,
                Type = type,
                Configuration = configuration,
                TenantId = tenantId,
            });

            action.id = id.ToJsonString();
            return action;
        }

        [HttpDelete, Route("api/form/{formId:ulong}/action/{actionId:ulong}")]
        public void DeleteAction(ulong formId, ulong actionId)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            SendCommand(new RemoveFormAction
            {
                AggregateId = formId,
                Id = actionId,
                TenantId = tenantId,
            });
        }

        [HttpPost, Route("api/form/{formId:ulong}/action/{actionId:ulong}")]
        public void UpdateAction(ulong formId, ulong actionId, [FromBody]dynamic action)
        {
            FormActionType type;
            string t = action.type;
            if (!Enum.TryParse(t, true, out type))
            {
                throw new ExceptionWithReason("Unable to parse FormActionType");
            }

            var configurationType = FormActionConfiguration.GetFormActionConfigurationType(type);
            var typeJson = JsonConvert.SerializeObject(action.configuration);
            var configuration = JsonConvert.DeserializeObject(typeJson, configurationType);

            var tenantId = _formRepository.Get(formId).TenantId;

            SendCommand(new ChangeFormAction
            {
                AggregateId = formId,
                Id = actionId,
                Type = type,
                Configuration = configuration,
                TenantId = tenantId,
            });
        }

        [HttpGet, Route("api/form/{formId:ulong}/templategenerator")]
        public object GetTemplateGenerator(ulong formId)
        {
            var form = _formRepository.Get(formId);
            var result = ExecuteQuery(new GetFormGenerator { FormId = formId, TenantId = form.TenantId });
            return result.SingleOrDefault();
        }


        [HttpPost, Route("api/form/{formId:ulong}/templategenerator")]
        public string SaveTemplateGenerator(ulong formId, [FromBody]TemplateGeneratorDto dto)
        {
            var form = _formRepository.Get(formId);

            var command = new ChangeFormGeneratorFields
            {
                AggregateId = formId,
                TenantId = form.TenantId,
                LabelPosition = dto.Type,
            };

            var sortOrder = 0;

            foreach (var field in dto.Fields)
            {
                var options = new string[0];

                if (!string.IsNullOrEmpty(field.Options))
                {
                    options = field.Options.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                }

                command.Fields.Add(new GeneratorField
                {
                    FieldId = ulong.Parse(field.FieldId),
                    Type = field.Type.ToLowerInvariant(),
                    SortOrder = sortOrder,
                    Options = options,
                    IsMultiselect = field.Multiple,
                    IsRequired = field.Required
                });

                sortOrder++;
            }

            SendCommand(command);

            var layoutType = (FormLayoutType)Enum.Parse(typeof(FormLayoutType), dto.Type, ignoreCase: true);
            var generator = _formGenerator.Create(formId);

            foreach (var field in command.Fields)
            {
                var fieldName = form.Fields.Single(f => f.Id == field.FieldId).Name;
                generator.AddField(fieldName, field.Type, field.IsRequired, field.Options, field.IsMultiselect);
            }

            return generator.Render(layoutType);
        }

        [HttpGet, Route("api/form/{formId:ulong}/template")]
        public string GetTemplate(ulong formId)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            return ExecuteQuery(new GetFormTemplate
            {
                FormId = formId,
                TenantId = tenantId,
            }).OfType<string>().SingleOrDefault();
        }

        [HttpPost, Route("api/form/{formId:ulong}/template")]
        public void UpdateTemplate(ulong formId, [FromBody] FormTemplateDto template)
        {
            var tenantId = _formRepository.Get(formId).TenantId;
            SendCommand(new ChangeFormTemplate
            {
                AggregateId = formId,
                Template = template.Template,
                TenantId = tenantId,
            });
        }

        public class FormTemplateDto
        {
            public string Template { get; set; }
        }

        public class NameDto
        {
            public string Name { get; set; }
        }

        public class TemplateGeneratorDto
        {
            public string Type { get; set; }
            public List<TemplateGeneratorFieldDto> Fields { get; set; }
        }

        public class TemplateGeneratorFieldDto
        {
            public string FieldId { get; set; }
            public string FieldName { get; set; }
            public int SortOrder { get; set; }
            public string Type { get; set; }
            public string Options { get; set; }
            public bool Multiple { get; set; }
            public bool Required { get; set; }
        }
    }
}