﻿(function () {
    angular.module('app').controller("AuthenticationController", ["$rootScope", "$scope", "$http", "$q", "$location", "$log", "localStorageService", "authService", "userService", "tenantService", function ($rootScope, $scope, $http, $q, $location, $log, localStorageService, authService, userService, tenantService) {
        $scope.loginForm = {
            username: "",
            password: ""
        };

        $scope.isError = false;

        function logout() {
            $http.post("/api/user/logout").success(function() {
                $rootScope.$broadcast('logout');
                localStorageService.clearAll();
            });
        }

        $scope.currentUser = userService.currentUser();

        $scope.logout = function () {
            logout();
            userService.logout();
            $location.path("/login");
        };

        $scope.lock = function () {
            logout();
            userService.lockout();
            $location.path("/lock");
        };

        $scope.unlock = function() {
            if (!$scope.loginForm.password) {
                return;
            }
            $scope.loginForm.username = $scope.currentUser.email;
            $scope.tryLogin();
        };

        $scope.tryLogin = function () {
            $scope.isError = false;

            var dto = {
                userName: $scope.loginForm.username,
                password: $scope.loginForm.password
            };

            if ($scope.isTotp) {
                dto.totp = $scope.loginForm.totp;
            }

            authService.login(dto).then(function () {
                $scope.isError = false;

                var qT = tenantService.reloadTenants();
                var qU = userService.refreshLoginData();

                $q.all([qT, qU]).then(function() {
                    $location.path("/dashboard");
                });
            }, function (err) {
                if (err && err.error && err.error === "totp_enabled") {
                    $scope.isTotp = true;
                    $scope.loginForm.totp = "";
                } else if (err && err.error && err.error === "u2f_enabled") {
                    $scope.isU2F = true;
                    $scope.loginForm.u2fRequest = err.description;
                    $scope.loginForm.u2f = "";
                } else {
                    $scope.isError = true;
                }
            });
        };

        $rootScope.$on("currentUserChanged", function() {
            $scope.currentUser = userService.currentUser();
        });
    }]);

    angular.module('app').config(["$httpProvider", function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptorService');
    }]);
}());