﻿namespace Cms.Aggregates.Pages
{
    public class PageContentImageValue : PageContentValueBase
    {
        public string FileId { get; set; }

        public string LinkType { get; set; }
        public string PageId { get; set; }
        public string ExternalLink { get; set; }

        public int Alpha { get; set; }
        public int Saturation { get; set; }
        public int Brightness { get; set; }
        public int Contrast { get; set; }
        public int RoundedCorners { get; set; }
        public string Filter { get; set; }
        public string Tint { get; set; }
    }
}