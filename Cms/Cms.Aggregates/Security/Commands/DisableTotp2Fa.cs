namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission(false)]
    public class DisableTotp2Fa : CommandBase { }
}