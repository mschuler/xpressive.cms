﻿using System;
using Cms.Aggregates;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.FileDirectories.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.FileDirectories
{
    internal class FileDirectoryNameChangedEventTranslator : IEventTranslator
    {
        private readonly IRepository<FileDirectory> _repository;

        public FileDirectoryNameChangedEventTranslator(IRepository<FileDirectory> repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FileDirectoryNameChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Dateiverzeichnis umbenannt";
        }

        public string GetContent(IEvent @event)
        {
            var e = (FileDirectoryNameChanged)@event;
            return string.Format("Ein Dateiverzeichnis wurde in <strong>{0}</strong> umbenannt.", e.Name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "file";
        }
    }
}