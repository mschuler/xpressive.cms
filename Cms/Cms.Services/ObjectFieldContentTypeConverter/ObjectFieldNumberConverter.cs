﻿using System;
using Cms.SharedKernel;

namespace Cms.Services.ObjectFieldContentTypeConverter
{
    internal class ObjectFieldNumberConverter : IObjectFieldContentTypeConverter
    {
        public bool CanHandle(string type)
        {
            return type.Equals("number", StringComparison.OrdinalIgnoreCase);
        }

        public string Serialize(string json)
        {
            decimal value;
            if (!string.IsNullOrEmpty(json) && decimal.TryParse(json, out value))
            {
                return value.ToString("G");
            }
            return "0";
        }

        public object Deserialize(string value)
        {
            return value;
        }

        public SortableObjectField DeserializeForSortableField(string value)
        {
            decimal d;
            if (!string.IsNullOrEmpty(value) && decimal.TryParse(value, out d))
            {
                return new SortableObjectField
                {
                    VisibleValue = d.ToString("G"),
                    SortableValue = d.ToString("00000000000000000000000000000.00000000000000000000000000000"),
                };
            }

            return new SortableObjectField("00000000000000000000000000000.00000000000000000000000000000");
        }

        public object DeserializeForPublicApi(string value)
        {
            return value;
        }

        public bool TryGetMetadataForPublicApi(string value, out object metadata)
        {
            metadata = null;
            return false;
        }

        public object DeserializeForInterpreter(string value)
        {
            return value;
        }

        public string SerializeForImport(string json, string format, string options)
        {
            decimal value;
            if (!string.IsNullOrEmpty(json) && decimal.TryParse(json, out value))
            {
                return value.ToString("G");
            }
            return "0";
        }

        public string SerializeForImportPreview(string json, string format, string options)
        {
            decimal value;
            if (!string.IsNullOrEmpty(json) && decimal.TryParse(json, out value))
            {
                return value.ToString("G");
            }
            return "0";
        }

        public bool IsValidValueForImport(string json, string format, string options)
        {
            decimal value;
            return !string.IsNullOrEmpty(json) && decimal.TryParse(json, out value);
        }
    }
}