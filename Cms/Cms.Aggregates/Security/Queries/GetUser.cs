namespace Cms.Aggregates.Security.Queries
{
    [RequiredPermission(false)]
    public class GetUser : QueryBase
    {
        public ulong AggregateId { get; set; }
    }
}