using Cms.Aggregates.Security;

namespace Cms.Aggregates.PageTemplates.Commands
{
    [RequiredPermission("pagetemplate", PermissionLevel.CanEverything)]
    public class DeletePageTemplate : CommandBase { }
}