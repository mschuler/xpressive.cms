﻿namespace Cms.Aggregates.Security.Events
{
    public class UserNameChanged : EventBase<User>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}