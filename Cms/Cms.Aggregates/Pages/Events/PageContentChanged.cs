namespace Cms.Aggregates.Pages.Events
{
    public class PageContentChanged : EventBase<PageBase>
    {
        public string Name { get; set; }
        public PageContentValueBase Value { get; set; }
    }
}