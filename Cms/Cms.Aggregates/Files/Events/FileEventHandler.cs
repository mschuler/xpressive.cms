﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.FileStore;
using Cms.SharedKernel;

namespace Cms.Aggregates.Files.Events
{
    public class FileEventHandler : EventHandlerBase
    {
        private readonly IFileRepository _repository;
        private readonly IRepository<FileDirectory> _directoryRepository;
        private readonly IFileStoreRepository _fileStoreRepository;
        private readonly HashSet<string> _fileExtensionsToAutomaticallyEnableTracking = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

        public FileEventHandler(IFileRepository repository, IRepository<FileDirectory> directoryRepository, IFileStoreRepository fileStoreRepository)
        {
            _repository = repository;
            _directoryRepository = directoryRepository;
            _fileStoreRepository = fileStoreRepository;

            var extensions = ConfigurationManager
                .AppSettings["activateFileTrackingForFileTypes"]
                .Split(new[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries)
                .ToList();

            foreach (var extension in extensions)
            {
                _fileExtensionsToAutomaticallyEnableTracking.Add(extension);
            }
        }

        public void When(FileCreated @event)
        {
            var file = new File
            {
                Id = @event.AggregateId,
                FileDirectoryId = @event.FileDirectoryId,
                PublicId = @event.PublicId,
                Name = @event.Name
            };

            var version = new FileVersion
            {
                Version = @event.FileVersion,
                Hash = @event.Hash,
                Date = @event.Date,
                Size = @event.Size,
            };

            var directory = _directoryRepository.Get(file.FileDirectoryId);
            file.FileStoreId = directory.FileStoreId;
            file.TenantId = directory.TenantId;

            file.Add(version);

            if (_fileExtensionsToAutomaticallyEnableTracking.Contains(file.Name.GetExtension()))
            {
                file.IsTracked = true;
            }

            _repository.Add(file);
        }

        public void When(FileVersionCreated @event)
        {
            var file = _repository.Get(@event.AggregateId);
            var version = new FileVersion
            {
                Version = @event.FileVersion,
                Hash = @event.Hash,
                Date = @event.Date,
                Size = @event.Size,
            };
            file.Add(version);
        }

        public void When(FileNameChanged @event)
        {
            var file = _repository.Get(@event.AggregateId);
            file.Name = @event.Name;
        }

        public void When(FileDirectoryChanged @event)
        {
            var file = _repository.Get(@event.AggregateId);
            file.FileDirectoryId = @event.FileDirectoryId;
        }

        public void When(FileTrackingEnabled @event)
        {
            var file = _repository.Get(@event.AggregateId);
            file.IsTracked = true;
        }

        public void When(FileTrackingDisabled @event)
        {
            var file = _repository.Get(@event.AggregateId);
            file.IsTracked = false;
        }

        public void When(FileMarkedAsMalicious @event)
        {
            var file = _repository.GetEvenIfDeleted(@event.AggregateId);
            file.IsMalicious = true;

            var store = _fileStoreRepository.Get(file.FileStoreId);
            store.Delete(file.Id, @event.FileVersion);
        }

        public void When(FileDeleted @event)
        {
            _repository.Delete(@event.AggregateId);
        }
    }
}