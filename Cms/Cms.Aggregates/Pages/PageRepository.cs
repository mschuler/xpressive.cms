﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.SharedKernel;

namespace Cms.Aggregates.Pages
{
    internal class PageRepository : RepositoryBase<PageBase>, IPageRepository
    {
        private static readonly object _lock = new object();
        private static int _currentMaxPublicId;

        public int GetNextPublicId()
        {
            lock (_lock)
            {
                if (_currentMaxPublicId == 0)
                {
                    var allPages = GetAll().OfType<Page>().ToList();
                    _currentMaxPublicId = allPages.Count == 0 ? 0 : allPages.Max(p => p.PublicId);
                }

                _currentMaxPublicId++;
                return _currentMaxPublicId;
            }
        }

        public Page GetByPublicId(int publicId)
        {
            return GetAll().OfType<Page>().SingleOrDefault(p => p.PublicId.Equals(publicId));
        }

        public IEnumerable<PageBase> GetAllByWebsiteId(ulong websiteId)
        {
            return GetAll()
                .Where(p => p.WebsiteId.Equals(websiteId))
                .ToList();
        }

        public override void DeleteAllByTenantId(ulong tenantId)
        {
            throw new NotSupportedException("WebsiteRepository is deleting these pages.").WithGuid();
        }
    }
}