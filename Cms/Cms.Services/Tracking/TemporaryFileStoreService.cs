using System;
using System.Collections.Generic;
using System.IO;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using log4net;

namespace Cms.Services.Tracking
{
    internal class TemporaryFileStoreService : ITemporaryFileStoreService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(TemporaryFileStoreService));
        private readonly IUniqueIdProvider _idProvider;

        static TemporaryFileStoreService()
        {
            try
            {
                var directory = new DirectoryInfo(GetDirectoryLocation());
                if (directory.Exists)
                {
                    foreach (var file in directory.EnumerateFiles())
                    {
                        file.Delete();
                    }
                }
                else
                {
                    directory.Create();
                }
            }
            catch (UnauthorizedAccessException)
            {
                _log.Error("Unable to delete files from temporary file directory.");
            }
            catch (Exception e)
            {
                _log.Error(e.Message, e);
            }
        }

        private static readonly IDictionary<ulong, ITemporaryFile> _cache = new Dictionary<ulong, ITemporaryFile>();

        public TemporaryFileStoreService(IUniqueIdProvider idProvider)
        {
            _idProvider = idProvider;
        }

        public ITemporaryFile Create()
        {
            var id = _idProvider.GetId();
            var file = string.Format("{0:D20}.bin", id);
            var path = Path.Combine(GetDirectoryLocation(), file);

            using (File.Create(path)) { }

            var temporaryFile = new TemporaryFile
            {
                Id = id,
                File = new FileInfo(path)
            };

            _cache.Add(id, temporaryFile);

            return temporaryFile;
        }

        public void Delete(ITemporaryFile file)
        {
            if (file != null)
            {
                if (file.File != null && file.File.Exists)
                {
                    try
                    {
                        file.File.Delete();
                    }
                    catch (UnauthorizedAccessException)
                    {
                        _log.Error("Unable to delete file " + file.File.Name);
                    }
                    catch (Exception e)
                    {
                        _log.Error(e.Message, e);
                    }
                }

                _cache.Remove(file.Id);
            }
        }

        public bool TryGet(ulong id, out ITemporaryFile file)
        {
            return _cache.TryGetValue(id, out file);
        }

        private static string GetDirectoryLocation()
        {
            var rootDirectory = AppDomain.CurrentDomain.BaseDirectory;
            rootDirectory = Path.Combine(rootDirectory, "filesTemporary");
            return rootDirectory;
        }

        private class TemporaryFile : ITemporaryFile
        {
            public ulong Id { get; set; }
            public FileInfo File { get; set; }
        }
    }
}