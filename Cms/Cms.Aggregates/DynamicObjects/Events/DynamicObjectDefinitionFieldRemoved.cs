﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDefinitionFieldRemoved : EventBase<DynamicObjectDefinition>
    {
        public string Name { get; set; }
    }
}