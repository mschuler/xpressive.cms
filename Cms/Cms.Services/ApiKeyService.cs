using System;
using System.Security.Cryptography;
using Cms.Services.Contracts;
using Cms.SharedKernel;
using log4net;
using Newtonsoft.Json;

namespace Cms.Services
{
    internal class ApiKeyService : IApiKeyService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ApiKeyService));
        private static readonly TimeSpan _validity = TimeSpan.FromDays(10);
        private readonly IEncryptionService _encryptionService;

        public ApiKeyService(IEncryptionService encryptionService)
        {
            _encryptionService = encryptionService;
        }

        public IApiKeyPair GenerateNewKey(string ipAddress)
        {
            var key = CreateKey();
            var k = new Key
            {
                ApiKey = key,
                Expiration = DateTime.UtcNow.Add(_validity),
                IpAddress = ipAddress
            };

            var json = JsonConvert.SerializeObject(k);
            var cookie = Encrypt(json);

            return new ApiKeyPair(key, cookie);
        }

        public bool IsValidApiKey(string key, string cookie, string ipAddress)
        {
            try
            {
                var json = Decrypt(cookie);
                var k = JsonConvert.DeserializeObject<Key>(json);

                return
                    k != null &&
                    k.ApiKey.Equals(key, StringComparison.Ordinal) &&
                    k.IpAddress.Equals(ipAddress, StringComparison.Ordinal) &&
                    k.Expiration > DateTime.UtcNow;
            }
            catch (Exception e)
            {
                _log.Error("Error while validating api key.", e);
                return false;
            }
        }

        private static string CreateKey()
        {
            using (var generator = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[32];
                generator.GetBytes(bytes);
                var value = Convert.ToBase64String(bytes).TrimEnd('=').ToValidUrlPart().Replace("/", string.Empty);
                return value;
            }
        }

        private string Encrypt(string plain)
        {
            var encrypted = _encryptionService.EncryptString(plain);
            return Convert.ToBase64String(encrypted);
        }

        private string Decrypt(string cipher)
        {
            var binary = Convert.FromBase64String(cipher);
            return _encryptionService.DecryptString(binary);
        }

        public class Key
        {
            public string ApiKey { get; set; }
            public string IpAddress { get; set; }
            public DateTime Expiration { get; set; }
        }

        private class ApiKeyPair : IApiKeyPair
        {
            public ApiKeyPair(string apiKey, string cookie)
            {
                ApiKey = apiKey;
                Cookie = cookie;
            }

            public string ApiKey { get; private set; }

            public string Cookie { get; private set; }
        }
    }
}