﻿using System;
using System.Linq;
using Cms.SharedKernel;

namespace Cms.Aggregates.Settings.Events
{
    internal class SettingsEventHandler : EventHandlerBase
    {
        private readonly ISettingsRepository _settingsRepository;
        private readonly IEncryptionService _encryptionService;

        public SettingsEventHandler(ISettingsRepository settingsRepository, IEncryptionService encryptionService)
        {
            _settingsRepository = settingsRepository;
            _encryptionService = encryptionService;
        }

        public void When(EmailSettingsChanged @event)
        {
            var settings = _settingsRepository.GetSettings(@event.AggregateId);
            var smtpPassword = Decrypt(@event.SmtpPassword);

            settings.EmailSettings = new EmailSettings(
                @event.EmailAddress,
                @event.ResponseAddress,
                @event.Host,
                @event.Port,
                @event.IsSmtpAuthenticationRequired,
                @event.IsSslUsedForAuthentication,
                @event.SmtpUsername,
                smtpPassword);
        }

        public void When(UrlShorteningSettingsChanged @event)
        {
            var settings = _settingsRepository.GetSettings(@event.AggregateId);
            var configuration = @event.Configuration.Select(GetDecryptedTuple).ToList();

            settings.UrlShorteningSettings = new UrlShorteningSettings(
                @event.ServiceName,
                configuration);
        }

        private string Decrypt(string encrypted)
        {
            if (string.IsNullOrEmpty(encrypted))
            {
                return string.Empty;
            }

            try
            {
                var bytes = Convert.FromBase64String(encrypted);
                return _encryptionService.DecryptString(bytes);
            }
            catch
            {
                return encrypted;
            }
        }

        private Tuple<string, string> GetDecryptedTuple(Tuple<string, string> tuple)
        {
            return Tuple.Create(tuple.Item1, Decrypt(tuple.Item2));
        }
    }
}