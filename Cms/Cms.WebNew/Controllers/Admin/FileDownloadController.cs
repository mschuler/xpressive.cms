using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cms.Aggregates;
using Cms.Aggregates.Files;
using Cms.Aggregates.FileStore;
using Cms.Aggregates.Websites;
using Cms.Services.Contracts;
using dotless.Core;
using log4net;
using WebApiContrib.Formatting;

namespace Cms.WebNew.Controllers.Admin
{
    [AllowAnonymous]
    public class FileDownloadController : ApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(FileDownloadController));
        private readonly IFileRepository _fileRepository;
        private readonly IFileStoreRepository _fileStoreRepository;
        private readonly IFileTrackingService _fileTrackingService;
        private readonly IWebsiteRepository _websiteRepository;

        public FileDownloadController(IFileRepository fileRepository, IFileStoreRepository fileStoreRepository, IFileTrackingService fileTrackingService, IWebsiteRepository websiteRepository)
        {
            _fileRepository = fileRepository;
            _fileStoreRepository = fileStoreRepository;
            _fileTrackingService = fileTrackingService;
            _websiteRepository = websiteRepository;
        }

        [HttpGet, HttpHead, Route("download/{fileId:ulong}/{version:ulong}/{name}")]
        public IHttpActionResult Download(ulong fileId, ulong version, string name, [FromUri]bool isTracked = true, [FromUri]int size = 0, [FromUri]int quality = 100)
        {
            return Download(fileId, version, null, name, isTracked, size, quality);
        }

        [HttpGet, HttpHead, Route("download/{fileId:ulong}/{version:ulong}/{json}/{name}")]
        public IHttpActionResult Download(ulong fileId, ulong version, string json, string name, [FromUri]bool isTracked = true, [FromUri]int size = 0, [FromUri]int quality = 100)
        {
            File file;
            if (!_fileRepository.TryGet(fileId, out file))
            {
                return NotFound();
            }

            var fileVersion = file.Versions.SingleOrDefault(v => v.Version == version);

            if (fileVersion == null)
            {
                return NotFound();
            }

            Track(file, isTracked);

            IHttpActionResult actionResult;
            if (TryConvert(name, file, version, out actionResult))
            {
                return actionResult;
            }

            var redirectUrl = GetRedirectUrl(file, json, size, quality);

            return Redirect(redirectUrl);
        }

        private Uri GetRedirectUrl(File file, string json, int size, int quality)
        {
            var store = _fileStoreRepository.Get(file.FileStoreId);
            size = Math.Max(0, size);

            try
            {
                if (!string.IsNullOrEmpty(json))
                {
                    var imageSettings = ImageSettingsDto.Deserialize(json);
                    imageSettings.Size = size;
                    imageSettings.Quality = quality;
                    return store.GetUrl(file, imageSettings);
                }
            }
            catch (Exception e)
            {
                _log.Error(e.Message);
            }

            return store.GetUrl(file, new ImageSettingsDto { Size = size });
        }

        private void Track(File file, bool isTracked)
        {
            if (Request.Headers.Referrer != null && Request.Headers.Referrer.LocalPath.StartsWith("/admin", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            if (isTracked && file.IsTracked)
            {
                var host = Request.RequestUri.Host;
                var website = _websiteRepository.GetByHost(host);
                var ipAddress = Request.GetClientIpAddress();
                var sessionId = GetTrackingCookieValue();
                _fileTrackingService.TrackFileViewAsync(website.Id, file.Id, ipAddress, sessionId);
            }
        }

        private string GetTrackingCookieValue()
        {
            var cookies = Request.Headers.GetCookies().ToList();

            foreach (var cookie in cookies)
            {
                foreach (var cookieState in cookie.Cookies)
                {
                    if (cookieState.Name.Equals("tsid", StringComparison.Ordinal))
                    {
                        return cookieState.Value;
                    }
                }
            }

            return string.Empty;
        }

        private bool TryConvert(string filename, File file, ulong version, out IHttpActionResult result)
        {
            try
            {
                if (filename.EndsWith(".css"))
                {
                    if (TryConvertLess(file, version, out result) ||
                        TryConvertSass(file, version, out result))
                    {
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                _log.Error(e.Message, e);
            }

            result = null;
            return false;
        }

        private bool TryConvertLess(File file, ulong version, out IHttpActionResult result)
        {
            var extension = file.GetExtension();
            if (extension.Equals("less", StringComparison.OrdinalIgnoreCase))
            {
                string less;
                if (TryGetFileContent(file, version, out less))
                {
                    var css = Less.Parse(less);
                    result = Content(HttpStatusCode.OK, css, new PlainTextFormatter(), "text/css");
                    return true;
                }
            }

            result = null;
            return false;
        }

        private bool TryConvertSass(File file, ulong version, out IHttpActionResult result)
        {
            var extension = file.GetExtension();
            if (extension.Equals("sass", StringComparison.OrdinalIgnoreCase) ||
                extension.Equals("scss", StringComparison.OrdinalIgnoreCase))
            {
                string location;
                string compiled;
                if (TryGetFileLocation(file, version, out location) && TryGetCompiledFilePath(file, version, out compiled))
                {
                    if (!System.IO.File.Exists(compiled))
                    {
                        var commandArgs = string.Format("--scss --style compressed --sourcemap=none \"{0}\" \"{1}\"", location, compiled);
                        var startInfo = new ProcessStartInfo(@"C:\Ruby22-x64\bin\sass", commandArgs);
                        startInfo.CreateNoWindow = true;
                        var process = Process.Start(startInfo);
                        process.WaitForExit(5000);
                    }

                    if (System.IO.File.Exists(compiled))
                    {
                        var css = System.IO.File.ReadAllText(compiled);

                        result = Content(HttpStatusCode.OK, css, new PlainTextFormatter(), "text/css");
                        return true;
                    }
                }
            }

            result = null;
            return false;
        }

        private bool TryGetCompiledFilePath(File file, ulong version, out string path)
        {
            if (TryGetFileLocation(file, version, out path))
            {
                var fileInfo = new System.IO.FileInfo(path);
                path = fileInfo.FullName.Substring(0, fileInfo.FullName.Length - fileInfo.Extension.Length) + ".css";
                return true;
            }

            return false;
        }

        private bool TryGetFileLocation(File file, ulong version, out string fileLocation)
        {
            var store = _fileStoreRepository.Get(file.FileStoreId);
            if (store.IsLocalFilesystem)
            {
                var location = store.GetLocalFileLocation(file.Id, version);

                if (System.IO.File.Exists(location))
                {
                    fileLocation = location;
                    return true;
                }
            }

            fileLocation = null;
            return false;
        }

        private bool TryGetFileContent(File file, ulong version, out string content)
        {
            string location;

            if (TryGetFileLocation(file, version, out location))
            {
                content = System.IO.File.ReadAllText(location);
                return true;
            }

            content = null;
            return false;
        }
    }
}