﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Cms.EventSourcing.Contracts.Events
{
    public interface IEvent
    {
        [IgnoreDataMember, JsonIgnore]
        ulong UserId { get; set; }

        [IgnoreDataMember, JsonIgnore]
        ulong Version { get; set; }

        [IgnoreDataMember, JsonIgnore]
        Type AggregateType { get; }

        [IgnoreDataMember, JsonIgnore]
        ulong AggregateId { get; set; }

        [IgnoreDataMember, JsonIgnore]
        DateTime Date { get; set; }
    }
}