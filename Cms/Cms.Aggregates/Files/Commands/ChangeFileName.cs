﻿using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Files.Commands
{
    [RequiredPermission("file", PermissionLevel.CanChange)]
    public class ChangeFileName : CommandBase
    {
        [Required(AllowEmptyStrings = false), MinLength(3)]
        public string Name { get; set; }
    }
}