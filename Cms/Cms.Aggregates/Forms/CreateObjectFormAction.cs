﻿using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Forms
{
    public class CreateObjectFormAction : FormActionConfiguration
    {
        public CreateObjectFormAction()
        {
            Mapping = new Dictionary<string, string>();
        }

        public string ObjectDefinitionId { get; set; }
        public string ObjectDefinitionName { get; set; }
        public IDictionary<string, string> Mapping { get; private set; }

        public override FormActionConfiguration Clone()
        {
            return new CreateObjectFormAction
            {
                ObjectDefinitionId = ObjectDefinitionId,
                ObjectDefinitionName = ObjectDefinitionName,
                Mapping = Mapping.Keys.ToDictionary(k => k, k => Mapping[k]),
            };
        }
    }
}