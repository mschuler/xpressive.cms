﻿using System;
using Cms.Aggregates;
using Cms.Aggregates.DynamicObjects;
using Cms.SharedKernel;
using log4net;
using Newtonsoft.Json;

namespace Cms.Services.ObjectFieldContentTypeConverter
{
    internal class ObjectFieldObjectConverter : IObjectFieldContentTypeConverter
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ObjectFieldObjectConverter));
        private readonly IDynamicObjectRepository _repository;
        private readonly IDynamicObjectDefinitionRepository _definitionRepository;
        private readonly IDynamicObjectDtoService _dtoService;

        public ObjectFieldObjectConverter(IDynamicObjectRepository repository, IDynamicObjectDtoService dtoService, IDynamicObjectDefinitionRepository definitionRepository)
        {
            _repository = repository;
            _dtoService = dtoService;
            _definitionRepository = definitionRepository;
        }

        public bool CanHandle(string type)
        {
            return type.Equals("object", StringComparison.OrdinalIgnoreCase);
        }

        public string Serialize(string json)
        {
            DtoWithId dto;

            try
            {
                dto = JsonConvert.DeserializeObject<DtoWithId>(json);
            }
            catch (Exception)
            {
                _log.ErrorFormat("Unable to deserialize to DtoWithId for json={0}", json);
                return "0";
            }

            ulong id;
            if (dto != null && ulong.TryParse(dto.Id, out id))
            {
                return dto.Id;
            }
            return "0";
        }

        public object Deserialize(string value)
        {
            var o = DeserializeToObject(value);
            if (o != null)
            {
                var definition = _definitionRepository.Get(o.DefinitionId);
                return _dtoService.GetDtoWithoutFields(o, definition);
            }
            return null;
        }

        public SortableObjectField DeserializeForSortableField(string value)
        {
            var o = DeserializeToObject(value);
            if (o != null)
            {
                var fullname = o.Title + " (Nr. " + o.PublicId + ")";
                return new SortableObjectField(fullname);
            }
            return new SortableObjectField();
        }

        public object DeserializeForPublicApi(string value)
        {
            return value;
        }

        public bool TryGetMetadataForPublicApi(string value, out object metadata)
        {
            metadata = null;
            return false;
        }

        public object DeserializeForInterpreter(string value)
        {
            return DeserializeToObject(value);
        }

        public string SerializeForImport(string json, string format, string options)
        {
            int publicId;
            if (int.TryParse(json, out publicId))
            {
                var o = _repository.GetByPublicId(publicId);
                if (o != null)
                {
                    return o.Id.ToJsonString();
                }
            }
            return "0";
        }

        public string SerializeForImportPreview(string json, string format, string options)
        {
            int publicId;
            if (int.TryParse(json, out publicId))
            {
                var o = _repository.GetByPublicId(publicId);
                if (o != null)
                {
                    return o.Title + " (Nr. " + o.PublicId + ")";
                }
            }
            return json;
        }

        public bool IsValidValueForImport(string json, string format, string options)
        {
            int publicId;
            return int.TryParse(json, out publicId) && _repository.GetByPublicId(publicId) != null;
        }

        private DynamicObject DeserializeToObject(string value)
        {
            ulong id;
            DynamicObject o;
            if (ulong.TryParse(value, out id) && _repository.TryGet(id, out o))
            {
                return o;
            }
            return null;
        }

        public class DtoWithId
        {
            public string Id { get; set; }
        }
    }
}