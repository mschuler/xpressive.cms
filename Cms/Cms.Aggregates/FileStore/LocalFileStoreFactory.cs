using System;
using System.Collections.Generic;

namespace Cms.Aggregates.FileStore
{
    internal class LocalFileStoreFactory : IFileStoreFactory
    {
        public Guid Id { get { return new Guid("427B9C16-34CA-4617-84A5-923599EC633A"); } }
        public string Name { get { return "Server file system"; } }

        public IFileStore Create()
        {
            return new LocalFileStore();
        }

        public IFileStore Create(ulong id, string name, IDictionary<string, string> settings)
        {
            return new LocalFileStore(settings)
            {
                Id = id,
                Name = name,
                FileStoreFactoryId = Id,
            };
        }

        public bool IsValidConfiguration(IDictionary<string, string> settings)
        {
            return true;
        }
    }
}