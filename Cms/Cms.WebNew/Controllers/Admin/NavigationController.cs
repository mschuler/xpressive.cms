﻿using System.Text;
using System.Web.Http;
using Cms.Aggregates.Security;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class NavigationController : ApiControllerBase
    {
        private readonly IGroupRepository _groupRepository;

        public NavigationController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, IGroupRepository groupRepository)
            : base(commandQueue, userRepository, queryHandler)
        {
            _groupRepository = groupRepository;
        }

        [HttpGet, Route("api/navigation/{tenantId:ulong}")]
        public string Get(ulong tenantId)
        {
            var user = CmsUser;
            var canSeePages = user.HasPermission("page", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeFiles = user.HasPermission("file", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeForms = user.HasPermission("form", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeAppointments = user.HasPermission("appointment", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeCalendarTemplates = user.HasPermission("calendartemplate", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeCalendarCategories = user.HasPermission("calendarcategory", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeObjects = user.HasPermission("object", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeObjectTemplates = user.HasPermission("objecttemplate", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeObjectDefinitions = user.HasPermission("objectdefinition", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeTenants = user.HasPermission("tenant", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeWebsites = user.HasPermission("website", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeePageTemplates = user.HasPermission("pagetemplate", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeUsers = user.HasPermission("user", tenantId, PermissionLevel.CanSee, _groupRepository);
            var canSeeGroups = user.HasPermission("group", tenantId, PermissionLevel.CanSee, _groupRepository);

            var json = new StringBuilder();

            json.Append("{\"dashboard\": true,");

            if (canSeePages || canSeeFiles || canSeeForms)
            {
                json.Append("\"content\": {");
                if (canSeePages) { json.Append("\"page\": true,"); }
                if (canSeeFiles) { json.Append("\"file\": true,"); }
                if (canSeeForms) { json.Append("\"form\": true,"); }
                json.Append("},");
            }

            if (canSeeAppointments || canSeeCalendarCategories || canSeeCalendarTemplates)
            {
                json.Append("\"calendar\": {");
                if (canSeeAppointments) { json.Append("\"appointments\": true,"); }
                if (canSeeCalendarTemplates) { json.Append("\"templates\": true,"); }
                if (canSeeCalendarCategories) { json.Append("\"categories\": true,"); }
                json.Append("},");
            }

            if (canSeeObjects || canSeeObjectTemplates || canSeeObjectDefinitions)
            {
                json.Append("\"object\": {");
                if (canSeeObjects) { json.Append("\"object\": true,"); }
                if (canSeeObjectTemplates) { json.Append("\"template\": true,"); }
                if (canSeeObjectDefinitions) { json.Append("\"definition\": true,"); }
                json.Append("},");
            }

            if (canSeeTenants || canSeeWebsites || canSeePageTemplates)
            {
                json.Append("\"master\": {");
                if (canSeeTenants) { json.Append("\"tenant\": true,"); }
                if (canSeeWebsites) { json.Append("\"website\": true,"); }
                if (canSeePageTemplates) { json.Append("\"pagetemplate\": true,"); }
                json.Append("},");
            }

            if (canSeeUsers || canSeeGroups)
            {
                json.Append("\"user\": {");
                if (canSeeUsers) { json.Append("\"user\": true,"); }
                if (canSeeGroups) { json.Append("\"group\": true,"); }
                json.Append("},");
            }

            if (user.IsSysadmin)
            {
                json.Append("\"system\": {");
                json.Append("\"eventlog\": true,");
                json.Append("\"serverlog\": true,");
                json.Append("\"setting\": true,");
                json.Append("\"maintenance\": true,");
                json.Append("\"configuration\": true,");
                json.Append("\"filestore\":true,");
                json.Append("},");
            }

            json.Append("}");

            json = json.Replace(" ", "");
            json = json.Replace(",}", "}");

            return json.ToString();
        }
    }
}