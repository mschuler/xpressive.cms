﻿namespace Cms.Aggregates.Pages
{
    public class PageLink : PageBase
    {
        public ulong PageId { get; set; }
        public string Url { get; set; }
        public override bool IsLink { get { return true; } }
    }
}