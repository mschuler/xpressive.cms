begin transaction

alter table FormField add Sort int not null default 0;

insert into Version (Major, Minor, Revision) values (0, 0, 5);

commit transaction