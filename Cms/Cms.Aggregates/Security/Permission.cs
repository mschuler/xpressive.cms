﻿namespace Cms.Aggregates.Security
{
    public class Permission
    {
        public Permission(string moduleId, PermissionLevel permissionLevel)
        {
            PermissionLevel = permissionLevel;
            ModuleId = moduleId;
        }

        public string ModuleId { get; private set; }
        public PermissionLevel PermissionLevel { get; private set; }
    }
}