﻿using System;
using RestSharp;

namespace Cms.Services.UrlShortening
{
    internal class OwlyShorteningService : UrlShorteningServiceBase
    {
        private const string ApiKeyKey = "API Key";

        public OwlyShorteningService(IUrlShorteningRepository urlShorteningRepository)
            : base(new[] { ApiKeyKey }, urlShorteningRepository) { }

        public override string Name { get { return "ow.ly"; } }

        protected override string ShortenInternal(string longUrl)
        {
            string apiKey;
            if (!TryGetConfigurationValue(ApiKeyKey, out apiKey) || string.IsNullOrEmpty(apiKey))
            {
                throw new InvalidOperationException("Invalid configuration.");
            }

            var client = new RestClient("http://ow.ly/api/1.1/");

            var request = new RestRequest("url/shorten", Method.GET);
            request.AddQueryParameter("longUrl", longUrl);
            request.AddQueryParameter("apiKey", apiKey);

            var response = client.Execute<OwlyResult>(request);

            if (response == null)
            {
                throw new InvalidOperationException("Unable to access service.");
            }

            if (response.Data == null || response.Data.Result == null)
            {
                throw new InvalidOperationException("Unable to parse JSON result.");
            }

            return response.Data.Result.ShortUrl;
        }

        public class OwlyResult
        {
            public OwlyResultData Result { get; set; }
        }

        public class OwlyResultData
        {
            public string Hash { get; set; }
            public string LongUrl { get; set; }
            public string ShortUrl { get; set; }
        }
    }
}