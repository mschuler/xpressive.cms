﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Pages
{
    public abstract class PageBase : AggregateBase
    {
        public static HashSet<string> InvalidRootPageNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "admin", "api", "signalr", "__browserlink", "metrics", "download"
        };

        private readonly object _childrenLock = new object();
        private readonly IList<PageBase> _children;

        protected PageBase()
        {
            _children = new List<PageBase>();
        }

        public ulong WebsiteId { get; set; }
        public string Name { get; set; }
        public ulong ParentPageId { get; set; }
        public virtual bool IsLink { get { return false; } }
        public virtual string Fullname { get { return Name; } }
        public DateTime LastChange { get; set; }
        public int SortOrder { get; set; }

        public IEnumerable<PageBase> Children
        {
            get
            {
                lock (_childrenLock)
                {
                    return _children.ToList();
                }
            }
        }

        public void Add(PageBase page)
        {
            lock (_childrenLock)
            {
                _children.Add(page);
            }
        }

        public void Remove(PageBase page)
        {
            lock (_childrenLock)
            {
                _children.Remove(page);
            }
        }
    }
}