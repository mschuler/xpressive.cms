﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cms.Aggregates.Security;
using Cms.EventSourcing.Contracts.Queries;
using Cms.SharedKernel;
using log4net;

namespace Cms.WebNew.Controllers
{
    public class QueryHandler : IQueryHandler
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(QueryHandler));
        private readonly IUserRepository _userRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly IList<EventSourcing.Contracts.Queries.IQueryHandler> _queryHandlers;

        public QueryHandler(IUserRepository userRepository, IGroupRepository groupRepository, IEnumerable<EventSourcing.Contracts.Queries.IQueryHandler> queryHandlers)
        {
            _userRepository = userRepository;
            _groupRepository = groupRepository;
            _queryHandlers = queryHandlers.ToList();
        }

        public IEnumerable<object> Handle(IQuery query, HttpRequestMessage request)
        {
            var user = _userRepository.Get(query.UserId);
            var type = query.GetType();
            if (user.IsLocked || !CheckUserRequirementsForRequiredPermissionAttribute.IsAllowedToExecute(type, user, query.TenantId, _groupRepository))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized).WithGuid();
            }

            var handler = _queryHandlers.SingleOrDefault(h => h.CanHandle(query));

            LogQuery(query, user, request, handler);

            if (handler != null)
            {
                return handler.Handle(query);
            }

            return null;
        }

        private void LogQuery(IQuery command, User user, HttpRequestMessage request, EventSourcing.Contracts.Queries.IQueryHandler queryHandler)
        {
            if (queryHandler == null)
            {
                _logger.WarnFormat(
                    "Execute query {0} from user {1} with ip address {2} and referrer {3} but is not handled by any query handler",
                    command.GetType().Name,
                    user.FullName,
                    request.GetClientIpAddress(),
                    request.GetReferrer());
            }
            else
            {
                _logger.InfoFormat(
                    "Execute query {0} from user {1} with ip address {2} and referrer {3} handled by {4}",
                    command.GetType().Name,
                    user.FullName,
                    request.GetClientIpAddress(),
                    request.GetReferrer(),
                    queryHandler.GetType().Name);
            }
        }
    }
}