namespace Cms.Aggregates.Security.Events
{
    public class GroupMemberAdded : EventBase<Group>
    {
        public ulong MemberId { get; set; }
    }
}