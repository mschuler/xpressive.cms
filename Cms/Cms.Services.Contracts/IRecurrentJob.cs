﻿namespace Cms.Services.Contracts
{
    public interface IRecurrentJob
    {
        string CronTab { get; }

        void Execute();
    }
}
