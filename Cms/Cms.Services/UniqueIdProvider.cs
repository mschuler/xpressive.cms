﻿using System;
using Cms.SharedKernel;
using UniqueIdGenerator.Net;

namespace Cms.Services
{
    internal class UniqueIdProvider : IUniqueIdProvider
    {
        private static readonly Generator _generator = new Generator(0, new DateTime(2014, 10, 10));

        public ulong GetId()
        {
            return _generator.NextLong();
        }
    }
}