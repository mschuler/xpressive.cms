using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Filters;
using Cms.EventSourcing.Services;
using Newtonsoft.Json.Serialization;

namespace Cms.WebNew.Controllers.RoutingAndAttributes
{
    public class ExceptionWithReasonFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is ExceptionWithReason)
            {
                var formatter = new JsonMediaTypeFormatter();
                formatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

                context.Response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new ObjectContent(typeof(ErrorMessage), new ErrorMessage(context), formatter)
                };
            }
            else if (context.Exception is HttpResponseException)
            {
                var e = (HttpResponseException)context.Exception;
                var httpCode = e.Response.StatusCode;
                if (httpCode == HttpStatusCode.Forbidden ||
                    httpCode == HttpStatusCode.Unauthorized)
                {
                    context.Response = new HttpResponseMessage(httpCode);
                }
            }
            else
            {
                var response = new HttpResponseMessage(HttpStatusCode.InternalServerError);

                if (context.Exception.Data.Contains("guid"))
                {
                    response.Content = new StringContent((string)context.Exception.Data["guid"]);
                }

                context.Response = response;
            }
            base.OnException(context);
        }

        public class ErrorMessage
        {
            public ErrorMessage(HttpActionExecutedContext context)
            {
                Message = context.Exception.Message;
            }

            public string Message { get; set; }
        }
    }
}