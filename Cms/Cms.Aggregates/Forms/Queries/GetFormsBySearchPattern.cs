using System;
using System.Linq;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Forms.Queries
{
    [RequiredPermission("form", PermissionLevel.CanSee)]
    public class GetFormsBySearchPattern : QueryBase
    {
        public string SearchPattern { get; set; }

        internal bool IsMatch(Form form)
        {
            var parts = SearchPattern.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return parts.All(a =>
                form.Name.ToUpperInvariant().Contains(a.ToUpperInvariant()) ||
                (form.PublicId.ToString("D").Contains(a)));
        }
    }
}