namespace Cms.Aggregates.Pages.Commands
{
    using System.ComponentModel.DataAnnotations;

    using Cms.Aggregates.Security;

    [RequiredPermission("page", PermissionLevel.CanChange)]
    public class RestorePageVersion : EventBase<PageBase>
    {
        [Required, ValidId]
        public ulong VersionToRestore { get; set; }
    }
}