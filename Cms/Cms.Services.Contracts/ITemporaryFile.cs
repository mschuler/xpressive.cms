﻿using System.IO;

namespace Cms.Services.Contracts
{
    public interface ITemporaryFile
    {
        ulong Id { get; }
        FileInfo File { get; }
    }
}