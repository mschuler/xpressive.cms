﻿using System.Collections.Generic;
using Cms.EventSourcing.Contracts.Commands;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.WebNew.Controllers
{
    public static class LocalEventStore
    {
        private static readonly IEventStore _eventStore;
        private static readonly EventSourcing.EventSourcing _eventSourcing;

        static LocalEventStore()
        {
            _eventStore = ServiceLocator.Resolve<IEventStore>();
            _eventSourcing = new EventSourcing.EventSourcing(_eventStore);
        }

        public static void RestoreModelFromEventStore()
        {
            _eventSourcing.RestoreModelFromEventStore();
        }

        public static void Command(ICommand command)
        {
            _eventSourcing.Invoke(command);
        }

        public static IEnumerable<IEvent> GetEvents(int skip, int take)
        {
            return _eventStore.Fetch(skip, take);
        }

        public static IEnumerable<IEvent> GetAllEvents()
        {
            return _eventStore.Fetch();
        }

        public static IEnumerable<IEvent> GetEventsByAggregate(ulong aggregateId)
        {
            return _eventStore.Fetch(aggregateId);
        }
    }
}