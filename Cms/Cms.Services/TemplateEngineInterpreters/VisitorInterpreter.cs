﻿using System;
using Cms.Services.Contracts;
using Cms.TemplateEngine;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.Services.TemplateEngineInterpreters
{
    internal class VisitorInterpreter : IInterpreter
    {
        private readonly IIpToCountryService _ipToCountryService;

        public VisitorInterpreter(IIpToCountryService ipToCountryService)
        {
            _ipToCountryService = ipToCountryService;
        }

        public int Priority { get; set; }

        public string Name
        {
            get { return "Visitor"; }
        }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function && tree.Value.Equals("visitor", StringComparison.OrdinalIgnoreCase))
            {
                tree.Result = new VisitorDummyObject();
            }
            else if (tree.ItemType == TreeItemType.Attribute && tree.LeftItem != null && tree.LeftItem.Result is VisitorDummyObject)
            {
                var result = string.Empty;
                var ip = TemplateStateMap.Get<string>("Visitor.IpAddress");

                switch (tree.Value.ToLowerInvariant())
                {
                    case "ip":
                    case "ipaddress":
                        result = ip;
                        break;
                    case "country":
                        var task = _ipToCountryService.GetCountryAsync(ip);
                        task.RunSynchronously();
                        result = task.Result;
                        break;
                }

                tree.Result = result;
            }
        }

        private class VisitorDummyObject { }
    }
}
