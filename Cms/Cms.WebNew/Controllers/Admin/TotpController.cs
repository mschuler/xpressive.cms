using System;
using System.Web.Http;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Commands;
using Cms.SharedKernel;

namespace Cms.WebNew.Controllers.Admin
{
    [Authorize]
    public class TotpController : ApiControllerBase
    {
        private readonly ITotpService _totpService;

        public TotpController(ICommandQueue commandQueue, IUserRepository userRepository, IQueryHandler queryHandler, ITotpService totpService)
            : base(commandQueue, userRepository, queryHandler)
        {
            _totpService = totpService;
        }

        [HttpGet, Route("api/2fa/totp")]
        public TotpDto Get()
        {
            var username = CmsUser.EmailAddress;
            var totp = _totpService.Generate(username);

            var secret = string.Join(
                " ",
                totp.SecretBase32.Substring(0, 4),
                totp.SecretBase32.Substring(4, 4),
                totp.SecretBase32.Substring(8, 4),
                totp.SecretBase32.Substring(12, 4),
                totp.SecretBase32.Substring(16, 4),
                totp.SecretBase32.Substring(20, 4),
                totp.SecretBase32.Substring(24, 4),
                totp.SecretBase32.Substring(28, 4));

            return new TotpDto
            {
                Secret = secret,
                Url = totp.Url,
                Secret64 = Convert.ToBase64String(totp.Secret)
            };
        }

        [HttpPut, Route("api/2fa/totp")]
        public TotpResult Save([FromBody]TotpDto dto)
        {
            var secret = Convert.FromBase64String(dto.Secret64);
            var isValid = _totpService.Verify(secret, dto.Code);

            try
            {
                SendCommand(new EnableTotp2Fa
                {
                    AggregateId = CmsUser.Id,
                    TenantId = 1,
                    TotpSecret = secret,
                    TotpCode = dto.Code,
                    Password = dto.Password
                });
            }
            catch
            {
                isValid = false;
            }

            return new TotpResult
            {
                IsSuccessful = isValid
            };
        }

        [HttpDelete, Route("api/2fa/totp")]
        public void Remove()
        {
            SendCommand(new DisableTotp2Fa
            {
                AggregateId = CmsUser.Id,
                TenantId = 1
            });
        }

        [HttpDelete, Route("api/2fa/totp/{userId:ulong}")]
        public void Remove(ulong userId)
        {
            SendCommand(new DisableTotp2FaForOtherUser
            {
                AggregateId = userId,
                TenantId = 1
            });
        }

        public class TotpDto
        {
            public string Url { get; set; }
            public string Secret { get; set; }
            public string Secret64 { get; set; }
            public string Code { get; set; }
            public string Password { get; set; }
        }

        public class TotpResult
        {
            public bool IsSuccessful { get; set; }
        }
    }
}