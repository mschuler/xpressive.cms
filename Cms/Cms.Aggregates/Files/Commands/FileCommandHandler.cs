using System;
using System.IO;
using Cms.Aggregates.FileDirectories;
using Cms.Aggregates.Files.Events;
using Cms.Aggregates.FileStore;
using Cms.EventSourcing.Contracts.Events;
using Cms.EventSourcing.Services;
using Cms.SharedKernel;

namespace Cms.Aggregates.Files.Commands
{
    public class FileCommandHandler : CommandHandlerBase
    {
        private readonly IFileRepository _repository;
        private readonly IRepository<FileDirectory> _directoryRepository;
        private readonly IFileStoreRepository _fileStoreRepository;
        private readonly IFileHashCalculator _fileHashCalculator;

        public FileCommandHandler(IFileRepository repository, IRepository<FileDirectory> directoryRepository, IFileStoreRepository fileStoreRepository, IFileHashCalculator fileHashCalculator)
        {
            _repository = repository;
            _directoryRepository = directoryRepository;
            _fileStoreRepository = fileStoreRepository;
            _fileHashCalculator = fileHashCalculator;
        }

        public void When(CreateFile command, Action<IEvent> eventHandler)
        {
            _repository.AssertIdIsNotUsed(command.AggregateId);

            var publicId = _repository.GetNextPublicId();
            var name = command.Name.Trim().RemoveControlChars();

            command.TemporaryFilePath = FileValidator.ValidateFile(command.TemporaryFilePath, name);

            var directory = _directoryRepository.Get(command.FileDirectoryId);
            var temporaryFile = new FileInfo(command.TemporaryFilePath);
            var hash = CreateFileVersion(directory.FileStoreId, name, command.AggregateId, command.FileVersion, temporaryFile);

            var e = new FileCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                FileDirectoryId = command.FileDirectoryId,
                PublicId = publicId,
                Name = name,
                FileVersion = command.FileVersion,
                Hash = hash,
                Size = (ulong)temporaryFile.Length,
            };

            temporaryFile.Delete();

            eventHandler(e);
        }

        public void When(CreateFileVersion command, Action<IEvent> eventHandler)
        {
            var file = _repository.Get(command.AggregateId);

            command.TemporaryFilePath = FileValidator.ValidateFile(command.TemporaryFilePath, file.Name);

            var temporaryFile = new FileInfo(command.TemporaryFilePath);
            var hash = CreateFileVersion(file.FileStoreId, file.Name, command.AggregateId, command.FileVersion, temporaryFile);

            var e = new FileVersionCreated
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                FileVersion = command.FileVersion,
                Hash = hash,
                Size = (ulong)temporaryFile.Length,
            };

            temporaryFile.Delete();

            eventHandler(e);
        }

        public void When(ChangeFileName command, Action<IEvent> eventHandler)
        {
            var file = _repository.Get(command.AggregateId);
            var name = command.Name.Trim().RemoveControlChars();

            var oldExtension = file.GetExtension().ToLowerInvariant();
            var newExtension = name.GetExtension().ToLowerInvariant();

            if (!oldExtension.Equals(newExtension, StringComparison.OrdinalIgnoreCase))
            {
                var withoutExtension = name.Substring(0, name.Length - newExtension.Length);
                name = withoutExtension + oldExtension;
            }

            var e = new FileNameChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                Name = name,
            };

            eventHandler(e);
        }

        public void When(ChangeFileDirectory command, Action<IEvent> eventHandler)
        {
            var file = _repository.Get(command.AggregateId);
            var oldTenant = _directoryRepository.Get(file.FileDirectoryId).TenantId;
            var newDirectory = _directoryRepository.Get(command.FileDirectoryId);
            var newTenant = newDirectory.TenantId;

            if (!oldTenant.Equals(newTenant))
            {
                throw new ExceptionWithReason("You can't move a file from one tenant to another.");
            }

            if (!file.FileStoreId.Equals(newDirectory.FileStoreId))
            {
                throw new ExceptionWithReason("You can't move a file to another file storage.");
            }

            var e = new FileDirectoryChanged
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                FileDirectoryId = command.FileDirectoryId,
            };

            eventHandler(e);
        }

        public void When(EnableFileTracking command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            eventHandler(new FileTrackingEnabled
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            });
        }

        public void When(DisableFileTracking command, Action<IEvent> eventHandler)
        {
            _repository.Get(command.AggregateId);

            eventHandler(new FileTrackingDisabled
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
            });
        }

        public void When(DeleteFile command, Action<IEvent> eventHandler)
        {
            var file = _repository.Get(command.AggregateId);

            var e = new FileDeleted
            {
                AggregateId = command.AggregateId,
                UserId = command.UserId,
                PublicId = file.PublicId,
                Name = file.Name,
            };

            eventHandler(e);
        }

        private string CreateFileVersion(ulong fileStoreId, string fileName, ulong fileId, ulong fileVersion, FileInfo temporaryFile)
        {
            var fileStore = _fileStoreRepository.Get(fileStoreId);
            var hash = _fileHashCalculator.Sha1AsBase64String(temporaryFile);

            fileStore.Save(fileName, fileId, fileVersion, temporaryFile);

            return hash;
        }
    }
}