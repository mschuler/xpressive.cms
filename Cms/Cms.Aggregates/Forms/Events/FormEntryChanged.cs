﻿using System.Collections.Generic;

namespace Cms.Aggregates.Forms.Events
{
    public class FormEntryChanged : EventBase<Form>
    {
        public ulong FormEntryId { get; set; }

        public Dictionary<string, string> Values { get; set; }
    }
}