﻿namespace Cms.Aggregates.Forms.Events
{
    public class FormActionRemoved : EventBase<Form>
    {
        public ulong Id { get; set; }
    }
}