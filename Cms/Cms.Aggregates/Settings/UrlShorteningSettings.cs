using System;
using System.Collections.Generic;

namespace Cms.Aggregates.Settings
{
    public class UrlShorteningSettings
    {
        public UrlShorteningSettings() { }

        public UrlShorteningSettings(string serviceName, IEnumerable<Tuple<string, string>> configuration)
        {
            ServiceName = serviceName;

            var config = new List<Tuple<string, string>>();

            foreach (var tuple in configuration)
            {
                config.Add(Tuple.Create(tuple.Item1, tuple.Item2));
            }

            Configuration = config;
        }

        public string ServiceName { get; private set; }

        public IEnumerable<Tuple<string, string>> Configuration { get; private set; }
    }
}