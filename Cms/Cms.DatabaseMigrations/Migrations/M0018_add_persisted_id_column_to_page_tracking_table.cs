namespace Cms.DatabaseMigrations.Migrations
{
    internal class M0018_add_persisted_id_column_to_page_tracking_table : DatabaseSqlMigration
    {
        public M0018_add_persisted_id_column_to_page_tracking_table() : base(18) { }

        protected override string GetSql()
        {
            return "alter table PageTracking add PersistedId nvarchar(50) not null default('')";
        }
    }
}