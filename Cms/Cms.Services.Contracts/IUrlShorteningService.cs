﻿using System.Collections.Generic;

namespace Cms.Services.Contracts
{
    public interface IUrlShorteningService
    {
        string Name { get; }

        IEnumerable<string> GetConfigurationKeys();

        void SetConfigurationValue(string key, string value);

        /// <summary>
        /// Connect to url shortening service and tries to shorten
        /// a default url with the given configuration values.
        /// </summary>
        bool VerifyConfiguration();

        /// <summary>
        /// If shortening fails, return longUrl.
        /// </summary>
        string ShortenUrl(string longUrl, bool useCache = true);
    }
}
