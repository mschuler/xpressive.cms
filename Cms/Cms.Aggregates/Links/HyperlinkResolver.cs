using System;
using System.Text.RegularExpressions;

namespace Cms.Aggregates.Links
{
    internal class HyperlinkResolver : ILinkResolverStrategy
    {
        private static readonly Regex _regex = new Regex(@"^[a-z]{2,5}://", RegexOptions.Compiled | RegexOptions.IgnoreCase, TimeSpan.FromSeconds(1));

        public bool CanHandle(ILink link)
        {
            return link.GetType() == typeof(Hyperlink);
        }

        public string GetHyperlink(ILink link)
        {
            var l = (Hyperlink)link;
            if (string.IsNullOrEmpty(l.Url))
            {
                return string.Empty;
            }

            if (_regex.IsMatch(l.Url))
            {
                return l.Url;
            }
            return "http://" + l.Url;
        }
    }
}