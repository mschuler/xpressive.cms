using Cms.Aggregates.Security;

namespace Cms.Aggregates.Pages.Queries
{
    [RequiredPermission("page", PermissionLevel.CanSee)]
    public class GetStarredPages : QueryBase
    {
        public ulong WebsiteId { get; set; }
    }
}