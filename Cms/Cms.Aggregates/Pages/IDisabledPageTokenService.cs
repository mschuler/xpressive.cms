namespace Cms.Aggregates.Pages
{
    /// <summary>
    /// Generate and validate tokens to view pages which are disabled.
    /// </summary>
    public interface IDisabledPageTokenService
    {
        string GetToken(ulong pageId);

        bool IsValidToken(ulong pageId, string token);
    }
}