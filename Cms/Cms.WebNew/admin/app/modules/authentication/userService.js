﻿(function () {
    var app = angular.module("app");
    app.provider("userService", function() {
        var user = null;

        this.$get = ["$http", "$log", "$rootScope", "$location", "$q", "currentUser", function ($http, $log, $rootScope, $location, $q, currentUser) {

            user = currentUser;

            function setCurrentUser(u) {
                angular.module("app").value("currentUser", u);
                user = u;
                $rootScope.$broadcast("currentUserChanged", user);
            }

            return {
                logout: function() {
                    setCurrentUser(null);
                },
                lockout: function () {
                    // keep the user data.
                },
                currentUser: function() {
                    return user;
                },
                refreshLoginData: function () {
                    var deferred = $q.defer();
                    $http.get("/api/user/current", { cache: false })
                        .success(function (data) {
                            setCurrentUser(data);
                            deferred.resolve(data);
                        })
                        .error(function (err) {
                            setCurrentUser(null);
                            deferred.reject(err);
                        });
                    return deferred.promise;
                }
            };
        }];
    });
}());