using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using Cms.Aggregates.Pages;
using Cms.SharedKernel;
using log4net;
using WebApiContrib.ResponseMessages;

namespace Cms.WebNew.Controllers.PageRendering
{
    public class ValidateUrl : IFilter<RenderPageMessage>
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ValidateUrl));
        private readonly IPageRepository _pageRepository;
        private readonly IPageUrlService _pageUrlService;

        public ValidateUrl(IPageRepository pageRepository, IPageUrlService pageUrlService)
        {
            _pageRepository = pageRepository;
            _pageUrlService = pageUrlService;
        }

        public bool Execute(RenderPageMessage msg)
        {
            Guid permalinkId;
            if (TryGetPermalinkId(msg, out permalinkId))
            {
                msg.Page = _pageRepository
                    .GetAll()
                    .OfType<Page>()
                    .Where(p => p.WebsiteId == msg.Website.Id)
                    .SingleOrDefault(p => p.Permalink.Equals(permalinkId));
            }

            string redirectUrl;
            var pageByUrl = GetPageByUrl(msg);

            if (TryGetPageLink(msg, pageByUrl, out redirectUrl))
            {
                msg.Response = new RedirectResponse(new Uri(redirectUrl, UriKind.Absolute));
                return false;
            }

            if (msg.Page == null)
            {
                msg.Page = GetPage(msg, GetPageByUrl(msg));
            }

            if (msg.Page == null)
            {
                var errorPage = _pageRepository
                    .GetAllByWebsiteId(msg.Website.Id)
                    .SingleOrDefault(p => p.ParentPageId == 0 && string.Equals(p.Name, "404", StringComparison.Ordinal));

                var errorPageLink = errorPage as PageLink;

                if (errorPageLink != null)
                {
                    msg.Response = new RedirectResponse(new Uri(errorPageLink.Url, UriKind.Absolute));
                    return false;
                }

                msg.Page = errorPage as Page;
                msg.StatusCode = HttpStatusCode.NotFound;
            }

            if (msg.Page == null)
            {
                msg.Response = new HttpResponseMessage(HttpStatusCode.NotFound);
                return false;
            }

            var originalUriString = _pageUrlService.GetUrl(msg.Page);
            var currentUriString = "/" + string.Join("/", msg.RouteParts);
            var fullUrl = msg.Request.RequestUri.Scheme + "://" + msg.Website.DefaultDomain.Host;

            if (!msg.Request.RequestUri.IsDefaultPort)
            {
                fullUrl += ":" + msg.Request.RequestUri.Port;
            }

            fullUrl += originalUriString;

            if (!string.IsNullOrEmpty(msg.Request.RequestUri.Query))
            {
                fullUrl += msg.Request.RequestUri.Query;
            }

            if (!msg.Domain.Equals(msg.Website.DefaultDomain))
            {
                msg.Response = new RedirectResponse(new Uri(fullUrl, UriKind.Absolute));
                return false;
            }

            if (msg.Page.Name.Equals("404", StringComparison.Ordinal))
            {
                return true;
            }

            if (!string.Equals(originalUriString, currentUriString, StringComparison.Ordinal))
            {
                if (msg.Website.DefaultPageId == msg.Page.Id && msg.RouteParts.Length == 0)
                {
                    return true;
                }

                msg.Response = new RedirectResponse(new Uri(fullUrl, UriKind.Absolute));
                return false;
            }

            return true;
        }

        private bool TryGetPageLink(RenderPageMessage msg, PageBase pageBase, out string redirectUrl)
        {
            var loopCount = 0;
            redirectUrl = null;

            while (pageBase is PageLink)
            {
                var pageLink = (PageLink)pageBase;

                if (!string.IsNullOrEmpty(pageLink.Url))
                {
                    redirectUrl = pageLink.Url;
                    return true;
                }

                pageBase = _pageRepository.Get(pageLink.PageId);
                loopCount++;

                if (loopCount > 100)
                {
                    _log.WarnFormat("Loop in PageLink resolver for {0}.", msg.Request.RequestUri);
                    return false;
                }
            }

            return false;
        }

        private Page GetPage(RenderPageMessage msg, PageBase pageBase)
        {
            var loopCount = 0;

            while (pageBase is PageLink)
            {
                var pageLink = (PageLink)pageBase;

                pageBase = _pageRepository.Get(pageLink.PageId);
                loopCount++;

                if (loopCount > 100)
                {
                    _log.WarnFormat("Loop in PageLink resolver for {0}.", msg.Request.RequestUri);
                    return null;
                }
            }

            return pageBase as Page;
        }

        private PageBase GetPageByUrl(RenderPageMessage msg)
        {
            var pageNames = msg.RouteParts.ToList();
            var pages = _pageRepository.GetAllByWebsiteId(msg.Website.Id).Where(p => p.ParentPageId == 0).ToList();

            PageBase page;

            if (msg.RouteParts.Length == 0 && _pageRepository.TryGet(msg.Website.DefaultPageId, out page))
            {
                return page;
            }

            while (true)
            {
                if (pageNames.Count == 0 || pages.Count == 0)
                {
                    return null;
                }

                var name = pageNames[0];
                pageNames.RemoveAt(0);

                page = pages.SingleOrDefault(p => p.Name.ToValidUrlPart().Equals(name.ToValidUrlPart(), StringComparison.OrdinalIgnoreCase));

                if (page != null && pageNames.Count > 0)
                {
                    pages = page.Children.ToList();
                    continue;
                }

                return page;
            }
        }

        private static bool TryGetPermalinkId(RenderPageMessage msg, out Guid guid)
        {
            guid = Guid.Empty;
            return msg.RouteParts.Length == 1 && Guid.TryParse(msg.RouteParts[0], out guid);
        }
    }
}