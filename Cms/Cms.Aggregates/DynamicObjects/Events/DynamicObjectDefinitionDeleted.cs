namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectDefinitionDeleted : EventBase<DynamicObjectDefinition>
    {
        public int PublicId { get; set; }
        public string Name { get; set; }
    }
}