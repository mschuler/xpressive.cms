﻿namespace Cms.Aggregates.Security.Commands
{
    [RequiredPermission("group", PermissionLevel.CanEverything)]
    public class DeleteGroup : CommandBase { }
}