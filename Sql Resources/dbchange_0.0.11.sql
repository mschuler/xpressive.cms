begin transaction

alter table Page add Sort int not null default 0;
alter table Page drop column IsDefault;
alter table Page drop column IsPublished;
alter table Form add Guid uniqueidentifier not null;

insert into Version (Major, Minor, Revision) values (0, 0, 11);

commit transaction
