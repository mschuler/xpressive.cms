﻿namespace Cms.EventSourcing.Contracts.Queries
{
    public interface IQuery
    {
        ulong UserId { get; set; }

        ulong TenantId { get; set; }
    }
}