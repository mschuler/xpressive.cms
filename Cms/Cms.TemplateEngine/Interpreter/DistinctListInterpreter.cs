using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    public sealed class DistinctListInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "distinct"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function &&
                tree.Value.Equals("distinct", StringComparison.OrdinalIgnoreCase) &&
                tree.LeftItem != null &&
                tree.LeftItem.Result is IEnumerable<object> &&
                tree.Children.Count == 0)
            {
                var result = ((IEnumerable)tree.LeftItem.Result).GetListOfSameType();
                result.AddRange(((IEnumerable<object>)tree.LeftItem.Result).Distinct());
                tree.Result = result;
            }
        }
    }
}