using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Pages.Events
{
    using Cms.EventSourcing.Contracts.Events;

    public class PageEventHandler : EventHandlerBase
    {
        private readonly IPageRepository _repository;
        private readonly IEventStore _eventStore;
        private readonly object _lock = new object();

        public PageEventHandler(IPageRepository repository, IEventStore eventStore)
        {
            _repository = repository;
            _eventStore = eventStore;
        }

        public void When(PageCreated @event)
        {
            lock (_lock)
            {
                var page = new Page
                {
                    Id = @event.AggregateId,
                    WebsiteId = @event.WebsiteId,
                    Name = @event.Name,
                    Title = @event.Title,
                    Permalink = @event.Permalink,
                    ParentPageId = @event.ParentPageId,
                    IsEnabled = false,
                    PublicId = @event.PublicId,
                    LastChange = @event.Date,
                };

                _repository.Add(page);
                AddPageToParent(page);
            }
        }

        public void When(PageNameChanged @event)
        {
            var page = _repository.Get(@event.AggregateId);
            page.Name = @event.Name;
            page.LastChange = @event.Date;
        }

        public void When(PageTitleChanged @event)
        {
            var page = (Page)_repository.Get(@event.AggregateId);
            page.Title = @event.Title;
            page.LastChange = @event.Date;
        }

        public void When(PageContentChanged @event)
        {
            var page = (Page)_repository.Get(@event.AggregateId);
            page.LastChange = @event.Date;

            page.Add(new PageContent
            {
                Name = @event.Name,
                Value = @event.Value
            });
        }

        public void When(PageMoved @event)
        {
            lock (_lock)
            {
                var page = _repository.Get(@event.AggregateId);
                page.LastChange = @event.Date;
                SetParent(page, @event.ParentPageId, @event.SortOrder);
            }
        }

        public void When(PageDeleted @event)
        {
            lock (_lock)
            {
                PageBase page;
                _repository.TryGetEvenIfDeleted(@event.AggregateId, out page);
                _repository.Delete(@event.AggregateId);

                PageBase parent;
                if (page != null && _repository.TryGetEvenIfDeleted(page.ParentPageId, out parent))
                {
                    parent.Remove(page);
                }
            }
        }

        public void When(PageTemplateChanged @event)
        {
            var page = (Page)_repository.Get(@event.AggregateId);
            page.PageTemplateId = @event.PageTemplateId;
            page.LastChange = @event.Date;
        }

        public void When(PageEnabled @event)
        {
            var page = (Page)_repository.Get(@event.AggregateId);
            page.IsEnabled = true;
            page.LastChange = @event.Date;
        }

        public void When(PageDisabled @event)
        {
            var page = (Page)_repository.Get(@event.AggregateId);
            page.IsEnabled = false;
            page.LastChange = @event.Date;
        }

        public void When(PageLinkCreated @event)
        {
            lock (_lock)
            {
                var page = new PageLink
                {
                    Id = @event.AggregateId,
                    WebsiteId = @event.WebsiteId,
                    Name = @event.Name,
                    ParentPageId = @event.ParentPageId,
                    PageId = @event.PageId,
                    LastChange = @event.Date,
                    Url = @event.Url,
                };

                _repository.Add(page);
                AddPageToParent(page);
            }
        }

        public void When(PageLinkChanged @event)
        {
            var page = (PageLink)_repository.Get(@event.AggregateId);
            page.Name = @event.Name;
            page.PageId = @event.PageId;
            page.LastChange = @event.Date;
            page.Url = @event.Url;
        }

        public void When(PageVersionRestored @event)
        {
            _repository.Delete(@event.AggregateId);

            var events = _eventStore.Fetch(@event.AggregateId).ToList();
            events = events.TakeWhile(e => e.Version <= @event.VersionToRestore).ToList();

            EventSourcing.EventSourcing.RestoreModelFromEvents(events);
        }

        private IList<PageBase> GetSiblings(PageBase page)
        {
            var siblings = _repository
                .GetAllByWebsiteId(page.WebsiteId)
                .Where(p => p.ParentPageId == page.ParentPageId)
                .Where(p => p.Id != page.Id)
                .ToList();

            return siblings;
        }

        private void SetParent(PageBase page, ulong newParentPageId, int sortOrder)
        {
            PageBase parent;
            if (_repository.TryGet(page.ParentPageId, out parent))
            {
                parent.Remove(page);
            }

            page.ParentPageId = newParentPageId;
            if (_repository.TryGet(newParentPageId, out parent))
            {
                parent.Add(page);
            }

            var siblings = GetSiblings(page);
            siblings = siblings.OrderBy(s => s.SortOrder).ThenBy(s => s.Name).ToList();
            siblings.Insert(sortOrder, page);

            for (var i = 0; i < siblings.Count; i++)
            {
                siblings[i].SortOrder = i;
            }
        }

        private void AddPageToParent(PageBase page)
        {
            var parent = page.ParentPageId != 0 ? _repository.Get(page.ParentPageId) : null;
            var siblings = GetSiblings(page);

            if (parent != null)
            {
                parent.Add(page);
            }

            if (siblings.Any())
            {
                page.SortOrder = siblings.Max(s => s.SortOrder);
            }
        }
    }
}