namespace Cms.Aggregates.Pages.Events
{
    public class PageTitleChanged : EventBase<PageBase>
    {
        public string Title { get; set; }
    }
}