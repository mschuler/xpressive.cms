begin transaction

create table TemplateParameter (
	Id int not null identity(1, 1),
	[Name] nvarchar(128) not null,
	[Type] nvarchar(64) not null default '',
	TemplateId int not null,

	constraint PK_TemplateParameter primary key (Id),
	constraint UX_TemplateParameter unique ([Name], TemplateId),
	constraint FK_TemplateParameter_Template foreign key (TemplateId) references Template (Id)
)

create table TemplateParameterOption (
	Id int not null identity(1, 1),
	[Key] nvarchar(64) not null,
	TemplateParameterId int not null,
	[Value] nvarchar(128) not null default '',

	constraint PK_TemplateParameterOptions primary key (Id),
	constraint UX_TemplateParameterOptions unique ([Key], TemplateParameterId),
	constraint FK_TemplateParameterOptions_TemplateParameter foreign key (TemplateParameterId) references TemplateParameter (Id)
)

insert into Version (Major, Minor, Revision) values (0, 0, 13);

commit transaction
