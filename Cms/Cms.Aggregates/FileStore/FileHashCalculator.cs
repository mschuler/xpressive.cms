using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Cms.Aggregates.FileStore
{
    internal class FileHashCalculator : IFileHashCalculator
    {
        public byte[] Sha1(FileInfo file)
        {
            using (var algorithm = new SHA1Managed())
            {
                return Get(file, algorithm);
            }
        }

        public byte[] Sha256(FileInfo file)
        {
            using (var algorithm = new SHA256Managed())
            {
                return Get(file, algorithm);
            }
        }

        public string Sha1AsBase64String(FileInfo file)
        {
            var hash = Sha1(file);
            return Convert.ToBase64String(hash);
        }

        public string Sha256AsHexString(FileInfo file)
        {
            var hash = Sha256(file);
            return ToHex(hash);
        }

        public string Sha256AsBase64String(FileInfo file)
        {
            var hash = Sha256(file);
            return Convert.ToBase64String(hash);
        }

        private string ToHex(byte[] hash)
        {
            var result = new StringBuilder(hash.Length * 2);

            foreach (var b in hash)
            {
                result.AppendFormat("{0:x2}", b);
            }

            return result.ToString();
        }

        private byte[] Get(FileInfo file, HashAlgorithm algorithm)
        {
            using (var stream = file.OpenRead())
            {
                return algorithm.ComputeHash(stream);
            }
        }
    }
}