﻿using System;

namespace Cms.EventSourcing.Contracts.Events
{
    public interface IEventSerializer
    {
        string Serialize(IEvent @event);

        IEvent Deserialize(string data, Type type);
    }
}