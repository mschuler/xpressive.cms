﻿(function() {
    angular.module("app").controller("EventlogController", ["$rootScope", "$scope", "$http", "tenantService", function ($rootScope, $scope, $http, tenantService) {
        $scope.events = [];
        $scope.isBusy = false;
        var isFinish = false;

        $scope.nextPage = function() {
            if ($scope.isBusy || isFinish) {
                return;
            }
            $scope.isBusy = true;

            var count = $scope.events.length;
            var url = String.format("/api/events/{1}?$skip={0}&$top=20&$orderby=Id desc", count, tenantService.getTenant().id);
            $http.get(url, { cache: false }).success(function (data) {
                if (data.length < 20) {
                    isFinish = true;
                }

                for (var i = 0; i < data.length; i++) {
                    $scope.events.push(data[i]);
                }
                $scope.isBusy = false;
            });
        };

        $rootScope.$on("logout", function () { $scope.events = []; });

        $rootScope.$on("tenantChanged", function() {
            $scope.events = [];
            isFinish = false;
            $scope.isBusy = false;
            $scope.nextPage();
        });
    }]);

    angular.module("app").controller("AggregateEventlogController", ["$rootScope", "$scope", "$http", "$routeParams", "tenantService", function ($rootScope, $scope, $http, $routeParams, tenantService) {
        var aggregateId = $routeParams.aggregateId;
        var aggregateType = $routeParams.aggregateType;
        $scope.events = [];
        $scope.isBusy = false;
        var isFinish = false;

        $scope.nextPage = function () {
            if ($scope.isBusy || isFinish) {
                return;
            }
            $scope.isBusy = true;

            var count = $scope.events.length;
            var url = String.format("/api/events/{1}/object/{3}/{2}?$skip={0}&$top=20&$orderby=Id desc", count, tenantService.getTenant().id, aggregateId, aggregateType);
            $http.get(url, { cache: false }).success(function (data) {
                if (data.length < 20) {
                    isFinish = true;
                }

                for (var i = 0; i < data.length; i++) {
                    $scope.events.push(data[i]);
                }
                $scope.isBusy = false;
            });
        };

        $rootScope.$on("logout", function () { $scope.events = []; });

        $rootScope.$on("tenantChanged", function () {
            $scope.events = [];
            isFinish = false;
            $scope.isBusy = false;
            $scope.nextPage();
        });
    }]);
}());