using System.Collections.Generic;

namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectCreated : EventBase<DynamicObject>
    {
        private readonly Dictionary<string, string> _values;

        public DynamicObjectCreated()
        {
            _values = new Dictionary<string, string>();
        }

        public int PublicId { get; set; }
        public ulong TenantId { get; set; }
        public ulong DefinitionId { get; set; }
        public string Title { get; set; }

        public Dictionary<string, string> Values
        {
            get { return _values; }
            set
            {
                _values.Clear();
                foreach (var pair in value)
                {
                    _values.Add(pair.Key, pair.Value);
                }
            }
        }
    }
}