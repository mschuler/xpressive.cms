﻿using System.Collections.Generic;
using System.Linq;

namespace Cms.Aggregates.Forms
{
    public class Form : AggregateBase
    {
        private readonly object _fieldsLock = new object();
        private readonly IList<FormField> _fields;
        private readonly object _entriesLock = new object();
        private readonly IList<FormEntry> _entries;
        private readonly object _actionsLock = new object();
        private readonly IList<FormAction> _actions;
        private readonly object _generatorFieldsLock = new object();
        private readonly IList<GeneratorField> _generatorFields;

        public Form(ulong tenantId, ulong id, string name, int publicId)
        {
            TenantId = tenantId;
            Id = id;
            Name = name;
            PublicId = publicId;

            _fields = new List<FormField>();
            _entries = new List<FormEntry>();
            _actions = new List<FormAction>();
            _generatorFields = new List<GeneratorField>();
        }

        public ulong TenantId { get; private set; }
        public string Name { get; set; }
        public int PublicId { get; private set; }
        public string Template { get; set; }
        public string GeneratorLabelPosition { get; set; }
        public bool IsDuplicate { get; set; }

        public IEnumerable<FormField> Fields
        {
            get
            {
                lock (_fieldsLock)
                {
                    return _fields.ToList();
                }
            }
        }

        public IEnumerable<FormEntry> Entries
        {
            get
            {
                lock (_entriesLock)
                {
                    return _entries.ToList();
                }
            }
        }

        public IEnumerable<FormAction> Actions
        {
            get
            {
                lock (_actionsLock)
                {
                    return _actions.ToList();
                }
            }
        }

        public IEnumerable<GeneratorField> GeneratorFields
        {
            get
            {
                lock (_generatorFieldsLock)
                {
                    return _generatorFields.ToList();
                }
            }
        }

        public void Add(FormField field)
        {
            lock (_fieldsLock)
            {
                _fields.Add(field);
            }
        }

        public void Remove(FormField field)
        {
            lock (_fieldsLock)
            {
                _fields.Remove(field);
            }
        }

        public void Add(FormEntry entry)
        {
            lock (_entriesLock)
            {
                _entries.Add(entry);
            }
        }

        public void Remove(FormEntry entry)
        {
            lock (_entriesLock)
            {
                _entries.Remove(entry);
            }
        }

        public void ClearEntries()
        {
            lock (_entriesLock)
            {
                _entries.Clear();
            }
        }

        public void Add(FormAction action)
        {
            lock (_actionsLock)
            {
                _actions.Add(action);
            }
        }

        public void Remove(FormAction action)
        {
            lock (_actionsLock)
            {
                _actions.Remove(action);
            }
        }

        public void Add(GeneratorField field)
        {
            lock (_generatorFieldsLock)
            {
                _generatorFields.Add(field);
            }
        }

        public void Remove(GeneratorField field)
        {
            lock (_generatorFieldsLock)
            {
                _generatorFields.Remove(field);
            }
        }

        public void ReplaceAll(IEnumerable<GeneratorField> fields)
        {
            lock (_generatorFieldsLock)
            {
                _generatorFields.Clear();

                foreach (var field in fields.OrderBy(f => f.SortOrder))
                {
                    _generatorFields.Add(field);
                }
            }
        }

        public object ToDto()
        {
            return new
            {
                Id = Id.ToJsonString(),
                Name,
                PublicId,
                IsDuplicate,
                Fullname = string.Format("{0} (Nr. {1:D})", Name, PublicId),
            };
        }
    }
}
