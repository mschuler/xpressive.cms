﻿namespace Cms.Aggregates.DynamicObjects.Events
{
    public class DynamicObjectTemplateDeleted : EventBase<DynamicObjectDefinition>
    {
        public ulong TemplateId { get; set; }
        public string Name { get; set; }
    }
}