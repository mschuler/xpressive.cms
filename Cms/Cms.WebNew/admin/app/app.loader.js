﻿(function(window) {
    var app = angular.module("app");

    window.deferredBootstrapper.bootstrap({
        element: window.document.body,
        module: 'app',
        resolve: {
            bootstrapResult: ["$http", "$log", "$q", "$location", function ($http, $log, $q, $location) {
                var deferred = $q.defer();

                app.value("tenants", []);
                app.value("currentTenant", null);
                app.value("currentUser", null);

                if (!Modernizr.localstorage) {
                    $log.warn("this browser does not support local storage.");
                    deferred.reject();
                    return deferred.promise;
                }

                var setTenantAndUser = function(ten, authDataToken) {
                    var storedCurrentTenant = angular.fromJson(localStorage['xpressivecms.currentTenant']);
                    var currentTenant = storedCurrentTenant
                        ? _.find(ten, function (tx) { return tx.id == storedCurrentTenant.id; })
                        : ten[0];
                    app.value("currentTenant", currentTenant);

                    $http.get("/api/user/current", { cache: false, headers: { Authorization: "Bearer " + authDataToken } }).success(function (cu) {
                        app.value("currentUser", cu);
                        deferred.resolve("yeah!");
                    }).error(function () {
                        $location.path('/login');
                        deferred.resolve("no!");
                    });
                };

                var authData = angular.fromJson(localStorage['xpressivecms.authorizationData']);
                if (authData && authData.expires) {
                    var exp = new Date(authData.expires);
                    var oneDay = 1000 * 60 * 60 * 24;

                    if ((exp - new Date) >= (30 * oneDay)) {
                        //var tenants = angular.fromJson(localStorage['xpressivecms.tenants']);

                        //if (tenants && tenants.length > 0) {
                        //    app.value("tenants", tenants);
                        //    setTenantAndUser(tenants, authData.token);
                        //} else {
                            $http.get("/api/tenants", { cache: false, headers: { Authorization: "Bearer " + authData.token } }).success(function(t) {
                                app.value("tenants", t);
                                localStorage['xpressivecms.tenants'] = angular.toJson(t);
                                setTenantAndUser(t, authData.token);
                            }).error(function () {
                                $location.path('/login');
                                deferred.resolve("no!");
                            });
                        //}
                    } else {
                        $location.path('/login');
                        deferred.resolve("no!");
                    }
                } else {
                    $location.path('/login');
                    deferred.resolve("no!");
                }

                return deferred.promise;
            }],
        },
        onError: function(error) {
            console.log(error);
        }
    });
}(window));