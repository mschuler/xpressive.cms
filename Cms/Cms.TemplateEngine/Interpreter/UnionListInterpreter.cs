using System;
using System.Collections;
using System.Collections.Generic;
using Cms.TemplateEngine.Enums;
using Cms.TemplateEngine.Interfaces;

namespace Cms.TemplateEngine.Interpreter
{
    public sealed class UnionListInterpreter : IInterpreter
    {
        public int Priority { get; set; }

        public string Name { get { return "union"; } }

        public void Calculate(TreeItem tree)
        {
            if (tree.ItemType == TreeItemType.Function &&
                tree.Value.Equals("union", StringComparison.OrdinalIgnoreCase) &&
                tree.LeftItem != null &&
                tree.LeftItem.Result is IEnumerable &&
                tree.Children.Count == 1 &&
                tree.Children[0].Result is IEnumerable)
            {
                var result = ((IEnumerable)tree.LeftItem.Result).GetListOfSameType();
                result.AddRange((IEnumerable<object>)tree.LeftItem.Result);
                result.AddRange((IEnumerable<object>)tree.Children[0].Result);
                tree.Result = result;
            }
        }
    }
}