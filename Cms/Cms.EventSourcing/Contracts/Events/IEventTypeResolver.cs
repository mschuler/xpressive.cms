﻿using System;

namespace Cms.EventSourcing.Contracts.Events
{
    public interface IEventTypeResolver
    {
        Type GetEventType(string typeName);
    }
}