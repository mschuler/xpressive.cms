begin transaction

alter table [File] add Description nvarchar(1024) not null default '';
alter table FormField add ErrorMessage nvarchar(128) not null default '';

insert into Version (Major, Minor, Revision) values (0, 0, 8);

commit transaction