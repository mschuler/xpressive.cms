﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Cms.DatabaseMigrations")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0db901d7-c67f-49df-8665-acc0d7399bbc")]

[assembly: InternalsVisibleTo("Cms.DatabaseMigrations.Tests")]
