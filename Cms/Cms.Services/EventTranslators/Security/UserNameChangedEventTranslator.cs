using System;
using Cms.Aggregates.Security;
using Cms.Aggregates.Security.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Security
{
    internal class UserNameChangedEventTranslator : IEventTranslator
    {
        private readonly IUserRepository _repository;

        public UserNameChangedEventTranslator(IUserRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(UserNameChanged);
        }

        public string GetHeader(IEvent @event)
        {
            return "Name ge�ndert";
        }

        public string GetContent(IEvent @event)
        {
            var e = (UserNameChanged)@event;
            var user = _repository.GetEvenIfDeleted(e.AggregateId);
            var username = string.Format("{0} {1}", user.FirstName, user.LastName).Trim();
            var newName = string.Format("{0} {1}", e.FirstName, e.LastName).Trim();
            return string.Format("Der Name des Benutzers <strong>{0}</strong> wurde in in <strong>{1}</strong> ge�ndert.", username, newName);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return 0;
        }

        public string GetAffectedModuleId()
        {
            return "sysadmin";
        }
    }
}