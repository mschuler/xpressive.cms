using System;
using Cms.Aggregates.Forms;
using Cms.Aggregates.Forms.Events;
using Cms.EventSourcing.Contracts.Events;
using Cms.Services.Contracts;

namespace Cms.Services.EventTranslators.Forms
{
    internal class FormEntriesClearedEventTranslator : IEventTranslator
    {
        private readonly IFormRepository _repository;

        public FormEntriesClearedEventTranslator(IFormRepository repository)
        {
            _repository = repository;
        }

        public bool IsResponsibleFor(Type eventType)
        {
            return eventType == typeof(FormEntriesCleared);
        }

        public string GetHeader(IEvent @event)
        {
            return "Formulareinträge gelöscht";
        }

        public string GetContent(IEvent @event)
        {
            var form = _repository.GetEvenIfDeleted(@event.AggregateId);
            var name = string.Format("{0} (Nr. {1})", form.Name, form.PublicId);
            return string.Format("Die Formulareinträge von Formular <strong>{0}</strong> wurden entfernt.", name);
        }

        public ulong GetAffectedTenantId(IEvent @event)
        {
            return _repository.GetEvenIfDeleted(@event.AggregateId).TenantId;
        }

        public string GetAffectedModuleId()
        {
            return "form";
        }
    }
}