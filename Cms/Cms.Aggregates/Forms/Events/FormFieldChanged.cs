﻿namespace Cms.Aggregates.Forms.Events
{
    public class FormFieldChanged : EventBase<Form>
    {
        public ulong Id { get; set; }

        public string Name { get; set; }
    }
}