using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cms.Aggregates.Links;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Calendars.Commands
{
    [RequiredPermission("appointment", PermissionLevel.CanChange)]
    public class ChangeAppointment : CommandBase
    {
        [ValidId]
        public ulong AppointmentId { get; set; }

        [Required(AllowEmptyStrings = false), MinLength(1)]
        public string Title { get; set; }

        public bool IsHighlight { get; set; }

        [Required]
        public DateTimeRange StartEnd { get; set; }

        [Required]
        public string Location { get; set; }

        public ILink Link { get; set; }

        [Required]
        public AppointmentRecurrence Recurrence { get; set; }

        [Required]
        public IList<string> Categories { get; set; }
    }
}