namespace Cms.Aggregates.Pages.Events
{
    public class PageTemplateChanged : EventBase<PageBase>
    {
        public ulong PageTemplateId { get; set; }
    }
}