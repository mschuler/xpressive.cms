﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Aggregates.Tenants;

namespace Cms.Aggregates.DynamicObjects.Queries
{
    internal class DynamicObjectDefinitionQueryHandler : QueryHandlerBase
    {
        private readonly IDynamicObjectDefinitionRepository _repository;
        private readonly IUserStarredDynamicObjectDefinitionRepository _userStarredRepository;
        private readonly ITenantRepository _tenantRepository;

        public DynamicObjectDefinitionQueryHandler(IDynamicObjectDefinitionRepository repository, IUserStarredDynamicObjectDefinitionRepository userStarredRepository, ITenantRepository tenantRepository)
        {
            _repository = repository;
            _userStarredRepository = userStarredRepository;
            _tenantRepository = tenantRepository;
        }

        protected override Type[] GetQueryTypes()
        {
            return new[]
            {
                typeof (GetAllObjectDefinitions),
                typeof (GetObjectDefinition),
                typeof (GetSharedDynamicObjectDefinitions)
            };
        }

        public IEnumerable<object> When(GetSharedDynamicObjectDefinitions query)
        {
            return _repository.GetAll()
                .Where(d => d.TenantId != query.TenantId)
                .Where(d => d.SharedForTenants.Contains(query.TenantId))
                .OrderBy(d => d.Name, StringComparer.OrdinalIgnoreCase)
                .Select(d => CreateDto(d, query.UserId, false));
        }

        public IEnumerable<object> When(GetAllObjectDefinitions query)
        {
            return _repository
                .GetAll(query.TenantId)
                .OrderBy(d => d.Name)
                .Select(d => CreateDto(d, query.UserId, false));
        }

        public IEnumerable<object> When(GetObjectDefinition query)
        {
            var definition = _repository.Get(query.Id);
            if (definition.TenantId != query.TenantId && !definition.SharedForTenants.Contains(query.TenantId))
            {
                yield break;
            }
            yield return CreateDto(definition, query.UserId, true);
        }

        private object CreateDto(DynamicObjectDefinition definition, ulong userId, bool withFields)
        {
            var fields = new List<object>();

            if (withFields)
            {
                fields = definition
                    .Fields
                    .OrderBy(f => f.SortOrder)
                    .ToList<object>();
            }

            var tenants = new List<object>();
            foreach (var tenantId in definition.SharedForTenants)
            {
                Tenant tenant;
                if (_tenantRepository.TryGet(tenantId, out tenant))
                {
                    tenants.Add(new
                    {
                        Id = tenant.Id.ToJsonString(),
                        tenant.Name
                    });
                }
            }

            return new
            {
                Id = definition.Id.ToJsonString(),
                Nr = definition.PublicId,
                definition.Name,
                Fields = fields,
                IsStarred = _userStarredRepository.IsStarred(userId, definition.Id),
                Fullname = string.Format("{0} (Nr. {1})", definition.Name, definition.PublicId),
                Shared = tenants
            };
        }
    }
}
