﻿using Cms.Aggregates.Security;

namespace Cms.Aggregates.DynamicObjects.Queries
{
    [RequiredPermission("object", PermissionLevel.CanSee)]
    public class GetObject : QueryBase
    {
        public ulong Id { get; set; }
    }
}