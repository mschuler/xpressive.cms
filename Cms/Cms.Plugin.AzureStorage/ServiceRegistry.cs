using Autofac;
using Cms.Aggregates.FileStore;

namespace Cms.Plugin.AzureStorage
{
    public class ServiceRegistry : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AzureFileStoreFactory>().As<IFileStoreFactory>();

            base.Load(builder);
        }
    }
}