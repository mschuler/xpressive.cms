﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Cms.Services.Tests
{
    public class Given_an_api_key_service
    {
        private readonly ITestOutputHelper _outputWriter;
        private readonly ApiKeyService _service;

        public Given_an_api_key_service(ITestOutputHelper outputWriter)
        {
            _outputWriter = outputWriter;
            _service = new ApiKeyService(EncryptionService.Create(Guid.NewGuid().ToString("N")));
        }

        [Fact]
        public void When_generating_keys_then_they_are_secure()
        {
            var pair = _service.GenerateNewKey("127.0.0.1");

            _outputWriter.WriteLine("ApiKey: {0}", pair.ApiKey);
            _outputWriter.WriteLine("Cookie: {0}", pair.Cookie);

            Assert.True(pair.Cookie.Length > 32);
        }

        [Fact]
        public void When_generating_keys_then_they_are_unique()
        {
            var keys = new ConcurrentBag<string>();
            var cookies = new ConcurrentBag<string>();

            Parallel.For(0, 10000, _ =>
            {
                var pair = _service.GenerateNewKey("127.0.0.1");
                keys.Add(pair.ApiKey);
                cookies.Add(pair.Cookie);
            });

            Assert.True(keys.Distinct(StringComparer.OrdinalIgnoreCase).Count() == 10000);
            Assert.True(cookies.Distinct(StringComparer.Ordinal).Count() == 10000);
        }

        [Fact]
        public void When_validating_then_the_pair_has_to_match()
        {
            var ipAddress = "127.0.0.1";
            var pair1 = _service.GenerateNewKey(ipAddress);
            var pair2 = _service.GenerateNewKey(ipAddress);

            Assert.True(_service.IsValidApiKey(pair1.ApiKey, pair1.Cookie, ipAddress));
            Assert.True(_service.IsValidApiKey(pair2.ApiKey, pair2.Cookie, ipAddress));

            Assert.False(_service.IsValidApiKey(pair1.ApiKey, pair2.Cookie, ipAddress));
            Assert.False(_service.IsValidApiKey(pair2.ApiKey, pair1.Cookie, ipAddress));
        }

        [Fact]
        public void When_validating_then_the_ip_address_has_to_match()
        {
            var ipAddress1 = "127.0.0.1";
            var ipAddress2 = "127.0.0.2";

            var pair1 = _service.GenerateNewKey(ipAddress1);
            var pair2 = _service.GenerateNewKey(ipAddress2);

            Assert.False(_service.IsValidApiKey(pair1.ApiKey, pair1.Cookie, ipAddress2));
            Assert.False(_service.IsValidApiKey(pair2.ApiKey, pair2.Cookie, ipAddress1));
        }
    }
}
