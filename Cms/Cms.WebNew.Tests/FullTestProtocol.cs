﻿using System;
using System.Diagnostics;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using RestSharp;
using Xunit;
using Xunit.Abstractions;

namespace Cms.WebNew.Tests
{
    public class FullTestProtocol
    {
        private static readonly string _baseUrl = "http://localhost:40029/";
        private readonly ITestOutputHelper _outputWriter;
        private string _oauthToken = "";

        public FullTestProtocol(ITestOutputHelper outputWriter)
        {
            _outputWriter = outputWriter;
        }

        [Fact(Skip = "This is a tool, not a test.")]
        public void BruteForcePassword()
        {
            var stopwatch = Stopwatch.StartNew();

            for (var i = 0; i < 5; i++)
            {
                TryLogin("test@test.test", "this is not the right one", HttpStatusCode.BadRequest);
            }

            stopwatch.Stop();
            _outputWriter.WriteLine("Took {0}ms", stopwatch.ElapsedMilliseconds);

            Assert.True(stopwatch.Elapsed.TotalSeconds > 10);
        }

        [Fact(Skip = "This is a tool, not a test.")]
        public void Run()
        {
            _oauthToken = Login("", "");

            var tenantId = CreateTenant("test tenant A");
            Post("api/tenant/" + tenantId, new { name = "test tenant ABC" });
            Delete("api/tenant/" + tenantId);
            tenantId = CreateTenant("Test Tenant B");
            var userIdForbidden = CreateUser(tenantId, "u_forbidden", "forbidden@test.test", "pwforbidden");
            var userIdView = CreateUser(tenantId, "u_view", "view@test.test", "pwview");
            var userIdEdit = CreateUser(tenantId, "u_edit", "edit@test.test", "pwedit");
            var userIdAdmin = CreateUser(tenantId, "u_admin", "admin@test.test", "pwadmin");
            var userIdTest = CreateUser(tenantId, "u_test", "test@test.test", "pwtest");

            try
            {
                var tokenForbidden = Login("forbidden@test.test", "pwforbidden");
                var tokenView = Login("view@test.test", "pwview");
                var tokenEdit = Login("edit@test.test", "pwedit");
                var tokenAdmin = Login("admin@test.test", "pwadmin");

                TryLogin("test@test.test", "pwtest");
                Post("api/users/" + tenantId + "/user/" + userIdTest + "/lock", null);
                TryLogin("test@test.test", "pwtest", HttpStatusCode.BadRequest);
                ChangePassword(tenantId, userIdTest, "u_test", "test@test.test", "pwtest2");
                Post("api/users/" + tenantId + "/user/" + userIdTest + "/unlock", null);
                TryLogin("test@test.test", "pwtest2");
                Delete("api/users/" + tenantId + "/user/" + userIdTest);
                TryLogin("test@test.test", "pwtest2", HttpStatusCode.BadRequest);

                var groupIdView = CreateGroup(tenantId, "g_view");
                var groupIdForbidden = CreateGroup(tenantId, "g_forbidden");
                var groupIdEdit = CreateGroup(tenantId, "g_edit");
                var groupIdAdmin = CreateGroup(tenantId, "g_admin");
                var groupIdTest = CreateGroup(tenantId, "test group A");

                Post("api/group/" + groupIdTest + "/name", new { name = "test group ABC" });
                Delete("api/group/" + groupIdTest);

                ChangePermission(groupIdForbidden, 0);
                ChangePermission(groupIdView, 1);
                ChangePermission(groupIdEdit, 2);
                ChangePermission(groupIdAdmin, 3);

                Post("api/members/" + groupIdForbidden + "/add/" + userIdForbidden, null);
                Post("api/members/" + groupIdView + "/add/" + userIdView, null);
                Post("api/members/" + groupIdEdit + "/add/" + userIdEdit, null);
                Post("api/members/" + groupIdAdmin + "/add/" + userIdAdmin, null);

                // save email settings

                var websiteId = CreateWebsite(tenantId, "test website A");
                Put("api/website/" + websiteId + "/domain", new { domain = "test.localhost" });
                Put("api/website/" + websiteId + "/domain", new { domain = "demo.localhost" });
                Post("api/website/" + websiteId + "/domain", new { oldDomain = "demo.localhost", newDomain = "preview.localhost" });
                Post("api/website/" + websiteId + "/default", new { domain = "test.localhost" });
                Delete("api/website/" + websiteId + "/domain", new { domain = "preview.localhost" });
                Post("api/website/" + websiteId, new { name = "test website ABC" });

                // create page templates

                // create object definition

                // create object

                var directoryId00 = PostWithId("api/filedirectory/" + tenantId + "/0", new { name = "directory 00" });
                var directoryId01 = PostWithId("api/filedirectory/" + tenantId + "/" + directoryId00, new { name = "directory 01" });
                var directoryId02 = PostWithId("api/filedirectory/" + tenantId + "/" + directoryId00, new { name = "directory 02" });
                var directoryId10 = PostWithId("api/filedirectory/" + tenantId + "/0", new { name = "directory 10" });
                var directoryId11 = PostWithId("api/filedirectory/" + tenantId + "/" + directoryId10, new { name = "directory 11" });
                var directoryId12 = PostWithId("api/filedirectory/" + tenantId + "/" + directoryId10, new { name = "directory 12" });

                // upload two files
                // delete directory
                // create directory
                // get trash files
                // move files to new directory
                // delete one file

                // create form
                // add form fields
                // add email action
                // set form template

                // create page
                // add form to page

                // test page -> html

                // send form
                // add save action
                // add redirect action
                // send form
                // get form entries
                // remove save action
                // send form
                // get form entries

                // test if u_forbidden sees nothing
                // test if u_view sees everything
                // test if u_view can't change anything
                // test if u_edit can change everything
                // test if u_edit can't delete anything
                // test if u_admin can delete everything
            }
            finally
            {
                Delete("api/users/" + tenantId + "/user/" + userIdForbidden);
                Delete("api/users/" + tenantId + "/user/" + userIdView);
                Delete("api/users/" + tenantId + "/user/" + userIdEdit);
                Delete("api/users/" + tenantId + "/user/" + userIdAdmin);
                Delete("api/tenant/" + tenantId);
            }
        }

        #region Login

        private string Login(string username, string password)
        {
            dynamic result = TryLogin(username, password);
            string accessToken = result.access_token;
            string tokenType = result.token_type;
            string userName = result.userName;

            Assert.True(accessToken.Length > 100);
            Assert.Equal("bearer", tokenType);
            Assert.Equal(username, userName);

            return accessToken;
        }

        private dynamic TryLogin(string username, string password, HttpStatusCode expectedStatusCode = HttpStatusCode.OK)
        {
            var restClient = new RestClient(_baseUrl);
            var request = new RestRequest("api/token", Method.POST);
            request.AddParameter("grant_type", "password");
            request.AddParameter("username", username);
            request.AddParameter("password", Convert.ToBase64String(Encoding.UTF8.GetBytes(password)));

            var response = restClient.Execute(request);
            Assert.Equal(expectedStatusCode, response.StatusCode);
            dynamic result = JsonConvert.DeserializeObject(response.Content);
            return result;
        }

        private void ChangePassword(ulong tenantId, ulong userId, string name, string email, string password)
        {
            Post("api/users/" + tenantId + "/user/" + userId, new
            {
                firstName = "",
                lastName = name,
                email,
                password = Convert.ToBase64String(Encoding.UTF8.GetBytes(password)),
            });
        }

        #endregion

        #region Tenants

        private ulong CreateTenant(string name)
        {
            return PutWithId("api/tenant", new { name });
        }

        #endregion

        #region Website

        private ulong CreateWebsite(ulong tenantId, string name)
        {
            return PutWithId("api/website/" + tenantId, new { name });
        }

        #endregion

        #region Groups & Users

        private ulong CreateGroup(ulong tenantId, string name)
        {
            return PostWithId("api/groups/" + tenantId, new { name });
        }

        private ulong CreateUser(ulong tenantId, string name, string email, string password)
        {
            password = Convert.ToBase64String(Encoding.UTF8.GetBytes(password));

            dynamic result = Post("api/users/" + tenantId, new { email, password, firstName = "", lastName = name }, HttpStatusCode.OK);
            string id = result.id;
            var userId = ulong.Parse(id);

            Assert.NotEqual(0UL, userId);

            return userId;
        }

        private void ChangePermission(ulong groupId, int permission)
        {
            Post("api/permissions/" + groupId, new { moduleId = "page", permission });
            Post("api/permissions/" + groupId, new { moduleId = "pagetemplate", permission });
            Post("api/permissions/" + groupId, new { moduleId = "file", permission });
            Post("api/permissions/" + groupId, new { moduleId = "form", permission });
            Post("api/permissions/" + groupId, new { moduleId = "object", permission });
            Post("api/permissions/" + groupId, new { moduleId = "objecttemplate", permission });
            Post("api/permissions/" + groupId, new { moduleId = "objectdefinition", permission });
            Post("api/permissions/" + groupId, new { moduleId = "appointment", permission });
            Post("api/permissions/" + groupId, new { moduleId = "calendarcategory", permission });
            Post("api/permissions/" + groupId, new { moduleId = "calendartemplate", permission });
            Post("api/permissions/" + groupId, new { moduleId = "tenant", permission });
            Post("api/permissions/" + groupId, new { moduleId = "website", permission });
            Post("api/permissions/" + groupId, new { moduleId = "user", permission });
            Post("api/permissions/" + groupId, new { moduleId = "group", permission });
        }

        #endregion

        #region helpers

        private dynamic Put(string url, object json, HttpStatusCode expectedStatusCode = HttpStatusCode.NoContent)
        {
            return SendJson(url, json, Method.PUT, expectedStatusCode);
        }

        private dynamic Post(string url, object json, HttpStatusCode expectedStatusCode = HttpStatusCode.NoContent)
        {
            return SendJson(url, json, Method.POST, expectedStatusCode);
        }

        private ulong PostWithId(string url, object json, HttpStatusCode expectedStatusCode = HttpStatusCode.OK)
        {
            return SendJsonAndGetId(url, json, Method.POST, expectedStatusCode);
        }

        private ulong PutWithId(string url, object json, HttpStatusCode expectedStatusCode = HttpStatusCode.OK)
        {
            return SendJsonAndGetId(url, json, Method.PUT, expectedStatusCode);
        }

        private ulong SendJsonAndGetId(string url, object json, Method method, HttpStatusCode expectedStatusCode = HttpStatusCode.OK)
        {
            var result = SendJson(url, json, method, expectedStatusCode);
            string id = result.id;
            var parsedId = ulong.Parse(id);
            Assert.NotEqual(0UL, parsedId);
            return parsedId;
        }

        private dynamic SendJson(string url, object json, Method method, HttpStatusCode expectedStatusCode)
        {
            var restClient = new RestClient(_baseUrl);
            var request = GetRequest(url, method);

            if (json != null)
            {
                request.AddJsonBody(json);
            }

            var response = restClient.Execute(request);

            if (response.StatusCode == HttpStatusCode.BadRequest)
            {
                Assert.True(false, response.Content);
            }

            Assert.Equal(expectedStatusCode, response.StatusCode);
            return JsonConvert.DeserializeObject(response.Content);
        }

        private void Delete(string url, object json = null, HttpStatusCode expectedStatusCode = HttpStatusCode.NoContent)
        {
            SendJson(url, json, Method.DELETE, expectedStatusCode);
        }

        private RestRequest GetRequest(string url, Method method)
        {
            var request = new RestRequest(url, method);
            request.AddHeader("Authorization", "Bearer " + _oauthToken);
            return request;
        }

        #endregion
    }
}