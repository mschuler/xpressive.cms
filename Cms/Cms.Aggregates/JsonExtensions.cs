using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Cms.Aggregates.Files;
using Cms.SharedKernel;
using Newtonsoft.Json;
using File = Cms.Aggregates.Files.File;

namespace Cms.Aggregates
{
    public static class JsonExtensions
    {
        public static string ToJsonString(this ulong id)
        {
            return id.ToString("D", CultureInfo.InvariantCulture);
        }

        public static string ToDownloadUrl(this File file, bool withTracking = true, ImageSettingsDto imageSettings = null)
        {
            var tracking = withTracking ? string.Empty : "?isTracked=false";
            var json = string.Empty;

            if (file.IsImage() && imageSettings != null && !imageSettings.Equals(new ImageSettingsDto()))
            {
                json = ImageSettingsDto.Serialize(imageSettings) + "/";
            }

            return string.Format(
                CultureInfo.InvariantCulture,
                "/download/{0:D}/{1:D}/{2}{3}{4}",
                file.Id,
                file.Versions.Max(v => v.Version),
                json,
                file.Name.ToValidFileName(),
                tracking);
        }

        public static string ToPreviewUrl(this File file)
        {
            if (!file.IsImage())
            {
                var extension = file.GetExtension();
                var rootDirectory = AppDomain.CurrentDomain.BaseDirectory;
                var filename = extension.Trim(' ', '.') + ".png";
                var path = Path.Combine(rootDirectory, @"admin\images\filetypes", filename);

                if (System.IO.File.Exists(path))
                {
                    return "/admin/images/filetypes/" + filename;
                }

                return "/admin/images/filetypes/default.png";
            }

            return string.Format(
                CultureInfo.InvariantCulture,
                "/api/preview/{0:D}/{1:D}/{2}",
                file.Id,
                file.Versions.Max(v => v.Version),
                file.Name.ToValidFileName());
        }
    }

    public class ImageSettingsDto
    {
        private static readonly Regex _removeEmptyAttributes = new Regex("\"(?:[a-zA-Z0-9]+)\":(?:\"\"|0),?", RegexOptions.Compiled, TimeSpan.FromMilliseconds(50));

        public static string Serialize(ImageSettingsDto dto)
        {
            var json = JsonConvert.SerializeObject(dto, Formatting.None);

            foreach (Match match in _removeEmptyAttributes.Matches(json))
            {
                json = json.Replace(match.Value, string.Empty);
            }

            var bytes = Encoding.UTF8.GetBytes(json);
            var base64 = Convert.ToBase64String(bytes, Base64FormattingOptions.None);

            base64 = base64.Replace("/", "_");
            base64 = base64.Replace("+", "-");
            //base64 = base64.Replace("=", "");
            return base64;
        }

        public static ImageSettingsDto Deserialize(string base64)
        {
            base64 = base64.Replace("_", "/");
            base64 = base64.Replace("-", "+");

            var bytes = Convert.FromBase64String(base64);
            var json = Encoding.UTF8.GetString(bytes);
            return JsonConvert.DeserializeObject<ImageSettingsDto>(json);
        }

        public ImageSettingsDto()
        {
            Alpha = 100;
            Quality = 100;
        }

        public int Size { get; set; }

        // 0 .. 100
        public int Alpha { get; set; }

        // -100 .. +100
        public int Saturation { get; set; }

        public int RoundedCorners { get; set; }

        // -100 .. +100
        public int Brightness { get; set; }

        // -100 .. +100
        public int Contrast { get; set; }

        // color name
        public string Tint { get; set; }

        // filter name
        public string Filter { get; set; }

        public int Quality { get; set; }

        protected bool Equals(ImageSettingsDto other)
        {
            return
                Size == other.Size &&
                Alpha == other.Alpha &&
                Saturation == other.Saturation &&
                RoundedCorners == other.RoundedCorners &&
                Brightness == other.Brightness &&
                Contrast == other.Contrast &&
                Quality == other.Quality &&
                string.Equals(Tint, other.Tint) &&
                string.Equals(Filter, other.Filter);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }
            if (obj.GetType() != GetType()) { return false; }
            return Equals((ImageSettingsDto)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Size;
                hashCode = (hashCode * 397) ^ Alpha;
                hashCode = (hashCode * 397) ^ Saturation;
                hashCode = (hashCode * 397) ^ RoundedCorners;
                hashCode = (hashCode * 397) ^ Brightness;
                hashCode = (hashCode * 397) ^ Contrast;
                hashCode = (hashCode * 397) ^ Quality;
                hashCode = (hashCode * 397) ^ (Tint != null ? Tint.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Filter != null ? Filter.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}