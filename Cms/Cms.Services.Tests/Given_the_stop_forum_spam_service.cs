﻿using Xunit;

namespace Cms.Services.Tests
{
    public class Given_the_stop_forum_spam_service
    {
        [Fact]
        public async void When_suspicious_ip_address_is_detected_then_it_is_spam()
        {
            var ipAddress = "146.185.234.48";
            var service = new StopForumSpamService();

            var result = await service.IsSpam(ipAddress, null);
            Assert.True(result);
        }
    }
}
