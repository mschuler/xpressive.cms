using System.Collections.Generic;

namespace Cms.Aggregates.Forms
{
    public interface IFormRepository : IRepository<Form>
    {
        int GetNextPublicId();

        Form GetByPublicId(int publicId);

        IEnumerable<Form> GetAll(ulong tenantId);
    }
}