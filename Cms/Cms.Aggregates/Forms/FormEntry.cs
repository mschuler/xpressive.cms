﻿using System;
using System.Collections.Generic;

namespace Cms.Aggregates.Forms
{
    public class FormEntry
    {
        public ulong Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string IpAddress { get; set; }
        public bool IsSpam { get; set; }
        public Dictionary<string, string> Values { get; set; }

        protected bool Equals(FormEntry other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }
            if (obj.GetType() != GetType()) { return false; }
            return Equals((FormEntry)obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}