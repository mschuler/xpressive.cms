﻿using System;
using System.Linq;
using Cms.Aggregates.Security;

namespace Cms.Aggregates.Files.Queries
{
    [RequiredPermission("file", PermissionLevel.CanSee)]
    public class GetFilesBySearchPattern : QueryBase
    {
        public string SearchPattern { get; set; }

        internal bool IsMatch(File file)
        {
            var parts = SearchPattern.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            return parts.All(p =>
                file.Name.ToUpperInvariant().Contains(p.ToUpperInvariant()) ||
                file.PublicId.ToString("D").Contains(p));
        }
    }
}